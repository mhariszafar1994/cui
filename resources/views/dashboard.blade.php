@extends('layout-new')

@section('content')
<div class="container-fluid">
	<div class="card min-h-50">
		<div class="card-body">
			<div class="row">
				<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
					<div class="d-md-flex">
						<div class="col flex-md-grow-0 mb-2">
							<img src="{{asset('/assets/img/welcome.png')}}" alt="" />
						</div>
						<div class="col">
							<h2>Welcome to CheckUrInsurance <b class="text-checkurinsurance"> John!</b></h2>
							<p>Below are some details related to your account:</p>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<hr class="border-dashed" />
			<div>
				<p class="m-0">You are logged in as a/an <a class="text-checkurinsurance" href="" title="">Administrator</a></p>
				<p class="m-0">You have selected to work on the following company: <a class="text-checkurinsurance" href="" title="">All Companies</a></p>
				<p class="m-0">You have selected to work on the following office: <a class="text-checkurinsurance" href="" title="">All Offices.</a></p>
			</div>
		</div>
	</div>
</div>
@endsection