<?
//roles check
if($moduleAll == 0 and ($moduleCompaniesEdit==0 and $moduleUsersEdit == 0)){
    echo "<script>window.location='index.php?do=authorization'</script>";
}
//vars
$sortBy = "";
$sortOrder = "";
$assignType = "companies";
$error = "";
$sqlVars = "";
$userType = "";
$queryString = "";
//if get assignType
if($_GET["sortBy"]){
    $sortBy = sanitize($_GET["sortBy"]);
}
if($_GET["sortOrder"]){
    $sortOrder = sanitize($_GET["sortOrder"]);
}
if($_GET["assignType"]){
    $assignType = sanitize($_GET["assignType"]);
    $queryString .= "&assignType=$assignType";
}
//if get userType
if($_GET["userType"]){
    $userType = sanitize($_GET["userType"]);
}
//vars to apply switch below
switch ($userType){
    case 'doctor':
    $headingRedirect = 'Doctor';
    $queryString .= '&userType='.$userType;
    break;
    default:
    $headingRedirect = 'User';
    break;
}
if($assignType == "users"){
    $pageTitle = "Assign Users";
    }else{
    $pageTitle = "Assign Company Offices";
}
//if get act
if($_GET["act"]){
    $act = sanitize($_GET["act"]);
}
//if get msg
if($_GET["msg"]){
    $msg = sanitize($_GET["msg"]);
}
//if get userId
if($_REQUEST["userId"]){
    $userId = sanitize($_REQUEST["userId"]);
    $userFname = getField("cui_users","userId",$userId,"userFname");
    $userLname = getField("cui_users","userId",$userId,"userLname");
    $userName = $userFname." ".$userLname;
    if($assignType!="users"){
        $sqlVars .= " and a.userId=$userId";
    }
    $queryString .= "&userId=$userId";
}
//if get companyId
if($_REQUEST["companyId"]){
    $companyId = sanitize($_REQUEST["companyId"]);
    $companyName = getField("cui_companies","companyId",$companyId,"companyName");
    if($assignType!="companies"){
        $sqlVars .= " and a.companyId=$companyId";
    }
    $queryString .= "&companyId=$companyId";
}
//if All Delete
if($act == "DeleteAll"){
    $checkid = sanitize($_POST["checkid"]);
	if( strpos($checkid, ",") !== false ) {
		$sel_check = explode(",",$checkid);
		foreach ($sel_check as $data) {
			mysqli_query($con, "DELETE FROM cui_users_companies WHERE assignId='$data'");
		}
	} else {
		mysqli_query($con, "DELETE FROM cui_users_companies WHERE assignId='$checkid'");
	}
//    mysql_query("DELETE FROM cui_users_companies WHERE assignId='$assignId'");
    $msg = "User has been unassigned!";
}
//if delete
if($act == "delete"){
    $assignId = sanitize($_GET["assignId"]);
//    mysql_query("DELETE FROM cui_users_companies WHERE assignId='$assignId'");
    mysqli_query($con, "DELETE FROM cui_users_companies WHERE assignId='$assignId'");
    $msg = "User has been unassigned!";
}
//post data
if($_POST){
    $userId = explode("|",$userId);
    $userId = $userId[0];
    $officeId = sanitize($_POST["officeId"]);
    //if all
    $officeAll = 0;
    if($officeId == "All"){
        $officeId = 0;
        $officeAll = 1;
        //delete other offices since all offices
//        mysql_query("DELETE FROM cui_users_companies WHERE companyId='$companyId' and userId='$userId'");
        mysqli_query($con, "DELETE FROM cui_users_companies WHERE companyId='$companyId' and userId='$userId'");
    }
    $sqlCheck = "SELECT * FROM cui_users_companies WHERE companyId='$companyId' and (officeId='$officeId' or officeAll='1') and userId='$userId'";
//    $resultCheck = mysql_query($sqlCheck);
    $resultCheck = mysqli_query($con, $sqlCheck);
//    if(mysql_num_rows($resultCheck)<1){
    if(@mysqli_num_rows($resultCheck)<1){
        //insert sql
//        mysql_query("INSERT INTO cui_users_companies (userId, companyId, officeId, officeAll, assignDate) values ($userId, $companyId, $officeId, $officeAll, '".date("Y-m-d")."')");
        mysqli_query($con, "INSERT INTO cui_users_companies (userId, companyId, officeId, officeAll, assignDate) values ($userId, $companyId, $officeId, $officeAll, '".date("Y-m-d")."')");
        //redirect
        echo "<script>window.location='".HTTP_SERVER."index.php?do=$do$queryString&msg=Assign Successful!'</script>";
    }else{
        $error = "Already Assigned!";
    }
}
?>
<script type="text/javascript">
function chkForm(){
    <?
    //assign type
    if($assignType == "company"){?>
        var a = document.getElementById("companyId").value;
        <?}else{?>
        var a = document.getElementById("userId").value;
    <?}?>
    var b = document.getElementById("officeId").value;
    if(a=="" || b==""){
        //alert("All fields are required!");
		swal({
			title: 'Error!',
			text: 'All fields are required!',
			type: "error",
			showCancelButton: false,
			confirmButtonText: 'OK',
			closeOnConfirm: true
		});
        return false;
    }
}
function officeOptions (companyId){
    if(companyId != ""){
        var i=-1;
        document.getElementById("officeId").options.length = 1;
        <?
        $moduleSql = "SELECT * from cui_companies_offices";
//        $moduleResult = mysql_query($moduleSql);
        $moduleResult = mysqli_query($con, $moduleSql);
//        if(mysql_num_rows($moduleResult)>0){
        if(@mysqli_num_rows($moduleResult)>0){
        ?>
            i++;
            document.getElementById("officeId").options[i] = new Option('- All Offices -','All');
            <?
//            while ($moduleRs = mysql_fetch_array($moduleResult)) {
            while ($moduleRs = @mysqli_fetch_array($moduleResult)) {
                $rsofficeId = $moduleRs["officeId"];
                $officeName = $moduleRs["officeName"];
                $jsCompanyId = $moduleRs["companyId"];
                if($rsofficeId == $officeId){
                    $sel = "selected";
                    }else{
                    $sel = "";
                }
                ?>
                if(<?=$jsCompanyId?> == companyId){
                    i++;
                    document.getElementById("officeId").options[i] = new Option('<?=$officeName?>','<?=$rsofficeId?>');
                }
                <?
            }
        }
        ?>
        }else{
        document.getElementById("officeId").options.length = 1;
        document.getElementById("officeId").options[0] = new Option('--- select ---','');
    }
}
function getlevel()
{
    var userid = document.getElementById('userId').value;
    var getuserlevel = userid.split("|");
    var userlevel = getuserlevel[1];
    if(userid == "") {
        document.getElementById("userlevel").style.display='none';
    }
    else{
        document.getElementById("userlevel").style.display='block';
        document.getElementById("level").innerHTML = userlevel;
    }
}
function getlevelusers(levelId)
{

    result = $.ajax({
		
		type: "GET",

		url: "<?php echo $httpsServer; ?>ajax.php",

		data:{json:'get-level-users',levelId:levelId,assignType:'<?php echo $assignType; ?>'},

		async: false

	}).responseText.split(',');
	
	if(result.length > 0)
	{
		//alert(result);	
		$("#levelusershowhide").show();
		$("#add_level_users").html(result);
	}
	
}
</script>
<h1 class="h1WithBg"><?=$pageTitle?></h1>
<div id="pageContainer">
<div id="breadcrumbs">
<?
if($assignType == "companies"){
    ?>
    <a href="<?=HTTP_SERVER?>index.php?do=users&userType=<?=$userType?>"><?=$headingRedirect?>s > <?=$userName?></a>&nbsp;&nbsp;|&nbsp;&nbsp;Assign Company Offices
    <?}else{?>
    <a href="<?=HTTP_SERVER?>index.php?do=companies">Companies > <?=$companyName?></a>&nbsp;&nbsp;|&nbsp;&nbsp;Assign Users
<?}?>
</div>
<style>
		hr {
			border-bottom: 0px;
		}
		table.form-spacing tbody tr td {
			padding-bottom: 9px;
		}
		</style>
<table class="form-spacing" cellpadding="5" cellspacing="0" align="center" width="100%">
<tr class="titleTr">
<td><h3 style="padding-top: 9px; padding-left: 5px;">Add New</h3></td>
<td><h3></h3></td>
</tr>
<tr>
<td>
<?if($error != ""){?>
    <div class="error"><?=$error?></div>
    <?}else{?>
    <?if($msg==""){?>
        <div class="message">All fields are required!</div>
        <?}else{?>
        <div class="success"><?=$msg?></div>
    <?}?>
<?}?>
</td>
</tr>
<tr>
<td>
<form method="POST">
<table   cellpadding="3" cellspacing="0" align="center">
<?
//if users
//$userSql = "select * from cui_users order by userFname ASC";
if($assignType == "users"){
    ?>
    <tr>
    <td width="120px">Select Level</td>
    <td>
    <select id="levelId" name="levelId" onchange="getlevelusers(this.value);">
    <?
    echo '<option value=""> --- select ---</option>';
    $userSql = "select * from cui_users_levels where levelId != 1 and levelStatus = 1 order by levelTitle";
//    $userResult = mysql_query($userSql);
    $userResult = mysqli_query($con, $userSql);
//    while ($userRs = mysql_fetch_array($userResult)) {
    while ($userRs = @mysqli_fetch_array($userResult)) {
        $levelId = $userRs["levelId"];
        $levelTitle = $userRs["levelTitle"];
        echo '<option value="'.$levelId.'">'.$levelTitle.'</option>';
    }
    ?>
    </select>
    </td>
</tr>
    <tr id="levelusershowhide" style="display:none">
    <td width="120px">Select User</td>
    <td id="add_level_users">
   		
    </td>
    <td>
    <div id="userlevel" style="display:none;">
    <table cellpadding="3" cellspacing="0" align="center" width="100%">
    <tr>
    <td align="left" style="padding-left:20px;">User Level</td>
    <td>
    <div id="level" style="font-weight:bold;"></div>
    </td>
    </tr>
    </table>
    </div>
    </td>
    </tr>
    <?}else{?>
    <tr>
    <td width="120px">Select Company</td>
    <td colspan="2">
    <select id="companyId" name="companyId" onchange="return officeOptions(this.value)">
    <?
    echo '<option value="">--- select ---</option>';
    $companySql = "select * from cui_companies where companyStatus='1' order by companyName ASC";
//    $companyResult = mysql_query($companySql);
    $companyResult = mysqli_query($con, $companySql);
//    while ($companyRs = mysql_fetch_array($companyResult)) {
    while ($companyRs = @mysqli_fetch_array($companyResult)) {
        $rsCompanyId = $companyRs["companyId"];
        $companyName = $companyRs["companyName"];
        echo '<option value="'.$rsCompanyId.'">'.$companyName.'</option>';
    }
    ?>
    </select>
    </td>
    </tr>
<?}?>
<tr>
<td>Select Office</td>
<td colspan="2">
<select id="officeId" name="officeId">
<option value="">--- select ---</option>
<? if($assignType != "companies"){
    echo '<option value="All">- All Offices -</option>';
    $editSql = "SELECT * from cui_companies_offices where companyId='$companyId'";
//    $editResult = mysql_query($editSql);
    $editResult = mysqli_query($con, $editSql);
//    while ($editRs = mysql_fetch_array($editResult)) {
    while ($editRs = @mysqli_fetch_array($editResult)) {
        $rsofficeId = $editRs["officeId"];
        $officeName = $editRs["officeName"];
        echo '<option value="'.$rsofficeId.'">'.$officeName.'</option>';
    }
}?>
</select>
</td>
</tr>
<tr>
<td>&nbsp;</td>
<td colspan="2"><input type="submit" value="Assign" class="btn searchbt" onclick="return chkForm()" />&nbsp;<input type="button" class="btn clearbt" value="Done" onclick="javascript:window.location='<?=HTTP_SERVER?>index.php?do=<?if($assignType=="companies"){?>users&userType=<?=$userType?><?}else{?>companies<?}?>'" /></td>
</tr>
</table>
</form>
</td>
</tr>
<table class="table table-bordered">
<thead>
<tr class="">
<td colspan="7"><h3 style="color:black">Assigned</h3></td>
</tr>
<tr class="" align="center">
	<td width="5%" align="center"><input type="checkbox" name="checkedAll" id="checkedAll" /></td>
    <td width="15%"><a
            href="<?= HTTP_SERVER ?>index.php?do=<?= $do ?><?= $queryString ?>&sortBy=userFname&sortOrder=<? if ($sortBy == "userFname") {
                if ($sortOrder == "ASC") {
                    echo "DESC";
                } else {
                    echo "ASC";
                }
            } else {
                echo "ASC";
            } ?>">User Name<?php if ($sortBy == "userFname") { ?>&nbsp;<img
                src="<?= HTTP_SERVER ?>images/sort_<?= strtolower($sortOrder) ?>.png" border="0"
                alt="<?= $sortOrder ?> Order" /><? } ?></a></td>
    <td width="15%"><a
            href="<?= HTTP_SERVER ?>index.php?do=<?= $do ?><?= $queryString ?>&sortBy=levelTitle&sortOrder=<? if ($sortBy == "levelTitle") {
                if ($sortOrder == "ASC") {
                    echo "DESC";
                } else {
                    echo "ASC";
                }
            } else {
                echo "ASC";
            } ?>">Level / Role<?php if ($sortBy == "levelTitle") { ?>&nbsp;<img
                src="<?= HTTP_SERVER ?>images/sort_<?= strtolower($sortOrder) ?>.png" border="0"
                alt="<?= $sortOrder ?> Order" /><? } ?></a></td>
    <td width="10%"><a
            href="<?= HTTP_SERVER ?>index.php?do=<?= $do ?><?= $queryString ?>&sortBy=companyCode&sortOrder=<? if ($sortBy == "companyCode") {
                if ($sortOrder == "ASC") {
                    echo "DESC";
                } else {
                    echo "ASC";
                }
            } else {
                echo "ASC";
            } ?>">Company Id<?php if ($sortBy == "companyCode") { ?>&nbsp;<img
                src="<?= HTTP_SERVER ?>images/sort_<?= strtolower($sortOrder) ?>.png" border="0"
                alt="<?= $sortOrder ?> Order" /><? } ?></a></td>
    <td width="25%"><a
            href="<?= HTTP_SERVER ?>index.php?do=<?= $do ?><?= $queryString ?>&sortBy=companyName&sortOrder=<? if ($sortBy == "companyName") {
                if ($sortOrder == "ASC") {
                    echo "DESC";
                } else {
                    echo "ASC";
                }
            } else {
                echo "ASC";
            } ?>">Company Name<?php if ($sortBy == "companyName") { ?>&nbsp;<img
                src="<?= HTTP_SERVER ?>images/sort_<?= strtolower($sortOrder) ?>.png" border="0"
                alt="<?= $sortOrder ?> Order" /><? } ?></a></td>
    <td width="20%"><a
            href="<?= HTTP_SERVER ?>index.php?do=<?= $do ?><?= $queryString ?>&sortBy=officeId&sortOrder=<? if ($sortBy == "officeId") {
                if ($sortOrder == "ASC") {
                    echo "DESC";
                } else {
                    echo "ASC";
                }
            } else {
                echo "ASC";
            } ?>">Office Name<?php if ($sortBy == "officeId") { ?>&nbsp;<img
                src="<?= HTTP_SERVER ?>images/sort_<?= strtolower($sortOrder) ?>.png" border="0"
                alt="<?= $sortOrder ?> Order" /><? } ?></a></td>
<td width="5%">Actions</td>
</tr>
</thead>
<?
//pagination code
if((!isset($_GET['page'])) && (!isset($_POST['page']))){
    $page = 1;
    } else {
    if (!$_GET['page']) {
        $page = $_POST['page'];
        } else {
        $page = $_GET['page'];
    }
}
//if get filters
if($_GET["filter"]){
    $filter = sanitize($_GET["filter"]);
    if($filter!=""){
        $filterText = sanitize($_GET["filterText"]);
        //more conditions
        if($filter == "clientStatus"){
            if($filterText == strtolower("blocked")){
                $filterText=0;
                }else{
                $filterText=1;
            }
            $sqlVars .= " and $filter =".$filterText;
            }else{
            $sqlVars .= " and $filter like '%".$filterText."%'";
        }
        $queryString .= "&filter=".$filter."&filterText=".$filterText;
    }
}

//based on assignType
if($sortBy!=""){
    $sqlVars .= " order by {$sortBy} {$sortOrder}";
} else {
    if($assignType == "users"){
        $sqlVars .= " order by companyName ASC";
    }else{
        $sqlVars .= " order by userFname ASC";
    }
}
$itemCountSql="select a.*,c.userFname,c.userLname,b.companyName from cui_users_companies as a join cui_companies as b join cui_users as c JOIN cui_users_levels AS ul ON ul.levelId = c.userLevel LEFT JOIN cui_companies_offices AS co ON co.officeId = a.officeId where a.userId=c.userId and b.companyId=a.companyId $sqlVars";
$itemCountResult = mysqli_query($con, $itemCountSql);
$itemCount = @mysqli_num_rows($itemCountResult);
if($itemCount>0){
    //pagination vars
    $limit = 15;
    $PaginateIt = new pagination();
    $PaginateIt->SetItemsPerPage($limit);
    $PaginateIt ->SetLinksToDisplay(5);
    $PaginateIt->SetItemCount($itemCount);
    $PaginateIt->SetLinksFormat( '<< Back', ' ', 'Next >>' );
    if($_GET["page"]){
        $pageVar = ($limit*$_GET["page"]) - $limit;
        }else{
        $pageVar = 0;
    }
    $productPageLinks = $PaginateIt->GetPageLinks();
    $counter=0;
    $sqlCompanies="select a.*,c.userFname,c.userLname,b.companyName,b.companyCode from cui_users_companies as a join cui_companies as b join cui_users as c JOIN cui_users_levels AS ul ON ul.levelId = c.userLevel LEFT JOIN cui_companies_offices AS co ON co.officeId = a.officeId where a.userId=c.userId and b.companyId=a.companyId $sqlVars ".$PaginateIt->GetSqlLimit();
//    $sqlCompaniesResult=mysql_query($sqlCompanies);
    $sqlCompaniesResult=mysqli_query($con, $sqlCompanies);
//    while ($sqlCompaniesRs=mysql_fetch_array($sqlCompaniesResult)) {
    while ($sqlCompaniesRs=@mysqli_fetch_array($sqlCompaniesResult)) {
        $counter++;
        $assignId = $sqlCompaniesRs["assignId"];
        $companyId = $sqlCompaniesRs["companyId"];
        $companyName = $sqlCompaniesRs["companyName"];
        $companyCode = $sqlCompaniesRs["companyCode"];
        $officeId = $sqlCompaniesRs["officeId"];
        $officeAll = $sqlCompaniesRs["officeAll"];
        if($officeId != 0 and $officeAll!=1){
            $officeName = getField("cui_companies_offices","officeId",$officeId,"officeName");
            }else{
            $officeName = "All Offices";
        }
        $userId = $sqlCompaniesRs["userId"];
        $userLevelId = getField("cui_users", "userId", $userId, "userLevel");
        $userLevel = getField("cui_users_levels", "levelId", $userLevelId, "levelTitle");
        $userFname = $sqlCompaniesRs["userFname"];
        $userLname = $sqlCompaniesRs["userLname"];
        if($counter>1){
            $bgcolor="lightgrey";
            $counter=0;
            }elsE{
            $bgcolor="#FFFFFF";
        }
		$bg_color = $counter % 2 === 0 ? 'class="alternatebg"' : '';
        ?>
        <tr <?=$bg_color;?>>
        <td align="center"><input type="checkbox" name="checked[]" value="<?=$assignId;?>" class="checkSingle" /></td>
        <td><?=$userFname?> <?=$userLname?></td>
        <td><?=$userLevel?></td>
        <td><?=$companyCode?></td>
        <td><?=$companyName?></td>
        <td><?=$officeName?></td>
        <td align="center">
        <table cellpadding="0" cellspacing="2" align="center">
        <tr>
        <td>
		<!--<button title="Delete" type="button" class="btn btn-default btn-sm trash" onclick="javascript:if(confirm('Are you sure you wish to unassign this company?')){window.location='index.php?do=<?=$do?>&assignId=<?=$assignId?>&act=delete<?=$queryString?>';}">-->
		<button title="Delete" type="button" class="btn btn-default btn-sm trash" onclick="javascript:swal({title: 'Confirmation!',text: 'Proceed with deletion?',type: 'warning',showCancelButton: true,confirmButtonText: 'OK',closeOnConfirm: false},function(){swal.close();window.location='index.php?do=<?=$do?>&assignId=<?=$assignId?>&act=delete<?=$queryString?>';})">
          <span class="glyphicon glyphicon-trash"></span> 
        </button>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        <?
    }
	?>
		<tr>
			<td align="cetner">
				<form action="index.php?do=users_companies&assignType=users&companyId=<?=$companyId;?>&act=DeleteAll" method="POST">
					<input type="hidden" name="checkid" id="checkid" />
					<button type="submit" id="btnsubmit" name="btnsubmit" style="display:none;"></button>
					<button title="Delete" type="button" class="btn btn-default btn-sm trash" onclick="javascript:swal({title: 'Confirmation!',text: 'Proceed with deletion?',type: 'warning',showCancelButton: true,confirmButtonText: 'OK',closeOnConfirm: false},function(){swal.close();$('#btnsubmit').click();})">
						<span class="glyphicon glyphicon-trash"></span> 
					</button>
				</form>
			</td>
			<td colspan="6" align="right">
				&nbsp;
			</td>
		</tr>
	<?
    //if pagination is needed
    if($itemCount > $limit){
        ?>
        <tr>
        <td colspan="7" align="right"><?echo $productPageLinks;?></td>
        </tr>
        <?
    }
    }else{
    echo "<tr>
    <td colspan=\"7\" align=\"center\" height=\"100px\">No Results Found !</td>
    </tr>";
}
?>
</table>
</table>
</div>
<script>
$(document).ready(function() {
	var checkvalues = [];
    $("#checkedAll").change(function() {
        if (this.checked) {
            $(".checkSingle").each(function() {
                this.checked=true;
				checkvalues.push(this.value);
            });
			$("#checkid").val(checkvalues);
        } else {
            $(".checkSingle").each(function() {
                this.checked=false;
				var that = this.value;
				checkvalues = jQuery.grep(checkvalues, function(value) {
					return value != that;
				});
            });
			$("#checkid").val(checkvalues);
        }
    });

    $(".checkSingle").click(function () {
        if ($(this).is(":checked")) {
			checkvalues.push(this.value);
            var isAllChecked = 0;

            $(".checkSingle").each(function() {
                if (!this.checked)
                    isAllChecked = 1;
            });

            if (isAllChecked == 0) {
                $("#checkedAll").prop("checked", true);
            }
			$("#checkid").val(checkvalues);			
        }
        else {
			var that = this.value;
			checkvalues = jQuery.grep(checkvalues, function(value) {
				return value != that;
			});
            $("#checkedAll").prop("checked", false);
			$("#checkid").val(checkvalues);
        }
    });
});
</script>