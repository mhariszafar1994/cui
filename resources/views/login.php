<?php
//variable initialization
$loginError="";
//if get expire
if($_GET["expire"]){
    $loginError = "Your session has expired due to inactivity!";
}
$createSessions = 0;
//posting form
if($_POST){
    $loginUser=sanitize($_POST["loginUser"]);
    $loginPassword=md5(sanitize($_POST["loginPassword"]));
    if($loginUser!="" and $loginPassword!=""){
        $loginSql="select * from cui_users where userLogin='$loginUser' and userPassword='$loginPassword' and userStatus='1'";
        //		$loginSql="select * from cui_users where userLogin='$loginUser' and userStatus='1'";
//        $loginResult=mysql_query($loginSql);
        $loginResult=mysqli_query($con, $loginSql);
//        if(mysql_num_rows($loginResult)>0){
        if(@mysqli_num_rows($loginResult)>0){
//            $userId=mysql_result($loginResult,0,"userId");
            $userId=mysqli_result($loginResult,0,"userId");
//            $userLevel=mysql_result($loginResult,0,"userLevel");
            $userLevel=mysqli_result($loginResult,0,"userLevel");
//            $userStatus=mysql_result($loginResult,0,"userStatus");
            $userStatus=mysqli_result($loginResult,0,"userStatus");
//            $userFname=mysql_result($loginResult,0,"userFname");
            $userFname=mysqli_result($loginResult,0,"userFname");
//			$userLname=mysql_result($loginResult,0,"userLname");
			$userLname=mysqli_result($loginResult,0,"userLname");
//            $userDefault=mysql_result($loginResult,0,"userDefault");
            $userDefault=mysqli_result($loginResult,0,"userDefault");
//            $userDefaultOfficeId=mysql_result($loginResult,0,"userDefaultOfficeId");
            $userDefaultOfficeId=mysqli_result($loginResult,0,"userDefaultOfficeId");
//            $userDefaultCompanyId=mysql_result($loginResult,0,"userDefaultCompanyId");
            $userDefaultCompanyId=mysqli_result($loginResult,0,"userDefaultCompanyId");
            if($userDefault==1){
                $_SESSION["cuiSessionOfficeId"] = $userDefaultOfficeId;
                $_SESSION["cuiSessionCompanyId"] = $userDefaultCompanyId;
            }

			//added by Noman - May-25-2015
			//creating session for custom form and that page tag - start
			if($userDefaultCompanyId!="" && $userDefaultCompanyId>0)
			{
				$tmpCustCompName = getField("cui_companies","companyId",$userDefaultCompanyId,"companyCompanyName"," and companyCustom='1'");
				$tmpCustCompTag = getField("cui_custom_companies","custCompanyName",$tmpCustCompName,"custCompanyTag"," and custCompanyStatus='1'");
				$_SESSION["cuiSessionCompanyTag"] = $tmpCustCompTag;
			}
			//creating session for custom form and that page tag - end
			
            if($userStatus!="1"){
                $loginError="Your account has been disabled !";
                }else{
                /*
                //now if user is admin
                if($userLevel == 1){
                    $createSessions = 1;
                    }else{
                    //otherwise check company
                    $sqlCompany = "SELECT * FROM cui_users_companies as a join cui_companies as b WHERE a.companyId=b.companyId and b.companyCode='$loginCompany' and a.userId=$userId";
                    $resultCompany = mysql_query($sqlCompany);
                    if(mysql_num_rows($resultCompany)>0){
                        $companyId = mysql_result($resultCompany,0,"companyId");
                        //create company session
                        $_SESSION["cuiSessionCompanyId"] = $companyId;
                        //allow other sessions
                        $createSessions = 1;
                        }else{
                        $loginError="You do not have access to this company!";
                    }
                }
                */
                $createSessions = 1;
                //if all ok
                if($createSessions == 1)
				{
                    //added by Noman - May-25-2015
					//Access login report for users - start
					$tmpUserName = $userFname." ".$userLname;
					$tmpIPAddress = $_SERVER['REMOTE_ADDR'];
					$tmpDateTime = date("Y-m-d H:i:s");
					$sqlLoginInfo = "Insert into cui_users_login_report(lrUserId,lrUserLogin,lrUserName,lrIPAddress,lrLoginDateTime) 
									values('$userId','$loginUser','$tmpUserName','$tmpIPAddress','$tmpDateTime')";
					//if CUI Employee then insert
					if($userLevel == '34' || $userLevel == '37')
					{
//						mysql_query($sqlLoginInfo);
						mysqli_query($con, $sqlLoginInfo);
					}
					//Access login report for users - end
					
					//declaring sessions
                    $_SESSION["cuiSessionUserFname"]=$userFname;
                    $_SESSION["cuiSessionUserLevel"]=$userLevel;
                    $_SESSION[$sessionUserID]=$userId;
                    // die(print_r($_SESSION));

                    echo "<script>window.location='index.php?do=home'</script>";
                }
            }
            }
        else{
            $loginError="Incorrect Login Information, Please try again";
        }
        }else{
        $loginError="Email / Password should not be blank";
    }
}
?>
<script type="text/javascript">
function chkForm(){
    var a = document.getElementById("loginUser").value;
    var b = document.getElementById("loginPassword").value;
    if(a=="" || b==""){
       $('.error').show();
       $('.error').html('Please enter your User Name and Password!');
       return false;
    }
}
</script>
<!--<h1 class="h1WithBg">Login Screen</h1>-->
           
          <div class="row lightblue">
			<div class="container">
				<div class="loginbox col-md-6 col-md-offset-3 col-xs-offset-2">
					<div class="customer_login">
						<h4 class="customer_login_heading">CUI Login</h4>
					</div>
					<div class="linestyles">
						<p>
						Exsiting members may login below using their login ID and password. 
						New members please contact CUI to get access at the
						applicaiton.
						</p>
					</div>
					<div class="loginbox_fields">
					<form method="POST" action="index.php" class="form">
						<? if (isset($loginError) && $loginError != '') { ?>
						<div class="error" style="margin-bottom:15px;"><?=$loginError?></div>
						<? } ?>
						<div class="form-group">
							<div class="icon-addon addon-md">
								<input type="text" placeholder="Enter your user name here" class="form-control" id="loginUser" name="loginUser">
								<label for="loginUser" class="glyphicon glyphicon-user" rel="tooltip" title="Enter User Name"></label>
							</div>
						</div>
			
						<div class="form-group">
							<div class="icon-addon addon-md">
							    <input type="hidden" name="csrf_token" value="<?php echo hash_hmac('sha256', '/my_form.php', $_SESSION['second_token']);?>" />
								<input type="password" placeholder="Enter your password here" class="form-control" id="loginPassword" name="loginPassword">
								<label for="email" class="glyphicon glyphicon-lock" rel="tooltip" title="Enter Password"></label>
							</div>
						</div>
						
						
			
						<div class="form-group">
							<button type="submit" class="btn sign-in_btn" onclick="chkForm()">Sign In</button>
							<div class="pull-right forgotpassword">
								<ul style="list-style: none; padding">
									<li style="display: inline;"><a href="<?php echo HTTP_SERVER;?>index.php?do=forgot_password">Forgot password?</a></li>
									<li style="display: inline; color: black">|</li>
									<li style="display: inline;"><a href="javascript:window.location='<?php echo str_replace('app/', '', HTTP_SERVER);?>contact-us/'">New User?</a></li>
								</ul>
							</div>
							
						</div>
					</form> 
					</div>
				</div>
			</div>
			</div>
            
            <!--Login Panel [Start]-->
            <!--<aside class="loginPnl brd-8 floatfix">
                <img src="images/ico-big-employee.png" alt="" class="circleIcon">
                <div class="lgHolder brd-8">
                    <form method="POST" action="index.php" class="form">
                        <h1>Employee Login</h1>
                        <div class="w280 mag-10 fl">
                            User Name <span class="asteric">*</span><br>
                            <input id="loginUser" name="loginUser" type="text" placeholder="Enter your user name">
                        </div>
                        
                        <div class="w280 mag-10 fr">
                            Password <span class="asteric">*</span><br>
                            <input id="loginPassword" name="loginPassword" type="password" placeholder="Enter password">
                        </div>
                        <div class="clear-all"></div>
                        <a href="#" class="lnk-fgpass fl">Forgot Password?</a>
                        <input type="submit" class="btn-silver2 btn-login fr" value="Login &#9658;" onclick="chkForm()">
                    </form>
                </div>
            </aside>-->
            <!--Login Panel [END]-->
            
            <!--New User SignUp -->
            <!--<aside class="nwSignup brd-5 floatfix">
                <h2 class="fl">New User? <img src="images/bigArrow.png" alt="" class="fr"></h2>
                <button type="button" class="fr" onclick="javascript:window.location='<?php echo str_replace('app/', '', HTTP_SERVER);?>contact-us/'">Click here to activate your account.</button>
            </aside>-->