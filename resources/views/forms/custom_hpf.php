<div id="divCustom">

<table cellpadding="3" cellspacing="0" border="0">
<tr>
<td width="130"><b>Network Type</b></td>
<td>
<select id="custNetwork" name="custNetwork" size="1">
    <option value=""></option>
    <option value="In Network" <? if($custNetwork=="In Network"){ ?>Selected<? } ?>>In Network</option>
    <option value="Out Of Network" <? if($custNetwork=="Out Of Network"){ ?>Selected<? } ?>>Out Of Network</option>
    <option value="Policy Terminated" <? if($custNetwork=="Policy Terminated"){ ?>Selected<? } ?>>Policy Terminated</option>
</select></td>
<td></td>
<td>
    <table border="0" cellpadding="2" cellspacing="0">
    <tr>
        <td nowrap="nowrap"><b>Payer&nbsp;ID</b></td>
        <td>&nbsp;</td>
        <td><input type="text" class="textbox" id="custPayorId" name="custPayorId" value="<?=$custPayorId?>" style="width: 100px" /></td>
        <td width="50px">&nbsp;</td>
        <td nowrap="nowrap"><b>E-Attachments</b></td>
        <td width="30">&nbsp;</td>
        <td><input <? if($custAttachments == "Yes"){?>checked<? } ?> type="radio" name="custAttachments" value="Yes" /></td>
        <td>Yes</td>
        <td width="20px">&nbsp;</td>
        <td><input <? if($custAttachments == "No"){?>checked<? } ?> type="radio" name="custAttachments" value="No" /></td>
        <td>No</td>
    </tr>
    </table></td>
</tr>

<tr class="alternate">
<td width="130"><b>Quick Note</b></td>
<td colspan="6"><input type="text" class="textbox" id="custQuickNote" name="custQuickNote" value="<?=$custQuickNote?>" style="width: 610px" maxlength="155" /></td>
<td></td>
</tr>

<tr>
<td width="130"><b>Effective Date</b></td>
<td colspan="2"><input type="text" class="textbox" id="custEffectiveDate" name="custEffectiveDate" value="<?=$custEffectiveDate?>" style="width: 60px" maxlength="10" /><input type="button" value="Select" class="smallButton" onclick="displayDatePicker('custEffectiveDate');" /></td>
<td colspan="5">
<table cellpadding="2" cellspacing="0">
  <tr>
  <td><b>Fee Schedule</b></td>
  <td width="30">&nbsp;</td>
  <td valign="top" width="20px"><input <? if($custFee == "PPO"){?>checked<? } ?> type="radio" name="custFee" value="PPO" /></td>
  <td valign="top">PPO</td>
  <td width="15px">&nbsp;</td>
  <td valign="top" width="20px"><input <? if($custFee == "UCR"){?>checked<? } ?> type="radio" name="custFee" value="UCR" /></td>
  <td valign="top">UCR</td>
  <td valign="top">&nbsp;</td>
  <td valign="top"><input type="text" class="textbox" id="custFeeName" name="custFeeName" value="<?=$custFeeName?>" /></td>
  </tr>
</table></td>
</tr>

<tr>
<td width="130"><b>ID</b></td>
<td><input type="text" class="textbox" id="custMemberId" name="custMemberId" value="<?=$custMemberId?>" style="width: 100px" /></td>
<td width="5"></td>
<td width="130">

<table cellpadding="2" cellspacing="0">
  <tr>
  <td nowrap="nowrap"><b>Group Name</b></td>
  <td width="30">&nbsp;</td>
  <td valign="top" width="20px"><input type="text" class="textbox" id="custGroup" name="custGroup" value="<?=$custGroup?>" style="width: 100px" /></td>
  </tr>
</table></td>
<td width="50"></td>
<td></td>
</tr>
<tr class="alternate">
<td><b>Benefits Run ?</b></td>
<td colspan="7">
<table cellpadding="1" cellspacing="0">
<tr>
<td width="20px"><input <? if($custBenefitsRun == "Calender"){?>checked<? } ?> type="radio" name="custBenefitsRun" value="Calender" /></td>
<td>Calendar</td>
<td width="15px">&nbsp;</td>
<td width="20px"><input <? if($custBenefitsRun == "Contract Year"){?>checked<? } ?> type="radio" name="custBenefitsRun" value="Contract Year" /></td>
<td>Contract Year</td>
<td width="5px">&nbsp;</td>
<td>&nbsp;</td>
<td>
<input type="text" class="textbox" id="custBenefitsDate" name="custBenefitsDate" value="<?=$custBenefitsDate?>" style="width: 60px" maxlength="10" /><input type="button" value="Select" class="smallButton" onclick="displayDatePicker('custBenefitsDate');" /></td>
</tr>
</table></td>
</tr>
<tr>
<td valign="top"><b>MTC</b></td>
<td colspan="6">
<table cellpadding="1" cellspacing="0">
<tr>
<td><input <? if($custMtc == "Yes"){?>checked<? } ?> type="radio" name="custMtc" value="Yes" /></td>
<td>Yes</td>
<td width="20px">&nbsp;</td>
<td><input <? if($custMtc == "No"){?>checked<? } ?> type="radio" name="custMtc" value="No" /></td>
<td>No</td>
<Td width="20px">&nbsp;</Td>
<td><b>Details</b></td>
<td width="15px">&nbsp;</td>
<Td><input type="text" class="textbox" id="custMtcDetail" name="custMtcDetail" value="<?=$custMtcDetail?>" style="width: 180px" /></Td>
</tr>
</table>
</td>
<td>&nbsp;</td>
</tr>
<tr class="alternate">
<td nowrap="nowrap"><b>Waiting Period</b></td>
<td colspan="7">
<table cellpadding="1" cellspacing="0">
<tr>
<td><input <? if($custWaitingPeriod == "Yes"){?>checked<? } ?> type="radio" name="custWaitingPeriod" value="Yes" /></td>
<td>Yes</td>
<td width="20px">&nbsp;</td>
<td><input <? if($custWaitingPeriod == "No"){?>checked<? } ?> type="radio" name="custWaitingPeriod" value="No" /></td>
<td>No</td>
<Td width="20px">&nbsp;</Td>
<td><b>Details</b></td>
<td width="15px">&nbsp;</td>
<Td><input type="text" class="textbox" id="custWaitingPeriodDetail" name="custWaitingPeriodDetail" value="<?=$custWaitingPeriodDetail?>" style="width: 180px" /></Td>
</tr>
</table></td>
</tr>
<tr>
  <td colspan="8">
  	<table cellpadding="2" cellspacing="0">
      <tr>
      <td width="132px" style="padding-left:0px;"><b>Yearly Max $</b></td>
      <td><input type="text" class="textbox" id="custYearlyMax" name="custYearlyMax" value="<?=$custYearlyMax?>" style="width: 60px" /></td>
      <td width="10px">&nbsp;</td>
      <td width="80px"><b>Applies to?</b></td>
      <td><input <? if($custYearlyApplies1 == "P"){?>checked<? } ?> type="checkbox" name="custYearlyApplies1" value="P" /></td>
      <td>P</td>
      <td width="2px"></td>
      <td><input <? if($custYearlyApplies2 == "B"){?>checked<? } ?> type="checkbox" name="custYearlyApplies2" value="B" /></td>
      <td>B</td>
      <td width="2px"></td>
      <td><input <? if($custYearlyApplies3 == "M"){?>checked<? } ?> type="checkbox" name="custYearlyApplies3" value="M" /></td>
      <td>M</td>
      <td width="20px">&nbsp;</td>
      <td width="50px"><b>Used $</b></td>
      <td><input type="text" class="textbox" id="custUsed" name="custUsed" value="<?=$custUsed?>" style="width: 60px" /></td>
      <td width="10px">&nbsp;</td>
      <td width="90px"><b>Roll over? $</b></td>
      <td><input type="text" class="textbox" id="custRollOver" name="custRollOver" value="<?=$custRollOver?>" style="width: 60px" /></td>
      </tr>
    </table>  </td>
  </tr>
<tr class="alternate">
  <td colspan="8">
  	<table cellpadding="2" cellspacing="0">
      <tr>
      <td width="132px" style="padding-left:0px;"><b>Ind. Deductible $</b></td>
      <td><input type="text" class="textbox" id="custDeduct" name="custDeduct" value="<?=$custDeduct?>" style="width: 60px" /></td>
      <td width="10px">&nbsp;</td>
      <td width="80px"><b>Applies to?</b></td>
      <td><input <? if($custDeductApplies1 == "P"){?>checked<? } ?> type="checkbox" name="custDeductApplies1" value="P" /></td>
      <td>P</td>
      <td width="2px"></td>
      <td><input <? if($custDeductApplies2 == "D"){?>checked<? } ?> type="checkbox" name="custDeductApplies2" value="D" /></td>
      <td>D</td>
      <td width="2px"></td>
      <td><input <? if($custDeductApplies3 == "B"){?>checked<? } ?> type="checkbox" name="custDeductApplies3" value="B" /></td>
      <td>B</td>
      <td width="2px"></td>
      <td><input <? if($custDeductApplies4 == "M"){?>checked<? } ?> type="checkbox" name="custDeductApplies4" value="M" /></td>
      <td>M</td>
      <td width="20px">&nbsp;</td>
      <td width="40px"><b>Met</b></td>
      <td><input <? if($custMet == "Yes"){?>checked<? } ?> type="radio" name="custMet" value="Yes" /></td>
      <td>Yes</td>
      <td width="5px"></td>
      <td><input <? if($custMet == "No"){?>checked<? } ?> type="radio" name="custMet" value="No" /></td>
      <td>No</td>
      </tr>
    </table>  </td>
</tr>
<tr>
  <td colspan="8">
  	<table cellpadding="2" cellspacing="0">
      <tr>
      <td width="132px" style="padding-left:0px;"><b>Fam. Deductible $</b></td>
      <td><input type="text" class="textbox" id="custFamilyDeduct" name="custFamilyDeduct" value="<?=$custFamilyDeduct?>" style="width: 60px" /></td>
      <td width="10px">&nbsp;</td>
      <td width="40px"><b>Met</b></td>
      <td><input <? if($custMet2 == "Yes"){?>checked<? } ?> type="radio" name="custMet2" value="Yes" /></td>
      <td>Yes</td>
      <td width="5px"></td>
      <td><input <? if($custMet2 == "No"){?>checked<? } ?> type="radio" name="custMet2" value="No" /></td>
      <td>No</td>
      </tr>
    </table>  </td>
</tr>
<tr class="alternate">
  <td colspan="8">
  	<table cellpadding="1" cellspacing="0" border="0">
    <tr>
    <td nowrap="nowrap"><b>Basic</b></td>
    <td width="5px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custBasic" name="custBasic" value="<?=$custBasic?>" style="width: 60px" />%</td>
    <td width="15px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>Major</b></td>
    <td width="5px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custMajor" name="custMajor" value="<?=$custMajor?>" style="width: 60px" />%</td>
    <td width="15px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>Perio</b></td>
    <td width="5px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custPerio" name="custPerio" value="<?=$custPerio?>" style="width: 60px" />%</td>
    <td width="15px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>Endo</b></td>
    <td width="5px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custEndo" name="custEndo" value="<?=$custEndo?>" style="width: 60px" />%</td>
    <td width="15px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>OS</b></td>
    <td width="5px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custOs" name="custOs" value="<?=$custOs?>" style="width: 60px" />%</td>
    
    <td width="15px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>OS to Med</b></td>
    <td width="5px">&nbsp;</td>
    <td><input <? if($custOsToMed == "Yes"){?>checked<? } ?> type="radio" name="custOsToMed" value="Yes" /></td>
    <td>Yes</td>
    <td width="5px"></td>
    <td><input <? if($custOsToMed == "No"){?>checked<? } ?> type="radio" name="custOsToMed" value="No" /></td>
    <td>No</td>
    </tr>
    </table>  </td>
</tr>


</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">Diagnostic & Preventative</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
<tr>
    <td width="130" nowrap="nowrap"><b>Complete Exam (0150)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPCompleteExam" name="custDPCompleteExam" value="<?=$custDPCompleteExam?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPCompleteExamFreq" name="custDPCompleteExamFreq" value="<?=$custDPCompleteExamFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td colspan="4">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="130" nowrap="nowrap"><b>Periodic Exam (0120)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custDPPeriodicExam" name="custDPPeriodicExam" value="<?=$custDPPeriodicExam?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>Freq</b></td>
            <td>
            <input type="text" class="textbox" id="custDPPeriodicExamFreq" name="custDPPeriodicExamFreq" value="<?=$custDPPeriodicExamFreq?>" style="width: 75px" />            </td>
        </tr>
        </table>
    </td>
    </tr>
<tr>
    <td nowrap="nowrap"><b>Limited Exam (0140)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPLimitedExam" name="custDPLimitedExam" value="<?=$custDPLimitedExam?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPLimitedExamFreq" name="custDPLimitedExamFreq" value="<?=$custDPLimitedExamFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td colspan="3">
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td nowrap="nowrap"><b>Separate from 0120?</b></td>
        <td><input <? if($custDPSeparate == "Yes"){?>checked<? } ?> type="radio" name="custDPSeparate" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custDPSeparate == "No"){?>checked<? } ?> type="radio" name="custDPSeparate" value="No" /></td>
        <td>No</td>
        <td>&nbsp;</td>
        <td nowrap="nowrap"><b>Allowed same day w/ TX?</b></td>
        <td><input <? if($custDPAllowed == "Yes"){?>checked<? } ?> type="radio" name="custDPAllowed" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custDPAllowed == "No"){?>checked<? } ?> type="radio" name="custDPAllowed" value="No" /></td>
        <td>No</td>
        </tr>
        </table>    </td>
    <td>&nbsp;</td>
</tr>

<tr>
    <td nowrap="nowrap"><b>FMX(0210)/Pano(0330)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPFMX" name="custDPFMX" value="<?=$custDPFMX?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPFMXFreq" name="custDPFMXFreq" value="<?=$custDPFMXFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td><input <? if($custDPShare == "Share"){?>checked<? } ?> type="radio" name="custDPShare" value="Share" /></td>
        <td nowrap="nowrap"><b>Share</b></td>
        <td><input <? if($custDPShare == "Separate"){?>checked<? } ?> type="radio" name="custDPShare" value="Separate" /></td>
        <td><b>Separate</b></td>
        <td width="30">&nbsp;</td>
        <td nowrap="nowrap"><b>Does FMX take freq of BW</b></td>
        <td width="5"></td>
        <td><input type="text" class="textbox" id="custDPFreqBw" name="custDPFreqBw" value="<?=$custDPFreqBw?>" style="width: 100px" /></td>
        </tr>
        </table>    </td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Bitewings (0270-74)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPBit" name="custDPBit" value="<?=$custDPBit?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPBitFreq" name="custDPBitFreq" value="<?=$custDPBitFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td>
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="130" nowrap="nowrap"><b>Periapicals (0220-30)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custDPPeriap" name="custDPPeriap" value="<?=$custDPPeriap?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custDPPeriapFreq" name="custDPPeriapFreq" value="<?=$custDPPeriapFreq?>" style="width: 75px" /></td>
            <td width="20px">&nbsp;</td>
            <td><b>Max?</b></td>
            <td><input type="text" class="textbox" id="custDPMax" name="custDPMax" value="<?=$custDPMax?>" style="width: 75px" /></td>
        </tr>
        </table>    </td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>

<tr>
    <td nowrap="nowrap"><b>Prophy (1110-20)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPProphy" name="custDPProphy" value="<?=$custDPProphy?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPProphyFreq" name="custDPProphyFreq" value="<?=$custDPProphyFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td>
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="130" nowrap="nowrap"><b>Fluoride (1208)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custDPFluoride" name="custDPFluoride" value="<?=$custDPFluoride?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custDPFluorideFreq" name="custDPFluorideFreq" value="<?=$custDPFluorideFreq?>" style="width: 75px" /></td>
            <td width="20px">&nbsp;</td>
            <td nowrap="nowrap"><b>To Age?</b></td>
            <td><input type="text" class="textbox" id="custDPToAge" name="custDPToAge" value="<?=$custDPToAge?>" style="width: 75px" /></td>
        </tr>
        </table>
    </td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Sealants (1351)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPSealants" name="custDPSealants" value="<?=$custDPSealants?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPSealantsFreq" name="custDPSealantsFreq" value="<?=$custDPSealantsFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td nowrap="nowrap"><b>To Age?</b></td>
        <td><input type="text" class="textbox" id="custDPSealantsToAge" name="custDPSealantsToAge" value="<?=$custDPSealantsToAge?>" style="width: 30px" /></td>
        <td width="5"></td>
        <td><input <? if($custDPMolarsOnly == "Perm. Molars Only"){?>checked<? } ?> type="checkbox" name="custDPMolarsOnly" value="Perm. Molars Only" /></td>
        <td nowrap="nowrap"><b>Perm. Molars Only</b></td>
        <td width="5px">&nbsp;</td>
        <td><input <? if($custDPWisdom == "Excludes Wisdom"){?>checked<? } ?> type="checkbox" name="custDPWisdom" value="Excludes Wisdom" /></td>
        <td nowrap="nowrap"><b>Excludes Wisdom</b></td>
        <td width="5px">&nbsp;</td>
        <td><input <? if($custDPPremolars == "Premolars"){?>checked<? } ?> type="checkbox" name="custDPPremolars" value="Premolars" /></td>
        <td nowrap="nowrap"><b>Premolars</b></td>
        </tr>
        </table>    </td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>

<tr>
    <td nowrap="nowrap"><b>Prev resin restoration (D1352)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPPrev" name="custDPPrev" value="<?=$custDPPrev?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPPrevFreq" name="custDPPrevFreq" value="<?=$custDPPrevFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td nowrap="nowrap"><b>To Age?</b></td>
        <td><input type="text" class="textbox" id="custDPPrevToAge" name="custDPPrevToAge" value="<?=$custDPPrevToAge?>" style="width: 30px" /></td>
        <td width="5"></td>
        </tr>
        </table>    </td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Space maintainer (D1510)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPSpaceMain" name="custDPSpaceMain" value="<?=$custDPSpaceMain?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPSpaceMainFreq" name="custDPSpaceMainFreq" value="<?=$custDPSpaceMainFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td nowrap="nowrap"><b>To Age?</b></td>
        <td><input type="text" class="textbox" id="custDPSpaceMainToAge" name="custDPSpaceMainToAge" value="<?=$custDPSpaceMainToAge?>" style="width: 30px" /></td>
        <td width="5"></td>
        </tr>
        </table>    </td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Space maintainer (D1515)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPSpaceMain2" name="custDPSpaceMain2" value="<?=$custDPSpaceMain2?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPSpaceMain2Freq" name="custDPSpaceMain2Freq" value="<?=$custDPSpaceMain2Freq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td nowrap="nowrap"><b>To Age?</b></td>
        <td><input type="text" class="textbox" id="custDPSpaceMain2ToAge" name="custDPSpaceMain2ToAge" value="<?=$custDPSpaceMain2ToAge?>" style="width: 30px" /></td>
        <td width="5"></td>
        </tr>
        </table>    </td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr><tr>    <td nowrap="nowrap"><b>Cone Beam (D0367)</b></td>    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPConeBeam" name="custDPConeBeam" value="<?=$custDPConeBeam?>" style="width: 50px" />%</td>    <td width="20">&nbsp;</td>    <td><b>Freq</b></td>    <td>    <input type="text" class="textbox" id="custDPConeBeamFreq" name="custDPConeBeamFreq" value="<?=$custDPConeBeamFreq?>" style="width: 75px" />    </td>    <td width="10">&nbsp;</td>    <td>    	<table cellpadding="1" cellspacing="0" border="0">        <tr>        <td nowrap="nowrap"><b>To Age?</b></td>        <td><input type="text" class="textbox" id="custDPConeBeamToAge" name="custDPConeBeamToAge" value="<?=$custDPConeBeamToAge?>" style="width: 30px" /></td>        <td width="5"></td>        </tr>        </table>    </td>    <td width="20">&nbsp;</td>    <td>&nbsp;</td>    <td>&nbsp;</td></tr>
<tr>
    <td nowrap="nowrap"><b>Oral cancer screening (0431)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPOralCancer" name="custDPOralCancer" value="<?=$custDPOralCancer?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPOralCancerFreq" name="custDPOralCancerFreq" value="<?=$custDPOralCancerFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td>
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>IOP (0350)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custDPIOP" name="custDPIOP" value="<?=$custDPIOP?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>Freq</b></td>
            <td>
            <input type="text" class="textbox" id="custDPIOPFreq" name="custDPIOPFreq" value="<?=$custDPIOPFreq?>" style="width: 75px" /></td>
        </tr>
        </table>
    </td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Missed Appointment (D9986)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPMissedApp" name="custDPMissedApp" value="<?=$custDPMissedApp?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custDPMissedAppFreq" name="custDPMissedAppFreq" value="<?=$custDPMissedAppFreq?>" style="width: 75px" /></td>
    <td width="10">&nbsp;</td>
    <td>
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Cancelled Appointment (D9987)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custDPCancelledApp" name="custDPCancelledApp" value="<?=$custDPCancelledApp?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custDPCancelledAppFreq" name="custDPCancelledAppFreq" value="<?=$custDPCancelledAppFreq?>" style="width: 75px" /></td>
        </tr>
        </table>
    </td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">Crowns & Composites</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
<tr>
    <td width="130" nowrap="nowrap"><b>Posterior Composites (2391-94)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custCCComposites" name="custCCComposites" value="<?=$custCCComposites?>" style="width: 50px" />%</td>
    <td width="10">&nbsp;</td>
    <td colspan="6">
    	<table cellpadding="1" cellspacing="0" border="0">
    	  <tr>
    	  <td nowrap="nowrap"><b>Downgrade?</b></td>
          <td><input <? if($custCCDowngrade == "Yes"){?>checked<? } ?> type="radio" name="custCCDowngrade" value="Yes" /></td>
          <td>Yes</td>
          <td width="5px">&nbsp;</td>
          <td><input <? if($custCCDowngrade == "No"){?>checked<? } ?> type="radio" name="custCCDowngrade" value="No" /></td>
          <td>No</td>
          <td width="20px">&nbsp;</td>
          <td><input <? if($custCCMolarsPre == "Molars"){?>checked<? } ?> type="radio" name="custCCMolarsPre" value="Molars" /></td>
          <td nowrap="nowrap"><b>Molars</b></td>
          <td><input <? if($custCCMolarsPre == "Premolars"){?>checked<? } ?> type="radio" name="custCCMolarsPre" value="Premolars" /></td>
          <td><b>Premolars</b></td>
          <td width="20">&nbsp;</td>
          <td width="30"><b>Freq</b></td>
          <td><input type="text" class="textbox" id="custCCMolarsPreFreq" name="custCCMolarsPreFreq" value="<?=$custCCMolarsPreFreq?>" style="width: 60px" /></td>
          </tr>
  	  </table></td>
    </tr>
<tr>
    <td nowrap="nowrap"><b>Posterior Crowns (2740)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custCCCrowns" name="custCCCrowns" value="<?=$custCCCrowns?>" style="width: 50px" />%</td>
    <td></td>
    <td colspan="6">
    	<table cellpadding="1" cellspacing="0" border="0">
    	  <tr>
    	  <td nowrap="nowrap"><b>Downgrade?</b></td>
          <td><input <? if($custCCCrownsDowngrade == "Yes"){?>checked<? } ?> type="radio" name="custCCCrownsDowngrade" value="Yes" /></td>
          <td>Yes</td>
          <td width="5px">&nbsp;</td>
          <td><input <? if($custCCCrownsDowngrade == "No"){?>checked<? } ?> type="radio" name="custCCCrownsDowngrade" value="No" /></td>
          <td>No</td>
          <td width="15px">&nbsp;</td>
          <td><b>Paid On</b></td>
          <td><input <? if($custCCPaidOn == "Prep"){?>checked<? } ?> type="radio" name="custCCPaidOn" value="Prep" /></td>
          <td nowrap="nowrap">Prep</td>
          <td><input <? if($custCCPaidOn == "Seat Date"){?>checked<? } ?> type="radio" name="custCCPaidOn" value="Seat Date" /></td>
          <td>Seat Date</td>
          <td width="15">&nbsp;</td>
          <td nowrap="nowrap"><b>Build Up (2950)</b></td>
          <td><input type="text" class="textbox" id="custCCBuildUp" name="custCCBuildUp" value="<?=$custCCBuildUp?>" style="width: 60px" /></td>
          </tr>
  	  </table>    </td>
    </tr>
<tr>
    <td nowrap="nowrap"><b>Re-Cement Crown (2920) Freq?</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custCCReCrown" name="custCCReCrown" value="<?=$custCCReCrown?>" style="width: 50px" />%</td>
    <td></td>
    <td><b>Freq</b></td>
    <td width="80"><input type="text" class="textbox" id="custCCReCrownFreq" name="custCCReCrownFreq" value="<?=$custCCReCrownFreq?>" style="width: 60px" />    </td>
    <td width="5"></td>
    <td colspan="3" valign="top">
    	<table cellpadding="1" cellspacing="0" border="0">
    	  <tr>
    	    <td nowrap="nowrap"><b>Same Provider Re-cementing OK?</b></td>
          <td><input <? if($custCCReCementing == "Yes"){?>checked<? } ?> type="radio" name="custCCReCementing" value="Yes" /></td>
          <td>Yes</td>
          <td width="10px">&nbsp;</td>
          <td><input <? if($custCCReCementing == "No"){?>checked<? } ?> type="radio" name="custCCReCementing" value="No" /></td>
          <td>No</td>
          </tr>
  	  </table></td>
    </tr>
<tr>
    <td nowrap="nowrap"><b>Protective Restoration (2940)</b></td>    <td nowrap="nowrap"><input type="text" class="textbox" id="custCCRestoration" name="custCCRestoration" value="<?=$custCCRestoration?>" style="width: 50px" />%</td>    <td></td>    <td><b>Freq</b></td>    <td><input type="text" class="textbox" id="custCCRestorationFreq" name="custCCRestorationFreq" value="<?=$custCCRestorationFreq?>" style="width: 60px" /></td>    <td></td>    <td colspan="3" valign="top">    	<table cellpadding="3" cellspacing="0">        <tr>            <td width="160" nowrap="nowrap"><b>Crown Lengthening (4249)</b></td>            <td nowrap="nowrap"><input type="text" class="textbox" id="custCCLength" name="custCCLength" value="<?=$custCCLength?>" style="width: 50px" />%</td>            <td width="20px">&nbsp;</td>            <td><b>Freq</b></td>            <td>            <input type="text" class="textbox" id="custCCLengthFreq" name="custCCLengthFreq" value="<?=$custCCLengthFreq?>" style="width: 75px" />            </td>        </tr>        </table>    	</td></tr>
<tr>    <td nowrap="nowrap"><b>Crowns D2750</b></td>    <td nowrap="nowrap"><input type="text" class="textbox" id="custCCCrowns2" name="custCCCrowns2" value="<?=$custCCCrowns2?>" style="width: 50px" />%</td>    <td nowrap="nowrap">&nbsp;</td>    <td><b>Freq</b></td>    <td><input type="text" class="textbox" id="custCCCrowns2Freq" name="custCCCrowns2Freq" value="<?=$custCCCrowns2Freq?>" style="width: 60px" /></td>    <td></td>    <td colspan="3" valign="top">    	<table cellpadding="3" cellspacing="0">        <tr>            <td width="160" nowrap="nowrap"><b>Veneers (D2960)</b></td>            <td nowrap="nowrap"><input type="text" class="textbox" id="custCCVeneers" name="custCCVeneers" value="<?=$custCCVeneers?>" style="width: 50px" />%</td>            <td width="20px">&nbsp;</td>            <td><b>Freq</b></td>            <td>            <input type="text" class="textbox" id="custCCVeneersFreq" name="custCCVeneersFreq" value="<?=$custCCVeneersFreq?>" style="width: 75px" />            			</td>        </tr>        </table>    	</td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Pulp cap D3120</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custCCPulp" name="custCCPulp" value="<?=$custCCPulp?>" style="width: 50px" />%</td>
    <td nowrap="nowrap">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custCCPulpFreq" name="custCCPulpFreq" value="<?=$custCCPulpFreq?>" style="width: 60px" /></td>
    <td></td>
    <td valign="top">&nbsp;</td>
    <td width="10">&nbsp;</td>
    <td>      </td>
</tr>

<tr>
    <td nowrap="nowrap"><b>Replacement time for crowns/bridges</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custCCReplacementTime" name="custCCReplacementTime" value="<?=$custCCReplacementTime?>" style="width: 50px" />%</td>
    <td nowrap="nowrap">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custCCReplacementTimeFreq" name="custCCReplacementTimeFreq" value="<?=$custCCReplacementTimeFreq?>" style="width: 60px" /></td>
    <td></td>
    <td valign="top">&nbsp;</td>
    <td width="10">&nbsp;</td>
    <td>      </td>
</tr>
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">Endo</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
  <tr>
    <td width="130" nowrap="nowrap"><b>Anterior (3310)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custEAnterior" name="custEAnterior" value="<?=$custEAnterior?>" style="width: 50px" />%</td>
    <td width="20px">&nbsp;</td>
    <td nowrap="nowrap"><b>Bi-Cuspid (3320)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custEBiCuspid" name="custEBiCuspid" value="<?=$custEBiCuspid?>" style="width: 75px" />%</td>
    <td width="10px">&nbsp;</td>
    <td valign="top" nowrap="nowrap"><b>Molars (3330)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custEMolars" name="custEMolars" value="<?=$custEMolars?>" style="width: 75px" />%</td>
    <td width="300px">&nbsp;</td>
  </tr>
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">Perio</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
  <tr>
    <td width="130" nowrap="nowrap"><b>SPR Deep Cleaning (4341-42)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custPCleaning" name="custPCleaning" value="<?=$custPCleaning?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custPCleaningFreq" name="custPCleaningFreq" value="<?=$custPCleaningFreq?>" style="width: 75px" /></td>
    <td width="10">&nbsp;</td>
    <td colspan="2" valign="top" nowrap="nowrap">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="100" nowrap="nowrap"><b>Quads per visit</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custPQuadsPerVisit" name="custPQuadsPerVisit" value="<?=$custPQuadsPerVisit?>" style="width: 75px" /></td>
            <td width="10px">&nbsp;</td>
            <td><b>If 2Quads-Wait</b></td>
            <td>
            <input type="text" class="textbox" id="custP2Quads" name="custP2Quads" value="<?=$custP2Quads?>" style="width: 75px" />            </td>
        </tr>
        </table>    </td>
    </tr>
  <tr>
    <td nowrap="nowrap"><b>Full Mouth Debridement (4355)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custPDebridement" name="custPDebridement" value="<?=$custPDebridement?>" style="width: 50px" /> %</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custPDebridementFreq" name="custPDebridementFreq" value="<?=$custPDebridementFreq?>" style="width: 75px" /></td>
    <td width="10">&nbsp;</td>
    <td valign="top" nowrap="nowrap">
    	<table cellpadding="1" cellspacing="0" border="0">
    	  <tr>
    	  <td nowrap="nowrap"><b>Takes place of PROPHY? </b></td>
          <td><input <? if($custPPXA == "Yes"){?>checked<? } ?> type="radio" name="custPPXA" value="Yes" /></td>
          <td>Yes</td>
          <td width="5px">&nbsp;</td>
          <td><input <? if($custPPXA == "No"){?>checked<? } ?> type="radio" name="custPPXA" value="No" /></td>
          <td>No</td>
          </tr>
  	  </table>    </td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Perio Maintenance (4910)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custPMaintenance" name="custPMaintenance" value="<?=$custPMaintenance?>" style="width: 50px" />
      %</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custPMaintenanceFreq" name="custPMaintenanceFreq" value="<?=$custPMaintenanceFreq?>" style="width: 75px" /></td>
    <td width="10">&nbsp;</td>
    <td valign="top" nowrap="nowrap">
    	<table cellpadding="1" cellspacing="0">
        <tr>
        <td><input <? if($custPCombInAdd == "Combined"){?>checked<? } ?> type="radio" name="custPCombInAdd" value="Combined" /></td>
        <td><b>Combined</b></td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custPCombInAdd == "In Addition to PROPHY"){?>checked<? } ?> type="radio" name="custPCombInAdd" value="In Addition to PROPHY" /></td>
        <td><b>In Addition to PROPHY</b></td>
        <td width="15px">&nbsp;</td>
        </tr>
        </table>    </td>
  </tr>
  
   <tr>
    <td nowrap="nowrap"><b>Gingival Inflammation(4346)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custGingivalInfl" name="custGingivalInfl" value="<?=$custGingivalInfl?>" style="width: 50px" />
      %</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custGingivalInflFreq" name="custGingivalInflFreq" value="<?=$custGingivalInflFreq?>" style="width: 75px" /></td>
    <td width="10">&nbsp;</td>
   
  </tr>
  <tr>
   <td><b>does it share with 1110</b></td>
   <td colspan="7">
      <table cellpadding="1" cellspacing="0">
         <tr>
            <td width="20px"><input <? if($custShare1110 == "Yes"){?>checked<? } ?> type="radio" name="custShare1110" value="Yes" /></td>
            <td>Yes</td>
            <td width="5px">&nbsp;</td>
            <td width="20px"><input <? if($custShare1110 == "No"){?>checked<? } ?> type="radio" name="custShare1110" value="No" /></td>
            <td>No</td>
            <td width="25px">&nbsp;</td>
            
            <td nowrap="nowrap"><b>does it share with 4910</b></td>
            <td><input <? if($custShare4910 == "Yes"){?>checked<? } ?> type="radio" name="custShare4910" value="Yes" /></td>
            <td>Yes</td>
            <td width="5px">&nbsp;</td>
            <td><input <? if($custShare4910 == "No"){?>checked<? } ?> type="radio" name="custShare4910" value="No" /></td>
            <td>No</td>
            <Td width="25px">&nbsp;</Td>
            <td nowrap="nowrap"><b>does it share with 4341/42</b></td>
            <td><input <? if($custShare4341 == "Yes"){?>checked<? } ?> type="radio" name="custShare4341" value="Yes" /></td>
            <td>Yes</td>
            <td width="5px">&nbsp;</td>
            <td><input <? if($custShare4341 == "No"){?>checked<? } ?> type="radio" name="custShare4341" value="No" /></td>
            <td>No</td>
            <Td width="20px">&nbsp;</Td>
         </tr>
      </table>
   </td>
</tr>
  
  <tr>
    <td nowrap="nowrap"><b>Desensitizing resin-cerv/root surf. (9911)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custPDesensit" name="custPDesensit" value="<?=$custPDesensit?>" style="width: 50px" />
      %</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custPDesensitFreq" name="custPDesensitFreq" value="<?=$custPDesensitFreq?>" style="width: 75px" /></td>
    <td width="10">&nbsp;</td>
    <td valign="top" nowrap="nowrap">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Arestin (4381)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custPArestin" name="custPArestin" value="<?=$custPArestin?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custPArestinFreq" name="custPArestinFreq" value="<?=$custPArestinFreq?>" style="width: 75px" /></td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td colspan="4" nowrap="nowrap"><b>Is there a time lapse between SCRP and Perio Maintenance?</b></td>
    <td colspan="3"><input type="text" class="textbox" id="custPTimeLapse" name="custPTimeLapse" value="<?=$custPTimeLapse?>" style="width: 200px" /></td>
  </tr>
  <tr>
    <td colspan="4" nowrap="nowrap"><b>Is there a time lapse between 4910 and 1110?</b></td>
    <td colspan="3"><input type="text" class="textbox" id="custPTimeLapse4910" name="custPTimeLapse4910" value="<?=$custPTimeLapse4910?>" style="width: 200px" /></td>
  </tr>

</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">Dentures</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
  <tr>
    <td nowrap="nowrap"><b>Replacement Time Freq</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDTime" name="custDTime" value="<?=$custDTime?>" style="width: 75px" /></td>
    <td width="10px">&nbsp;</td>
    <td nowrap="nowrap"><b>Covered @</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDCovered" name="custDCovered" value="<?=$custDCovered?>" style="width: 75px" />%</td>
    <td width="450px">&nbsp;</td>
  </tr>
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">Surgical</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
  <tr>
    <td width="130" nowrap="nowrap"><b>Implant (6010)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSImplant" name="custSImplant" value="<?=$custSImplant?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Prefab Abutment (6056)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custSPrefabAbutment" name="custSPrefabAbutment" value="<?=$custSPrefabAbutment?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>Custom Abutment (6057)</b></td>
            <td><input type="text" class="textbox" id="custSCustomAbutment" name="custSCustomAbutment" value="<?=$custSCustomAbutment?>" style="width: 50px" />%</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>Implant all ceramic crown (6058)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSceramicCrown" name="custSceramicCrown" value="<?=$custSceramicCrown?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Implant PFM (6066)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custSPFM" name="custSPFM" value="<?=$custSPFM?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>Bone Graft (6104)</b></td>
            <td><input type="text" class="textbox" id="custSGraft" name="custSGraft" value="<?=$custSGraft?>" style="width: 50px" />%</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>Bone Graft (7953)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSGraft2" name="custSGraft2" value="<?=$custSGraft2?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Membrane (4266)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custSMembrane" name="custSMembrane" value="<?=$custSMembrane?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>Membrane (4267)</b></td>
            <td><input type="text" class="textbox" id="custSMembrane2" name="custSMembrane2" value="<?=$custSMembrane2?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>N2O (9230)</b></td>
            <td><input type="text" class="textbox" id="custSN2O" name="custSN2O" value="<?=$custSN2O?>" style="width: 50px" />%</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>Consult (9310)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSConsult" name="custSConsult" value="<?=$custSConsult?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Anesthesia - First 30min (9223)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custSAnesthesia" name="custSAnesthesia" value="<?=$custSAnesthesia?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>Additional 15min (9243)</b></td>
            <td><input type="text" class="textbox" id="custSAdditional" name="custSAdditional" value="<?=$custSAdditional?>" style="width: 50px" />%</td>
        </tr>
        </table>
    </td>
  </tr>
  
  <tr>
    <td width="130" nowrap="nowrap"><b>Simple Ext (7140)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSExt" name="custSExt" value="<?=$custSExt?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Covered</b></td>
            <td><input <? if($custSExtCovered == "Medical"){?>checked<? } ?> type="radio" name="custSExtCovered" value="Medical" /></td>
            <td>Medical</td>
            <td width="10px">&nbsp;</td>
            <td><input <? if($custSExtCovered == "Dental"){?>checked<? } ?> type="radio" name="custSExtCovered" value="Dental" /></td>
            <td>Dental</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>Surgical Ext (7210)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSSurgical" name="custSSurgical" value="<?=$custSSurgical?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Covered</b></td>
            <td><input <? if($custSSurgicalCovered == "Medical"){?>checked<? } ?> type="radio" name="custSSurgicalCovered" value="Medical" /></td>
            <td>Medical</td>
            <td width="10px">&nbsp;</td>
            <td><input <? if($custSSurgicalCovered == "Dental"){?>checked<? } ?> type="radio" name="custSSurgicalCovered" value="Dental" /></td>
            <td>Dental</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>Soft Tissue Ext (7220)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSTissue" name="custSTissue" value="<?=$custSTissue?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Covered</b></td>
            <td><input <? if($custSTissueCovered == "Medical"){?>checked<? } ?> type="radio" name="custSTissueCovered" value="Medical" /></td>
            <td>Medical</td>
            <td width="10px">&nbsp;</td>
            <td><input <? if($custSTissueCovered == "Dental"){?>checked<? } ?> type="radio" name="custSTissueCovered" value="Dental" /></td>
            <td>Dental</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>Partial Bony Ext (7230)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSBony" name="custSBony" value="<?=$custSBony?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Covered</b></td>
            <td><input <? if($custSBonyCovered == "Medical"){?>checked<? } ?> type="radio" name="custSBonyCovered" value="Medical" /></td>
            <td>Medical</td>
            <td width="10px">&nbsp;</td>
            <td><input <? if($custSBonyCovered == "Dental"){?>checked<? } ?> type="radio" name="custSBonyCovered" value="Dental" /></td>
            <td>Dental</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>Complete Bony Ext (7240)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSBony2" name="custSBony2" value="<?=$custSBony2?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Covered</b></td>
            <td><input <? if($custSBony2Covered == "Medical"){?>checked<? } ?> type="radio" name="custSBony2Covered" value="Medical" /></td>
            <td>Medical</td>
            <td width="10px">&nbsp;</td>
            <td><input <? if($custSBony2Covered == "Dental"){?>checked<? } ?> type="radio" name="custSBony2Covered" value="Dental" /></td>
            <td>Dental</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>Ext Complications (7241)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSComp" name="custSComp" value="<?=$custSComp?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Covered</b></td>
            <td><input <? if($custSCompCovered == "Medical"){?>checked<? } ?> type="radio" name="custSCompCovered" value="Medical" /></td>
            <td>Medical</td>
            <td width="10px">&nbsp;</td>
            <td><input <? if($custSCompCovered == "Dental"){?>checked<? } ?> type="radio" name="custSCompCovered" value="Dental" /></td>
            <td>Dental</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>Root Tip Ext (7250)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSTip" name="custSTip" value="<?=$custSTip?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Covered</b></td>
            <td><input <? if($custSTipCovered == "Medical"){?>checked<? } ?> type="radio" name="custSTipCovered" value="Medical" /></td>
            <td>Medical</td>
            <td width="10px">&nbsp;</td>
            <td><input <? if($custSTipCovered == "Dental"){?>checked<? } ?> type="radio" name="custSTipCovered" value="Dental" /></td>
            <td>Dental</td>
        </tr>
        </table>
    </td>
  </tr>
  
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">Occlusal Guards</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
  <tr>
    <td width="130" nowrap="nowrap"><b>Occlusal Guards (9944)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custOc" name="custOc" value="<?=$custOc?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custOcFreq" name="custOcFreq" value="<?=$custOcFreq?>" style="width: 75px" /></td>
    <td width="10">&nbsp;</td>
    <td colspan="2" valign="top" nowrap="nowrap">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="100" nowrap="nowrap"><b>Age Minimum?</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custOcAgeMinimum" name="custOcAgeMinimum" value="<?=$custOcAgeMinimum?>" style="width: 75px" /></td>
            <td width="300">&nbsp;</td>
          </tr>
        </table>    </td>
    </tr>
    <tr>
    <td width="130" nowrap="nowrap"><b>Any Specific Guideline?</b></td>
    <td colspan="7" nowrap="nowrap">
    	<table cellpadding="1" cellspacing="0">
        <tr>
        <td><input <? if($custOcGuideline == "None"){?>checked<? } ?> type="radio" name="custOcGuideline" value="None" /></td>
        <td><b>None</b></td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custOcGuideline == "Bruxism"){?>checked<? } ?> type="radio" name="custOcGuideline" value="Bruxism" /></td>
        <td><b>Bruxism</b></td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custOcGuideline == "Osseous Surgery"){?>checked<? } ?> type="radio" name="custOcGuideline" value="Osseous Surgery" /></td>
        <td><b>Osseous Surgery</b></td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custOcGuideline == "Ortho"){?>checked<? } ?> type="radio" name="custOcGuideline" value="Ortho" /></td>
        <td><b>Ortho</b></td>
        </tr>
        </table>
    </td>
    </tr>
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">Orthodontic</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
  <tr>
    <td colspan="7" nowrap="nowrap">
    	<table cellpadding="1" cellspacing="0">
    	  <tr>
    	    <td><input <? if($custOr == "Subscriber"){?>checked<? } ?> type="radio" name="custOr" value="Subscriber" /></td>
          <td><b>Subscriber</b></td>
          <td width="10px">&nbsp;</td>
          <td><input <? if($custOr == "Spouse"){?>checked<? } ?> type="radio" name="custOr" value="Spouse" /></td>
          <td><b>Spouse</b></td>
          <td width="10px">&nbsp;</td>
          <td><input <? if($custOr == "Dependent"){?>checked<? } ?> type="radio" name="custOr" value="Dependent" /></td>
          <td><b>Dependent</b></td>
          <td width="10px">&nbsp;</td>
          <td><input <? if($custOr == "All Covered"){?>checked<? } ?> type="radio" name="custOr" value="All Covered" /></td>
          <td><b>All Covered</b></td>
          </tr>
  	  </table></td>
    </tr>
  <tr>
    <td width="60" nowrap="nowrap"><b>Ortho</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custOrOrtho" name="custOrOrtho" value="<?=$custOrOrtho?>" style="width: 50px" />%</td>
    <td colspan="3">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="50" nowrap="nowrap"><b>To Age</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custOrAge" name="custOrAge" value="<?=$custOrAge?>" style="width: 75px" /></td>
            <td width="20px">&nbsp;</td>
            <td nowrap="nowrap"><b>Lifetime Max $</b></td>
            <td><input type="text" class="textbox" id="custOrLifetimeMax" name="custOrLifetimeMax" value="<?=$custOrLifetimeMax?>" style="width: 50px" /></td>
            <td width="20px">&nbsp;</td>
            <td nowrap="nowrap"><b>Remaining $</b></td>
            <td><input type="text" class="textbox" id="custOrRemaining" name="custOrRemaining" value="<?=$custOrRemaining?>" style="width: 50px" /></td>
            <td width="20px">&nbsp;</td>
            <td nowrap="nowrap"><b>Deductible $</b></td>
            <td><input type="text" class="textbox" id="custOrDeductible" name="custOrDeductible" value="<?=$custOrDeductible?>" style="width: 50px" /></td>
        </tr>
        </table>    </td>
    <td colspan="2" valign="top" nowrap="nowrap">&nbsp;</td>
  </tr>
  <tr>
    <td width="60" nowrap="nowrap"><b>Initial Payment</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custOrPayment" name="custOrPayment" value="<?=$custOrPayment?>" style="width: 50px" />%</td>
    <td colspan="3">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="40" nowrap="nowrap"><b>Paid:</b></td>
            <td><input <? if($custOrPaidMonthly == "Monthly"){?>checked<? } ?> type="checkbox" name="custOrPaidMonthly" value="Monthly" /></td>
            <td nowrap="nowrap"><b>Monthly</b></td>
            <td width="5px">&nbsp;</td>
            <td><input <? if($custOrPaidQuarterly == "Quarterly"){?>checked<? } ?> type="checkbox" name="custOrPaidQuarterly" value="Quarterly" /></td>
            <td nowrap="nowrap"><b>Quarterly</b></td>
            <td width="5px">&nbsp;</td>
            <td><input <? if($custOrPaidAutomatic == "Automatic"){?>checked<? } ?> type="checkbox" name="custOrPaidAutomatic" value="Automatic" /></td>
            <td nowrap="nowrap"><b>Automatic</b></td>
            <td width="5px">&nbsp;</td>
            <td><input <? if($custOrClaim == "We File Claim"){?>checked<? } ?> type="checkbox" name="custOrClaim" value="We File Claim" /></td>
            <td nowrap="nowrap"><b>We File Claim (Manually)</b></td>
          </tr>
        </table>    </td>
    <td colspan="2" valign="top" nowrap="nowrap">&nbsp;</td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Work in progress covered?</b></td>
    <td colspan="4" nowrap="nowrap">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td><input <? if($custOrProgress == "Yes"){?>checked<? } ?> type="radio" name="custOrProgress" value="Yes" /></td>
            <td>Yes</td>
            <td width="5px">&nbsp;</td>
            <td><input <? if($custOrProgress == "No"){?>checked<? } ?> type="radio" name="custOrProgress" value="No" /></td>
            <td>No</td>
            <td width="200">&nbsp;</td>
          </tr>
        </table>
    </td>
    <td colspan="2" valign="top" nowrap="nowrap">&nbsp;</td>
  </tr>
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">History</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
  <tr>
    <td width="60" nowrap="nowrap"><b>History</b></td>
    <td nowrap="nowrap"><textarea class="textbox" id="custHistory" name="custHistory" rows="2"  style="width: 800px"><?=$custHistory?></textarea></td>
    </tr>
</table>

<br />

<?php
//$date = date_create(date('y-m-d'), timezone_open('Pacific/Nauru'));
$date = putenv("TZ=US/Pacific");
$pacific_time = date("h:i:s");
$todate = date("Y-m-d");

if($custDateTime == "")
{
	$custDateTime = $todate . " ". $pacific_time;
}
?>


  <table width="100%" cellpadding="5" cellspacing="0">
    <tr class="titleTr">
      <td colspan="4"><label id="rightLabel">Special Request</label></td>
    </tr>
  </table>
  <table cellpadding="3" cellspacing="5" width="100%" style="border:1px solid; background-color: #EFEFEF;">
    <tr>
      <td colspan="4"><b>Special Request</b></td>
    </tr>
    <tr>
      <td colspan="4">
      <textarea name="custSpecialRequest" id="custSpecialRequest" cols="100" rows="5"><?=$custSpecialRequest?></textarea>
      <script type="text/javascript">
	  CKEDITOR.replace( 'custSpecialRequest' );
	  </script>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td width="90px" valign="top"><b>Spoke with:</b></td>
      <td width="250px"><input type="text" class="textbox" id="custSpokeWith" name="custSpokeWith" value="<?=$custSpokeWith?>" style="width: 200px" /></td>
      <td width="140px" valign="top"><b>Account Executive:</b></td>
      <td><input type="text" class="textbox" id="custAccountExecutive" name="custAccountExecutive" value="<?=$custAccountExecutive?>" style="width: 200px" /></td>
    </tr>
    <tr>
      <td width="80px" valign="top"><b>Date/Time:</b></td>
      <td colspan="3"><input type="text" class="textbox" id="custDateTime" name="custDateTime" value="<?=date("m/d/Y h:i:s")?>" style="width: 200px" />
        <br />
        <p>Last Date / Time:
          <?=$custDateTime?>
        </p></td>
    </tr>
  </table>
</div>