<table cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td width="49%">
				<table cellpadding="3" cellspacing="0" width="100%">
					<tr class="titleTr">
						<td colspan="4"><b>Deductible</b></td>
					</tr>
					<tr>
						<td width="250px"><b>Individual $</b></td>
						<td><input type="text" class="textbox" id="dedIndividual" name="dedIndividual" value="<?=$dedIndividual?>" style="width: 100px" /></td>
					</tr>
					<tr class="alternate">
						<td><b>Family $</b></td>
						<td><input type="text" class="textbox" id="dedFamily" name="dedFamily" value="<?=$dedFamily?>" style="width: 100px" /></td>
					</tr>
					<tr>
						<td><b>Waived on Preventative</b></td>
						<td>
							<table cellpadding="1" cellspacing="0">
								<tr>
									<td><input <?if($dedWaived == "Yes"){?>checked<? } ?>  type="radio" name="dedWaived" value="Yes" /></td>
									<td>Yes</td>
									<td width="20px">&nbsp;</td>
									<td><input <?if($dedWaived == "No"){?>checked<? } ?>  type="radio" name="dedWaived" value="No" /></td>
									<td>No</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr class="alternate">
						<td><b>Has deductible been met this year</b></td>
						<td >
							<table cellpadding="1" cellspacing="0">
								<tr>
									<td><input <?if($dedMet == "Yes"){?>checked<? } ?>  type="radio" name="dedMet" value="Yes" /></td>
									<td>Yes</td>
									<td width="20px">&nbsp;</td>
									<td><input <?if($dedMet == "No"){?>checked<? } ?>  type="radio" name="dedMet" value="No" /></td>
									<td>No</td>
								</tr>
							</table>
						</td>
					</tr>									
				</table>	
			</td>
			<td width="2%">&nbsp;</td>
			<td width="49%">
				<table cellpadding="3" cellspacing="0" width="100%">
					<tr class="titleTr">
						<td colspan="4"><b><!--Out Of Network Decuctible-->out of Network deductible</b></td>
					</tr>
					<tr>
						<td width="250px"><b>Individual $</b></td>
						<td ><input type="text" class="textbox" id="outOfNetIndividual" name="outOfNetIndividual" value="<?=$outOfNetIndividual?>" style="width: 100px" /></td>
					</tr>
					<tr class="alternate">
						<td><b>Family $</b></td>
						<td ><input type="text" class="textbox" id="outOfNetFamily" name="outOfNetFamily" value="<?=$outOfNetFamily?>" style="width: 100px" /></td>
					</tr>
					<tr>
						<td><b>Waived on Preventative</b></td>
						<td >
							<table cellpadding="1" cellspacing="0">
								<tr>
									<td><input <?if($outOfNetWaived == "Yes"){?>checked<? } ?>  type="radio" name="outOfNetWaived" value="Yes" /></td>
									<td>Yes</td>
									<td width="20px">&nbsp;</td>
									<td><input <?if($outOfNetWaived == "No"){?>checked<? } ?>  type="radio" name="outOfNetWaived" value="No" /></td>
									<td>No</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr class="alternate">
						<td><b>Has out of deductible been met this year</b></td>
						<td >
							<table cellpadding="1" cellspacing="0">
								<tr>
									<td><input <?if($outOfNetMet == "Yes"){?>checked<? } ?>  type="radio" name="outOfNetMet" value="Yes" /></td>
									<td>Yes</td>
									<td width="20px">&nbsp;</td>
									<td><input <?if($outOfNetMet == "No"){?>checked<? } ?>  type="radio" name="outOfNetMet" value="No" /></td>
									<td>No</td>
								</tr>
							</table>
						</td>
					</tr>									
				</table>	
			</td>
		</tr>
	</table>
	<br />
	<table cellpadding="3" cellspacing="0" width="100%">
		<tr>
			<td width="180px"><b>Preventative @</b></td>
			<td><input type="text" class="textbox" id="prePercentage" name="prePercentage" value="<?=$prePercentage?>" style="width: 100px" />&nbsp;%</td>
			<td width="15px">&nbsp;</td>
			<td width="130px"><b>Out Of Network @</b></td>
			<td><input type="text" class="textbox" id="preNetwork" name="preNetwork" value="<?=$preNetwork?>" style="width: 100px" />&nbsp;%</td>
		</tr>
		<tr class="alternate">
			<td><b>Basic @</b></td>
			<td><input type="text" class="textbox" id="basicPercentage" name="basicPercentage" value="<?=$basicPercentage?>" style="width: 50px" /> %</td>			<td>&nbsp;</td>
			<td><b>Out of Network @</b></td>
			<td><input type="text" class="textbox" id="basicNetwork" name="basicNetwork" value="<?=$basicNetwork?>" style="width: 50px" /> %</td>
		</tr>
		<tr>
			<td><b>Prosthodontics/Major @</b></td>
			<td><input type="text" class="textbox" id="majorPer" name="majorPer" value="<?=$majorPer?>" style="width: 50px" /> %</td>	
			<td>&nbsp;</td>
			<td><b>Out of Network @</b></td>
			<td><input type="text" class="textbox" id="majorNetwork" name="majorNetwork" value="<?=$majorNetwork?>" style="width: 50px" /> %</td>
		</tr>
		<tr class="alternate">
			<td><b>FMX (0210)</b></td>
			<td colspan="4">
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td>Are they under</td>
						<td width="15px">&nbsp;</td>
						<td><input type="radio" <?if($preFmx == "Preventative"){?>checked<? } ?>  name="preFmx" value="Preventative" /></td>
						<td>Preventative</td>
						<td width="15px">&nbsp;</td>
						<td><input type="radio" <?if($preFmx == "Basic"){?>checked<? } ?>  name="preFmx" value="Basic" /></td>
						<td>Basic (Deductible waived)</td>
						<td width="15px">&nbsp;</td>
						<td><input type="radio" <?if($preFmxWaived == "Yes"){?>checked<? } ?>  name="preFmxWaived" value="Yes" /></td>
						<td>Yes</td>
						<td width="5px">&nbsp;</td>
						<td><input type="radio" <?if($preFmxWaived == "No"){?>checked<? } ?>  name="preFmxWaived" value="No" /></td>
						<td>No</td>
					</tr>												
				</table>
			</td>
		</tr>
		<tr>
			<td><b>Panorex (0330)</b></td>
			<td colspan="4">
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td>Are they under</td>
						<td width="15px">&nbsp;</td>
						<td><input type="radio" <?if($prePanorex == "Preventative"){?>checked<? } ?>  name="prePanorex" value="Preventative" /></td>
						<td>Preventative</td>
						<td width="15px">&nbsp;</td>
						<td><input type="radio" <?if($prePanorex == "Basic"){?>checked<? } ?>  name="prePanorex" value="Basic" /></td>
						<td>Basic</td>
						
					</tr>
				</table>
			</td>
		</tr>
		<tr class="alternate">
			<td><b>Periapicals (0220-0240)</b></td>
			<td colspan="4">
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td>Are they under</td>
						<td width="15px">&nbsp;</td>
						<td><input type="radio" <?if($prePeriapicals == "Preventative"){?>checked<? } ?>  name="prePeriapicals" value="Preventative" /></td>
						<td>Preventative</td>
						<td width="15px">&nbsp;</td>
						<td><input type="radio" <?if($prePeriapicals == "Basic"){?>checked<? } ?>  name="prePeriapicals" value="Basic" /></td>
						<td>Basic</td>
						
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table cellpadding="3" cellspacing="0" width="100%">
		<tr>
			<td width="170px"><b>Coordination of benefits</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td><input name="preCoordinateBenefits" type="radio" <? if($preCoordinateBenefits == "Standard"){?>checked<? } ?> value="Standard"/></td>
						<td width="30px">Standard</td>
						<td width="5px"><input name="preCoordinateBenefits" type="radio" <? if($preCoordinateBenefits == "Non-duplication"){?>checked<? } ?> value="Non-duplication"/></td>
						<td>Non-duplication</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>