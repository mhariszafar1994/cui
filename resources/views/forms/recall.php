<div id="divRecall">
	<table cellpadding="3" cellspacing="0" width="100%">
		<tr class="alternate">
			<td width="180px"><b>Total Max</b></td>
			<td>$ <input type="text" class="textbox" id="recTotMax" name="recTotMax" value="<?=$recTotMax?>" style="width: 100px" /></td>
		</tr>
		<tr>
			<td width="180px"><b>Remaining Max</b></td>
			<td>$ <input type="text" class="textbox" id="recRemMax" name="recRemMax" value="<?=$recRemMax?>" style="width: 100px" /></td>
		</tr>
		<tr class="alternate">
			<td width="180px"><b>History of Bitewings (0270)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td><input name="recHis" type="radio" <? if($recHis == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="recHis" type="radio" <? if($recHis == "No"){?>checked<? } ?> value="No"/></td>
						<td width="10px">No</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr class="alternate">
			<td width="180px" style="padding-left:25px;"><b>If Yes,Date</b></td>
			<td>
				<input type="text" class="textbox" id="recHisDate" name="recHisDate" value="<?=$recHisDate?>" style="width:100px;" readonly="yes" /> <img src="<?=HTTP_SERVER?>images/calendar.gif" alt="Calendar" onclick="displayDatePicker('recHisDate', this);"> <img src="<?=HTTP_SERVER?>images/icon_delete.png" alt="Calendar" onclick="document.getElementById('recHisDate').value=''">
			</td>
		</tr>
		<tr>
			<td width="180px"><b>History of FMX (0210)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td><input name="recFMX" type="radio" <? if($recFMX == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="recFMX" type="radio" <? if($recFMX == "No"){?>checked<? } ?> value="No"/></td>
						<td width="10px">No</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td style="padding-left:25px;" width="180px"><b>If Yes,Date</b></td>
			<td>
				<input type="text" class="textbox" id="recFMXDate" name="recFMXDate" value="<?=$recFMXDate?>" style="width:100px;" readonly="yes" /> <img src="<?=HTTP_SERVER?>images/calendar.gif" alt="Calendar" onclick="displayDatePicker('recFMXDate', this);"> <img src="<?=HTTP_SERVER?>images/icon_delete.png" alt="Calendar" onclick="document.getElementById('recFMXDate').value=''">
			</td>
		</tr>
		<tr class="alternate">
			<td width="180px"><b>History of Panorex (0330)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td><input name="recPanorex" type="radio" <? if($recPanorex == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="recPanorex" type="radio" <? if($recPanorex == "No"){?>checked<? } ?> value="No"/></td>
						<td width="10px">No</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr class="alternate">
			<td width="180px" style="padding-left:25px;"><b>If Yes,Date</b></td>
			<td>
				<input type="text" class="textbox" id="recPanorexDate" name="recPanorexDate" value="<?=$recPanorexDate?>" style="width:100px;" readonly="yes" /> <img src="<?=HTTP_SERVER?>images/calendar.gif" alt="Calendar" onclick="displayDatePicker('recPanorexDate', this);"> <img src="<?=HTTP_SERVER?>images/icon_delete.png" alt="Calendar" onclick="document.getElementById('recPanorexDate').value=''">
			</td>
		</tr>
		<tr>
			<td width="180px"><b>History of Prophy (1110)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td><input name="recProphy" type="radio" <? if($recProphy == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="recProphy" type="radio" <? if($recProphy == "No"){?>checked<? } ?> value="No"/></td>
						<td width="10px">No</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="180px" style="padding-left:25px;"><b>If Yes,Date</b></td>
			<td>
				<input type="text" class="textbox" id="recProphyDate" name="recProphyDate" value="<?=$recProphyDate?>" style="width:100px;" readonly="yes" /> <img src="<?=HTTP_SERVER?>images/calendar.gif" alt="Calendar" onclick="displayDatePicker('recProphyDate', this);"> <img src="<?=HTTP_SERVER?>images/icon_delete.png" alt="Calendar" onclick="document.getElementById('recProphyDate').value=''">
			</td>
		</tr>
		<tr class="alternate">
			<td width="180px"><b>History of Flouride (1203-1204)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td><input name="recFlouride" type="radio" <? if($recFlouride == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="recFlouride" type="radio" <? if($recFlouride == "No"){?>checked<? } ?> value="No"/></td>
						<td width="10px">No</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr class="alternate">
			<td width="180px" style="padding-left:25px;"><b>If Yes,Date</b></td>
			<td>
				<input type="text" class="textbox" id="recFlourideDate" name="recFlourideDate" value="<?=$recFlourideDate?>" style="width:100px;" readonly="yes" /> <img src="<?=HTTP_SERVER?>images/calendar.gif" alt="Calendar" onclick="displayDatePicker('recFlourideDate', this);"> <img src="<?=HTTP_SERVER?>images/icon_delete.png" alt="Calendar" onclick="document.getElementById('recFlourideDate').value=''">
			</td>
		</tr>
		<tr>
			<td width="180px"><b>History of Exams (0120)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td><input name="recExams" type="radio" <? if($recExams == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="recExams" type="radio" <? if($recExams == "No"){?>checked<? } ?> value="No"/></td>
						<td width="10px">No</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="180px" style="padding-left:25px;"><b>If Yes,Date</b></td>
			<td>
				<input type="text" class="textbox" id="recExamsDate" name="recExamsDate" value="<?=$recExamsDate?>" style="width:100px;" readonly="yes" /> <img src="<?=HTTP_SERVER?>images/calendar.gif" alt="Calendar" onclick="displayDatePicker('recExamsDate', this);"> <img src="<?=HTTP_SERVER?>images/icon_delete.png" alt="Calendar" onclick="document.getElementById('recExamsDate').value=''">
			</td>
		</tr>
		<tr class="alternate">
			<td width="180px"><b>History of Sealants (1351)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td><input name="recSealants" type="radio" <? if($recSealants == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="recSealants" type="radio" <? if($recSealants == "No"){?>checked<? } ?> value="No"/></td>
						<td width="10px">No</td>
						
					</tr>
				</table>
			</td>			
		</tr>
		<tr class="alternate">
			<td width="180px" style="padding-left:25px;"><b>If Yes,Date</b></td>
			<td>
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td><input type="text" class="textbox" id="recSealantsDate" name="recSealantsDate" value="<?=$recSealantsDate?>" style="width:100px;" readonly="yes" /> <img src="<?=HTTP_SERVER?>images/calendar.gif" alt="Calendar" onclick="displayDatePicker('recSealantsDate', this);"> <img src="<?=HTTP_SERVER?>images/icon_delete.png" alt="Calendar" onclick="document.getElementById('recSealantsDate').value=''"></td>
						<td width="15px">&nbsp;</td>
						<td width="50px">Teeth #</td>
						<td><input type="text" class="textbox" id="recSealantsTeeth" name="recSealantsTeeth" value="<?=$recSealantsTeeth?>" style="width: 80px"/></td>
					</tr>
				</table>
				
			</td>
		</tr>
        
        <tr>
			<td width="180px"><b>Periodontal Maintenance (D4910)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td><input name="recPerioMain" type="radio" <? if($recPerioMain == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="recPerioMain" type="radio" <? if($recPerioMain == "No"){?>checked<? } ?> value="No"/></td>
						<td width="10px">No</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="180px" style="padding-left:25px;"><b>If Yes,Date</b></td>
			<td>
				<input type="text" class="textbox" id="recPerioMainDate" name="recPerioMainDate" value="<?=$recPerioMainDate?>" style="width:100px;" readonly="yes" /> <img src="<?=HTTP_SERVER?>images/calendar.gif" alt="Calendar" onclick="displayDatePicker('recPerioMainDate', this);"> <img src="<?=HTTP_SERVER?>images/icon_delete.png" alt="Calendar" onclick="document.getElementById('recPerioMainDate').value=''">
			</td>
		</tr>
        
		<!--<tr>
			<td colspan="2"><b>CUI Notes</b></td>
		</tr>
		<tr>
			<td colspan="2"><textarea name="recRequests" id="recRequests" cols="100" rows="5"><?=$recRequests?></textarea></td>
		</tr>
		<tr class="alternate">
			<td colspan="2"><b>Pending claims</b></td>
		</tr>
		<tr class="alternate">
			<td colspan="2"><textarea name="recClaims" id="recClaims" cols="100" rows="5"><?=$recClaims?></textarea></td>
		</tr>
		<tr>
			<td colspan="2"><b>Any outstanding pre-auths</b></td>
		</tr>
		<tr>
			<td colspan="2"><textarea name="recAuths" id="recAuths" cols="100" rows="5"><?=$recAuths?></textarea></td>
		</tr>-->
	</table>
</div>