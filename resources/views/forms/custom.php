<div id="divCustom">

<table cellpadding="3" cellspacing="0">
<tr>
<td width="130"><b>Network Type</b></td>
<td>
<select id="custNetwork" name="custNetwork" size="1">
    <option value=""></option>
    <option value="In Network" <? if($custNetwork=="In Network"){ ?>Selected<? } ?>>In Network</option>
    <option value="Out Of Network" <? if($custNetwork=="Out Of Network"){ ?>Selected<? } ?>>Out Of Network</option>
    <option value="Policy Terminated" <? if($custNetwork=="Policy Terminated"){ ?>Selected<? } ?>>Policy Terminated</option>
</select></td>
<td></td>
<td>
    <table border="0" cellpadding="2" cellspacing="0">
    <tr>
        <td nowrap="nowrap"><b>Payer&nbsp;ID</b></td>
        <td>&nbsp;</td>
        <td><input type="text" class="textbox" id="custPayorId" name="custPayorId" value="<?=$custPayorId?>" style="width: 100px" /></td>
    </tr>
    </table></td>
</tr>

<tr class="alternate">
<td width="130"><b>Quick Note</b></td>
<td colspan="6"><input type="text" class="textbox" id="custQuickNote" name="custQuickNote" value="<?=$custQuickNote?>" style="width: 610px" maxlength="155" /></td>
<td></td>
</tr>

<tr>
<td width="130"><b>Effective Date</b></td>
<td colspan="2"><input readonly type="text" class="textbox" id="custEffectiveDate" name="custEffectiveDate" value="<?=$custEffectiveDate?>" style="width: 60px" /><input type="button" value="Select" class="smallButton" onclick="displayDatePicker('custEffectiveDate');" /></td>
<td colspan="5">
<table cellpadding="2" cellspacing="0">
  <tr>
  <td><b>Fee Schedule</b></td>
  <td width="30">&nbsp;</td>
  <td valign="top" width="20px"><input <? if($custFee == "Fee Schedule"){?>checked<? } ?> type="radio" name="custFee" value="Fee Schedule" /></td>
  <td valign="top">Fee Schedule</td>
  <td width="15px">&nbsp;</td>
  <td valign="top" width="20px"><input <? if($custFee == "UCR"){?>checked<? } ?> type="radio" name="custFee" value="UCR" /></td>
  <td valign="top">UCR</td>
  <td valign="top">&nbsp;</td>
  <td valign="top"><input type="text" class="textbox" id="custFeeName" name="custFeeName" value="<?=$custFeeName?>" /></td>
  </tr>
</table></td>
<td>&nbsp;</td>
</tr>
<tr class="alternate">
<td width="130"><b>Member ID</b></td>
<td><input type="text" class="textbox" id="custMemberId" name="custMemberId" value="<?=$custMemberId?>" style="width: 100px" /></td>
<td width="10">&nbsp;</td>
<td width="130">

<table cellpadding="2" cellspacing="0">
  <tr>
  <td><b>Group#</b></td>
  <td width="30">&nbsp;</td>
  <td valign="top" width="20px"><input type="text" class="textbox" id="custGroup" name="custGroup" value="<?=$custGroup?>" style="width: 100px" /></td>
  </tr>
</table></td>
<td></td>
<td width="10">&nbsp;</td>
<td width="150">&nbsp;</td>
<td></td>
</tr>
<tr>
<td><b>Plan Type</b></td>
<td colspan="7">
<table cellpadding="1" cellspacing="0">
<tr>
<td width="20px"><input <? if($custPlanType == "Calender"){?>checked<? } ?> type="radio" name="custPlanType" value="Calender" /></td>
<td>Calendar</td>
<td width="15px">&nbsp;</td>
<td width="20px"><input <? if($custPlanType == "Fiscal Year"){?>checked<? } ?> type="radio" name="custPlanType" value="Fiscal Year" /></td>
<td>Fiscal Year</td>
<td width="5px">&nbsp;</td>
<td>&nbsp;</td>
<td>
<input readonly type="text" class="textbox" id="custPlanDate" name="custPlanDate" value="<?=$custPlanDate?>" style="width: 60px" /><input type="button" value="Select" class="smallButton" onclick="displayDatePicker('custPlanDate');" /></td>
</tr>
</table></td>
</tr>
<tr class="alternate">
<td valign="top"><b>Coverage</b></td>
<td>
<table cellpadding="2" cellspacing="0">
<tr>
<td>
<select id="custCoverage" name="custCoverage" size="1">
    <option value=""></option>
    <option value="Single" <? if($custCoverage=="Single"){ ?>Selected<? } ?>>Single</option>
    <option value="Family" <? if($custCoverage=="Family"){ ?>Selected<? } ?>>Family</option>
</select></td>
</tr>
</table></td>
<td>&nbsp;</td>
<td colspan="4">
<table cellpadding="1" cellspacing="0">
<tr>
<td nowrap="nowrap"><b>Coordination Of Benefits</b></td>
<td width="15px">&nbsp;</td>
<td><input <? if($custCoordinationOfBenefits == "2nd"){?>checked<? } ?> type="radio" name="custCoordinationOfBenefits" value="2nd" /></td>
<td>2nd</td>
<td width="20px">&nbsp;</td>
<td><input <? if($custCoordinationOfBenefits == "Standard"){?>checked<? } ?> type="radio" name="custCoordinationOfBenefits" value="Standard" /></td>
<td>Standard</td>
<Td width="20px">&nbsp;</Td>
<td><input <? if($custCoordinationOfBenefits == "Non Duplicate"){?>checked<? } ?> type="radio" name="custCoordinationOfBenefits" value="Non Duplicate" /></td>
<td nowrap="nowrap">Non Duplicate</td>
</tr>
</table></td>
<td>&nbsp;</td>
</tr>
<tr>
<td><b>MTC</b></td>
<td colspan="7">
<table cellpadding="1" cellspacing="0">
<tr>
<td><input <? if($custMtc == "Yes"){?>checked<? } ?> type="radio" name="custMtc" value="Yes" /></td>
<td>Yes</td>
<td width="20px">&nbsp;</td>
<td><input <? if($custMtc == "No"){?>checked<? } ?> type="radio" name="custMtc" value="No" /></td>
<td>No</td>
<Td width="14px">&nbsp;</Td>
<td nowrap="nowrap"><b>Waiting Period</b></td>
<td width="15px">&nbsp;</td>
<td><input <? if($custWaitingPeriod == "Yes"){?>checked<? } ?> type="radio" name="custWaitingPeriod" value="Yes" /></td>
<td>Yes</td>
<td width="20px">&nbsp;</td>
<td><input <? if($custWaitingPeriod == "No"){?>checked<? } ?> type="radio" name="custWaitingPeriod" value="No" /></td>
<td>No</td>
<Td width="20px">&nbsp;</Td>
<td><b>Details</b></td>
<td width="15px">&nbsp;</td>
<Td><input type="text" class="textbox" id="custWaitingPeriodDetail" name="custWaitingPeriodDetail" value="<?=$custWaitingPeriodDetail?>" style="width: 180px" /></Td>
</tr>
</table></td>
</tr>
<tr class="alternate">
  <td colspan="8">
  	<table cellpadding="3" cellspacing="0">
      <tr>
      <td width="132px" style="padding-left:0px;"><b>Yearly Max $</b></td>
      <td><input type="text" class="textbox" id="custYearlyMax" name="custYearlyMax" value="<?=$custYearlyMax?>" style="width: 60px" /></td>
      <td width="10px">&nbsp;</td>
      <td width="70px"><b>Used $</b></td>
      <td><input type="text" class="textbox" id="custUsed" name="custUsed" value="<?=$custUsed?>" style="width: 60px" /></td>
      <td width="10px">&nbsp;</td>
      <td width="85px"><b>Deduct $</b></td>
      <td><input type="text" class="textbox" id="custDeduct" name="custDeduct" value="<?=$custDeduct?>" style="width: 60px" /></td>
      <td width="10px">&nbsp;</td>
      <td width="135px"><b>Family Deduct $</b></td>
      <td><input type="text" class="textbox" id="custFamilyDeduct" name="custFamilyDeduct" value="<?=$custFamilyDeduct?>" style="width: 60px" /></td>
      </tr>
    </table>  </td>
  </tr>
<tr>
  <td><b>Deductable Met</b></td>
  <td colspan="5">
  	<table cellpadding="1" cellspacing="0">
      <tr>
      	<td><input <? if($custMet == "Yes"){?>checked<? } ?> type="radio" name="custMet" value="Yes" /></td>
        <td>Yes</td>
        <td width="20px">&nbsp;</td>
        <td><input <? if($custMet == "No"){?>checked<? } ?> type="radio" name="custMet" value="No" /></td>
        <td>No</td>
        <Td width="14px">&nbsp;</Td>
      </tr>
    </table>  </td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
</tr>
<tr class="alternate">
  <td colspan="8">
  	<table cellpadding="1" cellspacing="0" border="0">
    <tr>
    <td nowrap="nowrap"><b>Prev</b></td>
    <td width="15px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custPrev" name="custPrev" value="<?=$custPrev?>" style="width: 60px" />%</td>
    <td width="25px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>Basic</b></td>
    <td width="15px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custBasic" name="custBasic" value="<?=$custBasic?>" style="width: 60px" />%</td>
    <td width="25px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>Major</b></td>
    <td width="15px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custMajor" name="custMajor" value="<?=$custMajor?>" style="width: 60px" />%</td>
    <td width="25px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>Perio</b></td>
    <td width="15px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custPerio" name="custPerio" value="<?=$custPerio?>" style="width: 60px" />%</td>
    <td width="25px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>Endo</b></td>
    <td width="15px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custEndo" name="custEndo" value="<?=$custEndo?>" style="width: 60px" />%</td>
    </tr>
    </table>  </td>
  </tr>
<tr>
  <td colspan="8">
  	<table cellpadding="1" cellspacing="0" border="0">
    <tr>
    <td nowrap="nowrap"><b>OS</b></td>
    <td width="26px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custOs" name="custOs" value="<?=$custOs?>" style="width: 60px" />%</td>
    <td width="25px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>OS To Med</b></td>
    <td width="15px">&nbsp;</td>
    <td><input <? if($custOsToMed == "Yes"){?>checked<? } ?> type="radio" name="custOsToMed" value="Yes" /></td>
    <td>Yes</td>
    <td width="20px">&nbsp;</td>
    <td><input <? if($custOsToMed == "No"){?>checked<? } ?> type="radio" name="custOsToMed" value="No" /></td>
    <td>No</td>
    </tr>
    </table>  </td>
</tr>
<tr class="alternate">
  <td><b>Deduct Applys:</b></td>
  <td colspan="7">
  	<table cellpadding="1" cellspacing="0" border="0">
    <tr>
    <td nowrap="nowrap"><b>Prev</b></td>
    <td width="15px">&nbsp;</td>
    <td><input <? if($custDAPrev == "Yes"){?>checked<? } ?> type="radio" name="custDAPrev" value="Yes" /></td>
    <td>Yes</td>
    <td width="10px">&nbsp;</td>
    <td><input <? if($custDAPrev == "No"){?>checked<? } ?> type="radio" name="custDAPrev" value="No" /></td>
    <td>No</td>
    <td width="20px"></td>
    <td nowrap="nowrap"><b>Basic</b></td>
    <td width="15px">&nbsp;</td>
    <td><input <? if($custDABasic == "Yes"){?>checked<? } ?> type="radio" name="custDABasic" value="Yes" /></td>
    <td>Yes</td>
    <td width="10px">&nbsp;</td>
    <td><input <? if($custDABasic == "No"){?>checked<? } ?> type="radio" name="custDABasic" value="No" /></td>
    <td>No</td>
    
    <td width="20px"></td>
    <td nowrap="nowrap"><b>Major</b></td>
    <td width="15px">&nbsp;</td>
    <td><input <? if($custDAMajor == "Yes"){?>checked<? } ?> type="radio" name="custDAMajor" value="Yes" /></td>
    <td>Yes</td>
    <td width="10px">&nbsp;</td>
    <td><input <? if($custDAMajor == "No"){?>checked<? } ?> type="radio" name="custDAMajor" value="No" /></td>
    <td>No</td>
    </tr>
    </table>  </td>
  </tr>
<tr class="alternate">
  <td>&nbsp;</td>
  <td colspan="7">
  	<table cellpadding="1" cellspacing="0" border="0">
    <tr>
    <td nowrap="nowrap"><b>Perio</b></td>
    <td width="10px">&nbsp;</td>
    <td><input <? if($custDAPerio == "Yes"){?>checked<? } ?> type="radio" name="custDAPerio" value="Yes" /></td>
    <td>Yes</td>
    <td width="10px">&nbsp;</td>
    <td><input <? if($custDAPerio == "No"){?>checked<? } ?> type="radio" name="custDAPerio" value="No" /></td>
    <td>No</td>
    <td width="20px"></td>
    <td nowrap="nowrap"><b>Endo</b></td>
    <td width="15px">&nbsp;</td>
    <td><input <? if($custDAEndo == "Yes"){?>checked<? } ?> type="radio" name="custDAEndo" value="Yes" /></td>
    <td>Yes</td>
    <td width="10px">&nbsp;</td>
    <td><input <? if($custDAEndo == "No"){?>checked<? } ?> type="radio" name="custDAEndo" value="No" /></td>
    <td>No</td>
    
    <td width="20px"></td>
    <td nowrap="nowrap"><b>OS</b></td>
    <td width="36px">&nbsp;</td>
    <td><input <? if($custDAOs == "Yes"){?>checked<? } ?> type="radio" name="custDAOs" value="Yes" /></td>
    <td>Yes</td>
    <td width="10px">&nbsp;</td>
    <td><input <? if($custDAOs == "No"){?>checked<? } ?> type="radio" name="custDAOs" value="No" /></td>
    <td>No</td>
    </tr>
    </table>  </td>
</tr>
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">PROCEDURE</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
<tr>
    <td width="130" nowrap="nowrap"><b>Prophy - D1110</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custProphy" name="custProphy" value="<?=$custProphy?>" style="width: 50px" />%</td>
    <td width="20px">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <select id="custProphyFreq" name="custProphyFreq" style="width: 80px">
    	<option value=""></option>
        <option value="2 x 12 Mo" <? if($custProphyFreq=="2 x 12 Mo"){ ?>Selected<? } ?>>2 x 12 Mo</option>
        <option value="1XCY" <? if($custProphyFreq=="1XCY"){ ?>Selected<? } ?>>1XCY</option>
        <option value="2XCY" <? if($custProphyFreq=="2XCY"){ ?>Selected<? } ?>>2XCY</option>
        <option value="1 x 6 Mo" <? if($custProphyFreq=="1 x 6 Mo"){ ?>Selected<? } ?>>1 x 6 Mo</option>
        <option value="1XFiscal Yr" <? if($custProphyFreq=="1XFiscal Yr"){ ?>Selected<? } ?>>1XFiscal Yr</option>
        <option value="2XFiscal Yr" <? if($custProphyFreq=="2XFiscal Yr"){ ?>Selected<? } ?>>2XFiscal Yr</option>
    </select>
    </td>
    <td width="20px">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td valign="top" width="20px"><input <? if($custProphyCyatTtd == "Cal Yr Any Time"){?>checked<? } ?> type="radio" name="custProphyCyatTtd" value="Cal Yr Any Time" /></td>
        <td valign="top" nowrap="nowrap"><b>Cal Yr Any Time</b></td>
        <td width="15px">&nbsp;</td>
        <td valign="top" width="20px"><input <? if($custProphyCyatTtd == "To The Date"){?>checked<? } ?> type="radio" name="custProphyCyatTtd" value="To The Date" /></td>
        <td valign="top" nowrap="nowrap"><b>To The Date</b></td>
        </tr>
        </table>
    </td>
    <td width="20px">&nbsp;</td>
    <td><b>History</b></td>
    <td><input type="text" class="textbox" id="custProphyHistory" name="custProphyHistory" value="<?=$custProphyHistory?>" style="width: 100px" /></td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Exams - D0150</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custExams" name="custExams" value="<?=$custExams?>" style="width: 50px" />%</td>
    <td width="20px">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <select id="custExamsFreq" name="custExamsFreq" style="width: 80px">
    	<option value=""></option>
        <option value="2 x 12 Mo" <? if($custExamsFreq=="2 x 12 Mo"){ ?>Selected<? } ?>>2 x 12 Mo</option>
        <option value="1XCY" <? if($custExamsFreq=="1XCY"){ ?>Selected<? } ?>>1XCY</option>
        <option value="2XCY" <? if($custExamsFreq=="2XCY"){ ?>Selected<? } ?>>2XCY</option>
        <option value="1 x 6 Mo" <? if($custExamsFreq=="1 x 6 Mo"){ ?>Selected<? } ?>>1 x 6 Mo</option>
        <option value="1XFiscal Yr" <? if($custExamsFreq=="1XFiscal Yr"){ ?>Selected<? } ?>>1XFiscal Yr</option>
        <option value="2XFiscal Yr" <? if($custExamsFreq=="2XFiscal Yr"){ ?>Selected<? } ?>>2XFiscal Yr</option>
    </select>
    </td>
    <td width="20px">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td nowrap="nowrap"><b>Share Freq</b></td>
        <td valign="top" width="20px"><input <? if($custExamShareFreq == "0120"){?>checked<? } ?> type="radio" name="custExamShareFreq" value="0120" /></td>
        <td valign="top" nowrap="nowrap">0120</td>
        <td width="15px">&nbsp;</td>
        <td valign="top" width="20px"><input <? if($custExamShareFreq == "0160"){?>checked<? } ?> type="radio" name="custExamShareFreq" value="0160" /></td>
        <td valign="top" nowrap="nowrap">0160</td>
        <td width="15px">&nbsp;</td>
        <td valign="top" width="20px"><input <? if($custExamShareFreq == "0180"){?>checked<? } ?> type="radio" name="custExamShareFreq" value="0180" /></td>
        <td valign="top" nowrap="nowrap">0180</td>
        </tr>
        </table>
    </td>
    <td width="20px">&nbsp;</td>
    <td><b>History</b></td>
    <td><input type="text" class="textbox" id="custExamHistory" name="custExamHistory" value="<?=$custExamHistory?>" style="width: 100px" /></td>
</tr>

<tr>
    <td nowrap="nowrap">&nbsp;</td>
    <td nowrap="nowrap">&nbsp;</td>
    <td width="20px">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td width="20px">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td nowrap="nowrap" ></td>
        <td valign="top" width="20px"><input <? if($custExamShareFreq2 == "0140"){?>checked<? } ?> type="radio" name="custExamShareFreq2" value="0140" /></td>
        <td valign="top" nowrap="nowrap">0140</td>
        <td width="15px">&nbsp;</td>
        <td valign="top"><input <? if($custExamShareFreqYN == "Yes"){?>checked<? } ?> type="radio" name="custExamShareFreqYN" value="Yes" /></td>
        <td valign="top">Yes</td>
        <td width="10px">&nbsp;</td>
        <td valign="top"><input <? if($custExamShareFreqYN == "No"){?>checked<? } ?> type="radio" name="custExamShareFreqYN" value="No" /></td>
        <td valign="top">No</td>
        <td>&nbsp;</td>
        <td valign="top"><b>Detail</b></td>
        <td>&nbsp;</td>
        <td valign="top"><input type="text" class="textbox" id="custExamShareFreqDet" name="custExamShareFreqDet" value="<?=$custExamShareFreqDet?>" style="width: 100px" /></td>
        </tr>
        </table>
    </td>
    <td width="20px">&nbsp;</td>
    <td></td>
    <td></td>
</tr>

<tr>
    <td nowrap="nowrap"><b>BWX - D0274</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custBwx" name="custBwx" value="<?=$custBwx?>" style="width: 50px" />%</td>
    <td width="20px">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <select id="custBwxFreq" name="custBwxFreq" style="width: 80px">
    	<option value=""></option>
        <option value="1 x 12 Mo" <? if($custBwxFreq=="1 x 12 Mo"){ ?>Selected<? } ?>>1 x 12 Mo</option>
        <option value="2 x 12 Mo" <? if($custBwxFreq=="2 x 12 Mo"){ ?>Selected<? } ?>>2 x 12 Mo</option>
        <option value="1XCY" <? if($custBwxFreq=="1XCY"){ ?>Selected<? } ?>>1XCY</option>
        <option value="2XCY" <? if($custBwxFreq=="2XCY"){ ?>Selected<? } ?>>2XCY</option>
        <option value="1 x 6 Mo" <? if($custBwxFreq=="1 x 6 Mo"){ ?>Selected<? } ?>>1 x 6 Mo</option>
        <option value="1XFiscal Yr" <? if($custBwxFreq=="1XFiscal Yr"){ ?>Selected<? } ?>>1XFiscal Yr</option>
        <option value="2XFiscal Yr" <? if($custBwxFreq=="2XFiscal Yr"){ ?>Selected<? } ?>>2XFiscal Yr</option>
    </select>
    </td>
    <td width="20px">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td nowrap="nowrap"><b>OK W/PA's</b></td>
        <td><input <? if($custBwxOkWPa == "Yes"){?>checked<? } ?> type="radio" name="custBwxOkWPa" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custBwxOkWPa == "No"){?>checked<? } ?> type="radio" name="custBwxOkWPa" value="No" /></td>
        <td>No</td>
        <td>&nbsp;</td>
        <td nowrap="nowrap"><b>W/Pano</b></td>
        <td><input <? if($custBwxWPano == "Yes"){?>checked<? } ?> type="radio" name="custBwxWPano" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custBwxWPano == "No"){?>checked<? } ?> type="radio" name="custBwxWPano" value="No" /></td>
        <td>No</td>
        </tr>
        </table>
    </td>
    <td width="20px">&nbsp;</td>
    <td><b>History</b></td>
    <td><input type="text" class="textbox" id="custBwxHistory" name="custBwxHistory" value="<?=$custBwxHistory?>" style="width: 100px" /></td>
</tr>
<tr>
    <td nowrap="nowrap"><b>FMX/Pano - D0210/D0330</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custFmx" name="custFmx" value="<?=$custFmx?>" style="width: 50px" />%</td>
    <td width="20px">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custFmxFreq" name="custFmxFreq" value="<?=$custFmxFreq?>" style="width: 75px" />
    </td>
    <td width="20px">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td nowrap="nowrap"><b>Shares</b></td>
        <td><input <? if($custFmxShares == "Yes"){?>checked<? } ?> type="radio" name="custFmxShares" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custFmxShares == "No"){?>checked<? } ?> type="radio" name="custFmxShares" value="No" /></td>
        <td>No</td>
        <td>&nbsp;</td>
        <td nowrap="nowrap"><b>OK/W 4 PA'S</b></td>
        <td><input <? if($custFmxOkWPa == "Yes"){?>checked<? } ?> type="radio" name="custFmxOkWPa" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custFmxOkWPa == "No"){?>checked<? } ?> type="radio" name="custFmxOkWPa" value="No" /></td>
        <td>No</td>
        </tr>
        </table>
    </td>
    <td width="20px">&nbsp;</td>
    <td><b>History</b></td>
    <td><input type="text" class="textbox" id="custFmxHistory" name="custFmxHistory" value="<?=$custFmxHistory?>" style="width: 100px" /></td>
</tr>
<tr>
    <td nowrap="nowrap"><b>PA's - D0220-D0230 </b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custPa" name="custPa" value="<?=$custPa?>" style="width: 50px" />%</td>
    <td width="20px">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custPaFreq" name="custPaFreq" value="<?=$custPaFreq?>" style="width: 75px" />
    </td>
    <td width="20px">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td nowrap="nowrap"><b>OK W/P</b></td>
        <td><input <? if($custPaOkWPano == "Yes"){?>checked<? } ?> type="radio" name="custPaOkWPano" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custPaOkWPano == "No"){?>checked<? } ?> type="radio" name="custPaOkWPano" value="No" /></td>
        <td>No</td>
        <td>&nbsp;</td>
        <td nowrap="nowrap"><b>D TO FMX</b></td>
        <td><input <? if($custPaDTFmx == "Yes"){?>checked<? } ?> type="radio" name="custPaDTFmx" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custPaDTFmx == "No"){?>checked<? } ?> type="radio" name="custPaDTFmx" value="No" /></td>
        <td>No</td>
        </tr>
        </table>
    </td>
    <td width="20px">&nbsp;</td>
    <td><b>History</b></td>
    <td><input type="text" class="textbox" id="custPaHistory" name="custPaHistory" value="<?=$custPaHistory?>" style="width: 100px" /></td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Sealants - D1351</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSealants" name="custSealants" value="<?=$custSealants?>" style="width: 50px" />%</td>
    <td width="20px">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custSealantsFreq" name="custSealantsFreq" value="<?=$custSealantsFreq?>" style="width: 75px" />
    </td>
    <td width="20px">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td width="30px"><b>Age</b></td>
        <td><input type="text" class="textbox" id="custSealantsAge" name="custSealantsAge" value="<?=$custSealantsAge?>" style="width: 30px" /></td>
        <td width="30px">&nbsp;</td>
        <td><input <? if($custSealantsMolars == "Yes"){?>checked<? } ?> type="checkbox" name="custSealantsMolars" value="Yes" /></td>
        <td nowrap="nowrap"><b>Molars</b></td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custSealantsPreMolars == "Yes"){?>checked<? } ?> type="checkbox" name="custSealantsPreMolars" value="Yes" /></td>
        <td nowrap="nowrap"><b>Pre Molars</b></td>
        </tr>
        </table>
    </td>
    <td width="20px">&nbsp;</td>
    <td><b>History</b></td>
    <td><input type="text" class="textbox" id="custSealantsHistory" name="custSealantsHistory" value="<?=$custSealantsHistory?>" style="width: 100px" /></td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Fluoride - D1208</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custFlouride" name="custFlouride" value="<?=$custFlouride?>" style="width: 50px" />%</td>
    <td width="20px">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <select id="custFlourideFreq" name="custFlourideFreq" style="width: 80px">
    	<option value=""></option>
        <option value="1 x 12 Mo" <? if($custFlourideFreq=="1 x 12 Mo"){ ?>Selected<? } ?>>1 x 12 Mo</option>
        <option value="2 x 12 Mo" <? if($custFlourideFreq=="2 x 12 Mo"){ ?>Selected<? } ?>>2 x 12 Mo</option>
        <option value="1XCY" <? if($custFlourideFreq=="1XCY"){ ?>Selected<? } ?>>1XCY</option>
        <option value="2XCY" <? if($custFlourideFreq=="2XCY"){ ?>Selected<? } ?>>2XCY</option>
        <option value="1 x 6 Mo" <? if($custFlourideFreq=="1 x 6 Mo"){ ?>Selected<? } ?>>1 x 6 Mo</option>
        <option value="1XFiscal Yr" <? if($custFlourideFreq=="1XFiscal Yr"){ ?>Selected<? } ?>>1XFiscal Yr</option>
        <option value="2XFiscal Yr" <? if($custFlourideFreq=="2XFiscal Yr"){ ?>Selected<? } ?>>2XFiscal Yr</option>
    </select>
    </td>
    <td width="20px">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td width="30px"><b>Age</b></td>
        <td><input type="text" class="textbox" id="custFlourideAge" name="custFlourideAge" value="<?=$custFlourideAge?>" style="width: 30px" /></td>
        </tr>
        </table>
    </td>
    <td width="20px">&nbsp;</td>
    <td><b>History</b></td>
    <td><input type="text" class="textbox" id="custFlourideHistory" name="custFlourideHistory" value="<?=$custFlourideHistory?>" style="width: 100px" /></td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Icons(PRR) - D1352</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custIcons" name="custIcons" value="<?=$custIcons?>" style="width: 50px" />%</td>
    <td width="20px">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custIconsFreq" name="custIconsFreq" value="<?=$custIconsFreq?>" style="width: 75px" />
    </td>
    <td width="20px">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td width="30px"><b>Age</b></td>
        <td><input type="text" class="textbox" id="custIconsAge" name="custIconsAge" value="<?=$custIconsAge?>" style="width: 30px" /></td>
        </tr>
        </table>
    </td>
    <td width="20px">&nbsp;</td>
    <td><b>History</b></td>
    <td><input type="text" class="textbox" id="custIconsHistory" name="custIconsHistory" value="<?=$custIconsHistory?>" style="width: 100px" /></td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Vizilite(OCS) - D0431</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custVizilite" name="custVizilite" value="<?=$custVizilite?>" style="width: 50px" />%</td>
    <td width="20px">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custViziliteFreq" name="custViziliteFreq" value="<?=$custViziliteFreq?>" style="width: 75px" />
    </td>
    <td width="20px">&nbsp;</td>
    <td>
    	
    </td>
    <td width="20px">&nbsp;</td>
    <td><b>History</b></td>
    <td><input type="text" class="textbox" id="custViziliteHistory" name="custViziliteHistory" value="<?=$custViziliteHistory?>" style="width: 100px" /></td>
</tr>
<tr>
    <td nowrap="nowrap"><b>SRP - D4341/D4342(1-3)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSpr" name="custSpr" value="<?=$custSpr?>" style="width: 50px" />%</td>
    <td width="20px">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custSprFreq" name="custSprFreq" value="<?=$custSprFreq?>" style="width: 75px" />
    </td>
    <td width="20px">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td valign="top" nowrap="nowrap"><b>Detail</b>&nbsp;&nbsp;<input type="text" class="textbox" id="custSprDet" name="custSprDet" value="<?=$custSprDet?>" style="width: 100px" /></td>
        <td width="10px">&nbsp;</td>
        <td nowrap="nowrap"><b>4 QUADS SAME DAY </b></td>
        <td><input <? if($custSprQSD == "Yes"){?>checked<? } ?> type="radio" name="custSprQSD" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custSprQSD == "No"){?>checked<? } ?> type="radio" name="custSprQSD" value="No" /></td>
        <td>No</td>
        </tr>
        </table>
    </td>
    <td width="20px">&nbsp;</td>
    <td><b>History</b></td>
    <td><input type="text" class="textbox" id="custSprHistory" name="custSprHistory" value="<?=$custSprHistory?>" style="width: 100px" /></td>
</tr>
<tr>
    <td nowrap="nowrap"><b>PERIO MAIN - D4910</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custPerioMain" name="custPerioMain" value="<?=$custPerioMain?>" style="width: 50px" />%</td>
    <td width="20px">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custPerioMainFreq" name="custPerioMainFreq" value="<?=$custPerioMainFreq?>" style="width: 75px" />
    </td>
    <td width="20px">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td valign="top" nowrap="nowrap"><b>Detail</b>&nbsp;&nbsp;<input type="text" class="textbox" id="custPerioMainDet" name="custPerioMainDet" value="<?=$custPerioMainDet?>" style="width: 100px" /></td>
        <td width="10px">&nbsp;</td>
        <td valign="top" width="20px"><input <? if($custPerioMainEoIa == "Either Or"){?>checked<? } ?> type="radio" name="custPerioMainEoIa" value="Either Or" /></td>
        <td valign="top" nowrap="nowrap"><b>Either Or</b></td>
        <td width="15px">&nbsp;</td>
        <td valign="top" width="20px"><input <? if($custPerioMainEoIa == "In Addition"){?>checked<? } ?> type="radio" name="custPerioMainEoIa" value="In Addition" /></td>
        <td valign="top" nowrap="nowrap"><b>In Addition</b></td>
        </tr>
        </table>
    </td>
    <td width="20px">&nbsp;</td>
    <td><b>History</b></td>
    <td><input type="text" class="textbox" id="custPerioMainHistory" name="custPerioMainHistory" value="<?=$custPerioMainHistory?>" style="width: 100px" /></td>
</tr>
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">CROWNS</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
<tr>
    <td width="130" nowrap="nowrap"><b>CROWNS - D2740</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custCrowns" name="custCrowns" value="<?=$custCrowns?>" style="width: 50px" />%</td>
    <td width="10px">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custCrownsFreq" name="custCrownsFreq" value="<?=$custCrownsFreq?>" style="width: 60px" />    </td>
    <td width="10px">&nbsp;</td>
    <td valign="top" nowrap="nowrap"><b>Detail</b>&nbsp;&nbsp;<input type="text" class="textbox" id="custCrownsDet" name="custCrownsDet" value="<?=$custCrownsDet?>" style="width: 100px" /></td>
    <td width="10px">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td nowrap="nowrap"><b>Pain On</b></td>
        <td><input <? if($custCrownsPaidOn == "Prep"){?>checked<? } ?> type="radio" name="custCrownsPaidOn" value="Prep" /></td>
        <td>Prep</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custCrownsPaidOn == "Seat"){?>checked<? } ?> type="radio" name="custCrownsPaidOn" value="Seat" /></td>
        <td>Seat</td>
        <td width="20px">&nbsp;</td>
        <td nowrap="nowrap"><b>Downgrad Post</b></td>
        <td><input <? if($custCrownsDowngradPost == "Yes"){?>checked<? } ?> type="radio" name="custCrownsDowngradPost" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custCrownsDowngradPost == "No"){?>checked<? } ?> type="radio" name="custCrownsDowngradPost" value="No" /></td>
        <td>No</td>
        </tr>
        </table>    </td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Build Up - D2950</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custBuildUp" name="custBuildUp" value="<?=$custBuildUp?>" style="width: 50px" />%</td>
    <td></td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custBuildUpFreq" name="custBuildUpFreq" value="<?=$custBuildUpFreq?>" style="width: 60px" />    </td>
    <td></td>
    <td valign="top"><b>Detail</b>&nbsp;&nbsp;<input type="text" class="textbox" id="custBuildUpDet" name="custBuildUpDet" value="<?=$custBuildUpDet?>" style="width: 100px" /></td>
    <td width="10px">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td nowrap="nowrap"><b>D2954 Paid If Done Same Day As Crown</b></td>
        <td><input <? if($custBuildUpPaid == "Yes"){?>checked<? } ?> type="radio" name="custBuildUpPaid" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custBuildUpPaid == "No"){?>checked<? } ?> type="radio" name="custBuildUpPaid" value="No" /></td>
        <td>No</td>
        </tr>
        </table>    </td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Crown Length. - D4249</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custCrownLen" name="custCrownLen" value="<?=$custCrownLen?>" style="width: 50px" />%</td>
    <td></td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custCrownLenFreq" name="custCrownLenFreq" value="<?=$custCrownLenFreq?>" style="width: 60px" />    </td>
    <td></td>
    <td valign="top"><b>Detail</b>&nbsp;&nbsp;<input type="text" class="textbox" id="custCrownLenDet" name="custCrownLenDet" value="<?=$custCrownLenDet?>" style="width: 100px" /></td>
    <td width="10px">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td nowrap="nowrap"><b>Paid If Done Same Day As Crown</b></td>
        <td><input <? if($custCrownLenPaid == "Yes"){?>checked<? } ?> type="radio" name="custCrownLenPaid" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custCrownLenPaid == "No"){?>checked<? } ?> type="radio" name="custCrownLenPaid" value="No" /></td>
        <td>No</td>
        </tr>
        </table>    </td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Pulp Cap</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custPulp" name="custPulp" value="<?=$custPulp?>" style="width: 50px" />%</td>
    <td></td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custPulpFreq" name="custPulpFreq" value="<?=$custPulpFreq?>" style="width: 60px" />    </td>
    <td></td>
    <td valign="top"><b>Detail</b>&nbsp;&nbsp;<input type="text" class="textbox" id="custPulpDet" name="custPulpDet" value="<?=$custPulpDet?>" style="width: 100px" /></td>
    <td width="10px">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td nowrap="nowrap"><b>D3110 (D)</b></td>
        <td><input <? if($custPulpD3110 == "Yes"){?>checked<? } ?> type="radio" name="custPulpD3110" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custPulpD3110 == "No"){?>checked<? } ?> type="radio" name="custPulpD3110" value="No" /></td>
        <td>No</td>
        <td>&nbsp;</td>
        <td nowrap="nowrap"><b>D3120(I)</b></td>
        <td><input <? if($custPulpD3120 == "Yes"){?>checked<? } ?> type="radio" name="custPulpD3120" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custPulpD3120 == "No"){?>checked<? } ?> type="radio" name="custPulpD3120" value="No" /></td>
        <td>No</td>
        </tr>
        </table>    </td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Replacement Clause</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custReplacement" name="custReplacement" value="<?=$custReplacement?>" style="width: 50px" />%</td>
    <td></td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custReplacementFreq" name="custReplacementFreq" value="<?=$custReplacementFreq?>" style="width: 60px" />    </td>
    <td></td>
    <td valign="top"><b>Detail</b>&nbsp;&nbsp;<input type="text" class="textbox" id="custReplacementDet" name="custReplacementDet" value="<?=$custReplacementDet?>" style="width: 100px" /></td>
    <td width="10px">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td><input <? if($custReplacementDenPar == "Dentures / Partials"){?>checked<? } ?> type="checkbox" name="custReplacementDenPar" value="Dentures / Partials" /></td>
        <td nowrap="nowrap"><b>Dentures / Partials</b></td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custReplacementCroImp == "Crowns / Implants"){?>checked<? } ?> type="checkbox" name="custReplacementCroImp" value="Crowns / Implants" /></td>
        <td nowrap="nowrap"><b>Crowns / Implants</b></td>
        </tr>
        </table>    </td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Composites</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custCompos" name="custCompos" value="<?=$custCompos?>" style="width: 50px" />%</td>
    <td></td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custComposFreq" name="custComposFreq" value="<?=$custComposFreq?>" style="width: 60px" />    </td>
    <td></td>
    <td valign="top"><b>Detail</b>&nbsp;&nbsp;<input type="text" class="textbox" id="custComposDet" name="custComposDet" value="<?=$custComposDet?>" style="width: 100px" /></td>
    <td width="10px">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td nowrap="nowrap"><b>Post Comp Downgrade</b></td>
        <td><input <? if($custComposDowngrade == "Yes"){?>checked<? } ?> type="radio" name="custComposDowngrade" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custComposDowngrade == "No"){?>checked<? } ?> type="radio" name="custComposDowngrade" value="No" /></td>
        <td>No</td>
        </tr>
        </table>    </td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Endo (RTC)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custEndoRtc" name="custEndoRtc" value="<?=$custEndoRtc?>" style="width: 50px" />%</td>
    <td></td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custEndoRtcFreq" name="custEndoRtcFreq" value="<?=$custEndoRtcFreq?>" style="width: 60px" />    </td>
    <td></td>
    <td valign="top"><b>Detail</b>&nbsp;&nbsp;<input type="text" class="textbox" id="custEndoRtcDet" name="custEndoRtcDet" value="<?=$custEndoRtcDet?>" style="width: 100px" /></td>
    <td width="10px">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td nowrap="nowrap"><b>Retreat</b></td>
        <td><input <? if($custEndoRtcRetreat == "Yes"){?>checked<? } ?> type="radio" name="custEndoRtcRetreat" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custEndoRtcRetreat == "No"){?>checked<? } ?> type="radio" name="custEndoRtcRetreat" value="No" /></td>
        <td>No</td>
        <td>&nbsp;</td>
        <td nowrap="nowrap"><b>D3346</b></td>
        <td><input <? if($custEndoRtcAnterior == "Yes"){?>checked<? } ?> type="radio" name="custEndoRtcAnterior" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custEndoRtcAnterior == "No"){?>checked<? } ?> type="radio" name="custEndoRtcAnterior" value="No" /></td>
        <td>No</td>
        <td>&nbsp;</td>
        <td nowrap="nowrap"><b>D3348</b></td>
        <td><input <? if($custEndoRtcPosterior == "Yes"){?>checked<? } ?> type="radio" name="custEndoRtcPosterior" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custEndoRtcPosterior == "No"){?>checked<? } ?> type="radio" name="custEndoRtcPosterior" value="No" /></td>
        <td>No</td>
        </tr>
        </table>    </td>
</tr>
<tr>
    <td width="130" nowrap="nowrap"><b>Occlusal Guard - D9940</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custOcclusal" name="custOcclusal" value="<?=$custOcclusal?>" style="width: 50px" />%</td>
    <td width="20px">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custOcclusalFreq" name="custOcclusalFreq" value="<?=$custOcclusalFreq?>" style="width: 60px" />
    </td>
    <td width="20px">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td nowrap="nowrap"><b>Detail</b>&nbsp;&nbsp;<input type="text" class="textbox" id="custOcclusalBruxism" name="custOcclusalBruxism" value="<?=$custOcclusalBruxism?>" style="width: 100px" /></td>
        <td>&nbsp;</td>
        </tr>
        </table>
    </td>
</tr>
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">OS / PERIO / IMPLANTS</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
  <tr>
    <td width="130" nowrap="nowrap"><b>Extractions - D7210</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custExtract" name="custExtract" value="<?=$custExtract?>" style="width: 50px" />
      %</td>
    <td width="20px">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custExtractFreq" name="custExtractFreq" value="<?=$custExtractFreq?>" style="width: 75px" /></td>
    <td width="10px">&nbsp;</td>
    <td valign="top" nowrap="nowrap"><b>Detail</b>&nbsp;&nbsp;<input type="text" class="textbox" id="custExtractDet" name="custExtractDet" value="<?=$custExtractDet?>" style="width: 100px" /></td>
    <td width="300px">&nbsp;</td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Oss Surg - D4261</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custOSS" name="custOSS" value="<?=$custOSS?>" style="width: 50px" /> %</td>
    <td width="20px">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custOSSFreq" name="custOSSFreq" value="<?=$custOSSFreq?>" style="width: 75px" /></td>
    <td width="10px">&nbsp;</td>
    <td valign="top" nowrap="nowrap"><b>Detail</b>&nbsp;&nbsp;<input type="text" class="textbox" id="custOSSDet" name="custOSSDet" value="<?=$custOSSDet?>" style="width: 100px" /></td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Bone Graft (Perio) - D4263</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custBone" name="custBone" value="<?=$custBone?>" style="width: 50px" />
      %</td>
    <td width="20px">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custBoneFreq" name="custBoneFreq" value="<?=$custBoneFreq?>" style="width: 75px" /></td>
    <td width="10px">&nbsp;</td>
    <td valign="top" nowrap="nowrap"><b>Detail</b>&nbsp;&nbsp;<input type="text" class="textbox" id="custBoneDet" name="custBoneDet" value="<?=$custBoneDet?>" style="width: 100px" /></td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Implant (Placment) - D6010</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custImplant" name="custImplant" value="<?=$custImplant?>" style="width: 50px" />
      %</td>
    <td width="20px">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custImplantFreq" name="custImplantFreq" value="<?=$custImplantFreq?>" style="width: 75px" /></td>
    <td width="10px">&nbsp;</td>
    <td valign="top" nowrap="nowrap"><b>Detail</b>&nbsp;&nbsp;<input type="text" class="textbox" id="custImplantDet" name="custImplantDet" value="<?=$custImplantDet?>" style="width: 100px" /></td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Imp Abutment Cust - D6057</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custImpAbutment" name="custImpAbutment" value="<?=$custImpAbutment?>" style="width: 50px" />
      %</td>
    <td width="20px">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custImpAbutmentFreq" name="custImpAbutmentFreq" value="<?=$custImpAbutmentFreq?>" style="width: 75px" /></td>
    <td width="10px">&nbsp;</td>
    <td valign="top" nowrap="nowrap"><b>Detail</b>&nbsp;&nbsp;<input type="text" class="textbox" id="custImpAbutmentDet" name="custImpAbutmentDet" value="<?=$custImpAbutmentDet?>" style="width: 100px" /></td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Imp Crown (PCN) - D6058</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custImpCrown" name="custImpCrown" value="<?=$custImpCrown?>" style="width: 50px" />
      %</td>
    <td width="20px">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custImpCrownFreq" name="custImpCrownFreq" value="<?=$custImpCrownFreq?>" style="width: 75px" /></td>
    <td width="10px">&nbsp;</td>
    <td valign="top" nowrap="nowrap"><b>Detail</b>&nbsp;&nbsp;<input type="text" class="textbox" id="custImpCrownDet" name="custImpCrownDet" value="<?=$custImpCrownDet?>" style="width: 100px" /></td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Imp Crown (PFM) - D6059</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custImpCrown2" name="custImpCrown2" value="<?=$custImpCrown2?>" style="width: 50px" />
      %</td>
    <td width="20px">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custImpCrown2Freq" name="custImpCrown2Freq" value="<?=$custImpCrown2Freq?>" style="width: 75px" /></td>
    <td width="10px">&nbsp;</td>
    <td valign="top" nowrap="nowrap"><b>Detail</b>&nbsp;&nbsp;<input type="text" class="textbox" id="custImpCrown2Det" name="custImpCrown2Det" value="<?=$custImpCrown2Det?>" style="width: 100px" /></td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Imp Ridg Preserv - D7953</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custImpRidg" name="custImpRidg" value="<?=$custImpRidg?>" style="width: 50px" />
      %</td>
    <td width="20px">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custImpRidgFreq" name="custImpRidgFreq" value="<?=$custImpRidgFreq?>" style="width: 75px" /></td>
    <td width="10px">&nbsp;</td>
    <td valign="top" nowrap="nowrap"><b>Detail</b>&nbsp;&nbsp;<input type="text" class="textbox" id="custImpRidgDet" name="custImpRidgDet" value="<?=$custImpRidgDet?>" style="width: 100px" /></td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>IV Sedation D9241 D9242 D9610</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSedation" name="custSedation" value="<?=$custSedation?>" style="width: 50px" />
      %</td>
    <td width="20px">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custSedationFreq" name="custSedationFreq" value="<?=$custSedationFreq?>" style="width: 75px" /></td>
    <td width="10px">&nbsp;</td>
    <td valign="top" nowrap="nowrap"><b>Detail</b>&nbsp;&nbsp;<input type="text" class="textbox" id="custSedationDet" name="custSedationDet" value="<?=$custSedationDet?>" style="width: 100px" /></td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Allograft - D4275</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custAllograft" name="custAllograft" value="<?=$custAllograft?>" style="width: 50px" />
      %</td>
    <td width="20px">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custAllograftFreq" name="custAllograftFreq" value="<?=$custAllograftFreq?>" style="width: 75px" /></td>
    <td width="10px">&nbsp;</td>
    <td valign="top" nowrap="nowrap"><b>Detail</b>&nbsp;&nbsp;<input type="text" class="textbox" id="custAllograftDet" name="custAllograftDet" value="<?=$custAllograftDet?>" style="width: 100px" /></td>
  </tr>
</table>
<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">ORTHO</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
  <tr>
    <td width="130" nowrap="nowrap"><b>ORTHO D8080 / D8090</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custOrtho" name="custOrtho" value="<?=$custOrtho?>" style="width: 50px" /></td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td width="430">&nbsp;</td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Ortho Max $</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custOrthoMax" name="custOrthoMax" value="<?=$custOrthoMax?>" style="width: 50px" /></td>
    <td width="20">&nbsp;</td>
    <td><b>Deduct $</b></td>
    <td><input type="text" class="textbox" id="custOrthoDeduct" name="custOrthoDeduct" value="<?=$custOrthoDeduct?>" style="width: 75px" /></td>
    <td width="430">&nbsp;</td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Remaining $</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custOrthoRemaining" name="custOrthoRemaining" value="<?=$custOrthoRemaining?>" style="width: 50px" /></td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Age Limit</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custOrthoAgeLimit" name="custOrthoAgeLimit" value="<?=$custOrthoAgeLimit?>" style="width: 50px" /></td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Subscriber / Spouse Coverage</b></td>
    <td colspan="3" nowrap="nowrap"><table cellpadding="1" cellspacing="0" border="0">
      <tr>
        <td><input <? if($custOrthoSubscriber == "Yes"){?>checked<? } ?> type="radio" name="custOrthoSubscriber" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custOrthoSubscriber == "No"){?>checked<? } ?> type="radio" name="custOrthoSubscriber" value="No" /></td>
        <td>No</td>
      </tr>
    </table></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Payments Initial</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custOrthoPaymentsInitial" name="custOrthoPaymentsInitial" value="<?=$custOrthoPaymentsInitial?>" style="width: 50px" />
      %</td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Claims Sent</b></td>
    <td colspan="4" nowrap="nowrap">
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td valign="top" width="20px"><input <? if($custOrthoClaimsSent == "Quarterly"){?>checked<? } ?> type="radio" name="custOrthoClaimsSent" value="Quarterly" /></td>
        <td valign="top" nowrap="nowrap">Quarterly</td>
        <td width="15px">&nbsp;</td>
        <td valign="top" width="20px"><input <? if($custOrthoClaimsSent == "Semi-Annually"){?>checked<? } ?> type="radio" name="custOrthoClaimsSent" value="Semi-Annually" /></td>
        <td valign="top" nowrap="nowrap">Semi-Annually</td>
        <td width="15px">&nbsp;</td>
        <td valign="top" width="20px"><input <? if($custOrthoClaimsSent == "Mon"){?>checked<? } ?> type="radio" name="custOrthoClaimsSent" value="Mon" /></td>
        <td valign="top" nowrap="nowrap">Mon</td>
        </tr>
        </table>    </td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Waiting Period</b></td>
    <td colspan="3" nowrap="nowrap">
    	<table cellpadding="1" cellspacing="0" border="0">
          <tr>
            <td><input <? if($custOrthoWaitingPeriod == "Yes"){?>checked<? } ?> type="radio" name="custOrthoWaitingPeriod" value="Yes" /></td>
            <td>Yes</td>
            <td width="10px">&nbsp;</td>
            <td><input <? if($custOrthoWaitingPeriod == "No"){?>checked<? } ?> type="radio" name="custOrthoWaitingPeriod" value="No" /></td>
            <td>No</td>
          </tr>
        </table>    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>How Payment Are Sent</b></td>
    <td colspan="4" nowrap="nowrap">
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td valign="top" width="20px"><input <? if($custOrthoHowPayment == "Auto"){?>checked<? } ?> type="radio" name="custOrthoHowPayment" value="Auto" /></td>
        <td valign="top" nowrap="nowrap">Auto</td>
        <td width="15px">&nbsp;</td>
        <td valign="top" width="20px"><input <? if($custOrthoHowPayment == "Billing Required"){?>checked<? } ?> type="radio" name="custOrthoHowPayment" value="Billing Required" /></td>
        <td valign="top" nowrap="nowrap">Billing Required</td>
        </tr>
        </table>    </td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Pretreat</b></td>
    <td colspan="3" nowrap="nowrap">
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td valign="top" width="20px"><input <? if($custOrthoPretreat == "Mandatory"){?>checked<? } ?> type="radio" name="custOrthoPretreat" value="Mandatory" /></td>
        <td valign="top" nowrap="nowrap">Mandatory</td>
        <td width="15px">&nbsp;</td>
        <td valign="top" width="20px"><input <? if($custOrthoPretreat == "Optional"){?>checked<? } ?> type="radio" name="custOrthoPretreat" value="Optional" /></td>
        <td valign="top" nowrap="nowrap">Optional</td>
        </tr>
        </table>    </td>
    <td>&nbsp;</td>
  </tr>
</table>

<br />

<?php
//$date = date_create(date('y-m-d'), timezone_open('Pacific/Nauru'));
$date = putenv("TZ=US/Pacific");
$pacific_time = date("h:i:s");
$todate = date("Y-m-d");

if($custDateTime == "")
{
	$custDateTime = $todate . " ". $pacific_time;
}
?>


  <table width="100%" cellpadding="5" cellspacing="0">
    <tr class="titleTr">
      <td colspan="4"><label id="rightLabel">Special Request</label></td>
    </tr>
  </table>
  <table cellpadding="3" cellspacing="5" width="100%" style="border:1px solid; background-color: #EFEFEF;">
    <tr>
      <td colspan="4"><b>Special Request</b></td>
    </tr>
    <tr>
      <td colspan="4">
      <textarea name="custSpecialRequest" id="custSpecialRequest" cols="100" rows="5"><?=$custSpecialRequest?></textarea>
      <script type="text/javascript">
	  CKEDITOR.replace( 'custSpecialRequest' );
	  </script>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td width="90px" valign="top"><b>Spoke with:</b></td>
      <td width="250px"><input type="text" class="textbox" id="custSpokeWith" name="custSpokeWith" value="<?=$custSpokeWith?>" style="width: 200px" /></td>
      <td width="140px" valign="top"><b>Account Executive:</b></td>
      <td><input type="text" class="textbox" id="custAccountExecutive" name="custAccountExecutive" value="<?=$custAccountExecutive?>" style="width: 200px" /></td>
    </tr>
    <tr>
      <td width="80px" valign="top"><b>Date/Time:</b></td>
      <td colspan="3"><input type="text" class="textbox" id="custDateTime" name="custDateTime" value="<?=date("m/d/Y h:i:s")?>" style="width: 200px" />
        <br />
        <p>Last Date / Time:
          <?=$custDateTime?>
        </p></td>
    </tr>
  </table>
</div>
