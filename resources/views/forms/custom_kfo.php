<div id="divCustom">

<table cellpadding="3" cellspacing="0" border="0">
<tr>
<td width="200"><b>Network Type</b></td>
<td>
<select id="custNetwork" name="custNetwork" size="1">
    <option value=""></option>
    <option value="In Network" <? if($custNetwork=="In Network"){ ?>Selected<? } ?>>In Network</option>
    <option value="Out Of Network" <? if($custNetwork=="Out Of Network"){ ?>Selected<? } ?>>Out Of Network</option>
    <option value="Policy Terminated" <? if($custNetwork=="Policy Terminated"){ ?>Selected<? } ?>>Policy Terminated</option>
</select></td>
<td></td>
<td>
    <table border="0" cellpadding="2" cellspacing="0">
    <tr>
        <td nowrap="nowrap"><b>Payer&nbsp;ID</b></td>
        <td>&nbsp;</td>
        <td><input type="text" class="textbox" id="custPayorId" name="custPayorId" value="<?=$custPayorId?>" style="width: 100px" /></td>
        <td width="50px">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td width="30">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td width="20px">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    </table></td>
</tr>

<tr class="alternate">
<td width="200"><b>Quick Note</b></td>
<td colspan="6"><input type="text" class="textbox" id="custQuickNote" name="custQuickNote" value="<?=$custQuickNote?>" style="width: 610px" maxlength="155" /></td>
<td></td>
</tr>

<tr>
<td width="200"><b>Effective Date</b></td>
<td colspan="2"><input type="text" class="textbox" id="custEffectiveDate" name="custEffectiveDate" value="<?=$custEffectiveDate?>" style="width: 60px" maxlength="10" /><input type="button" value="Select" class="smallButton" onclick="displayDatePicker('custEffectiveDate');" /></td>
<td colspan="5">
<table cellpadding="2" cellspacing="0">
  <tr>
  <td><b>Fee Schedule</b></td>
  <td width="30">&nbsp;</td>
  <td valign="top" width="20px"><input <? if($custFee == "PPO"){?>checked<? } ?> type="radio" name="custFee" value="PPO" /></td>
  <td valign="top">PPO</td>
  <td width="15px">&nbsp;</td>
  <td valign="top" width="20px"><input <? if($custFee == "UCR"){?>checked<? } ?> type="radio" name="custFee" value="UCR" /></td>
  <td valign="top">UCR</td>
  <td valign="top">&nbsp;</td>
  <td valign="top"><input type="text" class="textbox" id="custFeeName" name="custFeeName" value="<?=$custFeeName?>" /></td>
  </tr>
</table></td>
</tr>

<tr>
<td width="200"><b>Alternate ID</b></td>
<td><input type="text" class="textbox" id="custMemberId" name="custMemberId" value="<?=$custMemberId?>" style="width: 100px" /></td>
<td width="5"></td>
<td width="130">

<table cellpadding="2" cellspacing="0">
  <tr>
  <td nowrap="nowrap"><b>Group Name</b></td>
  <td width="30">&nbsp;</td>
  <td valign="top" width="20px"><input type="text" class="textbox" id="custGroup" name="custGroup" value="<?=$custGroup?>" style="width: 100px" /></td>
  <td width="30">&nbsp;</td>
  <?php 
	//Get group number from patient detail table
	$custGroupNum = getField('cui_patients', 'patientId', $patientId, 'patientGroup');
	$custGroupNum = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', encrypt_decrypt('decrypt', $custGroupNum));
  ?>
  <td nowrap="nowrap"><b>Group #</b></td>
  <td width="30">&nbsp;</td>
  <td valign="top" width="20px"><input type="text" class="textbox" id="custGroupNum" name="custGroupNum" value="<?=$custGroupNum?>" style="width: 100px" /></td>
  </tr>
</table></td>
<td width="50"></td>
<td></td>
</tr>
<tr class="alternate">
<td colspan="1"><b>Does this plan coordinate benefits</b></td>
<td colspan="4"><input type="text" class="textbox" id="custCoordinateBenefits" name="custCoordinateBenefits" value="<?=$custCoordinateBenefits?>" style="width:610px" /></td>
<td>&nbsp;</td>
</tr>

<tr class="">
    <td nowrap="nowrap"><b>Waiting Period</b></td>
    <td colspan="8">
        <table cellpadding="1" cellspacing="0">
            <tr>
                <td><input <? if($custWaitingPeriod == "Yes"){?>checked<? } ?> type="radio" name="custWaitingPeriod" value="Yes" /></td>
                <td>Yes</td>
                <td width="20px">&nbsp;</td>
                <td><input <? if($custWaitingPeriod == "No"){?>checked<? } ?> type="radio" name="custWaitingPeriod" value="No" /></td>
                <td>No</td>
                <Td width="20px">&nbsp;</Td>
                <td><b>Details</b></td>
                <td width="15px">&nbsp;</td>
                <td><input type="text" class="textbox" id="custWaitingPeriodDetail" name="custWaitingPeriodDetail" value="<?=$custWaitingPeriodDetail?>" style="width: 180px" /></Td>
            </tr>
        </table>
    </td>
</tr>

<tr class="alternate">
<td colspan="1"><b>Will you make payments directly to the provider?</b></td>
<td colspan="4"><input type="text" class="textbox" id="custDirectlyProvider" name="custDirectlyProvider" value="<?=$custDirectlyProvider?>" style="width:610px" /></td>
<td>&nbsp;</td>
</tr>

<tr class="">
<td colspan="1"><b>Are continuation of treatment forms needed?</b></td>
<td colspan="4"><input type="text" class="textbox" id="custTreatmentNeeded" name="custTreatmentNeeded" value="<?=$custTreatmentNeeded?>" style="width:610px" /></td>
<td>&nbsp;</td>
</tr>

<tr class="alternate">
<td colspan="1"><b>When calling Delta Do you pay on Premier?</b></td>
<td colspan="4"><input type="text" class="textbox" id="custDeltaPremier" name="custDeltaPremier" value="<?=$custDeltaPremier?>" style="width:610px" /></td>
<td>&nbsp;</td>
</tr>

<tr class="">
<td colspan="1"><b>Blue Cross Dental HMO Plan</b></td>
<td colspan="5">&nbsp;</td>
</tr>
<tr class="alternate">
	<td width="200"><u>4 Digit Contract Code:</u></td>
	<td><input type="text" class="textbox" id="custHMO4DigitContact" name="custHMO4DigitContact" value="<?=$custHMO4DigitContact?>" style="width: 150px" /></td>
	<td></td>
	<td>
		<table border="0" cellpadding="2" cellspacing="0">
		<tr>
			<td width="190"><u>Allowed Amount For:</u></td>
			<td>&nbsp;</td>
			<td><input type="text" class="textbox" id="custHMOAllowedAmountFor" name="custHMOAllowedAmountFor" value="<?=$custHMOAllowedAmountFor?>" style="width: 150px" /></td>
			<td width="50px">&nbsp;</td>
			<td nowrap="nowrap">&nbsp;</td>
			<td width="30">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td width="20px">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		</table>
	</td>
</tr>
<tr class="">
	<td width="200"><u>Copay:</u></td>
	<td><input type="text" class="textbox" id="custHMOCopay" name="custHMOCopay" value="<?=$custHMOCopay?>" style="width: 150px" /></td>
	<td></td>
	<td>
		<table border="0" cellpadding="2" cellspacing="0">
		<tr>
			<td width="190">Records:</td>
			<td>&nbsp;</td>
			<td><input type="text" class="textbox" id="custHMORecords" name="custHMORecords" value="<?=$custHMORecords?>" style="width: 150px" /></td>
			<td width="50px">&nbsp;</td>
			<td nowrap="nowrap">&nbsp;</td>
			<td width="30">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td width="20px">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		</table>
	</td>
</tr>
<tr class="alternate">
	<td width="200">-Adult</td>
	<td><input type="text" class="textbox" id="custHMOAdult" name="custHMOAdult" value="<?=$custHMOAdult?>" style="width: 150px" /></td>
	<td></td>
	<td>
		<table border="0" cellpadding="2" cellspacing="0">
		<tr>
			<td width="190">Retention:</td>
			<td>&nbsp;</td>
			<td><input type="text" class="textbox" id="custHMORetention" name="custHMORetention" value="<?=$custHMORetention?>" style="width: 150px" /></td>
			<td width="50px">&nbsp;</td>
			<td nowrap="nowrap">&nbsp;</td>
			<td width="30">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td width="20px">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		</table>
	</td>
</tr>
<tr class="">
	<td width="200">-Child</td>
	<td><input type="text" class="textbox" id="custHMOChild" name="custHMOChild" value="<?=$custHMOChild?>" style="width: 150px" /></td>
	<td></td>
	<td>
		<table border="0" cellpadding="2" cellspacing="0">
		<tr>
			<td width="190">To DR Amount:</td>
			<td>&nbsp;</td>
			<td><input type="text" class="textbox" id="custHMOToDRAmount" name="custHMOToDRAmount" value="<?=$custHMOToDRAmount?>" style="width: 150px" /></td>
			<td width="50px">&nbsp;</td>
			<td nowrap="nowrap">&nbsp;</td>
			<td width="30">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td width="20px">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		</table>
	</td>
</tr>
<tr class="alternate">
	<td width="200">&nbsp;</td>
	<td>&nbsp;</td>
	<td></td>
	<td>
		<table border="0" cellpadding="2" cellspacing="0">
		<tr>
			<td width="190">In Excess of 24MOS:</td>
			<td>&nbsp;</td>
			<td><input type="text" class="textbox" id="custHMOInExcess" name="custHMOInExcess" value="<?=$custHMOInExcess?>" style="width: 150px" /></td>
			<td width="50px">&nbsp;</td>
			<td nowrap="nowrap">&nbsp;</td>
			<td width="30">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td width="20px">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<br />

<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">Orthodontic</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
  <tr>
    <td colspan="7" nowrap="nowrap">
    	<table cellpadding="1" cellspacing="0">
    	  <tr>
    	    <td><input <? if($custOr == "Subscriber"){?>checked<? } ?> type="radio" name="custOr" value="Subscriber" /></td>
          <td>Subscriber</td>
          <td width="10px">&nbsp;</td>
          <td><input <? if($custOr == "Spouse"){?>checked<? } ?> type="radio" name="custOr" value="Spouse" /></td>
          <td>Spouse</td>
          <td width="10px">&nbsp;</td>
          <td><input <? if($custOr == "Dependent"){?>checked<? } ?> type="radio" name="custOr" value="Dependent" /></td>
          <td>Dependent</td>
          <td width="10px">&nbsp;</td>
          <td><input <? if($custOr == "All Covered"){?>checked<? } ?> type="radio" name="custOr" value="All Covered" /></td>
          <td>All Covered</td>
          </tr>
  	  </table></td>
    </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>Ortho</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custOrOrtho" name="custOrOrtho" value="<?=$custOrOrtho?>" style="width: 50px" />%</td>
    <td colspan="3">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="50" nowrap="nowrap"><b>To Age</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custOrAge" name="custOrAge" value="<?=$custOrAge?>" style="width: 75px" /></td>
            <td width="20px">&nbsp;</td>
            <td nowrap="nowrap"><b>Lifetime Max $</b></td>
            <td><input type="text" class="textbox" id="custOrLifetimeMax" name="custOrLifetimeMax" value="<?=$custOrLifetimeMax?>" style="width: 50px" /></td>
            <td width="20px">&nbsp;</td>
            <td nowrap="nowrap"><b>Remaining $</b></td>
            <td><input type="text" class="textbox" id="custOrRemaining" name="custOrRemaining" value="<?=$custOrRemaining?>" style="width: 50px" /></td>
            <td width="20px">&nbsp;</td>
            <td nowrap="nowrap"><b>Deductible $</b></td>
            <td><input type="text" class="textbox" id="custOrDeductible" name="custOrDeductible" value="<?=$custOrDeductible?>" style="width: 50px" /></td>
			<td width="20px">&nbsp;</td>
            <td nowrap="nowrap"><b>Ded Waived</b></td>
            <td><input type="text" class="textbox" id="custOrDedWaived" name="custOrDedWaived" value="<?=$custOrDedWaived?>" style="width: 50px" /></td>
        </tr>
        </table>    </td>
    <td colspan="2" valign="top" nowrap="nowrap">&nbsp;</td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>% paid at banding</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custOrPayment" name="custOrPayment" value="<?=$custOrPayment?>" style="width: 50px" />%</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="40" nowrap="nowrap"><b>Paid:</b></td>
            <td><input <? if($custOrPaidMonthly == "Monthly"){?>checked<? } ?> type="checkbox" name="custOrPaidMonthly" value="Monthly" /></td>
            <td nowrap="nowrap">Monthly</td>
            <td width="5px">&nbsp;</td>
            <td><input <? if($custOrPaidQuarterly == "Quarterly"){?>checked<? } ?> type="checkbox" name="custOrPaidQuarterly" value="Quarterly" /></td>
            <td nowrap="nowrap">Quarterly</td>
            <td width="5px">&nbsp;</td>
			<td><input <? if($custOrPaidSemiAnnual == "Semi-Annual"){?>checked<? } ?> type="checkbox" name="custOrPaidSemiAnnual" value="Semi-Annual" /></td>
            <td nowrap="nowrap">Semi-Annual</td>
            <td width="5px">&nbsp;</td>
            <td><input <? if($custOrPaidAnnual == "Annual"){?>checked<? } ?> type="checkbox" name="custOrPaidAnnual" value="Annual" /></td>
            <td nowrap="nowrap">Annual</td>
            <td width="10">&nbsp;</td>
			<td width="80" nowrap="nowrap"><b>Claims Paid:</b></td>
			<td><input <? if($custOrPaidAutomatic == "Automatic"){?>checked<? } ?> type="checkbox" name="custOrPaidAutomatic" value="Automatic" /></td>
            <td nowrap="nowrap">Automatic</td>
            <td width="5px">&nbsp;</td>
            <td><input <? if($custOrClaim == "Manually"){?>checked<? } ?> type="checkbox" name="custOrClaim" value="Manually" /></td>
            <td nowrap="nowrap">Manually</td>
          </tr>
        </table>
	</td>
  </tr>
  <tr>
    <td colspan="2"><b>Any prior history of benefits paid out?</b></td>
    <td colspan="5" nowrap="nowrap">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td><input type="text" class="textbox" id="custOrHistoryBenefits" name="custOrHistoryBenefits" value="<?=$custOrHistoryBenefits?>" style="width: 765px" /></td>
          </tr>
        </table>
    </td>
  </tr>
</table>

<br />

<?php
//$date = date_create(date('y-m-d'), timezone_open('Pacific/Nauru'));
$date = putenv("TZ=US/Pacific");
$pacific_time = date("h:i:s");
$todate = date("Y-m-d");

if($custDateTime == "")
{
	$custDateTime = $todate . " ". $pacific_time;
}
?>


  <table width="100%" cellpadding="5" cellspacing="0">
    <tr class="titleTr">
      <td colspan="4"><label id="rightLabel">Special Request</label></td>
    </tr>
  </table>
  <table cellpadding="3" cellspacing="5" width="100%" style="border:1px solid; background-color: #EFEFEF;">
    <tr>
      <td colspan="4"><b>Special Request</b></td>
    </tr>
    <tr>
      <td colspan="4">
      <textarea name="custSpecialRequest" id="custSpecialRequest" cols="100" rows="5"><?=$custSpecialRequest?></textarea>
      <script type="text/javascript">
	  CKEDITOR.replace( 'custSpecialRequest' );
	  </script>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td width="90px" valign="top"><b>Spoke with:</b></td>
      <td width="250px"><input type="text" class="textbox" id="custSpokeWith" name="custSpokeWith" value="<?=$custSpokeWith?>" style="width: 200px" /></td>
      <td width="140px" valign="top"><b>Account Executive:</b></td>
      <td><input type="text" class="textbox" id="custAccountExecutive" name="custAccountExecutive" value="<?=$custAccountExecutive?>" style="width: 200px" /></td>
    </tr>
    <tr>
      <td width="80px" valign="top"><b>Date/Time:</b></td>
      <td colspan="3"><input type="text" class="textbox" id="custDateTime" name="custDateTime" value="<?=date("m/d/Y h:i:s")?>" style="width: 200px" />
        <br />
        <p>Last Date / Time:
          <?=$custDateTime?>
        </p></td>
    </tr>
  </table>
</div>