<div id="div<?=$divCounter+=1?>" style="display: none;">

	<table cellpadding="3" cellspacing="0" width="100%">

	<tr>

		<td width="230px"><b>Oral Exam (0120)</b></td>

		<td><input type="radio" <? if($freOral == "1 x Yr"){ ?>checked<? } ?>  name="freOral" value="1 x Yr" /></td>

		<td>1 x Yr</td>

	</tr>

	<tr>

		<td>&nbsp;</td>

		<td><input type="radio" <? if($freOral == "2 x Yr"){ ?>checked<? } ?>  name="freOral" value="2 x Yr" /></td>

		<td>2 x Yr&nbsp;&nbsp;&nbsp;(if twice a year than)</td>

		<td width="15px">&nbsp;</td>

		<td><input type="radio" <? if($freOralTime == "Anytime"){ ?>checked<? } ?>  name="freOralTime" value="Anytime" /></td>

		<td>Anytime</td>

		<td width="5px">&nbsp;</td>

		<td><input type="radio" <? if($freOralTime == "Every 6 Months"){ ?>checked<? } ?>  name="freOralTime" value="Every 6 Months" /></td>

		<td>Every 6 M</td>

		<td width="5px">&nbsp;</td>

		<td><input type="radio" <? if($freOralTime == "Every 12 Months"){ ?>checked<? } ?>  name="freOralTime" value="Every 12 Months" /></td>

		<td>Every 12 M</td>

	</tr>

	<tr>

		<td>&nbsp;</td>

		<td><input type="radio" <? if($freOral == "Others"){ ?>checked<? } ?>  name="freOral" value="Others" /></td>

		<td>Others (If Others, please specify)</td>

		<td>&nbsp;</td>

		<td colspan="8"><input type="text" class="textbox" name="freOralOther" value="<?=$freOralOther?>" /></td>

	</tr>

	<tr class="alternate">

		<td><b>Problem focused exam (0140)</b></td>
		<td colspan="11">
			<table cellpadding="1" cellspacing="0">
				<tr>
					<td width="120px">Percentage (%)</td>
					<td><input type="text" class="textbox" name="freExamPercentage" value="<?=$freExamPercentage?>" /></td>
					<td width="20px"></td>
					<td width="120px">Frequency</td>
					<td><input type="text" class="textbox" name="freExamFrequency" value="<?=$freExamFrequency?>" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr class="alternate">
		<td>Does it count against freq of reg exam (0120)</td>
		<td colspan="11">

			<table cellpadding="1" cellspacing="0">

				<tr>

					<td valign="top">

						<table cellpadding="0" cellspacing="0">

							<tr>

								<td><input type="radio" <? if($freExam == "Yes"){ ?>checked<? } ?>  name="freExam" value="Yes" /></td>

								<td>Yes</td>

								<td width="15px">&nbsp;</td>

								<td><td><input type="radio" <? if($freExam == "No"){ ?>checked<? } ?>  name="freExam" value="No" /></td></td>

								<td>No</td>

							</tr>

						</table>

					</td>

				</tr>

			</table>

		</td>

	</tr>

	<tr>

		<td width="230px"><b>Prophy (01110 & 01120)</b></td>

		<td><input type="radio" <? if($freProphy == "1 x Yr"){ ?>checked<? } ?>  name="freProphy" value="1 x Yr" /></td>

		<td>1 x Yr</td>

	</tr>

	<tr>

		<td>&nbsp;</td>

		<td><input type="radio" <? if($freProphy == "2 x Yr"){ ?>checked<? } ?>  name="freProphy" value="2 x Yr" /></td>

		<td>2 x Yr&nbsp;&nbsp;&nbsp;(if twice a year than)</td>

		<td width="15px">&nbsp;</td>

		<td><input type="radio" <? if($freProphyTime == "Anytime"){ ?>checked<? } ?>  name="freProphyTime" value="Anytime" /></td>

		<td>Anytime</td>

		<td width="5px">&nbsp;</td>

		<td><input type="radio" <? if($freProphyTime == "Every 6 Months"){ ?>checked<? } ?>  name="freProphyTime" value="Every 6 Months" /></td>

		<td>Every 6 M</td>

		<td width="5px">&nbsp;</td>

		<td><input type="radio" <? if($freProphyTime == "Every 12 Months"){ ?>checked<? } ?>  name="freProphyTime" value="Every 12 Months" /></td>

		<td>Every 12 M</td>

	</tr>

	<tr>

		<td>&nbsp;</td>

		<td><input type="radio" <? if($freProphy == "Others"){ ?>checked<? } ?>  name="freProphy" value="Others" /></td>

		<td>Others (If Others, please specify)</td>

		<td>&nbsp;</td>

		<td colspan="8"><input type="text" class="textbox" name="freProphyOther" value="<?=$freProphyOther?>" /></td>

	</tr>

	<tr class="alternate">

		<td><b>Bitewings Children (0270)</b></td>

		<td colspan="11">

			<table cellpadding="0" cellspacing="0">

				<tr>

					<td valign="top">

						<table cellpadding="0" cellspacing="0">

							<tr>

								<td><input type="radio" <?if($freChildren == "Yes"){?>checked<? } ?>  name="freChildren" value="Yes" /></td>

								<td>Yes</td>

								<td width="15px">&nbsp;</td>

								<td><td><input type="radio" <?if($freChildren == "No"){?>checked<? } ?>  name="freChildren" value="No" /></td></td>

								<td>No</td>

								<td width="15px">&nbsp;</td>

								<td><b>If Yes</b></td>

								<td width="15px">&nbsp;</td>

								<td><input type="radio" <?if($freChildrenAge == "1 x Yr"){?>checked<? } ?>  name="freChildrenAge" value="1 x Yr" /></td>

								<td>1 x Yr</td>

								<td width="15px">&nbsp;</td>

								<td><input type="radio" <?if($freChildrenAge == "2 x Yr"){?>checked<? } ?>  name="freChildrenAge" value="2 x Yr" /></td>

								<td>2 x Yr</td>

								<td width="15px">&nbsp;</td>

								<td><input type="radio" <?if($freChildrenAge == "Unlimited"){?>checked<? } ?>  name="freChildrenAge" value="Unlimited" /></td>

								<td>Unlimited</td>

								<td width="15px">&nbsp;</td>

								<td><input type="radio" <?if($freChildrenAge == "Limit"){?>checked<? } ?>  name="freChildrenAge" value="Limit" /></td>

								<td>Age Limit</td>

								<td width="10px">&nbsp;</td>

								<td><input name="freChildrenAgeLimit" type="text" class="textbox" style="width: 30px" value="<?=$freChildrenAgeLimit?>" /></td>

							</tr>

						</table>

					</td>

				</tr>

			</table>

		</td>

	</tr>

	<tr>

		<td><b>Bitewings Adults (0270)</b></td>

		<td colspan="11">

			<table cellpadding="0" cellspacing="0">

				<tr>

					<td valign="top">

						<table cellpadding="0" cellspacing="0">

							<tr>

								<td><input type="radio" <?if($freAdult == "Yes"){?>checked<? } ?>  name="freAdult" value="Yes" /></td>

								<td>Yes</td>

								<td width="15px">&nbsp;</td>

								<td><td><input type="radio" <?if($freAdult == "No"){?>checked<? } ?>  name="freAdult" value="No" /></td></td>

								<td>No</td>

								<td width="15px">&nbsp;</td>

								<td><b>If Yes</b></td>

								<td width="15px">&nbsp;</td>

								<td><input type="radio" <?if($freAdultAge == "1 x Yr"){?>checked<? } ?>  name="freAdultAge" value="1 x Yr" /></td>

								<td>1 x Yr</td>

								<td width="15px">&nbsp;</td>

	<td><input type="radio" <?if($freAdultAge == "2 x Yr"){?>checked<? } ?>  name="freAdultAge" value="2 x Yr" /></td>

								<td>2 x Yr</td>

								<td width="15px">&nbsp;</td>

	<td><input type="radio" <?if($freAdultAge == "Unlimited"){?>checked<? } ?>  name="freAdultAge" value="Unlimited" /></td>

								<td>Unlimited</td>

							</tr>

						</table>

					</td>
				</tr>
			</table>
		</td>
	</tr>
	<? if($_SESSION["tmpSessionCompanyId"] == "17" || $_SESSION["tmpSessionCompanyId"] == "18"
	 || $_SESSION["tmpSessionCompanyId"] == "34" || $_SESSION["tmpSessionCompanyId"] == "35"
	 || $_SESSION["tmpSessionCompanyId"] == "36" || $_SESSION["tmpSessionCompanyId"] == "37"
	 || $_SESSION["tmpSessionCompanyId"] == "38" || $_SESSION["tmpSessionCompanyId"] == "39"
	 || $_SESSION["tmpSessionCompanyId"] == "40" || $_SESSION["tmpSessionCompanyId"] == "41"
	 || $_SESSION["tmpSessionCompanyId"] == "42" || $_SESSION["tmpSessionCompanyId"] == "43"
	 || $_SESSION["tmpSessionCompanyId"] == "44" || $_SESSION["tmpSessionCompanyId"] == "45"
	 || $_SESSION["tmpSessionCompanyId"] == "46" || $_SESSION["tmpSessionCompanyId"] == "47"
	 || $_SESSION["tmpSessionCompanyId"] == "50" || $_SESSION["tmpSessionCompanyId"] == "51"
	 || $_SESSION["tmpSessionCompanyId"] == "52" || $_SESSION["tmpSessionCompanyId"] == "53"
	 || $_SESSION["tmpSessionCompanyId"] == "54" || $_SESSION["tmpSessionCompanyId"] == "55"
	 || $_SESSION["tmpSessionCompanyId"] == "59" || $_SESSION["tmpSessionCompanyId"] == "60"
	 || $_SESSION["tmpSessionCompanyId"] == "61" || $_SESSION["tmpSessionCompanyId"] == "62"
	 || $_SESSION["tmpSessionCompanyId"] == "65" || $_SESSION["tmpSessionCompanyId"] == "66"
	 || $_SESSION["tmpSessionCompanyId"] == "67" || $_SESSION["tmpSessionCompanyId"] == "68"
	 || $_SESSION["tmpSessionCompanyId"] == "69" || $_SESSION["tmpSessionCompanyId"] == "70"
	 || $_SESSION["tmpSessionCompanyId"] == "71" || $_SESSION["tmpSessionCompanyId"] == "72"
	 || $_SESSION["tmpSessionCompanyId"] == "73" || $_SESSION["tmpSessionCompanyId"] == "74"
	 || $_SESSION["tmpSessionCompanyId"] == "75" || $_SESSION["tmpSessionCompanyId"] == "76"
	 || $_SESSION["tmpSessionCompanyId"] == "77" || $_SESSION["tmpSessionCompanyId"] == "78"
	 || $_SESSION["tmpSessionCompanyId"] == "111" || $_SESSION["tmpSessionCompanyId"] == "112"
	 ){ ?>
    <tr class="alternate">
		<td><b>Bitewings Other (0270)</b></td>
		<td colspan="11">
			<table cellpadding="1" cellspacing="0">
				<tr>
					<td><input type="text" class="textbox" style="width:200px;" name="freOther" value="<?=$freOther?>" /></td>
				</tr>
			</table>
		</td>
	</tr>
    <? } ?>
	<tr class="alternate">
		<td><b>Replacement fillings (for e.g. 02140)</b></td>
		<td colspan="11">
			<table cellpadding="0" cellspacing="0">

				<tr>

					<td valign="top">

						<table cellpadding="0" cellspacing="0">

							<tr>

								<td><input type="radio" <?if($freReplacement == "Yes"){?>checked<? } ?>  name="freReplacement" value="Yes" /></td>

								<td>Yes</td>

								<td width="15px">&nbsp;</td>

								<td><td><input type="radio" <?if($freReplacement == "No"){?>checked<? } ?>  name="freReplacement" value="No" /></td></td>

								<td>No</td>

								<td width="15px">&nbsp;</td>

								<td><b>If Yes, Every</b></td>

								<td width="15px">&nbsp;</td>

								<td><input name="freReplacementYear" type="text" class="textbox" style="width: 30px" value="<?=$freReplacementYear?>" /></td>

								<td width="10px">&nbsp;</td>

								<td><b>Yrs</b></td>

							</tr>

						</table>

					</td>

				</tr>

			</table>

		</td>

	</tr>

	<tr>

		<td><b>FMX (0210)</b></td>

		<td colspan="11">

			<table cellpadding="0" cellspacing="0">

				<tr>

					<td valign="top">

						<table cellpadding="0" cellspacing="0">

							<tr>

								<td><input type="radio" <?if($freFmx == "Yes"){?>checked<? } ?>  name="freFmx" value="Yes" /></td>

								<td>Yes</td>

								<td width="15px">&nbsp;</td>

								<td><td><input type="radio" <?if($freFmx == "No"){?>checked<? } ?>  name="freFmx" value="No" /></td></td>

								<td>No</td>

								<td width="15px">&nbsp;</td>

								<td><b>If Yes, Every</b></td>

								<td width="15px">&nbsp;</td>

								<td><input name="freFmxYear" type="text" class="textbox" style="width: 30px" value="<?=$freFmxYear?>" /></td>

								<td width="10px">&nbsp;</td>

								<td><b>Yrs</b></td>

							</tr>

						</table>

					</td>

				</tr>

			</table>

		</td>

	</tr>

	<tr class="alternate">

		<td><b>Panorex (0330)</b></td>

		<td colspan="11">

			<table cellpadding="0" cellspacing="0">

				<tr>

					<td valign="top">

						<table cellpadding="0" cellspacing="0">

							<tr>

								<td><input type="radio" <?if($frePanorex == "Yes"){?>checked<? } ?>  name="frePanorex" value="Yes" /></td>

								<td>Yes</td>

								<td width="15px">&nbsp;</td>

								<td><td><input type="radio" <?if($frePanorex == "No"){?>checked<? } ?>  name="frePanorex" value="No" /></td></td>

								<td>No</td>

								<td width="15px">&nbsp;</td>

								<td><b>If Yes, Every</b></td>

								<td width="15px">&nbsp;</td>

								<td><input name="frePanorexYear" type="text" class="textbox" style="width: 30px" value="<?=$frePanorexYear?>" /></td>

								<td width="10px">&nbsp;</td>

								<td><b>Yrs</b></td>

							</tr>

						</table>

					</td>

				</tr>

			</table>

		</td>

	</tr>

	<tr>

		<td><b>Is FMX separate from Panorex</b></td>

		<td colspan="11">

			<table cellpadding="0" cellspacing="0">

				<tr>

					<td valign="top">

						<table cellpadding="0" cellspacing="0">

							<tr>

								<td><input type="radio" <?if($freSeperate == "Yes"){?>checked<? } ?>  name="freSeperate" value="Yes" /></td>

								<td>Yes</td>

								<td width="15px">&nbsp;</td>

								<td><td><input type="radio" <?if($freSeperate == "No"){?>checked<? } ?>  name="freSeperate" value="No" /></td></td>

								<td>No</td>

								<td width="15px">&nbsp;</td>

								<td><b> If No, then any restrictions?</b></td>

								<td width="15px">&nbsp;</td>

								<td><textarea name="freSeperateYear" type="text" class="textbox" style="height: 30px"><?=$freSeperateYear?></textarea></td>

							</tr>

						</table>

					</td>

				</tr>

			</table>

		</td>

	</tr>

	<tr class="alternate">

		<td width="230px"><b>Fluoride (1208)</b></td>

		<td><input type="radio" <?if($freFlouride == "1 x Yr"){?>checked<? } ?>  name="freFlouride" value="1 x Yr" /></td>

		<td>1 x Yr</td>

	</tr>

	<tr class="alternate">

		<td>&nbsp;</td>

		<td><input type="radio" <?if($freFlouride == "2 x Yr"){?>checked<? } ?>  name="freFlouride" value="2 x Yr" /></td>

		<td>2 x Yr&nbsp;&nbsp;&nbsp;(if twice a year than)</td>

		<td width="15px">&nbsp;</td>

		<td><input type="radio" <?if($freFlourideYear == "Anytime"){?>checked<? } ?>  name="freFlourideYear" value="Anytime" /></td>

		<td>Anytime</td>

		<td width="5px">&nbsp;</td>

		<td><input type="radio" <?if($freFlourideYear == "Every 6 Months"){?>checked<? } ?>  name="freFlourideYear" value="Every 6 Months" /></td>

		<td>Every 6 M</td>

		<td width="5px">&nbsp;</td>

		<td><input type="radio" <?if($freFlourideTime == "Every 12 Months"){?>checked<? } ?>  name="freFlourideTime" value="Every 12 Months" /></td>

		<td>Every 12 M</td>

	</tr>

	<tr class="alternate">

		<td>&nbsp;</td>

		<td><input type="radio" <?if($freFlouride == "Limit"){?>checked<? } ?>  name="freFlouride" value="Limit" /></td>

		<td>Age Limit</td>

		<td width="10px">&nbsp;</td>

		<td><input name="freFlourideAgeLimit" type="text" class="textbox" style="width: 30px" value="<?=$freFlourideAgeLimit?>" /></td>

	</tr>

	<tr class="alternate">

		<td>&nbsp;</td>

		<td><input type="radio" <?if($freFlouride == "Others"){?>checked<? } ?>  name="freFlouride" value="Others" /></td>

		<td>Others (If Others, please specify)</td>

		<td>&nbsp;</td>

		<td colspan="8"><input type="text" class="textbox" name="freFlourideOther" value="<?=$freFlourideOther?>" /></td>

	</tr>

	

	<tr>

		<td><b>Sealants (1351)</b></td>

		<td colspan="11">

			<table cellpadding="1" cellspacing="0">

				<tr>

					<td><input type="radio" <?if($freSealants == "Percentage"){?>checked<? } ?>  name="freSealants" value="Percentage" /></td>

					<td><input type="text" name="freSealantsPer" class="textbox" style="width: 40px" value="<?=$freSealantsPer?>" /></td>

					<td>&nbsp;%</td>

					<td width="30px" align="center"><b>OR</b></td>								

					<td><input type="radio" <?if($freSealants == "Preventative"){?>checked<? } ?>  name="freSealants" value="Preventative" /></td>

					<td>Preventative</td>

					<td width="15px">&nbsp;</td>

					<td><input type="radio" <?if($freSealants == "Basic"){?>checked<? } ?>  name="freSealants" value="Basic" /></td>

					<td>Basic</td>

					<td width="15px">&nbsp;</td>

					<td><input type="radio" <?if($freSealants == "Not Covered"){?>checked<? } ?>  name="freSealants" value="Not Covered" /></td>

					<td>Not Covered</td>

				</tr>

				<tr>

					<td colspan="12">

						<table cellpadding="1" cellspacing="0">

							<tr>

								<td width="20px">&nbsp;</td>

								<td><b>Deductible Applied?</b></td>

								<td width="15px">&nbsp;</td>

								<td><input type="radio" <?if($freSealantsDedApp == "Yes"){?>checked<? } ?>  name="freSealantsDedApp" value="Yes" /></td>

								<td>Yes</td>

								<td width="15px">&nbsp;</td>

								<td><input type="radio" <?if($freSealantsDedApp == "No"){?>checked<? } ?>  name="freSealantsDedApp" value="No" /></td>

								<td>No</td>

							</tr>

							<tr>

								<td width="20px">&nbsp;</td>

								<td>Frequency</td>

								<td width="15px">&nbsp;</td>

								<td colspan="5"><input type="text" class="textbox" name="freSealantsFrequency" value="<?=$freSealantsFrequency?>" /></td>

							</tr>

						</table>

					</td>

				</tr>

			</table>		

		</td>

	</tr>			

	</table>