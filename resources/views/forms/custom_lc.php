<!-- Custom Longmont Dental Form -->
<div id="divCustom">
    <table cellpadding="3" cellspacing="0" border="0">
        <tr class="alternate">
            <td width="130"><b>Network Type</b></td>
            <td>
                <select id="custNetwork" name="custNetwork" size="1">
                    <option value=""></option>
                    <option value="In Network" <? if($custNetwork=="In Network"){ ?>Selected<? } ?>>In Network</option>
                    <option value="Out Of Network" <? if($custNetwork=="Out Of Network"){ ?>Selected<? } ?>>Out Of Network</option>
                    <option value="Policy Terminated" <? if($custNetwork=="Policy Terminated"){ ?>Selected<? } ?>>Policy Terminated</option>
                </select>
            </td>
            <td></td>
            <td>
                <table border="0" cellpadding="2" cellspacing="0">
                    <tr>
                        <td nowrap="nowrap"><b>Payer&nbsp;ID</b></td>
                        <td>&nbsp;</td>
                        <td><input type="text" class="textbox" id="custPayorId" name="custPayorId" value="<?=$custPayorId?>" style="width: 100px" /></td>
                        <td width="50px">&nbsp;</td>
                        <td nowrap="nowrap"><b>E-Attachments</b></td>
                        <td width="30">&nbsp;</td>
                        <td><input <? if($custAttachments == "Yes"){?>checked<? } ?> type="radio" name="custAttachments" value="Yes" /></td>
                        <td>Yes</td>
                        <td width="20px">&nbsp;</td>
                        <td><input <? if($custAttachments == "No"){?>checked<? } ?> type="radio" name="custAttachments" value="No" /></td>
                        <td>No</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="130"><b>Quick Note</b></td>
            <td colspan="6"><input type="text" class="textbox" id="custQuickNote" name="custQuickNote" value="<?=$custQuickNote?>" style="width: 610px" maxlength="155" /></td>
            <td></td>
        </tr>
        <tr class="alternate">
            <td width="130"><b>Effective Date</b></td>
            <td colspan="2"><input type="text" class="textbox" id="custEffectiveDate" name="custEffectiveDate" value="<?=$custEffectiveDate?>" style="width: 60px" maxlength="10" /><input type="button" value="Select" class="smallButton" onclick="displayDatePicker('custEffectiveDate');" /></td>
            <td colspan="5">
                <table cellpadding="2" cellspacing="0">
                    <tr>
                        <td><b>Fee Schedule</b></td>
                        <td width="30">&nbsp;</td>
                        <td valign="top" width="20px"><input <? if($custFee == "PPO"){?>checked<? } ?> type="radio" name="custFee" value="PPO" /></td>
                        <td valign="top">PPO</td>
                        <td width="15px">&nbsp;</td>
                        <td valign="top" width="20px"><input <? if($custFee == "UCR"){?>checked<? } ?> type="radio" name="custFee" value="UCR" /></td>
                        <td valign="top">UCR</td>
                        <td valign="top">&nbsp;</td>
                        <td valign="top"><input type="text" class="textbox" id="custFeeName" name="custFeeName" value="<?=$custFeeName?>" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="130"><b>ID</b></td>
            <td><input type="text" class="textbox" id="custMemberId" name="custMemberId" value="<?=$custMemberId?>" style="width: 100px" /></td>
            <td width="5"></td>
            <td width="130">
                <table cellpadding="2" cellspacing="0">
                    <tr>
                        <td nowrap="nowrap"><b>Group Name</b></td>
                        <td width="30">&nbsp;</td>
                        <td valign="top" width="20px"><input type="text" class="textbox" id="custGroup" name="custGroup" value="<?=$custGroup?>" style="width: 100px" /></td>
                    </tr>
                </table>
            </td>
            <td width="50"></td>
            <td></td>
        </tr>
        <tr class="alternate">
            <td><b>Benefits Run ?</b></td>
            <td colspan="7">
                <table cellpadding="1" cellspacing="0">
                    <tr>
                        <td width="20px"><input <? if($custBenefitsRun == "Calender"){?>checked<? } ?> type="radio" name="custBenefitsRun" value="Calender" /></td>
                        <td>Calendar</td>
                        <td width="15px">&nbsp;</td>
                        <td width="20px"><input <? if($custBenefitsRun == "Contract Year"){?>checked<? } ?> type="radio" name="custBenefitsRun" value="Contract Year" /></td>
                        <td>Contract Year</td>
                        <td width="5px">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="text" class="textbox" id="custBenefitsDate" name="custBenefitsDate" value="<?=$custBenefitsDate?>" style="width: 60px" maxlength="10" /><input type="button" value="Select" class="smallButton" onclick="displayDatePicker('custBenefitsDate');" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top"><b>MTC</b></td>
            <td colspan="6">
                <table cellpadding="1" cellspacing="0">
                    <tr>
                        <td><input <? if($custMtc == "Yes"){?>checked<? } ?> type="radio" name="custMtc" value="Yes" /></td>
                        <td>Yes</td>
                        <td width="20px">&nbsp;</td>
                        <td><input <? if($custMtc == "No"){?>checked<? } ?> type="radio" name="custMtc" value="No" /></td>
                        <td>No</td>
                        <td width="20px">&nbsp;</td>
                        <td><b>Details</b></td>
                        <td width="15px">&nbsp;</td>
                        <td><input type="text" class="textbox" id="custMtcDetail" name="custMtcDetail" value="<?=$custMtcDetail?>" style="width: 180px" /></td>
                    </tr>
                </table>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr class="alternate">
            <td nowrap="nowrap"><b>Waiting Period</b></td>
            <td colspan="7">
                <table cellpadding="1" cellspacing="0">
                    <tr>
                        <td><input <? if($custWaitingPeriod == "Yes"){?>checked<? } ?> type="radio" name="custWaitingPeriod" value="Yes" /></td>
                        <td>Yes</td>
                        <td width="20px">&nbsp;</td>
                        <td><input <? if($custWaitingPeriod == "No"){?>checked<? } ?> type="radio" name="custWaitingPeriod" value="No" /></td>
                        <td>No</td>
                        <td width="20px">&nbsp;</td>
                        <td><b>Details</b></td>
                        <td width="15px">&nbsp;</td>
                        <td><input type="text" class="textbox" id="custWaitingPeriodDetail" name="custWaitingPeriodDetail" value="<?=$custWaitingPeriodDetail?>" style="width: 180px" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="8">
                <table cellpadding="2" cellspacing="0">
                    <tr>
                        <td width="132px" style="padding-left:0px;"><b>Yearly Max $</b></td>
                        <td><input type="text" class="textbox" id="custYearlyMax" name="custYearlyMax" value="<?=$custYearlyMax?>" style="width: 60px" /></td>
                        <td width="10px">&nbsp;</td>
                        <td width="80px"><b>Applies to?</b></td>
                        <td><input <? if($custYearlyApplies1 == "P"){?>checked<? } ?> type="checkbox" name="custYearlyApplies1" value="P" /></td>
                        <td>P</td>
                        <td width="2px"></td>
                        <td><input <? if($custYearlyApplies2 == "B"){?>checked<? } ?> type="checkbox" name="custYearlyApplies2" value="B" /></td>
                        <td>B</td>
                        <td width="2px"></td>
                        <td><input <? if($custYearlyApplies3 == "M"){?>checked<? } ?> type="checkbox" name="custYearlyApplies3" value="M" /></td>
                        <td>M</td>
                        <td width="20px">&nbsp;</td>
                        <td width="50px"><b>Used $</b></td>
                        <td><input type="text" class="textbox" id="custUsed" name="custUsed" value="<?=$custUsed?>" style="width: 60px" /></td>
                        <td width="10px">&nbsp;</td>
                        <td width="90px"><b>Roll over? $</b></td>
                        <td><input type="text" class="textbox" id="custRollOver" name="custRollOver" value="<?=$custRollOver?>" style="width: 60px" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="alternate">
            <td colspan="8">
                <table cellpadding="2" cellspacing="0">
                    <tr>
                        <td width="132px" style="padding-left:0px;"><b>Ind. Deductible $</b></td>
                        <td><input type="text" class="textbox" id="custDeduct" name="custDeduct" value="<?=$custDeduct?>" style="width: 60px" /></td>
                        <td width="10px">&nbsp;</td>
                        <td width="80px"><b>Applies to?</b></td>
                        <td><input <? if($custDeductApplies1 == "P"){?>checked<? } ?> type="checkbox" name="custDeductApplies1" value="P" /></td>
                        <td>P</td>
                        <td width="2px"></td>
                        <td><input <? if($custDeductApplies2 == "D"){?>checked<? } ?> type="checkbox" name="custDeductApplies2" value="D" /></td>
                        <td>D</td>
                        <td width="2px"></td>
                        <td><input <? if($custDeductApplies3 == "B"){?>checked<? } ?> type="checkbox" name="custDeductApplies3" value="B" /></td>
                        <td>B</td>
                        <td width="2px"></td>
                        <td><input <? if($custDeductApplies4 == "M"){?>checked<? } ?> type="checkbox" name="custDeductApplies4" value="M" /></td>
                        <td>M</td>
                        <td width="20px">&nbsp;</td>
                        <td width="40px"><b>Met</b></td>
                        <td><input <? if($custMet == "Yes"){?>checked<? } ?> type="radio" name="custMet" value="Yes" /></td>
                        <td>Yes</td>
                        <td width="5px"></td>
                        <td><input <? if($custMet == "No"){?>checked<? } ?> type="radio" name="custMet" value="No" /></td>
                        <td>No</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="8">
                <table cellpadding="2" cellspacing="0">
                    <tr>
                        <td width="132px" style="padding-left:0px;"><b>Fam. Deductible $</b></td>
                        <td><input type="text" class="textbox" id="custFamilyDeduct" name="custFamilyDeduct" value="<?=$custFamilyDeduct?>" style="width: 60px" /></td>
                        <td width="10px">&nbsp;</td>
                        <td width="40px"><b>Met</b></td>
                        <td><input <? if($custMet2 == "Yes"){?>checked<? } ?> type="radio" name="custMet2" value="Yes" /></td>
                        <td>Yes</td>
                        <td width="5px"></td>
                        <td><input <? if($custMet2 == "No"){?>checked<? } ?> type="radio" name="custMet2" value="No" /></td>
                        <td>No</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="alternate">
            <td colspan="8">
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                    	<td nowrap="nowrap"><b>Prev</b></td>
                        <td width="5px">&nbsp;</td>
                        <td nowrap="nowrap"><input type="text" class="textbox" id="custPrev" name="custPrev" value="<?=$custPrev?>" style="width: 60px" />%</td>
                        <td width="15px">&nbsp;</td>
                    	<td nowrap="nowrap"><b>Basic</b></td>
                        <td width="5px">&nbsp;</td>
                        <td nowrap="nowrap"><input type="text" class="textbox" id="custBasic" name="custBasic" value="<?=$custBasic?>" style="width: 60px" />%</td>
                        <td width="15px">&nbsp;</td>
                        <td nowrap="nowrap"><b>Major</b></td>
                        <td width="5px">&nbsp;</td>
                        <td nowrap="nowrap"><input type="text" class="textbox" id="custMajor" name="custMajor" value="<?=$custMajor?>" style="width: 60px" />%</td>
                        <td width="15px">&nbsp;</td>
                        <td nowrap="nowrap"><b>Perio</b></td>
                        <td width="5px">&nbsp;</td>
                        <td nowrap="nowrap"><input type="text" class="textbox" id="custPerio" name="custPerio" value="<?=$custPerio?>" style="width: 60px" />%</td>
                        <td width="15px">&nbsp;</td>
                        <td nowrap="nowrap"><b>Endo</b></td>
                        <td width="5px">&nbsp;</td>
                        <td nowrap="nowrap"><input type="text" class="textbox" id="custEndo" name="custEndo" value="<?=$custEndo?>" style="width: 60px" />%</td>
                        <td width="15px">&nbsp;</td>
                        <td nowrap="nowrap"><b>OS</b></td>
                        <td width="5px">&nbsp;</td>
                        <td nowrap="nowrap"><input type="text" class="textbox" id="custOs" name="custOs" value="<?=$custOs?>" style="width: 60px" />%</td>
                        <td width="15px">&nbsp;</td>
                        <td nowrap="nowrap"><b>OS to Med</b></td>
                        <td width="5px">&nbsp;</td>
                        <td><input <? if($custOsToMed == "Yes"){?>checked<? } ?> type="radio" name="custOsToMed" value="Yes" /></td>
                        <td>Yes</td>
                        <td width="5px"></td>
                        <td><input <? if($custOsToMed == "No"){?>checked<? } ?> type="radio" name="custOsToMed" value="No" /></td>
                        <td>No</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table width="100%" cellpadding="5" cellspacing="0">
        <tr class="titleTr">
            <td><label id="rightLabel">Diagnostic & Preventative</label></td>
        </tr>
    </table>
    <table cellpadding="3" cellspacing="0" width="100%">
        <tr>
            <td nowrap="nowrap"><b>Limited Exam (0140)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custDPLimitedExam" name="custDPLimitedExam" value="<?=$custDPLimitedExam?>" style="width: 50px" />%</td>
            <td width="20">&nbsp;</td>
            <td><b>Freq</b></td>
            <td>
                <input type="text" class="textbox" id="custDPLimitedExamFreq" name="custDPLimitedExamFreq" value="<?=$custDPLimitedExamFreq?>" style="width: 75px" />    
            </td>
            <td width="10">&nbsp;</td>
            <td colspan="10">
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td nowrap="nowrap"><b>Separate from 0120?</b></td>
                        <td><input <? if($custDPSeparate == "Yes"){?>checked<? } ?> type="radio" name="custDPSeparate" value="Yes" /></td>
                        <td>Yes</td>
                        <td width="10px">&nbsp;</td>
                        <td><input <? if($custDPSeparate == "No"){?>checked<? } ?> type="radio" name="custDPSeparate" value="No" /></td>
                        <td>No</td>
                        <td>&nbsp;</td>
                        <td nowrap="nowrap"><b>Allowed same day w/ TX?</b></td>
                        <td><input <? if($custDPAllowed == "Yes"){?>checked<? } ?> type="radio" name="custDPAllowed" value="Yes" /></td>
                        <td>Yes</td>
                        <td width="10px">&nbsp;</td>
                        <td><input <? if($custDPAllowed == "No"){?>checked<? } ?> type="radio" name="custDPAllowed" value="No" /></td>
                        <td>No</td>
                       
                    </tr>
                </table>
            </td>
            <td>&nbsp;</td>
        </tr>
        <!-- Begin adding new fields 09 October 2015 (Muhammad Shoaib) -->
      
        <tr class="alternate">
            <td nowrap="nowrap"><b>Periapicals (0220-30)</b></td>
            <td><input type="text" class="textbox" name="custDPPeriapicals" id="custDPPeriapicals" value="<?=$custDPPeriapicals?>" style="width: 50px;">%
            </td>
            <td width="10">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" name="custDPPeriapicalsFreq" id="custDPPeriapicalsFreq" value="<?=$custDPPeriapicalsFreq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
            <td width="50px"><b>Max?</b></td>
            <td><input type="text" class="textbox" name="custDPPeriapicalsFreqMax" id="custDPPeriapicalsFreqMax" value="<?=$custDPPeriapicalsFreqMax?>" style="width: 60px;"></td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
			<td nowrap="nowrap"><b>FMX(0210)/Pano(0330)</b></td>
			<td nowrap="nowrap"><input type="text" class="textbox" id="custDPFMX" name="custDPFMX" value="<?=$custDPFMX?>" style="width: 50px" />%</td>
			<td width="20">&nbsp;</td>
			<td><b>Freq</b></td>
			<td>
				<input type="text" class="textbox" id="custDPFMXFreq" name="custDPFMXFreq" value="<?=$custDPFMXFreq?>" style="width: 75px" />
			</td>
			<td width="10">&nbsp;</td>
			<td>
				<table cellpadding="1" cellspacing="0" border="0">
					<tr>
						<td><input <? if($custDPShare == "Share"){?>checked<? } ?> type="radio" name="custDPShare" value="Share" /></td>
						<td nowrap="nowrap"><b>Share</b></td>
						<td><input <? if($custDPShare == "Separate"){?>checked<? } ?> type="radio" name="custDPShare" value="Separate" /></td>
						<td><b>Separate</b></td>
						<td width="30">&nbsp;</td>
						<!--<td nowrap="nowrap"><b>Does FMX take freq of BW</b></td>
						<td width="5"></td>
						<td><input type="text" class="textbox" id="custDPFreqBw" name="custDPFreqBw" value="<?=$custDPFreqBw?>" style="width: 100px" /></td>-->
					</tr>
				</table>
			</td>
			<td width="20">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr class="alternate">
        	<td nowrap="nowrap"><b>Re-Eval post op (0171)</b></td>
            <td><input type="text" class="textbox" name="custRe_Eval" id="custRe_Eval" value="<?=$custRe_Eval?>" style="width: 50px;">%
            </td>
            <td width="10">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" name="custRe_EvalFreq" id="custRe_EvalFreq" value="<?=$custRe_EvalFreq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
            
            <td nowrap="nowrap"><b>Missed Apt (9986)</b></td>
            <td><input type="text" class="textbox" name="custMissedApt" id="custMissedApt" value="<?=$custMissedApt?>" style="width: 50px;">%
            </td>
            <td width="10">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" name="custMissedAptFreq" id="custMissedAptFreq" value="<?=$custMissedAptFreq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
        </tr>
        
		<tr>
			<td nowrap="nowrap"><b>Cone Beam (D0364)</b></td>
			<td nowrap="nowrap"><input type="text" class="textbox" id="custDPConeBeam" name="custDPConeBeam" value="<?=$custDPConeBeam?>" style="width: 50px" />%</td>
			<td width="20">&nbsp;</td>    <td><b>Freq</b></td>
			<td><input type="text" class="textbox" id="custDPConeBeamFreq" name="custDPConeBeamFreq" value="<?=$custDPConeBeamFreq?>" style="width: 75px" /></td>
			<td width="10">&nbsp;</td>
			
			<td nowrap="nowrap"><b>Pulp Testing 0460</b></td>
            <td><input type="text" class="textbox" name="custPulpTesting" id="custPulpTesting" value="<?=$custPulpTesting?>" style="width: 50px;">%</td>
            <td width="10">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" name="custPulpTestingFreq" id="custPulpTestingFreq" value="<?=$custPulpTestingFreq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
			
			<!--<td>
				<table cellpadding="1" cellspacing="0" border="0">
					<tr>
						<td nowrap="nowrap"><b>To Age?</b></td>
						<td><input type="text" class="textbox" id="custDPConeBeamToAge" name="custDPConeBeamToAge" value="<?=$custDPConeBeamToAge?>" style="width: 30px" /></td>
						<td width="5"></td>
					</tr>
				</table>
			</td>
			<td width="20">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>-->
		</tr>
    </table>
    <!-- Begin adding new fields 09 October 2015 (Muhammad Shoaib) -->
    <table width="100%" cellpadding="5" cellspacing="0">
        <tr class="titleTr">
            <td><label id="rightLabel">Composites</label></td>
        </tr>
    </table>
    <table cellpadding="3" cellspacing="0" width="100%">
        <tr>
            <td nowrap="nowrap"><b>Posterior Composites (2391-94)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custComPosteriorComposites" name="custComPosteriorComposites" value="<?=$custComPosteriorComposites?>" style="width: 50px" />%</td>
            <td colspan="3">
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td nowrap="nowrap"><b>Downgrade?</b></td>
                        <td><input <? if($custComDowngrade == "Yes"){?>checked<? } ?> type="radio" name="custComDowngrade" value="Yes" /></td>
                        <td>Yes</td>
                        <td width="10px">&nbsp;</td>
                        <td><input <? if($custComDowngrade == "No"){?>checked<? } ?> type="radio" name="custComDowngrade" value="No" /></td>
                        <td>No</td>
                        <td>&nbsp;</td>
                        <td nowrap="nowrap"><b>&nbsp;</b></td>
                        <td><input <? if($custComAllowed == "Molars"){?>checked<? } ?> type="radio" name="custComAllowed" value="Molars" /></td>
                        <td><b>Molars</b></td>
                        <td width="10px">&nbsp;</td>
                        <td><input <? if($custComAllowed == "Premolars"){?>checked<? } ?> type="radio" name="custComAllowed" value="Premolars" /></td>
                        <td><b>Premolars</b></td>
                    </tr>
                </table>
            </td>
            <td width="20">&nbsp;</td>
            <td><b>Freq</b></td>
            <td>
                <input type="text" class="textbox" id="custComFreq" name="custComFreq" value="<?=$custComFreq?>" style="width: 75px" /> 
            </td>
            <td width="10">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <!-- Begin adding new fields 09 October 2015 (Muhammad Shoaib) -->

    <table cellpadding="3" cellspacing="0" border="0" width="100%">
       <tr>
        <td colspan="10">
                <tr class="alternate">
                    <td nowrap="nowrap"><b>Anterior Composites 2330</b></td>
                    <td><input type="text" class="textbox" name="custCom2330" id="custCom2330" value="<?=$custCom2330?>" style="width: 50px;">%</td>
                    <td width="5px">&nbsp;</td>
                    <td nowrap="nowrap"><b>Freq</b></td>
                    <td><input type="text" class="textbox" name="custCom2330Freq" id="custCom2330Freq" value="<?=$custCom2330Freq?>" style="width: 75px;"></td>
                    <td width="5">&nbsp;</td>
                    <td nowrap="nowrap"><b>BU 2950</b></td>
                    <td><input type="text" class="textbox" name="custCom2950" id="custCom2950" value="<?=$custCom2950?>" style="width: 50px;">%</td>
                    <td width="5px">&nbsp;</td>
                    <td nowrap="nowrap"><b>Freq</b></td>
                    <td><input type="text" class="textbox" name="custCom2950Freq" id="custCom2950Freq" value="<?=$custCom2950Freq?>" style="width: 75px;"></td>	
                    <td width="5">&nbsp;</td>
            <td nowrap="nowrap"><b>SUBMIT W/CROWN?</b></td>
            <td><input <? if($custComCrown == "Yes"){?>checked<? } ?> type="radio" name="custComCrown" value="Yes" /></td>
            <td>Yes</td>
            <td width="10px">&nbsp;</td>
            <td><input <? if($custComCrown == "No"){?>checked<? } ?> type="radio" name="custComCrown" value="No" /></td>
            <td>No</td>

                    
                    <td width="20">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                </td>
     </tr>
    </table>

      <table cellpadding="3" cellspacing="0" border="0" width="100%">
       <tr>
        <td colspan="5">
               <tr>
                    <td nowrap="nowrap"><b>Post and Core BU (2954)</b></td>
                    <td><input type="text" class="textbox" name="custPostCoreBU" id="custPostCoreBU" value="<?=$custPostCoreBU?>" style="width: 50px;">%</td>
                    <td width="15px">&nbsp;</td>
                    <td nowrap="nowrap"><b>Freq</b></td>
                    <td><input type="text" class="textbox" name="custPostCoreBUFreq" id="custPostCoreBUFreq" value="<?=$custPostCoreBUFreq?>" style="width: 75px;"></td>
                    
                    <td width="15px">&nbsp;</td>
                    <td colspan="8">&nbsp;</td>
                </tr> 
          </td>
     </tr>
      </table>

    <table width="100%" cellpadding="5" cellspacing="0">
        <tr class="titleTr">
            <td><label id="rightLabel">Endo</label></td>
        </tr>
    </table>
    <table cellpadding="3" cellspacing="0" width="100%">
        <tr>
            <td nowrap="nowrap"><b>Anterior (3310)</b></td>
            <td width="150"><input type="text" class="textbox" id="custEndoAnterior" name="custEndoAnterior" value="<?=$custEndoAnterior?>" style="width: 50px;">%</td>
            <td width="20">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custEndoAnteriorFreq" name="custEndoAnteriorFreq" value="<?=$custEndoAnteriorFreq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
            <td>
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td nowrap="nowrap"><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                        <td width="10">&nbsp;</td>
                        <td><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                        <td width="10">&nbsp;</td>
                        <td><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="alternate">
            <td nowrap="nowrap"><b>Bi-Cuspid (3320)</b></td>
            <td width="150"><input type="text" class="textbox" id="custEndoCuspid" name="custEndoCuspid" value="<?=$custEndoCuspid?>" style="width: 50px;">%</td>
            <td width="20">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custEndoCuspidFreq" name="custEndoCuspidFreq" value="<?=$custEndoCuspidFreq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
            <td>
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td nowrap="nowrap"><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                        <td width="10">&nbsp;</td>
                        <td><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                        <td width="10">&nbsp;</td>
                        <td><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td nowrap="nowrap"><b>Molars (3330)</b></td>
            <td width="150"><input type="text" class="textbox" id="custEndoMolars" name="custEndoMolars" value="<?=$custEndoMolars?>" style="width: 50px;">%</td>
            <td width="20">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custEndoMolarsFreq" name="custEndoMolarsFreq" value="<?=$custEndoMolarsFreq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
            <td>
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td nowrap="nowrap"><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                        <td width="10">&nbsp;</td>
                        <td><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                        <td width="10">&nbsp;</td>
                        <td><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="alternate">
            <td width="150"><b>Retreat Anterior (3346)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custEndo3346" name="custEndo3346" value="<?=$custEndo3346?>" style="width: 50px;">%</td>
            <td width="20">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custEndo3346Freq" name="custEndo3346Freq" value="<?=$custEndo3346Freq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
            <td>
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td nowrap="nowrap"><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                        <td width="10">&nbsp;</td>
                        <td><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                        <td width="10">&nbsp;</td>
                        <td><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td nowrap="nowrap"><b>Retreat Bi-Cuspid (3347)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custEndo3347" name="custEndo3347" value="<?=$custEndo3347?>" style="width: 50px;">%</td>
            <td width="20">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custEndo3347Freq" name="custEndo3347Freq" value="<?=$custEndo3347Freq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
            <td>
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td nowrap="nowrap"><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                        <td width="10">&nbsp;</td>
                        <td><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                        <td width="10">&nbsp;</td>
                        <td><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="alternate">
            <td nowrap="nowrap"><b>Retreat Molars (3348)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custEndo3348" name="custEndo3348" value="<?=$custEndo3348?>" style="width: 50px;">%</td>
            <td width="20">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custEndo3348Freq" name="custEndo3348Freq" value="<?=$custEndo3348Freq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
            <td>
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td nowrap="nowrap"><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                        <td width="10">&nbsp;</td>
                        <td><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                        <td width="10">&nbsp;</td>
                        <td><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr colspan="10">
            <td nowrap="nowrap"><b>IS THERE A TIME LIMITATION?</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custLimitation" name="custLimitation" value="<?=$custLimitation?>" style="width: 150px;"></td>
        </tr>
        <tr class="alternate">
            <td nowrap="nowrap"><b>RE TREATMENT DONE BY THE SAME PROVIDER WOULD BE OK?</b></td>
            <td nowrap="nowrap" colspan="10">
                <table cellpadding="1" cellspacing="0" border="0" >
                    <tr>
                        <td><input <? if($custTreatment == "Yes"){?>checked<? } ?> type="radio" name="custTreatment" value="Yes" /></td>
                        <td>Yes</td>
                        <td width="10px">&nbsp;</td>
                        <td><input <? if($custTreatment == "No"){?>checked<? } ?> type="radio" name="custTreatment" value="No" /></td>
                        <td>No</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="5" cellspacing="0">
        <tr class="titleTr">
            <td><label id="rightLabel">Codes</label></td>
        </tr>
    </table>
    <table cellpadding="3" cellspacing="0" width="100%">
     <tr>
         	<td nowrap="nowrap"><b>Post Removal (2955)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custPostRemoval" name="custPostRemoval" value="<?=$custPostRemoval?>" style="width: 50px;">%</td>
            <td width="20">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custPostRemovalFreq" name="custPostRemovalFreq" value="<?=$custPostRemovalFreq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
            <td>
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td nowrap="nowrap"><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                        <td width="10">&nbsp;</td>
                        <td><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                        <td width="10">&nbsp;</td>
                        <td><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
	 <tr>
         	<td nowrap="nowrap"><b>Obstruction (3331)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custCodes3331" name="custCodes3331" value="<?=$custCodes3331?>" style="width: 50px;">%</td>
            <td width="20">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custCodes3331Freq" name="custCodes3331Freq" value="<?=$custCodes3331Freq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
            <td>
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td nowrap="nowrap"><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                        <td width="10">&nbsp;</td>
                        <td><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                        <td width="10">&nbsp;</td>
                        <td><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>

		<tr class="alternate">
         	<td colspan="7">
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td width="565"><b>Will be covered when billed WITH RCT and/or ReTX, or if considered as part of the treatment?</b></td>
                        <td width="10">&nbsp;</td>
                        <td><input type="text" class="textbox" id="custEndoRCTReTX" name="custEndoRCTReTX" value="<?=$custEndoRCTReTX?>" style="width: 380px"></td>
                    </tr>
                </table>
            </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
	 
        
        <tr>
            <td nowrap="nowrap"><b>Incomplete (3332)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custEndo3332" name="custEndo3332" value="<?=$custEndo3332?>" style="width: 50px;">%</td>
            <td width="20">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custEndo3332Freq" name="custEndo3332Freq" value="<?=$custEndo3332Freq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
            <td>
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td nowrap="nowrap"><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                        <td width="10">&nbsp;</td>
                        <td><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                        <td width="10">&nbsp;</td>
                        <td><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>

		<tr class="alternate">
         	<td colspan="7">
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td width="565"><b>Will be paid as Incomplete, or downgraded to a D9110?</b></td>
                        <td width="10">&nbsp;</td>
                        <td><input type="text" class="textbox" id="custEndo9110" name="custEndo9110" value="<?=$custEndo9110?>" style="width: 380px"></td>
                    </tr>
                </table>
            </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        
         <tr>
            <td nowrap="nowrap"><b>Root Repair (3333)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custCodes3333" name="custCodes3333" value="<?=$custCodes3333?>" style="width: 50px;">%</td>
            <td width="20">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custCodes3333Freq" name="custCodes3333Freq" value="<?=$custCodes3333Freq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
            <td>
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td nowrap="nowrap">
                            <b>
                                <!--Periapicals (0220-30)-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 50px;">%-->
                        </td>
                        <td width="10">&nbsp;</td>
                        <td>
                            <b>
                                <!--Freq-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 60px;">-->
                        </td>
                        <td width="10">&nbsp;</td>
                        <td>
                            <b>
                                <!--Max?-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 60px;">-->
                        </td>
                    </tr>
                </table>
            </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        
        <tr class="alternate">
            <td nowrap="nowrap" width="150"><b>Apicoectomy Anterior (3410)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custCodes3410" name="custCodes3410" value="<?=$custCodes3410?>" style="width: 50px;">%</td>
            <td width="20">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custCodes3410Freq" name="custCodes3410Freq" value="<?=$custCodes3410Freq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
            <td>
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td nowrap="nowrap">
                            <b>
                                <!--Periapicals (0220-30)-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 50px;">%-->
                        </td>
                        <td width="10">&nbsp;</td>
                        <td>
                            <b>
                                <!--Freq-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 60px;">-->
                        </td>
                        <td width="10">&nbsp;</td>
                        <td>
                            <b>
                                <!--Max?-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 60px;">-->
                        </td>
                    </tr>
                </table>
            </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td nowrap="nowrap"><b>Apicoectomy Bi-Cuspid (3421)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custCodes3421" name="custCodes3421" value="<?=$custCodes3421?>" style="width: 50px;">%</td>
            <td width="20">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custCodes3421Freq" name="custCodes3421Freq" value="<?=$custCodes3421Freq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
            <td>
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td nowrap="nowrap">
                            <b>
                                <!--Periapicals (0220-30)-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 50px;">%-->
                        </td>
                        <td width="10">&nbsp;</td>
                        <td>
                            <b>
                                <!--Freq-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 60px;">-->
                        </td>
                        <td width="10">&nbsp;</td>
                        <td>
                            <b>
                                <!--Max?-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 60px;">-->
                        </td>
                    </tr>
                </table>
            </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr  class="alternate">
            <td nowrap="nowrap"><b>Apicoectomy Molars (3425)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custCodes3425" name="custCodes3425" value="<?=$custCodes3425?>" style="width: 50px;">%</td>
            <td width="20">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custCodes3425Freq" name="custCodes3425Freq" value="<?=$custCodes3425Freq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
            <td>
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td nowrap="nowrap">
                            <b>
                                <!--Periapicals (0220-30)-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 50px;">%-->
                        </td>
                        <td width="10">&nbsp;</td>
                        <td>
                            <b>
                                <!--Freq-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 60px;">-->
                        </td>
                        <td width="10">&nbsp;</td>
                        <td>
                            <b>
                                <!--Max?-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 60px;">-->
                        </td>
                    </tr>
                </table>
            </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td nowrap="nowrap"><b>Apicoectomy Additional Root (3426)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custCodes3426" name="custCodes3426" value="<?=$custCodes3426?>" style="width: 50px;">%</td>
            <td width="20">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custCodes3426Freq" name="custCodes3426Freq" value="<?=$custCodes3426Freq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
            <td>
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td nowrap="nowrap">
                            <b>
                                <!--Periapicals (0220-30)-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 50px;">%-->
                        </td>
                        <td width="10">&nbsp;</td>
                        <td>
                            <b>
                                <!--Freq-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 60px;">-->
                        </td>
                        <td width="10">&nbsp;</td>
                        <td>
                            <b>
                                <!--Max?-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 60px;">-->
                        </td>
                    </tr>
                </table>
            </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr  class="alternate">
            <td nowrap="nowrap"><b>Peri-rad w/o apicoectomy (3427)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custCodes3427" name="custCodes3427" value="<?=$custCodes3427?>" style="width: 50px;">%</td>
            <td width="20">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custCodes3427Freq" name="custCodes3427Freq" value="<?=$custCodes3427Freq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
            <td>
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td nowrap="nowrap">
                            <b>
                                <!--Periapicals (0220-30)-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 50px;">%-->
                        </td>
                        <td width="10">&nbsp;</td>
                        <td>
                            <b>
                                <!--Freq-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 60px;">-->
                        </td>
                        <td width="10">&nbsp;</td>
                        <td>
                            <b>
                                <!--Max?-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 60px;">-->
                        </td>
                    </tr>
                </table>
            </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td nowrap="nowrap"><b>Retrograde Filling (3430)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custCodes3430" name="custCodes3430" value="<?=$custCodes3430?>" style="width: 50px;">%</td>
            <td width="20">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custCodes3430Freq" name="custCodes3430Freq" value="<?=$custCodes3430Freq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
            <td>
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td nowrap="nowrap">
                            <b>
                                <!--Periapicals (0220-30)-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 50px;">%-->
                        </td>
                        <td width="10">&nbsp;</td>
                        <td>
                            <b>
                                <!--Freq-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 60px;">-->
                        </td>
                        <td width="10">&nbsp;</td>
                        <td>
                            <b>
                                <!--Max?-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 60px;">-->
                        </td>
                    </tr>
                </table>
            </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr  class="alternate">
            <td nowrap="nowrap"><b>Membrane graft (3432)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custCodes3432" name="custCodes3432" value="<?=$custCodes3432?>" style="width: 50px;">%</td>
            <td width="20">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custCodes3432Freq" name="custCodes3432Freq" value="<?=$custCodes3432Freq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
            <td>
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td nowrap="nowrap">
                            <b>
                                <!--Periapicals (0220-30)-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 50px;">%-->
                        </td>
                        <td width="10">&nbsp;</td>
                        <td>
                            <b>
                                <!--Freq-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 60px;">-->
                        </td>
                        <td width="10">&nbsp;</td>
                        <td>
                            <b>
                                <!--Max?-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 60px;">-->
                        </td>
                    </tr>
                </table>
            </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        
        <tr>
            <td nowrap="nowrap"><b>I & D (7510)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custCodes7510" name="custCodes7510" value="<?=$custCodes7510?>" style="width: 50px;">%</td>
            <td width="20">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custCodes7510Freq" name="custCodes7510Freq" value="<?=$custCodes7510Freq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
            <td>
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td nowrap="nowrap">
                            <b>
                                <!--Periapicals (0220-30)-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 50px;">%-->
                        </td>
                        <td width="10">&nbsp;</td>
                        <td>
                            <b>
                                <!--Freq-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 60px;">-->
                        </td>
                        <td width="10">&nbsp;</td>
                        <td>
                            <b>
                                <!--Max?-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 60px;">-->
                        </td>
                    </tr>
                </table>
            </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="alternate">
            <td nowrap="nowrap"><b>Nitrous Oxide (9230)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custCodes9230" name="custCodes9230" value="<?=$custCodes9230?>" style="width: 50px;">%</td>
            <td width="20">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custCodes9230Freq" name="custCodes9230Freq" value="<?=$custCodes9230Freq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
            <td>
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td nowrap="nowrap">
                            <b>
                                <!--Periapicals (0220-30)-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 50px;">%-->
                        </td>
                        <td width="10">&nbsp;</td>
                        <td>
                            <b>
                                <!--Freq-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 60px;">-->
                        </td>
                        <td width="10">&nbsp;</td>
                        <td>
                            <b>
                                <!--Max?-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 60px;">-->
                        </td>
                    </tr>
                </table>
            </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td nowrap="nowrap"><b>Internal Bleaching (9974)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custCodes9974" name="custCodes9974" value="<?=$custCodes9974?>" style="width: 50px;">%</td>
            <td width="20">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custCodes9974Freq" name="custCodes9974Freq" value="<?=$custCodes9974Freq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
            <td>
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td nowrap="nowrap">
                            <b>
                                <!--Periapicals (0220-30)-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 50px;">%-->
                        </td>
                        <td width="10">&nbsp;</td>
                        <td>
                            <b>
                                <!--Freq-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 60px;">-->
                        </td>
                        <td width="10">&nbsp;</td>
                        <td>
                            <b>
                                <!--Max?-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 60px;">-->
                        </td>
                    </tr>
                </table>
            </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr  class="alternate">
            <td nowrap="nowrap"><b>Sedation (9248)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custCodes9248" name="custCodes9248" value="<?=$custCodes9248?>" style="width: 50px;">%</td>
            <td width="20">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custCodes9248Freq" name="custCodes9248Freq" value="<?=$custCodes9248Freq?>" style="width: 75px;"></td>
            <td width="10">&nbsp;</td>
            <td>
                <table cellpadding="1" cellspacing="0" border="0">
                    <tr>
                        <td nowrap="nowrap">
                            <b>
                                <!--Periapicals (0220-30)-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 50px;">%-->
                        </td>
                        <td width="10">&nbsp;</td>
                        <td>
                            <b>
                                <!--Freq-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 60px;">-->
                        </td>
                        <td width="10">&nbsp;</td>
                        <td>
                            <b>
                                <!--Max?-->
                            </b>
                        </td>
                        <td>
                            <!--<input type="text" class="textbox" name="" id="" value="" style="width: 60px;">-->
                        </td>
                    </tr>
                </table>
            </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <br />
    <table width="100%" cellpadding="5" cellspacing="0">
        <tr class="titleTr">
            <td><label id="rightLabel">History</label></td>
        </tr>
    </table>
    <table cellpadding="3" cellspacing="0" width="100%">
        <tr>
            <td width="60" nowrap="nowrap"><b>History</b></td>
            <td nowrap="nowrap"><textarea class="textbox" id="custHistory" name="custHistory" rows="2"  style="width: 800px;height:70px;margin-top: 10px;"><?=$custHistory?></textarea></td>
        </tr>
    </table>
    <br />
    <?php
        //$date = date_create(date('y-m-d'), timezone_open('Pacific/Nauru'));
        $date = putenv("TZ=US/Pacific");
        $pacific_time = date("h:i:s");
        $todate = date("Y-m-d");
        
        if($custDateTime == "")
        {
        	$custDateTime = $todate . " ". $pacific_time;
        }
        ?>
    <table width="100%" cellpadding="5" cellspacing="0">
        <tr class="titleTr">
            <td colspan="4"><label id="rightLabel">Special Request</label></td>
        </tr>
    </table>
    <table cellpadding="3" cellspacing="5" width="100%" style="border:1px solid; background-color: #EFEFEF;">
        <tr>
            <td colspan="4"><b>Special Request</b></td>
        </tr>
        <tr>
            <td colspan="4">
                <textarea name="custSpecialRequest" id="custSpecialRequest" cols="100" rows="5"><?=$custSpecialRequest?></textarea>
                <script type="text/javascript">
                    CKEDITOR.replace( 'custSpecialRequest' );
                </script>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td width="90px" valign="top"><b>Spoke with:</b></td>
            <td width="250px"><input type="text" class="textbox" id="custSpokeWith" name="custSpokeWith" value="<?=$custSpokeWith?>" style="width: 200px" /></td>
            <td width="140px" valign="top"><b>Account Executive:</b></td>
            <td><input type="text" class="textbox" id="custAccountExecutive" name="custAccountExecutive" value="<?=$custAccountExecutive?>" style="width: 200px" /></td>
        </tr>
        <tr>
            <td width="80px" valign="top"><b>Date/Time:</b></td>
            <td colspan="3">
                <input type="text" class="textbox" id="custDateTime" name="custDateTime" value="<?=date("m/d/Y h:i:s")?>" style="width: 200px" />
                <br />
                <p>Last Date / Time:
                    <?=$custDateTime?>
                </p>
            </td>
        </tr>
    </table>
</div>