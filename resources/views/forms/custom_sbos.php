<div id="divCustom">

<table cellpadding="3" cellspacing="0" border="0">
<tr>
<td width="130"><b>Network Type</b></td>
<td>
<select id="custNetwork" name="custNetwork" size="1">
    <option value=""></option>
    <option value="In Network" <? if($custNetwork=="In Network"){ ?>Selected<? } ?>>In Network</option>
    <option value="Out Of Network" <? if($custNetwork=="Out Of Network"){ ?>Selected<? } ?>>Out Of Network</option>
    <option value="Policy Terminated" <? if($custNetwork=="Policy Terminated"){ ?>Selected<? } ?>>Policy Terminated</option>
</select></td>
<td></td>
<td>
    <table border="0" cellpadding="2" cellspacing="0">
    <tr>
        <td nowrap="nowrap"><b>Payer&nbsp;ID</b></td>
        <td>&nbsp;</td>
        <td><input type="text" class="textbox" id="custPayorId" name="custPayorId" value="<?=$custPayorId?>" style="width: 100px" /></td>
        <td width="50px">&nbsp;</td>
        <td nowrap="nowrap"><b>E-Attachments</b></td>
        <td width="30">&nbsp;</td>
        <td><input <? if($custAttachments == "Yes"){?>checked<? } ?> type="radio" name="custAttachments" value="Yes" /></td>
        <td>Yes</td>
        <td width="20px">&nbsp;</td>
        <td><input <? if($custAttachments == "No"){?>checked<? } ?> type="radio" name="custAttachments" value="No" /></td>
        <td>No</td>
    </tr>
    </table></td>
</tr>

<tr class="alternate">
<td width="130"><b>Quick Note</b></td>
<td colspan="6"><input type="text" class="textbox" id="custQuickNote" name="custQuickNote" value="<?=$custQuickNote?>" style="width: 610px" maxlength="155" /></td>
<td></td>
</tr>

<tr>
<td width="130"><b>Effective Date</b></td>
<td colspan="2"><input type="text" class="textbox" id="custEffectiveDate" name="custEffectiveDate" value="<?=$custEffectiveDate?>" style="width: 60px" maxlength="10" /><input type="button" value="Select" class="smallButton" onclick="displayDatePicker('custEffectiveDate');" /></td>
<td colspan="5">
<table cellpadding="2" cellspacing="0">
  <tr>
  <td><b>Fee Schedule</b></td>
  <td width="30">&nbsp;</td>
  <td valign="top" width="20px"><input <? if($custFee == "PPO"){?>checked<? } ?> type="radio" name="custFee" value="PPO" /></td>
  <td valign="top">PPO</td>
  <td width="15px">&nbsp;</td>
  <td valign="top" width="20px"><input <? if($custFee == "UCR"){?>checked<? } ?> type="radio" name="custFee" value="UCR" /></td>
  <td valign="top">UCR</td>
  <td valign="top">&nbsp;</td>
  <td valign="top"><input type="text" class="textbox" id="custFeeName" name="custFeeName" value="<?=$custFeeName?>" /></td>
  </tr>
</table></td>
</tr>

<tr>
<td width="130"><b>ID</b></td>
<td><input type="text" class="textbox" id="custMemberId" name="custMemberId" value="<?=$custMemberId?>" style="width: 100px" /></td>
<td width="5"></td>
<td width="130">

<table cellpadding="2" cellspacing="0">
  <tr>
  <td nowrap="nowrap"><b>Group Name</b></td>
  <td width="30">&nbsp;</td>
  <td valign="top" width="20px"><input type="text" class="textbox" id="custGroup" name="custGroup" value="<?=$custGroup?>" style="width: 100px" /></td>
   <td width="30">&nbsp;</td>
	<td nowrap="nowrap"><b>Group #</b></td>
	<td width="30">&nbsp;</td>
<?php 
	//Get group number from patient detail table
	$custGroupNum = getField('cui_patients', 'patientId', $patientId, 'patientGroup');
	$custGroupNum = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', encrypt_decrypt('decrypt', $custGroupNum));
	?>
	<td valign="top" width="20px"><input type="text" class="textbox" id="custGroupNum" name="custGroupNum" value="<?=$custGroupNum?>" style="width: 100px" /></td>
  </tr>
</table></td>
<td width="50"></td>
<td></td>
</tr>
<tr class="alternate">
<td><b>Benefits Run ?</b></td>
<td colspan="7">
<table cellpadding="1" cellspacing="0">
<tr>
<td width="20px"><input <? if($custBenefitsRun == "Calender"){?>checked<? } ?> type="radio" name="custBenefitsRun" value="Calender" /></td>
<td>Calendar</td>
<td width="15px">&nbsp;</td>
<td width="20px"><input <? if($custBenefitsRun == "Contract Year"){?>checked<? } ?> type="radio" name="custBenefitsRun" value="Contract Year" /></td>
<td>Contract Year</td>
<td width="5px">&nbsp;</td>
<td>&nbsp;</td>
<td>
<input type="text" class="textbox" id="custBenefitsDate" name="custBenefitsDate" value="<?=$custBenefitsDate?>" style="width: 60px" maxlength="10" /><input type="button" value="Select" class="smallButton" onclick="displayDatePicker('custBenefitsDate');" /></td>
</tr>
</table></td>
</tr>
<tr>
<td valign="top"><b>MTC</b></td>
<td colspan="6">
<table cellpadding="1" cellspacing="0">
<tr>
<td><input <? if($custMtc == "Yes"){?>checked<? } ?> type="radio" name="custMtc" value="Yes" /></td>
<td>Yes</td>
<td width="20px">&nbsp;</td>
<td><input <? if($custMtc == "No"){?>checked<? } ?> type="radio" name="custMtc" value="No" /></td>
<td>No</td>
<Td width="20px">&nbsp;</Td>
<td><b>Details</b></td>
<td width="15px">&nbsp;</td>
<Td><input type="text" class="textbox" id="custMtcDetail" name="custMtcDetail" value="<?=$custMtcDetail?>" style="width: 180px" /></Td>
</tr>
</table>
</td>
<td>&nbsp;</td>
</tr>
<tr class="alternate">
   <td nowrap="nowrap"><b>Waiting Period</b></td>
   <td colspan="7">
      <table cellpadding="1" cellspacing="0">
         <tr>
            <td><input <? if($custWaitingPeriod == "Yes"){?>checked<? } ?> type="radio" name="custWaitingPeriod" value="Yes" /></td>
            <td>Yes</td>
            <td width="20px">&nbsp;</td>
            <td><input <? if($custWaitingPeriod == "No"){?>checked<? } ?> type="radio" name="custWaitingPeriod" value="No" /></td>
            <td>No</td>
            <Td width="20px">&nbsp;</Td>
            <td><b>Details</b></td>
            <td width="15px">&nbsp;</td>
            <Td><input type="text" class="textbox" id="custWaitingPeriodDetail" name="custWaitingPeriodDetail" value="<?=$custWaitingPeriodDetail?>" style="width: 180px" /></Td>
         </tr>
      </table>
   </td>

</tr>
<tr>
  <td colspan="8">
  	<table cellpadding="2" cellspacing="0">
      <tr>
      <td width="132px" style="padding-left:0px;"><b>Yearly Max $</b></td>
      <td><input type="text" class="textbox" id="custYearlyMax" name="custYearlyMax" value="<?=$custYearlyMax?>" style="width: 60px" /></td>
      <td width="10px">&nbsp;</td>
      <td width="80px"><b>Applies to?</b></td>
      <td><input <? if($custYearlyApplies1 == "P"){?>checked<? } ?> type="checkbox" name="custYearlyApplies1" value="P" /></td>
      <td>P</td>
      <td width="2px"></td>
      <td><input <? if($custYearlyApplies2 == "B"){?>checked<? } ?> type="checkbox" name="custYearlyApplies2" value="B" /></td>
      <td>B</td>
      <td width="2px"></td>
      <td><input <? if($custYearlyApplies3 == "M"){?>checked<? } ?> type="checkbox" name="custYearlyApplies3" value="M" /></td>
      <td>M</td>
      <td width="20px">&nbsp;</td>
      <td width="50px"><b>Used $</b></td>
      <td><input type="text" class="textbox" id="custUsed" name="custUsed" value="<?=$custUsed?>" style="width: 60px" /></td>
      <td width="10px">&nbsp;</td>
      <td width="90px"><b>Roll over? $</b></td>
      <td><input type="text" class="textbox" id="custRollOver" name="custRollOver" value="<?=$custRollOver?>" style="width: 60px" /></td>
      </tr>
    </table>  </td>
  </tr>
<tr class="alternate">
  <td colspan="8">
  	<table cellpadding="2" cellspacing="0">
      <tr>
      <td width="132px" style="padding-left:0px;"><b>Ind. Deductible $</b></td>
      <td><input type="text" class="textbox" id="custDeduct" name="custDeduct" value="<?=$custDeduct?>" style="width: 60px" /></td>
      <td width="10px">&nbsp;</td>
      <td width="80px"><b>Applies to?</b></td>
      <td><input <? if($custDeductApplies1 == "P"){?>checked<? } ?> type="checkbox" name="custDeductApplies1" value="P" /></td>
      <td>P</td>
      <td width="2px"></td>
      <td><input <? if($custDeductApplies2 == "D"){?>checked<? } ?> type="checkbox" name="custDeductApplies2" value="D" /></td>
      <td>D</td>
      <td width="2px"></td>
      <td><input <? if($custDeductApplies3 == "B"){?>checked<? } ?> type="checkbox" name="custDeductApplies3" value="B" /></td>
      <td>B</td>
      <td width="2px"></td>
      <td><input <? if($custDeductApplies4 == "M"){?>checked<? } ?> type="checkbox" name="custDeductApplies4" value="M" /></td>
      <td>M</td>
      <td width="20px">&nbsp;</td>
      <td width="40px"><b>Met</b></td>
      <td><input <? if($custMet == "Yes"){?>checked<? } ?> type="radio" name="custMet" value="Yes" /></td>
      <td>Yes</td>
      <td width="5px"></td>
      <td><input <? if($custMet == "No"){?>checked<? } ?> type="radio" name="custMet" value="No" /></td>
      <td>No</td>
      </tr>
    </table>  </td>
</tr>
<tr>
  <td colspan="8">
  	<table cellpadding="2" cellspacing="0">
      <tr>
      <td width="132px" style="padding-left:0px;"><b>Fam. Deductible $</b></td>
      <td><input type="text" class="textbox" id="custFamilyDeduct" name="custFamilyDeduct" value="<?=$custFamilyDeduct?>" style="width: 60px" /></td>
      <td width="10px">&nbsp;</td>
      <td width="40px"><b>Met</b></td>
      <td><input <? if($custMet2 == "Yes"){?>checked<? } ?> type="radio" name="custMet2" value="Yes" /></td>
      <td>Yes</td>
      <td width="5px"></td>
      <td><input <? if($custMet2 == "No"){?>checked<? } ?> type="radio" name="custMet2" value="No" /></td>
      <td>No</td>
      </tr>
    </table>  </td>
</tr>
<tr class="alternate">
  <td colspan="8">
  	<table cellpadding="1" cellspacing="0" border="0">
    <tr>
    <td nowrap="nowrap"><b>Basic</b></td>
    <td width="5px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custBasic" name="custBasic" value="<?=$custBasic?>" style="width: 60px" />%</td>
    <td width="15px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>Major</b></td>
    <td width="5px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custMajor" name="custMajor" value="<?=$custMajor?>" style="width: 60px" />%</td>
    <td width="15px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>Perio</b></td>
    <td width="5px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custPerio" name="custPerio" value="<?=$custPerio?>" style="width: 60px" />%</td>
    <td width="15px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>Endo</b></td>
    <td width="5px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custEndo" name="custEndo" value="<?=$custEndo?>" style="width: 60px" />%</td>
    <td width="15px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>OS</b></td>
    <td width="5px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custOs" name="custOs" value="<?=$custOs?>" style="width: 60px" />%</td>
    
    <td width="15px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>OS to Med</b></td>
    <td width="5px">&nbsp;</td>
    <td><input <? if($custOsToMed == "Yes"){?>checked<? } ?> type="radio" name="custOsToMed" value="Yes" /></td>
    <td>Yes</td>
    <td width="5px"></td>
    <td><input <? if($custOsToMed == "No"){?>checked<? } ?> type="radio" name="custOsToMed" value="No" /></td>
    <td>No</td>
    </tr>
    </table>  </td>
</tr>

	<tr>
	   <td colspan="2"><b>After insurence is maxed: Charge our fee or Insurance Allowable?</b></td>
	   <td colspan="4">
		  <table cellpadding="1" cellspacing="0">
			 <td><input type="text" class="textbox" id="custAfterinsurence" name="custAfterinsurence" value="<?=$custAfterinsurence?>" style="width: 450px" /></td>
			 <td width="50">&nbsp;</td>
		  </table>
	   </td>
	</tr>
	<tr class="alternate">
	   <td colspan="2" ><b>If Procedure is not a covered benefit: Charge our fee or Insurance Allowable?</b></td>
	   <td colspan="4">
		  <table cellpadding="1" cellspacing="0">
			 <td><input type="text" class="textbox" id="custCoveredbenefit" name="custCoveredbenefit" value="<?=$custCoveredbenefit?>" style="width: 450px" /></td>
		  </table>
	   </td>
	</tr>
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">Diagnostic & Preventative</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">

<tr>
    <td nowrap="nowrap"><b>Limited Exam (0140)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPLimitedExam" name="custDPLimitedExam" value="<?=$custDPLimitedExam?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPLimitedExamFreq" name="custDPLimitedExamFreq" value="<?=$custDPLimitedExamFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td colspan="3">
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td nowrap="nowrap"><b>Separate from 0120?</b></td>
        <td><input <? if($custDPSeparate == "Yes"){?>checked<? } ?> type="radio" name="custDPSeparate" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custDPSeparate == "No"){?>checked<? } ?> type="radio" name="custDPSeparate" value="No" /></td>
        <td>No</td>
        <td>&nbsp;</td>
        <td nowrap="nowrap"><b>Allowed same day w/ TX?</b></td>
        <td><input <? if($custDPAllowed == "Yes"){?>checked<? } ?> type="radio" name="custDPAllowed" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custDPAllowed == "No"){?>checked<? } ?> type="radio" name="custDPAllowed" value="No" /></td>
        <td>No</td>
        </tr>
        </table>    </td>
    <td>&nbsp;</td>
</tr>

<tr>
    <td nowrap="nowrap"><b>FMX(0210)/Pano(0330)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPFMX" name="custDPFMX" value="<?=$custDPFMX?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPFMXFreq" name="custDPFMXFreq" value="<?=$custDPFMXFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td><input <? if($custDPShare == "Share"){?>checked<? } ?> type="radio" name="custDPShare" value="Share" /></td>
        <td nowrap="nowrap"><b>Share</b></td>
        <td><input <? if($custDPShare == "Separate"){?>checked<? } ?> type="radio" name="custDPShare" value="Separate" /></td>
        <td><b>Separate</b></td>
        <td width="30">&nbsp;</td>
       
        </tr>
        </table>    </td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<!--<tr>
    <td nowrap="nowrap"><b>Bitewings (0270-74)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPBit" name="custDPBit" value="<?=$custDPBit?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPBitFreq" name="custDPBitFreq" value="<?=$custDPBitFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>

    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>-->
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">Surgical</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
  <tr>
    <td width="130" nowrap="nowrap"><b>Implant (6010)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSImplant" name="custSImplant" value="<?=$custSImplant?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>7950</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custSPrefabAbutment" name="custSPrefabAbutment" value="<?=$custSPrefabAbutment?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>7951</b></td>
            <td><input type="text" class="textbox" id="custSCustomAbutment" name="custSCustomAbutment" value="<?=$custSCustomAbutment?>" style="width: 50px" />%</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>7280</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSceramicCrown" name="custSceramicCrown" value="<?=$custSceramicCrown?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>7283</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custSPFM" name="custSPFM" value="<?=$custSPFM?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>7286</b></td>
            <td><input type="text" class="textbox" id="custSGraft" name="custSGraft" value="<?=$custSGraft?>" style="width: 50px" />%</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>Bone Graft (7953)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSGraft2" name="custSGraft2" value="<?=$custSGraft2?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Membrane (4266)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custSMembrane" name="custSMembrane" value="<?=$custSMembrane?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>Membrane (4267)</b></td>
            <td><input type="text" class="textbox" id="custSMembrane2" name="custSMembrane2" value="<?=$custSMembrane2?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>4263</b></td>
            <td><input type="text" class="textbox" id="custSN2O" name="custSN2O" value="<?=$custSN2O?>" style="width: 50px" />%</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Anesthesia - First 30min (9220)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custSAnesthesia" name="custSAnesthesia" value="<?=$custSAnesthesia?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
          
            <td><b>If Anesthesia is NOT a covered benefit: Charge our fee or Insurance Allowable?</b></td>
            <td><input type="text" class="textbox" id="custSAdditional" name="custSAdditional" value="<?=$custSAdditional?>" style="width: 110px" /></td>
        </tr>
        </table>
    </td>
  </tr>
    <tr>
    <td nowrap="nowrap" ><b>Any Restrictions?</b></td>
	<td nowrap="nowrap" colspan="5"><input type="text" class="textbox" id="custAnyRestrictions" name="custAnyRestrictions" value="<?=$custAnyRestrictions?>" style="width: 470px" /></td>

  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>Simple Ext (7140)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSExt" name="custSExt" value="<?=$custSExt?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Covered</b></td>
            <td><input <? if($custSExtCovered == "Medical"){?>checked<? } ?> type="radio" name="custSExtCovered" value="Medical" /></td>
            <td>Medical</td>
            <td width="10px">&nbsp;</td>
            <td><input <? if($custSExtCovered == "Dental"){?>checked<? } ?> type="radio" name="custSExtCovered" value="Dental" /></td>
            <td>Dental</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>Surgical Ext (7210)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSSurgical" name="custSSurgical" value="<?=$custSSurgical?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Covered</b></td>
            <td><input <? if($custSSurgicalCovered == "Medical"){?>checked<? } ?> type="radio" name="custSSurgicalCovered" value="Medical" /></td>
            <td>Medical</td>
            <td width="10px">&nbsp;</td>
            <td><input <? if($custSSurgicalCovered == "Dental"){?>checked<? } ?> type="radio" name="custSSurgicalCovered" value="Dental" /></td>
            <td>Dental</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>Soft Tissue Ext (7220)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSTissue" name="custSTissue" value="<?=$custSTissue?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Covered</b></td>
            <td><input <? if($custSTissueCovered == "Medical"){?>checked<? } ?> type="radio" name="custSTissueCovered" value="Medical" /></td>
            <td>Medical</td>
            <td width="10px">&nbsp;</td>
            <td><input <? if($custSTissueCovered == "Dental"){?>checked<? } ?> type="radio" name="custSTissueCovered" value="Dental" /></td>
            <td>Dental</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>Partial Bony Ext (7230)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSBony" name="custSBony" value="<?=$custSBony?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Covered</b></td>
            <td><input <? if($custSBonyCovered == "Medical"){?>checked<? } ?> type="radio" name="custSBonyCovered" value="Medical" /></td>
            <td>Medical</td>
            <td width="10px">&nbsp;</td>
            <td><input <? if($custSBonyCovered == "Dental"){?>checked<? } ?> type="radio" name="custSBonyCovered" value="Dental" /></td>
            <td>Dental</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>Complete Bony Ext (7240)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSBony2" name="custSBony2" value="<?=$custSBony2?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Covered</b></td>
            <td><input <? if($custSBony2Covered == "Medical"){?>checked<? } ?> type="radio" name="custSBony2Covered" value="Medical" /></td>
            <td>Medical</td>
            <td width="10px">&nbsp;</td>
            <td><input <? if($custSBony2Covered == "Dental"){?>checked<? } ?> type="radio" name="custSBony2Covered" value="Dental" /></td>
            <td>Dental</td>
        </tr>
        </table>
    </td>
  </tr>
  
  
</table>



<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">History</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
  <tr>
    <td width="60" nowrap="nowrap"><b>History</b></td>
    <td nowrap="nowrap"><textarea class="textbox" id="custHistory" name="custHistory" rows="2"  style="width: 800px"><?=$custHistory?></textarea></td>
    </tr>
</table>

<br />

<?php
//$date = date_create(date('y-m-d'), timezone_open('Pacific/Nauru'));
$date = putenv("TZ=US/Pacific");
$pacific_time = date("h:i:s");
$todate = date("Y-m-d");

if($custDateTime == "")
{
	$custDateTime = $todate . " ". $pacific_time;
}
?>


  <table width="100%" cellpadding="5" cellspacing="0">
    <tr class="titleTr">
      <td colspan="4"><label id="rightLabel">Special Request</label></td>
    </tr>
  </table>
  <table cellpadding="3" cellspacing="5" width="100%" style="border:1px solid; background-color: #EFEFEF;">
    <tr>
      <td colspan="4"><b>Special Request</b></td>
    </tr>
    <tr>
      <td colspan="4">
      <textarea name="custSpecialRequest" id="custSpecialRequest" cols="100" rows="5"><?=$custSpecialRequest?></textarea>
      <script type="text/javascript">
	  CKEDITOR.replace( 'custSpecialRequest' );
	  </script>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td width="90px" valign="top"><b>Spoke with:</b></td>
      <td width="250px"><input type="text" class="textbox" id="custSpokeWith" name="custSpokeWith" value="<?=$custSpokeWith?>" style="width: 200px" /></td>
      <td width="140px" valign="top"><b>Account Executive:</b></td>
      <td><input type="text" class="textbox" id="custAccountExecutive" name="custAccountExecutive" value="<?=$custAccountExecutive?>" style="width: 200px" /></td>
    </tr>
    <tr>
      <td width="80px" valign="top"><b>Date/Time:</b></td>
      <td colspan="3"><input type="text" class="textbox" id="custDateTime" name="custDateTime" value="<?=date("m/d/Y h:i:s")?>" style="width: 200px" />
        <br />
        <p>Last Date / Time:
          <?=$custDateTime?>
        </p></td>
    </tr>
  </table>
</div>
