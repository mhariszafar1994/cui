<div id="divCustom">

<table cellpadding="3" cellspacing="0" border="0">
<tr>
<td width="130"><b>Network Type</b></td>
<td width="185">
<select id="custNetwork" name="custNetwork" size="1">
    <option value=""></option>
    <option value="In Network" <? if($custNetwork=="In Network"){ ?>Selected<? } ?>>In Network</option>
    <option value="Out Of Network" <? if($custNetwork=="Out Of Network"){ ?>Selected<? } ?>>Out Of Network</option>
    <option value="Policy Terminated" <? if($custNetwork=="Policy Terminated"){ ?>Selected<? } ?>>Policy Terminated</option>
</select></td>
<td></td>
<td>
    <table border="0" cellpadding="2" cellspacing="0">
    <tr>
        <td nowrap="nowrap"><b>Payer&nbsp;ID</b></td>
        <td>&nbsp;</td>
        <td><input type="text" class="textbox" id="custPayorId" name="custPayorId" value="<?=$custPayorId?>" style="width: 100px" /></td>
        <td width="50px">&nbsp;</td>
    </tr>
    </table></td>
</tr>

<tr class="alternate">
<td width="130"><b>Quick Note</b></td>
<td colspan="6"><input type="text" class="textbox" id="custQuickNote" name="custQuickNote" value="<?=$custQuickNote?>" style="width: 610px" maxlength="155" /></td>
<td></td>
</tr>

<tr>
<td width="130"><b>Effective Date</b></td>
<td colspan="2"><input type="text" class="textbox" id="custEffectiveDate" name="custEffectiveDate" value="<?=$custEffectiveDate?>" style="width: 60px" maxlength="10" /><input type="button" value="Select" class="smallButton" onclick="displayDatePicker('custEffectiveDate');" /></td>
<td colspan="5">
<table cellpadding="2" cellspacing="0">
  <tr>
  <td><b>Fee Schedule</b></td>
  <td width="30">&nbsp;</td>
  <td valign="top" width="20px"><input <? if($custFee == "PPO"){?>checked<? } ?> type="radio" name="custFee" value="PPO" /></td>
  <td valign="top">PPO</td>
  <td width="15px">&nbsp;</td>
  <td valign="top" width="20px"><input <? if($custFee == "UCR"){?>checked<? } ?> type="radio" name="custFee" value="UCR" /></td>
  <td valign="top">UCR</td>
  <td valign="top">&nbsp;</td>
  <td valign="top"><input type="text" class="textbox" id="custFeeName" name="custFeeName" value="<?=$custFeeName?>" /></td>
  </tr>
</table></td>
</tr>

<tr>
<td width="130"><b>ID</b></td>
<td><input type="text" class="textbox" id="custMemberId" name="custMemberId" value="<?=$custMemberId?>" style="width: 100px" /></td>
<td width="5"></td>
<td width="130">

<table cellpadding="2" cellspacing="0">
  <tr>
  <td nowrap="nowrap"><b>Group Name</b></td>
  <td width="30">&nbsp;</td>
  <td valign="top" width="20px"><input type="text" class="textbox" id="custGroup" name="custGroup" value="<?=$custGroup?>" style="width: 100px" /></td>
  </tr>
</table></td>
<td width="50"></td>
<td></td>
</tr>
<tr class="alternate">
<td><b>Benefits Run ?</b></td>
<td colspan="7">
<table cellpadding="1" cellspacing="0">
<tr>
<td width="20px"><input <? if($custBenefitsRun == "Calender"){?>checked<? } ?> type="radio" name="custBenefitsRun" value="Calender" /></td>
<td>Calendar</td>
<td width="15px">&nbsp;</td>
<td width="20px"><input <? if($custBenefitsRun == "Contract Year"){?>checked<? } ?> type="radio" name="custBenefitsRun" value="Contract Year" /></td>
<td>Contract Year</td>
<td width="5px">&nbsp;</td>
<td>&nbsp;</td>
<td><input type="text" class="textbox" id="custBenefitsDate" name="custBenefitsDate" value="<?=$custBenefitsDate?>" style="width: 60px" maxlength="10" /><input type="button" value="Select" class="smallButton" onclick="displayDatePicker('custBenefitsDate');" /></td>
<td width="20px">&nbsp;</td>
<td nowrap="nowrap"><b>Benefit Type</b></td>
<td width="10">&nbsp;</td>
<td>
<select id="custBenefitsType" name="custBenefitsType" size="1">
    <option value=""></option>
    <option value="Individual" <? if($custBenefitsType=="Individual"){ ?>Selected<? } ?>>Individual</option>
    <option value="Family" <? if($custBenefitsType=="Family"){ ?>Selected<? } ?>>Family</option>
</select>
</td>   
</tr>
</table></td>
</tr>
<tr>
<td valign="top"><b>MTC</b></td>
<td colspan="6">
<table cellpadding="1" cellspacing="0">
<tr>
<td><input <? if($custMtc == "Yes"){?>checked<? } ?> type="radio" name="custMtc" value="Yes" /></td>
<td>Yes</td>
<td width="20px">&nbsp;</td>
<td><input <? if($custMtc == "No"){?>checked<? } ?> type="radio" name="custMtc" value="No" /></td>
<td>No</td>
<Td width="20px">&nbsp;</Td>
<td><b>Details</b></td>
<td width="15px">&nbsp;</td>
<Td><input type="text" class="textbox" id="custMtcDetail" name="custMtcDetail" value="<?=$custMtcDetail?>" style="width: 180px" /></Td>
</tr>
</table>
</td>
<td>&nbsp;</td>
</tr>
<tr class="alternate">
<td nowrap="nowrap"><b>Waiting Period</b></td>
<td colspan="7">
<table cellpadding="1" cellspacing="0">
<tr>
<td><input <? if($custWaitingPeriod == "Yes"){?>checked<? } ?> type="radio" name="custWaitingPeriod" value="Yes" /></td>
<td>Yes</td>
<td width="20px">&nbsp;</td>
<td><input <? if($custWaitingPeriod == "No"){?>checked<? } ?> type="radio" name="custWaitingPeriod" value="No" /></td>
<td>No</td>
<Td width="20px">&nbsp;</Td>
<td><b>Details</b></td>
<td width="15px">&nbsp;</td>
<Td><input type="text" class="textbox" id="custWaitingPeriodDetail" name="custWaitingPeriodDetail" value="<?=$custWaitingPeriodDetail?>" style="width: 180px" /></Td>
</tr>
</table></td>
</tr>
<tr>
  <td colspan="8">
  	<table cellpadding="2" cellspacing="0">
      <tr>
      <td width="132px" style="padding-left:0px;"><b>Yearly Max $</b></td>
      <td><input type="text" class="textbox" id="custYearlyMax" name="custYearlyMax" value="<?=$custYearlyMax?>" style="width: 60px" /></td>
      <td width="10px">&nbsp;</td>
      <td width="80px"><b>Applies to?</b></td>
      <td><input <? if($custYearlyApplies1 == "P"){?>checked<? } ?> type="checkbox" name="custYearlyApplies1" value="P" /></td>
      <td>P</td>
      <td width="2px"></td>
      <td><input <? if($custYearlyApplies2 == "B"){?>checked<? } ?> type="checkbox" name="custYearlyApplies2" value="B" /></td>
      <td>B</td>
      <td width="2px"></td>
      <td><input <? if($custYearlyApplies3 == "M"){?>checked<? } ?> type="checkbox" name="custYearlyApplies3" value="M" /></td>
      <td>M</td>
      <td width="20px">&nbsp;</td>
      <td width="50px"><b>Used $</b></td>
      <td><input type="text" class="textbox" id="custUsed" name="custUsed" value="<?=$custUsed?>" style="width: 60px" /></td>
      <td width="10px">&nbsp;</td>
      <td width="90px"><b>Roll over? $</b></td>
      <td><input type="text" class="textbox" id="custRollOver" name="custRollOver" value="<?=$custRollOver?>" style="width: 60px" /></td>
      </tr>
    </table>  </td>
  </tr>
<tr class="alternate">
  <td colspan="8">
  	<table cellpadding="2" cellspacing="0">
      <tr>
      <td width="132px" style="padding-left:0px;"><b>Ind. Deductible $</b></td>
      <td><input type="text" class="textbox" id="custDeduct" name="custDeduct" value="<?=$custDeduct?>" style="width: 60px" /></td>
      <td width="10px">&nbsp;</td>
      <td width="80px"><b>Applies to?</b></td>
      <td><input <? if($custDeductApplies1 == "P"){?>checked<? } ?> type="checkbox" name="custDeductApplies1" value="P" /></td>
      <td>P</td>
      <td width="2px"></td>
      <td><input <? if($custDeductApplies2 == "D"){?>checked<? } ?> type="checkbox" name="custDeductApplies2" value="D" /></td>
      <td>D</td>
      <td width="2px"></td>
      <td><input <? if($custDeductApplies3 == "B"){?>checked<? } ?> type="checkbox" name="custDeductApplies3" value="B" /></td>
      <td>B</td>
      <td width="2px"></td>
      <td><input <? if($custDeductApplies4 == "M"){?>checked<? } ?> type="checkbox" name="custDeductApplies4" value="M" /></td>
      <td>M</td>
      <td width="20px">&nbsp;</td>
      <td width="40px"><b>Met</b></td>
      <td><input <? if($custMet == "Yes"){?>checked<? } ?> type="radio" name="custMet" value="Yes" /></td>
      <td>Yes</td>
      <td width="5px"></td>
      <td><input <? if($custMet == "No"){?>checked<? } ?> type="radio" name="custMet" value="No" /></td>
      <td>No</td>
      </tr>
    </table>  </td>
</tr>
<tr>
  <td colspan="8">
  	<table cellpadding="2" cellspacing="0">
      <tr>
      <td width="132px" style="padding-left:0px;"><b>Fam. Deductible $</b></td>
      <td><input type="text" class="textbox" id="custFamilyDeduct" name="custFamilyDeduct" value="<?=$custFamilyDeduct?>" style="width: 60px" /></td>
      <td width="10px">&nbsp;</td>
      <td width="40px"><b>Met</b></td>
      <td><input <? if($custMet2 == "Yes"){?>checked<? } ?> type="radio" name="custMet2" value="Yes" /></td>
      <td>Yes</td>
      <td width="5px"></td>
      <td><input <? if($custMet2 == "No"){?>checked<? } ?> type="radio" name="custMet2" value="No" /></td>
      <td>No</td>
      </tr>
    </table>  </td>
</tr>
<tr class="alternate">
  <td colspan="8">
  	<table cellpadding="1" cellspacing="0" border="0">
    <tr>
    <td nowrap="nowrap"><b>Basic</b></td>
    <td width="15px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custBasic" name="custBasic" value="<?=$custBasic?>" style="width: 60px" />%</td>
    <td width="25px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>Major</b></td>
    <td width="15px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custMajor" name="custMajor" value="<?=$custMajor?>" style="width: 60px" />%</td>
    <td width="25px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>Perio</b></td>
    <td width="15px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custPerio" name="custPerio" value="<?=$custPerio?>" style="width: 60px" />%</td>
    <td width="25px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>Endo</b></td>
    <td width="15px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custEndo" name="custEndo" value="<?=$custEndo?>" style="width: 60px" />%</td>
    <td width="25px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>OS</b></td>
    <td width="26px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custOs" name="custOs" value="<?=$custOs?>" style="width: 60px" />%</td>
    </tr>
    </table>  </td>
</tr>
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">Diagnostic & Preventative</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
<tr>
    <td width="130" nowrap="nowrap"><b>Complete Exam (0150)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPCompleteExam" name="custDPCompleteExam" value="<?=$custDPCompleteExam?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPCompleteExamFreq" name="custDPCompleteExamFreq" value="<?=$custDPCompleteExamFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td colspan="4">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="130" nowrap="nowrap"><b>Periodic Exam (0120)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custDPPeriodicExam" name="custDPPeriodicExam" value="<?=$custDPPeriodicExam?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>Freq</b></td>
            <td>
            <input type="text" class="textbox" id="custDPPeriodicExamFreq" name="custDPPeriodicExamFreq" value="<?=$custDPPeriodicExamFreq?>" style="width: 75px" />            </td>
        </tr>
        </table>
    </td>
    </tr>
<tr>
    <td nowrap="nowrap"><b>Limited Exam (0140)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPLimitedExam" name="custDPLimitedExam" value="<?=$custDPLimitedExam?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPLimitedExamFreq" name="custDPLimitedExamFreq" value="<?=$custDPLimitedExamFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td colspan="3">
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td nowrap="nowrap"><b>Separate from 0120?</b></td>
        <td><input <? if($custDPSeparate == "Yes"){?>checked<? } ?> type="radio" name="custDPSeparate" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custDPSeparate == "No"){?>checked<? } ?> type="radio" name="custDPSeparate" value="No" /></td>
        <td>No</td>
        <td>&nbsp;</td>
        <td nowrap="nowrap"><b>Allowed same day w/ TX?</b></td>
        <td><input <? if($custDPAllowed == "Yes"){?>checked<? } ?> type="radio" name="custDPAllowed" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custDPAllowed == "No"){?>checked<? } ?> type="radio" name="custDPAllowed" value="No" /></td>
        <td>No</td>
        </tr>
        </table>    </td>
    <td>&nbsp;</td>
</tr>

<tr>
    <td nowrap="nowrap"><b>FMX(0210)/Pano(0330)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPFMX" name="custDPFMX" value="<?=$custDPFMX?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPFMXFreq" name="custDPFMXFreq" value="<?=$custDPFMXFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td><input <? if($custDPShare == "Share"){?>checked<? } ?> type="radio" name="custDPShare" value="Share" /></td>
        <td nowrap="nowrap"><b>Share</b></td>
        <td><input <? if($custDPShare == "Separate"){?>checked<? } ?> type="radio" name="custDPShare" value="Separate" /></td>
        <td><b>Separate</b></td>
        <td width="30">&nbsp;</td>
        <td nowrap="nowrap"><b>Does FMX take freq of BW</b></td>
        <td width="5"></td>
        <td><input type="text" class="textbox" id="custDPFreqBw" name="custDPFreqBw" value="<?=$custDPFreqBw?>" style="width: 100px" /></td>
        </tr>
        </table>    </td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Bitewings (0270-74)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPBit" name="custDPBit" value="<?=$custDPBit?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPBitFreq" name="custDPBitFreq" value="<?=$custDPBitFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td>
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="130" nowrap="nowrap"><b>Periapicals (0220-30)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custDPPeriap" name="custDPPeriap" value="<?=$custDPPeriap?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custDPPeriapFreq" name="custDPPeriapFreq" value="<?=$custDPPeriapFreq?>" style="width: 75px" /></td>
            <td width="20px">&nbsp;</td>
            <td><b>Max?</b></td>
            <td><input type="text" class="textbox" id="custDPMax" name="custDPMax" value="<?=$custDPMax?>" style="width: 75px" /></td>
        </tr>
        </table>    </td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>

<tr>
    <td nowrap="nowrap"><b>Prophy (1110-20)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPProphy" name="custDPProphy" value="<?=$custDPProphy?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPProphyFreq" name="custDPProphyFreq" value="<?=$custDPProphyFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td>
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="130" nowrap="nowrap"><b>Fluoride (1208/1206)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custDPFluoride" name="custDPFluoride" value="<?=$custDPFluoride?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custDPFluorideFreq" name="custDPFluorideFreq" value="<?=$custDPFluorideFreq?>" style="width: 75px" /></td>
            <td width="20px">&nbsp;</td>
            <td nowrap="nowrap"><b>To Age?</b></td>
            <td><input type="text" class="textbox" id="custDPToAge" name="custDPToAge" value="<?=$custDPToAge?>" style="width: 75px" /></td>
        </tr>
        </table>
    </td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Sealants (1351)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPSealants" name="custDPSealants" value="<?=$custDPSealants?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPSealantsFreq" name="custDPSealantsFreq" value="<?=$custDPSealantsFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td nowrap="nowrap"><b>To Age?</b></td>
        <td><input type="text" class="textbox" id="custDPSealantsToAge" name="custDPSealantsToAge" value="<?=$custDPSealantsToAge?>" style="width: 30px" /></td>
        <td width="5"></td>
        <td><input <? if($custDPMolarsOnly == "Perm. Molars Only"){?>checked<? } ?> type="checkbox" name="custDPMolarsOnly" value="Perm. Molars Only" /></td>
        <td nowrap="nowrap"><b>Perm. Molars Only</b></td>
        <td width="5px">&nbsp;</td>
        <td><input <? if($custDPWisdom == "Excludes Wisdom"){?>checked<? } ?> type="checkbox" name="custDPWisdom" value="Excludes Wisdom" /></td>
        <td nowrap="nowrap"><b>Excludes Wisdom</b></td>
        <td width="5px">&nbsp;</td>
        <td><input <? if($custDPPremolars == "Premolars"){?>checked<? } ?> type="checkbox" name="custDPPremolars" value="Premolars" /></td>
        <td nowrap="nowrap"><b>Premolars</b></td>
        </tr>
        </table>    </td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Space Maintainers (1510)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPSpace" name="custDPSpace" value="<?=$custDPSpace?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPSpaceFreq" name="custDPSpaceFreq" value="<?=$custDPSpaceFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>To Age?</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custDPSpaceToAge" name="custDPSpaceToAge" value="<?=$custDPSpaceToAge?>" style="width: 30px" /></td>
            <td width="20px">&nbsp;</td>
        </tr>
        </table>
    </td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">Basic</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
  <tr>
    <td width="130" nowrap="nowrap"><b>Posterior Composites (2391-94)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custBaComposites" name="custBaComposites" value="<?=$custBaComposites?>" style="width: 50px" />%</td>
    <td width="10">&nbsp;</td>
    <td colspan="6">
    	<table cellpadding="1" cellspacing="0" border="0">
    	  <tr>
    	  <td nowrap="nowrap"><b>Downgrade?</b></td>
          <td><input <? if($custBaDowngrade == "Yes"){?>checked<? } ?> type="radio" name="custBaDowngrade" value="Yes" /></td>
          <td>Yes</td>
          <td width="5px">&nbsp;</td>
          <td><input <? if($custBaDowngrade == "No"){?>checked<? } ?> type="radio" name="custBaDowngrade" value="No" /></td>
          <td>No</td>
          <td width="20px">&nbsp;</td>
          <td><input <? if($custBaMolarsPre == "Molars"){?>checked<? } ?> type="radio" name="custBaMolarsPre" value="Molars" /></td>
          <td nowrap="nowrap"><b>Molars</b></td>
          <td><input <? if($custBaMolarsPre == "Premolars"){?>checked<? } ?> type="radio" name="custBaMolarsPre" value="Premolars" /></td>
          <td><b>Premolars</b></td>
          <td width="20">&nbsp;</td>
          <td width="30"><b>Freq</b></td>
          <td><input type="text" class="textbox" id="custBaMolarsPreFreq" name="custBaMolarsPreFreq" value="<?=$custBaMolarsPreFreq?>" style="width: 60px" /></td>
          </tr>
  	  </table></td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>SPR Deep Cleaning (4341-42)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custBaCleaning" name="custBaCleaning" value="<?=$custBaCleaning?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custBaCleaningFreq" name="custBaCleaningFreq" value="<?=$custBaCleaningFreq?>" style="width: 75px" /></td>
    <td width="10">&nbsp;</td>
    <td colspan="2" valign="top" nowrap="nowrap">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="100" nowrap="nowrap"><b>All 4 Same Day</b></td>
            <td><input <? if($custBaSameDay == "Yes"){?>checked<? } ?> type="radio" name="custBaSameDay" value="Yes" /></td>
            <td>Yes</td>
            <td width="5px">&nbsp;</td>
            <td><input <? if($custBaSameDay == "No"){?>checked<? } ?> type="radio" name="custBaSameDay" value="No" /></td>
            <td>No</td>
        </tr>
        </table>
        </td>
    </tr>
  <tr>
    <td nowrap="nowrap"><b>Full Mouth Debridement (4355)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custBaDebridement" name="custBaDebridement" value="<?=$custBaDebridement?>" style="width: 50px" /> %</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custBaDebridementFreq" name="custBaDebridementFreq" value="<?=$custBaDebridementFreq?>" style="width: 75px" /></td>
    <td width="10">&nbsp;</td>
    <td valign="top" nowrap="nowrap">
    	<table cellpadding="1" cellspacing="0" border="0">
    	  <tr>
    	  <td nowrap="nowrap"><b>Takes place of PROPHY? </b></td>
          <td><input <? if($custBaPXA == "Yes"){?>checked<? } ?> type="radio" name="custBaPXA" value="Yes" /></td>
          <td>Yes</td>
          <td width="5px">&nbsp;</td>
          <td><input <? if($custBaPXA == "No"){?>checked<? } ?> type="radio" name="custBaPXA" value="No" /></td>
          <td>No</td>
          </tr>
  	  </table>    </td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Perio Maintenance (4910)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custBaMaintenance" name="custBaMaintenance" value="<?=$custBaMaintenance?>" style="width: 50px" />
      %</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custBaMaintenanceFreq" name="custBaMaintenanceFreq" value="<?=$custBaMaintenanceFreq?>" style="width: 75px" /></td>
    <td width="10">&nbsp;</td>
    <td valign="top" nowrap="nowrap">
    	<table cellpadding="1" cellspacing="0">
        <tr>
        <td><input <? if($custBaCombInAdd == "Combined"){?>checked<? } ?> type="radio" name="custBaCombInAdd" value="Combined" /></td>
        <td><b>Combined</b></td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custBaCombInAdd == "In Addition to PROPHY"){?>checked<? } ?> type="radio" name="custBaCombInAdd" value="In Addition to PROPHY" /></td>
        <td><b>In Addition to PROPHY</b></td>
        <td width="15px">&nbsp;</td>
        </tr>
        </table>    </td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Desensitizing resin-cerv/root surf. (9911)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custBaDesensit" name="custBaDesensit" value="<?=$custBaDesensit?>" style="width: 50px" />
      %</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custBaDesensitFreq" name="custBaDesensitFreq" value="<?=$custBaDesensitFreq?>" style="width: 75px" /></td>
    <td width="10">&nbsp;</td>
    <td valign="top" nowrap="nowrap">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>SSC (D2930)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custBaSSC" name="custBaSSC" value="<?=$custBaSSC?>" style="width: 30px" />%</td>
            <td width="10px">&nbsp;</td>
            <td><b>N2O (9230)</b></td>
            <td><input type="text" class="textbox" id="custBaN2O" name="custBaN2O" value="<?=$custBaN2O?>" style="width: 30px" />%</td>
            <td width="10px">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custBaN2OFreq" name="custBaN2OFreq" value="<?=$custBaN2OFreq?>" style="width: 75px" /></td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>Occlusal Guards (9940)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custBaOc" name="custBaOc" value="<?=$custBaOc?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custBaOcFreq" name="custBaOcFreq" value="<?=$custBaOcFreq?>" style="width: 75px" /></td>
    <td width="10">&nbsp;</td>
    <td colspan="2" valign="top" nowrap="nowrap">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="100" nowrap="nowrap"><b>Age Minimum?</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custBaOcAgeMinimum" name="custBaOcAgeMinimum" value="<?=$custBaOcAgeMinimum?>" style="width: 75px" /></td>
            <td width="300">&nbsp;</td>
          </tr>
        </table>    </td>
    </tr>
    <tr>
    <td width="130" nowrap="nowrap"><b>Any Specific Guideline?</b></td>
    <td colspan="7" nowrap="nowrap">
    	<table cellpadding="1" cellspacing="0">
        <tr>
        <td><input <? if($custBaOcGuideline == "None"){?>checked<? } ?> type="radio" name="custBaOcGuideline" value="None" /></td>
        <td><b>None</b></td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custBaOcGuideline == "Bruxism"){?>checked<? } ?> type="radio" name="custBaOcGuideline" value="Bruxism" /></td>
        <td><b>Bruxism</b></td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custBaOcGuideline == "Osseous Surgery"){?>checked<? } ?> type="radio" name="custBaOcGuideline" value="Osseous Surgery" /></td>
        <td><b>Osseous Surgery</b></td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custBaOcGuideline == "Ortho"){?>checked<? } ?> type="radio" name="custBaOcGuideline" value="Ortho" /></td>
        <td><b>Ortho</b></td>
        </tr>
        </table>
    </td>
    </tr>
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">Major</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
<tr>
    <td nowrap="nowrap"><b>Posterior Crowns (2740)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custMaCrowns" name="custMaCrowns" value="<?=$custMaCrowns?>" style="width: 50px" />%</td>
    <td></td>
    <td colspan="6">
    	<table cellpadding="1" cellspacing="0" border="0">
    	  <tr>
    	  <td nowrap="nowrap"><b>Downgrade?</b></td>
          <td><input <? if($custMaCrownsDowngrade == "Yes"){?>checked<? } ?> type="radio" name="custMaCrownsDowngrade" value="Yes" /></td>
          <td>Yes</td>
          <td width="5px">&nbsp;</td>
          <td><input <? if($custMaCrownsDowngrade == "No"){?>checked<? } ?> type="radio" name="custMaCrownsDowngrade" value="No" /></td>
          <td>No</td>
          <td width="15px">&nbsp;</td>
          <td nowrap="nowrap"><b>Paid On</b></td>
          <td><input <? if($custMaPaidOn == "Prep"){?>checked<? } ?> type="radio" name="custMaPaidOn" value="Prep" /></td>
          <td nowrap="nowrap">Prep</td>
          <td><input <? if($custMaPaidOn == "Seat Date"){?>checked<? } ?> type="radio" name="custMaPaidOn" value="Seat Date" /></td>
          <td nowrap="nowrap">Seat Date</td>
          <td width="15">&nbsp;</td>
          <td nowrap="nowrap"><b>Build Up (2950)</b></td>
          <td><input type="text" class="textbox" id="custMaBuildUp" name="custMaBuildUp" value="<?=$custMaBuildUp?>" style="width: 60px" /></td>
          </tr>
  	  </table>    </td>
    </tr>
	<tr>
    <td nowrap="nowrap"><b>Pulp Cap (D3110)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custMaPulpCap" name="custMaPulpCap" value="<?=$custMaPulpCap?>" style="width: 50px" />%</td>
    <td></td>
    <td><b>Freq</b></td>
    <td width="80"><input type="text" class="textbox" id="custMaPulpCapFreq" name="custMaPulpCapFreq" value="<?=$custMaPulpCapFreq?>" style="width: 60px" />    </td>
    <td width="5"></td>
    <td colspan="3" valign="top">
    	<table cellpadding="1" cellspacing="0" border="0">
    	  <tr>
    	    <td nowrap="nowrap"><b>Palliative (D9110)</b></td>
          	<td nowrap="nowrap"><input type="text" class="textbox" id="custMaPalliative" name="custMaPalliative" value="<?=$custMaPalliative?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custMaPalliativeFreq" name="custMaPalliativeFreq" value="<?=$custMaPalliativeFreq?>" style="width: 75px" /></td>
          </tr>
  	  </table></td>
    </tr>
<tr>
    <td nowrap="nowrap"><b>Implant all ceramic crown (6058)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custMaCeramicCrown" name="custMaCeramicCrown" value="<?=$custMaCeramicCrown?>" style="width: 50px" />
      %</td>
    <td></td>
    <td colspan="6">
    	<table cellpadding="1" cellspacing="0" border="0">
    	  <tr>
    	  <td><b>Custom Abutment (6057)</b></td>
    	  <td></td>
          <td><input type="text" class="textbox" id="custMaCustomAbutment" name="custMaCustomAbutment" value="<?=$custMaCustomAbutment?>" style="width: 60px" /></td>
          </tr>
        </table>
    </td>
    </tr>
<tr>
    <td nowrap="nowrap"><b>Replacement time for prosthetics</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custMaReplacementTime" name="custMaReplacementTime" value="<?=$custMaReplacementTime?>" style="width: 50px" />%</td>
    <td nowrap="nowrap">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custMaReplacementTimeFreq" name="custMaReplacementTimeFreq" value="<?=$custMaReplacementTimeFreq?>" style="width: 60px" /></td>
    <td></td>
    <td valign="top">&nbsp;</td>
    <td width="10">&nbsp;</td>
    <td>      </td>
</tr>
</table>

<br />
<table width="100%" cellpadding="3" cellspacing="0">
<tr class="titleTr">
	<td></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
  <tr>
    <td width="500" nowrap="nowrap"><b>Can Prophy be done prior to D4341 or D4342?</b></td>
    <td colspan="3" nowrap="nowrap">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td><input <? if($custUProphy == "Yes"){?>checked<? } ?> type="radio" name="custUProphy" value="Yes" /></td>
            <td>Yes</td>
            <td width="5px">&nbsp;</td>
            <td><input <? if($custUProphy == "No"){?>checked<? } ?> type="radio" name="custUProphy" value="No" /></td>
            <td>No</td>
            <td width="20"></td>
            <td nowrap="nowrap"><b>How many days prior?</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custUProphyDays" name="custUProphyDays" value="<?=$custUProphyDays?>" style="width: 50px" /></td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="500" nowrap="nowrap"><b>Restorative (composites & amalgam) Frequency for redo for same tooth/surfaces?</b></td>
    <td colspan="3" nowrap="nowrap"><input type="text" class="textbox" id="custURestorative" name="custURestorative" value="<?=$custURestorative?>" style="width: 100px" /></td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>D2740 Ceramic Crowns downgrade to 02790 High Nobel Metal?</b></td>
    <td colspan="3" nowrap="nowrap">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td><input <? if($custUCeramicCrowns == "Yes"){?>checked<? } ?> type="radio" name="custUCeramicCrowns" value="Yes" /></td>
            <td>Yes</td>
            <td width="5px">&nbsp;</td>
            <td><input <? if($custUCeramicCrowns == "No"){?>checked<? } ?> type="radio" name="custUCeramicCrowns" value="No" /></td>
            <td>No</td>
        </tr>
        </table>    </td>
  </tr>
  
  <tr>
    <td nowrap="nowrap"><b>D2740 Ceramic Crowns downgrade to D2791 base Metal?</b></td>
    <td colspan="3" nowrap="nowrap">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td><input <? if($custUCrownsBase == "Yes"){?>checked<? } ?> type="radio" name="custUCrownsBase" value="Yes" /></td>
            <td>Yes</td>
            <td width="5px">&nbsp;</td>
            <td><input <? if($custUCrownsBase == "No"){?>checked<? } ?> type="radio" name="custUCrownsBase" value="No" /></td>
            <td>No</td>
        </tr>
        </table>    </td>
  </tr>
  
  <tr>
    <td nowrap="nowrap"><b>D2740 Ceramic Crowns downgrade to D2792 Nobel Metal?</b></td>
    <td colspan="3" nowrap="nowrap">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td><input <? if($custUCrownsNobel == "Yes"){?>checked<? } ?> type="radio" name="custUCrownsNobel" value="Yes" /></td>
            <td>Yes</td>
            <td width="5px">&nbsp;</td>
            <td><input <? if($custUCrownsNobel == "No"){?>checked<? } ?> type="radio" name="custUCrownsNobel" value="No" /></td>
            <td>No</td>
        </tr>
        </table>    </td>
  </tr>
 
  <tr>
    <td nowrap="nowrap"><b>D4212 Gingivectomy to allow access</b></td>
    <td width="80" nowrap="nowrap"><input type="text" class="textbox" id="custUGingivectomy" name="custUGingivectomy" value="<?=$custUGingivectomy?>" style="width: 50px" />%</td>
    <td width="2">&nbsp;</td>
    <td>
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Freq</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custUGingivectomyFreq" name="custUGingivectomyFreq" value="<?=$custUGingivectomyFreq?>" style="width: 50px" /></td>
          </tr>
        </table>    </td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>D7510 Incision and Drainage</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custUIncision" name="custUIncision" value="<?=$custUIncision?>" style="width: 50px" />%</td>
    <td>&nbsp;</td>
    <td>
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Freq</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custUIncisionFreq" name="custUIncisionFreq" value="<?=$custUIncisionFreq?>" style="width: 50px" /></td>
          </tr>
        </table>    </td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>D5820 and 05821 Interim Partial</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custUInterim" name="custUInterim" value="<?=$custUInterim?>" style="width: 50px" />%</td>
    <td>&nbsp;</td>
    <td>
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Freq</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custUInterimFreq" name="custUInterimFreq" value="<?=$custUInterimFreq?>" style="width: 50px" /></td>
          </tr>
        </table>    </td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>D2940 Sedative Filling</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custUSedative" name="custUSedative" value="<?=$custUSedative?>" style="width: 50px" />%</td>
    <td>&nbsp;</td>
    <td>
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Freq</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custUSedativeFreq" name="custUSedativeFreq" value="<?=$custUSedativeFreq?>" style="width: 50px" /></td>
          </tr>
        </table>    </td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>D9910 Application of desensitizing</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custUApp" name="custUApp" value="<?=$custUApp?>" style="width: 50px" />%</td>
    <td>&nbsp;</td>
    <td>
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Freq</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custUAppFreq" name="custUAppFreq" value="<?=$custUAppFreq?>" style="width: 50px" /></td>
            <td width="20"></td>
            <td nowrap="nowrap"><b>Fee</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custUAppFee" name="custUAppFee" value="<?=$custUAppFee?>" style="width: 50px" /></td>
          </tr>
        </table>    </td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>D0460 Pulp Vitality Test</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custUPulp" name="custUPulp" value="<?=$custUPulp?>" style="width: 50px" />%</td>
    <td>&nbsp;</td>
    <td>
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Freq</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custUPulpFreq" name="custUPulpFreq" value="<?=$custUPulpFreq?>" style="width: 50px" /></td>
          </tr>
        </table>    </td>
  </tr>
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">History</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
  <tr>
    <td width="60" nowrap="nowrap"><b>History</b></td>
    <td nowrap="nowrap"><textarea class="textbox" id="custHistory" name="custHistory" rows="2"  style="width: 800px"><?=$custHistory?></textarea></td>
    </tr>
</table>

<br />

<?php
//$date = date_create(date('y-m-d'), timezone_open('Pacific/Nauru'));
$date = putenv("TZ=US/Pacific");
$pacific_time = date("h:i:s");
$todate = date("Y-m-d");

if($custDateTime == "")
{
	$custDateTime = $todate . " ". $pacific_time;
}
?>


  <table width="100%" cellpadding="5" cellspacing="0">
    <tr class="titleTr">
      <td colspan="4"><label id="rightLabel">Special Request</label></td>
    </tr>
  </table>
  <table cellpadding="3" cellspacing="5" width="100%" style="border:1px solid; background-color: #EFEFEF;">
    <tr>
      <td colspan="4"><b>Special Request</b></td>
    </tr>
    <tr>
      <td colspan="4">
      <textarea name="custSpecialRequest" id="custSpecialRequest" cols="100" rows="5"><?=$custSpecialRequest?></textarea>
      <script type="text/javascript">
	  CKEDITOR.replace( 'custSpecialRequest' );
	  </script>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td width="90px" valign="top"><b>Spoke with:</b></td>
      <td width="250px"><input type="text" class="textbox" id="custSpokeWith" name="custSpokeWith" value="<?=$custSpokeWith?>" style="width: 200px" /></td>
      <td width="140px" valign="top"><b>Account Executive:</b></td>
      <td><input type="text" class="textbox" id="custAccountExecutive" name="custAccountExecutive" value="<?=$custAccountExecutive?>" style="width: 200px" /></td>
    </tr>
    <tr>
      <td width="80px" valign="top"><b>Date/Time:</b></td>
      <td colspan="3"><input type="text" class="textbox" id="custDateTime" name="custDateTime" value="<?=date("m/d/Y h:i:s")?>" style="width: 200px" />
        <br />
        <p>Last Date / Time:
          <?=$custDateTime?>
        </p></td>
    </tr>
  </table>
</div>
