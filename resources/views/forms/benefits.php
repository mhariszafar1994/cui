<div id="div<?=$divCounter+=1?>" <? if($patientEligibilityCustom == "1"){ ?>style="display: none;"<? } ?>>
<table cellpadding="3" cellspacing="0">
<tr>
<td width="130px"><b>Electronic Payer ID#</b></td>
<td colspan="8"><input type="text" class="textbox" id="benPayerId" name="benPayerId" value="<?=$benPayerId?>" style="width: 100px" /></td>
</tr>
<tr class="alternate">
<td width="130px"><b>Coverage</b></td>
<td colspan="8">
<!--<input type="text" class="textbox" id="benCoverage" name="benCoverage" value="<?=$benCoverage?>" style="width: 100px" />-->
<select name="benCoverage" id="benCoverage">
<option value="">Select Coverage</option>
<?if($benCoverage=="Single"){?>
    <option value="Single" selected="selected">Single</option>
    <option value="Family">Family</option>
    <?} elseif($benCoverage=="Family"){?>
    <option value="Single">Single</option>
    <option value="Family" selected="selected">Family</option>
    <?} else {?>
    <option value="Single">Single</option>
    <option value="Family">Family</option>
<?}?>
</select>
</td>
</tr>

<? if($_SESSION["tmpSessionCompanyId"] == "17" || $_SESSION["tmpSessionCompanyId"] == "18" 
 || $_SESSION["tmpSessionCompanyId"] == "34" || $_SESSION["tmpSessionCompanyId"] == "35" 
 || $_SESSION["tmpSessionCompanyId"] == "36" || $_SESSION["tmpSessionCompanyId"] == "37"
 || $_SESSION["tmpSessionCompanyId"] == "38" || $_SESSION["tmpSessionCompanyId"] == "39"
 || $_SESSION["tmpSessionCompanyId"] == "40" || $_SESSION["tmpSessionCompanyId"] == "41"
 || $_SESSION["tmpSessionCompanyId"] == "42" || $_SESSION["tmpSessionCompanyId"] == "43"
 || $_SESSION["tmpSessionCompanyId"] == "44" || $_SESSION["tmpSessionCompanyId"] == "45"
 || $_SESSION["tmpSessionCompanyId"] == "46" || $_SESSION["tmpSessionCompanyId"] == "47"
 || $_SESSION["tmpSessionCompanyId"] == "50" || $_SESSION["tmpSessionCompanyId"] == "51"
 || $_SESSION["tmpSessionCompanyId"] == "52" || $_SESSION["tmpSessionCompanyId"] == "53"
 || $_SESSION["tmpSessionCompanyId"] == "54" || $_SESSION["tmpSessionCompanyId"] == "55"
 || $_SESSION["tmpSessionCompanyId"] == "59" || $_SESSION["tmpSessionCompanyId"] == "60"
 || $_SESSION["tmpSessionCompanyId"] == "61" || $_SESSION["tmpSessionCompanyId"] == "62"
 || $_SESSION["tmpSessionCompanyId"] == "65" || $_SESSION["tmpSessionCompanyId"] == "66"
 || $_SESSION["tmpSessionCompanyId"] == "67" || $_SESSION["tmpSessionCompanyId"] == "68"
 || $_SESSION["tmpSessionCompanyId"] == "69" || $_SESSION["tmpSessionCompanyId"] == "70"
 || $_SESSION["tmpSessionCompanyId"] == "71" || $_SESSION["tmpSessionCompanyId"] == "72"
 || $_SESSION["tmpSessionCompanyId"] == "73" || $_SESSION["tmpSessionCompanyId"] == "74"
 || $_SESSION["tmpSessionCompanyId"] == "75" || $_SESSION["tmpSessionCompanyId"] == "76"
 || $_SESSION["tmpSessionCompanyId"] == "77" || $_SESSION["tmpSessionCompanyId"] == "78"
 ){ ?>
<tr>
<td width="130px"><b>Term Date</b></td>
<td colspan="8"><input readonly type="text" class="textbox" id="benTermDate" name="benTermDate" value="<?=$benTermDate?>" style="width: 60px" /><input type="button" value="Select" class="smallButton" onclick="displayDatePicker('benTermDate');" /></td>
</tr>
<? }else{ ?>
<input type="hidden" id="benTermDate" name="benTermDate" value="" />
<? } ?>

<tr>
<td width="130px"><b>Effective Date</b></td>
<td colspan="8"><input readonly type="text" class="textbox" id="benEffectiveDate" name="benEffectiveDate" value="<?=$benEffectiveDate?>" style="width: 60px" /><input type="button" value="Select" class="smallButton" onclick="displayDatePicker('benEffectiveDate');" /></td>
</tr>

<tr class="alternate">
<td width="130px"><b>Max Allowance $</b></td>
<td><input type="text" class="textbox" id="benMaxAllowance" name="benMaxAllowance" value="<?=$benMaxAllowance?>" style="width: 100px" /></td>
<td width="10px">&nbsp;</td>
<td width="130px"><b>Max Remaining $</b></td>
<td><input type="text" class="textbox" id="benMaxRemaining" name="benMaxRemaining" value="<?=$benMaxRemaining?>" style="width: 100px" /></td>
<td width="10px">&nbsp;</td>
<td width="160px"><b>Max (if out of network) $</b></td>
<td><input type="text" class="textbox" id="benMaxNetwork" name="benMaxNetwork" value="<?=$benMaxNetwork?>" style="width: 100px" /></td>
</tr>
<tr>
<td><b>Calendar Options</b></td>
<td colspan="7">
<table cellpadding="1" cellspacing="0">
<tr>
<td width="20px"><input <?if($benCalender == "Calender"){?>checked<? } ?> type="radio" name="benCalender" value="Calender" /></td>
<td>Calendar</td>
<td width="15px">&nbsp;</td>
<td width="20px"><input <?if($benCalender == "Fiscal Year"){?>checked<? } ?> type="radio" name="benCalender" value="Fiscal Year" /></td>
<td>Fiscal Year</td>
<td width="5px">&nbsp;</td>
<td>
<select name="benFiscalStartMonth">
<option value="01">MM</option>
<?
for ($i=1;$i<13;$i++){
    $j = $i;
    if($i<10){
        $j = "0".$i;
    }
    //selected
    $sel = "";
    if($j == $benFiscalStartMonth){
        $sel = "selected";
    }
    echo "<option $sel value=\"$j\">$j</option>";
}
?>
</select>
<select name="benFiscalStartDay">
<option value="01">DD</option>
<?
for ($i=1;$i<32;$i++){
    $j = $i;
    if($i<10){
        $j = "0".$i;
    }
    //selected
    $sel = "";
    if($j == $benFiscalStartDay){
        $sel = "selected";
    }
    echo "<option $sel value=\"$j\">$j</option>";
}
?>
</select>
</td>
<td width="25px" align="center">To</td>
<td>
<select name="benFiscalEndMonth">
<option value="01">MM</option>
<?
for ($i=1;$i<13;$i++){
    $j = $i;
    if($i<10){
        $j = "0".$i;
    }
    //selected
    $sel = "";
    if($j == $benFiscalEndMonth){
        $sel = "selected";
    }
    echo "<option $sel value=\"$j\">$j</option>";
}
?>
</select>
<select name="benFiscalEndDay">
<option value="01">DD</option>
<?
for ($i=1;$i<32;$i++){
    $j = $i;
    if($i<10){
        $j = "0".$i;
    }
    //selected
    $sel = "";
    if($j == $benFiscalEndDay){
        $sel = "selected";
    }
    echo "<option $sel value=\"$j\">$j</option>";
}
?>
</select>
</td>
<td width="20px">&nbsp;</td>
<td width="20px"><input <?if($benCalender == "Other"){?>checked<? } ?> type="radio" name="benCalender" value="Other" /></td>
<td>Other</td>
<td width="5px">&nbsp;</td>
<td><input type="text" name="benOther" class="textbox" style="width: 100px" value="<?=$benOther?>" /></td>
</tr>
</table>
</td>
</tr>
<tr class="alternate">
<td valign="top"><b>Fee Schedule</b></td>
<td colspan="7">
<table cellpadding="2" cellspacing="0">
<tr>
<td valign="top" width="20px"><input <?if($benSchedule == "UCR"){?>checked<? } ?> type="radio" name="benSchedule" value="UCR" /></td>
<td valign="top">UCR</td>
<td width="15px">&nbsp;</td>
<td valign="top" width="20px"><input <?if($benSchedule == "PPO"){?>checked<? } ?> type="radio" name="benSchedule" value="PPO" /></td>
<td valign="top">PPO</td>
<td width="15px">&nbsp;</td>
<td valign="top" width="20px"><input <?if($benSchedule == "Fee"){?>checked<? } ?> type="radio" name="benSchedule" value="Fee" /></td>
<td valign="top">Fee Schedule (If fee Schedule then ask Insurance company can we have it Faxed or emailed to us and if not how do we get the copy.)</td>
</tr>
<tr>
<td colspan="7">&nbsp;</td>
<td><textarea name="benScheduleDesc" class="textbox" style="width: 95%"><?=$benScheduleDesc?></textarea></td>
</tr>
</table>
</td>
</tr>
<tr>
<td><b>Is this an incentive plan?</b></td>
<td colspan="7">
<table cellpadding="1" cellspacing="0">
<tr>
<td><input <?if($benIncentive == "Yes"){?>checked<? } ?> type="radio" name="benIncentive" value="Yes" /></td>
<td>Yes</td>
<td width="20px">&nbsp;</td>
<td><input <?if($benIncentive == "No"){?>checked<? } ?> type="radio" name="benIncentive" value="No" /></td>
<td>No</td>
<Td width="20px">&nbsp;</Td>
<td>If Yes (%)</td>
<td width="5px">&nbsp;</td>
<td><input type="text" name="benIncentivePer" class="textbox" style="width: 100px" value="<?=$benIncentivePer?>" /></td>
</tr>
</table>
</td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
<tr>
<td width="170px"><b>Coordination of benefits</b></td>
<td>
<table cellpadding="1" cellspacing="0">
<tr>
<td><input name="insurBenefits" type="radio" <? if($insurBenefits == "Standard"){?>checked<? } ?> value="Standard"/></td>
<td width="30px">Standard</td>
<td width="5px"><input name="insurBenefits" type="radio" <? if($insurBenefits == "Non-duplication"){?>checked<? } ?> value="Non-duplication"/></td>
<td>Non-duplication</td>
</tr>
</table>
</td>
</tr>
</table>