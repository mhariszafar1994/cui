<div id="div<?=$divCounter+=1?>" style="display: none;">
<table cellpadding="3" cellspacing="0" width="100%">
		<tr>
			<td width="200px"><b>Any outstanding pre-auths?</b></td>
			<td>
				<input type="text" class="textbox" id="codeOutstanding" name="codeOutstanding" value="<?=$codeOutstanding?>" style="width:300px;"/>
			</td>
		</tr>
		<tr class="alternate">
			<td width="200px"><b>Any pending claims</b></td>
			<td>
				<input type="text" class="textbox" id="codePending" name="codePending" value="<?=$codePending?>" style="width:300px;"/>
			</td>
		</tr>
		</table>
		<table cellpadding="3" cellspacing="0" width="100%">
		<tr>
			<td width="260px"><b>History of procedures if yes, teeth # or dates</b></td>
			<td>
					<input type="text" class="textbox" id="codeHistory" name="codeHistory" value="<?=$codeHistory?>" style="width:300px;"/>
			</td>
		</tr>
		</table>
		<table cellpadding="3" cellspacing="0" width="100%">
		<tr>
			<td width="100px" style="padding-left:25px;"><b>Crowns (2750)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td><input name="codeCrowns2750" type="radio" <? if($codeCrowns2750 == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="codeCrowns2750" type="radio" <? if($codeCrowns2750 == "No"){?>checked<? } ?> value="No"/></td>
						<td width="10px">No</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="100px" style="padding-left:25px;"><b>History of FMX</b></td>
			<td>
				<table cellpadding="1" cellspacing="0" style="width: 75%;">
					<tr>
						<td><input name="codeFmxHistory" type="radio" <? if($codeFmxHistory == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="codeFmxHistory" type="radio" <? if($codeFmxHistory == "No"){?>checked<? } ?> value="No"/></td>
						<td width="25px">No</td>
						<td width="100px">If Yes, then date</td>
						<td><input type="text" class="textbox" id="codeFmxDate" name="codeFmxDate" value="<?=$codeFmxDate?>" style="width:100px;" readonly="yes" /> <img src="<?=HTTP_SERVER?>images/calendar.gif" alt="Calendar" onclick="displayDatePicker('codeFmxDate', this);"> <img src="<?=HTTP_SERVER?>images/icon_delete.png" alt="Calendar" onclick="document.getElementById('codeFmxDate').value=''"></td>
					</tr>
				</table>
			</td>
		</tr>
		</table>
		<table cellpadding="3" cellspacing="0" width="100%">
		<tr>
			<td width="200px" style="padding-left:25px;"><b>Scaling & root Planing (4341)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td><input name="codeScaling" type="radio" <? if($codeScaling == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="codeScaling" type="radio" <? if($codeScaling == "No"){?>checked<? } ?> value="No"/></td>
						<td>No</td>													
					</tr>
				</table>
			</td>
		</tr>
		</table>
		<table cellpadding="3" cellspacing="0" width="100%">
		<tr>
			<td width="50px" style="padding-left:25px;"><b>If yes</b></td>
			<td>
				<table cellpadding="1" cellspacing="0" style="width: 75%;">
					<tr>
						<td>UR</td>
						<td><input type="text" class="textbox" id="codeScalingUR" name="codeScalingUR" value="<?=$codeScalingUR?>" style="width:100px;" readonly="yes" /> <img src="<?=HTTP_SERVER?>images/calendar.gif" alt="Calendar" onclick="displayDatePicker('codeScalingUR', this);"> <img src="<?=HTTP_SERVER?>images/icon_delete.png" alt="Calendar" onclick="document.getElementById('codeScalingUR').value=''"></td>
						<td width="10px"></td>
						<td>UL</td>
						<td><input type="text" class="textbox" id="codeScalingUL" name="codeScalingUL" value="<?=$codeScalingUL?>" style="width:100px;" readonly="yes" /> <img src="<?=HTTP_SERVER?>images/calendar.gif" alt="Calendar" onclick="displayDatePicker('codeScalingUL', this);"> <img src="<?=HTTP_SERVER?>images/icon_delete.png" alt="Calendar" onclick="document.getElementById('codeScalingUL').value=''"></td>	
						<td width="10px"></td>
						<td>LL</td>
						<td><input type="text" class="textbox" id="codeScalingLL" name="codeScalingLL" value="<?=$codeScalingLL?>" style="width:100px;" readonly="yes" /> <img src="<?=HTTP_SERVER?>images/calendar.gif" alt="Calendar" onclick="displayDatePicker('codeScalingLL', this);"> <img src="<?=HTTP_SERVER?>images/icon_delete.png" alt="Calendar" onclick="document.getElementById('codeScalingLL').value=''"></td>
						<td width="10px"></td>
						<td>LR</td>
						<td><input type="text" class="textbox" id="codeScalingLR" name="codeScalingLR" value="<?=$codeScalingLR?>" style="width:100px;" readonly="yes" /> <img src="<?=HTTP_SERVER?>images/calendar.gif" alt="Calendar" onclick="displayDatePicker('codeScalingLR', this);"> <img src="<?=HTTP_SERVER?>images/icon_delete.png" alt="Calendar" onclick="document.getElementById('codeScalingLR').value=''"></td>											
					</tr>
				</table>
			</td>
		</tr>
		</table>
		<table cellpadding="3" cellspacing="0" width="100%">
		<tr>
			<td width="120px" style="padding-left:25px;"><b>History of Bite Wings</b></td>
			<td>
				<table cellpadding="1" cellspacing="0" style="width: 75%;">
					<tr>
						<td style="width:55px"><input name="codeScalingBite" type="radio" <? if($codeScalingBite == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="codeScalingBite" type="radio" <? if($codeScalingBite == "No"){?>checked<? } ?> value="No"/></td>
						<td width="25px">No</td>
						<td width="100px">If Yes, then date</td>
						<td><input type="text" class="textbox" id="codeScalingBitDate" name="codeScalingBitDate" value="<?=$codeScalingBitDate?>" style="width:100px;" readonly="yes" /> <img src="<?=HTTP_SERVER?>images/calendar.gif" alt="Calendar" onclick="displayDatePicker('codeScalingBitDate', this);"> <img src="<?=HTTP_SERVER?>images/icon_delete.png" alt="Calendar" onclick="document.getElementById('codeScalingBitDate').value=''"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="120px" style="padding-left:25px;"><b>History of Prophy</b></td>
			<td>
				<table cellpadding="1" cellspacing="0" style="width: 75%;">
					<tr>
						<td style="width:55px"><input name="codeScalingProphy" type="radio" <? if($codeScalingProphy == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="codeScalingProphy" type="radio" <? if($codeScalingProphy == "No"){?>checked<? } ?> value="No"/></td>
						<td width="25px">No</td>
						<td width="100px">If Yes, then date</td>
						<td><input type="text" class="textbox" id="codeScalingProphyDate" name="codeScalingProphyDate" value="<?=$codeScalingProphyDate?>" style="width:100px;" readonly="yes" /> <img src="<?=HTTP_SERVER?>images/calendar.gif" alt="Calendar" onclick="displayDatePicker('codeScalingProphyDate', this);"> <img src="<?=HTTP_SERVER?>images/icon_delete.png" alt="Calendar" onclick="document.getElementById('codeScalingProphyDate').value=''"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="120px" style="padding-left:25px;"><b>History of Flouride</b></td>
			<td>
				<table cellpadding="1" cellspacing="0" style="width: 75%;">
					<tr>
						<td style="width:55px"><input name="codeScalingFlouride" type="radio" <? if($codeScalingFlouride == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="codeScalingFlouride" type="radio" <? if($codeScalingFlouride == "No"){?>checked<? } ?> value="No"/></td>
						<td width="25px">No</td>
						<td width="100px">If Yes, then date</td>
						<td><input type="text" class="textbox" id="codeScalingFlourideDate" name="codeScalingFlourideDate" value="<?=$codeScalingFlourideDate?>" style="width:100px;" readonly="yes" /> <img src="<?=HTTP_SERVER?>images/calendar.gif" alt="Calendar" onclick="displayDatePicker('codeScalingFlourideDate', this);"> <img src="<?=HTTP_SERVER?>images/icon_delete.png" alt="Calendar" onclick="document.getElementById('codeScalingFlourideDate').value=''"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="120px" style="padding-left:25px;"><b>History of Exams</b></td>
			<td>
				<table cellpadding="1" cellspacing="0" style="width: 75%;">
					<tr>
						<td style="width:55px"><input name="codeScalingExams" type="radio" <? if($codeScalingExams == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="codeScalingExams" type="radio" <? if($codeScalingExams == "No"){?>checked<? } ?> value="No"/></td>
						<td width="25px">No</td>
						<td width="100px">If Yes, then date</td>
						<td><input type="text" class="textbox" id="codeScalingExamsDate" name="codeScalingExamsDate" value="<?=$codeScalingExamsDate?>" style="width:100px;" readonly="yes" /> <img src="<?=HTTP_SERVER?>images/calendar.gif" alt="Calendar" onclick="displayDatePicker('codeScalingExamsDate', this);"> <img src="<?=HTTP_SERVER?>images/icon_delete.png" alt="Calendar" onclick="document.getElementById('codeScalingExamsDate').value=''"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="120px" style="padding-left:25px;"><b>History of Sealants (1351)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0" style="width: 75%;">
					<tr>
						<td style="width:55px"><input name="codeSealants" type="radio" <? if($codeSealants == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="codeSealants" type="radio" <? if($codeSealants == "No"){?>checked<? } ?> value="No"/></td>
						<td width="25px">No</td>
						<td width="100px">If Yes, then date</td>
						<td><input type="text" class="textbox" id="codeSealantsDate" name="codeSealantsDate" value="<?=$codeSealantsDate?>" style="width:100px;" readonly="yes" /> <img src="<?=HTTP_SERVER?>images/calendar.gif" alt="Calendar" onclick="displayDatePicker('codeSealantsDate', this);"> <img src="<?=HTTP_SERVER?>images/icon_delete.png" alt="Calendar" onclick="document.getElementById('codeSealantsDate').value=''"></td>
						<td width="15px">&nbsp;</td>
						<td width="50px">Teeth #</td>
						<td><input type="text" class="textbox" id="codeSealantsTeeth" name="codeSealantsTeeth" value="<?=$codeSealantsTeeth?>" style="width: 80px"/></td>
					</tr>
					<tr>
						<td colspan="5">&nbsp;</td>
						<td><input type="text" class="textbox" id="codeSealantsDate2" name="codeSealantsDate2" value="<?=$codeSealantsDate2?>" style="width:100px;" readonly="yes" /> <img src="<?=HTTP_SERVER?>images/calendar.gif" alt="Calendar" onclick="displayDatePicker('codeSealantsDate2', this);"> <img src="<?=HTTP_SERVER?>images/icon_delete.png" alt="Calendar" onclick="document.getElementById('codeSealantsDate2').value=''"></td>
						<td width="15px">&nbsp;</td>
						<td width="50px">Teeth #</td>
						<td><input type="text" class="textbox" id="codeSealantsTeeth2" name="codeSealantsTeeth2" value="<?=$codeSealantsTeeth2?>" style="width: 80px"/></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>	
</div>