<div id="div<?=$divCounter+=1?>" style="display: none;">
	<table cellpadding="3" cellspacing="0" width="100%">
		
		<tr>
			<td width="320px"><b>Posterior composites e.g (2391)-Covered</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td colspan="5px"><input name="basicPosterior" type="radio" <? if($basicPosterior == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="basicPosterior" type="radio" <? if($basicPosterior == "No"){?>checked<? } ?> value="No"/></td>
						<td width="10px">No</td>
						<td width="20px">&nbsp;</td>
						<td>Any Limitations</td>
						<td>&nbsp;</td>
						<td><input type="text" class="textbox" id="basicPosteriorLimit" name="basicPosteriorLimit" value="<?=$basicPosteriorLimit?>" style="width: 200px" /></td>
					</tr>
				</table>
			</td>										
		</tr>
		<tr class="alternate">
			<td width="320px"><b>Bicuspids e.g (tooth #4) considered anterior or posterior</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td colspan="5px"><input name="basicBicuspids" type="radio" <? if($basicBicuspids == "Anterior"){?>checked<? } ?> value="Anterior"/></td>
						<td width="30px">Anterior</td>
						<td width="5px"><input name="basicBicuspids" type="radio" <? if($basicBicuspids == "Posterior"){?>checked<? } ?> value="Posterior"/></td>
						<td width="10px">Posterior</td>
					</tr>
				</table>
			</td>										
		</tr>
		<tr>
			<td width="320px"><b>Endodontics e.g (3310)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td width="75px"><input type="text" class="textbox" id="basicEndodonticsPer" name="basicEndodonticsPer" value="<?=$basicEndodonticsPer?>" style="width: 50px" /> %</td>
						<td width="30px">AND</td>
						<td width="5px"><input name="basicEndodontics" type="radio" <? if($basicEndodontics == "Basic"){?>checked<? } ?> value="Basic"/></td>
						<td width="40px">Basic</td>
						<td width="25px">OR</td>
						<td width="5px"><input name="basicEndodontics" type="radio" <? if($basicEndodontics == "Major"){?>checked<? } ?> value="Major"/></td>
						<td width="10px">Major</td>
					</tr>
				</table>
			</td>										
		</tr>
		<tr>
			<td width="320px" style="padding-left: 25px;"><b>Out of Network (If doctor out of network ask question)</b></td>
			<td>
				<div style="margin-left:3px !important;"><input type="text" class="textbox" id="basicEndodonticsOut" name="basicEndodonticsOut" value="<?=$basicEndodonticsOut?>" style="width: 50px" /> %</div>
			</td>
		</tr>
		<tr class="alternate">
			<td width="320px"><b>Retreat (3348)</b></td>
			<td>
				<div style="margin-left:3px !important;"><input type="text" class="textbox" id="basicRetreat" name="basicRetreat" value="<?=$basicRetreat?>" style="width: 50px" /> %</div>
			</td>
		</tr>
		<tr>
			<td width="320px"><b>Periodontal e.g (4341)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td width="75px"><input type="text" class="textbox" id="basicPeriodontalPer" name="basicPeriodontalPer" value="<?=$basicPeriodontalPer?>" style="width: 50px" /> %</td>
						<td width="30px">AND</td>
						<td width="5px"><input name="basicPeriodontal" type="radio" <? if($basicPeriodontal == "Basic"){?>checked<? } ?> value="Basic"/></td>
						<td width="40px">Basic</td>
						<td width="25px">OR</td>
						<td width="5px"><input name="basicPeriodontal" type="radio" <? if($basicPeriodontal == "Major"){?>checked<? } ?> value="Major"/></td>
						<td width="10px">Major</td>
					</tr>
				</table>
			</td>										
		</tr>
		<tr>
			<td width="320px" style="padding-left: 25px;"><b>Out of Network (If doctor out of network ask question)</b></td>
			<td>
				<div style="margin-left:3px !important;"><input type="text" class="textbox" id="basicPeriodontalOut" name="basicPeriodontalOut" value="<?=$basicPeriodontalOut?>" style="width: 50px" /> %</div>
			</td>
		</tr>
		<tr class="alternate">
			<td width="320px"><b>Oral Surgery e.g (7210)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td width="75px"><input type="text" class="textbox" id="basicOral7210Per" name="basicOral7210Per" value="<?=$basicOral7210Per?>" style="width: 50px" /> %</td>
						<td width="30px">AND</td>
						<td width="5px"><input name="basicOral7210" type="radio" <? if($basicOral7210 == "Basic"){?>checked<? } ?> value="Basic"/></td>
						<td width="40px">Basic</td>
						<td width="25px">OR</td>
						<td width="5px"><input name="basicOral7210" type="radio" <? if($basicOral7210 == "Major"){?>checked<? } ?> value="Major"/></td>
						<td width="10px">Major</td>
					</tr>
				</table>
			</td>										
		</tr>
		<tr class="alternate">
			<td width="320px" style="padding-left: 25px;"><b>Out of Network (If doctor out of network ask question)</b></td>
			<td>
				<div style="margin-left:3px !important;"><input type="text" class="textbox" id="basicOral7210Out" name="basicOral7210Out" value="<?=$basicOral7210Out?>" style="width: 50px" /> %</div>
			</td>
		</tr>
		<tr class="alternate">
			<td width="320px" style="padding-left: 25px;"><b>Is there age restriction? If yes what age?</b></td>
			<td>
				<div style="margin-left:3px !important;"><input type="text" class="textbox" id="basicOral7210Res" name="basicOral7210Res" value="<?=$basicOral7210Res?>" style="width: 50px" /></div>
			</td>
		</tr>
		<tr>
			<td width="320px"><b>Oral Surgery e.g (7220)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td width="75px"><input type="text" class="textbox" id="basicOral7220Per" name="basicOral7220Per" value="<?=$basicOral7220Per?>" style="width: 50px" /> %</td>
						<td width="30px">AND</td>
						<td width="5px"><input name="basicOral7220" type="radio" <? if($basicOral7220 == "Basic"){?>checked<? } ?> value="Basic"/></td>
						<td width="40px">Basic</td>
						<td width="25px">OR</td>
						<td width="5px"><input name="basicOral7220" type="radio" <? if($basicOral7220 == "Major"){?>checked<? } ?> value="Major"/></td>
						<td width="10px">Major</td>
					</tr>
				</table>
			</td>										
		</tr>
		<tr>
			<td width="320px" style="padding-left: 25px;"><b>Out of Network (If doctor out of network ask question)</b></td>
			<td>
				<div style="margin-left:3px !important;"><input type="text" class="textbox" id="basicOral7220Out" name="basicOral7220Out" value="<?=$basicOral7220Out?>" style="width: 50px" /> %</div>
			</td>
		</tr>
		<tr>
			<td width="320px" style="padding-left: 25px;"><b>Is there age restriction? If yes what age?</b></td>
			<td>
				<div style="margin-left:3px !important;"><input type="text" class="textbox" id="basicOral7220Res" name="basicOral7220Res" value="<?=$basicOral7220Res?>" style="width: 50px" /></div>
			</td>
		</tr>
		<tr class="alternate">
			<td width="320px"><b>Oral Surgery e.g (7230)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td width="75px"><input type="text" class="textbox" id="basicOral7230Per" name="basicOral7230Per" value="<?=$basicOral7230Per?>" style="width: 50px" /> %</td>
						<td width="30px">AND</td>
						<td width="5px"><input name="basicOral7230" type="radio" <? if($basicOral7230 == "Basic"){?>checked<? } ?> value="Basic"/></td>
						<td width="40px">Basic</td>
						<td width="25px">OR</td>
						<td width="5px"><input name="basicOral7230" type="radio" <? if($basicOral7230 == "Major"){?>checked<? } ?> value="Major"/></td>
						<td width="10px">Major</td>
					</tr>
				</table>
			</td>										
		</tr>
		<tr class="alternate">
			<td width="320px" style="padding-left: 25px;"><b>Out of Network (If doctor out of network ask question)</b></td>
			<td>
				<div style="margin-left:3px !important;"><input type="text" class="textbox" id="basicOral7230Out" name="basicOral7230Out" value="<?=$basicOral7230Out?>" style="width: 50px" /> %</div>
			</td>
		</tr>
		<tr class="alternate">
			<td width="320px" style="padding-left: 25px;"><b>Is there age restriction? If yes what age?</b></td>
			<td>
				<div style="margin-left:3px !important;"><input type="text" class="textbox" id="basicOral7230Res" name="basicOral7230Res" value="<?=$basicOral7230Res?>" style="width: 50px" /></div>
			</td>
		</tr>
		<tr>
			<td width="320px"><b>Oral Surgery e.g (7240)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td width="75px"><input type="text" class="textbox" id="basicOral7240Per" name="basicOral7240Per" value="<?=$basicOral7240Per?>" style="width: 50px" /> %</td>
						<td width="30px">AND</td>
						<td width="5px"><input name="basicOral7240" type="radio" <? if($basicOral7240 == "Basic"){?>checked<? } ?> value="Basic"/></td>
						<td width="40px">Basic</td>
						<td width="25px">OR</td>
						<td width="5px"><input name="basicOral7240" type="radio" <? if($basicOral7240 == "Major"){?>checked<? } ?> value="Major"/></td>
						<td width="10px">Major</td>
					</tr>
				</table>
			</td>										
		</tr>
		<tr>
			<td width="320px" style="padding-left: 25px;"><b>Out of Network (If doctor out of network ask question)</b></td>
			<td>
				<div style="margin-left:3px !important;"><input type="text" class="textbox" id="basicOral7240Out" name="basicOral7240Out" value="<?=$basicOral7240Out?>" style="width: 50px" /> %</div>
			</td>
		</tr>
		<tr>
			<td width="320px" style="padding-left: 25px;"><b>Is there age restriction? If yes what age?</b></td>
			<td>
				<div style="margin-left:3px !important;"><input type="text" class="textbox" id="basicOral7240Res" name="basicOral7240Res" value="<?=$basicOral7240Res?>" style="width: 50px" /></div>
			</td>
		</tr>
	</table>
</div>