<div id="divCustom">

<table cellpadding="3" cellspacing="0" border="0">
<tr>
<td width="130"><b>Network Type</b></td>
<td>
<select id="custNetwork" name="custNetwork" size="1">
    <option value=""></option>
    <option value="In Network" <? if($custNetwork=="In Network"){ ?>Selected<? } ?>>In Network</option>
    <option value="Out Of Network" <? if($custNetwork=="Out Of Network"){ ?>Selected<? } ?>>Out Of Network</option>
    <option value="Policy Terminated" <? if($custNetwork=="Policy Terminated"){ ?>Selected<? } ?>>Policy Terminated</option>
</select></td>
<td></td>
<td>
    <table border="0" cellpadding="2" cellspacing="0">
    <tr>
        <td nowrap="nowrap"><b>Payer&nbsp;ID</b></td>
        <td>&nbsp;</td>
        <td><input type="text" class="textbox" id="custPayorId" name="custPayorId" value="<?=$custPayorId?>" style="width: 100px" /></td>
        <td width="50px">&nbsp;</td>
        <td nowrap="nowrap"><b>E-Attachments</b></td>
        <td width="30">&nbsp;</td>
        <td><input <? if($custAttachments == "Yes"){?>checked<? } ?> type="radio" name="custAttachments" value="Yes" /></td>
        <td>Yes</td>
        <td width="20px">&nbsp;</td>
        <td><input <? if($custAttachments == "No"){?>checked<? } ?> type="radio" name="custAttachments" value="No" /></td>
        <td>No</td>
    </tr>
    </table></td>
</tr>

<tr class="alternate">
<td width="130"><b>Quick Note</b></td>
<td colspan="6"><input type="text" class="textbox" id="custQuickNote" name="custQuickNote" value="<?=$custQuickNote?>" style="width: 610px" maxlength="155" /></td>
<td></td>
</tr>

<tr>
<td width="130"><b>Effective Date</b></td>
<td colspan="2"><input type="text" class="textbox" id="custEffectiveDate" name="custEffectiveDate" value="<?=$custEffectiveDate?>" style="width: 60px" maxlength="10" /><input type="button" value="Select" class="smallButton" onclick="displayDatePicker('custEffectiveDate');" /></td>
<td colspan="5">
<table cellpadding="2" cellspacing="0">
  <tr>
  <td><b>Fee Schedule</b></td>
  <td width="30">&nbsp;</td>
  <td valign="top" width="20px"><input <? if($custFee == "PPO"){?>checked<? } ?> type="radio" name="custFee" value="PPO" /></td>
  <td valign="top">PPO</td>
  <td width="15px">&nbsp;</td>
  <td valign="top" width="20px"><input <? if($custFee == "UCR"){?>checked<? } ?> type="radio" name="custFee" value="UCR" /></td>
  <td valign="top">UCR</td>
  <td valign="top">&nbsp;</td>
  <td valign="top"><input type="text" class="textbox" id="custFeeName" name="custFeeName" value="<?=$custFeeName?>" /></td>
  </tr>
</table></td>
</tr>
<?
//Get group number from patient detail table
	$custGroupNum = getField('cui_patients', 'patientId', $patientId, 'patientGroup');
	$custGroupNum = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', encrypt_decrypt('decrypt', $custGroupNum));
?>
<tr>
	<td width="130"><b>Alternate ID</b></td>
	<td><input type="text" class="textbox" id="custMemberId" name="custMemberId" value="<?=$custMemberId?>" style="width: 100px" /></td>
	<td width="5"></td>
	<td width="130">
		<table cellpadding="2" cellspacing="0">
			<tr>
				<td nowrap="nowrap"><b>Group Name</b></td>
				<td width="30">&nbsp;</td>
				<td valign="top" width="20px"><input type="text" class="textbox" id="custGroup" name="custGroup" value="<?=$custGroup?>" style="width: 100px" /></td>
				<td width="30">&nbsp;</td>
				<td nowrap="nowrap"><b>Group #</b></td>
				<td width="30">&nbsp;</td>
				<td valign="top" width="20px"><input type="text" class="textbox" id="custGroupNum" name="custGroupNum" value="<?=$custGroupNum?>" style="width: 100px" /></td>
			</tr>
		</table>
	</td>
	<td width="50"></td>
	<td></td>
</tr>


<tr class="alternate">
	<td><b>Coverage Type</b></td>
	<td colspan="7">
		<table cellpadding="1" cellspacing="0">
			<tr>
				<td width="20px"><input <? if($custCoverageType == "Single"){?>checked<? } ?> type="radio" name="custCoverageType" value="Single" /></td>
				<td>Single</td>
				<td width="15px">&nbsp;</td>
				<td width="20px"><input <? if($custCoverageType == "Employee Spouse"){?>checked<? } ?> type="radio" name="custCoverageType" value="Employee Spouse" /></td>
				<td>Employee+Spouse</td>
				<td width="15px">&nbsp;</td>
				<td width="20px"><input <? if($custCoverageType == "Family"){?>checked<? } ?> type="radio" name="custCoverageType" value="Family" /></td>
				<td>Family</td>
				<td width="15px">&nbsp;</td>
				<td width="20px"><input <? if($custCoverageType == "Others"){?>checked<? } ?> type="radio" name="custCoverageType" value="Others" /></td>
				<td>Others</td>
				<td width="15px">&nbsp;</td>
				<td>
					<input type="text" class="textbox" id="custCoverageTypeOthers" name="custCoverageTypeOthers" value="<?=$custCoverageTypeOthers?>" style="width: 250px" />
				</td>
			</tr>
		</table>
	</td>
</tr>

<tr class="alternate">
<td><b>Benefits Run ?</b></td>
<td colspan="7">
<table cellpadding="1" cellspacing="0">
<tr>
<td width="20px"><input <? if($custBenefitsRun == "Calender"){?>checked<? } ?> type="radio" name="custBenefitsRun" value="Calender" /></td>
<td>Calendar</td>
<td width="15px">&nbsp;</td>
<td width="20px"><input <? if($custBenefitsRun == "Contract Year"){?>checked<? } ?> type="radio" name="custBenefitsRun" value="Contract Year" /></td>
<td>Contract Year</td>
<td width="5px">&nbsp;</td>
<td>&nbsp;</td>
<td>
<input type="text" class="textbox" id="custBenefitsDate" name="custBenefitsDate" value="<?=$custBenefitsDate?>" style="width: 60px" maxlength="10" /><input type="button" value="Select" class="smallButton" onclick="displayDatePicker('custBenefitsDate');" /></td>
<td width="15px">&nbsp;</td>
<td>Claims Fax #</td>
<td>
<input type="text" class="textbox" id="custBenefitsFax" name="custBenefitsFax" value="<?=$custBenefitsFax?>" style="width: 100px"  /></td>
</tr>
</table></td>
</tr>
<tr>
<td valign="top"><b>MTC</b></td>
<td colspan="6">
<table cellpadding="1" cellspacing="0">
<tr>
<td><input <? if($custMtc == "Yes"){?>checked<? } ?> type="radio" name="custMtc" value="Yes" /></td>
<td>Yes</td>
<td width="20px">&nbsp;</td>
<td><input <? if($custMtc == "No"){?>checked<? } ?> type="radio" name="custMtc" value="No" /></td>
<td>No</td>
<Td width="20px">&nbsp;</Td>
<td><b>Details</b></td>
<td width="15px">&nbsp;</td>
<Td><input type="text" class="textbox" id="custMtcDetail" name="custMtcDetail" value="<?=$custMtcDetail?>" style="width: 180px" /></Td>
<td width="15px">&nbsp;</td>
<td>Filing Limit</td>
<td>
<input type="text" class="textbox" id="custMtcFilingLimit" name="custMtcFilingLimit" value="<?=$custMtcFilingLimit?>" style="width: 100px"  /></td>
</tr>
</table>
</td>
<td>&nbsp;</td>
</tr>
<tr class="alternate">
<td nowrap="nowrap"><b>Waiting Period</b></td>
<td colspan="7">
<table cellpadding="1" cellspacing="0">
<tr>
<td><input <? if($custWaitingPeriod == "Yes"){?>checked<? } ?> type="radio" name="custWaitingPeriod" value="Yes" /></td>
<td>Yes</td>
<td width="20px">&nbsp;</td>
<td><input <? if($custWaitingPeriod == "No"){?>checked<? } ?> type="radio" name="custWaitingPeriod" value="No" /></td>
<td>No</td>
<Td width="20px">&nbsp;</Td>
<td><b>Details</b></td>
<td width="15px">&nbsp;</td>
<Td><input type="text" class="textbox" id="custWaitingPeriodDetail" name="custWaitingPeriodDetail" value="<?=$custWaitingPeriodDetail?>" style="width: 180px" /></Td>
</tr>
</table></td>
</tr>
<tr>
  <td colspan="8">
  	<table cellpadding="2" cellspacing="0">
      <tr>
      <td width="132px" style="padding-left:0px;"><b>Yearly Max $</b></td>
      <td><input type="text" class="textbox" id="custYearlyMax" name="custYearlyMax" value="<?=$custYearlyMax?>" style="width: 60px" /></td>
      <td width="10px">&nbsp;</td>
      <td width="80px"><b>Applies to?</b></td>
      <td><input <? if($custYearlyApplies1 == "P"){?>checked<? } ?> type="checkbox" name="custYearlyApplies1" value="P" /></td>
      <td>P</td>
      <td width="2px"></td>
      <td><input <? if($custYearlyApplies2 == "B"){?>checked<? } ?> type="checkbox" name="custYearlyApplies2" value="B" /></td>
      <td>B</td>
      <td width="2px"></td>
      <td><input <? if($custYearlyApplies3 == "M"){?>checked<? } ?> type="checkbox" name="custYearlyApplies3" value="M" /></td>
      <td>M</td>
      <td width="20px">&nbsp;</td>
      <td width="50px"><b>Used $</b></td>
      <td><input type="text" class="textbox" id="custUsed" name="custUsed" value="<?=$custUsed?>" style="width: 60px" /></td>
      <td width="10px">&nbsp;</td>
      <td width="90px"><b>Roll over? $</b></td>
      <td><input type="text" class="textbox" id="custRollOver" name="custRollOver" value="<?=$custRollOver?>" style="width: 60px" /></td>
      </tr>
    </table>  </td>
  </tr>
<tr class="alternate">
  <td colspan="8">
  	<table cellpadding="2" cellspacing="0">
      <tr>
      <td width="132px" style="padding-left:0px;"><b>Ind. Deductible $</b></td>
      <td><input type="text" class="textbox" id="custDeduct" name="custDeduct" value="<?=$custDeduct?>" style="width: 60px" /></td>
      <td width="10px">&nbsp;</td>
      <td width="80px"><b>Applies to?</b></td>
      <td><input <? if($custDeductApplies1 == "P"){?>checked<? } ?> type="checkbox" name="custDeductApplies1" value="P" /></td>
      <td>P</td>
      <td width="2px"></td>
      <td><input <? if($custDeductApplies2 == "D"){?>checked<? } ?> type="checkbox" name="custDeductApplies2" value="D" /></td>
      <td>D</td>
      <td width="2px"></td>
      <td><input <? if($custDeductApplies3 == "B"){?>checked<? } ?> type="checkbox" name="custDeductApplies3" value="B" /></td>
      <td>B</td>
      <td width="2px"></td>
      <td><input <? if($custDeductApplies4 == "M"){?>checked<? } ?> type="checkbox" name="custDeductApplies4" value="M" /></td>
      <td>M</td>
      <td width="20px">&nbsp;</td>
      <td width="40px"><b>Met</b></td>
      <td><input <? if($custMet == "Yes"){?>checked<? } ?> type="radio" name="custMet" value="Yes" /></td>
      <td>Yes</td>
      <td width="5px"></td>
      <td><input <? if($custMet == "No"){?>checked<? } ?> type="radio" name="custMet" value="No" /></td>
      <td>No</td>
      </tr>
    </table>  </td>
</tr>
<tr>
  <td colspan="8">
  	<table cellpadding="2" cellspacing="0">
      <tr>
      <td width="132px" style="padding-left:0px;"><b>Fam. Deductible $</b></td>
      <td><input type="text" class="textbox" id="custFamilyDeduct" name="custFamilyDeduct" value="<?=$custFamilyDeduct?>" style="width: 60px" /></td>
      <td width="10px">&nbsp;</td>
      <td width="40px"><b>Met</b></td>
      <td><input <? if($custMet2 == "Yes"){?>checked<? } ?> type="radio" name="custMet2" value="Yes" /></td>
      <td>Yes</td>
      <td width="5px"></td>
      <td><input <? if($custMet2 == "No"){?>checked<? } ?> type="radio" name="custMet2" value="No" /></td>
      <td>No</td>
      </tr>
    </table>  </td>
</tr>
<tr class="alternate">
  <td colspan="8">
  	<table cellpadding="1" cellspacing="0" border="0">
    <tr>
	<td nowrap="nowrap"><b>Prev</b></td>
    <td width="5px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custPrev" name="custPrev" value="<?=$custPrev?>" style="width: 60px" />%</td>
    <td width="15px">&nbsp;</td>
	
    <td nowrap="nowrap"><b>Basic</b></td>
    <td width="5px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custBasic" name="custBasic" value="<?=$custBasic?>" style="width: 60px" />%</td>
    <td width="15px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>Major</b></td>
    <td width="5px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custMajor" name="custMajor" value="<?=$custMajor?>" style="width: 60px" />%</td>
    <td width="15px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>Perio</b></td>
    <td width="5px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custPerio" name="custPerio" value="<?=$custPerio?>" style="width: 60px" />%</td>
    <td width="15px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>Endo</b></td>
    <td width="5px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custEndo" name="custEndo" value="<?=$custEndo?>" style="width: 60px" />%</td>
    <td width="15px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>OS</b></td>
    <td width="5px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custOs" name="custOs" value="<?=$custOs?>" style="width: 60px" />%</td>
    
    <td width="15px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>OS to Med</b></td>
    <td width="5px">&nbsp;</td>
    <td><input <? if($custOsToMed == "Yes"){?>checked<? } ?> type="radio" name="custOsToMed" value="Yes" /></td>
    <td>Yes</td>
    <td width="5px"></td>
    <td><input <? if($custOsToMed == "No"){?>checked<? } ?> type="radio" name="custOsToMed" value="No" /></td>
    <td>No</td>
    </tr>
    </table>  </td>
</tr>
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">Diagnostic & Preventative</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
<tr>
    <td width="130" nowrap="nowrap"><b>Which Exam Share Freq?</b></td>
    <td colspan="5">
		<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td><input <? if($custDPWhichExamShare0120 == "0120"){?>checked<? } ?> type="checkbox" name="custDPWhichExamShare0120" value="0120" /></td>
		<td>0120</td>
		<td width="10px">&nbsp;</td>
        <td><input <? if($custDPWhichExamShare0140 == "0140"){?>checked<? } ?> type="checkbox" name="custDPWhichExamShare0140" value="0140" /></td>
        <td>0140</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custDPWhichExamShare0150 == "0150"){?>checked<? } ?> type="checkbox" name="custDPWhichExamShare0150" value="0150" /></td>
        <td>0150</td>
		<td width="10px">&nbsp;</td>
        <td><input <? if($custDPWhichExamShare9310 == "9310"){?>checked<? } ?> type="checkbox" name="custDPWhichExamShare9310" value="9310" /></td>
        <td>9310</td>
        </tr>
        </table> 
	</td>
    <td colspan="4">
    	&nbsp;
    </td>
</tr>
<tr>
    <td width="130" nowrap="nowrap"><b>Complete Exam (0150)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPCompleteExam" name="custDPCompleteExam" value="<?=$custDPCompleteExam?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPCompleteExamFreq" name="custDPCompleteExamFreq" value="<?=$custDPCompleteExamFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td colspan="4">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="130" nowrap="nowrap"><b>Periodic Exam (0120)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custDPPeriodicExam" name="custDPPeriodicExam" value="<?=$custDPPeriodicExam?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>Freq</b></td>
            <td>
            <input type="text" class="textbox" id="custDPPeriodicExamFreq" name="custDPPeriodicExamFreq" value="<?=$custDPPeriodicExamFreq?>" style="width: 75px" />            </td>
        </tr>
        </table>
    </td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Limited Exam (0140)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPLimitedExam" name="custDPLimitedExam" value="<?=$custDPLimitedExam?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPLimitedExamFreq" name="custDPLimitedExamFreq" value="<?=$custDPLimitedExamFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td colspan="3">
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <!--<td nowrap="nowrap"><b>Separate from 0120?</b></td>
        <td><input <? if($custDPSeparate == "Yes"){?>checked<? } ?> type="radio" name="custDPSeparate" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custDPSeparate == "No"){?>checked<? } ?> type="radio" name="custDPSeparate" value="No" /></td>
        <td>No</td>
        <td>&nbsp;</td>-->
        <td nowrap="nowrap"><b>Allowed same day w/ TX?</b></td>
        <td><input <? if($custDPAllowed == "Yes"){?>checked<? } ?> type="radio" name="custDPAllowed" value="Yes" /></td>
        <td>Yes</td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custDPAllowed == "No"){?>checked<? } ?> type="radio" name="custDPAllowed" value="No" /></td>
        <td>No</td>
        </tr>
        </table>    </td>
    <td>&nbsp;</td>
</tr>

<tr>
    <td nowrap="nowrap"><b>FMX(0210)/Pano(0330)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPFMX" name="custDPFMX" value="<?=$custDPFMX?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPFMXFreq" name="custDPFMXFreq" value="<?=$custDPFMXFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td><input <? if($custDPShare == "Share"){?>checked<? } ?> type="radio" name="custDPShare" value="Share" /></td>
        <td nowrap="nowrap"><b>Share</b></td>
        <td><input <? if($custDPShare == "Separate"){?>checked<? } ?> type="radio" name="custDPShare" value="Separate" /></td>
        <td><b>Separate</b></td>
        <td width="30">&nbsp;</td>
        <!--<td nowrap="nowrap"><b>Does FMX take freq of BW</b></td>
        <td width="5"></td>
        <td><input type="text" class="textbox" id="custDPFreqBw" name="custDPFreqBw" value="<?=$custDPFreqBw?>" style="width: 100px" /></td>-->
        </tr>
        </table>    </td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Bitewings (0270-74)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPBit" name="custDPBit" value="<?=$custDPBit?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPBitFreq" name="custDPBitFreq" value="<?=$custDPBitFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td>
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="130" nowrap="nowrap"><b>Periapicals (0220-30)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custDPPeriap" name="custDPPeriap" value="<?=$custDPPeriap?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custDPPeriapFreq" name="custDPPeriapFreq" value="<?=$custDPPeriapFreq?>" style="width: 75px" /></td>
            <td width="20px">&nbsp;</td>
            <td><b>Max?</b></td>
            <td><input type="text" class="textbox" id="custDPMax" name="custDPMax" value="<?=$custDPMax?>" style="width: 75px" /></td>
        </tr>
        </table>    </td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>

<tr>
    <td nowrap="nowrap"><b>Prophy (1110-20)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPProphy" name="custDPProphy" value="<?=$custDPProphy?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPProphyFreq" name="custDPProphyFreq" value="<?=$custDPProphyFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td>
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="130" nowrap="nowrap"><b>Fluoride (1206)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custDPFluoride" name="custDPFluoride" value="<?=$custDPFluoride?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custDPFluorideFreq" name="custDPFluorideFreq" value="<?=$custDPFluorideFreq?>" style="width: 75px" /></td>
            <td width="20px">&nbsp;</td>
            <td nowrap="nowrap"><b>To Age?</b></td>
            <td><input type="text" class="textbox" id="custDPToAge" name="custDPToAge" value="<?=$custDPToAge?>" style="width: 75px" /></td>
        </tr>
        </table>
    </td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Sealants (1351)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPSealants" name="custDPSealants" value="<?=$custDPSealants?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPSealantsFreq" name="custDPSealantsFreq" value="<?=$custDPSealantsFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td>
    	<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td nowrap="nowrap"><b>To Age?</b></td>
        <td><input type="text" class="textbox" id="custDPSealantsToAge" name="custDPSealantsToAge" value="<?=$custDPSealantsToAge?>" style="width: 30px" /></td>
        <td width="5"></td>
        <td><input <? if($custDPMolarsOnly == "Perm. Molars Only"){?>checked<? } ?> type="checkbox" name="custDPMolarsOnly" value="Perm. Molars Only" /></td>
        <td nowrap="nowrap"><b>Perm. Molars Only</b></td>
        <td width="5px">&nbsp;</td>
        <td><input <? if($custDPWisdom == "Excludes Wisdom"){?>checked<? } ?> type="checkbox" name="custDPWisdom" value="Excludes Wisdom" /></td>
        <td nowrap="nowrap"><b>Excludes Wisdom</b></td>
        <td width="5px">&nbsp;</td>
        <td><input <? if($custDPPremolars == "Premolars"){?>checked<? } ?> type="checkbox" name="custDPPremolars" value="Premolars" /></td>
        <td nowrap="nowrap"><b>Premolars</b></td>
        </tr>
        </table>    </td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>

<tr>
    <td nowrap="nowrap"><b>Oral Image (D0240)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPPrev" name="custDPPrev" value="<?=$custDPPrev?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPPrevFreq" name="custDPPrevFreq" value="<?=$custDPPrevFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td>
    	<!--<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td nowrap="nowrap"><b>To Age?</b></td>
        <td><input type="text" class="textbox" id="custDPPrevToAge" name="custDPPrevToAge" value="<?=$custDPPrevToAge?>" style="width: 30px" /></td>
        <td width="5"></td>
        </tr>
        </table>-->
	</td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Consult (D9310)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPSpaceMain" name="custDPSpaceMain" value="<?=$custDPSpaceMain?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPSpaceMainFreq" name="custDPSpaceMainFreq" value="<?=$custDPSpaceMainFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td>
    	<!--<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td nowrap="nowrap"><b>To Age?</b></td>
        <td><input type="text" class="textbox" id="custDPSpaceMainToAge" name="custDPSpaceMainToAge" value="<?=$custDPSpaceMainToAge?>" style="width: 30px" /></td>
        <td width="5"></td>
        </tr>
        </table>-->
	</td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td nowrap="nowrap"><b>3D CT (D0367)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPSpaceMain2" name="custDPSpaceMain2" value="<?=$custDPSpaceMain2?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPSpaceMain2Freq" name="custDPSpaceMain2Freq" value="<?=$custDPSpaceMain2Freq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td>
    	<!--<table cellpadding="1" cellspacing="0" border="0">
        <tr>
        <td nowrap="nowrap"><b>To Age?</b></td>
        <td><input type="text" class="textbox" id="custDPSpaceMain2ToAge" name="custDPSpaceMain2ToAge" value="<?=$custDPSpaceMain2ToAge?>" style="width: 30px" /></td>
        <td width="5"></td>
        </tr>
        </table>-->
	</td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Oral cancer screening (0431)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPOralCancer" name="custDPOralCancer" value="<?=$custDPOralCancer?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td>
    <input type="text" class="textbox" id="custDPOralCancerFreq" name="custDPOralCancerFreq" value="<?=$custDPOralCancerFreq?>" style="width: 75px" />    </td>
    <td width="10">&nbsp;</td>
    <td>
    	<!--<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>IOP (0350)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custDPIOP" name="custDPIOP" value="<?=$custDPIOP?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>Freq</b></td>
            <td>
            <input type="text" class="textbox" id="custDPIOPFreq" name="custDPIOPFreq" value="<?=$custDPIOPFreq?>" style="width: 75px" /></td>
        </tr>
        </table>-->
    </td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<!--<tr>
    <td nowrap="nowrap"><b>Missed Appointment (D9986)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPMissedApp" name="custDPMissedApp" value="<?=$custDPMissedApp?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custDPMissedAppFreq" name="custDPMissedAppFreq" value="<?=$custDPMissedAppFreq?>" style="width: 75px" /></td>
    <td width="10">&nbsp;</td>
    <td>
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Cancelled Appointment (D9987)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custDPCancelledApp" name="custDPCancelledApp" value="<?=$custDPCancelledApp?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>Freq</b></td>
            <td><input type="text" class="textbox" id="custDPCancelledAppFreq" name="custDPCancelledAppFreq" value="<?=$custDPCancelledAppFreq?>" style="width: 75px" /></td>
        </tr>
        </table>
    </td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>-->
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">Restorative</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
<tr>
    <td width="130" nowrap="nowrap"><b>Posterior Composites (2391-94)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custCCComposites" name="custCCComposites" value="<?=$custCCComposites?>" style="width: 50px" />%</td>
    <td width="10">&nbsp;</td>
    <td colspan="6">
    	<table cellpadding="1" cellspacing="0" border="0">
    	  <tr>
    	  <!--<td nowrap="nowrap"><b>Downgrade?</b></td>
          <td><input <? if($custCCDowngrade == "Yes"){?>checked<? } ?> type="radio" name="custCCDowngrade" value="Yes" /></td>
          <td>Yes</td>
          <td width="5px">&nbsp;</td>
          <td><input <? if($custCCDowngrade == "No"){?>checked<? } ?> type="radio" name="custCCDowngrade" value="No" /></td>
          <td>No</td>
          <td width="20px">&nbsp;</td>-->
		  <td nowrap="nowrap"><b>Alt Benefits?</b></td>
          <td><input <? if($custCCMolarsPre == "All Teeth"){?>checked<? } ?> type="radio" name="custCCMolarsPre" value="All Teeth" /></td>
          <td>All Teeth</td>
          <td width="5px">&nbsp;</td>
          <td>OR</td>
		  <td width="20px">&nbsp;</td>
          <td><input <? if($custCCMolarsPre == "Molars"){?>checked<? } ?> type="radio" name="custCCMolarsPre" value="Molars" /></td>
          <td nowrap="nowrap">Molars</td>
          <td><input <? if($custCCMolarsPre == "Posterior"){?>checked<? } ?> type="radio" name="custCCMolarsPre" value="Posterior" /></td>
          <td>Posterior</td>
          <td width="20">&nbsp;</td>
          <td width="30"><b>Freq</b></td>
          <td><input type="text" class="textbox" id="custCCMolarsPreFreq" name="custCCMolarsPreFreq" value="<?=$custCCMolarsPreFreq?>" style="width: 60px" /></td>
          </tr>
  	  </table></td>
    </tr>
<tr>
    <td nowrap="nowrap"><b>Posterior Crowns (2740)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custCCCrowns" name="custCCCrowns" value="<?=$custCCCrowns?>" style="width: 50px" />%</td>
    <td></td>
    <td colspan="6">
    	<table cellpadding="1" cellspacing="0" border="0">
    	  <tr>
		  <td width="57"><b>Freq</b></td>
		  <td width="60"><input type="text" class="textbox" id="custCCCrownsFreq" name="custCCCrownsFreq" value="<?=$custCCCrownsFreq?>" style="width: 60px" /></td>
		  <td width="27">&nbsp;</td>
		  <td nowrap="nowrap"><b>Alt Benefits?</b></td>
          <td><input <? if($custCCPCMolarsPre == "All Teeth"){?>checked<? } ?> type="radio" name="custCCPCMolarsPre" value="All Teeth" /></td>
          <td>All Teeth</td>
          <td width="5px">&nbsp;</td>
          <td>OR</td>
		  <td width="20px">&nbsp;</td>
          <td><input <? if($custCCPCMolarsPre == "Molars"){?>checked<? } ?> type="radio" name="custCCPCMolarsPre" value="Molars" /></td>
          <td nowrap="nowrap">Molars</td>
          <td><input <? if($custCCPCMolarsPre == "Posterior"){?>checked<? } ?> type="radio" name="custCCPCMolarsPre" value="Posterior" /></td>
          <td>Posterior</td>
		  <td width="20">&nbsp;</td>
    	  <!--<td nowrap="nowrap"><b>Downgrade?</b></td>
          <td><input <? if($custCCCrownsDowngrade == "Yes"){?>checked<? } ?> type="radio" name="custCCCrownsDowngrade" value="Yes" /></td>
          <td>Yes</td>
          <td width="5px">&nbsp;</td>
          <td><input <? if($custCCCrownsDowngrade == "No"){?>checked<? } ?> type="radio" name="custCCCrownsDowngrade" value="No" /></td>
          <td>No</td>
          <td width="15px">&nbsp;</td>
          <td><b>Paid On</b></td>
          <td><input <? if($custCCPaidOn == "Prep"){?>checked<? } ?> type="radio" name="custCCPaidOn" value="Prep" /></td>
          <td nowrap="nowrap">Prep</td>
          <td><input <? if($custCCPaidOn == "Seat Date"){?>checked<? } ?> type="radio" name="custCCPaidOn" value="Seat Date" /></td>
          <td>Seat Date</td>
          <td width="15">&nbsp;</td>-->
          </tr>
  	  </table>    </td>
    </tr>
<tr>
    <td nowrap="nowrap"><b>Build Up (2950)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custCCBuildUp" name="custCCBuildUp" value="<?=$custCCBuildUp?>" style="width: 50px" />%</td>
    <td></td>
    <td><b>Freq</b></td>
    <td width="80"><input type="text" class="textbox" id="custCCBuildUpFreq" name="custCCBuildUpFreq" value="<?=$custCCBuildUpFreq?>" style="width: 60px" />    </td>
    <td width="5"></td>
    <td colspan="3" valign="top">
    </td>
    </tr>
<tr>
    <td nowrap="nowrap"><b>Re-Cement Crown (2920)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custCCReCrown" name="custCCReCrown" value="<?=$custCCReCrown?>" style="width: 50px" />%</td>
    <td></td>
    <td><b>Freq</b></td>
    <td width="80"><input type="text" class="textbox" id="custCCReCrownFreq" name="custCCReCrownFreq" value="<?=$custCCReCrownFreq?>" style="width: 60px" />    </td>
    <td width="5"></td>
    <td colspan="3" valign="top">
    	<table cellpadding="1" cellspacing="0" border="0">
    	  <tr>
			<td width="160" nowrap="nowrap"><b>Post & Core (D2954)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custCCPC" name="custCCPC" value="<?=$custCCPC?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>Freq</b></td>
            <td>
				<input type="text" class="textbox" id="custCCPCFreq" name="custCCPCFreq" value="<?=$custCCPCFreq?>" style="width: 75px" />
			</td>
    	  <!--<td nowrap="nowrap"><b>Same Provider Re-cementing OK?</b></td>
          <td><input <? if($custCCReCementing == "Yes"){?>checked<? } ?> type="radio" name="custCCReCementing" value="Yes" /></td>
          <td>Yes</td>
          <td width="10px">&nbsp;</td>
          <td><input <? if($custCCReCementing == "No"){?>checked<? } ?> type="radio" name="custCCReCementing" value="No" /></td>
          <td>No</td>-->
          </tr>
  	  </table></td>
    </tr>
<tr>
    <td nowrap="nowrap"><b>Veneers (D2960-62)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custCCRestoration" name="custCCRestoration" value="<?=$custCCRestoration?>" style="width: 50px" />%</td>
    <td></td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custCCRestorationFreq" name="custCCRestorationFreq" value="<?=$custCCRestorationFreq?>" style="width: 60px" /></td>
    <td></td>
    <td colspan="3" valign="top">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="160" nowrap="nowrap"><b>Recement Bridge (D6930)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custCCLength" name="custCCLength" value="<?=$custCCLength?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>Freq</b></td>
            <td>
            <input type="text" class="textbox" id="custCCLengthFreq" name="custCCLengthFreq" value="<?=$custCCLengthFreq?>" style="width: 75px" />            </td>
        </tr>
        </table>    </td>
</tr>

<tr>
    <td nowrap="nowrap"><b>Bridge (D6245-D6740)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custCCCrowns2" name="custCCCrowns2" value="<?=$custCCCrowns2?>" style="width: 50px" />%</td>
    <td nowrap="nowrap">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custCCCrowns2Freq" name="custCCCrowns2Freq" value="<?=$custCCCrowns2Freq?>" style="width: 60px" /></td>
    <td></td>
    <td colspan="3" valign="top">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="160" nowrap="nowrap"><b>Alt Benefits?</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custCCCrownsAltBenefit" name="custCCCrownsAltBenefit" value="<?=$custCCCrownsAltBenefit?>" style="width: 200px" />%</td>
        </tr>
        </table>
	</td>
</tr>
<tr>
    <td nowrap="nowrap"><b>Recement: Inlay-onlay-venner (D2910)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custCCPulp" name="custCCPulp" value="<?=$custCCPulp?>" style="width: 50px" />%</td>
    <td nowrap="nowrap">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custCCPulpFreq" name="custCCPulpFreq" value="<?=$custCCPulpFreq?>" style="width: 60px" /></td>
    <td></td>
    <td valign="top">&nbsp;</td>
    <td width="10">&nbsp;</td>
    <td>      </td>
</tr>

<tr>
    <td nowrap="nowrap"><b>Replacement time for crowns/bridges</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custCCReplacementTime" name="custCCReplacementTime" value="<?=$custCCReplacementTime?>" style="width: 50px" />%</td>
    <td nowrap="nowrap">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custCCReplacementTimeFreq" name="custCCReplacementTimeFreq" value="<?=$custCCReplacementTimeFreq?>" style="width: 60px" /></td>
    <td></td>
    <td valign="top">&nbsp;</td>
    <td width="10">&nbsp;</td>
    <td>      </td>
</tr>
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">Endo</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
  <tr>
    <td width="230" nowrap="nowrap"><b>Anterior (3310)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custEAnterior" name="custEAnterior" value="<?=$custEAnterior?>" style="width: 75px" />%</td>
    <td width="20px">&nbsp;</td>
    <td nowrap="nowrap"><b>Bi-Cuspid (3320)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custEBiCuspid" name="custEBiCuspid" value="<?=$custEBiCuspid?>" style="width: 75px" />%</td>
    <td width="10px">&nbsp;</td>
    <td valign="top" nowrap="nowrap"><b>Molars (3330)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custEMolars" name="custEMolars" value="<?=$custEMolars?>" style="width: 75px" />%</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>In-Direct Pulp Cap (D3120)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custEIndirect" name="custEIndirect" value="<?=$custEIndirect?>" style="width: 75px" />%</td>
    <td width="10px">&nbsp;</td>
    <td valign="top" nowrap="nowrap"><b>Freq</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custEIndirectFreq" name="custEIndirectFreq" value="<?=$custEIndirectFreq?>" style="width: 75px" /></td>
	<td width="20px">&nbsp;</td>
	<td nowrap="nowrap"><b>Pulp Debridement (D3221)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custEPulpDebridement" name="custEPulpDebridement" value="<?=$custEPulpDebridement?>" style="width: 75px" />%</td>
    <td width="10px">&nbsp;</td>
    <td valign="top" nowrap="nowrap"><b>Freq</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custEPulpDebridementFreq" name="custEPulpDebridementFreq" value="<?=$custEPulpDebridementFreq?>" style="width: 75px" /></td>
  </tr>
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">Periodontal</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
  <tr>
    <td width="130" nowrap="nowrap"><b>SPR Deep Cleaning (4341-42)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custPCleaning" name="custPCleaning" value="<?=$custPCleaning?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custPCleaningFreq" name="custPCleaningFreq" value="<?=$custPCleaningFreq?>" style="width: 75px" /></td>
    <td width="10">&nbsp;</td>
    <td colspan="2" valign="top" nowrap="nowrap">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="220" nowrap="nowrap"><b>How many quads allowed same day?</b></td>
			<td><input <? if($custPCleaningQuads == "2"){?>checked<? } ?> type="radio" name="custPCleaningQuads" value="2" /></td>
			<td>2</td>
			<td width="10px">&nbsp;</td>
			<td>OR</td>
			<td width="10px">&nbsp;</td>
			<td><input <? if($custPCleaningQuads == "4"){?>checked<? } ?> type="radio" name="custPCleaningQuads" value="4" /></td>
			<td>4</td>
			<td width="15px">&nbsp;</td>
        </tr>
        </table>
	</td>
    </tr>
  <tr>
    <td nowrap="nowrap"><b>Full Mouth Debridement (4355)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custPDebridement" name="custPDebridement" value="<?=$custPDebridement?>" style="width: 50px" /> %</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custPDebridementFreq" name="custPDebridementFreq" value="<?=$custPDebridementFreq?>" style="width: 75px" /></td>
    <td width="10">&nbsp;</td>
    <td valign="top" nowrap="nowrap">
    	<!--<table cellpadding="1" cellspacing="0" border="0">
    	  <tr>
    	  <td nowrap="nowrap"><b>Takes place of PROPHY? </b></td>
          <td><input <? if($custPPXA == "Yes"){?>checked<? } ?> type="radio" name="custPPXA" value="Yes" /></td>
          <td>Yes</td>
          <td width="5px">&nbsp;</td>
          <td><input <? if($custPPXA == "No"){?>checked<? } ?> type="radio" name="custPPXA" value="No" /></td>
          <td>No</td>
          </tr>
  	  </table>-->
    </td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Perio Maintenance (4910)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custPMaintenance" name="custPMaintenance" value="<?=$custPMaintenance?>" style="width: 50px" />
      %</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custPMaintenanceFreq" name="custPMaintenanceFreq" value="<?=$custPMaintenanceFreq?>" style="width: 75px" /></td>
    <td width="10">&nbsp;</td>
    <td valign="top" nowrap="nowrap">
		<table cellpadding="1" cellspacing="0" border="0">
    	  <tr>
    	  <td nowrap="nowrap"><b>Does PMT share freq w Px (D1110)?</b></td>
          <td><input <? if($custPMaintenancePMTShare == "Yes"){?>checked<? } ?> type="radio" name="custPMaintenancePMTShare" value="Yes" /></td>
          <td>Yes</td>
          <td width="5px">&nbsp;</td>
          <td><input <? if($custPMaintenancePMTShare == "No"){?>checked<? } ?> type="radio" name="custPMaintenancePMTShare" value="No" /></td>
          <td>No</td>
          </tr>
		</table>
    	<!--<table cellpadding="1" cellspacing="0">
        <tr>
        <td><input <? if($custPCombInAdd == "Combined"){?>checked<? } ?> type="radio" name="custPCombInAdd" value="Combined" /></td>
        <td><b>Combined</b></td>
        <td width="10px">&nbsp;</td>
        <td><input <? if($custPCombInAdd == "In Addition to PROPHY"){?>checked<? } ?> type="radio" name="custPCombInAdd" value="In Addition to PROPHY" /></td>
        <td><b>In Addition to PROPHY</b></td>
        <td width="15px">&nbsp;</td>
        </tr>
        </table>-->
	</td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Gingival Inflammation (4346)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custGingivalInflammation" name="custGingivalInflammation" value="<?=$custGingivalInflammation?>" style="width: 50px" /> %</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custGingivalInflammationFreq" name="custGingivalInflammationFreq" value="<?=$custGingivalInflammationFreq?>" style="width: 75px" /></td>
    <td width="10">&nbsp;</td>
    <td valign="top" nowrap="nowrap">
    	<!--<table cellpadding="1" cellspacing="0" border="0">
    	  <tr>
    	  <td nowrap="nowrap"><b>Deductible Waived</b></td>
          <td><input <? if($custGingivalInflammationWaived == "Yes"){?>checked<? } ?> type="radio" name="custGingivalInflammationWaived" value="Yes" /></td>
          <td>Yes</td>
          <td width="5px">&nbsp;</td>
          <td><input <? if($custGingivalInflammationWaived == "No"){?>checked<? } ?> type="radio" name="custGingivalInflammationWaived" value="No" /></td>
          <td>No</td>
          </tr>
		</table>-->
	</td>
  </tr>
  <!--<tr>
	<td colspan="3"  valign="top" nowrap="nowrap">
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td nowrap="nowrap"><b>Does it share with 1110</b></td>
                <td><input <? if($custsharewith1110 == "Yes"){?>checked<? } ?> type="radio" name="custsharewith1110" value="Yes" /></td>
                <td>Yes</td>
                <td width="5px">&nbsp;</td>
                <td><input <? if($custsharewith1110 == "No"){?>checked<? } ?> type="radio" name="custsharewith1110" value="No" /></td>
                <td>No</td>
                <td width="10">&nbsp;</td>
			</tr>
		</table>
	</td>
	<td colspan="4"  valign="top" nowrap="nowrap">
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td nowrap="nowrap"><b>Does it share with 4910</b></td>
                <td><input <? if($custsharewith4910 == "Yes"){?>checked<? } ?> type="radio" name="custsharewith4910" value="Yes" /></td>
                <td>Yes</td>
                <td width="5px">&nbsp;</td>
                <td><input <? if($custsharewith4910 == "No"){?>checked<? } ?> type="radio" name="custsharewith4910" value="No" /></td>
                <td>No</td>
                <td width="15px">&nbsp;</td>
                <td nowrap="nowrap"><b>Does it share with 4341/42</b></td>
                <td><input <? if($custsharewith4341 == "Yes"){?>checked<? } ?> type="radio" name="custsharewith4341" value="Yes" /></td>
                <td>Yes</td>
                <td width="5px">&nbsp;</td>
                <td><input <? if($custsharewith4341 == "No"){?>checked<? } ?> type="radio" name="custsharewith4341" value="No" /></td>
                <td>No</td>
			</tr>
		</table>
	</td>
  </tr>-->
  <tr>
    <td nowrap="nowrap"><b>Crown Lengthening (D4249)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custPDesensit" name="custPDesensit" value="<?=$custPDesensit?>" style="width: 50px" />
      %</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custPDesensitFreq" name="custPDesensitFreq" value="<?=$custPDesensitFreq?>" style="width: 75px" /></td>
    <td width="10">&nbsp;</td>
    <td valign="top" nowrap="nowrap">
		<table cellpadding="1" cellspacing="0" border="0">
    	  <tr>
    	  <td nowrap="nowrap"><b>Same day crown procedure allowed?</b></td>
          <td><input <? if($custPDesensitProcedure == "Yes"){?>checked<? } ?> type="radio" name="custPDesensitProcedure" value="Yes" /></td>
          <td>Yes</td>
          <td width="5px">&nbsp;</td>
          <td><input <? if($custPDesensitProcedure == "No"){?>checked<? } ?> type="radio" name="custPDesensitProcedure" value="No" /></td>
          <td>No</td>
          </tr>
		</table>
    	<!--<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Biologic (D4265)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custPBiologic" name="custPBiologic" value="<?=$custPBiologic?>" style="width: 75px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>Resorb (D4266)</b></td>
            <td><input type="text" class="textbox" id="custPResorb" name="custPResorb" value="<?=$custPResorb?>" style="width: 75px" />%</td>
			<td width="20px">&nbsp;</td>
            <td><b>Non-resorb (D4267)</b></td>
            <td><input type="text" class="textbox" id="custPNonResorb" name="custPNonResorb" value="<?=$custPNonResorb?>" style="width: 75px" />%</td>
        </tr>
        </table>-->
    </td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Biologic (D4265)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custPBiologic" name="custPBiologic" value="<?=$custPBiologic?>" style="width: 50px" />
      %</td>
    <td width="20">&nbsp;</td>
    <td><b>Resorb (D4266)</b></td>
    <td><input type="text" class="textbox" id="custPResorb" name="custPResorb" value="<?=$custPResorb?>" style="width: 75px" /></td>
    <td width="10">&nbsp;</td>
    <td valign="top" nowrap="nowrap">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Non-resorb (D4267)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custPNonResorb" name="custPNonResorb" value="<?=$custPNonResorb?>" style="width: 75px" />%</td>
            <td width="20px">&nbsp;</td>
        </tr>
        </table>
    </td>
  </tr>
  <!--<tr>
    <td colspan="4" nowrap="nowrap"><b>Is there a time lapse between SCRP and Perio Maintenance?</b></td>
    <td colspan="3"><input type="text" class="textbox" id="custPTimeLapse" name="custPTimeLapse" value="<?=$custPTimeLapse?>" style="width: 200px" /></td>
  </tr>-->
  <tr>
    <td colspan="4" nowrap="nowrap"><b>Are graft/membranes allowed with ext or implant on the same day?</b></td>
    <td colspan="3"><input type="text" class="textbox" id="custPGraftMembrane" name="custPGraftMembrane" value="<?=$custPGraftMembrane?>" style="width: 200px" /></td>
  </tr>
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">Prosthodonic</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
  <!--<tr>
    <td nowrap="nowrap"><b>Replacement Time Freq</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDTime" name="custDTime" value="<?=$custDTime?>" style="width: 75px" /></td>
    <td width="10px">&nbsp;</td>
    <td nowrap="nowrap"><b>Covered @</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDCovered" name="custDCovered" value="<?=$custDCovered?>" style="width: 75px" />%</td>
    <td width="20px">&nbsp;</td>
	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td width="10px">&nbsp;</td>
	<td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>-->
  <tr>
    <td nowrap="nowrap"><b>Dentures (D5110-D5120-D5130-D5140)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDDentures" name="custDDentures" value="<?=$custDDentures?>" style="width: 75px" />%</td>
    <td width="10px">&nbsp;</td>
    <td nowrap="nowrap"><b>Freq</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDDenturesFreq" name="custDDenturesFreq" value="<?=$custDDenturesFreq?>" style="width: 75px" /></td>
    <td width="20px">&nbsp;</td>
	<td nowrap="nowrap"><b>Partial (5211-5212-5213-5214)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPartial" name="custDPartial" value="<?=$custDPartial?>" style="width: 75px" />%</td>
    <td width="10px">&nbsp;</td>
	<td nowrap="nowrap"><b>Freq</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDPartialFreq" name="custDPartialFreq" value="<?=$custDPartialFreq?>" style="width: 75px" /></td>
	</td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Interim Dentures (5810-5811)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDIDentures" name="custDIDentures" value="<?=$custDIDentures?>" style="width: 75px" />%</td>
    <td width="10px">&nbsp;</td>
    <td nowrap="nowrap"><b>Freq</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDIDenturesFreq" name="custDIDenturesFreq" value="<?=$custDIDenturesFreq?>" style="width: 75px" /></td>
    <td width="20px">&nbsp;</td>
	<td nowrap="nowrap"><b>Interim Partial (5820-5821)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDIPartial" name="custDIPartial" value="<?=$custDIPartial?>" style="width: 75px" />%</td>
    <td width="10px">&nbsp;</td>
	<td nowrap="nowrap"><b>Freq</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDIPartialFreq" name="custDIPartialFreq" value="<?=$custDIPartialFreq?>" style="width: 75px" /></td>
	</td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Reline (5730-5761)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDReline" name="custDReline" value="<?=$custDReline?>" style="width: 75px" />%</td>
    <td width="10px">&nbsp;</td>
    <td nowrap="nowrap"><b>Freq</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDRelineFreq" name="custDRelineFreq" value="<?=$custDRelineFreq?>" style="width: 75px" /></td>
    <td width="20px">&nbsp;</td>
	<td nowrap="nowrap"><b>Adjust (5410-5422)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDAdjust" name="custDAdjust" value="<?=$custDAdjust?>" style="width: 75px" />%</td>
    <td width="10px">&nbsp;</td>
	<td nowrap="nowrap"><b>Freq</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDAdjustFreq" name="custDAdjustFreq" value="<?=$custDAdjustFreq?>" style="width: 75px" /></td>
	</td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Repair (5510-5640)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDRepair" name="custDRepair" value="<?=$custDRepair?>" style="width: 75px" />%</td>
    <td width="10px">&nbsp;</td>
    <td nowrap="nowrap"><b>Freq</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDRepairFreq" name="custDRepairFreq" value="<?=$custDRepairFreq?>" style="width: 75px" /></td>
    <td width="20px">&nbsp;</td>
	<td nowrap="nowrap"><b>Flex Base Partials (5225-5226)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDFlexPartials" name="custDFlexPartials" value="<?=$custDFlexPartials?>" style="width: 75px" />%</td>
    <td width="10px">&nbsp;</td>
	<td nowrap="nowrap"><b>Freq</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custDFlexPartialsFreq" name="custDFlexPartialsFreq" value="<?=$custDFlexPartialsFreq?>" style="width: 75px" /></td>
	</td>
  </tr>
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">Oral Surgical Implant Services</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
  <tr>
    <td width="235" nowrap="nowrap"><b>Implant (6010)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSImplant" name="custSImplant" value="<?=$custSImplant?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Freq</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custSImplantFreq" name="custSImplantFreq" value="<?=$custSImplantFreq?>" style="width: 60px" /></td>
            <td width="20px">&nbsp;</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="235" nowrap="nowrap"><b>Screw Retained Single Crown (D6065)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSPrefabAbutment" name="custSPrefabAbutment" value="<?=$custSPrefabAbutment?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Freq</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custSPrefabAbutmentFreq" name="custSPrefabAbutmentFreq" value="<?=$custSPrefabAbutmentFreq?>" style="width: 60px" /></td>
            <td width="20px">&nbsp;</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="235" nowrap="nowrap"><b>Implant Abutment by Lab (6057)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSCustomAbutment" name="custSCustomAbutment" value="<?=$custSCustomAbutment?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Freq</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custSCustomAbutmentFreq" name="custSCustomAbutmentFreq" value="<?=$custSCustomAbutmentFreq?>" style="width: 60px" /></td>
            <td width="20px">&nbsp;</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>Implant Retainer Crown (D6075)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSceramicCrown" name="custSceramicCrown" value="<?=$custSceramicCrown?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Freq</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custSceramicCrownFreq" name="custSceramicCrownFreq" value="<?=$custSceramicCrownFreq?>" style="width: 60px" /></td>
            <td width="20px">&nbsp;</td>
        </tr>
        </table>
    </td>
  </tr>
  <!--<tr>
    <td width="130" nowrap="nowrap"><b>Implant Crown (6059)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSPFM" name="custSPFM" value="<?=$custSPFM?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Freq</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custSPFMFreq" name="custSPFMFreq" value="<?=$custSPFMFreq?>" style="width: 60px" /></td>
            <td width="20px">&nbsp;</td>
        </tr>
        </table>
    </td>
  </tr>-->
  <tr>
    <td width="130" nowrap="nowrap"><b>Imp Bone Graft (6104)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSGraft" name="custSGraft" value="<?=$custSGraft?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Freq</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custSGraftFreq" name="custSGraftFreq" value="<?=$custSGraftFreq?>" style="width: 60px" /></td>
            <td width="20px">&nbsp;</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>Overdentures/All-On-4 (D6110-D6115)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSGraft2" name="custSGraft2" value="<?=$custSGraft2?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Freq</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custSGraft2Freq" name="custSGraft2Freq" value="<?=$custSGraft2Freq?>" style="width: 60px" /></td>
            <td width="20px">&nbsp;</td>
        </tr>
        </table>
    </td>
  </tr>
  <!--<tr>
    <td width="130" nowrap="nowrap"><b>All-on-X (6114-6115)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSMembrane" name="custSMembrane" value="<?=$custSMembrane?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Freq</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custSMembraneFreq" name="custSMembraneFreq" value="<?=$custSMembraneFreq?>" style="width: 60px" /></td>
            <td width="20px">&nbsp;</td>
        </tr>
        </table>
    </td>
  </tr>-->
  <!--<tr>
    <td width="130" nowrap="nowrap"><b>Consult (9310)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSConsult" name="custSConsult" value="<?=$custSConsult?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Anesthesia - First 30min (9223)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custSAnesthesia" name="custSAnesthesia" value="<?=$custSAnesthesia?>" style="width: 50px" />%</td>
            <td width="20px">&nbsp;</td>
            <td><b>Additional 15min (9243)</b></td>
            <td><input type="text" class="textbox" id="custSAdditional" name="custSAdditional" value="<?=$custSAdditional?>" style="width: 50px" />%</td>
        </tr>
        </table>
    </td>
  </tr>-->
  <tr>
    <td width="130" nowrap="nowrap"><b>Simple Ext (7140)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSExt" name="custSExt" value="<?=$custSExt?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Covered</b></td>
            <td><input <? if($custSExtCovered == "Medical"){?>checked<? } ?> type="radio" name="custSExtCovered" value="Medical" /></td>
            <td>Medical</td>
            <td width="10px">&nbsp;</td>
            <td><input <? if($custSExtCovered == "Dental"){?>checked<? } ?> type="radio" name="custSExtCovered" value="Dental" /></td>
            <td>Dental</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>Surgical Ext (7210)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSSurgical" name="custSSurgical" value="<?=$custSSurgical?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Covered</b></td>
            <td><input <? if($custSSurgicalCovered == "Medical"){?>checked<? } ?> type="radio" name="custSSurgicalCovered" value="Medical" /></td>
            <td>Medical</td>
            <td width="10px">&nbsp;</td>
            <td><input <? if($custSSurgicalCovered == "Dental"){?>checked<? } ?> type="radio" name="custSSurgicalCovered" value="Dental" /></td>
            <td>Dental</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>Soft Tissue Ext (7220)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSTissue" name="custSTissue" value="<?=$custSTissue?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Covered</b></td>
            <td><input <? if($custSTissueCovered == "Medical"){?>checked<? } ?> type="radio" name="custSTissueCovered" value="Medical" /></td>
            <td>Medical</td>
            <td width="10px">&nbsp;</td>
            <td><input <? if($custSTissueCovered == "Dental"){?>checked<? } ?> type="radio" name="custSTissueCovered" value="Dental" /></td>
            <td>Dental</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>Partial Bony Ext (7230)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSBony" name="custSBony" value="<?=$custSBony?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Covered</b></td>
            <td><input <? if($custSBonyCovered == "Medical"){?>checked<? } ?> type="radio" name="custSBonyCovered" value="Medical" /></td>
            <td>Medical</td>
            <td width="10px">&nbsp;</td>
            <td><input <? if($custSBonyCovered == "Dental"){?>checked<? } ?> type="radio" name="custSBonyCovered" value="Dental" /></td>
            <td>Dental</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td width="130" nowrap="nowrap"><b>Complete Bony Ext (7240)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSBony2" name="custSBony2" value="<?=$custSBony2?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Covered</b></td>
            <td><input <? if($custSBony2Covered == "Medical"){?>checked<? } ?> type="radio" name="custSBony2Covered" value="Medical" /></td>
            <td>Medical</td>
            <td width="10px">&nbsp;</td>
            <td><input <? if($custSBony2Covered == "Dental"){?>checked<? } ?> type="radio" name="custSBony2Covered" value="Dental" /></td>
            <td>Dental</td>
        </tr>
        </table>
    </td>
  </tr>
  <!--<tr>
    <td width="130" nowrap="nowrap"><b>Ext Complications (7241)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSComp" name="custSComp" value="<?=$custSComp?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Covered</b></td>
            <td><input <? if($custSCompCovered == "Medical"){?>checked<? } ?> type="radio" name="custSCompCovered" value="Medical" /></td>
            <td>Medical</td>
            <td width="10px">&nbsp;</td>
            <td><input <? if($custSCompCovered == "Dental"){?>checked<? } ?> type="radio" name="custSCompCovered" value="Dental" /></td>
            <td>Dental</td>
        </tr>
        </table>
    </td>
  </tr>-->
  <tr>
    <td width="130" nowrap="nowrap"><b>Root Tip Ext (7250)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custSTip" name="custSTip" value="<?=$custSTip?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td colspan="5">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Covered</b></td>
            <td><input <? if($custSTipCovered == "Medical"){?>checked<? } ?> type="radio" name="custSTipCovered" value="Medical" /></td>
            <td>Medical</td>
            <td width="10px">&nbsp;</td>
            <td><input <? if($custSTipCovered == "Dental"){?>checked<? } ?> type="radio" name="custSTipCovered" value="Dental" /></td>
            <td>Dental</td>
        </tr>
        </table>
    </td>
  </tr>
  
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">Adjunctive</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
  <tr>
    <td width="235" nowrap="nowrap"><b>Occlusal Guards (9940)</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custOc" name="custOc" value="<?=$custOc?>" style="width: 50px" />%</td>
    <td width="20">&nbsp;</td>
    <td><b>Freq</b></td>
    <td><input type="text" class="textbox" id="custOcFreq" name="custOcFreq" value="<?=$custOcFreq?>" style="width: 75px" /></td>
    <td width="10">&nbsp;</td>
    <td colspan="2" valign="top" nowrap="nowrap">
    	<!--<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="100" nowrap="nowrap"><b>Age Minimum?</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custOcAgeMinimum" name="custOcAgeMinimum" value="<?=$custOcAgeMinimum?>" style="width: 75px" /></td>
            <td width="300">&nbsp;</td>
          </tr>
        </table>-->
	</td>
    </tr>
    <!--<tr>
		<td width="130" nowrap="nowrap"><b>Any Specific Guideline?</b></td>
		<td colspan="7" nowrap="nowrap">
			<table cellpadding="1" cellspacing="0">
			<tr>
				<td><input <? if($custOcGuideline == "None"){?>checked<? } ?> type="radio" name="custOcGuideline" value="None" /></td>
				<td><b>None</b></td>
				<td width="10px">&nbsp;</td>
				<td><input <? if($custOcGuideline == "Bruxism"){?>checked<? } ?> type="radio" name="custOcGuideline" value="Bruxism" /></td>
				<td><b>Bruxism</b></td>
				<td width="10px">&nbsp;</td>
				<td><input <? if($custOcGuideline == "Osseous Surgery"){?>checked<? } ?> type="radio" name="custOcGuideline" value="Osseous Surgery" /></td>
				<td><b>Osseous Surgery</b></td>
				<td width="10px">&nbsp;</td>
				<td><input <? if($custOcGuideline == "Ortho"){?>checked<? } ?> type="radio" name="custOcGuideline" value="Ortho" /></td>
				<td><b>Ortho</b></td>
			</tr>
			</table>
		</td>
    </tr>-->
	<tr>
		<td width="130" nowrap="nowrap"><b>Pallative (9110)</b></td>
		<td nowrap="nowrap"><input type="text" class="textbox" id="custOPallative" name="custOPallative" value="<?=$custOPallative?>" style="width: 50px" />%</td>
		<td width="20">&nbsp;</td>
		<td><b>Freq</b></td>
		<td><input type="text" class="textbox" id="custOPallativeFreq" name="custOPallativeFreq" value="<?=$custOPallativeFreq?>" style="width: 75px" /></td>
		<td width="10">&nbsp;</td>
		<td colspan="2" valign="top" nowrap="nowrap">
			<table cellpadding="3" cellspacing="0">
				<tr>
					<td width="200" nowrap="nowrap"><b>Same day xray or tx allowed?</b></td>
					<td><input <? if($custOXray == "Yes"){?>checked<? } ?> type="radio" name="custOXray" value="Yes" /></td>
					<td><b>Yes</b></td>
					<td width="10px">&nbsp;</td>
					<td><input <? if($custOXray == "No"){?>checked<? } ?> type="radio" name="custOXray" value="No" /></td>
					<td><b>No</b></td>
					<td width="300">&nbsp;</td>
				</tr>
			</table>
		</td>
    </tr>
	<tr>
		<td width="130" nowrap="nowrap"><b>Section Bridge (9120)</b></td>
		<td nowrap="nowrap"><input type="text" class="textbox" id="custOSectionBridge" name="custOSectionBridge" value="<?=$custOSectionBridge?>" style="width: 50px" />%</td>
		<td width="20">&nbsp;</td>
		<td><b>Freq</b></td>
		<td><input type="text" class="textbox" id="custOSectionBridgeFreq" name="custOSectionBridgeFreq" value="<?=$custOSectionBridgeFreq?>" style="width: 75px" /></td>
		<td width="10">&nbsp;</td>
		<td colspan="2" valign="top" nowrap="nowrap">
			&nbsp;
		</td>
    </tr>
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">Orthodontic</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
  <tr>
    <td colspan="7" nowrap="nowrap">
    	<table cellpadding="1" cellspacing="0">
    	  <tr>
    	    <td><input <? if($custOr == "Subscriber"){?>checked<? } ?> type="radio" name="custOr" value="Subscriber" /></td>
          <td><b>Subscriber</b></td>
          <td width="10px">&nbsp;</td>
          <td><input <? if($custOr == "Spouse"){?>checked<? } ?> type="radio" name="custOr" value="Spouse" /></td>
          <td><b>Spouse</b></td>
          <td width="10px">&nbsp;</td>
          <td><input <? if($custOr == "Dependent"){?>checked<? } ?> type="radio" name="custOr" value="Dependent" /></td>
          <td><b>Dependent</b></td>
          <td width="10px">&nbsp;</td>
          <td><input <? if($custOr == "All Covered"){?>checked<? } ?> type="radio" name="custOr" value="All Covered" /></td>
          <td><b>All Covered</b></td>
          </tr>
  	  </table></td>
    </tr>
  <tr>
    <td width="60" nowrap="nowrap"><b>Ortho</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custOrOrtho" name="custOrOrtho" value="<?=$custOrOrtho?>" style="width: 50px" />%</td>
    <td colspan="3">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="50" nowrap="nowrap"><b>To Age</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custOrAge" name="custOrAge" value="<?=$custOrAge?>" style="width: 75px" /></td>
            <td width="20px">&nbsp;</td>
            <td nowrap="nowrap"><b>Lifetime Max $</b></td>
            <td><input type="text" class="textbox" id="custOrLifetimeMax" name="custOrLifetimeMax" value="<?=$custOrLifetimeMax?>" style="width: 50px" /></td>
            <td width="20px">&nbsp;</td>
            <td nowrap="nowrap"><b>Remaining $</b></td>
            <td><input type="text" class="textbox" id="custOrRemaining" name="custOrRemaining" value="<?=$custOrRemaining?>" style="width: 50px" /></td>
            <td width="20px">&nbsp;</td>
            <td nowrap="nowrap"><b>Deductible $</b></td>
            <td><input type="text" class="textbox" id="custOrDeductible" name="custOrDeductible" value="<?=$custOrDeductible?>" style="width: 50px" /></td>
        </tr>
        </table>    </td>
    <td colspan="2" valign="top" nowrap="nowrap">&nbsp;</td>
  </tr>
  <tr>
    <td width="60" nowrap="nowrap"><b>Initial Payment</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custOrPayment" name="custOrPayment" value="<?=$custOrPayment?>" style="width: 50px" />%</td>
    <td colspan="3">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="40" nowrap="nowrap"><b>Paid:</b></td>
            <td><input <? if($custOrPaidMonthly == "Monthly"){?>checked<? } ?> type="checkbox" name="custOrPaidMonthly" value="Monthly" /></td>
            <td nowrap="nowrap"><b>Monthly</b></td>
            <td width="5px">&nbsp;</td>
            <td><input <? if($custOrPaidQuarterly == "Quarterly"){?>checked<? } ?> type="checkbox" name="custOrPaidQuarterly" value="Quarterly" /></td>
            <td nowrap="nowrap"><b>Quarterly</b></td>
            <td width="5px">&nbsp;</td>
            <td><input <? if($custOrPaidAutomatic == "Automatic"){?>checked<? } ?> type="checkbox" name="custOrPaidAutomatic" value="Automatic" /></td>
            <td nowrap="nowrap"><b>Automatic</b></td>
            <td width="5px">&nbsp;</td>
            <td><input <? if($custOrClaim == "We File Claim"){?>checked<? } ?> type="checkbox" name="custOrClaim" value="We File Claim" /></td>
            <td nowrap="nowrap"><b>We File Claim (Manually)</b></td>
          </tr>
        </table>    </td>
    <td colspan="2" valign="top" nowrap="nowrap">&nbsp;</td>
  </tr>
  <tr>
    <td nowrap="nowrap"><b>Work in progress covered?</b></td>
    <td nowrap="nowrap">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td><input <? if($custOrProgress == "Yes"){?>checked<? } ?> type="radio" name="custOrProgress" value="Yes" /></td>
            <td>Yes</td>
            <td width="5px">&nbsp;</td>
            <td><input <? if($custOrProgress == "No"){?>checked<? } ?> type="radio" name="custOrProgress" value="No" /></td>
            <td>No</td>
            <td width="200">&nbsp;</td>
          </tr>
        </table>
    </td>
    <td colspan="6" valign="top" nowrap="nowrap">
		<table cellpadding="3" cellspacing="0">
        <tr>
            <td nowrap="nowrap"><b>Diagnostic cast under Ortho (0470)</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custOrDiagnostic" name="custOrDiagnostic" value="<?=$custOrDiagnostic?>" style="width: 250px" />%</td>
          </tr>
        </table>
	</td>
  </tr>
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">History</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
  <tr>
    <td width="60" nowrap="nowrap"><b>History</b></td>
    <td nowrap="nowrap"><textarea class="textbox" id="custHistory" name="custHistory" rows="2"  style="width: 800px"><?=$custHistory?></textarea></td>
    </tr>
</table>

<br />

<?php
//$date = date_create(date('y-m-d'), timezone_open('Pacific/Nauru'));
$date = putenv("TZ=US/Pacific");
$pacific_time = date("h:i:s");
$todate = date("Y-m-d");

if($custDateTime == "")
{
	$custDateTime = $todate . " ". $pacific_time;
}
?>


  <table width="100%" cellpadding="5" cellspacing="0">
    <tr class="titleTr">
      <td colspan="4"><label id="rightLabel">Special Request</label></td>
    </tr>
  </table>
  <table cellpadding="3" cellspacing="5" width="100%" style="border:1px solid; background-color: #EFEFEF;">
    <tr>
      <td colspan="4"><b>Special Request</b></td>
    </tr>
    <tr>
      <td colspan="4">
      <textarea name="custSpecialRequest" id="custSpecialRequest" cols="100" rows="5"><?=$custSpecialRequest?></textarea>
      <script type="text/javascript">
	  CKEDITOR.replace( 'custSpecialRequest' );
	  </script>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td width="90px" valign="top"><b>Spoke with:</b></td>
      <td width="250px"><input type="text" class="textbox" id="custSpokeWith" name="custSpokeWith" value="<?=$custSpokeWith?>" style="width: 200px" /></td>
      <td width="140px" valign="top"><b>Account Executive:</b></td>
      <td><input type="text" class="textbox" id="custAccountExecutive" name="custAccountExecutive" value="<?=$custAccountExecutive?>" style="width: 200px" /></td>
    </tr>
    <tr>
      <td width="80px" valign="top"><b>Date/Time:</b></td>
      <td colspan="3"><input type="text" class="textbox" id="custDateTime" name="custDateTime" value="<?=date("m/d/Y h:i:s")?>" style="width: 200px" />
        <br />
        <p>Last Date / Time:
          <?=$custDateTime?>
        </p></td>
    </tr>
  </table>
</div>