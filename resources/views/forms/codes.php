<div id="div<?=$divCounter+=1?>" style="display: none;">
	
    <? 
	if($_SESSION["tmpSessionCompanyId"] == "17"  || $_SESSION["tmpSessionCompanyId"] == "34" 
    || $_SESSION["tmpSessionCompanyId"] == "35" || $_SESSION["tmpSessionCompanyId"] == "36"
    || $_SESSION["tmpSessionCompanyId"] == "37" || $_SESSION["tmpSessionCompanyId"] == "38" || $_SESSION["tmpSessionCompanyId"] == "39"
	|| $_SESSION["tmpSessionCompanyId"] == "40" || $_SESSION["tmpSessionCompanyId"] == "41"
	|| $_SESSION["tmpSessionCompanyId"] == "42" || $_SESSION["tmpSessionCompanyId"] == "43"
	|| $_SESSION["tmpSessionCompanyId"] == "44" || $_SESSION["tmpSessionCompanyId"] == "45"
	|| $_SESSION["tmpSessionCompanyId"] == "46" || $_SESSION["tmpSessionCompanyId"] == "47"
	|| $_SESSION["tmpSessionCompanyId"] == "50" || $_SESSION["tmpSessionCompanyId"] == "51"
	|| $_SESSION["tmpSessionCompanyId"] == "52" || $_SESSION["tmpSessionCompanyId"] == "53"
	|| $_SESSION["tmpSessionCompanyId"] == "54" || $_SESSION["tmpSessionCompanyId"] == "55"
	|| $_SESSION["tmpSessionCompanyId"] == "59" || $_SESSION["tmpSessionCompanyId"] == "60"
	|| $_SESSION["tmpSessionCompanyId"] == "61" || $_SESSION["tmpSessionCompanyId"] == "62"
	|| $_SESSION["tmpSessionCompanyId"] == "65" || $_SESSION["tmpSessionCompanyId"] == "66"
	|| $_SESSION["tmpSessionCompanyId"] == "67" || $_SESSION["tmpSessionCompanyId"] == "68"
	|| $_SESSION["tmpSessionCompanyId"] == "69" || $_SESSION["tmpSessionCompanyId"] == "70"
	|| $_SESSION["tmpSessionCompanyId"] == "71" || $_SESSION["tmpSessionCompanyId"] == "72"
	|| $_SESSION["tmpSessionCompanyId"] == "73" || $_SESSION["tmpSessionCompanyId"] == "74"
	|| $_SESSION["tmpSessionCompanyId"] == "75" || $_SESSION["tmpSessionCompanyId"] == "76"
	|| $_SESSION["tmpSessionCompanyId"] == "77" || $_SESSION["tmpSessionCompanyId"] == "78"
	|| $_SESSION["tmpSessionCompanyId"] == "111" || $_SESSION["tmpSessionCompanyId"] == "112"
	){ ?>
	<table cellpadding="3" cellspacing="0" width="100%">

		<tr>

			<td width="200px"><b>0350 (intra-oral pictures)- Covered</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeIntra" type="radio" <? if($codeIntra == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeIntra" type="radio" <? if($codeIntra == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td width="200px" style="padding-left:25px;"><b>Per Film</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeIntraFilm" type="radio" <? if($codeIntraFilm == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeIntraFilm" type="radio" <? if($codeIntraFilm == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td width="200px" style="padding-left:25px;"><b>Max no. of films</b></td>

			<td>

				<input type="text" class="textbox" id="codeIntraMax" name="codeIntraMax" value="<?=$codeIntraMax?>" />

			</td>

		</tr>
        
        <tr class="alternate">
			<td width="200px"><b>0383 (CT Image)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td><input name="codeCt" type="radio" <? if($codeCt == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="codeCt" type="radio" <? if($codeCt == "No"){?>checked<? } ?> value="No"/></td>
						<td width="10px">No</td>
					</tr>
				</table>
			</td>
		</tr>

		<tr>

			<td width="200px"><b>2962 (Veneers) - Covered</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeVeneer" type="radio" <? if($codeVeneer == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeVeneer" type="radio" <? if($codeVeneer == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td colspan="2">

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td width="50px">&nbsp;</td>

						<td>If Yes</td>

						<td width="15px">&nbsp;</td>

						<td>

							<table cellpadding="1" cellspacing="0">

								<tr>

									<td><input name="codeVeneerBasic" type="radio" <? if($codeVeneerBasic == "Basic"){?>checked<? } ?> value="Basic"/></td>

									<td width="30px">Basic</td>

									<td width="5px"><input name="codeVeneerBasic" type="radio" <? if($codeVeneerBasic == "Major"){?>checked<? } ?> value="Major"/></td>

									<td width="10px">Major</td>

								</tr>

							</table>

						</td>

						

					</tr>

				</table>

			</td>

		</tr>
        
        <tr class="alternate">

			<td width="200px"><b>4263 (bone grafting)</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeBone" type="radio" <? if($codeBone == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeBone" type="radio" <? if($codeBone == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr class="alternate">

			<td width="200px" style="padding-left:25px;" ><b>If yes, is covered in conjunction with extraction</b></td>

			<td valign="top">

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeBoneConjunction" type="radio" <? if($codeBoneConjunction == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeBoneConjunction" type="radio" <? if($codeBoneConjunction == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>
		</tr>
		
        <tr class="alternate">
			<td width="200px"><b>4266 (guided tissue regeneration)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td><input name="codeGuided" type="radio" <? if($codeGuided == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="codeGuided" type="radio" <? if($codeGuided == "No"){?>checked<? } ?> value="No"/></td>
						<td width="10px">No</td>
					</tr>
				</table>
			</td>
		</tr>

		<tr>
			<td width="200px"><b>4273(connective tissue graft)</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeConnective" type="radio" <? if($codeConnective == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeConnective" type="radio" <? if($codeConnective == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>
        
        <tr>
        	<td colspan="2">
            	<table cellpadding="3" cellspacing="0" width="100%">
                    <tr>
                        <td width="343"><b>Payment Structure for more than<br />
                      one in same quadrant</b></td>
                        <td width="864">
                          <input type="text" class="textbox" id="codeConnectivePayment" name="codeConnectivePayment" value="<?=$codeConnectivePayment?>" style="width:250px;"/>
                      </td>
                  </tr>
                </table>
            </td>
        </tr>

		<tr class="alternate">

			<td><b>4381 (Controlled release)</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="code4381" type="radio" <? if($code4381 == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>
						<td width="5px"><input name="code4381" type="radio" <? if($code4381 == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>
        
        <tr>

			<td><b>4910 (perio maintenance)</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codePerio4910" type="radio" <? if($codePerio4910 == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="40px">Yes</td>

						<td width="25px">OR</td>

						<td width="5px"><input name="codePerio4910" type="radio" <? if($codePerio4910 == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td width="200px" style="padding-left:25px;"><b>Basic / Preventative / Major</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codePerio" type="radio" <? if($codePerio == "Basic"){?>checked<? } ?> value="Basic"/></td>

						<td width="10px">Basic</td>

						<td width="5px"><input name="codePerio" type="radio" <? if($codePerio == "Preventative"){?>checked<? } ?> value="Preventative"/></td>

						<td width="10px">Preventative</td>

						<td width="5px"><input name="codePerio" type="radio" <? if($codePerio == "Major"){?>checked<? } ?> value="Major"/></td>

						<td width="10px">Major</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td width="200px" style="padding-left:25px;"><b>Frequency per year</b></td>

			<td>

				<input type="text" class="textbox" id="codePerioFreq" name="codePerioFreq" value="<?=$codePerioFreq?>" />

			</td>

		</tr>

		<tr>

			<td width="200px" style="padding-left:25px;"><b>Paid in addition to Prophy(0110)</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codePerioAddition" type="radio" <? if($codePerioAddition == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codePerioAddition" type="radio" <? if($codePerioAddition == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td width="200px" style="padding-left:25px;"><b>Paid in conjunction with Prophy</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codePerioConjunction" type="radio" <? if($codePerioConjunction == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codePerioConjunction" type="radio" <? if($codePerioConjunction == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td width="200px" style="padding-left:25px;"><b>Covered in combination with Flouride</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codePerioFlouride" type="radio" <? if($codePerioFlouride == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codePerioFlouride" type="radio" <? if($codePerioFlouride == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>
        
        <tr class="alternate">
			<td><b>4921 (Gingival Irrigation)</b></td>
			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>
						<td><input name="code4921" type="radio" <? if($code4921 == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="code4921" type="radio" <? if($code4921 == "No"){?>checked<? } ?> value="No"/></td>
						<td width="10px">No</td>
					</tr>

				</table>
			</td>
		</tr>
        
        <tr>
			<td><b>5810 (Interim Full Upper)</b></td>
			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>
						<td><input name="code5810" type="radio" <? if($code5810 == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="code5810" type="radio" <? if($code5810 == "No"){?>checked<? } ?> value="No"/></td>
						<td width="10px">No</td>
					</tr>

				</table>
			</td>
		</tr>
        
        <tr class="alternate">
			<td><b>5820 (Interim Full Lower)</b></td>
			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>
						<td><input name="code5820" type="radio" <? if($code5820 == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="code5820" type="radio" <? if($code5820 == "No"){?>checked<? } ?> value="No"/></td>
						<td width="10px">No</td>
					</tr>

				</table>
			</td>
		</tr>
        
	</table>


	<table cellpadding="3" cellspacing="0" width="100%">

		<tr>

			<td width="200px"><b>6010 (implant)- Covered</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeImplant" type="radio" <? if($codeImplant == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeImplant" type="radio" <? if($codeImplant == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td width="200px" style="padding-left:25px;" ><b>Alternate benefit if not covered</b></td>

			<td valign="top">

				<input type="text" class="textbox" id="codeImplantBenefit" name="codeImplantBenefit" value="<?=$codeImplantBenefit?>" style="width:250px;"/>

			</td>

		</tr>
        
		<tr class="alternate">
			<td width="200px"><b>D6057 (implant abutment)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td><input name="codeImpAbutment" type="radio" <? if($codeImpAbutment == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="codeImpAbutment" type="radio" <? if($codeImpAbutment == "No"){?>checked<? } ?> value="No"/></td>
						<td width="30px">No</td>
					</tr>
				</table>
			</td>
		</tr>
        
        <tr class="alternate">
			<td width="200px"><b>D6059 (implant crown)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td><input name="codeImpCrown" type="radio" <? if($codeImpCrown == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="codeImpCrown" type="radio" <? if($codeImpCrown == "No"){?>checked<? } ?> value="No"/></td>
						<td width="30px">No</td>
					</tr>
				</table>
			</td>
		</tr>
        
        <tr>
			<td width="200px"><b>6104 (Bone Graft + Imp, same day)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td><input name="code6104" type="radio" <? if($code6104 == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="code6104" type="radio" <? if($code6104 == "No"){?>checked<? } ?> value="No"/></td>
						<td width="10px">No</td>
					</tr>
				</table>
			</td>
		</tr>
        
        <tr class="alternate">
			<td width="200px"><b>7953 (Bone repl)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td><input name="code7953" type="radio" <? if($code7953 == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="code7953" type="radio" <? if($code7953 == "No"){?>checked<? } ?> value="No"/></td>
						<td width="10px">No</td>
					</tr>
				</table>
			</td>
		</tr>
        
        <tr>

			<td width="100px"><b>9110 Frequency</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeFrequency9110" type="radio" <? if($codeFrequency9110 == "Basic"){?>checked<? } ?> value="Basic"/></td>

						<td width="30px">Basic</td>

						<td width="5px"><input name="codeFrequency9110" type="radio" <? if($codeFrequency9110 == "Preventative"){?>checked<? } ?> value="Preventative"/></td>

						<td width="30px">Preventative</td>

						<td width="5px"><input name="codeFrequency9110" type="radio" <? if($codeFrequency9110 == "Not Covered"){?>checked<? } ?> value="Not Covered"/></td>

						<td>Not Covered</td>

						<td width="15px">&nbsp;</td>

						<td>Does it count the frequency of an exam?</td>

						<td><input name="codeFrequency9110Count" type="radio" <? if($codeFrequency9110Count == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeFrequency9110Count" type="radio" <? if($codeFrequency9110Count == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr class="alternate">

			<td width="200px"><b>9223 (General anesthetic)</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeGen9220" type="radio" <? if($codeGen9220 == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeGen9220" type="radio" <? if($codeGen9220 == "No"){?>checked<? } ?> value="No"/></td>

						<td width="30px">No</td>

						<td width="80px">% in network</td>

						<td width="70px"><input type="text" class="textbox" id="codeGen9220Per" name="codeGen9220Per" value="<?=$codeGen9220Per?>" style="width:50px;"/></td>

						<td width="100px">% out of network</td>

						<td><input type="text" class="textbox" id="codeGen9220Network" name="codeGen9220Network" value="<?=$codeGen9220Network?>" style="width:50px;"/></td>

					</tr>

				</table>

			</td>

		</tr>

		<tr class="alternate">

			<td width="200px" style="padding-left:25px;"><b>Is there age restriction?</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeGen9220Res" type="radio" <? if($codeGen9220Res == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeGen9220Res" type="radio" <? if($codeGen9220Res == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr class="alternate">

			<td width="200px" style="padding-left:25px;"><b>If yes what age?</b></td>

			<td>

				<input type="text" class="textbox" id="codeGen9220Age" name="codeGen9220Age" value="<?=$codeGen9220Age?>" />

			</td>

		</tr>

		<?php /*?><tr>

			<td width="200px"><b>9221(General anesthestic) - Covered</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeGen9221" type="radio" <? if($codeGen9221 == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeGen9221" type="radio" <? if($codeGen9221 == "No"){?>checked<? } ?> value="No"/></td>

						<td>No</td>													

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td width="200px" style="padding-left:25px;"><b>Is there age restriction?</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeGen9221Res" type="radio" <? if($codeGen9221Res == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeGen9221Res" type="radio" <? if($codeGen9221Res == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td width="200px" style="padding-left:25px;"><b>If yes what age?</b></td>

			<td>

				<input type="text" class="textbox" id="codeGen9221Age" name="codeGen9221Age" value="<?=$codeGen9221Age?>" />

			</td>
		</tr>
        <?php */?>
        <tr class="">
			<td width="200px"><b>9230 (Nitrous Oxide)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td><input name="code9230" type="radio" <? if($code9230 == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="code9230" type="radio" <? if($code9230 == "No"){?>checked<? } ?> value="No"/></td>
						<td width="10px">No</td>
					</tr>
				</table>
			</td>
		</tr>
        
        <tr class="alternate">
			<td width="200px"><b>9243 (IV Sedation)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td><input name="code9241" type="radio" <? if($code9241 == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="code9241" type="radio" <? if($code9241 == "No"){?>checked<? } ?> value="No"/></td>
						<td width="10px">No</td>
					</tr>
				</table>
			</td>
		</tr>
        <?php /*?>
        <tr class="alternate">
			<td width="200px"><b>9242 (IV Conscious Sedation 15 min)</b></td>
			<td>
				<table cellpadding="1" cellspacing="0">
					<tr>
						<td><input name="code9242" type="radio" <? if($code9242 == "Yes"){?>checked<? } ?> value="Yes"/></td>
						<td width="30px">Yes</td>
						<td width="5px"><input name="code9242" type="radio" <? if($code9242 == "No"){?>checked<? } ?> value="No"/></td>
						<td width="10px">No</td>
					</tr>
				</table>
			</td>
		</tr>
        <?php */?>
		</table>


		<table cellpadding="3" cellspacing="0" width="100%">
		<tr>

			<td width="250px"><b>9248 (Non IV conscious sedation)-Covered</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeSedation" type="radio" <? if($codeSedation == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeSedation" type="radio" <? if($codeSedation == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td colspan="2">

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td width="50px">&nbsp;</td>

						<td>If yes</td>

						<td width="15px">&nbsp;</td>

						<td>

							<table cellpadding="1" cellspacing="0">

								<tr>

									<td><input name="codeSedationBasic" type="radio" <? if($codeSedationBasic == "Basic"){?>checked<? } ?> value="Basic"/></td>

									<td width="30px">Basic</td>

									<td width="30px" align="center">OR</td>

									<td width="5px"><input name="codeSedationBasic" type="radio" <? if($codeSedationBasic == "Major"){?>checked<? } ?> value="Major"/></td>

									<td width="10px">Major</td>

								</tr>

							</table>

						</td>

					</tr>

					

					<!-- new changes -->

					<tr>

						<td width="50px">&nbsp;</td>

						<td>Is there age restriction?</td>

						<td width="15px">&nbsp;</td>

						<td>

							<table cellpadding="1" cellspacing="0">

								<tr>

									<td>

										<input type="radio" <? if($codeSedationAgeRestriction == "Yes"){?>checked<? } ?>  name="codeSedationAgeRestriction" value="Yes" /></td>

									<td>Yes</td>

									<td width="15px">&nbsp;</td>

									<td><input type="radio" <? if($codeSedationAgeRestriction == "No"){?>checked<? } ?>  name="codeSedationAgeRestriction" value="No" /></td>

									<td>No</td>

								</tr>

							</table>

						</td>

					</tr>

					<tr>

						<td width="20px">&nbsp;</td>

						<td>If yes what age?</td>

						<td width="15px">&nbsp;</td>

						<td><input type="text" class="textbox" name="codeSedationAge" value="<?=$codeSedationAge?>" /></td>

					</tr>

				</table>

			</td>

		</tr>

		</table>

		<table cellpadding="3" cellspacing="0" width="100%">

		<tr class="alternate">

			<td width="100px"><b>9310 Frequency</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeFrequency" type="radio" <? if($codeFrequency == "Basic"){?>checked<? } ?> value="Basic"/></td>

						<td width="30px">Basic</td>

						<td width="5px"><input name="codeFrequency" type="radio" <? if($codeFrequency == "Preventative"){?>checked<? } ?> value="Preventative"/></td>

						<td width="30px">Preventative</td>

						<td width="5px"><input name="codeFrequency" type="radio" <? if($codeFrequency == "Not Covered"){?>checked<? } ?> value="Not Covered"/></td>

						<td>Not Covered</td>

						<td width="15px">&nbsp;</td>

						<td>Does it count the frequency of an exam?</td>

						<td><input name="codeFrequencyCount" type="radio" <? if($codeFrequencyCount == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeFrequencyCount" type="radio" <? if($codeFrequencyCount == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		</table>

		<table cellpadding="3" cellspacing="0" width="100%">

		

		<tr>

			<td width="120px" valign="top"><b>X-rays necessary</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td width="80px">Crowns</td>

						<td><input name="xrayCrowns" type="radio" <? if($xrayCrowns == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="xrayCrowns" type="radio" <? if($xrayCrowns == "No"){?>checked<? } ?> value="No"/></td>

						<td width="50px">No</td>

						<td width="100px">Bridges</td>

						<td><input name="xrayBridges" type="radio" <? if($xrayBridges == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="xrayBridges" type="radio" <? if($xrayBridges == "No"){?>checked<? } ?> value="No"/></td>

						<td width="25px">No</td>

					</tr>

					<tr>

						<td width="80px">RCT</td>

						<td><input name="xrayRct" type="radio" <? if($xrayRct == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="xrayRct" type="radio" <? if($xrayRct == "No"){?>checked<? } ?> value="No"/></td>

						<td width="50px">No</td>

						<td width="100px">Dentures</td>

						<td><input name="xrayDentures" type="radio" <? if($xrayDentures == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="xrayDentures" type="radio" <? if($xrayDentures == "No"){?>checked<? } ?> value="No"/></td>

						<td width="25px">No</td>

					</tr>

					<tr>

						<td width="80px">Extractions</td>

						<td><input name="xrayExtractions" type="radio" <? if($xrayExtractions == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="xrayExtractions" type="radio" <? if($xrayExtractions == "No"){?>checked<? } ?> value="No"/></td>

						<td width="50px">No</td>

						<td width="100px">Inlaylorlary(2642)</td>

						<td><input name="xrayInlay" type="radio" <? if($xrayInlay == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="xrayInlay" type="radio" <? if($xrayInlay == "No"){?>checked<? } ?> value="No"/></td>

						<td width="25px">No</td>

					</tr>

					<tr>

						<td width="80px">SRP(4241)</td>

						<td><input name="xraySrp4241" type="radio" <? if($xraySrp4241 == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="xraySrp4241" type="radio" <? if($xraySrp4241 == "No"){?>checked<? } ?> value="No"/></td>

						<td width="50px">No</td>

						<td width="100px">SRP(4341)</td>

						<td><input name="xraySrp4341" type="radio" <? if($xraySrp4341 == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="xraySrp4341" type="radio" <? if($xraySrp4341 == "No"){?>checked<? } ?> value="No"/></td>

						<td width="25px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		</table>
        
        
    <? }else{ ?>
    
    <table cellpadding="3" cellspacing="0" width="100%">

		<tr>

			<td width="200px"><b>0350 (intra-oral pictures)- Covered</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeIntra" type="radio" <? if($codeIntra == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeIntra" type="radio" <? if($codeIntra == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td width="200px" style="padding-left:25px;"><b>Per Film</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeIntraFilm" type="radio" <? if($codeIntraFilm == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeIntraFilm" type="radio" <? if($codeIntraFilm == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td width="200px" style="padding-left:25px;"><b>Max no. of films</b></td>

			<td>

				<input type="text" class="textbox" id="codeIntraMax" name="codeIntraMax" value="<?=$codeIntraMax?>" />

			</td>

		</tr>

		<tr class="alternate">

			<td width="200px"><b>2962 (Veneers) - Covered</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeVeneer" type="radio" <? if($codeVeneer == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeVeneer" type="radio" <? if($codeVeneer == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr class="alternate">

			<td colspan="2">

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td width="50px">&nbsp;</td>

						<td>If Yes</td>

						<td width="15px">&nbsp;</td>

						<td>

							<table cellpadding="1" cellspacing="0">

								<tr>

									<td><input name="codeVeneerBasic" type="radio" <? if($codeVeneerBasic == "Basic"){?>checked<? } ?> value="Basic"/></td>

									<td width="30px">Basic</td>

									<td width="5px"><input name="codeVeneerBasic" type="radio" <? if($codeVeneerBasic == "Major"){?>checked<? } ?> value="Major"/></td>

									<td width="10px">Major</td>

								</tr>

							</table>

						</td>

						

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td><b>4910 (perio maintenance)</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codePerio4910" type="radio" <? if($codePerio4910 == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="40px">Yes</td>

						<td width="25px">OR</td>

						<td width="5px"><input name="codePerio4910" type="radio" <? if($codePerio4910 == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td width="200px" style="padding-left:25px;"><b>Basic / Preventative / Major</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codePerio" type="radio" <? if($codePerio == "Basic"){?>checked<? } ?> value="Basic"/></td>

						<td width="10px">Basic</td>

						<td width="5px"><input name="codePerio" type="radio" <? if($codePerio == "Preventative"){?>checked<? } ?> value="Preventative"/></td>

						<td width="10px">Preventative</td>

						<td width="5px"><input name="codePerio" type="radio" <? if($codePerio == "Major"){?>checked<? } ?> value="Major"/></td>

						<td width="10px">Major</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td width="200px" style="padding-left:25px;"><b>Frequency per year</b></td>

			<td>

				<input type="text" class="textbox" id="codePerioFreq" name="codePerioFreq" value="<?=$codePerioFreq?>" />


			</td>

		</tr>

		<tr>

			<td width="200px" style="padding-left:25px;"><b>Paid in addition to Prophy(0110)</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codePerioAddition" type="radio" <? if($codePerioAddition == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codePerioAddition" type="radio" <? if($codePerioAddition == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td width="200px" style="padding-left:25px;"><b>Paid in conjunction with Prophy</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codePerioConjunction" type="radio" <? if($codePerioConjunction == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codePerioConjunction" type="radio" <? if($codePerioConjunction == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td width="200px" style="padding-left:25px;"><b>Covered in combination with Flouride</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codePerioFlouride" type="radio" <? if($codePerioFlouride == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codePerioFlouride" type="radio" <? if($codePerioFlouride == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr class="alternate">

			<td width="200px"><b>4263 (bone grafting)</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeBone" type="radio" <? if($codeBone == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeBone" type="radio" <? if($codeBone == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr class="alternate">

			<td width="200px" style="padding-left:25px;" ><b>If yes, is covered in conjunction with extraction</b></td>

			<td valign="top">

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeBoneConjunction" type="radio" <? if($codeBoneConjunction == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeBoneConjunction" type="radio" <? if($codeBoneConjunction == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td width="200px"><b>4273(connective tissue graft)</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeConnective" type="radio" <? if($codeConnective == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeConnective" type="radio" <? if($codeConnective == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

	</table>

	<table cellpadding="3" cellspacing="0" width="100%">

		<tr class="alternate">

			<td width="320px"><b>Payment Structure for more than one in same quadrant</b></td>

			<td>

				<input type="text" class="textbox" id="codeConnectivePayment" name="codeConnectivePayment" value="<?=$codeConnectivePayment?>" style="width:250px;"/>

			</td>

		</tr>

	</table>

	<table cellpadding="3" cellspacing="0" width="100%">

		<tr>

			<td width="200px"><b>6010 (implant)- Covered</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeImplant" type="radio" <? if($codeImplant == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeImplant" type="radio" <? if($codeImplant == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td width="200px" style="padding-left:25px;" ><b>Alternate benefit if not covered</b></td>

			<td valign="top">

				<input type="text" class="textbox" id="codeImplantBenefit" name="codeImplantBenefit" value="<?=$codeImplantBenefit?>" style="width:250px;"/>

			</td>

		</tr>

		<tr class="alternate">

			<td width="200px"><b>6066(implant crown)-Covered</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeCrowns" type="radio" <? if($codeCrowns == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeCrowns" type="radio" <? if($codeCrowns == "No"){?>checked<? } ?> value="No"/></td>

						<td width="30px">No</td>

						<td width="80px">% in network</td>

						<td width="70px"><input type="text" class="textbox" id="codeCrownsPer" name="codeCrownsPer" value="<?=$codeCrownsPer?>" style="width:50px;"/></td>

						<td width="100px">% out of network</td>

						<td><input type="text" class="textbox" id="codeCrownsNetwork" name="codeCrownsNetwork" value="<?=$codeCrownsNetwork?>" style="width:50px;"/></td>

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td width="200px"><b>9220(General anesthetic)-Covered</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeGen9220" type="radio" <? if($codeGen9220 == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeGen9220" type="radio" <? if($codeGen9220 == "No"){?>checked<? } ?> value="No"/></td>

						<td width="30px">No</td>

						<td width="80px">% in network</td>

						<td width="70px"><input type="text" class="textbox" id="codeGen9220Per" name="codeGen9220Per" value="<?=$codeGen9220Per?>" style="width:50px;"/></td>

						<td width="100px">% out of network</td>

						<td><input type="text" class="textbox" id="codeGen9220Network" name="codeGen9220Network" value="<?=$codeGen9220Network?>" style="width:50px;"/></td>

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td width="200px" style="padding-left:25px;"><b>Is there age restriction?</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeGen9220Res" type="radio" <? if($codeGen9220Res == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeGen9220Res" type="radio" <? if($codeGen9220Res == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td width="200px" style="padding-left:25px;"><b>If yes what age?</b></td>

			<td>

				<input type="text" class="textbox" id="codeGen9220Age" name="codeGen9220Age" value="<?=$codeGen9220Age?>" />

			</td>

		</tr>

		<tr class="alternate">

			<td width="200px"><b>9221(General anesthestic) - Covered</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeGen9221" type="radio" <? if($codeGen9221 == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeGen9221" type="radio" <? if($codeGen9221 == "No"){?>checked<? } ?> value="No"/></td>

						<td>No</td>													

					</tr>

				</table>

			</td>

		</tr>

		<tr class="alternate">

			<td width="200px" style="padding-left:25px;"><b>Is there age restriction?</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeGen9221Res" type="radio" <? if($codeGen9221Res == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeGen9221Res" type="radio" <? if($codeGen9221Res == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr class="alternate">

			<td width="200px" style="padding-left:25px;"><b>If yes what age?</b></td>

			<td>

				<input type="text" class="textbox" id="codeGen9221Age" name="codeGen9221Age" value="<?=$codeGen9221Age?>" />

			</td>

		</tr>

		</table>

		<table cellpadding="3" cellspacing="0" width="100%">

		<tr>

			<td width="100px"><b>9310 Frequency</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeFrequency" type="radio" <? if($codeFrequency == "Basic"){?>checked<? } ?> value="Basic"/></td>

						<td width="30px">Basic</td>

						<td width="5px"><input name="codeFrequency" type="radio" <? if($codeFrequency == "Preventative"){?>checked<? } ?> value="Preventative"/></td>

						<td width="30px">Preventative</td>

						<td width="5px"><input name="codeFrequency" type="radio" <? if($codeFrequency == "Not Covered"){?>checked<? } ?> value="Not Covered"/></td>

						<td>Not Covered</td>

						<td width="15px">&nbsp;</td>

						<td>Does it count the frequency of an exam?</td>

						<td><input name="codeFrequencyCount" type="radio" <? if($codeFrequencyCount == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeFrequencyCount" type="radio" <? if($codeFrequencyCount == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td width="100px"><b>9110 Frequency</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeFrequency9110" type="radio" <? if($codeFrequency9110 == "Basic"){?>checked<? } ?> value="Basic"/></td>

						<td width="30px">Basic</td>

						<td width="5px"><input name="codeFrequency9110" type="radio" <? if($codeFrequency9110 == "Preventative"){?>checked<? } ?> value="Preventative"/></td>

						<td width="30px">Preventative</td>

						<td width="5px"><input name="codeFrequency9110" type="radio" <? if($codeFrequency9110 == "Not Covered"){?>checked<? } ?> value="Not Covered"/></td>

						<td>Not Covered</td>

						<td width="15px">&nbsp;</td>

						<td>Does it count the frequency of an exam?</td>

						<td><input name="codeFrequency9110Count" type="radio" <? if($codeFrequency9110Count == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeFrequency9110Count" type="radio" <? if($codeFrequency9110Count == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		</table>

		<table cellpadding="3" cellspacing="0" width="100%">

		<tr class="alternate">

			<td width="250px"><b>9248 (Non IV conscious sedation)-Covered</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="codeSedation" type="radio" <? if($codeSedation == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="codeSedation" type="radio" <? if($codeSedation == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr class="alternate">

			<td colspan="2">

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td width="50px">&nbsp;</td>

						<td>If yes</td>

						<td width="15px">&nbsp;</td>

						<td>

							<table cellpadding="1" cellspacing="0">

								<tr>

									<td><input name="codeSedationBasic" type="radio" <? if($codeSedationBasic == "Basic"){?>checked<? } ?> value="Basic"/></td>

									<td width="30px">Basic</td>

									<td width="30px" align="center">OR</td>

									<td width="5px"><input name="codeSedationBasic" type="radio" <? if($codeSedationBasic == "Major"){?>checked<? } ?> value="Major"/></td>

									<td width="10px">Major</td>

								</tr>

							</table>

						</td>

					</tr>

					

					<!-- new changes -->

					<tr>

						<td width="50px">&nbsp;</td>

						<td>Is there age restriction?</td>

						<td width="15px">&nbsp;</td>

						<td>

							<table cellpadding="1" cellspacing="0">

								<tr>

									<td>

										<input type="radio" <? if($codeSedationAgeRestriction == "Yes"){?>checked<? } ?>  name="codeSedationAgeRestriction" value="Yes" /></td>

									<td>Yes</td>

									<td width="15px">&nbsp;</td>

									<td><input type="radio" <? if($codeSedationAgeRestriction == "No"){?>checked<? } ?>  name="codeSedationAgeRestriction" value="No" /></td>

									<td>No</td>

								</tr>

							</table>

						</td>

					</tr>

					<tr>

						<td width="20px">&nbsp;</td>

						<td>If yes what age?</td>

						<td width="15px">&nbsp;</td>

						<td><input type="text" class="textbox" name="codeSedationAge" value="<?=$codeSedationAge?>" /></td>

					</tr>

				</table>

			</td>

		</tr>

		</table>



		<table cellpadding="3" cellspacing="0" width="100%">

		

		<tr>

			<td width="120px" valign="top"><b>X-rays necessary</b></td>

			<td>

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td width="80px">Crowns</td>

						<td><input name="xrayCrowns" type="radio" <? if($xrayCrowns == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="xrayCrowns" type="radio" <? if($xrayCrowns == "No"){?>checked<? } ?> value="No"/></td>

						<td width="50px">No</td>

						<td width="100px">Bridges</td>

						<td><input name="xrayBridges" type="radio" <? if($xrayBridges == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="xrayBridges" type="radio" <? if($xrayBridges == "No"){?>checked<? } ?> value="No"/></td>

						<td width="25px">No</td>

					</tr>

					<tr>

						<td width="80px">RCT</td>

						<td><input name="xrayRct" type="radio" <? if($xrayRct == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="xrayRct" type="radio" <? if($xrayRct == "No"){?>checked<? } ?> value="No"/></td>

						<td width="50px">No</td>

						<td width="100px">Dentures</td>

						<td><input name="xrayDentures" type="radio" <? if($xrayDentures == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="xrayDentures" type="radio" <? if($xrayDentures == "No"){?>checked<? } ?> value="No"/></td>

						<td width="25px">No</td>

					</tr>

					<tr>

						<td width="80px">Extractions</td>

						<td><input name="xrayExtractions" type="radio" <? if($xrayExtractions == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="xrayExtractions" type="radio" <? if($xrayExtractions == "No"){?>checked<? } ?> value="No"/></td>

						<td width="50px">No</td>

						<td width="100px">Inlaylorlary(2642)</td>

						<td><input name="xrayInlay" type="radio" <? if($xrayInlay == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="xrayInlay" type="radio" <? if($xrayInlay == "No"){?>checked<? } ?> value="No"/></td>

						<td width="25px">No</td>

					</tr>

					<tr>

						<td width="80px">SRP(4241)</td>

						<td><input name="xraySrp4241" type="radio" <? if($xraySrp4241 == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="xraySrp4241" type="radio" <? if($xraySrp4241 == "No"){?>checked<? } ?> value="No"/></td>

						<td width="50px">No</td>

						<td width="100px">SRP(4341)</td>

						<td><input name="xraySrp4341" type="radio" <? if($xraySrp4341 == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="xraySrp4341" type="radio" <? if($xraySrp4341 == "No"){?>checked<? } ?> value="No"/></td>

						<td width="25px">No</td>

					</tr>

				</table>

			</td>

		</tr>

	</table>
    
    <? } ?>    

</div>