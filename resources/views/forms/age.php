<br /><br />

	<table cellpadding="3" cellspacing="0" width="100%">

		<tr>

			<td width="220px"><b>1st Molars</b></td>

			<td width="5px"><input name="ageMolars1" type="radio" <? if($ageMolars1 == "Yes"){?>checked<? } ?> value="Yes"/></td>

			<td width="210px"><input type="text" class="textbox" id="ageMolars1Desc" name="ageMolars1Desc" value="<?=$ageMolars1Desc?>" style="width: 200px" /></td>

			<td width="10px">Yrs</td>

			<td width="50px">OR</td>

			<td width="10px"><input name="ageMolars1" type="radio" <? if($ageMolars1 == "Not Covered"){?>checked<? } ?> value="Not Covered"/></td>

			<td>Not covered</td>

		</tr>

		<tr class="alternate">

			<td width="220px"><b>2nd Molars</b></td>

			<td width="5px"><input name="ageMolars2" type="radio" <? if($ageMolars2 == "Yes"){?>checked<? } ?> value="Yes"/></td>

			<td width="210px"><input type="text" class="textbox" id="ageMolars2Desc" name="ageMolars2Desc" value="<?=$ageMolars2Desc?>" style="width: 200px" /></td>

			<td width="10px">Yrs</td>

			<td width="50px">OR</td>

			<td width="10px"><input name="ageMolars2" type="radio" <? if($ageMolars2 == "Not Covered"){?>checked<? } ?> value="Not Covered"/></td>

			<td>Not covered</td>

		</tr>

		<tr>

			<td width="220px"><b>Bicuspids</b></td>

			<td width="5px"><input name="ageBiscuspids" type="radio" <? if($ageBiscuspids == "Yes"){?>checked<? } ?> value="Yes"/></td>

			<td width="210px"><input type="text" class="textbox" id="ageBiscuspidsDesc" name="ageBiscuspidsDesc" value="<?=$ageBiscuspidsDesc?>" style="width: 200px" /></td>

			<td width="10px">Yrs</td>

			<td width="50px">OR</td>

			<td width="10px"><input name="ageBiscuspids" type="radio" <? if($ageBiscuspids == "Not Covered"){?>checked<? } ?> value="Not Covered"/></td>

			<td>Not covered</td>

		</tr>

		<tr class="alternate">

			<td width="220px"><b>3rd Molars</b></td>

			<td width="5px"><input name="ageMolars3" type="radio" <? if($ageMolars3 == "Yes"){?>checked<? } ?> value="Yes"/></td>

			<td width="210px"><input type="text" class="textbox" id="ageMolars3Desc" name="ageMolars3Desc" value="<?=$ageMolars3Desc?>" style="width: 200px" /></td>

			<td width="10px">Yrs</td>

			<td width="50px">OR</td>

			<td width="10px"><input name="ageMolars3" type="radio" <? if($ageMolars3 == "Not Covered"){?>checked<? } ?> value="Not Covered"/></td>

			<td>Not covered</td>

		</tr>

		<tr>

			<td width="220px"><b>Space Maintainer (1510)</b></td>

			<td colspan="6">

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td width="5px"><input name="ageSpace" type="radio" <? if($ageSpace == "Preventive"){?>checked<? } ?> value="Preventive"/></td>

						<td width="50px">Preventative</td>

						<td><input name="ageSpace" type="radio" <? if($ageSpace == "Basic"){?>checked<? } ?> value="Basic"/></td>

						<td width="50px">Basic</td>													

						<td width="60px">Upto Age</td>

						<td width="60px"><input type="text" class="textbox" id="ageSpaceAge" name="ageSpaceAge" value="<?=$ageSpaceAge?>" style="width: 50px" /></td>

						<td width="40px">yrs</td>

						<td width="10px"><input name="ageSpace" type="radio" <? if($ageSpace == "Not covered"){?>checked<? } ?> value="Not covered"/></td>

						<td>Not covered</td>

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td>&nbsp;</td>

			<td colspan="6">

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td width="5px">&nbsp;</td>

						<td>Frequency</td>

						<td width="5px">&nbsp;</td>

						<td><input type="text" class="textbox" name="ageFrequency" value="<?=$ageFrequency?>" /></td>

					</tr>

				</table>

			</td>

		</tr>
        
        <? if($_SESSION["tmpSessionCompanyId"] != "17" && $_SESSION["tmpSessionCompanyId"] != "18" 
        && $_SESSION["tmpSessionCompanyId"] != "34" && $_SESSION["tmpSessionCompanyId"] != "35"
        && $_SESSION["tmpSessionCompanyId"] != "36" && $_SESSION["tmpSessionCompanyId"] != "37"
		&& $_SESSION["tmpSessionCompanyId"] != "38" && $_SESSION["tmpSessionCompanyId"] != "39"
		&& $_SESSION["tmpSessionCompanyId"] != "40" && $_SESSION["tmpSessionCompanyId"] != "41"
		&& $_SESSION["tmpSessionCompanyId"] != "42" && $_SESSION["tmpSessionCompanyId"] != "43"
		&& $_SESSION["tmpSessionCompanyId"] != "44" && $_SESSION["tmpSessionCompanyId"] != "45"
		&& $_SESSION["tmpSessionCompanyId"] != "46" && $_SESSION["tmpSessionCompanyId"] != "47"
		&& $_SESSION["tmpSessionCompanyId"] != "50" && $_SESSION["tmpSessionCompanyId"] != "51"
		&& $_SESSION["tmpSessionCompanyId"] != "52" && $_SESSION["tmpSessionCompanyId"] != "53"
		&& $_SESSION["tmpSessionCompanyId"] != "54" && $_SESSION["tmpSessionCompanyId"] != "55"
		&& $_SESSION["tmpSessionCompanyId"] != "59" && $_SESSION["tmpSessionCompanyId"] != "60"
		&& $_SESSION["tmpSessionCompanyId"] != "61" && $_SESSION["tmpSessionCompanyId"] != "62"
		&& $_SESSION["tmpSessionCompanyId"] != "65" && $_SESSION["tmpSessionCompanyId"] != "66"
		&& $_SESSION["tmpSessionCompanyId"] != "67" && $_SESSION["tmpSessionCompanyId"] != "68"
		&& $_SESSION["tmpSessionCompanyId"] != "69" && $_SESSION["tmpSessionCompanyId"] != "70"
		&& $_SESSION["tmpSessionCompanyId"] != "71" && $_SESSION["tmpSessionCompanyId"] != "72"
		&& $_SESSION["tmpSessionCompanyId"] != "73" && $_SESSION["tmpSessionCompanyId"] != "74"
		&& $_SESSION["tmpSessionCompanyId"] != "75" && $_SESSION["tmpSessionCompanyId"] != "76"
		&& $_SESSION["tmpSessionCompanyId"] != "74" && $_SESSION["tmpSessionCompanyId"] != "78"				&& $_SESSION["tmpSessionCompanyId"] != "111" && $_SESSION["tmpSessionCompanyId"] != "112"
		){ ?>
		<tr class="alternate">

			<td width="220px"><b>Adult Floride (1204)</b></td>

			<td colspan="6">

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="ageFlouride" type="radio" <? if($ageFlouride == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="ageFlouride" type="radio" <? if($ageFlouride == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>																				

		</tr>

		<tr class="alternate">

			<td width="220px" valign="top" style="padding-left: 25px;"><b>If Yes</b></td>

			<td colspan="6">

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td width="5px"><input name="ageFlourideAge" type="radio" <? if($ageFlourideAge == "1 x Yr"){?>checked<? } ?> value="1 x Yr"/></td>

						<td colspan="7">1 x Yr</td>

					</tr>

					<tr>

						<td width="5px"><input name="ageFlourideAge" type="radio" <? if($ageFlourideAge == "2 x Yr"){?>checked<? } ?> value="2 x Yr"/></td>

						<td width="50px">2 x Yr</td>

						<td width="120px">If twice a year then</td>

						<td width="5px"><input name="ageFlourideTime" type="radio" <? if($ageFlourideTime == "Any time"){?>checked<? } ?> value="Any time"/></td>

						<td width="50px">Any time</td>

						<td width="5px"><input name="ageFlourideTime" type="radio" <? if($ageFlourideTime == "Every 6 months"){?>checked<? } ?> value="Every 6 months"/></td>

						<td>Every 6 months</td>

					</tr>

					<tr>

						<td width="5px"><input name="ageFlourideAge" type="radio" <? if($ageFlourideAge == "Other"){?>checked<? } ?> value="Other"/></td>

						<td>Other</td>

						<td colspan="5">

						<input type="text" class="textbox" id="ageFlourideOther" name="ageFlourideOther" value="<?=$ageFlourideOther?>" style="width: 100px" />

						</td>	

					</tr>

				</table>

			</td>

		</tr>
        <? } ?>

		<tr>

			<td width="220px"><b>Scaling and Root Planing (4341)</b></td>

			<td colspan="6">

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="ageScaling" type="radio" <? if($ageScaling == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="ageScaling" type="radio" <? if($ageScaling == "No"){?>checked<? } ?> value="No"/></td>

						<td width="50px">No</td>

						<td width="35px">Month</td>

						<td width="120px"><input type="text" class="textbox" id="ageScalingMonth" name="ageScalingMonth" value="<?=$ageScalingMonth?>" style="width: 100px" /></td>

						<td width="30px">Year</td>

						<td><input type="text" class="textbox" id="ageScalingYear" name="ageScalingYear" value="<?=$ageScalingYear?>" style="width: 100px" /></td>

					</tr>

				</table>

			</td>																				

		</tr>

		<tr class="alternate">

			<td width="220px"><b>All 4 Quadrants be done on same day?</b></td>

			<td colspan="6" valign="top">

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input name="ageQuadrants" type="radio" <? if($ageQuadrants == "Yes"){?>checked<? } ?> value="Yes"/></td>

						<td width="30px">Yes</td>

						<td width="5px"><input name="ageQuadrants" type="radio" <? if($ageQuadrants == "No"){?>checked<? } ?> value="No"/></td>

						<td width="10px">No</td>

					</tr>

				</table>

			</td>																				

		</tr>

	</table>
    
    <table width="100%" cellpadding="5" cellspacing="0">

    
    <tr>
			<td width="20%"><b>Pocket Depth for SRP</b></td>
			<td colspan="4">
				<input type="text" style="width: 300px" value="<?php echo $pocketDepthSrp ?>" name="pocketDepthSrp" id="pocketDepthSrp" class="textbox">
			</td>
		</tr>
    

  </table>

</div>