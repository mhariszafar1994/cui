<div id="divCustom">

<table cellpadding="3" cellspacing="0" border="0">
<tr>
<td width="130"><b>Network Type</b></td>
<td>
<select id="custNetwork" name="custNetwork" size="1">
    <option value=""></option>
    <option value="In Network" <? if($custNetwork=="In Network"){ ?>Selected<? } ?>>In Network</option>
    <option value="Out Of Network" <? if($custNetwork=="Out Of Network"){ ?>Selected<? } ?>>Out Of Network</option>
    <option value="Policy Terminated" <? if($custNetwork=="Policy Terminated"){ ?>Selected<? } ?>>Policy Terminated</option>
</select></td>
<td></td>
<td>
    <table border="0" cellpadding="2" cellspacing="0">
    <tr>
        <td nowrap="nowrap"><b>Payer&nbsp;ID</b></td>
        <td>&nbsp;</td>
        <td><input type="text" class="textbox" id="custPayorId" name="custPayorId" value="<?=$custPayorId?>" style="width: 100px" /></td>
        <td width="50px">&nbsp;</td>
        <td nowrap="nowrap"><b>E-Attachments</b></td>
        <td width="30">&nbsp;</td>
        <td><input <? if($custAttachments == "Yes"){?>checked<? } ?> type="radio" name="custAttachments" value="Yes" /></td>
        <td>Yes</td>
        <td width="20px">&nbsp;</td>
        <td><input <? if($custAttachments == "No"){?>checked<? } ?> type="radio" name="custAttachments" value="No" /></td>
        <td>No</td>
    </tr>
    </table></td>
</tr>

<tr class="alternate">
<td width="130"><b>Quick Note</b></td>
<td colspan="6"><input type="text" class="textbox" id="custQuickNote" name="custQuickNote" value="<?=$custQuickNote?>" style="width: 610px" maxlength="155" /></td>
<td></td>
</tr>

<tr>
<td width="130"><b>Effective Date</b></td>
<td colspan="2"><input type="text" class="textbox" id="custEffectiveDate" name="custEffectiveDate" value="<?=$custEffectiveDate?>" style="width: 60px" maxlength="10" /><input type="button" value="Select" class="smallButton" onclick="displayDatePicker('custEffectiveDate');" /></td>
<td colspan="5">
<table cellpadding="2" cellspacing="0">
  <tr>
  <td><b>Fee Schedule</b></td>
  <td width="30">&nbsp;</td>
  <td valign="top" width="20px"><input <? if($custFee == "PPO"){?>checked<? } ?> type="radio" name="custFee" value="PPO" /></td>
  <td valign="top">PPO</td>
  <td width="15px">&nbsp;</td>
  <td valign="top" width="20px"><input <? if($custFee == "UCR"){?>checked<? } ?> type="radio" name="custFee" value="UCR" /></td>
  <td valign="top">UCR</td>
  <td valign="top">&nbsp;</td>
  <td valign="top"><input type="text" class="textbox" id="custFeeName" name="custFeeName" value="<?=$custFeeName?>" /></td>
  </tr>
</table></td>
</tr>

<tr class="alternate">
<td width="130"><b>Original Eligibility date</b></td>
<td colspan="2"><input type="text" class="textbox" id="custOriginalEffectiveDate" name="custOriginalEffectiveDate" value="<?=$custOriginalEffectiveDate?>" style="width: 60px" maxlength="10" /><input type="button" value="Select" class="smallButton" onclick="displayDatePicker('custOriginalEffectiveDate');" /></td>
<td colspan="5">&nbsp;</td>
</tr>
<tr>
	<td width="130"><b>ID</b></td>
	<td><input type="text" class="textbox" id="custMemberId" name="custMemberId" value="<?=$custMemberId?>" style="width: 100px" /></td>
	<td width="5"></td>
	<td width="130">
		<table cellpadding="2" cellspacing="0">
		<tr>
			<td nowrap="nowrap"><b>Group Name</b></td>
			<td width="30">&nbsp;</td>
			<td valign="top" width="20px"><input type="text" class="textbox" id="custGroup" name="custGroup" value="<?=$custGroup?>" style="width: 100px" /></td>
			<td width="30">&nbsp;</td>
			<td nowrap="nowrap"><b>Group #</b></td>
			<td width="30">&nbsp;</td>
			<td valign="top" width="20px"><input type="text" class="textbox" id="custGroupNum" name="custGroupNum" value="<?=$custGroupNum?>" style="width: 100px" /></td>
		</tr>
		</table>
	</td>
	<td width="50"></td>
	<td></td>
</tr>
<tr class="alternate">
<td><b>Benefits Run ?</b></td>
<td colspan="7">
<table cellpadding="1" cellspacing="0">
<tr>
<td width="20px"><input <? if($custBenefitsRun == "Calender"){?>checked<? } ?> type="radio" name="custBenefitsRun" value="Calender" /></td>
<td>Calendar</td>
<td width="15px">&nbsp;</td>
<td width="20px"><input <? if($custBenefitsRun == "Contract Year"){?>checked<? } ?> type="radio" name="custBenefitsRun" value="Contract Year" /></td>
<td>Contract Year</td>
<td width="5px">&nbsp;</td>
<td>&nbsp;</td>
<td>
<input type="text" class="textbox" id="custBenefitsDate" name="custBenefitsDate" value="<?=$custBenefitsDate?>" style="width: 60px" maxlength="10" /><input type="button" value="Select" class="smallButton" onclick="displayDatePicker('custBenefitsDate');" /></td>
<td width="20px">&nbsp;</td>
<td><b>Relationship to ins holder</b></td>
<td width="15px">&nbsp;</td>
<td><input type="text" class="textbox" id="custBenefitsRelationship" name="custBenefitsRelationship" value="<?=$custBenefitsRelationship?>" style="width: 180px" /></td>
</tr>
</table></td>
</tr>
<tr>
<td valign="top"><b>MTC</b></td>
<td colspan="6">
<table cellpadding="1" cellspacing="0">
<tr>
<td><input <? if($custMtc == "Yes"){?>checked<? } ?> type="radio" name="custMtc" value="Yes" /></td>
<td>Yes</td>
<td width="20px">&nbsp;</td>
<td><input <? if($custMtc == "No"){?>checked<? } ?> type="radio" name="custMtc" value="No" /></td>
<td>No</td>
<Td width="20px">&nbsp;</Td>
<td><b>Details</b></td>
<td width="15px">&nbsp;</td>
<Td><input type="text" class="textbox" id="custMtcDetail" name="custMtcDetail" value="<?=$custMtcDetail?>" style="width: 180px" /></Td>
<Td width="20px">&nbsp;</Td>
<td><b>Dependent/Student coverage to what age</b></td>
<td width="15px">&nbsp;</td>
<Td><input type="text" class="textbox" id="custMtcDependent" name="custMtcDependent" value="<?=$custMtcDependent?>" style="width: 50px" /></Td>
</tr>
</table>
</td>
<td>&nbsp;</td>
</tr>
<tr class="alternate">
<td nowrap="nowrap"><b>Waiting Period</b></td>
<td colspan="7">
<table cellpadding="1" cellspacing="0">
<tr>
<td><input <? if($custWaitingPeriod == "Yes"){?>checked<? } ?> type="radio" name="custWaitingPeriod" value="Yes" /></td>
<td>Yes</td>
<td width="20px">&nbsp;</td>
<td><input <? if($custWaitingPeriod == "No"){?>checked<? } ?> type="radio" name="custWaitingPeriod" value="No" /></td>
<td>No</td>
<Td width="20px">&nbsp;</Td>
<td><b>Details</b></td>
<td width="15px">&nbsp;</td>
<Td><input type="text" class="textbox" id="custWaitingPeriodDetail" name="custWaitingPeriodDetail" value="<?=$custWaitingPeriodDetail?>" style="width: 180px" /></Td>
</tr>
</table></td>
</tr>
<tr>
  <td colspan="8">
  	<table cellpadding="2" cellspacing="0">
      <tr>
      <td width="132px" style="padding-left:0px;"><b>Family member covered</b></td>
      <td><input type="text" class="textbox" id="custFamilyCovered" name="custFamilyCovered" value="<?=$custFamilyCovered?>" style="width: 200px" /></td>
      </tr>
    </table>
  </td>
</tr>
<tr class="alternate">
  <td colspan="8">
  	<table cellpadding="2" cellspacing="0">
      <tr>
      <td width="132px" style="padding-left:0px;"><b>Primary or secondary Ins</b></td>
      <td><input type="text" class="textbox" id="custPrimaryIns" name="custPrimaryIns" value="<?=$custPrimaryIns?>" style="width: 200px" /></td>
      </tr>
    </table>
  </td>
</tr>
<tr>
  <td colspan="8">
  	<table cellpadding="2" cellspacing="0">
      <tr>
      <td width="132px" style="padding-left:0px;"><b>Yearly Max $</b></td>
      <td><input type="text" class="textbox" id="custYearlyMax" name="custYearlyMax" value="<?=$custYearlyMax?>" style="width: 60px" /></td>
      <td width="10px">&nbsp;</td>
      <td width="80px"><b>Applies to?</b></td>
      <td><input <? if($custYearlyApplies1 == "P"){?>checked<? } ?> type="checkbox" name="custYearlyApplies1" value="P" /></td>
      <td>P</td>
      <td width="2px"></td>
      <td><input <? if($custYearlyApplies2 == "B"){?>checked<? } ?> type="checkbox" name="custYearlyApplies2" value="B" /></td>
      <td>B</td>
      <td width="2px"></td>
      <td><input <? if($custYearlyApplies3 == "M"){?>checked<? } ?> type="checkbox" name="custYearlyApplies3" value="M" /></td>
      <td>M</td>
      <td width="20px">&nbsp;</td>
      <td width="50px"><b>Used $</b></td>
      <td><input type="text" class="textbox" id="custUsed" name="custUsed" value="<?=$custUsed?>" style="width: 60px" /></td>
      <td width="10px">&nbsp;</td>
      <td width="90px"><b>Roll over? $</b></td>
      <td><input type="text" class="textbox" id="custRollOver" name="custRollOver" value="<?=$custRollOver?>" style="width: 60px" /></td>
      </tr>
    </table>  </td>
  </tr>
<tr class="alternate">
  <td colspan="8">
  	<table cellpadding="2" cellspacing="0">
      <tr>
      <td width="132px" style="padding-left:0px;"><b>Ind. Deductible $</b></td>
      <td><input type="text" class="textbox" id="custDeduct" name="custDeduct" value="<?=$custDeduct?>" style="width: 60px" /></td>
      <td width="10px">&nbsp;</td>
      <td width="80px"><b>Applies to?</b></td>
      <td><input <? if($custDeductApplies1 == "P"){?>checked<? } ?> type="checkbox" name="custDeductApplies1" value="P" /></td>
      <td>P</td>
      <td width="2px"></td>
      <td><input <? if($custDeductApplies2 == "D"){?>checked<? } ?> type="checkbox" name="custDeductApplies2" value="D" /></td>
      <td>D</td>
      <td width="2px"></td>
      <td><input <? if($custDeductApplies3 == "B"){?>checked<? } ?> type="checkbox" name="custDeductApplies3" value="B" /></td>
      <td>B</td>
      <td width="2px"></td>
      <td><input <? if($custDeductApplies4 == "M"){?>checked<? } ?> type="checkbox" name="custDeductApplies4" value="M" /></td>
      <td>M</td>
      <td width="20px">&nbsp;</td>
      <td width="40px"><b>Met</b></td>
      <td><input <? if($custMet == "Yes"){?>checked<? } ?> type="radio" name="custMet" value="Yes" /></td>
      <td>Yes</td>
      <td width="5px"></td>
      <td><input <? if($custMet == "No"){?>checked<? } ?> type="radio" name="custMet" value="No" /></td>
      <td>No</td>
      </tr>
    </table>  </td>
</tr>
<tr>
  <td colspan="8">
  	<table cellpadding="2" cellspacing="0">
      <tr>
      <td width="132px" style="padding-left:0px;"><b>Fam. Deductible $</b></td>
      <td><input type="text" class="textbox" id="custFamilyDeduct" name="custFamilyDeduct" value="<?=$custFamilyDeduct?>" style="width: 60px" /></td>
      <td width="10px">&nbsp;</td>
      <td width="40px"><b>Met</b></td>
      <td><input <? if($custMet2 == "Yes"){?>checked<? } ?> type="radio" name="custMet2" value="Yes" /></td>
      <td>Yes</td>
      <td width="5px"></td>
      <td><input <? if($custMet2 == "No"){?>checked<? } ?> type="radio" name="custMet2" value="No" /></td>
      <td>No</td>
      </tr>
    </table>  </td>
</tr>
<tr class="alternate">
  <td colspan="8">
  	<table cellpadding="1" cellspacing="0" border="0">
    <tr>
    <td nowrap="nowrap"><b>Basic</b></td>
    <td width="5px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custBasic" name="custBasic" value="<?=$custBasic?>" style="width: 60px" />%</td>
    <td width="15px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>Major</b></td>
    <td width="5px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custMajor" name="custMajor" value="<?=$custMajor?>" style="width: 60px" />%</td>
    <td width="15px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>Perio</b></td>
    <td width="5px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custPerio" name="custPerio" value="<?=$custPerio?>" style="width: 60px" />%</td>
    <td width="15px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>Endo</b></td>
    <td width="5px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custEndo" name="custEndo" value="<?=$custEndo?>" style="width: 60px" />%</td>
    <td width="15px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>OS</b></td>
    <td width="5px">&nbsp;</td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custOs" name="custOs" value="<?=$custOs?>" style="width: 60px" />%</td>
    
    <td width="15px">&nbsp;</td>
    
    <td nowrap="nowrap"><b>OS to Med</b></td>
    <td width="5px">&nbsp;</td>
    <td><input <? if($custOsToMed == "Yes"){?>checked<? } ?> type="radio" name="custOsToMed" value="Yes" /></td>
    <td>Yes</td>
    <td width="5px"></td>
    <td><input <? if($custOsToMed == "No"){?>checked<? } ?> type="radio" name="custOsToMed" value="No" /></td>
    <td>No</td>
    </tr>
    </table>  </td>
</tr>


</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">Benefit Levels</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="85%">
<tr>
	<td width="18%"><h3 style="color:black;">SERVICES</h3></td>
	<td width="18%"><h3 style="color:black;">BENEFITS</h3></td>
	<td width="64%"><h3 style="color:black;">DEDUCTIBLE APPLIES</h3></td>
</tr>
<tr>
	<td><b>Diagnostics</b></td>
	<td><input type="text" class="textbox" id="custBLDiagnostics" name="custBLDiagnostics" value="<?=$custBLDiagnostics?>" style="width: 60px" />%</td>
	<td>
		<table cellpadding="1" cellspacing="0" border="0">
			<tr>
				<td><input <? if($custBLDiagnosticsBenefit == "Yes"){?>checked<? } ?> type="radio" name="custBLDiagnosticsBenefit" value="Yes" /></td>
				<td>Yes</td>
				<td width="5px"></td>
				<td><input <? if($custBLDiagnosticsBenefit == "No"){?>checked<? } ?> type="radio" name="custBLDiagnosticsBenefit" value="No" /></td>
				<td>No</td>
			</tr>
		</table>
	</td>
</tr>
<tr class="alternate">
	<td><b>Preventative</b></td>
	<td><input type="text" class="textbox" id="custBLPreventative" name="custBLPreventative" value="<?=$custBLPreventative?>" style="width: 60px" />%</td>
	<td>
		<table cellpadding="1" cellspacing="0" border="0">
			<tr>
				<td><input <? if($custBLPreventativeBenefit == "Yes"){?>checked<? } ?> type="radio" name="custBLPreventativeBenefit" value="Yes" /></td>
				<td>Yes</td>
				<td width="5px"></td>
				<td><input <? if($custBLPreventativeBenefit == "No"){?>checked<? } ?> type="radio" name="custBLPreventativeBenefit" value="No" /></td>
				<td>No</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td><b>Sealants</b></td>
	<td><input type="text" class="textbox" id="custBLSealants" name="custBLSealants" value="<?=$custBLSealants?>" style="width: 60px" />%</td>
	<td>
		<table cellpadding="1" cellspacing="0" border="0">
			<tr>
				<td><input <? if($custBLSealantsBenefit == "Yes"){?>checked<? } ?> type="radio" name="custBLSealantsBenefit" value="Yes" /></td>
				<td>Yes</td>
				<td width="5px"></td>
				<td><input <? if($custBLSealantsBenefit == "No"){?>checked<? } ?> type="radio" name="custBLSealantsBenefit" value="No" /></td>
				<td>No</td>
			</tr>
		</table>
	</td>
</tr>
<tr class="alternate">
	<td><b>Periodontics</b></td>
	<td><input type="text" class="textbox" id="custBLPeriodontics" name="custBLPeriodontics" value="<?=$custBLPeriodontics?>" style="width: 60px" />%</td>
	<td>
		<table cellpadding="1" cellspacing="0" border="0">
			<tr>
				<td><input <? if($custBLPeriodonticsBenefit == "Yes"){?>checked<? } ?> type="radio" name="custBLPeriodonticsBenefit" value="Yes" /></td>
				<td>Yes</td>
				<td width="5px"></td>
				<td><input <? if($custBLPeriodonticsBenefit == "No"){?>checked<? } ?> type="radio" name="custBLPeriodonticsBenefit" value="No" /></td>
				<td>No</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td><b>4381 Covered?</b></td>
	<td colspan="2">
		<table cellpadding="1" cellspacing="0" border="0">
			<tr>
				<td><input <? if($custBL4381Covered == "Yes"){?>checked<? } ?> type="radio" name="custBL4381Covered" value="Yes" /></td>
				<td>Yes</td>
				<td width="5px"></td>
				<td><input <? if($custBL4381Covered == "No"){?>checked<? } ?> type="radio" name="custBL4381Covered" value="No" /></td>
				<td>No</td>
				<td width="20"></td>
				<td><b>4910 Covered?</b></td>
				<td width="15px"></td>
				<td><input <? if($custBL4910Covered == "Yes"){?>checked<? } ?> type="radio" name="custBL4910Covered" value="Yes" /></td>
				<td>Yes</td>
				<td width="5px"></td>
				<td><input <? if($custBL4910Covered == "No"){?>checked<? } ?> type="radio" name="custBL4910Covered" value="No" /></td>
				<td>No</td>
				<td width="20"></td>
				<td><b>Freq</b></td>
				<td width="5px"></td>
				<td><input type="text" class="textbox" id="custBL4381Per" name="custBL4381Per" value="<?=$custBL4381Per?>" style="width: 60px" /></td>
			</tr>
		</table>
	</td>
</tr>
<tr class="alternate">
	<td><b>Surgical Perio</b></td>
	<td><input type="text" class="textbox" id="custBLSurgicalPerio" name="custBLSurgicalPerio" value="<?=$custBLSurgicalPerio?>" style="width: 60px" />%</td>
	<td>
		<table cellpadding="1" cellspacing="0" border="0">
			<tr>
				<td><input <? if($custBLSurgicalPerioBenefit == "Yes"){?>checked<? } ?> type="radio" name="custBLSurgicalPerioBenefit" value="Yes" /></td>
				<td>Yes</td>
				<td width="5px"></td>
				<td><input <? if($custBLSurgicalPerioBenefit == "No"){?>checked<? } ?> type="radio" name="custBLSurgicalPerioBenefit" value="No" /></td>
				<td>No</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td><b>Fillings</b></td>
	<td><input type="text" class="textbox" id="custBLFillings" name="custBLFillings" value="<?=$custBLFillings?>" style="width: 60px" />%</td>
	<td>
		<table cellpadding="1" cellspacing="0" border="0">
			<tr>
				<td><input <? if($custBLFillingsBenefit == "Yes"){?>checked<? } ?> type="radio" name="custBLFillingsBenefit" value="Yes" /></td>
				<td>Yes</td>
				<td width="5px"></td>
				<td><input <? if($custBLFillingsBenefit == "No"){?>checked<? } ?> type="radio" name="custBLFillingsBenefit" value="No" /></td>
				<td>No</td>
				<td width="15px"></td>
				<td><b>Alternative Benefit</b></td>
				<td><input <? if($custBLFillingsAltBenefit == "Yes"){?>checked<? } ?> type="radio" name="custBLFillingsAltBenefit" value="Yes" /></td>
				<td>Yes</td>
				<td width="5px"></td>
				<td><input <? if($custBLFillingsAltBenefit == "No"){?>checked<? } ?> type="radio" name="custBLFillingsAltBenefit" value="No" /></td>
				<td>No</td>
			</tr>
		</table>
	</td>
</tr>
<tr class="alternate">
	<td><b>Endodontics</b></td>
	<td><input type="text" class="textbox" id="custBLEndodontics" name="custBLEndodontics" value="<?=$custBLEndodontics?>" style="width: 60px" />%</td>
	<td></td>
</tr>
<tr>
	<td><b>Simple Extractions</b></td>
	<td><input type="text" class="textbox" id="custBLSimpleExtraction" name="custBLSimpleExtraction" value="<?=$custBLSimpleExtraction?>" style="width: 60px" />%</td>
	<td></td>
</tr>
<tr class="alternate">
	<td><b>Surgical Extractions</b></td>
	<td><input type="text" class="textbox" id="custBLSurgicalExtraction" name="custBLSurgicalExtraction" value="<?=$custBLSurgicalExtraction?>" style="width: 60px" />%</td>
	<td></td>
</tr>
</table>
<br />
<table cellpadding="3" cellspacing="0" width="85%">
<tr>
	<td width="18%"><h3 style="color:black;">SERVICES</h3></td>
	<td width="18%"><h3 style="color:black;">BENEFITS</h3></td>
	<td width="64%"></td>
</tr>
<tr>
	<td><b>Oral Surgery</b></td>
	<td><input type="text" class="textbox" id="custBLOralSurgery" name="custBLOralSurgery" value="<?=$custBLOralSurgery?>" style="width: 60px" />%</td>
	<td>
		<table cellpadding="1" cellspacing="0" border="0">
			<td><b>Replacement Freq</b></td>
			<td width="5px"></td>
			<td><input type="text" class="textbox" id="custBLOralSurgeryFreq" name="custBLOralSurgeryFreq" value="<?=$custBLOralSurgeryFreq?>" style="width: 150px" /></td>
		</table>
	</td>
</tr>
<tr class="alternate">
	<td><b>Inlay / Onlay</b></td>
	<td><input type="text" class="textbox" id="custBLInlayOnlay" name="custBLInlayOnlay" value="<?=$custBLInlayOnlay?>" style="width: 60px" />%</td>
	<td>
		<table cellpadding="1" cellspacing="0" border="0">
			<td><b>Replacement Freq</b></td>
			<td width="5px"></td>
			<td><input type="text" class="textbox" id="custBLInlayOnlayFreq" name="custBLInlayOnlayFreq" value="<?=$custBLInlayOnlayFreq?>" style="width: 150px" /></td>
		</table>
	</td>
</tr>
<tr>
	<td><b>Crowns</b></td>
	<td><input type="text" class="textbox" id="custBLCrowns" name="custBLCrowns" value="<?=$custBLCrowns?>" style="width: 60px" />%</td>
	<td>
		<table cellpadding="1" cellspacing="0" border="0">
			<td><b>Replacement Freq</b></td>
			<td width="5px"></td>
			<td><input type="text" class="textbox" id="custBLCrownsFreq" name="custBLCrownsFreq" value="<?=$custBLCrownsFreq?>" style="width: 150px" /></td>
		</table>
	</td>
</tr>
<tr class="alternate">
	<td><b>Repairs / Relines</b></td>
	<td><input type="text" class="textbox" id="custBLRepairsRelines" name="custBLRepairsRelines" value="<?=$custBLRepairsRelines?>" style="width: 60px" />%</td>
	<td>
		<table cellpadding="1" cellspacing="0" border="0">
			<td><b>Replacement Freq</b></td>
			<td width="5px"></td>
			<td><input type="text" class="textbox" id="custBLRepairsRelinesFreq" name="custBLRepairsRelinesFreq" value="<?=$custBLRepairsRelinesFreq?>" style="width: 150px" /></td>
		</table>
	</td>
</tr>
<tr>
	<td><b>Removable Prosthodontics</b></td>
	<td><input type="text" class="textbox" id="custBLRemovableProsthodontics" name="custBLRemovableProsthodontics" value="<?=$custBLRemovableProsthodontics?>" style="width: 60px" />%</td>
	<td>
		<table cellpadding="1" cellspacing="0" border="0">
			<td><b>Replacement Freq</b></td>
			<td width="5px"></td>
			<td><input type="text" class="textbox" id="custBLRemovableProsthodonticsFreq" name="custBLRemovableProsthodonticsFreq" value="<?=$custBLRemovableProsthodonticsFreq?>" style="width: 150px" /></td>
		</table>
	</td>
</tr>
<tr class="alternate">
	<td><b>Fixed Prosthodontics</b></td>
	<td><input type="text" class="textbox" id="custBLFixedProsthodontics" name="custBLFixedProsthodontics" value="<?=$custBLFixedProsthodontics?>" style="width: 60px" />%</td>
	<td>
		<table cellpadding="1" cellspacing="0" border="0">
			<td><b>Replacement Freq</b></td>
			<td width="5px"></td>
			<td><input type="text" class="textbox" id="custBLFixedProsthodonticsFreq" name="custBLFixedProsthodonticsFreq" value="<?=$custBLFixedProsthodonticsFreq?>" style="width: 150px" /></td>
		</table>
	</td>
</tr>
<tr>
	<td><b>Implants</b></td>
	<td><input type="text" class="textbox" id="custBLImplants" name="custBLImplants" value="<?=$custBLImplants?>" style="width: 60px" />%</td>
	<td></td>
</tr>
<tr class="alternate">
	<td><b>Occlusal Guard (9944)<b></td>
	<td><input type="text" class="textbox" id="custBLOcclusalGuard" name="custBLOcclusalGuard" value="<?=$custBLOcclusalGuard?>" style="width: 60px" />%</td>
	<td>
		<table cellpadding="1" cellspacing="0" border="0">
			<td><b>Limitations</b></td>
			<td width="5px"></td>
			<td><input type="text" class="textbox" id="custBLOcclusalGuardLimitations" name="custBLOcclusalGuardLimitations" value="<?=$custBLOcclusalGuardLimitations?>" style="width: 150px" /></td>
		</table>
	</td>
</tr>
<tr>
	<td><b>Orthotics</b></td>
	<td><input type="text" class="textbox" id="custBLOrthotics" name="custBLOrthotics" value="<?=$custBLOrthotics?>" style="width: 60px" />%</td>
	<td>
		<table cellpadding="1" cellspacing="0" border="0">
			<td><b>Limitations</b></td>
			<td width="5px"></td>
			<td><input type="text" class="textbox" id="custBLOrthoticsLimitations" name="custBLOrthoticsLimitations" value="<?=$custBLOrthoticsLimitations?>" style="width: 150px" /></td>
		</table>
	</td>
</tr>
</table>
<br />
<table cellpadding="3" cellspacing="0" width="85%">
<tr>
	<td width="18%"><h3 style="color:black;">SERVICES</h3></td>
	<td width="14%"><h3 style="color:black;">FREQ LIMIT</h3></td>
	<td width="14%"><h3 style="color:black;">AGE LIMIT</h3></td>
	<td width="54%"><h3 style="color:black;">WAITING PERIOD</h3></td>
</tr>
<tr>
	<td><b>Fmx or Pano</b></td>
	<td><input type="text" class="textbox" id="custBLFmxPanoFreq" name="custBLFmxPanoFreq" value="<?=$custBLFmxPanoFreq?>" style="width: 60px" /></td>
	<td><input type="text" class="textbox" id="custBLFmxPanoAge" name="custBLFmxPanoAge" value="<?=$custBLFmxPanoAge?>" style="width: 60px" /></td>
	<td><input type="text" class="textbox" id="custBLFmxPanoWaiting" name="custBLFmxPanoWaiting" value="<?=$custBLFmxPanoWaiting?>" style="width: 200px" /></td>
</tr>
<tr class="alternate">
	<td><b>BW X-ray (2 / 4)</b></td>
	<td><input type="text" class="textbox" id="custBLBWXrayFreq" name="custBLBWXrayFreq" value="<?=$custBLBWXrayFreq?>" style="width: 60px" /></td>
	<td><input type="text" class="textbox" id="custBLBWXrayAge" name="custBLBWXrayAge" value="<?=$custBLBWXrayAge?>" style="width: 60px" /></td>
	<td><input type="text" class="textbox" id="custBLBWXrayWaiting" name="custBLBWXrayWaiting" value="<?=$custBLBWXrayWaiting?>" style="width: 200px" /></td>
</tr>
<tr>
	<td><b>PA's</b></td>
	<td><input type="text" class="textbox" id="custBLPAFreq" name="custBLPAFreq" value="<?=$custBLPAFreq?>" style="width: 60px" /></td>
	<td><input type="text" class="textbox" id="custBLPAAge" name="custBLPAAge" value="<?=$custBLPAAge?>" style="width: 60px" /></td>
	<td><input type="text" class="textbox" id="custBLPAWaiting" name="custBLPAWaiting" value="<?=$custBLPAWaiting?>" style="width: 200px" /></td>
</tr>
<tr class="alternate">
	<td><b>COE/Lmtd/Peiro exam</b></td>
	<td><input type="text" class="textbox" id="custBLCOEFreq" name="custBLCOEFreq" value="<?=$custBLCOEFreq?>" style="width: 60px" /></td>
	<td><input type="text" class="textbox" id="custBLCOEAge" name="custBLCOEAge" value="<?=$custBLCOEAge?>" style="width: 60px" /></td>
	<td><input type="text" class="textbox" id="custBLCOEWaiting" name="custBLCOEWaiting" value="<?=$custBLCOEWaiting?>" style="width: 200px" /></td>
</tr>
<tr>
	<td><b>Prophy</b></td>
	<td><input type="text" class="textbox" id="custBLProphyFreq" name="custBLProphyFreq" value="<?=$custBLProphyFreq?>" style="width: 60px" /></td>
	<td><input type="text" class="textbox" id="custBLProphyAge" name="custBLProphyAge" value="<?=$custBLProphyAge?>" style="width: 60px" /></td>
	<td><input type="text" class="textbox" id="custBLProphyWaiting" name="custBLProphyWaiting" value="<?=$custBLProphyWaiting?>" style="width: 200px" /></td>
</tr>
<tr class="alternate">
	<td><b>Flouride</b></td>
	<td><input type="text" class="textbox" id="custBLFlourideFreq" name="custBLFlourideFreq" value="<?=$custBLFlourideFreq?>" style="width: 60px" /></td>
	<td><input type="text" class="textbox" id="custBLFlourideAge" name="custBLFlourideAge" value="<?=$custBLFlourideAge?>" style="width: 60px" /></td>
	<td><input type="text" class="textbox" id="custBLFlourideWaiting" name="custBLFlourideWaiting" value="<?=$custBLFlourideWaiting?>" style="width: 200px" /></td>
</tr>
<tr>
	<td><b>Sealants</b></td>
	<td><input type="text" class="textbox" id="custBLSealantsFreq" name="custBLSealantsFreq" value="<?=$custBLSealantsFreq?>" style="width: 60px" /></td>
	<td><input type="text" class="textbox" id="custBLSealantsAge" name="custBLSealantsAge" value="<?=$custBLSealantsAge?>" style="width: 60px" /></td>
	<td><input type="text" class="textbox" id="custBLSealantsWaiting" name="custBLSealantsWaiting" value="<?=$custBLSealantsWaiting?>" style="width: 200px" /></td>
</tr>
<tr class="alternate">
	<td><b>ICON (2990)</b></td>
	<td><input type="text" class="textbox" id="custBLICONFreq" name="custBLICONFreq" value="<?=$custBLICONFreq?>" style="width: 60px" /></td>
	<td><input type="text" class="textbox" id="custBLICONAge" name="custBLICONAge" value="<?=$custBLICONAge?>" style="width: 60px" /></td>
	<td><input type="text" class="textbox" id="custBLICONWaiting" name="custBLICONWaiting" value="<?=$custBLICONWaiting?>" style="width: 200px" /></td>
</tr>
<tr>
	<td><b>FMD (4355)</b></td>
	<td><input type="text" class="textbox" id="custBLFMDFreq" name="custBLFMDFreq" value="<?=$custBLFMDFreq?>" style="width: 60px" /></td>
	<td><input type="text" class="textbox" id="custBLFMDAge" name="custBLFMDAge" value="<?=$custBLFMDAge?>" style="width: 60px" /></td>
	<td><input type="text" class="textbox" id="custBLFMDWaiting" name="custBLFMDWaiting" value="<?=$custBLFMDWaiting?>" style="width: 200px" /></td>
</tr>
<tr class="alternate">
	<td><b>Downgrade to Prophy</b></td>
	<td colspan="3">
		<table cellpadding="1" cellspacing="0" border="0">
			<td><input <? if($custBLDowngradeProphy == "Yes"){?>checked<? } ?> type="radio" name="custBLDowngradeProphy" value="Yes" /></td>
			<td>Yes</td>
			<td width="5px"></td>
			<td><input <? if($custBLDowngradeProphy == "No"){?>checked<? } ?> type="radio" name="custBLDowngradeProphy" value="No" /></td>
			<td>No</td>
		</table>
	</td>
</tr>
<tr>
	<td><b>SRP (4341 - 4342)</b></td>
	<td><input type="text" class="textbox" id="custBLSRPFreq" name="custBLSRPFreq" value="<?=$custBLSRPFreq?>" style="width: 60px" /></td>
	<td><input type="text" class="textbox" id="custBLSRPAge" name="custBLSRPAge" value="<?=$custBLSRPAge?>" style="width: 60px" /></td>
	<td><input type="text" class="textbox" id="custBLSRPWaiting" name="custBLSRPWaiting" value="<?=$custBLSRPWaiting?>" style="width: 200px" /></td>
</tr>
<tr class="alternate">
	<td><b>Perio Mant (4910)</b></td>
	<td><input type="text" class="textbox" id="custBLPerioMantFreq" name="custBLPerioMantFreq" value="<?=$custBLPerioMantFreq?>" style="width: 60px" /></td>
	<td><input type="text" class="textbox" id="custBLPerioMantAge" name="custBLPerioMantAge" value="<?=$custBLPerioMantAge?>" style="width: 60px" /></td>
	<td>&nbsp;</td>
</tr>
<tr>
	<td><b>4210 / 4211 / 4212</b></td>
	<td><input type="text" class="textbox" id="custBL4210Freq" name="custBL4210Freq" value="<?=$custBL4210Freq?>" style="width: 60px" /></td>
	<td><input type="text" class="textbox" id="custBL4210Age" name="custBL4210Age" value="<?=$custBL4210Age?>" style="width: 60px" /></td>
	<td>&nbsp;</td>
</tr>
<tr class="alternate">
	<td><b>Arestin (4381)</b></td>
	<td><input type="text" class="textbox" id="custBLArestinFreq" name="custBLArestinFreq" value="<?=$custBLArestinFreq?>" style="width: 60px" /></td>
	<td><input type="text" class="textbox" id="custBLArestinAge" name="custBLArestinAge" value="<?=$custBLArestinAge?>" style="width: 60px" /></td>
	<td>&nbsp;</td>
</tr>
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">Orthodontic</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
  <tr>
    <td colspan="7" nowrap="nowrap">
    	<table cellpadding="1" cellspacing="0">
    	  <tr>
    	    <td><input <? if($custOr == "Subscriber"){?>checked<? } ?> type="radio" name="custOr" value="Subscriber" /></td>
          <td><b>Subscriber</b></td>
          <td width="10px">&nbsp;</td>
          <td><input <? if($custOr == "Spouse"){?>checked<? } ?> type="radio" name="custOr" value="Spouse" /></td>
          <td><b>Spouse</b></td>
          <td width="10px">&nbsp;</td>
          <td><input <? if($custOr == "Dependent"){?>checked<? } ?> type="radio" name="custOr" value="Dependent" /></td>
          <td><b>Dependent</b></td>
          <td width="10px">&nbsp;</td>
          <td><input <? if($custOr == "All Covered"){?>checked<? } ?> type="radio" name="custOr" value="All Covered" /></td>
          <td><b>All Covered</b></td>
          </tr>
  	  </table></td>
    </tr>
  <tr class="alternate">
    <td width="60" nowrap="nowrap"><b>Ortho</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custOrOrtho" name="custOrOrtho" value="<?=$custOrOrtho?>" style="width: 50px" />%</td>
    <td colspan="3">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="50" nowrap="nowrap"><b>To Age</b></td>
            <td nowrap="nowrap"><input type="text" class="textbox" id="custOrAge" name="custOrAge" value="<?=$custOrAge?>" style="width: 75px" /></td>
            <td width="20px">&nbsp;</td>
            <td nowrap="nowrap"><b>Lifetime Max $</b></td>
            <td><input type="text" class="textbox" id="custOrLifetimeMax" name="custOrLifetimeMax" value="<?=$custOrLifetimeMax?>" style="width: 50px" /></td>
            <td width="20px">&nbsp;</td>
            <td nowrap="nowrap"><b>Remaining $</b></td>
            <td><input type="text" class="textbox" id="custOrRemaining" name="custOrRemaining" value="<?=$custOrRemaining?>" style="width: 50px" /></td>
            <td width="20px">&nbsp;</td>
            <td nowrap="nowrap"><b>Deductible $</b></td>
            <td><input type="text" class="textbox" id="custOrDeductible" name="custOrDeductible" value="<?=$custOrDeductible?>" style="width: 50px" /></td>
        </tr>
        </table>    </td>
    <td colspan="2" valign="top" nowrap="nowrap">&nbsp;</td>
  </tr>
  <tr>
    <td width="60" nowrap="nowrap"><b>Initial Payment</b></td>
    <td nowrap="nowrap"><input type="text" class="textbox" id="custOrPayment" name="custOrPayment" value="<?=$custOrPayment?>" style="width: 50px" />%</td>
    <td colspan="3">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td width="40" nowrap="nowrap"><b>Paid:</b></td>
            <td><input <? if($custOrPaidMonthly == "Monthly"){?>checked<? } ?> type="checkbox" name="custOrPaidMonthly" value="Monthly" /></td>
            <td nowrap="nowrap"><b>Monthly</b></td>
            <td width="5px">&nbsp;</td>
            <td><input <? if($custOrPaidQuarterly == "Quarterly"){?>checked<? } ?> type="checkbox" name="custOrPaidQuarterly" value="Quarterly" /></td>
            <td nowrap="nowrap"><b>Quarterly</b></td>
            <td width="5px">&nbsp;</td>
            <td><input <? if($custOrPaidAutomatic == "Automatic"){?>checked<? } ?> type="checkbox" name="custOrPaidAutomatic" value="Automatic" /></td>
            <td nowrap="nowrap"><b>Automatic</b></td>
            <td width="5px">&nbsp;</td>
            <td><input <? if($custOrClaim == "We File Claim"){?>checked<? } ?> type="checkbox" name="custOrClaim" value="We File Claim" /></td>
            <td nowrap="nowrap"><b>We File Claim (Manually)</b></td>
          </tr>
        </table>    </td>
    <td colspan="2" valign="top" nowrap="nowrap">&nbsp;</td>
  </tr>
  <tr class="alternate">
    <td nowrap="nowrap"><b>Work in progress covered?</b></td>
    <td colspan="4" nowrap="nowrap">
    	<table cellpadding="3" cellspacing="0">
        <tr>
            <td><input <? if($custOrProgress == "Yes"){?>checked<? } ?> type="radio" name="custOrProgress" value="Yes" /></td>
            <td>Yes</td>
            <td width="5px">&nbsp;</td>
            <td><input <? if($custOrProgress == "No"){?>checked<? } ?> type="radio" name="custOrProgress" value="No" /></td>
            <td>No</td>
            <td width="200">&nbsp;</td>
          </tr>
        </table>
    </td>
    <td colspan="2" valign="top" nowrap="nowrap">&nbsp;</td>
  </tr>
</table>

<br />
<table width="100%" cellpadding="5" cellspacing="0">
<tr class="titleTr">
	<td><label id="rightLabel">History</label></td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" width="100%">
  <tr>
    <td width="60" nowrap="nowrap"><b>History</b></td>
    <td nowrap="nowrap"><textarea class="textbox" id="custHistory" name="custHistory" rows="2"  style="width: 800px"><?=$custHistory?></textarea></td>
    </tr>
</table>

<br />

<?php
//$date = date_create(date('y-m-d'), timezone_open('Pacific/Nauru'));
$date = putenv("TZ=US/Pacific");
$pacific_time = date("h:i:s");
$todate = date("Y-m-d");

if($custDateTime == "")
{
	$custDateTime = $todate . " ". $pacific_time;
}
?>


  <table width="100%" cellpadding="5" cellspacing="0">
    <tr class="titleTr">
      <td colspan="4"><label id="rightLabel">Special Request</label></td>
    </tr>
  </table>
  <table cellpadding="3" cellspacing="5" width="100%" style="border:1px solid; background-color: #EFEFEF;">
    <tr>
      <td colspan="4"><b>Special Request</b></td>
    </tr>
    <tr>
      <td colspan="4">
      <textarea name="custSpecialRequest" id="custSpecialRequest" cols="100" rows="5"><?=$custSpecialRequest?></textarea>
      <script type="text/javascript">
	  CKEDITOR.replace( 'custSpecialRequest' );
	  </script>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td width="90px" valign="top"><b>Spoke with:</b></td>
      <td width="250px"><input type="text" class="textbox" id="custSpokeWith" name="custSpokeWith" value="<?=$custSpokeWith?>" style="width: 200px" /></td>
      <td width="140px" valign="top"><b>Account Executive:</b></td>
      <td><input type="text" class="textbox" id="custAccountExecutive" name="custAccountExecutive" value="<?=$custAccountExecutive?>" style="width: 200px" /></td>
    </tr>
    <tr>
      <td width="80px" valign="top"><b>Date/Time:</b></td>
      <td colspan="3"><input type="text" class="textbox" id="custDateTime" name="custDateTime" value="<?=date("m/d/Y h:i:s")?>" style="width: 200px" />
        <br />
        <p>Last Date / Time:
          <?=$custDateTime?>
        </p></td>
    </tr>
  </table>
</div>