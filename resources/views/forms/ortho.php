<div id="divOrthodontics" style="display: none">

	<table cellpadding="5" cellspacing="0">

		<tr>

			<td width="100px"><b>Maximum $</b></td>

			<td><input type="text" class="textbox" id="orthoMax" name="orthoMax" value="<?=$orthoMax?>" style="width: 100px" /></td>

			<td width="15px">&nbsp;</td>

			<td width="100px"><b>Max Remaining $</b></td>

			<td><input type="text" class="textbox" id="orthoMaxRemaining" name="orthoMaxRemaining" value="<?=$orthoMaxRemaining?>" style="width: 100px" /></td>

			<td width="15px">&nbsp;</td>

			<td width="150px"><b>Max (if out of network) $</b></td>

			<td><input type="text" class="textbox" id="orthoMaxNetwork" name="orthoMaxNetwork" value="<?=$orthoMaxNetwork?>" style="width: 100px" /></td>

		</tr>

		<tr class="alternate">

			<td><b>Deductible $</b></td>

			<td><input type="text" class="textbox" id="orthoDeductible" name="orthoDeductible" value="<?=$orthoDeductible?>" style="width: 100px" /></td>

			<td>&nbsp;</td>

			<td><b>Percentage %</b></td>

			<td><input type="text" class="textbox" id="orthoPercentage" name="orthoPercentage" value="<?=$orthoPercentage?>" style="width: 100px" /></td>

			<td colspan="3">&nbsp;</td>

		</tr>

		<tr>

			<td colspan="3"><b>Who is eligible under the plan?</b></td>								

			<td colspan="4">

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input <?if($orthoEligibleSelf == "Self"){?>checked<?}?>  type="checkbox" name="orthoEligibleSelf" value="Self" /></td>

						<td>Self</td>											

						<td width="10px">&nbsp;</td>

						<td><input <?if($orthoEligibleSpouse == "Spouse"){?>checked<?}?>  type="checkbox" name="orthoEligibleSpouse" value="Spouse" /></td>

						<td>Spouse</td>

						<td width="10px">&nbsp;</td>													

						<td><input <?if($orthoEligibleDependent == "Dependent"){?>checked<?}?>  type="checkbox" name="orthoEligibleDependent" value="Dependent" /></td>

						<td>Dependent</td>											

						<td width="10px">&nbsp;</td>

						<td><input <?if($orthoEligibleStudent == "Student"){?>checked<?}?>  type="checkbox" name="orthoEligibleStudent" value="Student" /></td>

						<td>Student</td>											

					</tr>

				</table>

			</td>

		</tr>

		<tr class="alternate">

			<td><b>Age Limitations</b></td>

			<td colspan="7">

				<table cellpadding="3" cellspacing="0">

					<tr>

						<td width="150px">Adult Coverage</td>

						<td><input type="radio" <?if($orthoAgeAdult == "Yes"){?>checked<? } ?>  name="orthoAgeAdult" value="Yes" /></td>

						<td>Yes</td>

						<td width="5px">&nbsp;</td>												

						<td><input type="radio" <?if($orthoAgeAdult == "No"){?>checked<? } ?>  name="orthoAgeAdult" value="No" /></td>

						<td>No</td>

					</tr>

					<tr>

						<td>Children - Age</td>

						<td colspan="5"><input type="text" class="textbox" id="orthoAgeChildren" name="orthoAgeChildren" value="<?=$orthoAgeChildren?>" style="width: 100px" /></td>

					</tr>

					<tr>

						<td>Full Time Student Age</td>

						<td colspan="5"><input type="text" class="textbox" id="orthoAgeStudent" name="orthoAgeStudent" value="<?=$orthoAgeStudent?>" style="width: 100px" /></td>

					</tr>

				</table>

			</td>

		</tr>		

		<tr>

			<td><b>Payment Terms</b></td>

			<td colspan="7">

				<table cellpadding="3" cellspacing="0">

					<tr>

						<td><input type="radio" <?if($orthoPaymentBulk == "Bulk"){?>checked<? } ?>  name="orthoPaymentBulk" value="Bulk" /></td>

						<td>Bulk</td>

						<td width="5px">&nbsp;</td>												

						<td><input type="radio" <?if($orthoPaymentBulk == "Initial"){?>checked<? } ?>  name="orthoPaymentBulk" value="Initial" /></td>

						<td>Initial</td>

						<td width="5px">&nbsp;</td>

						<td>&nbsp;</td>

						<td>&nbsp;</td>

						<td>&nbsp;</td>

						<td><b>How Much $</b></td>

						<td><input type="text" class="textbox" id="orthoPaymentBulkAmount" name="orthoPaymentBulkAmount" value="<?=$orthoPaymentBulkAmount?>" style="width: 100px" /></td>

					</tr>

					<tr>

						<td><input type="radio" <?if($orthoPaymentMethod == "Quarterly"){?>checked<? } ?>  name="orthoPaymentMethod" value="Quarterly" /></td>

						<td>Quarterly</td>

						<td width="5px">&nbsp;</td>												

						<td><input type="radio" <?if($orthoPaymentMethod == "Bi-annually"){?>checked<? } ?>  name="orthoPaymentMethod" value="Bi-annually" /></td>

						<td>Bi-annually</td>

						<td width="5px">&nbsp;</td>												

						<td><input type="radio" <?if($orthoPaymentMethod == "Monthly"){?>checked<? } ?>  name="orthoPaymentMethod" value="Monthly" /></td>

						<td>Monthly</td>

						<td width="25px">&nbsp;</td>

						<td><b>Other $</b></td>

						<td><input type="text" class="textbox" id="orthoPaymentOther" name="orthoPaymentOther" value="<?=$orthoPaymentOther?>" style="width: 100px" /></td>

						

					</tr>

				</table>

			</td>

		</tr>

		<tr class="alternate">

			<td colspan="3"><b>Automatic payment or billing necessary</b></td>

			<td colspan="5">

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input type="radio" <?if($orthoPaymentAuto == "Automatic"){?>checked<? } ?>  name="orthoPaymentAuto" value="Automatic" /></td>

						<td>Automatic</td>

						<td width="5px">&nbsp;</td>												

						<td><input type="radio" <?if($orthoPaymentAuto == "Billing Necessary"){?>checked<? } ?>  name="orthoPaymentAuto" value="Billing Necessary" /></td>

						<td>Billing Necessary</td>									

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td colspan="7"><b>Is the patient covered for Adult or Adolescent Orthodontic Coverage?</b></td>

			<td colspan="1">

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input type="radio" <?if($orthoAdolescent == "Yes"){?>checked<? } ?>  name="orthoAdolescent" value="Yes" /></td>

						<td>Yes</td>

						<td width="5px">&nbsp;</td>												

						<td><input type="radio" <?if($orthoAdolescent == "No"){?>checked<? } ?>  name="orthoAdolescent" value="No" /></td>

						<td>No</td>								

					</tr>

				</table>

			</td>

		</tr>

		<tr class="alternate">

			<td colspan="7"><b>Is the orthodontic coverage benefit totally independent from general dental benefit?</b></td>

			<td colspan="1">

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input type="radio" <?if($orthoIndependent == "Yes"){?>checked<? } ?>  name="orthoIndependent" value="Yes" /></td>

						<td>Yes</td>

						<td width="5px">&nbsp;</td>												

						<td><input type="radio" <?if($orthoIndependent == "No"){?>checked<? } ?>  name="orthoIndependent" value="No" /></td>

						<td>No</td>								

					</tr>

				</table>

			</td>

		</tr>

		<tr class="alternate">

			<td colspan="2" style="padding-left: 25px;"><b>If no, then coordinated - how?</b></td>

			<td colspan="6"><input type="text" class="textbox" id="orthoorthoIndependentHow" name="orthoorthoIndependentHow" value="<?=$orthoorthoIndependentHow?>" style="width: 98%" /></td>

		</tr>

		<tr>

			<td colspan="7"><b>Is there a specific waiting period for orthodontic benefits?</b></td>

			<td colspan="1">

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input type="radio" <?if($orthoWaiting == "Yes"){?>checked<? } ?>  name="orthoWaiting" value="Yes" /></td>

						<td>Yes</td>

						<td width="5px">&nbsp;</td>												

						<td><input type="radio" <?if($orthoWaiting == "No"){?>checked<? } ?>  name="orthoWaiting" value="No" /></td>

						<td>No</td>								

					</tr>

				</table>

			</td>

		</tr>

		<tr class="alternate">

			<td colspan="7"><b>Does the plan require treatment by an in-network PPO/HMO plan dentist for orthodontic benefit coverage?</b></td>

			<td colspan="1">

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input type="radio" <?if($orthoDentist == "Yes"){?>checked<? } ?>  name="orthoDentist" value="Yes" /></td>

						<td>Yes</td>

						<td width="5px">&nbsp;</td>												

						<td><input type="radio" <?if($orthoDentist == "No"){?>checked<? } ?>  name="orthoDentist" value="No" /></td>

						<td>No</td>								

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td colspan="7"><b>If the dentist is out of network,what is the reduction in reimbursements, if any?</b></td>

			<td colspan="1">

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input type="radio" <?if($orthoReduction == "Yes"){?>checked<? } ?>  name="orthoReduction" value="Yes" /></td>

						<td>Yes</td>

						<td width="5px">&nbsp;</td>												

						<td><input type="radio" <?if($orthoReduction == "No"){?>checked<? } ?>  name="orthoReduction" value="No" /></td>

						<td>No</td>								

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td colspan="2" style="padding-left: 25px;"><b>If yes, what reduction?</b></td>

			<td colspan="6"><input type="text" class="textbox" id="orthoorthoReductionAmountHow" name="orthoorthoReductionAmountHow" value="<?=$orthoorthoReductionAmountHow?>" style="width: 98%" /></td>

		</tr>

		<tr class="alternate">

			<td colspan="7"><b>If the Invisalign dentist is a member of orthodontic patient's network carrier, does the plan control the maximum fee that the dentist may charge by contract?</b></td>

			<td colspan="1">

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input type="radio" <?if($orthoInvisalign == "Yes"){?>checked<? } ?>  name="orthoInvisalign" value="Yes" /></td>

						<td>Yes</td>

						<td width="5px">&nbsp;</td>												

						<td><input type="radio" <?if($orthoInvisalign == "No"){?>checked<? } ?>  name="orthoInvisalign" value="No" /></td>

						<td>No</td>								

					</tr>

				</table>

			</td>

		</tr>

		<tr class="alternate">

			<td colspan="7" style="padding-left: 25px;"><b>If yes,can an additional "esthetic" option be charged??</b></td>

			<td colspan="1">

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input type="radio" <?if($orthoInvisalignEsthetic == "Yes"){?>checked<? } ?>  name="orthoInvisalignEsthetic" value="Yes" /></td>

						<td>Yes</td>

						<td width="5px">&nbsp;</td>												

						<td><input type="radio" <?if($orthoInvisalignEsthetic == "No"){?>checked<? } ?>  name="orthoInvisalignEsthetic" value="No" /></td>

						<td>No</td>								

					</tr>

				</table>

			</td>

		</tr>	

		<tr>

			<td colspan="7"><b>Do they require additional documentation on the initial claim form?</b></td>

			<td colspan="1">

				<table cellpadding="1" cellspacing="0">

					<tr>

						<td><input type="radio" <?if($orthoDoc == "Yes"){?>checked<? } ?>  name="orthoDoc" value="Yes" /></td>

						<td>Yes</td>

						<td width="5px">&nbsp;</td>												

						<td><input type="radio" <?if($orthoDoc == "No"){?>checked<? } ?>  name="orthoDoc" value="No" /></td>

						<td>No</td>								

					</tr>

				</table>

			</td>

		</tr>

		<tr>

			<td colspan="2" style="padding-left: 25px;"><b>If so, what?</b></td>

			<td colspan="6"><input type="text" class="textbox" id="orthoorthoDocWhat" name="orthoorthoDocWhat" value="<?=$orthoorthoDocWhat?>" style="width: 98%" /></td>

		</tr>		

				

	</table>

</div>