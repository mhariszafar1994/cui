<?
//roles check
//if($moduleAll == 0 and $moduleUsersView == 0){
$_SESSION[$sessionUserID] = 108;
if($_SESSION[$sessionUserID] != 108 && $_SESSION[$sessionUserID] != 156){
	echo "<script>window.location='index.php?do=authorization'</script>";
}

//vars
$success = 0;
$act = "";
$showUserForm = 0;
$msg = "";
$sortBy = "";
$sortOrder = "";
$userType = '';
$queryString = '';
$sqlVars = '';

//if get userType
if($_GET["manageType"]){
	if($_SESSION[$sessionUserID] == 108 || $_SESSION[$sessionUserID] == 156)
	{
		$manageType = sanitize($_GET["manageType"]);
	}else{
		$manageType = "report";
	}
}
	
//vars to apply switch below
switch ($manageType){
	case 'report':
		$headingTitle = 'Status Report';
		$queryString .= '&manageType='.$manageType;
		$sqlVars .= " ";
		break;
	case 'access':
		$headingTitle = 'User Access';
		$queryString .= '&manageType='.$manageType;
		$sqlVars .= " ";
		break;
	case 'timesheet':
		$headingTitle = 'Timesheets';
		$queryString .= '&manageType='.$manageType;
		$sqlVars .= " ";
		break;
	case 'newcheck':
		$headingTitle = 'New/Check Status Report';
		$queryString .= '&manageType='.$manageType;
		$sqlVars .= " ";
		break;
	case 'companyoffice':
		$headingTitle = 'Companies/Offices Report';
		$queryString .= '&manageType='.$manageType;
		$sqlVars .= " ";
		break;
	default:
		$headingTitle = 'Summary Report';
		$sqlVars .= " ";
		break;
}	

//filters
if($_GET["filter"]){
	$filter = sanitize($_GET["filter"]);
	$filterText = sanitize($_GET["filterText"]);
	$queryString .= "&filter=$filter&filterText=$filterText";
}else{
	$filterText = "";
	$filter = "";
}

if($_POST["totalRec"])
{
	for($i=1; $i<=$_POST["totalRec"]; $i++)
	{
		$checkVal = $_POST["chk".$i];
		//delete records
		$sqlDel = "Update cui_users_login_report Set lrStatus='0' where lrId='$checkVal'";
//		mysql_query($sqlDel);
		mysqli_query($con, $sqlDel);
	}
}

?>
<h1 class="h1WithBg"><?=$headingTitle?></h1>
<div id="pageContainer">
	

<?if($msg!=""){?>
	<div class="success"><?=$msg?></div>
<?}?>

<?
$compSql = "SELECT * FROM cui_companies WHERE companyShowOnReport = '1' ORDER BY companyName";
$compResult = mysqli_query($con, $compSql);
while ($compRs = @mysqli_fetch_array($compResult))
{
	$tmpComp .= '<option value="'.$compRs["companyId"].'">'.$compRs["companyName"].'</option>';
}
//
$offSql = "select * from cui_companies_offices order by officeName";
$offResult = mysqli_query($con, $offSql);
while ($offRs = @mysqli_fetch_array($offResult))
{
    $tmpOff .= '<option value="'.$offRs["officeId"].'">'.$offRs["officeName"].' ('.initials(getField("cui_companies","companyId",$offRs["companyId"],"companyName")).')</option>';
}
?>

<script type="text/javascript">
function showCompanies(str, companyId)
{
    if (str=="")
    {
        document.getElementById("filterCompany").innerHTML='<select name="filterCompany" id="filterCompany" tabindex="38"><option value="">--- All Companies ---</option><?=$tmpComp?></select>';
        return;
    }
    if (window.XMLHttpRequest)
    {
        xmlhttp=new XMLHttpRequest();
    }
    else
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("filterCompany").innerHTML=xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","getCompanies.php?userId="+str+"&companyId="+companyId,true);
    xmlhttp.send();
}

function showOffices(str, officeId)
{
    if (str=="")
    {
        document.getElementById("filterOffice").innerHTML='<select name="filterOffice" id="filterOffice" tabindex="38" style="width:150px;"><option value="">--- All Offices ---</option><?=$tmpOff?></select>';
        return;
    }
    if (window.XMLHttpRequest)
    {
        xmlhttp=new XMLHttpRequest();
    }
    else
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("filterOffice").innerHTML=xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","getOffices.php?companyId="+str+"&officeId="+officeId,true);
    xmlhttp.send();
}


$(document).ready( function(){
<? if($_GET["filterUsers"] != ""){ ?>
	showCompanies('<?=$_GET["filterUsers"]?>', '<?=$_GET["filterCompany"]?>');
<? } ?>
<? if($_GET["filterCompany"] != ""){ ?>
	//showOffices('<?=$_GET["filterCompany"]?>', '<?=$_GET["filterOffice"]?>');
<? } ?>
});

</script>

<script type="text/javascript">
var ie = document.all?1:0 

function colorSwap(elInput,elTr)
{
	with(elInput)
	{
		if(checked)
		{
			var temp = "#DDF1FF";
			elTr.style.background = "#FFF7E5";
		}else{ 
			elTr.style.background = "lightgrey";
		}
	} 
} 

function getTR(elE)
{
	if (ie)
	{
		while (elE.tagName!="TR")
		{
			elE=elE.parentElement;
		}
	}else{
		while (elE.tagName!="TR")
		{
			elE=elE.parentNode;
		}
	}
	//alert(elE.id); 
return elE;
}

function checkAll(isOnload){ 
var trk=0; 
for (var i=0;i<frm.elements.length;i++) 
{ 
var e = frm.elements[i]; 
if ((e.name != 'allbox') && (e.type=='checkbox')) 
{ 
if (isOnload != 1) 
{ 
trk++; 
e.checked = frm.allbox.checked; 
colorSwap(e,getTR(e)); 
} 
else 
{ 
e.tabIndex = i; 
colorSwap(e,getTR(e)); 
} 
} 
} 
} 

function checkOne(elInput){ 
colorSwap(elInput,getTR(elInput)); 
var TB=TO=0; 
for (var i=0;i<frm.elements.length;i++) 
{ 
var e = frm.elements[i]; 
if ((e.name != 'allbox') && (e.type=='checkbox')) 
{ 
TB++; 
if (e.checked) 
TO++; 
} 
} 
if (TO==TB) 
frm.allbox.checked=true; 
else 
frm.allbox.checked=false; 
}

<!--
function chk_emp(thisform){

    var tatal_t;
	total_t = thisform.totalRec.value;
	
	s = 0; 
	 			
    for (x=1; x<=total_t; x++) {
	
	s = s+thisform["chk"+x].checked;
	
	}
	
	if (s < 1) {
		alert ("Please select a record before delete.");
	return false;			
	}	
    
	
	if(!confirm("Are you sure you want to delete selected record(s)."))
	{
	return false;
	}
	
return true;
}

//-->
</script>

<?php
function initials($str) {
    $ret = '';
    foreach (explode(' ', $str) as $word)
        $ret .= strtoupper($word[0]);
    return $ret;
}
?>
<style>
		hr {
			border-bottom: 0px;
		}
		table.form-spacing tbody tr td {
			padding-bottom: 9px;
		}
		</style>

<?php if($manageType == "report"){ ?>
	<!--<form method="GET" action="index.php?do=manage&manageType=<?=$manageType?>">-->
		<div class="searchcard">
			<div class="searchformcontainer">
				<form class="form-inline" method="GET" action="index.php?do=manage&manageType=<?=$manageType?>">
				<input type="hidden" name="do" value="<?=$do?>" />
				<div class="col-md-12 clearfix">
					<div class="pull-left">
						<h3 style="color:black;"><?=$headingTitle?> Search</h3>
					</div>
					<div class="pull-right">
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=report'" <? if($manageType=="report"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt" >Status Report</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=access'" <? if($manageType=="access"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">User Access</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=summary'" <? if($manageType=="summary"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">Summary Report</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=newcheck'" <? if($manageType=="newcheck"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">New/Check Status Report</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=timesheet'" <? if($manageType=="timesheet"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">Timesheets</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=companyoffice'" <? if($manageType=="companyoffice"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">Companies/Offices</button>
						</div>
					</div>
				</div>
				
				<div class="col-md-12 clearfix" style="padding:8px 15px;">
					<div class="pull-left">
					<div class="form-group">
						CUI Users
						<select name="filterUsers" onchange="showCompanies(this.value, '0');" class="selectpicker" style="width:130px !important">
							<option value="">All Users</option>
							<?
                                $usersSql = "select * from cui_users where userLevel='34' order by userFname, userLname";
								$usersResult = mysqli_query($con, $usersSql);
								//
								$compSql = "SELECT * FROM cui_companies WHERE companyShowOnReport = '1' ORDER BY companyName";
								$compResult = mysqli_query($con, $compSql);
								//
								$tmpCond = "";
								if($_GET["filterCompany"] != "")
								{
									$tmpCompany = $_GET["filterCompany"];
									$tmpCond = " Where companyId='$tmpCompany' ";
								}
								$offSql = "select * from cui_companies_offices $tmpCond order by officeName";
								$offResult = mysqli_query($con, $offSql);
							?>
							<? while ($usersRs = @mysqli_fetch_array($usersResult)){ ?>
                            <option value="<?=$usersRs["userId"]?>" <? if($_GET["filterUsers"]==$usersRs["userId"]){ ?>selected<? } ?>><?=$usersRs["userFname"]." ".$usersRs["userLname"]?></option>
                            <? } ?>
						</select>
					</div>
					<div class="form-group">
						Companies
						<select name="filterCompany" id="filterCompany" onchange="showOffices(this.value, '0');" class="selectpicker" style="width:130px !important">
                            <option value="">All Companies</option>
                            <? while ($compRs = @mysqli_fetch_array($compResult)){ ?>
                              	<option value="<?=$compRs["companyId"]?>" <? if($_GET["filterCompany"]==$compRs["companyId"]){ ?>selected<? } ?>><?=$compRs["companyName"]?></option>
                            <? } ?>
                        </select>
					</div>
					<div class="form-group">
						Offices
						<select name="filterOffice" id="filterOffice" style="width:150px;" class="selectpicker" style="width:130px !important">
                            <option value="">All Offices</option>
                            <? while ($offRs = @mysqli_fetch_array($offResult)){ ?>
                               	<? if($_GET["filterCompany"] == ""){ ?>
                                   	<option value="<?=$offRs["officeId"]?>" <? if($_GET["filterOffice"]==$offRs["officeId"]){ ?>selected<? } ?>><?=$offRs["officeName"].' ('.initials(getField("cui_companies","companyId",$offRs["companyId"],"companyName")).')'?></option>
                                <? }else{ ?>
                                   	<option value="<?=$offRs["officeId"]?>" <? if($_GET["filterOffice"]==$offRs["officeId"]){ ?>selected<? } ?>><?=$offRs["officeName"]?></option>
                                <? } ?>
                            <? } ?>
                        </select>
					</div>
					<div class="form-group">
						From
						<input type="text" name="filterDate" id="filterDate" class="textbox form-control" style="width: 90px !important;" value="<? if($_GET["filterDate"]!=""){ ?><?=$_GET["filterDate"]?><? } ?>" size="8">
						<input type="button" class="btn clearbt" value="Pick" onclick="displayDatePicker('filterDate');" tabindex="32" />
					</div>
					<div class="form-group">
						To
						<input type="text" name="filterDate2" id="filterDate2" class="textbox form-control" style="width: 90px !important;" value="<? if($_GET["filterDate2"]!=""){ ?><?=$_GET["filterDate2"]?><? } ?>" size="8">
						<input type="button" class="btn clearbt" value="Pick" onclick="displayDatePicker('filterDate2');" tabindex="32" />
					</div>
					</div>
					<div class="pull-right">
					<div class="form-group">
						<input type="hidden" name="manageType" value="<?=$manageType?>" />
						<button type="submit" name="btn_report_status" class="btn searchbt">Search</button>
					</div>
					<div class="form-group">
						<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=report'" class="btn clearbt">Clear</button>
					</div>
					</div>
				</div>
				</form>
			</div>
		</div>
	<br />	
	<?
	if (array_key_exists('btn_report_status', $_GET)) {
	$html='';
    $html.='<table border="1" cellpadding="5" cellspacing="0" width="100%" style="border:1px solid; border-collapse:collapse; font-family:Calibri, sans-serif, Arial, Helvetica; font-size:14px;">
        <tr class="titleTr">
          <td align="center" rowspan="2"><b>S.No.</b></td>
		  <td align="center" rowspan="2"><b>Patient ID</b></td>
		  <td align="center" rowspan="2"><b>Patient Name</b></td>
          <td colspan="8" align="center"><b>Type of Breakdown</b></td>
          <td align="center" rowspan="2"><b>Time spent<br />(in&nbsp;minutes)</b></td>
          <td align="center" rowspan="2" width="20%"><b>Remarks</b></td>
        </tr>
        <tr class="titleTr">
            <td align="center"><b>Full/<br />Custom</b></td>
            <td align="center"><b>Partial</b></td>
            <td align="center"><b>Ortho</b></td>
            <td align="center"><b>Codes</b></td>
            <td align="center"><b>Special<br />Request</b></td>
            <td align="center"><b>Incorrect<br />Info</b></td>
            <td align="center"><b>Create<br />Patient</b></td>
            <td align="center"><b>Remote<br />Access</b></td>
        </tr>';
        
    	$sqlVars = "";
		$sqlVars2 = "";
		
		if($_GET["filterUsers"] != "")
		{
			$filterUsers = $_GET["filterUsers"];
			$sqlVars .= " And srUserId='$filterUsers'";
			$sqlVars2 .= " And srUserId='$filterUsers'";
		}
		if($_GET["filterCompany"] != "")
		{
			$filterCompany = $_GET["filterCompany"];
			$sqlVars .= " And srCompanyId='$filterCompany'";
			$sqlVars2 .= " And srCompanyId='$filterCompany'";
		}
		if($_GET["filterOffice"] != "")
		{
			$filterOffice = $_GET["filterOffice"];
			$sqlVars .= " And srOfficeId='$filterOffice'";
			$sqlVars2 .= " And srOfficeId='$filterOffice'";
		}
		if($_GET["filterDate"] != "")
		{
			$filterDate = date("Y-m-d", strtotime($_GET["filterDate"]));
			$sqlVars .= " And srDateTime >= '$filterDate'";
		}
		if($_GET["filterDate2"] != "")
		{
			$filterDate2 = date("Y-m-d", strtotime($_GET["filterDate2"]));
			$sqlVars .= " And srDateTime <= '$filterDate2 23:59:59'";
		}
        //
        $sqlDate="select srId, DATE(srDateTime) as dateTime from cui_patients_status_report where srId > '0' $sqlVars GROUP BY DATE(srDateTime) ORDER BY srDateTime";
        $sqlDateResult=mysqli_query($con, $sqlDate);
		//$sqlDateResult2=mysqli_query($con, $sqlDate);
		$sqlDateResult2=$sqlDateResult;
		$sqlDateRs_array = @mysqli_fetch_all($sqlDateResult,MYSQLI_ASSOC);
		foreach($sqlDateRs_array as $sqlDateRs)
		//while ($sqlDateRs=@mysqli_fetch_array($sqlDateResult))
		{
			$tmpSrId = $sqlDateRs["srId"];
			$tmpSrDateTime = $sqlDateRs["dateTime"];
			
			$html.='<tr style="height:10px; background-color:#f5f5f5">
						<td colspan="13"><b>'.date("m/d/Y", strtotime($tmpSrDateTime)).'</b></td>
					</tr>';
			
			$sqlPatients="select * from cui_patients_status_report where srDateTime like '$tmpSrDateTime%' $sqlVars2 order by substring_index(TRIM(srPatientName), ' ', -1)";
			$sqlPatientsResult=mysqli_query($con, $sqlPatients);

			$srFullCustomCounter2=0;
			$srPartialCounter2=0;
			$srOrthoCounter2=0;
			$srCodesCounter2=0;
			$srSpecialRequestCounter2=0;
			$srIncorrectInfoCounter2=0;
			$srCreatePatientCounter2=0;
			$srRemotzAccessCounter2=0;
			$srTimeSpentCounter2=0;
			$sqlDateRs_array2 = @mysqli_fetch_all($sqlPatientsResult,MYSQLI_ASSOC);
			foreach($sqlDateRs_array2 as $sqlPatientsRs)
			//while ($sqlPatientsRs=@mysqli_fetch_array($sqlPatientsResult))
			{
				$counter++;
				$sCounter++;
				$srId=$sqlPatientsRs["srId"];
				$srPatientId=$sqlPatientsRs["srPatientId"];
				$srPatientName=$sqlPatientsRs["srPatientName"];
				$srUserId=$sqlPatientsRs["srUserId"];
				$srCompanyId=$sqlPatientsRs["srCompanyId"];
				$srOfficeId=$sqlPatientsRs["srOfficeId"];
				$srFullCustom=$sqlPatientsRs["srFullCustom"];
				$srPartial=$sqlPatientsRs["srPartial"];
				$srOrtho=$sqlPatientsRs["srOrtho"];
				$srCodes=$sqlPatientsRs["srCodes"];
				$srSpecialRequest=$sqlPatientsRs["srSpecialRequest"];
				$srIncorrectInfo=$sqlPatientsRs["srIncorrectInfo"];
				$srCreatePatient=$sqlPatientsRs["srCreatePatient"];
				$srRemotzAccess=$sqlPatientsRs["srRemotzAccess"];
				$srTimeSpent=$sqlPatientsRs["srTimeSpent"];
				$srRemarks=$sqlPatientsRs["srRemarks"];
				$srDateTime=$sqlPatientsRs["srDateTime"];
				
				//counters
				$srFullCustomCounter+=$sqlPatientsRs["srFullCustom"];
				$srPartialCounter+=$sqlPatientsRs["srPartial"];
				$srOrthoCounter+=$sqlPatientsRs["srOrtho"];
				$srCodesCounter+=$sqlPatientsRs["srCodes"];
				$srSpecialRequestCounter+=$sqlPatientsRs["srSpecialRequest"];
				$srIncorrectInfoCounter+=$sqlPatientsRs["srIncorrectInfo"];
				$srCreatePatientCounter+=$sqlPatientsRs["srCreatePatient"];
				$srRemotzAccessCounter+=$sqlPatientsRs["srRemotzAccess"];
				$srTimeSpentCounter+=$sqlPatientsRs["srTimeSpent"];
				//counters2
				$srFullCustomCounter2+=$sqlPatientsRs["srFullCustom"];
				$srPartialCounter2+=$sqlPatientsRs["srPartial"];
				$srOrthoCounter2+=$sqlPatientsRs["srOrtho"];
				$srCodesCounter2+=$sqlPatientsRs["srCodes"];
				$srSpecialRequestCounter2+=$sqlPatientsRs["srSpecialRequest"];
				$srIncorrectInfoCounter2+=$sqlPatientsRs["srIncorrectInfo"];
				$srCreatePatientCounter2+=$sqlPatientsRs["srCreatePatient"];
				$srRemotzAccessCounter2+=$sqlPatientsRs["srRemotzAccess"];
				$srTimeSpentCounter2+=$sqlPatientsRs["srTimeSpent"];
				
				if($counter>1){
					$bgcolor="#FFFFFF";
					$counter=0;
				}elsE{
					$bgcolor="lightgrey";
				}
				
				if($filterUsers == "")
				{
					$srUserName="<br><span style='font-size:8pt;'>(".getField("cui_users","userId",$sqlPatientsRs["srUserId"],"userFname")." ".getField("cui_users","userId",$sqlPatientsRs["srUserId"],"userLname").")</span>";
				}else{
					$srUserName="<br>";
				}
				
				$html.='<tr bgcolor="'.$bgcolor.'">
					<td align="center">'.$sCounter.'</td>
					<td align="left">'.$srPatientId.'</td>';
					if($filterCompany == "")
					{
						$html.='<td align="left">'.$srPatientName.' (<acronym title="'.getField("cui_companies","companyId",$srCompanyId,"companyName").'">'.initials(getField("cui_companies","companyId",$srCompanyId,"companyName")).'</acronym>)'.$srUserName.'</td>';
					}else{
						$html.='<td align="left">'.$srPatientName.$srUserName.'</td>';
					}
					$html.='<td align="center">'.$srFullCustom.'</td>
					<td align="center">'.$srPartial.'</td>
					<td align="center">'.$srOrtho.'</td>
					<td align="center">'.$srCodes.'</td>
					<td align="center">'.$srSpecialRequest.'</td>
					<td align="center">'.$srIncorrectInfo.'</td>
					<td align="center">'.$srCreatePatient.'</td>
					<td align="center">'.$srRemotzAccess.'</td>
					<td align="center">'.$srTimeSpent.'</td>
					<td align="center">'.$srRemarks.'</td>
				</tr>';
			}
			
			$html.='<tr>
						<td colspan="3" align="right">No. Patients&nbsp;</td>
						<td align="center">'.$srFullCustomCounter2.'</td>
						<td align="center">'.$srPartialCounter2.'</td>
						<td align="center">'.$srOrthoCounter2.'</td>
						<td align="center">'.$srCodesCounter2.'</td>
						<td align="center">'.$srSpecialRequestCounter2.'</td>
						<td align="center">'.$srIncorrectInfoCounter2.'</td>
						<td align="center">'.$srCreatePatientCounter2.'</td>
						<td align="center">'.$srRemotzAccessCounter2.'</td>
						<td align="center"><b>'.$srTimeSpentCounter2.'</b></td>
						<td></td>
					</tr>';
        }
		
        //if(@mysqli_num_rows($sqlDateResult) == 0)
        if(count($sqlDateRs_array) == 0)
        {
            $html.='<tr><td align="center" colspan="13"><br>No Record(s)</td></tr>';
        }
        
        $html.='
		<tr>
        	<td colspan="13" style="height:10px; background-color:#f5f5f5"></td>
        </tr>
        <tr>
        	<td colspan="3" align="right"><b>Total No. Patients&nbsp;</b></td>
            <td align="center"><b>'.$srFullCustomCounter.'</b></td>
            <td align="center"><b>'.$srPartialCounter.'</b></td>
            <td align="center"><b>'.$srOrthoCounter.'</b></td>
            <td align="center"><b>'.$srCodesCounter.'</b></td>
            <td align="center"><b>'.$srSpecialRequestCounter.'</b></td>
            <td align="center"><b>'.$srIncorrectInfoCounter.'</b></td>
            <td align="center"><b>'.$srCreatePatientCounter.'</b></td>
            <td align="center"><b>'.$srRemotzAccessCounter.'</b></td>
            <td align="center"><b>'.$srTimeSpentCounter.'</b></td>
            <td></td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="5" cellspacing="0" width="100%" style="border:0px solid; font-family:Calibri, sans-serif, Arial, Helvetica; font-size:14px;">
    	<tr>
            <td width="220" align="right"><b>Total Time Spent:</b></td>
            <td><b>&nbsp;'.round($srTimeSpentCounter/60, 2).' Hrs.</b></td>
        </tr>
    </table>';
	
	
	$html2='<table border="0" cellpadding="5" cellspacing="0" width="100%" style="text-algin:center;">
		<tr>
			<td width="100" align="left"><img src="'.HTTP_SERVER.'images/report_logo.jpg" height="30" /></td>
			<td align="center" width="635"><b>CheckUrInsurance.com - Daily Progress Report&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
		</tr>
	</table><br>';
	
	$tmpUser = "All Users";
	$tmpCompany = "All Companies";
	$tmpOffice = "All Offices";
	$tmpDate = "All";
	$tmpDate2 = "All";
	
	if($filterUsers != "")
	{
		$tmpUser = getField("cui_users","userId",$filterUsers,"userFname")." ".getField("cui_users","userId",$filterUsers,"userLname");
	}
	if($filterCompany != "")
	{
		$tmpCompany = getField("cui_companies","companyId",$filterCompany,"companyName");
	}
	if($_GET["filterOffice"] != "")
	{
		$tmpOffice = getField("cui_companies_offices","officeId",$filterOffice,"officeName");
	}
	if($filterDate)
	{ 
		$tmpDate = date("m/d/Y", strtotime($filterDate));
	}
	if($filterDate2)
	{ 
		$tmpDate2 = date("m/d/Y", strtotime($filterDate2));
	}
	
	$html2.='<table border="0" cellpadding="5" cellspacing="0" width="700" style="text-algin:center;">
		<tr>
			<td width="450"><b>Name: '.$tmpUser.'</b></td>
			<td><b>Dental Company: '.$tmpCompany.'</b></td>
		</tr>
		<tr>
			<td><b>Date: '.$tmpDate.' - '.$tmpDate2.'</b></td>
			<td><b>Dental Office: '.$tmpOffice.'</b></td>
		</tr>
	</table><br>';
	
	
    $html2.='<table border="1" cellpadding="5" cellspacing="4" width="100%" style="border:1px solid #000; border-collapse:collapse; font-size:8pt;">
        <tr style="background: #f5f5f5">
          <td align="center" rowspan="2"><b>S.No.</b></td>
		  <td align="center" rowspan="2"><b>Pat. ID</b></td>
		  <td align="center" width="125" rowspan="2"><b>Patient Name</b></td>
          <td colspan="8" align="center"><b>Type of Breakdown</b></td>
          <td align="center" rowspan="2"><b>Time spent<br />(in&nbsp;minutes)</b></td>
          <td align="center" rowspan="2" width="160"><b>Remarks</b></td>
        </tr>
        <tr style="background: #f5f5f5">
            <td align="center"><b>Full/<br />Custom</b></td>
            <td align="center"><b>Partial</b></td>
            <td align="center"><b>Ortho</b></td>
            <td align="center"><b>Codes</b></td>
            <td align="center"><b>Special<br />Request</b></td>
            <td align="center"><b>Incorrect<br />Info</b></td>
            <td align="center"><b>Create<br />Patient</b></td>
            <td align="center"><b>Remote<br />Access</b></td>
        </tr>';
        
    	//
		$counter=0;
		$sCounter=0;
		$srFullCustomCounter=0;
		$srPartialCounter=0;
		$srOrthoCounter=0;
		$srCodesCounter=0;
		$srSpecialRequestCounter=0;
		$srIncorrectInfoCounter=0;
		$srCreatePatientCounter=0;
		$srRemotzAccessCounter=0;
		$srTimeSpentCounter=0;
		
		foreach ($sqlDateRs_array as $sqlDateRs)
		//while ($sqlDateRs=@mysqli_fetch_array($sqlDateResult2))
		{
			$tmpSrId = $sqlDateRs["srId"];
			$tmpSrDateTime = $sqlDateRs["dateTime"];
			
			$html2.='<tr style="height:10px; background-color:#f5f5f5">
						 <td colspan="13"><b>'.date("m/d/Y", strtotime($tmpSrDateTime)).'</b></td>
					 </tr>';
			
			$sqlPatients="select * from cui_patients_status_report where srDateTime like '$tmpSrDateTime%' $sqlVars2 order by substring_index(TRIM(srPatientName), ' ', -1)";
			$sqlPatientsResult=mysqli_query($con, $sqlPatients);
			$sqlDateRs_array2 = @mysqli_fetch_all($sqlPatientsResult,MYSQLI_ASSOC);
			//
			$srFullCustomCounter2=0;
			$srPartialCounter2=0;
			$srOrthoCounter2=0;
			$srCodesCounter2=0;
			$srSpecialRequestCounter2=0;
			$srIncorrectInfoCounter2=0;
			$srCreatePatientCounter2=0;
			$srRemotzAccessCounter2=0;
			$srTimeSpentCounter2=0;
			
			foreach ($sqlDateRs_array2 as $sqlPatientsRs)
			//while($sqlPatientsRs=@mysqli_fetch_array($sqlPatientsResult))
			{
				$counter++;
				$sCounter++;
				$srId=$sqlPatientsRs["srId"];
				$srPatientId=$sqlPatientsRs["srPatientId"];
				$srPatientName=$sqlPatientsRs["srPatientName"];
				$srUserId=$sqlPatientsRs["srUserId"];
				$srCompanyId=$sqlPatientsRs["srCompanyId"];
				$srFullCustom=$sqlPatientsRs["srFullCustom"];
				$srPartial=$sqlPatientsRs["srPartial"];
				$srOrtho=$sqlPatientsRs["srOrtho"];
				$srCodes=$sqlPatientsRs["srCodes"];
				$srSpecialRequest=$sqlPatientsRs["srSpecialRequest"];
				$srIncorrectInfo=$sqlPatientsRs["srIncorrectInfo"];
				$srCreatePatient=$sqlPatientsRs["srCreatePatient"];
				$srRemotzAccess=$sqlPatientsRs["srRemotzAccess"];
				$srTimeSpent=$sqlPatientsRs["srTimeSpent"];
				$srRemarks=$sqlPatientsRs["srRemarks"];
				$srDateTime=$sqlPatientsRs["srDateTime"];
				
				//counters
				$srFullCustomCounter+=$sqlPatientsRs["srFullCustom"];
				$srPartialCounter+=$sqlPatientsRs["srPartial"];
				$srOrthoCounter+=$sqlPatientsRs["srOrtho"];
				$srCodesCounter+=$sqlPatientsRs["srCodes"];
				$srSpecialRequestCounter+=$sqlPatientsRs["srSpecialRequest"];
				$srIncorrectInfoCounter+=$sqlPatientsRs["srIncorrectInfo"];
				$srCreatePatientCounter+=$sqlPatientsRs["srCreatePatient"];
				$srRemotzAccessCounter+=$sqlPatientsRs["srRemotzAccess"];
				$srTimeSpentCounter+=$sqlPatientsRs["srTimeSpent"];
				//counters2
				$srFullCustomCounter2+=$sqlPatientsRs["srFullCustom"];
				$srPartialCounter2+=$sqlPatientsRs["srPartial"];
				$srOrthoCounter2+=$sqlPatientsRs["srOrtho"];
				$srCodesCounter2+=$sqlPatientsRs["srCodes"];
				$srSpecialRequestCounter2+=$sqlPatientsRs["srSpecialRequest"];
				$srIncorrectInfoCounter2+=$sqlPatientsRs["srIncorrectInfo"];
				$srCreatePatientCounter2+=$sqlPatientsRs["srCreatePatient"];
				$srRemotzAccessCounter2+=$sqlPatientsRs["srRemotzAccess"];
				$srTimeSpentCounter2+=$sqlPatientsRs["srTimeSpent"];
				
				if($counter>1){
					$bgcolor="#FFFFFF";
					$counter=0;
				}elsE{
					$bgcolor="lightgrey";
				}
				
				if($filterUsers == "")
				{
					$srUserName="<br><span style='font-size:8pt;'>(".getField("cui_users","userId",$sqlPatientsRs["srUserId"],"userFname")." ".getField("cui_users","userId",$sqlPatientsRs["srUserId"],"userLname").")</span>";
				}else{
					$srUserName="<br>";
				}
				
				$html2.='<tr bgcolor="'.$bgcolor.'">
					<td align="center">'.$sCounter.'</td>
					<td align="left">'.$srPatientId.'</td>';
					if($filterCompany == "")
					{
						$html2.='<td width="125" align="left">'.$srPatientName.' ('.initials(getField("cui_companies","companyId",$srCompanyId,"companyName")).')'.$srUserName.'</td>';
					}else{
						$html2.='<td width="125" align="left">'.$srPatientName.$srUserName.'</td>';
					}
					$html2.='<td align="center">'.$srFullCustom.'</td>
					<td align="center">'.$srPartial.'</td>
					<td align="center">'.$srOrtho.'</td>
					<td align="center">'.$srCodes.'</td>
					<td align="center">'.$srSpecialRequest.'</td>
					<td align="center">'.$srIncorrectInfo.'</td>
					<td align="center">'.$srCreatePatient.'</td>
					<td align="center">'.$srRemotzAccess.'</td>
					<td align="center">'.$srTimeSpent.'</td>
					<td align="center" width="130">'.$srRemarks.'</td>
				</tr>';
			}
			
			$html2.='<tr>
						<td colspan="3" align="right">No. Patients&nbsp;</td>
						<td align="center">'.$srFullCustomCounter2.'</td>
						<td align="center">'.$srPartialCounter2.'</td>
						<td align="center">'.$srOrthoCounter2.'</td>
						<td align="center">'.$srCodesCounter2.'</td>
						<td align="center">'.$srSpecialRequestCounter2.'</td>
						<td align="center">'.$srIncorrectInfoCounter2.'</td>
						<td align="center">'.$srCreatePatientCounter2.'</td>
						<td align="center">'.$srRemotzAccessCounter2.'</td>
						<td align="center"><b>'.$srTimeSpentCounter2.'</b></td>
						<td></td>
					</tr>';
		}
        
        if(@mysqli_num_rows($sqlDateResult2) == 0)
        {
            $html2.='<tr><td align="center" colspan="13"><br>No Record(s)</td></tr>';
        }
        
        $html2.='
		<tr>
        	<td colspan="13" style="height:10px; background-color:#f5f5f5"></td>
        </tr>
        <tr>
        	<td colspan="3" align="right"><b>Total No. Patients&nbsp;</b></td>
            <td align="center"><b>'.$srFullCustomCounter.'</b></td>
            <td align="center"><b>'.$srPartialCounter.'</b></td>
            <td align="center"><b>'.$srOrthoCounter.'</b></td>
            <td align="center"><b>'.$srCodesCounter.'</b></td>
            <td align="center"><b>'.$srSpecialRequestCounter.'</b></td>
            <td align="center"><b>'.$srIncorrectInfoCounter.'</b></td>
            <td align="center"><b>'.$srCreatePatientCounter.'</b></td>
            <td align="center"><b>'.$srRemotzAccessCounter.'</b></td>
            <td align="center"><b>'.$srTimeSpentCounter.'</b></td>
            <td></td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="5" cellspacing="0" width="100%" style="font-size:12px;">
    	<tr>
            <td width="150" align="right"><b>Total Time Spent:</b></td>
            <td><b>&nbsp;'.round($srTimeSpentCounter/60, 2).' Hrs.</b></td>
        </tr>
    </table>';
	
	//
	if(isset($_GET["filterUsers"]) || isset($_GET["filterCompany"]) || isset($_GET["filterDate"]))
	{
	    //todo: removed old logic to generate the text file (logic created by Saadat)
	    //die('filterCompany');
//		$fp = fopen("files/CUI_Status_Report.txt", "w");
//		fwrite($fp, "$html2\n");
//		fclose($fp);
		echo $html;
        $sql = "UPDATE cui_reports SET cui_status_report='{$html2}' where id=1";
        mysqli_query($con, $sql);

	}
	?>

	<? if(isset($_GET["filterUsers"]) || isset($_GET["filterCompany"]) || isset($_GET["filterDate"])){ ?>
    	<div align="center"><input type="button" class="noprint btn searchbt" value="Generate PDF" title="Generate PDF" onclick="window.location='generate_status_report_pdf.php';" /></div>
    <? } ?>
	
	<? } ?>

<? }else if($manageType == "access"){ ?>

	<!--<form method="GET" action="index.php?do=manage&manageType=<?=$manageType?>">-->
		<div class="searchcard">
			<div class="searchformcontainer">
				<form class="form-inline" method="GET" action="index.php?do=manage&manageType=<?=$manageType?>">
				<input type="hidden" name="do" value="<?=$do?>" />
				<div class="col-md-12 clearfix">
					<div class="pull-left">
						<h3 style="color:black;"><?=$headingTitle?></h3>
					</div>
					<? if($moduleAll == 1 or $moduleUsersAdd == 1){?>
					<div class="pull-right">
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=report'" <? if($manageType=="report"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt" >Status Report</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=access'" <? if($manageType=="access"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">User Access</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=summary'" <? if($manageType=="summary"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">Summary Report</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=newcheck'" <? if($manageType=="newcheck"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">New/Check Status Report</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=timesheet'" <? if($manageType=="timesheet"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">Timesheets</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=companyoffice'" <? if($manageType=="companyoffice"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">Companies/Offices</button>
						</div>
						
					</div>
					<? } ?>
				</div>
				
				<div class="col-md-12 clearfix" style="padding:8px 15px;">
					<div class="pull-left">
					<div class="form-group">
						CUI Users
						<select name="filterUsers" onchange="showCompanies(this.value, '0');" class="selectpicker" style="width:130px !important">
							<option value="">All Users</option>
							<?
                                $usersSql = "select * from cui_users where userLevel='34' order by userFname, userLname";
								$usersResult = mysqli_query($con, $usersSql);
								//
								$compSql = "SELECT * FROM cui_companies WHERE companyShowOnReport = '1' ORDER BY companyName";
								$compResult = mysqli_query($con, $compSql);
								//
								$tmpCond = "";
								if($_GET["filterCompany"] != "")
								{
									$tmpCompany = $_GET["filterCompany"];
									$tmpCond = " Where companyId='$tmpCompany' ";
								}
								$offSql = "select * from cui_companies_offices $tmpCond order by officeName";
								$offResult = mysqli_query($con, $offSql);
							?>
							<? while ($usersRs = @mysqli_fetch_array($usersResult)){ ?>
                            <option value="<?=$usersRs["userId"]?>" <? if($_GET["filterUsers"]==$usersRs["userId"]){ ?>selected<? } ?>><?=$usersRs["userFname"]." ".$usersRs["userLname"]?></option>
                            <? } ?>
						</select>
					</div>
					<div class="form-group">
						IP Address
						<input type="text" name="filterIP" id="filterIP" class="textbox form-control" value="<? if($_GET["filterIP"]!=""){ ?><?=$_GET["filterIP"]?><? } ?>" size="20">
					</div>
					<div class="form-group">
						Date
						<input type="text" name="filterDate" id="filterDate" class="textbox form-control" style="width: 90px !important;" value="<? if($_GET["filterDate"]!=""){ ?><?=$_GET["filterDate"]?><? } ?>" size="12">
                        <input type="button" class="btn clearbt" value="Pick" onclick="displayDatePicker('filterDate');" tabindex="32" />
					</div>
					</div>
					<div class="pull-right">
					<div class="form-group">
						<input type="hidden" name="manageType" value="<?=$manageType?>" />
						<button type="submit" class="btn searchbt">Search</button>
					</div>
					<div class="form-group">
						<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=access'" class="btn clearbt">Clear</button>
					</div>
					</div>
				</div>
				</form>
			</div>
		</div>
	<br />
    
    <? if($_GET["filterUsers"] || $_GET["filterIP"] || $_GET["filterDate"]){ ?>
    <form name="theForm" action="" method="post">
    <script language="javascript">var frm = document.theForm;</script>
    <table class="table table-bordered form-spacing" border="1" cellpadding="5" cellspacing="0" width="100%" style="border:1px solid; border-collapse:collapse; font-family:Calibri, sans-serif, Arial, Helvetica; font-size:14px;">
        <thead>
        <tr>
            <td align="center"><input type="checkbox" name="allbox" id="allbox" value="1" onclick="checkAll(0);" /></td>
            <td align="center"><b>User ID</b></td>
            <td align="center"><b>User Login</b></td>
            <td align="center"><b>User Name</b></td>
            <td align="center"><b>IP Address</b></td>
            <td align="center"><b>Logged-In Date/Time</b></td>
        </tr>
        </thead>
        </tbody>
    
        <?
		$sqlVars = "";
		
		if($_GET["filterUsers"] != "")
		{
			$filterUsers = $_GET["filterUsers"];
			$sqlVars .= " And lrUserId='$filterUsers'";
		}
		if($_GET["filterIP"] != "")
		{
			$filterIP = $_GET["filterIP"];
			$sqlVars .= " And lrIPAddress like '%$filterIP%'";
		}
		if($_GET["filterDate"] != "")
		{
			$filterDate = date("Y-m-d", strtotime($_GET["filterDate"]));
			$sqlVars .= " And lrLoginDateTime like '$filterDate%'";
		}
		
        //
        $sqlUsers="select * from cui_users_login_report where lrStatus='1' $sqlVars order by lrId DESC";
        $sqlUsersResult=mysqli_query($con, $sqlUsers);
        while ($sqlUsersRs=@mysqli_fetch_array($sqlUsersResult)) {
            $counter++;
            $lrId=$sqlUsersRs["lrId"];
            $lrUserId=$sqlUsersRs["lrUserId"];
            $lrUserLogin=$sqlUsersRs["lrUserLogin"];
            $lrUserName=$sqlUsersRs["lrUserName"];
            $lrIPAddress=$sqlUsersRs["lrIPAddress"];
            $lrLoginDateTime=date("m/d/Y H:i:s a", strtotime($sqlUsersRs["lrLoginDateTime"]))." &nbsp;PDT";
            
            if($counter>1){
                $bgcolor="#FFFFFF";
                $counter=0;
            }elsE{
                $bgcolor="lightgrey";
            }
            ?>
            <tr class="alternatebg">
                <td align="center"><input type="checkbox" name="chk<?=$counter?>" id="chk<?=$counter?>" value="<?=$lrId?>" onclick="checkOne(this);" /></td>
                <td align="center"><?=$lrUserId?></td>
                <td align="center"><?=$lrUserLogin?></td>
                <td align="center"><?=$lrUserName?></td>
                <td align="center"><?=$lrIPAddress?></td>
                <td align="center"><?=$lrLoginDateTime?></td>
            </tr>
            
        <?
        }
        
        if(@mysqli_num_rows($sqlUsersResult) == 0)
        {
            echo "<tr><td align='center' colspan='6'><br>No Record(s)</td></tr>";
        }else{
			echo '<tr><td colspan="6"><input type="hidden" name="totalRec" value="'.$counter.'"><input type="submit" name="Submit" value="Delete Selected" class="smallButton" onClick="return chk_emp(this.form);"></td></tr>';
		}
        ?>
		</tbody>
    </table>
    </form>
    <? } ?>
<? }else if($manageType == "companyoffice"){ ?>
	<?php
        $companies = '';
        $compSql = "SELECT * FROM cui_companies WHERE companyShowOnReport = '1' ORDER BY companyName";
        $compResult = mysqli_query($con, $compSql);
        $count_companies = @mysqli_num_rows($compResult);
        $compsn = 0;
        while ($compRs = @mysqli_fetch_array($compResult)) {
            $compsn++;
            $companyId = $compRs['companyId'];
            $companyName = $compRs['companyName'] . " - All Offices";
            $officeSql = "SELECT * FROM cui_companies_offices WHERE companyId = '$companyId' ORDER BY officeName";
            $officeResult = mysqli_query($con, $officeSql);
            $count_offices = @mysqli_num_rows($officeResult);
            $companies .= '{';
            $companies .= "id: " . $compsn . ",";
            $companies .= "title: '" . $companyName . "',";
            $companies .= "value: " . $companyId . "";
            $offsn = 0;
            if (@mysqli_num_rows($officeResult) > 0) {
                $companies .= ',subs: [';
                while ($officeRs = @mysqli_fetch_array($officeResult)) {
                    $officeId = $officeRs['officeId'];
                    $officeName = $officeRs['officeName'];
                    $offsn++;
                    if ($count_offices == $offsn) {
                        $companies .= '{';
                        $companies .= "id: " . $compsn . $offsn . ",";
                        $companies .= "title: '" . $officeName . "',";
                        $companies .= "value: " . $officeId . "";
                        $companies .= "}";
                    } else {
                        $companies .= "{";
                        $companies .= "id: " . $compsn . $offsn . ",";
                        $companies .= "title: '" . $officeName . "',";
                        $companies .= "value: " . $officeId . "";
                        $companies .= "},";
                    }
                }
                $companies .= "]";
            }
            if ($count_companies == $compsn) {
                $companies .= "}";
            } else {
                $companies .= "},";
            }
        }
        ?>

        <!--        <link rel="stylesheet" href="css/combotree.css"/>
        <script src="javascript/icontains.js" type="text/javascript"></script>-->
        <!--        <script src="javascript/comboTreePlugin.js" type="text/javascript"></script>-->
        <div class="searchcard">
            <div class="searchformcontainer">
                <div class="form-inline col-md-12 clearfix">
                    <div class="pull-left">
                        <h3 style="color:black;"><?= $headingTitle ?> Search</h3>
                    </div>
                    <div class="pull-right">
                        <div class="form-group">
                            <button type="button"
                                    onclick="javascript: window.location='index.php?do=manage&manageType=report'"
                                    <? if ($manageType == "report"){ ?>style="background-color:#CCCCCC;" disabled<? } ?>
                                    class="btn searchbt">Status Report
                            </button>
                        </div>
                        <div class="form-group">
                            <button type="button"
                                    onclick="javascript: window.location='index.php?do=manage&manageType=access'"
                                    <? if ($manageType == "access"){ ?>style="background-color:#CCCCCC;" disabled<? } ?>
                                    class="btn searchbt">User Access
                            </button>
                        </div>
                        <div class="form-group">
                            <button type="button"
                                    onclick="javascript: window.location='index.php?do=manage&manageType=summary'"
                                    <? if ($manageType == "summary"){ ?>style="background-color:#CCCCCC;"
                                    disabled<? } ?> class="btn searchbt">Summary Report
                            </button>
                        </div>
                        <div class="form-group">
                            <button type="button"
                                    onclick="javascript: window.location='index.php?do=manage&manageType=newcheck'"
                                    <? if ($manageType == "newcheck"){ ?>style="background-color:#CCCCCC;"
                                    disabled<? } ?> class="btn searchbt">New/Check Status Report
                            </button>
                        </div>
                        <div class="form-group">
                            <button type="button"
                                    onclick="javascript: window.location='index.php?do=manage&manageType=timesheet'"
                                    <? if ($manageType == "timesheet"){ ?>style="background-color:#CCCCCC;"
                                    disabled<? } ?> class="btn searchbt">Timesheets
                            </button>
                        </div>
                        <div class="form-group">
                            <button type="button"
                                    onclick="javascript: window.location='index.php?do=manage&manageType=companyoffice'"
                                    <? if ($manageType == "companyoffice"){ ?>style="background-color:#CCCCCC;"
                                    disabled<? } ?> class="btn searchbt">Companies/Offices
                            </button>
                        </div>
                    </div>
                </div>

                <div class="form-inline col-md-12 clearfix" style="padding:8px 15px;">
                    <div class="pull-left" style="width: 75%;">
                        <!-- <div class="form-group" style="width: 38%;">
                             <input type="text" id="justAnInputBox" name="compoff" placeholder="Select"/>
                         </div>-->
                        <form style="display:inline;" method="POST">
                            <input type="hidden" name="do" value="<?= $do ?>"/>
                            <input type="hidden" id="selectedvalues" name="selectedvalues" placeholder="OfficeId"/>
                            <input type="hidden" id="selectedcompvalues" name="selectedcompvalues"
                                   placeholder="CompanyId"/>
                            <div class="form-group">
                                From
                                <input autocomplete="off" type="text" name="filterDate" id="filterDate" class="textbox form-control"
                                       style="width: 90px !important;"
                                       value="<? if ($_POST["filterDate"] != "") { ?><?= $_POST["filterDate"] ?><? } ?>"
                                       size="8">
                                <input type="button" class="btn clearbt" value="Pick"
                                       onclick="displayDatePicker('filterDate');" tabindex="32"/>
                            </div>
                            <div class="form-group">
                                To
                                <input autocomplete="off" type="text" name="filterDate2" id="filterDate2" class="textbox form-control"
                                       style="width: 90px !important;"
                                       value="<? if ($_POST["filterDate2"] != "") { ?><?= $_POST["filterDate2"] ?><? } ?>"
                                       size="8">
                                <input type="button" class="btn clearbt" value="Pick"
                                       onclick="displayDatePicker('filterDate2');" tabindex="32"/>
                            </div>
                    </div>
                    <div class="pull-right">
                        <div class="form-group">
                            <input type="hidden" name="manageType" value="<?= $manageType ?>"/>
                            <button type="submit" name="btn_report_companyoffice" class="btn searchbt">Get Report &
                                Download
                            </button>
                        </div>
                        <div class="form-group">
                            <button type="button"
                                    onclick="javascript: window.location='index.php?do=manage&manageType=companyoffice'"
                                    class="btn clearbt">Clear
                            </button>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    <?php
    if (array_key_exists('btn_report_companyoffice', $_POST)) {

        if (!empty($_POST["selectedvalues"])) {
            //echo "running";
            $filterOffice2 = $_POST["selectedvalues"];
            $filterCompany2 = $_POST["selectedcompvalues"];
            //  print_r($filterOffice2);
            //  print_r($filterCompany2);
            $single_pdf = array();
            $company_pdf = array();
            if (!empty($filterCompany2)) {
                foreach ($filterCompany2 as $company) {
                    foreach ($filterOffice2 as $key => $office) {
                        if ($key == $company) {
                            $company_pdf[$key] = $office;
                            unset($filterOffice2[$key]);
                        }
                    }
                }
            }
        }

        function single_reports($filterOffice2)
        {
            global $con;

            $compID = array();
            foreach ($filterOffice2 as $key => $offices) {

                foreach ($offices as $office_id){
                    $compID[] = $key;
                    $id_array[] = $office_id;
                }
                //$ids = implode("','", $id_array);
                //  var_dump($filterOffice1);


            }
//print_r($ids);
//die();

//$sqlVars .= " AND srOfficeId IN ('$filterOffice')";
            //echo "<pre>";print_r($id_array);echo "</pre>";exit();
            $i = 0;
            foreach ($id_array as $id) {
				$counter_sn=0;
				$sCounter=0;
				$srFullCustomCounter=0;
				$srPartialCounter=0;
				$srOrthoCounter=0;
				$srCodesCounter=0;
				$srSpecialRequestCounter=0;
				$srIncorrectInfoCounter=0;
				$srCeatePatientCounter=0;
				$srRemotzAccessCounter=0;
				$srTimeSpentCounter=0;
                $html = '';
				//$html .= '<h2 align="center">'.getField("cui_companies_offices co INNER JOIN cui_companies c ON co.companyId = c.companyId", "co.officeId", $id, "c.companyName").'</h2>';
				$html .= '<h2 align="center">'.getField("cui_companies", "companyId", $compID[$i], "companyName").'</h2>';
				$html .= '<h3 align="center" style="color:black;">'.getField("cui_companies_offices", "officeId", $id, "officeName").'</h3><br>';
                $html .= '<table border="1" cellpadding="5" cellspacing="0" width="100%" style="border:1px solid; border-collapse:collapse; font-family:Calibri, sans-serif, Arial, Helvetica; font-size:14px;">
        <tr class="titleTr">
          <td align="center" rowspan="2"><b>S.No.</b></td>
		  <td align="center" rowspan="2"><b>Patient ID</b></td>
		  <td align="center" rowspan="2"><b>Patient Name</b></td>
          <td colspan="8" align="center"><b>Type of Breakdown</b></td>
          <td align="center" rowspan="2"><b>Time spent<br />(in&nbsp;minutes)</b></td>
          <td align="center" rowspan="2" width="20%"><b>Remarks</b></td>
        </tr>
        <tr class="titleTr">
            <td align="center"><b>Full/<br />Custom</b></td>
            <td align="center"><b>Partial</b></td>
            <td align="center"><b>Ortho</b></td>
            <td align="center"><b>Codes</b></td>
            <td align="center"><b>Special<br />Request</b></td>
            <td align="center"><b>Incorrect<br />Info</b></td>
            <td align="center"><b>Create<br />Patient</b></td>
            <td align="center"><b>Remote<br />Access</b></td>
        </tr>';

                $sqlVars = "";
                $sqlVars2 = "";
                //echo $id;

                $filterCompany = $compID[$i];
                $sqlVars .= " And srCompanyId='$filterCompany'";
                $sqlVars2 .= " And srCompanyId='$filterCompany'";
                $sqlVars .= " And srOfficeId IN('$id')";
                $sqlVars2 .= " And srOfficeId IN('$id')";

                if ($_POST["filterDate"] != "") {
                    $filterDate = date("Y-m-d", strtotime($_POST["filterDate"]));
                    $sqlVars .= " And srDateTime >= '$filterDate'";
                }
                if ($_POST["filterDate2"] != "") {
                    $filterDate2 = date("Y-m-d", strtotime($_POST["filterDate2"]));
                    $sqlVars .= " And srDateTime <= '$filterDate2 23:59:59'";
                }
//
                $sqlDate = "select srId, DATE(srDateTime) as dateTime from cui_patients_status_report where srId > '0' $sqlVars GROUP BY DATE(srDateTime) ORDER BY srDateTime";
                $sqlDateResult = mysqli_query($con, $sqlDate);
//$sqlDateResult2=mysqli_query($con, $sqlDate);
                $sqlDateResult2 = $sqlDateResult;
                $sqlDateRs_array = @mysqli_fetch_all($sqlDateResult, MYSQLI_ASSOC);

                foreach ($sqlDateRs_array as $sqlDateRs) {
                    $tmpSrId = $sqlDateRs["srId"];
                    $tmpSrDateTime = $sqlDateRs["dateTime"];

                    $html .= '<tr style="height:10px; background-color:#f5f5f5">
						<td colspan="13"><b>' . date("m/d/Y", strtotime($tmpSrDateTime)) . '</b></td>
					</tr>';

                    $sqlPatients = "select * from cui_patients_status_report where srDateTime like '$tmpSrDateTime%' $sqlVars2 order by substring_index(TRIM(srPatientName), ' ', -1)";
                    $sqlPatientsResult = mysqli_query($con, $sqlPatients);

                    $srFullCustomCounter2 = 0;
                    $srPartialCounter2 = 0;
                    $srOrthoCounter2 = 0;
                    $srCodesCounter2 = 0;
                    $srSpecialRequestCounter2 = 0;
                    $srIncorrectInfoCounter2 = 0;
                    $srCreatePatientCounter2 = 0;
                    $srRemotzAccessCounter2 = 0;
                    $srTimeSpentCounter2 = 0;
                    $sqlDateRs_array2 = @mysqli_fetch_all($sqlPatientsResult, MYSQLI_ASSOC);
                    foreach ($sqlDateRs_array2 as $sqlPatientsRs) //while ($sqlPatientsRs=@mysqli_fetch_array($sqlPatientsResult))
                    {
                        $counter++;
                        $sCounter++;
                        $srId = $sqlPatientsRs["srId"];
                        $srPatientId = $sqlPatientsRs["srPatientId"];
                        $srPatientName = $sqlPatientsRs["srPatientName"];
                        $srUserId = $sqlPatientsRs["srUserId"];
                        $srCompanyId = $sqlPatientsRs["srCompanyId"];
                        $srOfficeId = $sqlPatientsRs["srOfficeId"];
                        $srFullCustom = $sqlPatientsRs["srFullCustom"];
                        $srPartial = $sqlPatientsRs["srPartial"];
                        $srOrtho = $sqlPatientsRs["srOrtho"];
                        $srCodes = $sqlPatientsRs["srCodes"];
                        $srSpecialRequest = $sqlPatientsRs["srSpecialRequest"];
                        $srIncorrectInfo = $sqlPatientsRs["srIncorrectInfo"];
                        $srCreatePatient = $sqlPatientsRs["srCreatePatient"];
                        $srRemotzAccess = $sqlPatientsRs["srRemotzAccess"];
                        $srTimeSpent = $sqlPatientsRs["srTimeSpent"];
                        $srRemarks = $sqlPatientsRs["srRemarks"];
                        $srDateTime = $sqlPatientsRs["srDateTime"];

                        //counters
                        $srFullCustomCounter += $sqlPatientsRs["srFullCustom"];
                        $srPartialCounter += $sqlPatientsRs["srPartial"];
                        $srOrthoCounter += $sqlPatientsRs["srOrtho"];
                        $srCodesCounter += $sqlPatientsRs["srCodes"];
                        $srSpecialRequestCounter += $sqlPatientsRs["srSpecialRequest"];
                        $srIncorrectInfoCounter += $sqlPatientsRs["srIncorrectInfo"];
                        $srCreatePatientCounter += $sqlPatientsRs["srCreatePatient"];
                        $srRemotzAccessCounter += $sqlPatientsRs["srRemotzAccess"];
                        $srTimeSpentCounter += $sqlPatientsRs["srTimeSpent"];
                        //counters2
                        $srFullCustomCounter2 += $sqlPatientsRs["srFullCustom"];
                        $srPartialCounter2 += $sqlPatientsRs["srPartial"];
                        $srOrthoCounter2 += $sqlPatientsRs["srOrtho"];
                        $srCodesCounter2 += $sqlPatientsRs["srCodes"];
                        $srSpecialRequestCounter2 += $sqlPatientsRs["srSpecialRequest"];
                        $srIncorrectInfoCounter2 += $sqlPatientsRs["srIncorrectInfo"];
                        $srCreatePatientCounter2 += $sqlPatientsRs["srCreatePatient"];
                        $srRemotzAccessCounter2 += $sqlPatientsRs["srRemotzAccess"];
                        $srTimeSpentCounter2 += $sqlPatientsRs["srTimeSpent"];

                        if ($counter > 1) {
                            $bgcolor = "#FFFFFF";
                            $counter = 0;
                        } elsE {
                            $bgcolor = "lightgrey";
                        }

                        if ($filterUsers == "") {
                            $srUserName = "<br><span style='font-size:8pt;'>(" . getField("cui_users", "userId", $sqlPatientsRs["srUserId"], "userFname") . " " . getField("cui_users", "userId", $sqlPatientsRs["srUserId"], "userLname") . ")</span>";
                        } else {
                            $srUserName = "<br>";
                        }

                        $html .= '<tr bgcolor="' . $bgcolor . '">
					<td align="center">' . $sCounter . '</td>
					<td align="left">' . $srPatientId . '</td>';
                        if ($filterCompany == "") {
                            $html .= '<td align="left">' . $srPatientName . ' (<acronym title="' . getField("cui_companies", "companyId", $srCompanyId, "companyName") . '">' . initials(getField("cui_companies", "companyId", $srCompanyId, "companyName")) . '</acronym>)' . $srUserName . '</td>';
                        } else {
                            $html .= '<td align="left">' . $srPatientName . $srUserName . '</td>';
                        }
                        $html .= '<td align="center">' . $srFullCustom . '</td>
					<td align="center">' . $srPartial . '</td>
					<td align="center">' . $srOrtho . '</td>
					<td align="center">' . $srCodes . '</td>
					<td align="center">' . $srSpecialRequest . '</td>
					<td align="center">' . $srIncorrectInfo . '</td>
					<td align="center">' . $srCreatePatient . '</td>
					<td align="center">' . $srRemotzAccess . '</td>
					<td align="center">' . $srTimeSpent . '</td>
					<td align="center">' . $srRemarks . '</td>
				</tr>';
                    }

                    $html .= '<tr>
						<td colspan="3" align="right">No. Patients&nbsp;</td>
						<td align="center">' . $srFullCustomCounter2 . '</td>
						<td align="center">' . $srPartialCounter2 . '</td>
						<td align="center">' . $srOrthoCounter2 . '</td>
						<td align="center">' . $srCodesCounter2 . '</td>
						<td align="center">' . $srSpecialRequestCounter2 . '</td>
						<td align="center">' . $srIncorrectInfoCounter2 . '</td>
						<td align="center">' . $srCreatePatientCounter2 . '</td>
						<td align="center">' . $srRemotzAccessCounter2 . '</td>
						<td align="center"><b>' . $srTimeSpentCounter2 . '</b></td>
						<td></td>
					</tr>';
                }

//if(@mysqli_num_rows($sqlDateResult) == 0)
                if (count($sqlDateRs_array) == 0) {
                    $html .= '<tr><td align="center" colspan="13"><br>No Record(s)</td></tr>';
                }

                $html .= '
		<tr>
        	<td colspan="13" style="height:10px; background-color:#f5f5f5"></td>
        </tr>
        <tr>
        	<td colspan="3" align="right"><b>Total No. Patients&nbsp;</b></td>
            <td align="center"><b>' . $srFullCustomCounter . '</b></td>
            <td align="center"><b>' . $srPartialCounter . '</b></td>
            <td align="center"><b>' . $srOrthoCounter . '</b></td>
            <td align="center"><b>' . $srCodesCounter . '</b></td>
            <td align="center"><b>' . $srSpecialRequestCounter . '</b></td>
            <td align="center"><b>' . $srIncorrectInfoCounter . '</b></td>
            <td align="center"><b>' . $srCreatePatientCounter . '</b></td>
            <td align="center"><b>' . $srRemotzAccessCounter . '</b></td>
            <td align="center"><b>' . $srTimeSpentCounter . '</b></td>
            <td></td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="5" cellspacing="0" width="100%" style="border:0px solid; font-family:Calibri, sans-serif, Arial, Helvetica; font-size:14px;">
    	<tr>
            <td width="220" align="right"><b>Total Time Spent:</b></td>
            <td><b>&nbsp;' . round($srTimeSpentCounter / 60, 2) . ' Hrs.</b></td>
        </tr>
    </table>';


                $html2 = '<table border="0" cellpadding="5" cellspacing="0" width="100%" style="text-algin:center;">
		<tr>
			<td width="100" align="left"><img src="' . HTTP_SERVER . 'images/report_logo.jpg" height="30" /></td>
			<td align="center" width="635"><b>CheckUrInsurance.com - Daily Progress Report&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
		</tr>
	</table><br>';

                $tmpUser = "All Users";
                $tmpCompany = "All Companies";
                $tmpOffice = "All Offices";
                $tmpDate = "All";
                $tmpDate2 = "All";

                if ($filterUsers != "") {
                    $tmpUser = getField("cui_users", "userId", $filterUsers, "userFname") . " " . getField("cui_users", "userId", $filterUsers, "userLname");
                }

                $tmpCompany = getField("cui_companies", "companyId", $filterCompany, "companyName");
                $id . $tmpOffice = getField("cui_companies_offices", "officeId", $id, "officeName");


                if ($filterDate) {
                    $tmpDate = date("m/d/Y", strtotime($filterDate));
                }
                if ($filterDate2) {
                    $tmpDate2 = date("m/d/Y", strtotime($filterDate2));
                }

                $html2 .= '<table border="0" cellpadding="5" cellspacing="0" width="700" style="text-algin:center;">
		<tr>
			<td width="450"><b>Name: ' . $tmpUser . '</b></td>
			<td><b>Dental Company: ' . $tmpCompany . '</b></td>
		</tr>
		<tr>
			<td><b>Date: ' . $tmpDate . ' - ' . $tmpDate2 . '</b></td>
			<td><b>Dental Office: ' . $tmpOffice . '</b></td>
		</tr>
	</table><br>';
                $html2 .= '<table border="1" cellpadding="5" cellspacing="4" width="100%" style="border:1px solid #000; border-collapse:collapse; font-size:8pt;">
        <tr style="background: #f5f5f5">
          <td align="center" rowspan="2"><b>S.No.</b></td>
		  <td align="center" rowspan="2"><b>Pat. ID</b></td>
		  <td align="center" width="125" rowspan="2"><b>Patient Name</b></td>
          <td colspan="8" align="center"><b>Type of Breakdown</b></td>
          <td align="center" rowspan="2"><b>Time spent<br />(in&nbsp;minutes)</b></td>
          <td align="center" rowspan="2" width="160"><b>Remarks</b></td>
        </tr>
        <tr style="background: #f5f5f5">
            <td align="center"><b>Full/<br />Custom</b></td>
            <td align="center"><b>Partial</b></td>
            <td align="center"><b>Ortho</b></td>
            <td align="center"><b>Codes</b></td>
            <td align="center"><b>Special<br />Request</b></td>
            <td align="center"><b>Incorrect<br />Info</b></td>
            <td align="center"><b>Create<br />Patient</b></td>
            <td align="center"><b>Remote<br />Access</b></td>
        </tr>';

                $counter = 0;
                $sCounter = 0;
                $srFullCustomCounter = 0;
                $srPartialCounter = 0;
                $srOrthoCounter = 0;
                $srCodesCounter = 0;
                $srSpecialRequestCounter = 0;
                $srIncorrectInfoCounter = 0;
                $srCreatePatientCounter = 0;
                $srRemotzAccessCounter = 0;
                $srTimeSpentCounter = 0;

                foreach ($sqlDateRs_array as $sqlDateRs) {
                    $tmpSrId = $sqlDateRs["srId"];
                    $tmpSrDateTime = $sqlDateRs["dateTime"];

                    $html2 .= '<tr style="height:10px; background-color:#f5f5f5">
						 <td colspan="13"><b>' . date("m/d/Y", strtotime($tmpSrDateTime)) . '</b></td>
					 </tr>';

                    $sqlPatients = "select * from cui_patients_status_report where srDateTime like '$tmpSrDateTime%' $sqlVars2 order by substring_index(TRIM(srPatientName), ' ', -1)";
                    $sqlPatientsResult = mysqli_query($con, $sqlPatients);
                    $sqlDateRs_array2 = @mysqli_fetch_all($sqlPatientsResult, MYSQLI_ASSOC);
                    //
                    $srFullCustomCounter2 = 0;
                    $srPartialCounter2 = 0;
                    $srOrthoCounter2 = 0;
                    $srCodesCounter2 = 0;
                    $srSpecialRequestCounter2 = 0;
                    $srIncorrectInfoCounter2 = 0;
                    $srCreatePatientCounter2 = 0;
                    $srRemotzAccessCounter2 = 0;
                    $srTimeSpentCounter2 = 0;

                    foreach ($sqlDateRs_array2 as $sqlPatientsRs) //while($sqlPatientsRs=@mysqli_fetch_array($sqlPatientsResult))
                    {
                        $counter++;
                        $sCounter++;
                        $srId = $sqlPatientsRs["srId"];
                        $srPatientId = $sqlPatientsRs["srPatientId"];
                        $srPatientName = $sqlPatientsRs["srPatientName"];
                        $srUserId = $sqlPatientsRs["srUserId"];
                        $srCompanyId = $sqlPatientsRs["srCompanyId"];
                        $srFullCustom = $sqlPatientsRs["srFullCustom"];
                        $srPartial = $sqlPatientsRs["srPartial"];
                        $srOrtho = $sqlPatientsRs["srOrtho"];
                        $srCodes = $sqlPatientsRs["srCodes"];
                        $srSpecialRequest = $sqlPatientsRs["srSpecialRequest"];
                        $srIncorrectInfo = $sqlPatientsRs["srIncorrectInfo"];
                        $srCreatePatient = $sqlPatientsRs["srCreatePatient"];
                        $srRemotzAccess = $sqlPatientsRs["srRemotzAccess"];
                        $srTimeSpent = $sqlPatientsRs["srTimeSpent"];
                        $srRemarks = $sqlPatientsRs["srRemarks"];
                        $srDateTime = $sqlPatientsRs["srDateTime"];

                        //counters
                        $srFullCustomCounter += $sqlPatientsRs["srFullCustom"];
                        $srPartialCounter += $sqlPatientsRs["srPartial"];
                        $srOrthoCounter += $sqlPatientsRs["srOrtho"];
                        $srCodesCounter += $sqlPatientsRs["srCodes"];
                        $srSpecialRequestCounter += $sqlPatientsRs["srSpecialRequest"];
                        $srIncorrectInfoCounter += $sqlPatientsRs["srIncorrectInfo"];
                        $srCreatePatientCounter += $sqlPatientsRs["srCreatePatient"];
                        $srRemotzAccessCounter += $sqlPatientsRs["srRemotzAccess"];
                        $srTimeSpentCounter += $sqlPatientsRs["srTimeSpent"];
                        //counters2
                        $srFullCustomCounter2 += $sqlPatientsRs["srFullCustom"];
                        $srPartialCounter2 += $sqlPatientsRs["srPartial"];
                        $srOrthoCounter2 += $sqlPatientsRs["srOrtho"];
                        $srCodesCounter2 += $sqlPatientsRs["srCodes"];
                        $srSpecialRequestCounter2 += $sqlPatientsRs["srSpecialRequest"];
                        $srIncorrectInfoCounter2 += $sqlPatientsRs["srIncorrectInfo"];
                        $srCreatePatientCounter2 += $sqlPatientsRs["srCreatePatient"];
                        $srRemotzAccessCounter2 += $sqlPatientsRs["srRemotzAccess"];
                        $srTimeSpentCounter2 += $sqlPatientsRs["srTimeSpent"];

                        if ($counter > 1) {
                            $bgcolor = "#FFFFFF";
                            $counter = 0;
                        } elsE {
                            $bgcolor = "lightgrey";
                        }

                        if ($filterUsers == "") {
                            $srUserName = "<br><span style='font-size:8pt;'>(" . getField("cui_users", "userId", $sqlPatientsRs["srUserId"], "userFname") . " " . getField("cui_users", "userId", $sqlPatientsRs["srUserId"], "userLname") . ")</span>";
                        } else {
                            $srUserName = "<br>";
                        }

                        $html2 .= '<tr bgcolor="' . $bgcolor . '">
					<td align="center">' . $sCounter . '</td>
					<td align="left">' . $srPatientId . '</td>';
                        if ($filterCompany == "") {
                            $html2 .= '<td width="125" align="left">' . $srPatientName . ' (' . initials(getField("cui_companies", "companyId", $srCompanyId, "companyName")) . ')' . $srUserName . '</td>';
                        } else {
                            $html2 .= '<td width="125" align="left">' . $srPatientName . $srUserName . '</td>';
                        }
                        $html2 .= '<td align="center">' . $srFullCustom . '</td>
					<td align="center">' . $srPartial . '</td>
					<td align="center">' . $srOrtho . '</td>
					<td align="center">' . $srCodes . '</td>
					<td align="center">' . $srSpecialRequest . '</td>
					<td align="center">' . $srIncorrectInfo . '</td>
					<td align="center">' . $srCreatePatient . '</td>
					<td align="center">' . $srRemotzAccess . '</td>
					<td align="center">' . $srTimeSpent . '</td>
					<td align="center" width="130">' . $srRemarks . '</td>
				</tr>';
                    }

                    $html2 .= '<tr>
						<td colspan="3" align="right">No. Patients&nbsp;</td>
						<td align="center">' . $srFullCustomCounter2 . '</td>
						<td align="center">' . $srPartialCounter2 . '</td>
						<td align="center">' . $srOrthoCounter2 . '</td>
						<td align="center">' . $srCodesCounter2 . '</td>
						<td align="center">' . $srSpecialRequestCounter2 . '</td>
						<td align="center">' . $srIncorrectInfoCounter2 . '</td>
						<td align="center">' . $srCreatePatientCounter2 . '</td>
						<td align="center">' . $srRemotzAccessCounter2 . '</td>
						<td align="center"><b>' . $srTimeSpentCounter2 . '</b></td>
						<td></td>
					</tr>';
                }

                if (@mysqli_num_rows($sqlDateResult2) == 0) {
                    $html2 .= '<tr><td align="center" colspan="13"><br>No Record(s)</td></tr>';
                }

                $html2 .= '
		<tr>
        	<td colspan="13" style="height:10px; background-color:#f5f5f5"></td>
        </tr>
        <tr>
        	<td colspan="3" align="right"><b>Total No. Patients&nbsp;</b></td>
            <td align="center"><b>' . $srFullCustomCounter . '</b></td>
            <td align="center"><b>' . $srPartialCounter . '</b></td>
            <td align="center"><b>' . $srOrthoCounter . '</b></td>
            <td align="center"><b>' . $srCodesCounter . '</b></td>
            <td align="center"><b>' . $srSpecialRequestCounter . '</b></td>
            <td align="center"><b>' . $srIncorrectInfoCounter . '</b></td>
            <td align="center"><b>' . $srCreatePatientCounter . '</b></td>
            <td align="center"><b>' . $srRemotzAccessCounter . '</b></td>
            <td align="center"><b>' . $srTimeSpentCounter . '</b></td>
            <td></td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="5" cellspacing="0" width="100%" style="font-size:12px;">
    	<tr>
            <td width="150" align="right"><b>Total Time Spent:</b></td>
            <td><b>&nbsp;' . round($srTimeSpentCounter / 60, 2) . ' Hrs.</b></td>
        </tr>
    </table>';

                $i++;
                echo $html;
                /*$pdf = '';
                $zipallow = false;
                foreach ($html2 as $office) {
                    $pdf .= $office;
                }
                //echo $pdf;
                $html2pdf = new HTML2PDF('P', 'A4', 'en');
                $html2pdf->writeHTML(str_replace('&nbsp;', ' ', $html2));
                $html2pdf->Output('files/comp_office_pdf/'.$tmpCompany.'-'.$tmpOffice.'.pdf', 'F');
                $zipallow = true;*/
            }

            // echo $html2;

        }
        function company_reports($company_pdf)
        {
            global $con;
            // print_r($company_pdf);
            $html = '';
            $compID = array();
            foreach ($company_pdf as $key => $offices) {

                $compID[] = $key;
                $id_array[] = implode("','", $offices);


            }
            // print_r($id_array);
//$filterOffice = implode("','", $filterOffice1);
//$sqlVars .= " AND srOfficeId IN ('$filterOffice')";
            $i = 0;
            foreach ($compID as $id) {
            $html .= '<h2 align="center">'.getField("cui_companies", "companyId", $id, "companyName").'</h2>';
            $html .= '<h3 align="center" style="color:black;">All Offices</h3><br>';
				
				$counter_sn=0;
			$sCounter=0;
			$srFullCustomCounter=0;
			$srPartialCounter=0;
			$srOrthoCounter=0;
			$srCodesCounter=0;
			$srSpecialRequestCounter=0;
			$srIncorrectInfoCounter=0;
			$srCreatePatientCounter=0;
			$srRemotzAccessCounter=0;
			$srTimeSpentCounter=0;
                //  print_r($id_array[$i]);
                $html .= '<table border="1" cellpadding="5" cellspacing="0" width="100%" style="border:1px solid; border-collapse:collapse; font-family:Calibri, sans-serif, Arial, Helvetica; font-size:14px;">
        <tr class="titleTr">
          <td align="center" rowspan="2"><b>S.No.</b></td>
		  <td align="center" rowspan="2"><b>Patient ID</b></td>
		  <td align="center" rowspan="2"><b>Patient Name</b></td>
          <td colspan="8" align="center"><b>Type of Breakdown</b></td>
          <td align="center" rowspan="2"><b>Time spent<br />(in&nbsp;minutes)</b></td>
          <td align="center" rowspan="2" width="20%"><b>Remarks</b></td>
        </tr>
        <tr class="titleTr">
            <td align="center"><b>Full/<br />Custom</b></td>
            <td align="center"><b>Partial</b></td>
            <td align="center"><b>Ortho</b></td>
            <td align="center"><b>Codes</b></td>
            <td align="center"><b>Special<br />Request</b></td>
            <td align="center"><b>Incorrect<br />Info</b></td>
            <td align="center"><b>Create<br />Patient</b></td>
            <td align="center"><b>Remote<br />Access</b></td>
        </tr>';

                $sqlVars = "";
                $sqlVars2 = "";
                //echo $id;

                $filterCompany = $id;
                $sqlVars .= " And srCompanyId='$filterCompany'";
                $sqlVars2 .= " And srCompanyId='$filterCompany'";
                $sqlVars .= " And srOfficeId IN('$id_array[$i]')";
                $sqlVars2 .= " And srOfficeId IN('$id_array[$i]')";

                if ($_POST["filterDate"] != "") {
                    $filterDate = date("Y-m-d", strtotime($_POST["filterDate"]));
                    $sqlVars .= " And srDateTime >= '$filterDate'";
                }
                if ($_POST["filterDate2"] != "") {
                    $filterDate2 = date("Y-m-d", strtotime($_POST["filterDate2"]));
                    $sqlVars .= " And srDateTime <= '$filterDate2 23:59:59'";
                }
//
                $sqlDate = "select srId, DATE(srDateTime) as dateTime from cui_patients_status_report where srId > '0' $sqlVars GROUP BY DATE(srDateTime) ORDER BY srDateTime";
                $sqlDateResult = mysqli_query($con, $sqlDate);
//$sqlDateResult2=mysqli_query($con, $sqlDate);
                $sqlDateResult2 = $sqlDateResult;
                $sqlDateRs_array = @mysqli_fetch_all($sqlDateResult, MYSQLI_ASSOC);

                foreach ($sqlDateRs_array as $sqlDateRs) {
                    $tmpSrId = $sqlDateRs["srId"];
                    $tmpSrDateTime = $sqlDateRs["dateTime"];

                    $html .= '<tr style="height:10px; background-color:#f5f5f5">
						<td colspan="13"><b>' . date("m/d/Y", strtotime($tmpSrDateTime)) . '</b></td>
					</tr>';

                    $sqlPatients = "select * from cui_patients_status_report where srDateTime like '$tmpSrDateTime%' $sqlVars2 order by substring_index(TRIM(srPatientName), ' ', -1)";
                    $sqlPatientsResult = mysqli_query($con, $sqlPatients);

                   
                    $sqlDateRs_array2 = @mysqli_fetch_all($sqlPatientsResult, MYSQLI_ASSOC);
					$counter=0;
					
					$srFullCustomCounter2=0;
					$srPartialCounter2=0;
					$srOrthoCounter2=0;
					$srCodesCounter2=0;
					$srSpecialRequestCounter2=0;
					$srIncorrectInfoCounter2=0;
					$srCreatePatientCounter2=0;
					$srRemotzAccessCounter2=0;
					$srTimeSpentCounter2=0;
                    foreach ($sqlDateRs_array2 as $sqlPatientsRs) //while ($sqlPatientsRs=@mysqli_fetch_array($sqlPatientsResult))
                    {
                        $counter++;
                        $sCounter++;
                        $srId = $sqlPatientsRs["srId"];
                        $srPatientId = $sqlPatientsRs["srPatientId"];
                        $srPatientName = $sqlPatientsRs["srPatientName"];
                        $srUserId = $sqlPatientsRs["srUserId"];
                        $srCompanyId = $sqlPatientsRs["srCompanyId"];
                        $srOfficeId = $sqlPatientsRs["srOfficeId"];
                        $srFullCustom = $sqlPatientsRs["srFullCustom"];
                        $srPartial = $sqlPatientsRs["srPartial"];
                        $srOrtho = $sqlPatientsRs["srOrtho"];
                        $srCodes = $sqlPatientsRs["srCodes"];
                        $srSpecialRequest = $sqlPatientsRs["srSpecialRequest"];
                        $srIncorrectInfo = $sqlPatientsRs["srIncorrectInfo"];
                        $srCreatePatient = $sqlPatientsRs["srCreatePatient"];
                        $srRemotzAccess = $sqlPatientsRs["srRemotzAccess"];
                        $srTimeSpent = $sqlPatientsRs["srTimeSpent"];
                        $srRemarks = $sqlPatientsRs["srRemarks"];
                        $srDateTime = $sqlPatientsRs["srDateTime"];

                        //counters
                        $srFullCustomCounter += $sqlPatientsRs["srFullCustom"];
                        $srPartialCounter += $sqlPatientsRs["srPartial"];
                        $srOrthoCounter += $sqlPatientsRs["srOrtho"];
                        $srCodesCounter += $sqlPatientsRs["srCodes"];
                        $srSpecialRequestCounter += $sqlPatientsRs["srSpecialRequest"];
                        $srIncorrectInfoCounter += $sqlPatientsRs["srIncorrectInfo"];
                        $srCreatePatientCounter += $sqlPatientsRs["srCreatePatient"];
                        $srRemotzAccessCounter += $sqlPatientsRs["srRemotzAccess"];
                        $srTimeSpentCounter += $sqlPatientsRs["srTimeSpent"];
                        //counters2
                        $srFullCustomCounter2 += $sqlPatientsRs["srFullCustom"];
                        $srPartialCounter2 += $sqlPatientsRs["srPartial"];
                        $srOrthoCounter2 += $sqlPatientsRs["srOrtho"];
                        $srCodesCounter2 += $sqlPatientsRs["srCodes"];
                        $srSpecialRequestCounter2 += $sqlPatientsRs["srSpecialRequest"];
                        $srIncorrectInfoCounter2 += $sqlPatientsRs["srIncorrectInfo"];
                        $srCreatePatientCounter2 += $sqlPatientsRs["srCreatePatient"];
                        $srRemotzAccessCounter2 += $sqlPatientsRs["srRemotzAccess"];
                        $srTimeSpentCounter2 += $sqlPatientsRs["srTimeSpent"];

                        if ($counter > 1) {
                            $bgcolor = "#FFFFFF";
                            $counter = 0;
                        } elsE {
                            $bgcolor = "lightgrey";
                        }

                        if ($filterUsers == "") {
                            $srUserName = "<br><span style='font-size:8pt;'>(" . getField("cui_users", "userId", $sqlPatientsRs["srUserId"], "userFname") . " " . getField("cui_users", "userId", $sqlPatientsRs["srUserId"], "userLname") . ")</span>";
                        } else {
                            $srUserName = "<br>";
                        }

                        $html .= '<tr bgcolor="' . $bgcolor . '">
					<td align="center">' . $sCounter . '</td>
					<td align="left">' . $srPatientId . '</td>';
                        if ($filterCompany == "") {
                            $html .= '<td align="left">' . $srPatientName . ' (<acronym title="' . getField("cui_companies", "companyId", $srCompanyId, "companyName") . '">' . initials(getField("cui_companies", "companyId", $srCompanyId, "companyName")) . '</acronym>)' . $srUserName . '</td>';
                        } else {
                            $html .= '<td align="left">' . $srPatientName . $srUserName . '</td>';
                        }
                        $html .= '<td align="center">' . $srFullCustom . '</td>
					<td align="center">' . $srPartial . '</td>
					<td align="center">' . $srOrtho . '</td>
					<td align="center">' . $srCodes . '</td>
					<td align="center">' . $srSpecialRequest . '</td>
					<td align="center">' . $srIncorrectInfo . '</td>
					<td align="center">' . $srCreatePatient . '</td>
					<td align="center">' . $srRemotzAccess . '</td>
					<td align="center">' . $srTimeSpent . '</td>
					<td align="center">' . $srRemarks . '</td>
				</tr>';
                    }

                    $html .= '<tr>
						<td colspan="3" align="right">No. Patients&nbsp;</td>
						<td align="center">' . $srFullCustomCounter2 . '</td>
						<td align="center">' . $srPartialCounter2 . '</td>
						<td align="center">' . $srOrthoCounter2 . '</td>
						<td align="center">' . $srCodesCounter2 . '</td>
						<td align="center">' . $srSpecialRequestCounter2 . '</td>
						<td align="center">' . $srIncorrectInfoCounter2 . '</td>
						<td align="center">' . $srCreatePatientCounter2 . '</td>
						<td align="center">' . $srRemotzAccessCounter2 . '</td>
						<td align="center"><b>' . $srTimeSpentCounter2 . '</b></td>
						<td></td>
					</tr>';
                }

//if(@mysqli_num_rows($sqlDateResult) == 0)
                if (count($sqlDateRs_array) == 0) {
                    $html .= '<tr><td align="center" colspan="13"><br>No Record(s)</td></tr>';
                }

                $html .= '
		<tr>
        	<td colspan="13" style="height:10px; background-color:#f5f5f5"></td>
        </tr>
        <tr>
        	<td colspan="3" align="right"><b>Total No. Patients&nbsp;</b></td>
            <td align="center"><b>' . $srFullCustomCounter . '</b></td>
            <td align="center"><b>' . $srPartialCounter . '</b></td>
            <td align="center"><b>' . $srOrthoCounter . '</b></td>
            <td align="center"><b>' . $srCodesCounter . '</b></td>
            <td align="center"><b>' . $srSpecialRequestCounter . '</b></td>
            <td align="center"><b>' . $srIncorrectInfoCounter . '</b></td>
            <td align="center"><b>' . $srCreatePatientCounter . '</b></td>
            <td align="center"><b>' . $srRemotzAccessCounter . '</b></td>
            <td align="center"><b>' . $srTimeSpentCounter . '</b></td>
            <td></td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="5" cellspacing="0" width="100%" style="border:0px solid; font-family:Calibri, sans-serif, Arial, Helvetica; font-size:14px;">
    	<tr>
            <td width="220" align="right"><b>Total Time Spent:</b></td>
            <td><b>&nbsp;' . round($srTimeSpentCounter / 60, 2) . ' Hrs.</b></td>
        </tr>
    </table>';
                $i++;
            }

            echo $html;


        }


        company_reports($company_pdf);
        single_reports($filterOffice2);
        ?>
        <?php
    }
    else {
        $userSql = "SELECT company.companyId, company.companyName, cui_companies_offices.officeId , cui_companies_offices.officeName FROM cui_companies AS company
                    LEFT JOIN cui_companies_offices ON company.companyId = cui_companies_offices.companyId 
                    WHERE companyShowOnReport = 1 order by companyName";
        //					$userResult = mysql_query($userSql);
        $userResult = mysqli_query($con, $userSql);
        $rows = mysqli_fetch_all($userResult, MYSQLI_ASSOC);

        //print_r($rows);
        $perv_company_id = 0;
        $count = 0;

        echo "<div class='listing'> <h2>Companies and Offices</h2><span class='deselect-all pull-right can-select'><a href='javascript:;'>Unselect All</a></span><span class='select-all pull-right can-select'><a href='javascript:;'>Select All</a></span></div>";
        foreach ($rows as $row) {
            if ($count == 0) {
                echo "<div class='col-md-3'>";
            }


            if ($perv_company_id == null || $perv_company_id != $row['companyId']) {
                $count++;
                if ($count == 21) {
                    echo "</div></div><div class='col-md-3'>";
                } elseif ($count > 1) {
                    echo "</div>";
                }
                $name = str_replace(' ', '_', $row['companyName']);
                echo "<div class='company-card'><h4><input type='checkbox' class='check' name='selectedcompvalues[]' value='" . $row['companyId'] . "' id='" . $name . "'>" . $row['companyName'] . "</h4>";
                $perv_company_id = $row['companyId'];
            }
            if (in_array($perv_company_id, $row)) {
                echo "<p class='checkbox'><input type='checkbox' name='selectedvalues[".$row['companyId']."][]' value='" . $row['officeId'] . "'>" . $row['officeName'] . "</p>";
            }
            if ($count == 21) {
                $count = 1;
            }
        }
    }
    ?>
        </form>
    <?php if (array_key_exists('btn_report_companyoffice', $_POST)) { ?>
		<br>
        <form action="index.php?do=company_reports" target="_blank" method="post">
            <input type="hidden" name="selectedvalues" value='<?php echo json_encode($_POST["selectedvalues"]); ?>' />
            <input type="hidden" name="selectedcompvalues" value='<?php echo json_encode($_POST["selectedcompvalues"]); ?>' />
            <input type="hidden" name="filterDate" value="<?php echo $_POST["filterDate"]; ?>" />
            <input type="hidden" name="filterDate2" value="<?php echo $_POST["filterDate2"]; ?>" />


            <div align="center"><input type="submit" class="noprint btn searchbt" value="Generate PDF" title="Generate PDF" /></div>
        </form>

    <?php } ?>
<? }else if($manageType == "newcheck"){ ?>

	<script type='text/javascript'>
        $(document).ready(function () {

            console.log("HELLO");
            function exportTableToCSV($table, filename) {
                var $headers = $table.find('tr:has(th)')
                    ,$rows = $table.find('tr:has(td)')

                    // Temporary delimiter characters unlikely to be typed by keyboard
                    // This is to avoid accidentally splitting the actual contents
                    ,tmpColDelim = String.fromCharCode(11) // vertical tab character
                    ,tmpRowDelim = String.fromCharCode(0) // null character

                    // actual delimiter characters for CSV format
                    ,colDelim = '","'
                    ,rowDelim = '"\r\n"';

                    // Grab text from table into CSV formatted string
                    var csv = '"';
                    csv += formatRows($headers.map(grabRow));
                    csv += rowDelim;
                    csv += formatRows($rows.map(grabRow)) + '"';

                    // Data URI
                    var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

                // For IE (tested 10+)
                if (window.navigator.msSaveOrOpenBlob) {
                    var blob = new Blob([decodeURIComponent(encodeURI(csv))], {
                        type: "text/csv;charset=utf-8;"
                    });
                    navigator.msSaveBlob(blob, filename);
                } else {
                    $(this)
                        .attr({
                            'download': filename
                            ,'href': csvData
                            //,'target' : '_blank' //if you want it to open in a new window
                    });
                }

                //------------------------------------------------------------
                // Helper Functions 
                //------------------------------------------------------------
                // Format the output so it has the appropriate delimiters
                function formatRows(rows){
                    return rows.get().join(tmpRowDelim)
                        .split(tmpRowDelim).join(rowDelim)
                        .split(tmpColDelim).join(colDelim);
                }
                // Grab and format a row from the table
                function grabRow(i,row){
                     
                    var $row = $(row);
                    //for some reason $cols = $row.find('td') || $row.find('th') won't work...
                    var $cols = $row.find('td'); 
                    if(!$cols.length) $cols = $row.find('th');  

                    return $cols.map(grabCol)
                                .get().join(tmpColDelim);
                }
                // Grab and format a column from the table 
                function grabCol(j,col){
                    var $col = $(col),
                        $text = $col.text();

                    return $text.replace('"', '""'); // escape double quotes

                }
            }


            // This must be a hyperlink
            $("#export").click(function (event) {
                // var outputFile = 'export'
                var outputFile = window.prompt("What do you want to name your output file") || 'export';
                outputFile = outputFile.replace('.csv','') + '.csv'
                 
                // CSV
                exportTableToCSV.apply(this, [$('#newcheckreport'), outputFile]);
                
                // IF CSV, don't do event.preventDefault() or return false
                // We actually need this to be a typical hyperlink
            });
        });
    </script>
	<!--<form method="GET" action="index.php?do=manage&manageType=<?=$manageType?>">-->
		<div class="searchcard">
			<div class="searchformcontainer">
				<form class="form-inline" method="GET" action="index.php?do=manage&manageType=<?=$manageType?>">
				<input type="hidden" name="do" value="<?=$do?>" />
				<div class="col-md-12 clearfix">
					<div class="pull-left">
						<h3 style="color:black;"><?=$headingTitle?> Search</h3>
					</div>
					<div class="pull-right">
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=report'" <? if($manageType=="report"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt" >Status Report</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=access'" <? if($manageType=="access"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">User Access</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=summary'" <? if($manageType=="summary"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">Summary Report</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=newcheck'" <? if($manageType=="newcheck"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">New/Check Status Report</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=timesheet'" <? if($manageType=="timesheet"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">Timesheets</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=companyoffice'" <? if($manageType=="companyoffice"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">Companies/Offices</button>
						</div>
					</div>
				</div>
				
				<div class="col-md-12 clearfix" style="padding:8px 15px;">
					<div class="pull-left">
					<div class="form-group">
						From
						<input type="text" name="filterDate" id="filterDate" class="textbox form-control" style="width: 90px !important;" value="<? if($_GET["filterDate"]!=""){ ?><?=$_GET["filterDate"]?><? } ?>" size="8">
						<input type="button" class="btn clearbt" value="Pick" onclick="displayDatePicker('filterDate');" tabindex="32" />
					</div>
					<div class="form-group">
						To
						<input type="text" name="filterDate2" id="filterDate2" class="textbox form-control" style="width: 90px !important;" value="<? if($_GET["filterDate2"]!=""){ ?><?=$_GET["filterDate2"]?><? } ?>" size="8">
						<input type="button" class="btn clearbt" value="Pick" onclick="displayDatePicker('filterDate2');" tabindex="32" />
					</div>
					</div>
					<div class="pull-right">
					<div class="form-group">
						<input type="hidden" name="manageType" value="<?=$manageType?>" />
						<button type="submit" name="btn_report_newcheck" class="btn searchbt">Search</button>
					</div>
					<div class="form-group">
						<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=newcheck'" class="btn clearbt">Clear</button>
					</div>
					</div>
				</div>
				</form>
			</div>
		</div>
	<br />
	
	<?
	if (array_key_exists('btn_report_newcheck', $_GET)) {
	$html='';
    $html.='<table border="1" id="newcheckreport" cellpadding="5" cellspacing="0" width="100%" style="border:1px solid; border-collapse:collapse; font-family:Calibri, sans-serif, Arial, Helvetica; font-size:14px;">
        <tr class="titleTr">
            <td align="center"><b>No</b></td>
            <td align="center"><b>Patient ID</b></td>
            <td align="center"><b>Patient First Name</b></td>
            <td align="center"><b>Patient Last Name</b></td>
            <td align="center"><b>Company ID</b></td>
            <td align="center"><b>Company Name</b></td>
            <td align="center"><b>Office ID</b></td>
            <td align="center"><b>Office Name</b></td>
            <td align="center"><b>Patient Status</b></td>
            <td align="center"><b>Patient Added</b></td>
            <td align="center"><b>Patient App Date</b></td>
        </tr>';
        
    	$sqlVars = "";
		$sqlVars2 = "";
		
		if($_GET["filterDate"] != "")
		{
			$filterDate = date("Y-m-d", strtotime($_GET["filterDate"]));
			$sqlVars .= " And srDateTime >= '$filterDate'";
		}
		if($_GET["filterDate2"] != "")
		{
			$filterDate2 = date("Y-m-d", strtotime($_GET["filterDate2"]));
			$sqlVars .= " And srDateTime <= '$filterDate2 23:59:59'";
		}
        //
        $sqlDate="SELECT p.patientID, p.patientfname, p.`patientLname`, o.companyid, c.companyName AS 'Company Name', o.`officeId`, o.`officeName` AS 'Office Name',  p.`patientStatus`, p.patientAdded, p.`patientAppDate`
		FROM cui_patients p, cui_companies c, cui_companies_offices o
		WHERE p.`companyId` = c.`companyId` 
		AND p.officeid = o.officeId 
		AND p.patientAdded >= DATE('$filterDate') AND p.`patientAdded` <= DATE('$filterDate2') 
		AND p.`patientStatus` NOT IN ('Updated','Printed','Checked','Missed')
		ORDER BY p.patientAdded";
        //$sqlDate="select srId, DATE(srDateTime) as dateTime from cui_patients_status_report where srId > '0' $sqlVars GROUP BY DATE(srDateTime) ORDER BY srDateTime";
        $sqlDateResult=mysqli_query($con, $sqlDate);
		$sqlDateRs_array = @mysqli_fetch_all($sqlDateResult,MYSQLI_ASSOC);
		$sncount = 0;
		foreach($sqlDateRs_array as $sqlDateRs)
		{
			$sncount++;
			$counter++;
			$rs_patientid = $sqlDateRs["patientID"];
			$rs_patientfname = $sqlDateRs["patientfname"];
			$rs_patientlname = $sqlDateRs["patientLname"];
			$rs_companyid = $sqlDateRs["companyid"];
			$rs_companyname = $sqlDateRs["Company Name"];
			$rs_officeid = $sqlDateRs["officeId"];
			$rs_officename = $sqlDateRs["Office Name"];
			$rs_patientStatus = $sqlDateRs["patientStatus"];
			$rs_patientAdded = $sqlDateRs["patientAdded"];
			$rs_patientAppDate = $sqlDateRs["patientAppDate"];
			
			if($counter>1){
				$bgcolor="#FFFFFF";
				$counter=0;
			}elsE{
				$bgcolor="lightgrey";
			}
				
			$html.='<tr bgcolor="'.$bgcolor.'">
				<td>'.$sncount.'</td>
				<td>'.$rs_patientid.'</td>
				<td>'.$rs_patientfname.'</td>
				<td>'.$rs_patientlname.'</td>
				<td>'.$rs_companyid.'</td>
				<td>'.$rs_companyname.'</td>
				<td>'.$rs_officeid.'</td>
				<td>'.$rs_officename.'</td>
				<td>'.$rs_patientStatus.'</td>
				<td>'.date('m/d/Y H:i:s', strtotime($rs_patientAdded)).'</td>
				<td>'.date('m/d/Y H:i:s', strtotime($rs_patientAppDate)).'</td>
			</tr>';
        }
		
        if(count($sqlDateRs_array) == 0)
        {
            $html.='<tr><td align="center" colspan="10"><br>No Record(s)</td></tr>';
        }
        
        $html.='
    </table>';
	echo $html;
	?>
	<br>
	
	<div align="center">
		<a href="#" class="noprint btn searchbt" id ="export" role="button" >Generate CSV</a>
	</div>
    
	
	<? } ?>
<? }else if($manageType == "timesheet"){ ?>
	<!--<form method="GET" action="index.php?do=manage&manageType=<?=$manageType?>">-->
		<div class="searchcard">
			<div class="searchformcontainer">
				<form class="form-inline" method="GET" action="index.php?do=manage&manageType=<?=$manageType?>">
				<input type="hidden" name="do" value="<?=$do?>" />
				<div class="col-md-12 clearfix">
					<div class="pull-left">
						<h3 style="color:black;"><?=$headingTitle?> Search</h3>
					</div>
					<div class="pull-right">
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=report'" <? if($manageType=="report"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt" >Status Report</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=access'" <? if($manageType=="access"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">User Access</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=summary'" <? if($manageType=="summary"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">Summary Report</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=newcheck'" <? if($manageType=="newcheck"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">New/Check Status Report</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=timesheet'" <? if($manageType=="timesheet"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">Timesheets</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=companyoffice'" <? if($manageType=="companyoffice"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">Companies/Offices</button>
						</div>
					</div>
				</div>
				
				<div class="col-md-12 clearfix" style="padding:8px 15px;">
					<div class="pull-left">
					<div class="form-group">
						CUI Users
						<select name="filterUsers" onchange="showCompanies(this.value, '0');" class="selectpicker" style="width:130px !important">
							<option value="">All Users</option>
							<?
                                $usersSql = "select * from cui_users where userLevel='34' order by userFname, userLname";
								$usersResult = mysqli_query($con, $usersSql);
								//
								$compSql = "SELECT * FROM cui_companies WHERE companyShowOnReport = '1' ORDER BY companyName";
								$compResult = mysqli_query($con, $compSql);
								//
								$tmpCond = "";
								if($_GET["filterCompany"] != "")
								{
									$tmpCompany = $_GET["filterCompany"];
									$tmpCond = " Where companyId='$tmpCompany' ";
								}
								$offSql = "select * from cui_companies_offices $tmpCond order by officeName";
								$offResult = mysqli_query($con, $offSql);
							?>
							<? while ($usersRs = @mysqli_fetch_array($usersResult)){ ?>
                            <option value="<?=$usersRs["userId"]?>" <? if($_GET["filterUsers"]==$usersRs["userId"]){ ?>selected<? } ?>><?=$usersRs["userFname"]." ".$usersRs["userLname"]?></option>
                            <? } ?>
						</select>
					</div>
					<div class="form-group">
						Companies
						<select name="filterCompany" id="filterCompany" onchange="showOffices(this.value, '0');" class="selectpicker" style="width:130px !important">
                            <option value="">All Companies</option>
                            <? while ($compRs = @mysqli_fetch_array($compResult)){ ?>
                              	<option value="<?=$compRs["companyId"]?>" <? if($_GET["filterCompany"]==$compRs["companyId"]){ ?>selected<? } ?>><?=$compRs["companyName"]?></option>
                            <? } ?>
                        </select>
					</div>
					<div class="form-group">
						Offices
						<select name="filterOffice" id="filterOffice" style="width:150px;" class="selectpicker" style="width:130px !important">
                            <option value="">All Offices</option>
                            <? while ($offRs = @mysqli_fetch_array($offResult)){ ?>
                               	<? if($_GET["filterCompany"] == ""){ ?>
                                   	<option value="<?=$offRs["officeId"]?>" <? if($_GET["filterOffice"]==$offRs["officeId"]){ ?>selected<? } ?>><?=$offRs["officeName"].' ('.initials(getField("cui_companies","companyId",$offRs["companyId"],"companyName")).')'?></option>
                                <? }else{ ?>
                                   	<option value="<?=$offRs["officeId"]?>" <? if($_GET["filterOffice"]==$offRs["officeId"]){ ?>selected<? } ?>><?=$offRs["officeName"]?></option>
                                <? } ?>
                            <? } ?>
                        </select>
					</div>
					<div class="form-group">
						From
						<input type="text" name="filterDate" id="filterDate" class="textbox form-control" style="width: 90px !important;" value="<? if($_GET["filterDate"]!=""){ ?><?=$_GET["filterDate"]?><? } ?>" size="8">
						<input type="button" class="btn clearbt" value="Pick" onclick="displayDatePicker('filterDate');" tabindex="32" />
					</div>
					<div class="form-group">
						To
						<input type="text" name="filterDate2" id="filterDate2" class="textbox form-control" style="width: 90px !important;" value="<? if($_GET["filterDate2"]!=""){ ?><?=$_GET["filterDate2"]?><? } ?>" size="8">
						<input type="button" class="btn clearbt" value="Pick" onclick="displayDatePicker('filterDate2');" tabindex="32" />
					</div>
					</div>
					<div class="pull-right">
					<div class="form-group">
						<input type="hidden" name="manageType" value="<?=$manageType?>" />
						<button type="submit" class="btn searchbt">Search</button>
					</div>
					<div class="form-group">
						<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=timesheet'" class="btn clearbt">Clear</button>
					</div>
					</div>
				</div>
				</form>
			</div>
		</div>
	<br />	
<? }else{ ?>
	<!--<form method="GET" action="index.php?do=manage&manageType=<?=$manageType?>">-->
		<div class="searchcard">
			<div class="searchformcontainer">
				<form class="form-inline" method="GET" action="index.php?do=manage&manageType=<?=$manageType?>">
				<input type="hidden" name="do" value="<?=$do?>" />
				<div class="col-md-12 clearfix">
					<div class="pull-left">
						<h3 style="color:black;"><?=$headingTitle?> Search</h3>
					</div>
					<div class="pull-right">
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=report'" <? if($manageType=="report"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt" >Status Report</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=access'" <? if($manageType=="access"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">User Access</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=summary'" <? if($manageType=="summary"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">Summary Report</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=newcheck'" <? if($manageType=="newcheck"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">New/Check Status Report</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=timesheet'" <? if($manageType=="timesheet"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">Timesheets</button>
						</div>
						<div class="form-group">
							<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=companyoffice'" <? if($manageType=="companyoffice"){ ?>style="background-color:#CCCCCC;" disabled<? } ?> class="btn searchbt">Companies/Offices</button>
						</div>
					</div>
				</div>
				
				<div class="col-md-12 clearfix" style="padding:8px 15px;">
					<div class="pull-left">
					<div class="form-group">
						CUI Users
						<select name="filterUsers" onchange="showCompanies(this.value, '0');" class="selectpicker" style="width:130px !important">
							<option value="">All Users</option>
							<?
                                $usersSql = "select * from cui_users where userLevel='34' order by userFname, userLname";
								$usersResult = mysqli_query($con, $usersSql);
								//
								$compSql = "SELECT * FROM cui_companies WHERE companyShowOnReport = '1' ORDER BY companyName";
								$compResult = mysqli_query($con, $compSql);
								//
								$tmpCond = "";
								if($_GET["filterCompany"] != "")
								{
									$tmpCompany = $_GET["filterCompany"];
									$tmpCond = " Where companyId='$tmpCompany' ";
								}
								$offSql = "select * from cui_companies_offices $tmpCond order by officeName";
								$offResult = mysqli_query($con, $offSql);
							?>
							<? while ($usersRs = @mysqli_fetch_array($usersResult)){ ?>
                            <option value="<?=$usersRs["userId"]?>" <? if($_GET["filterUsers"]==$usersRs["userId"]){ ?>selected<? } ?>><?=$usersRs["userFname"]." ".$usersRs["userLname"]?></option>
                            <? } ?>
						</select>
					</div>
					<div class="form-group">
						Companies
						<select name="filterCompany" id="filterCompany" onchange="showOffices(this.value, '0');" class="selectpicker" style="width:130px !important">
                            <option value="">All Companies</option>
                            <? while ($compRs = @mysqli_fetch_array($compResult)){ ?>
                              	<option value="<?=$compRs["companyId"]?>" <? if($_GET["filterCompany"]==$compRs["companyId"]){ ?>selected<? } ?>><?=$compRs["companyName"]?></option>
                            <? } ?>
                        </select>
					</div>
					<div class="form-group">
						Offices
						<select name="filterOffice" id="filterOffice" style="width:150px;" class="selectpicker" style="width:130px !important">
                            <option value="">All Offices</option>
                            <? while ($offRs = @mysqli_fetch_array($offResult)){ ?>
                               	<? if($_GET["filterCompany"] == ""){ ?>
                                   	<option value="<?=$offRs["officeId"]?>" <? if($_GET["filterOffice"]==$offRs["officeId"]){ ?>selected<? } ?>><?=$offRs["officeName"].' ('.initials(getField("cui_companies","companyId",$offRs["companyId"],"companyName")).')'?></option>
                                <? }else{ ?>
                                   	<option value="<?=$offRs["officeId"]?>" <? if($_GET["filterOffice"]==$offRs["officeId"]){ ?>selected<? } ?>><?=$offRs["officeName"]?></option>
                                <? } ?>
                            <? } ?>
                        </select>
					</div>
					<div class="form-group">
						From
						<input type="text" name="filterDate" id="filterDate" class="textbox form-control" style="width: 90px !important;" value="<? if($_GET["filterDate"]!=""){ ?><?=$_GET["filterDate"]?><? } ?>" size="8">
						<input type="button" class="btn clearbt" value="Pick" onclick="displayDatePicker('filterDate');" tabindex="32" />
					</div>
					<div class="form-group">
						To
						<input type="text" name="filterDate2" id="filterDate2" class="textbox form-control" style="width: 90px !important;" value="<? if($_GET["filterDate2"]!=""){ ?><?=$_GET["filterDate2"]?><? } ?>" size="8">
						<input type="button" class="btn clearbt" value="Pick" onclick="displayDatePicker('filterDate2');" tabindex="32" />
					</div>
					</div>
					<div class="pull-right">
					<div class="form-group">
						<input type="hidden" name="manageType" value="<?=$manageType?>" />
						<button type="submit" class="btn searchbt">Search</button>
					</div>
					<div class="form-group">
						<button type="button" onclick="javascript: window.location='index.php?do=manage&manageType=summary'" class="btn clearbt">Clear</button>
					</div>
					</div>
				</div>
				</form>
			</div>
		</div>
	<br />	
	
    <? if(isset($_GET["filterUsers"]) || isset($_GET["filterCompany"]) || isset($_GET["filterDate"]) || isset($_GET["filterDate2"])){ ?>
	<table border="1" cellpadding="5" cellspacing="0" width="100%" style="border:1px solid; border-collapse:collapse; font-family:Calibri, sans-serif, Arial, Helvetica; font-size:14px;">
        <tr class="titleTr">
          <td align="center"><b>Date</b></td>
		  <td align="center"><b>No. of Patients</b></td>
          <td align="center" ><b>No. of Minutes</b></td>
          <td width="70%"><b>Notes</b></td>
        </tr>
        <?
    	$sqlVars = "";
		
		if($_GET["filterUsers"] != "")
		{
			$filterUsers = $_GET["filterUsers"];
			$sqlVars .= " And srUserId='$filterUsers'";
		}
		if($_GET["filterCompany"] != "")
		{
			$filterCompany = $_GET["filterCompany"];
			$sqlVars .= " And srCompanyId='$filterCompany'";
		}
		if($_GET["filterDate"] != "")
		{
			$filterDate = date("Y-m-d", strtotime($_GET["filterDate"]));
			$sqlVars .= " And srDateTime >= DATE('$filterDate')";
		}
		if($_GET["filterDate2"] != "")
		{
			$filterDate2 = date("Y-m-d", strtotime($_GET["filterDate2"]));
			$sqlVars .= " And srDateTime <= DATE('$filterDate2')";
		}
		
        //
        $sqlPatients="SELECT srId, srDateTime, COUNT(*) as pCount, sum(srTimeSpent) as timeSpent, sum(srFullCustom) as fullCustom, sum(srPartial) as partial, 
		sum(srOrtho) as ortho, sum(srCodes) as codes, sum(srSpecialRequest) as specialRequest, sum(srIncorrectInfo) as incorrectInfo, 
		sum(srCreatePatient) as createPatient, sum(srRemotzAccess) as remotzAccess 
		FROM cui_patients_status_report  Where srId > '0' $sqlVars GROUP BY DATE(srDateTime) ORDER BY srDateTime";
        $sqlPatientsResult=mysqli_query($con, $sqlPatients);
		$sqlPatientsResult2=mysqli_query($con, $sqlPatients);
        while ($sqlPatientsRs=@mysqli_fetch_array($sqlPatientsResult)) {
            $counter++;
            $sCounter++;
			$srId=$sqlPatientsRs["srId"];
            $srDateTime=date("m/d/Y", strtotime($sqlPatientsRs["srDateTime"]));
            $pCount=$sqlPatientsRs["pCount"];
            $timeSpent=$sqlPatientsRs["timeSpent"];
            $fullCustom=$sqlPatientsRs["fullCustom"];
            $partial=$sqlPatientsRs["partial"];
            $ortho=$sqlPatientsRs["ortho"];
            $codes=$sqlPatientsRs["codes"];
            $specialRequest=$sqlPatientsRs["specialRequest"];
            $incorrectInfo=$sqlPatientsRs["incorrectInfo"];
            $createPatient=$sqlPatientsRs["createPatient"];
            $remotzAccess=$sqlPatientsRs["remotzAccess"];
            
			//counters
			$srPatientCounter+=$pCount;
			$srTimeSpentCounter+=$timeSpent;
			
            if($counter>1){
                $bgcolor="#FFFFFF";
                $counter=0;
            }elsE{
                $bgcolor="lightgrey";
            }
            ?>
            <tr bgcolor="<?=$bgcolor?>">
                <td align="center"><?=$srDateTime?></td>
                <td align="center"><?=$pCount?></td>
                <td align="center"><?=$timeSpent?></td>
                <td>
				Total Patients: <?=$pCount?>, Total Minutes: <?=$timeSpent?>, Total Patients Breakdown=> Full/Custom: <?=$fullCustom?>, Partial: <?=$partial?>, 
                Ortho: <?=$ortho?>, Codes: <?=$codes?>,<br />Special Request: <?=$specialRequest?>, Incorrect Info: <?=$incorrectInfo?>, Create Patient: <?=$createPatient?>, 
                Remote Access: <?=$remotzAccess?>
                </td>
            </tr>
        <?
        }
        
        if(@mysqli_num_rows($sqlPatientsResult) == 0)
        {
		?>
            <tr><td align="center" colspan="4"><br>No Record(s)</td></tr>
        <?
        }else{
        ?>
            <tr>
                <td align="right"><b>Total&nbsp;</b></td>
                <td align="center"><b><?=$srPatientCounter?></b></td>
                <td align="center"><b><?=$srTimeSpentCounter?></b></td>
                <td></td>
            </tr>
        
        <?
        }
		?>
    </table>
    <br />
    <table border="0" cellpadding="5" cellspacing="0" width="100%" style="border:0px solid; font-family:Calibri, sans-serif, Arial, Helvetica; font-size:14px;">
    	<tr>
            <td width="220" align="right"><b>Total Hours:</b></td>
            <td><b>&nbsp;<?=round($srTimeSpentCounter/60, 2)?> Hrs.</b></td>
        </tr>
    </table>
	<? } ?>
    
	<?
	//for print only /////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	if(isset($_GET["filterUsers"]) || isset($_GET["filterCompany"]) || isset($_GET["filterDate"]) || isset($_GET["filterDate2"]))
	{
		$tmpUser = "All Users";
		$tmpCompany = "All Companies";
		$tmpDate = "All";
		$tmpDate2 = "All";
		
		if($filterUsers != "")
		{
			$tmpUser = getField("cui_users","userId",$filterUsers,"userFname")." ".getField("cui_users","userId",$filterUsers,"userLname");
		}
		if($filterCompany != "")
		{
			$tmpCompany = getField("cui_companies","companyId",$filterCompany,"companyName");
		}
		if($filterDate)
		{ 
			$tmpDate = date("m/d/Y", strtotime($filterDate));
		}
		if($filterDate2)
		{ 
			$tmpDate2 = date("m/d/Y", strtotime($filterDate2));
		}
		
		$html2='<table border="0" cellpadding="5" cellspacing="0" width="100%" style="text-algin:center;">
			<tr>
				<td width="80" align="left"><img src="'.HTTP_SERVER.'images/report_logo.jpg" height="30" /></td>
				<td align="center" width="630"><b>Monthly CUI Summary Report - '.$tmpCompany.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
			</tr>
		</table><br>';
		
		$html2.='<table border="0" cellpadding="5" cellspacing="0" width="100%" style="text-algin:center;">
			<tr>
				<td><b>Consultant - '.$tmpUser.'</b></td>
				<td width="50"></td>
				<td><b>From: '.$tmpDate.' - To: '.$tmpDate2.'</b></td>
			</tr>
		</table><br>';
		
		$html2.='<table border="1" cellpadding="5" cellspacing="4" width="100%" style="border:1px solid #000; border-collapse:collapse; font-size:8pt;">
			<tr style="background: #f5f5f5">
			  <td width="10%" align="center"><b>Date</b></td>
			  <td width="10%" align="center"><b>&nbsp;No. of Patients&nbsp;</b></td>
			  <td width="10%" align="center" ><b>&nbsp;No. of Minutes&nbsp;</b></td>
			  <td width="70%"><b>&nbsp;Notes</b></td>
			</tr>';
			
			$counter=0;
			$sCounter=0;
			$srFullCustomCounter=0;
			$srPartialCounter=0;
			$srOrthoCounter=0;
			$srCodesCounter=0;
			$srSpecialRequestCounter=0;
			$srIncorrectInfoCounter=0;
			$srCreatePatientCounter=0;
			$srRemotzAccessCounter=0;
			$srTimeSpentCounter=0;
			
			while ($sqlPatientsRs=@mysqli_fetch_array($sqlPatientsResult2)) {
				$counter++;
				$sCounter++;
				$srId=$sqlPatientsRs["srId"];
				$srDateTime=date("m/d/Y", strtotime($sqlPatientsRs["srDateTime"]));
				$pCount=$sqlPatientsRs["pCount"];
				$timeSpent=$sqlPatientsRs["timeSpent"];
				$fullCustom=$sqlPatientsRs["fullCustom"];
				$partial=$sqlPatientsRs["partial"];
				$ortho=$sqlPatientsRs["ortho"];
				$codes=$sqlPatientsRs["codes"];
				$specialRequest=$sqlPatientsRs["specialRequest"];
				$incorrectInfo=$sqlPatientsRs["incorrectInfo"];
				$createPatient=$sqlPatientsRs["createPatient"];
				$remotzAccess=$sqlPatientsRs["remotzAccess"];
				
				//counters
				$srTimeSpentCounter+=$pCount;
				$srTimeSpentCounter+=$timeSpent;
				
				if($counter>1){
					$bgcolor="#FFFFFF";
					$counter=0;
				}elsE{
					$bgcolor="lightgrey";
				}
				
				$html2.='<tr bgcolor="'.$bgcolor.'">
					<td align="center">&nbsp;'.$srDateTime.'&nbsp;</td>
					<td align="center">'.$pCount.'</td>
					<td align="center">'.$timeSpent.'</td>
					<td>&nbsp;Total Patients: '.$pCount.', Total Minutes: '.$timeSpent.', Total Patients Breakdown=> Full/Custom: '.$fullCustom.', Partial: '.$partial.', 
					Ortho: '.$ortho.',&nbsp;<br>&nbsp;Codes: '.$codes.', Special Request: '.$specialRequest.', Incorrect Info: '.$incorrectInfo.', Create Patient: '.$createPatient.', 
					Remote Access: '.$remotzAccess.'&nbsp;</td>
				</tr>';
			}
			
			if(@mysqli_num_rows($sqlPatientsResult2) == 0)
			{
				$html2.='<tr><td align="center" colspan="4"><br>No Record(s)</td></tr>';
			}else{
				$html2.='<tr>
					<td align="right"><b>Total&nbsp;</b></td>
					<td align="center"><b>'.$srPatientCounter.'</b></td>
					<td align="center"><b>'.$srTimeSpentCounter.'</b></td>
					<td></td>
				</tr>';
			}
			
			$html2.='
		</table>
		<br />
		<table border="0" cellpadding="5" cellspacing="0" width="100%" style="font-size:12px;">
			<tr>
				<td width="170" align="right"><b>Total Hours:</b></td>
				<td><b>&nbsp;'.round($srTimeSpentCounter/60, 2).' Hrs.</b></td>
			</tr>
		</table>';
	}
	?>

	<?
    if(isset($_GET["filterUsers"]) || isset($_GET["filterCompany"]) || isset($_GET["filterDate"]) || isset($_GET["filterDate2"]))
	{
    //todo: removed old logic to generate the text file (logic created by Saadat)
//		$fp = fopen("files/CUI_Summary_Report.txt", "w");
//		fwrite($fp, "$html2\n");
//		fclose($fp);

    $sql = "UPDATE cui_reports SET cui_summary_report='{$html2}' where id=1" ;
    mysqli_query($con, $sql);

	?>
    
    	<div align="center"><input type="button" class="noprint btn searchbt" value="Generate PDF" title="Generate PDF" onclick="window.location='generate_summary_report_pdf.php';" /></div>
    <?
    }
	?>

<? } ?>



</div>
<style>
    p.checkbox {
        margin-left: 60px;
    }

    .company-card input {
        margin-right: 10px;
    }

    h4 {
        color: #2b2b2b;
        font-size: 14px;
        font-weight: 600;
        text-transform: uppercase;
        padding-left: 20px;

    }

    .listing h2 {
        float: left;
        width: 80%;
    }

    .can-select {
        margin: 26px 10px;
        font-weight: bold;
        font-size: 12px;
    }

    .comboTreeWrapper {
        position: relative;
        text-align: left !important;
    }
</style>
<script>

    $(document).ready(function(){
        $(".select-all a").on("click", function () {
            $(".company-card input:checkbox").prop('checked', 'checked');
        });
        $(".deselect-all a").on("click", function () {
            $(".company-card input:checkbox").prop('checked', false);
        });

        $(document).on('change','.check, .checkbox input:checkbox', function(){

            if($(this).hasClass('check')){

                if( $(this).prop('checked') ){
                    $(this).parents('.company-card').find('input:checkbox').prop('checked',true);
                }
                else {
                    $(this).parents('.company-card').find('input:checkbox').removeAttr('checked');
                }

            }
            else {

                if( $(this).prop('checked') ){
                    //$(this).parents('.company-card').find('.input:checkbox:not[class="check"]').prop('checked',true);
                }
                else {
                    $(this).parents('.company-card').find('.check').removeAttr('checked');
                }

            }
        });


    });
    /*
        $(document).on("change", ".check", function () {

            $(this).is(':checked')
                ? $(this).parent().parent().find(".checkbox input:checkbox").prop('checked', true)
                : $(this).parent().parent().find(".checkbox input:checkbox").removeAttr('checked')
            ;
            /*
            var inp = $(this).parent().parent().find(".checkbox input:checkbox");
            if (inp.prop("checked") == false && $(this).is(":checked") == true) {
                $(inp).prop('checked', 'checked');
            } else if ($(this).is(":checked") == false) {
                $(inp).removeAttr('checked');
                $(this).removeAttr('checked');
            }

        });*/
</script>