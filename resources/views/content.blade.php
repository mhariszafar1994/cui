@extends('layout-new')

@section('content')
<div class="container-fluid">
	<div class="card min-h-50">
		<div class="card-body">
			<!-- <div class="custom-control custom-checkbox mb-1">
				<input type="checkbox" class="custom-control-input" id="customCheck">
				<label class="custom-control-label" for="customCheck">CheckBox</label>
			</div> -->
			<h2 class="mb-3">Companies</h2>
			<div class="nav title-nav mb-3">
				<select class="selectpicker mr-2 dropdown" data-style="btn-white">
					<option value="" selected>---select---</option>
				</select>
				<div class="position-relative mr-2 responsive-elements">
					<input class="form-control" type="text" name="" placeholder="Search Text">
					<button class="fa fa-search searchBtnIcon text-checkurinsurance"></button>
				</div>
				<div class="radio radio-css mr-2 responsive-elements">
					<div class="d-inline-block mr-3">
						<input type="radio" name="radio_cssd" id="radio_7" checked="">
						<label for="radio_7">AND</label>
					</div>
					<div class="d-inline-block">
						<input type="radio" name="radio_cssd" id="radio_8" checked="">
						<label for="radio_8">OR</label>
					</div>
				</div>
				<a class="btn btn-dark mr-md-2 responsive-elements" href="javascript:;" title="">Clear</a>
				<a class="btn btn-success responsive-elements" href="javascript:;" title="">Add Company</a>
			</div>
			<div>
				<div class="nav bg-light title-nav justify-content-center">
					<b>Company Listing</b>
				</div>
				<table class="table table-striped table-td-valign-middle" width="100%">
					<thead>
						<tr>
							<th class="text-center">Status</th>
							<th class="text-center">Company Id</th>
							<th class="text-center">Company Name</th>
							<th class="text-center">Country</th>
							<th class="text-center">State</th>
							<th class="text-center">Zip</th>
							<th class="text-center">Actions</th>
						</tr>
					</thead>
					<tbody>
						<tr class="odd gradeX">
							<td align="center">
								<label class="integators bg-success"></label>
							</td>
							<td align="center">4545242412125656</td>
							<td align="center">Sale</td>
							<td align="center">Approved</td>
							<td align="center">Credit Card</td>
							<td align="center">$212.36</td>
							<td align="center">
								<div>
									<a class="btn btn-dark" href="" title="">
										<i class="fa fa-pencil"></i>
									</a>
									<a class="btn btn-checkurinsurance" href="" title="">
										<i class="fa fa-trash"></i>
									</a>
									<div class="d-inline-block figure-img custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="customSwitch1">
										<label class="custom-control-label" for="customSwitch1"></label>
									</div>
									<a class="btn btn-default" href="" title="">
										<i class="fa fa-search"></i>
									</a>
									<a class="btn btn-default" href="" title="">
										<i class="fa fa-user"></i>
									</a>
									<a class="btn btn-success" href="" title="">
										<i class="fa fa-check"></i>
									</a>
								</div>
							</td>
						</tr>
						<tr class="odd gradeX">
							<td align="center">
								<label class="integators bg-danger"></label>
							</td>
							<td align="center">4545242412125656</td>
							<td align="center">Sale</td>
							<td align="center">Approved</td>
							<td align="center">Credit Card</td>
							<td align="center">$212.36</td>
							<td align="center">
								<div>
									<a class="btn btn-dark" href="" title="">
										<i class="fa fa-pencil"></i>
									</a>
									<a class="btn btn-checkurinsurance" href="" title="">
										<i class="fa fa-trash"></i>
									</a>
									<div class="d-inline-block figure-img custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="customSwitch2">
										<label class="custom-control-label" for="customSwitch2"></label>
									</div>
									<a class="btn btn-default" href="" title="">
										<i class="fa fa-search"></i>
									</a>
									<a class="btn btn-default" href="" title="">
										<i class="fa fa-user"></i>
									</a>
									<a class="btn btn-success" href="" title="">
										<i class="fa fa-check"></i>
									</a>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection