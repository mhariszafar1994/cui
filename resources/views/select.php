<?
//if get act
//echo "test: ".$_SESSION[$sessionUserID];
//$_SESSION['cuiSessionUserId']
$act = "";
$msg = '';
if ($_GET["act"]) {
    $act = sanitize($_GET["act"]);
}
if ($_GET["msg"]) {
    $msg = sanitize($_GET["msg"]);
}
/*if(!isset($_GET['comp'])) {
    if(isset($_SESSION["cuiSessionCompanyId"]) && isset($_SESSION["cuiSessionOfficeId"])){
        echo "<script>window.location='index.php?do=home'</script>";
        exit();
    }
}*/
if(!isset($_GET['comp']))
{
    $sql_default = "SELECT userDefault FROM cui_users WHERE userId='{$_SESSION[$sessionUserID]}'";
}else{
    $sql_default = "SELECT userDefault FROM cui_users WHERE userId='{$_SESSION[$sessionUserID]}' AND userDefaultOfficeId='{$_SESSION['cuiSessionOfficeId']}' AND userDefaultCompanyId='{$_SESSION['cuiSessionCompanyId']}'";
}



//$userResult=mysql_query($sql_default);
$userResult=mysqli_query($con, $sql_default);
$userDefault = 0;
//if(mysql_num_rows($userResult)){
if(@mysqli_num_rows($userResult)){
//    $userDefault = mysql_result($userResult,0,"userDefault");
    $userDefault = mysqli_result($userResult,0,"userDefault");
}
//post vars
if(array_key_exists('pagesubmit',$_POST)){
    $default = 0;
    if(isset($_POST['chkDefault'])) {
        $default = $_POST['chkDefault'];
    }
    if($_POST["companyId"]){
        $_SESSION["cuiSessionCompanyId"] = sanitize($_POST["companyId"]);
		
		//added by Noman - May-25-2015
		//creating session for custom form and that page tag - start
		$tmpUserCompanyId = sanitize($_POST["companyId"]);
		if($tmpUserCompanyId!="" && $tmpUserCompanyId>0)
		{
			$tmpCustCompName = getField("cui_companies","companyId",$tmpUserCompanyId,"companyCompanyName"," and companyCustom='1'");
			$tmpCustCompTag = getField("cui_custom_companies","custCompanyName",$tmpCustCompName,"custCompanyTag"," and custCompanyStatus='1'");
			$_SESSION["cuiSessionCompanyTag"] = $tmpCustCompTag;
		}
		//creating session for custom form and that page tag - end
    }
    if($_POST["officeId"]){
        $_SESSION["cuiSessionOfficeId"] = sanitize($_POST["officeId"]);
        }else{
        $_SESSION["cuiSessionOfficeId"] = "";
    }
    #### Default set
    if($default==1 || $default==2)
	{
        $sql = "UPDATE cui_users SET userDefault=$default, userDefaultOfficeId='{$_SESSION['cuiSessionOfficeId']}', userDefaultCompanyId='{$_SESSION['cuiSessionCompanyId']}' WHERE userId='{$_SESSION[$sessionUserID]}'";
//        mysql_query($sql);
        mysqli_query($con, $sql);
    }
    echo "<script>window.location='index.php?do=home'</script>";
    exit();
}
?>
<?
//include functions 2 file
include(SITE_ROOT . 'includes/functions_2.php');
?>
<script type="text/javascript">
function chkForm(){
    var a = document.getElementById("companyId").value;
    var b = document.getElementById("officeId").value;
    if(a == "" || b==""){
        alert("You must select a company and office to proceed!");
        return false;
    }
}
</script>
<style>
		hr {
			border-bottom: 0px;
		}
		table.form-spacing tbody tr td {
			padding-bottom: 9px;
		}
</style>
<h1 class="h1WithBg">Select Company</h1>
<div id="pageContainer">
<table class="form-spacing" width="100%" cellpadding="0" cellspacing="0" align="center">
<tr>
<td valign="top">Select a company below. Selecting an office is optional. Selecting a company here will define the data you see throughout the application.</td>
</tr>
<?
//div error and success
if($msg!=''){?>
    <tr>
    <td height="40px"><div class="message"><?=$msg?></div></td>
    </tr>
<?}?>
<?/*
<tr>
<td>
<table cellpadding="5" cellspacing="5" id="hotelControls" class="controls">
<tr>
<td class="titleTd">
<table cellpadding="1" cellspacing="0" align="center">
<tr align="center">
<td>
<a href="index.php?do=change_password"><img border="0" src="<?=HTTP_SERVER?>images/icon_settings.png" alt="Account Settings" /></a><br><a href="index.php?do=change_password">My Account</a>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
*/?>
<tr>
<td>
<form method="POST">
<table cellpadding="5" cellspacing="0" align="center">
<tr>
<td width="120px">Select Company</td>
<td>
<select id="companyId" name="companyId" onchange="return officeOptions(this.value, 1)">
<?
if($moduleAll == 1 or $moduleCompaniesViewAll == 1){
    $companySql = "select * from cui_companies where companyStatus='1' order by companyName ASC";
    echo '<option value="All">- All Companies -</option>';
}else{
    if($_SESSION["cuiSessionUserLevel"] <= 34)
	{
		$companySql = "select * from cui_companies where companyStatus='1' order by companyName ASC";
    	echo '<option value="All">- All Companies -</option>';
	}else{
		$companySql = "select distinct(a.companyId) from cui_companies as a join cui_users_companies as b where a.companyId=b.companyId and b.userId='$sessionId' order by a.companyName ASC";
		echo '<option value="">--- select ---</option>';
	}
}
//$companyResult = mysql_query($companySql);
$companyResult = mysqli_query($con, $companySql);
//while ($companyRs = mysql_fetch_array($companyResult)) {
while ($companyRs = @mysqli_fetch_array($companyResult)) {
    $rsCompanyId = $companyRs["companyId"];
    $companyName = getField("cui_companies","companyId",$rsCompanyId,"companyName");
    $sel = '';
    if($_SESSION["cuiSessionCompanyId"] == $rsCompanyId){
        $sel = "selected";
    }
    echo '<option '.$sel.' value="'.$rsCompanyId.'">'.$companyName.'</option>';
}
?>
</select>
</td>
</tr>
<tr>
<td>Select Office</td>
<td>
<select id="officeId" name="officeId">
<?
if($moduleAll == 1 or $moduleCompaniesViewAll == 1){
    echo '<option value="All">- All Offices -</option>';
}else{
    if($_SESSION["cuiSessionUserLevel"] <= 34)
	{
    	echo '<option value="All">- All Offices -</option>';
	}else{	
		echo '<option value="">--- select ---</option>';
	}
}
if($_SESSION["cuiSessionCompanyId"]){
    if($moduleAll == 1 or $moduleCompaniesViewAll == 1){
        $editSql = "SELECT * from cui_companies_offices where companyId='$sessionCompanyId'";
        }else{
        $editSql = "select a.companyId, b.officeId from cui_companies as a join cui_users_companies as b where a.companyId=b.companyId and b.userId='$sessionId' and a.companyId=".$sessionCompanyId." order by b.officeId ASC";
    }
//    $editResult = mysql_query($editSql);
    $editResult = mysqli_query($con, $editSql);
//    while ($editRs = mysql_fetch_array($editResult)) {
    while ($editRs = @mysqli_fetch_array($editResult)) {
        $rsofficeId = $editRs["officeId"];
        $officeName = getField("cui_companies_offices","officeId",$rsofficeId,"officeName");
        if($rsofficeId == 0){
            $rsofficeId = "All";
            $officeName = "- All Offices -";
            $getAllOfficesList = getCompanyOffices($sessionCompanyId);
            //add all offices
            $sel = '';
            if($sessionOfficeId == $rsofficeId){
                $sel = "selected";
            }
            //add below condition on Jun-30-2015 with CUI employees
			if($_SESSION["cuiSessionUserLevel"] > 34)
			{
				echo '<option '.$sel.' value="'.$rsofficeId.'">'.$officeName.'</option>';
			}
            //explode
            $getAllOfficesListEx = explode("|", $getAllOfficesList);
            foreach ($getAllOfficesListEx as $GAOL){
                //office name
                $getOfficeName = getField("cui_companies_offices","officeId",$GAOL,"officeName");
                $sel = '';
                if($sessionOfficeId == $GAOL){
                    $sel = "selected";
                }
                echo '<option '.$sel.' value="'.$GAOL.'">'.$getOfficeName.'</option>';
            }
            //skip loop
            break;
        }
        $sel = '';
        if($_SESSION["cuiSessionOfficeId"] == $rsofficeId){
            $sel = "selected";
        }
        echo '<option '.$sel.' value="'.$rsofficeId.'">'.$officeName.'</option>';
    }
}?>
</select>
</td>
</tr>
<tr>
<td>Set As Default</td>
<td>
<?php /*?><input type="checkbox" id="chkDefault" name="chkDefault" value="1" <? if($userDefault==1){ ?> checked="checked" <? } ?> /><?php */?>
<input type="radio" name="chkDefault" id="chkDefault" value="1" <? if($userDefault==1){ ?> checked="checked" <? } ?> /> Yes
<input type="radio" name="chkDefault" id="chkDefault" value="0" <? if($userDefault!=1){ ?> checked="checked" <? } ?> /> No
<input type="radio" name="chkDefault" id="chkDefault" value="2" /> Reset
</td>
</tr>
<tr>
<td>&nbsp;</td>
<td><input type="submit" value="Submit" name="pagesubmit" class="submit" onclick="return chkForm()" /></td>
</tr>
</table>
</form>
</td>
</tr>
</table>
</div>