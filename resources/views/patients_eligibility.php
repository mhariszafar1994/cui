<style>
@import url('https://fonts.googleapis.com/css?family=Open+Sans:400,700');
table{font-family: 'Open Sans', sans-serif !important;}
hr {
	border-bottom: 0px;
}
table.form-spacing tbody tr td {
	padding-bottom: 9px;
}
table.form-spacing tbody tr td label {
	margin-bottom: 0px;
    margin-top: -15px;
    position: unset;
    top: 11px;
}
tr.titleTr label {
    padding:  5px 10px 0px;
    position:  relative;
    top: 6px;
}

tr.titleTr td {
    padding-bottom:  0px !important;
    padding: 8px 0px!important;
}

td.left-table-sidebar {
    padding-right: 10px !important;
}

td.left-table-sidebar tr.titleTr {
    padding: 0px !important;
}

td.left-table-sidebar tr.titleTr td {
    padding: 7px 10px !important;
}

td.left-table-sidebar td.leftMenuTd {
    padding-bottom: 0px;
    border: 0px;
    border-bottom: 1px solid #ccc;
    border: 1px solid #ccc;
}

td.left-table-sidebar td.leftMenuTd a {
    display: block;
    padding: 5px 10px !important;
}
td, th {
    padding: 2px 3px !important;
}
table.form-spacing {
    font-size: 12px;
}
</style>
<div id="myCustomLoader" style="position: fixed; top: 0px; left: 0px; width: 100%; height: 100%; background-color: rgba(255,255,255,0.7); z-index: 9999999999; display: none;">
<img src="images/^EA5FC4EF9512393471AB8DF0FC86BA17401514F25D2723CED9^tfile_urlpv.gif" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
</div>
<?php if(isset($_GET["faizan"])){?>
<table cellpadding="5" cellspacing="0" border="1">
<tr>
	<td>Patient ID</td>
	<td>Name</td>
	<td>Group ID (Detail TBL)</td>
	<td>Group ID (Breakdown TBL)</td>
	
</tr>
<?
$sql = "SELECT p.*, c.custGroupNum FROM cui_patients_custom_db AS c JOIN cui_patients AS p WHERE p.patientId = c.patientId AND (custGroupNum!='' OR patientGroup!='') ORDER BY patientFname ASC";
$result = mysqli_query($con, $sql);
while ($rs = mysqli_fetch_array($result)){
	$pid = $rs["patientId"];
	$pGroup1 = $rs["patientGroup"];
	$pGroup2 = $rs["custGroupNum"];
	$pName= $rs["patientFname"]. " ".$rs["patientLname"];
	
	?>
	
		<tr>
			<td><?php echo $pid;?></td>
			<td><?php echo $pName;?></td>
			<td><?php echo $pGroup1;?> (<?php echo preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', encrypt_decrypt('decrypt', $pGroup1));;?>)</td>
			<td><?php echo $pGroup2;?></td>
		</tr>
	
	<?
}
?>
</table>
<?
}
//echo "<pre>";
//print_r($_SESSION);
//die();
//roles check
if($moduleAll == 0 and $modulePatientsView == 0 and ($modulePatientsEdit == 0 and $modulePatientsCheck == 0)){
	echo "<script>window.location='index.php?do=authorization'</script>";
}
//Company All checker - start
if($_SESSION["cuiSessionCompanyId"] == "All")
{
	if($_GET["patientId"] != "")
	{
		$tmpPatientId = sanitize($_GET["patientId"]);
		$tmpSql = "Select companyId, officeId, doctorId from cui_patients where patientId='$tmpPatientId'";
//		$tmpRs = mysql_query($tmpSql);
		$tmpRs = mysqli_query($con, $tmpSql);
		//
//		if(mysql_num_rows($tmpRs) > 0)
		if(@mysqli_num_rows($tmpRs) > 0)
		{
//			$compId = mysql_result($tmpRs, 0, "companyId");
			$compId = mysqli_result($tmpRs, 0, "companyId");
//			$offId = mysql_result($tmpRs, 0, "officeId");
			$offId = mysqli_result($tmpRs, 0, "officeId");
			$_SESSION["tmpSessionCompanyId"] = $compId;
			$_SESSION["tmpSessionOfficeId"] = $offId;
			//for custom tag
			$tmpCustCompName = getField("cui_companies","companyId",$compId,"companyCompanyName"," and companyCustom='1'");
			$tmpCustCompTag = getField("cui_custom_companies","custCompanyName",$tmpCustCompName,"custCompanyTag"," and custCompanyStatus='1'");
			$_SESSION["tmpSessionCompanyTag"] = $tmpCustCompTag;
		}
	}
}else{

	$_SESSION["tmpSessionCompanyId"] = $_SESSION["cuiSessionCompanyId"];
	$_SESSION["tmpSessionOfficeId"] = $_SESSION["cuiSessionOfficeId"];
	$_SESSION["tmpSessionCompanyTag"] = $_SESSION["cuiSessionCompanyTag"];
}
//Company All checker - end
//die($_SESSION["tmpSessionCompanyId"]);
$checkCustom = getField("cui_companies","companyId",$_SESSION["tmpSessionCompanyId"],"companyCustom"," and companyStatus='1'");
//if get patientStatus
$patientStatus = '';
if($_GET["patientStatus"]){
	$patientStatus = sanitize($_GET["patientStatus"]);
}
//vars
if($checkCustom == "1")
{
	$patientEligibilityCustom = 1;

}else{
	$patientEligibilityCustom = 0;
}
$patientEligibilityFull = 0;
$patientEligibilityPartial = 0;
$patientEligibilityOrtho = 0;
// insert / update values
if($_POST)
{
	/*echo "<pre>";
	print_r($_POST);
	echo "<br><br>";*/
	$patientId = sanitize($_GET["patientId"]);
	//assign all posts to vars
	foreach ($_POST as $key=>$value){
		if(!is_numeric($key) and $key!=""){
			//remove commas
			$value = str_replace("'", "", $value);
			//initialize variables using eval
			if($key == "custSpecialRequest")
			{
				eval("$".$key." = '".$value."';");
			}else{
				eval("$".$key." = '".sanitize($value)."';");
			}
		}
	}
	//to exclude
	$excludeCounter=-1;
	$excludeArray[$excludeCounter+=1] = "submit";
	//added by Noman - May-18-2015 - because of Status Report form
	$excludeArray[$excludeCounter+=1] = "srFullCustom";
	$excludeArray[$excludeCounter+=1] = "srPartial";
	$excludeArray[$excludeCounter+=1] = "srOrtho";
	$excludeArray[$excludeCounter+=1] = "srCodes";
	$excludeArray[$excludeCounter+=1] = "srSpecialRequest";
	$excludeArray[$excludeCounter+=1] = "srIncorrectInfo";
	$excludeArray[$excludeCounter+=1] = "srCreatePatient";
	$excludeArray[$excludeCounter+=1] = "srRemotzAccess";
	$excludeArray[$excludeCounter+=1] = "srTimeSpent";
	$excludeArray[$excludeCounter+=1] = "srRemarks";
	$excludeArray[$excludeCounter+=1] = "custGroupNum";
	//start adding Status Report
	if($srTimeSpent != "")
	{
		$patientName = getField("cui_patients","patientId",$patientId,"patientFname")." ".getField("cui_patients","patientId",$patientId,"patientLname");
		$userId = $_SESSION[$sessionUserID];
		$srCompanyId = $_SESSION["tmpSessionCompanyId"];
		//
		if($_GET["patientId"] != "")
		{
			$tmpPatientId = sanitize($_GET["patientId"]);
			$tmpSql = "Select companyId, officeId, doctorId from cui_patients where patientId='$tmpPatientId'";
//			$tmpRs = mysql_query($tmpSql);
			$tmpRs = mysqli_query($con, $tmpSql);
			//
//			if(mysql_num_rows($tmpRs) > 0)
			if(@mysqli_num_rows($tmpRs) > 0)
			{
//				$srOfficeId = mysql_result($tmpRs, 0, "officeId");
				$srOfficeId = mysqli_result($tmpRs, 0, "officeId");
			}
		}
		$currDate = date("Y-m-d H:i:s");
		if(!$srFullCustom)
		{
			$srFullCustom=0;
		}
		if(!$srPartial)
		{
			$srPartial=0;
		}
		if(!$srOrtho)
		{
			$srOrtho=0;
		}
		if(!$srCodes)
		{
			$srCodes=0;
		}
		if(!$srSpecialRequest)
		{
			$srSpecialRequest=0;
		}
		if(!$srIncorrectInfo)
		{
			$srIncorrectInfo=0;
		}
		if(!$srCreatePatient)
		{
			$srCreatePatient=0;
		}
		if(!$srRemotzAccess)
		{
			$srRemotzAccess=0;
		}
		$sqlSR = "Insert into cui_patients_status_report (srPatientId,srPatientName,srUserId,srCompanyId,srOfficeId,srFullCustom,srPartial,srOrtho,srCodes,srSpecialRequest,srIncorrectInfo,srCreatePatient,srRemotzAccess,srTimeSpent,srRemarks,srDateTime) 
				  values('$patientId','$patientName','$userId','$srCompanyId','$srOfficeId','$srFullCustom','$srPartial','$srOrtho','$srCodes','$srSpecialRequest','$srIncorrectInfo','$srCreatePatient','$srRemotzAccess','$srTimeSpent','$srRemarks','$currDate')";
//		mysql_query($sqlSR);
		mysqli_query($con, $sqlSR);
	}
	
	//If group number not empty
	//start adding Status Report
	if($custGroupNum != "")
	{
		//Insert into patient original table
		$custGroupNum = encrypt_decrypt('encrypt', $custGroupNum);
		mysqli_query($con, "UPDATE cui_patients SET patientGroup = '$custGroupNum' WHERE patientId= '$patientId'");
	}
	
	// to include
	$_POST["patientId"] = $patientId;
	//checking for table name
	if($patientEligibilityCustom == 1)
	{
		$tblName = "cui_patients_".$_SESSION["tmpSessionCompanyTag"];

		//Fields to check
		$checkBoxFieldArray = array(
			'custYearlyApplies1',
			'custYearlyApplies2',
			'custYearlyApplies3',
			'custDeductApplies1',
			'custDeductApplies2',
			'custDeductApplies3',
			'custDeductApplies4'
		);

		//Run loop get all fields from database table in an array for faster code execution
		$columnSql = "SHOW COLUMNS FROM $tblName";
//		$columnResult = mysql_query($columnSql);
		$columnResult = mysqli_query($con, $columnSql);
//		if(mysql_num_rows($columnResult)>0){
		if(@mysqli_num_rows($columnResult)>0){
			//Running loop of all fields
//		    while ($columnRsArray = mysql_fetch_array($columnResult)) {
			while ($columnRsArray = @mysqli_fetch_array($columnResult)) {
				if(in_array($columnRsArray['Field'], $checkBoxFieldArray)){
					if(!$_POST[$columnRsArray['Field']]){
						eval('$_POST['.$columnRsArray['Field'].'] = "";');
					}
				}
			}
		}

	}else{
		$tblName = "cui_patients_full";
	}


	// check if to insert or update
	$sqlPatients = "SELECT * FROM ".$tblName." WHERE patientId='$patientId'";
//    $resultPatients = mysql_query($sqlPatients);
	$resultPatients = mysqli_query($con, $sqlPatients);
//	if(mysql_num_rows($resultPatients)>0)
	if(@mysqli_num_rows($resultPatients)>0)
	{
		/*var_dump("<pre>",$_POST);
        exit;*/
		$updateFunction = fastInsertUpdate("update",$tblName,$_POST, $excludeArray," WHERE patientId='$patientId'");
		if($updateFunction == "success"){
			$redirect = 1;
			$msg = "Patient data has been updated!";
		}else{
			$error = $updateFunction;
		}
	}else{
		//var_dump("<pre>",$_POST);
		//exit;
		$patientInsert = fastInsertUpdate("insert",$tblName,$_POST, $excludeArray,"",1);
		if($patientInsert>0){
			$redirect = 1;
			$msg = "Patient data has been saved!";
		}else{
			$error = $insertFunction;
		}
	}
	if ($_POST["submit"] == "Save & Submit") {
		//main table vars
		$patientEligibility = getField("cui_patients","patientId",$patientId,"patientEligibility");
		$patientOrtho = getField("cui_patients","patientId",$patientId,"patientOrtho");
		if($patientEligibility != 'Custom')
		{
			$customStatus = 0;
			//see if full or partial
			if($patientEligibility == 'Full'){
				$fullStatus = 1;
				$partialStatus = 0;
			}else{
				$partialStatus = 1;
				$fullStatus = 0;
			}
			//see if ortho needs to be there
			if($patientOrtho == "Yes"){
				$orthoStatus = 1;
			}else{
				$orthoStatus = 0;
			}
		}else{
			if($patientEligibility == 'Custom')
			{
				$customStatus = 1;
				$fullStatus = 0;
				$partialStatus = 0;
				$orthoStatus = 0;
			}else{
				$customStatus = 0;
				$fullStatus = 0;
				$partialStatus = 0;
				$orthoStatus = 0;
			}
		}
		//sending email to exiting patient after Save & Submit only
		// additional header pieces for errors, From cc's, bcc's, etc
		$headers = "From: Checkurinsurance <info@checkurinsurance.com>\r\n";
		$headers.="Content-type: text/html; charset=iso-8859-1\r\n";
		//subject
		$patientFname = getField("cui_patients","patientId",$patientId,"patientFname");
		$patientLname = getField("cui_patients","patientId",$patientId,"patientLname");
		$officeId = getField("cui_patients","patientId",$patientId,"officeId");
		$companyId = getField("cui_patients","patientId",$patientId,"companyId");
		$updatedBy = getField("cui_users","userId",$_SESSION[$sessionUserID],"userFname")." ".getField("cui_users","userId",$_SESSION[$sessionUserID],"userLname");
		if(getField("cui_patients","patientId",$patientId,"patientAppDate") != "" && getField("cui_patients","patientId",$patientId,"patientAppDate") != "0000-00-00 00:00:00")
		{
			$patientAppDate = date("m/d/Y", strtotime(getField("cui_patients","patientId",$patientId,"patientAppDate")));
			$patientAppTime = date("H:i:s a", strtotime(getField("cui_patients","patientId",$patientId,"patientAppDate")));
		}else{
			$patientAppDate = "00/00/0000";
			$patientAppTime = "00:00:00";
		}
		$subject = "Patient '".$patientFname." ".$patientLname." (".getField("cui_companies","companyId",$companyId,"companyName")." - ".getField("cui_companies_offices","officeId",$officeId,"officeName").")' Status Has Been Changed to Updated - ".$patientAppDate." ".$patientAppTime;
		//$toEmail = "info@checkurinsurance.com";
		//$toEmail = "nomanmustafa.khan@gmail.com";
		//message
		$message = "Dear Admin,<br><br>";
		$message .= "The patient <b>".$patientFname." ".$patientLname." (ID: ".$patientId.")</b> status has been changed to Updated<br>";
		$message .= "Appointment Date/Time:  ".$patientAppDate." ".$patientAppTime."<br>";
		$message .= "Updated By:  ".$updatedBy."<br>";
		$message .= "Updated Date/Time:  ".date('m/d/Y  H:i a')." PDT.<br><br>";
		$message .= $httpHost;
		//$message .= "Checkurinsurance.com";
		mail($toEmail,$subject,$message,$headers);
		//additinal email sending to Company users
		$companyEmails = getField("cui_companies","companyId",$_SESSION["tmpSessionCompanyId"],"companyRecipientEmails"," and companyStatus='1'");
		$tmpSplit = explode(",", $companyEmails);
		if(count($tmpSplit) > 0)
		{
			for($s=0; $s<count($tmpSplit); $s++)
			{
				mail($tmpSplit[$s],$subject,$message,$headers);
			}
		}
		//echo $subject."<br><br>";
		//echo $message; exit;
		//get history id // and then add to history table
		/*$historyId=getField("cui_patients_history","patientId",$patientId,"historyId","order by historyId desc limit 0,1");
        $updateHistory = mysql_query("UPDATE cui_patients_history SET customStatus='$customStatus', fullStatus='$fullStatus', partialStatus='$partialStatus', orthoStatus='$orthoStatus', updaterId='$_SESSION[cuiSessionUserId]', updatedDate='".time()."' WHERE patientId='$patientId' ANd historyId='$historyId'");*/
		if($patientEligibility == "Custom")
		{
			$custom = 1;
		}else{
			$custom = 0;
		}
		if($patientEligibility == "Partial")
		{
			$partial = 1;
			$full = 0;
		}else{
			$full = 1;
			$partial = 0;
		}
		if($patientOrtho == "Yes")
		{
			$ortho = 1;
		}else{
			$ortho = 0;
		}
		//creating REMARKS - start
		$remarks = "";
		$ramarksEligibility = "";
		if($_SESSION["tmpSessionCompanyId"] == "16" 		|| $_SESSION["tmpSessionCompanyId"] == "20" 				
		|| $_SESSION["tmpSessionCompanyId"] == "21"  		|| $_SESSION["tmpSessionCompanyId"] == "22" 				
		|| $_SESSION["tmpSessionCompanyId"] == "27" 		 				
		|| $_SESSION["tmpSessionCompanyId"] == "29" 		|| $_SESSION["tmpSessionCompanyId"] == "30" 				
		|| $_SESSION["tmpSessionCompanyId"] == "33"		
		|| $_SESSION["tmpSessionCompanyId"] == "34" 		|| $_SESSION["tmpSessionCompanyId"] == "83"		
		|| $_SESSION["tmpSessionCompanyId"] == "88" 
		|| $_SESSION["tmpSessionCompanyId"] == "103"		
		|| $_SESSION["tmpSessionCompanyId"] == "113" 		|| $_SESSION["tmpSessionCompanyId"] == "114"
		|| $_SESSION["tmpSessionCompanyId"] == "115"		
		|| $_SESSION["tmpSessionCompanyId"] == "118"		|| $_SESSION["tmpSessionCompanyId"] == "119"
		|| $_SESSION["tmpSessionCompanyId"] == "120"		|| $_SESSION["tmpSessionCompanyId"] == "121"
		|| $_SESSION["tmpSessionCompanyId"] == "122"		
		|| $_SESSION["tmpSessionCompanyId"] == "124"		|| $_SESSION["tmpSessionCompanyId"] == "125"
		|| $_SESSION["tmpSessionCompanyId"] == "126"		|| $_SESSION["tmpSessionCompanyId"] == "127"
		|| $_SESSION["tmpSessionCompanyId"] == "128"		|| $_SESSION["tmpSessionCompanyId"] == "130"		
		|| $_SESSION["tmpSessionCompanyId"] == "131"
		|| $_SESSION["tmpSessionCompanyId"] == "132"		|| $_SESSION["tmpSessionCompanyId"] == "133"
		|| $_SESSION["tmpSessionCompanyId"] == "134"		|| $_SESSION["tmpSessionCompanyId"] == "135"
		|| $_SESSION["tmpSessionCompanyId"] == "136"
		|| $_SESSION["tmpSessionCompanyId"] == "137"
		|| $_SESSION["tmpSessionCompanyId"] == "138"
		|| $_SESSION["tmpSessionCompanyId"] == "139"
		|| $_SESSION["tmpSessionCompanyId"] == "140"
		|| $_SESSION["tmpSessionCompanyId"] == "141"
		|| $_SESSION["tmpSessionCompanyId"] == "142"
		|| $_SESSION["tmpSessionCompanyId"] == "143"
		|| $_SESSION["tmpSessionCompanyId"] == "144"
		|| $_SESSION["tmpSessionCompanyId"] == "145"
		|| $_SESSION["tmpSessionCompanyId"] == "146"
		|| $_SESSION["tmpSessionCompanyId"] == "147"
		|| $_SESSION["tmpSessionCompanyId"] == "148"
		|| $_SESSION["tmpSessionCompanyId"] == "149"
		|| $_SESSION["tmpSessionCompanyId"] == "150" 
		|| $_SESSION["tmpSessionCompanyId"] == "151"
		|| $_SESSION["tmpSessionCompanyId"] == "152"
		|| $_SESSION["tmpSessionCompanyId"] == "153"
		|| $_SESSION["tmpSessionCompanyId"] == "154"
		|| $_SESSION["tmpSessionCompanyId"] == "155"
		|| $_SESSION["tmpSessionCompanyId"] == "156"
		|| $_SESSION["tmpSessionCompanyId"] == "157"
		|| $_SESSION["tmpSessionCompanyId"] == "158" 
		|| $_SESSION["tmpSessionCompanyId"] == "159" 
		|| $_SESSION["tmpSessionCompanyId"] == "160"
		|| $_SESSION["tmpSessionCompanyId"] == "161"
		|| $_SESSION["tmpSessionCompanyId"] == "162"
		|| $_SESSION["tmpSessionCompanyId"] == "163"
		|| $_SESSION["tmpSessionCompanyId"] == "164"
		|| $_SESSION["tmpSessionCompanyId"] == "165"
		|| $_SESSION["tmpSessionCompanyId"] == "166"
		|| $_SESSION["tmpSessionCompanyId"] == "167"
		|| $_SESSION["tmpSessionCompanyId"] == "168"
		|| $_SESSION["tmpSessionCompanyId"] == "169"
		|| $_SESSION["tmpSessionCompanyId"] == "170"
		|| $_SESSION["tmpSessionCompanyId"] == "172"
		|| $_SESSION["tmpSessionCompanyId"] == "173"
		|| $_SESSION["tmpSessionCompanyId"] == "174"
		|| $_SESSION["tmpSessionCompanyId"] == "175"
		|| $_SESSION["tmpSessionCompanyId"] == "176"
		|| $_SESSION["tmpSessionCompanyId"] == "177"
		|| $_SESSION["tmpSessionCompanyId"] == "178") 
		{
			$ramarksEligibility .= " for";
			if ($patientEligibility == "Custom")
			{
				$ramarksEligibility .= " Custom";
			}
			if($patientEligibility == "Partial")
			{
				$ramarksEligibility .= " Partial";
			}
			$ramarksEligibility .= " bd";
		}else{
			if ($patientEligibility == "Custom")
			{
				$ramarksEligibility .= " for Custom bd";
			}else{
				if($patientEligibility != "" || $patientOrtho != "")
				{
					$ramarksEligibility .= " for";
					if($patientEligibility == "Full")
					{
						if($patientOrtho == "Yes")
						{
							$ramarksEligibility .= " Full + ";
						}else{
							$ramarksEligibility .= " Full";
						}
					}
					if($patientEligibility == "Partial"){
						if($patientOrtho == "Yes")
						{
							$ramarksEligibility .= " Partial + ";
						}else{
							$ramarksEligibility .= " Partial";
						}
					}
					if($patientOrtho == "Yes"){
						$ramarksEligibility .= " Ortho";
					}
					$ramarksEligibility .= " bd";
				}else{
					$ramarksEligibility .= "";
				}
			}
		}

		//creating REMARKS - end
		//add history
		$remarks = "Patient changed from ".$patientStatus." to Updated".$ramarksEligibility;
		//$sqlHistory = "INSERT INTO cui_patients_history (patientId, custom, full, partial, ortho, checkerId, checkedDate,remarks) values ('$patientId', '$custom', '$full', '$partial', '$ortho', '$_SESSION[$sessionUserID]', '".time()."','$remarks')";
		//$sqlHistory = "INSERT INTO cui_patients_history (patientId, custom, full, partial, ortho, checkerId, checkedDate, updaterId, updatedDate, remarks) values ('$patientId', '$custom', '$full', '$partial', '$ortho', '$_SESSION[$sessionUserID]', '".time()."','$_SESSION[cuiSessionUserId]', '".time()."', '$remarks')";
		$sqlHistory = "INSERT INTO cui_patients_history (patientId, custom, customStatus, full, fullStatus, partial, partialStatus, ortho, orthoStatus, checkerId, checkedDate, updaterId, updatedDate, remarks) values ('$patientId', '$custom', '$customStatus', '$full', '$fullStatus', '$partial', '$partialStatus', '$ortho', '$orthoStatus', '$_SESSION[$sessionUserID]', '".time()."','$_SESSION[cuiSessionUserId]', '".time()."', '$remarks')";
//	    mysql_query($sqlHistory);
		mysqli_query($con, $sqlHistory);
		$dateTimeUpdated = date("Y-m-d H:i:s");
//        $updateHistory = mysql_query("UPDATE cui_patients SET patientStatus='Updated', patientUpdated='$dateTimeUpdated' WHERE patientId='$patientId'");
		$updateHistory = mysqli_query($con, "UPDATE cui_patients SET patientStatus='Updated', patientUpdated='$dateTimeUpdated' WHERE patientId='$patientId'");
//        $updateHistory = mysql_query("UPDATE cui_patients SET patientCustomDone='1' WHERE patientId='$patientId' AND patientEligibility='Custom'");
		$updateHistory = mysqli_query($con, "UPDATE cui_patients SET patientCustomDone='1' WHERE patientId='$patientId' AND patientEligibility='Custom'");
//		$updateHistory = mysql_query("UPDATE cui_patients SET patientFullDone='1' WHERE patientId='$patientId' AND patientEligibility='Full'");
		$updateHistory = mysqli_query($con, "UPDATE cui_patients SET patientFullDone='1' WHERE patientId='$patientId' AND patientEligibility='Full'");
//        $updateHistory = mysql_query("UPDATE cui_patients SET patientPartialDone='1' WHERE patientId='$patientId' AND patientEligibility='Partial'");
		$updateHistory = mysqli_query($con, "UPDATE cui_patients SET patientPartialDone='1' WHERE patientId='$patientId' AND patientEligibility='Partial'");
//        $updateHistory = mysql_query("UPDATE cui_patients SET patientOrthoDone='1' WHERE patientId='$patientId' AND patientOrtho='Yes'");
		$updateHistory = mysqli_query($con, "UPDATE cui_patients SET patientOrthoDone='1' WHERE patientId='$patientId' AND patientOrtho='Yes'");
		echo "<script>window.location='index.php?do=patients&patientId=$patientId&patientStatus=$patientStatus&msg=Patient has been updated!'</script>";
		exit;
	} elseif ($_POST["submit"] == "Save") {
		//main table vars
		$patientEligibility = getField("cui_patients","patientId",$patientId,"patientEligibility");
		$patientOrtho = getField("cui_patients","patientId",$patientId,"patientOrtho");
		if($patientEligibility != 'Custom')
		{
			$customStatus = 0;
			//see if full or partial
			if($patientEligibility == 'Full'){
				$fullStatus = 1;
				$partialStatus = 0;
			}else{
				$partialStatus = 1;
				$fullStatus = 0;
			}
			//see if ortho needs to be there
			if($patientOrtho == "Yes"){
				$orthoStatus = 1;
			}else{
				$orthoStatus = 0;
			}
		}else{
			if($patientEligibility == 'Custom')
			{
				$customStatus = 1;
				$fullStatus = 0;
				$partialStatus = 0;
				$orthoStatus = 0;
			}else{
				$customStatus = 0;
				$fullStatus = 0;
				$partialStatus = 0;
				$orthoStatus = 0;
			}
		}
		if($patientEligibility == "Custom")
		{
			$custom = 1;
		}else{
			$custom = 0;
		}
		if($patientEligibility == "Partial")
		{
			$partial = 1;
			$full = 0;
		}else{
			$full = 1;
			$partial = 0;
		}
		if($patientOrtho == "Yes")
		{
			$ortho = 1;
		}else{
			$ortho = 0;
		}
		//creating REMARKS - start
		$remarks = "";
		$ramarksEligibility = "";
		if($_SESSION["tmpSessionCompanyId"] == "16" 		|| $_SESSION["tmpSessionCompanyId"] == "20" 				
		|| $_SESSION["tmpSessionCompanyId"] == "21"  		|| $_SESSION["tmpSessionCompanyId"] == "22" 				
		|| $_SESSION["tmpSessionCompanyId"] == "27" 		 				
		|| $_SESSION["tmpSessionCompanyId"] == "29" 		|| $_SESSION["tmpSessionCompanyId"] == "30" 				
 		|| $_SESSION["tmpSessionCompanyId"] == "33"		
		|| $_SESSION["tmpSessionCompanyId"] == "34" 		|| $_SESSION["tmpSessionCompanyId"] == "83"		
		|| $_SESSION["tmpSessionCompanyId"] == "88" 	
		|| $_SESSION["tmpSessionCompanyId"] == "103"		
		|| $_SESSION["tmpSessionCompanyId"] == "113" 		|| $_SESSION["tmpSessionCompanyId"] == "114"
		|| $_SESSION["tmpSessionCompanyId"] == "115"		
		|| $_SESSION["tmpSessionCompanyId"] == "118"		|| $_SESSION["tmpSessionCompanyId"] == "119"
		|| $_SESSION["tmpSessionCompanyId"] == "120"		|| $_SESSION["tmpSessionCompanyId"] == "121"
		|| $_SESSION["tmpSessionCompanyId"] == "122"		
		|| $_SESSION["tmpSessionCompanyId"] == "124"		|| $_SESSION["tmpSessionCompanyId"] == "125"
		|| $_SESSION["tmpSessionCompanyId"] == "126"		|| $_SESSION["tmpSessionCompanyId"] == "127"
		|| $_SESSION["tmpSessionCompanyId"] == "128"		|| $_SESSION["tmpSessionCompanyId"] == "130"		
		|| $_SESSION["tmpSessionCompanyId"] == "131"
		|| $_SESSION["tmpSessionCompanyId"] == "132"		|| $_SESSION["tmpSessionCompanyId"] == "133"
		|| $_SESSION["tmpSessionCompanyId"] == "134"		|| $_SESSION["tmpSessionCompanyId"] == "135"
		|| $_SESSION["tmpSessionCompanyId"] == "136"
		|| $_SESSION["tmpSessionCompanyId"] == "137"
		|| $_SESSION["tmpSessionCompanyId"] == "138"
		|| $_SESSION["tmpSessionCompanyId"] == "139"
		|| $_SESSION["tmpSessionCompanyId"] == "140"
		|| $_SESSION["tmpSessionCompanyId"] == "141"
		|| $_SESSION["tmpSessionCompanyId"] == "142"
		|| $_SESSION["tmpSessionCompanyId"] == "143"
		|| $_SESSION["tmpSessionCompanyId"] == "144"
		|| $_SESSION["tmpSessionCompanyId"] == "145"
		|| $_SESSION["tmpSessionCompanyId"] == "146"
		|| $_SESSION["tmpSessionCompanyId"] == "147"
		|| $_SESSION["tmpSessionCompanyId"] == "148"
		|| $_SESSION["tmpSessionCompanyId"] == "149"
		|| $_SESSION["tmpSessionCompanyId"] == "150"
		|| $_SESSION["tmpSessionCompanyId"] == "151"
		|| $_SESSION["tmpSessionCompanyId"] == "152"
		|| $_SESSION["tmpSessionCompanyId"] == "153"
		|| $_SESSION["tmpSessionCompanyId"] == "154"
		|| $_SESSION["tmpSessionCompanyId"] == "155"
		|| $_SESSION["tmpSessionCompanyId"] == "156"
		|| $_SESSION["tmpSessionCompanyId"] == "157"
		|| $_SESSION["tmpSessionCompanyId"] == "158" 
		|| $_SESSION["tmpSessionCompanyId"] == "159" 
		|| $_SESSION["tmpSessionCompanyId"] == "160"
		|| $_SESSION["tmpSessionCompanyId"] == "161"
		|| $_SESSION["tmpSessionCompanyId"] == "162"
		|| $_SESSION["tmpSessionCompanyId"] == "163"
		|| $_SESSION["tmpSessionCompanyId"] == "164"
		|| $_SESSION["tmpSessionCompanyId"] == "165"
		|| $_SESSION["tmpSessionCompanyId"] == "166"
		|| $_SESSION["tmpSessionCompanyId"] == "167"
		|| $_SESSION["tmpSessionCompanyId"] == "168"
		|| $_SESSION["tmpSessionCompanyId"] == "169"
		|| $_SESSION["tmpSessionCompanyId"] == "170"
		|| $_SESSION["tmpSessionCompanyId"] == "172"
		|| $_SESSION["tmpSessionCompanyId"] == "173"
		|| $_SESSION["tmpSessionCompanyId"] == "174"
		|| $_SESSION["tmpSessionCompanyId"] == "175"
		|| $_SESSION["tmpSessionCompanyId"] == "176"
		|| $_SESSION["tmpSessionCompanyId"] == "177"
		|| $_SESSION["tmpSessionCompanyId"] == "178")
		{
			$ramarksEligibility .= " for";
			if ($patientEligibility == "Custom")
			{
				$ramarksEligibility .= " Custom";
			}
			if($patientEligibility == "Partial")
			{
				$ramarksEligibility .= " Partial";
			}
			$ramarksEligibility .= " bd";
		}else{
			if ($patientEligibility == "Custom")
			{
				$ramarksEligibility .= " for Custom bd";
			}else{
				if($patientEligibility != "" || $patientOrtho != "")
				{
					$ramarksEligibility .= " for";
					if($patientEligibility == "Full")
					{
						if($patientOrtho == "Yes")
						{
							$ramarksEligibility .= " Full + ";
						}else{
							$ramarksEligibility .= " Full";
						}
					}
					if($patientEligibility == "Partial"){
						if($patientOrtho == "Yes")
						{
							$ramarksEligibility .= " Partial + ";
						}else{
							$ramarksEligibility .= " Partial";
						}
					}
					if($patientOrtho == "Yes"){
						$ramarksEligibility .= " Ortho";
					}
					$ramarksEligibility .= " bd";
				}else{
					$ramarksEligibility .= "";
				}
			}
		}
		//$remarks = "Patient changed from ".$patientStatus." to Updated".$ramarksEligibility;
		$remarks = "Patient data saved";
		$sqlHistory = "INSERT INTO cui_patients_history (patientId, custom, customStatus, full, fullStatus, partial, partialStatus, ortho, orthoStatus, checkerId, checkedDate, updaterId, updatedDate, remarks) values ('$patientId', '$custom', '$customStatus', '$full', '$fullStatus', '$partial', '$partialStatus', '$ortho', '$orthoStatus', '$_SESSION[$sessionUserID]', '".time()."','$_SESSION[cuiSessionUserId]', '".time()."', '$remarks')";
		mysqli_query($con, $sqlHistory);
	}
}
//get vars
if($_GET["patientId"]){
	$patientId = sanitize($_GET["patientId"]);
	//main table vars
	$patientEligibility = getField("cui_patients","patientId",$patientId,"patientEligibility");
	$patientOrtho = getField("cui_patients","patientId",$patientId,"patientOrtho");
	//see if full or partial
	if($patientEligibility != 'Custom' || $checkCustom == "0")
	{
		$patientEligibilityCustom = 0;
		if($patientEligibility == 'Full'){
			$patientEligibilityFull = 1;
		}else{
			$patientEligibilityPartial = 1;
		}
		//see if ortho needs to be there
		if($patientOrtho == "Yes"){
			$patientEligibilityOrtho = 1;
		}
	}else{
		$patientEligibilityCustom = 1;
		$patientEligibilityFull = 0;
		$patientEligibilityPartial = 0;
		$patientEligibilityOrtho = 0;
	}
	if($checkCustom == 1)
	{
		$tblName = "cui_patients_".$_SESSION["tmpSessionCompanyTag"];
	}else{
		$tblName = "cui_patients_full";
	}
		
	$sqlPatients = "SELECT * FROM ".$tblName." WHERE patientId='$patientId'";
//    $resultPatients = mysql_query($sqlPatients);
	$resultPatients = mysqli_query($con, $sqlPatients);
//    if(mysql_num_rows($resultPatients)>0){
	if(@mysqli_num_rows($resultPatients)>0){
//        $rspatients = mysql_fetch_array($resultPatients);
		$rspatients = @mysqli_fetch_array($resultPatients, MYSQLI_ASSOC);
		//foreach loop for all fields
		foreach ($rspatients as $key=>$value){
			//the above array brings numbers unwanted
			if(!is_numeric($key) and $key!=""){
				//remove commas
				$value = str_replace("'", "", $value);
				//initialize variables using eval
				eval("$".$key." = '".$value."';");
			}
		}
		//conditions and restrictions
	}
	$sqlPatientss = "SELECT patientGroup FROM cui_patients WHERE patientId='$patientId'";
	$resultPatientss = mysqli_query($con, $sqlPatientss);
	if(@mysqli_num_rows($resultPatientss)>0){
		$rspatientss = @mysqli_fetch_array($resultPatientss, MYSQLI_ASSOC);
		$custGroupNum = trim(encrypt_decrypt('decrypt', $rspatientss['patientGroup']));
	} else {
		$custGroupNum = '';
	}
}else{

	echo "<script>window.location='index.php?do=patients&patientStatus='.$patientStatus</script>";
}
?>

<textarea id="htmlpdf" name="htmlpdf" style="display:none;"></textarea>
<script type="text/javascript">
	var allEle = {},
		isDeleteAll = false;
	$(document).ready(function(){
		<?php if($patientEligibilityCustom == 1): ?>
		isDeleteAll = true;
		<?php else: ?>
		checkToDelete();
		<?php endif; ?>
	});
	
	function checkToDelete() {
		allEle = {};
		if($('.leftMenuTd').length > 1){
			$('.leftMenuSelected').parent().children().each(function(i, e){
				getAllActiveFields($(e).attr('data-targetEle'));
			});
		} else {
			// delete all thing
			getAllActiveFields($('.leftMenuTd a').attr('data-targetEle'));
		}
	}

	function addDataInEleObject(e){
		if(e.hasAttribute("type")){
			//no select
			// check radio
			if($(e).attr('type') == 'radio'){
				allEle[$(e).attr('name')] = (($('[name="'+$(e).attr('name')+'"]:checked').val() != undefined) ? $('[name="'+$(e).attr('name')+'"]:checked').val() : '');
			} else {
				allEle[$(e).attr('name')] = $('[name="'+$(e).attr('name')+'"]').val();
			}
		} else {
			//all select
			allEle[$(e).attr('name')] = $('[name="'+$(e).attr('name')+'"]').val();
		}
	}

	function getAllActiveFields(div){
		$('#'+div+' :input:not([type="button"])').each(function(i, e){
			addDataInEleObject(e);
		});
		$('#specialRequestForClearData :input:not([type="button"])').each(function(i, e){
			addDataInEleObject(e);
		});
	}

	function clear_data(){

		if(confirm("Are you sure you want to clear the data? Once the data is cleared then it can't be inserted back.")){
$('#myCustomLoader').show();
			var checkpatient;
			var type;
			alert('<?php echo $patientEligibilityCustom ?>');
			<?php
//
			if($patientEligibilityPartial == "1" && $patientEligibilityOrtho == "1"){
				//P + O
				echo "checkpatient={patientPartialDone: 1,patientOrthoDone: 1,generatePdf: 1};";

			}
			elseif($patientEligibilityPartial == "1" && $patientEligibilityOrtho == "0"){
				//P
				echo "checkpatient={patientPartialDone: 1,generatePdf: 1};";
				echo "type='P';";
			}
			elseif($patientEligibilityFull == "1" && $patientEligibilityOrtho == "0"){
				//F
				echo "checkpatient={patientFullDone: 1,generatePdf: 1};";
				echo "type='F';";
			}
			elseif($patientEligibilityFull == "1" && $patientEligibilityOrtho == "1"){
				//F + O
				echo "checkpatient={patientFullDone: 1,patientOrthoDone: 1,generatePdf: 1};";
				echo "type='F_O';";
			}
			elseif($patientEligibilityFull == "1" && $patientEligibilityPartial == "1" && $patientEligibilityOrtho == "1"){
				//F + P + O
				echo "checkpatient={patientFullDone: 1,patientPartialDone: 1,patientOrthoDone: 1,generatePdf: 1};";
				echo "type='F_P_O';";
			}
			else{
				//custom patients
				echo "checkpatient={patientCustomDone: 1,generatePdf: 1};";
				echo "type='C';";
			}

			?>
			$.ajax({
				type: "POST",
				<?php if($patientEligibilityCustom == "1"){?>
				url: 'generate_report.php?patientId=<?php echo $patientId; ?>&printed=false',
				<?php }else{?>
				url: 'generate_pdf_default.php?patientId=<?php echo $patientId; ?>&f=<?php echo $patientEligibilityFull ;?>&p=<?php echo $patientEligibilityPartial;?>&o=<?php echo $patientEligibilityOrtho;?>&onlyhtml=true',
				<?php }?>
				data: checkpatient,
				success: function (response) {
				    // confirm('here');
					//
					$('#display_custom').hide().html(response);
					$('#htmlpdf').val(response);
					<?php if($patientEligibilityCustom == "1"){?>
					var html_to_store = $('#display_custom').children('div').children('div').html();
					<?php }else{?>
					var html_to_store = $('#htmlpdf').val();
					<?php }?>
					$.ajax({
						type: "POST",
						url: 'index.php?api=clear_patients_data',
						data: {
							action: "submit",
							patient_id: '<?php echo $patientId; ?>',
							deleted_by: '<?php echo $sessionId; ?>',
							tblName: '<?php echo $tblName; ?>',
							request_data: html_to_store,
							type: type,
							allFields: allEle,
							isRemove: isDeleteAll
						},
						dataType: "json",
						success: function (response) {
							$('#display_custom').html('');
							if(response.bool) {
$('#myCustomLoader').hide();
								//var fullUrl = '<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>';
var fullUrl = '<?php echo HTTP_SERVER.str_replace('/app/', '', $_SERVER[REQUEST_URI]); ?>';
//console.log('<?php echo HTTP_SERVER; ?>');
//console.log('<?php echo str_replace('/app/', '', $_SERVER[REQUEST_URI]); ?>');
								//window.location = fullUrl;
                                console.log(fullUrl)
//location.reload(true);
							}
						}
					});
				}
			});
		}
	}
	
	function chkForm(b)
	{
		<? if($_SESSION["tmpSessionCompanyTag"] == "custom_dl" && $patientEligibilityCustom == 1){ ?>
		var date_regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/ ;
		var tmpDate1 = document.getElementById("custEffectiveDate").value;
		var tmpDate2 = document.getElementById("custBenefitsDate").value;
		if(tmpDate1 != "")
		{
			if(!(date_regex.test(tmpDate1)))
			{
				//alert("Please use the correct date format i.e. mm/dd/yyyy");
				swal({
					title: 'Error!',
					text: 'Please use the correct date format i.e. mm/dd/yyyy',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: true
				});
				return false;
			}
		}
		if(tmpDate2 != "")
		{
			if(!(date_regex.test(tmpDate2)))
			{
				//alert("Please use the correct date format i.e. mm/dd/yyyy");
				swal({
					title: 'Error!',
					text: 'Please use the correct date format i.e. mm/dd/yyyy',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: true
				});
				return false;
			}
		}
		<? } ?>
		if( (document.getElementById("srFullCustom").checked || document.getElementById("srPartial").checked || document.getElementById("srOrtho").checked || document.getElementById("srCodes").checked || document.getElementById("srSpecialRequest").checked || document.getElementById("srIncorrectInfo").checked || document.getElementById("srCreatePatient").checked || document.getElementById("srRemotzAccess").checked) && document.getElementById("srTimeSpent").value=="")
		{
			/*alert('Please enter "Time Spent" if you selected any checkbox.');
			document.getElementById("srTimeSpent").focus();*/
			swal({
				title: 'Error!',
				text: 'Please enter "Time Spent" if you selected any checkbox.',
				type: "error",
				showCancelButton: false,
				confirmButtonText: 'OK',
				closeOnConfirm: false
			},
			function(){
				swal.close();
				$("#srTimeSpent").focus();
			});
			return false;
		}
		if( (!document.getElementById("srFullCustom").checked && !document.getElementById("srPartial").checked && !document.getElementById("srOrtho").checked && !document.getElementById("srCodes").checked && !document.getElementById("srSpecialRequest").checked && !document.getElementById("srIncorrectInfo").checked && !document.getElementById("srCreatePatient").checked && !document.getElementById("srRemotzAccess").checked) && document.getElementById("srTimeSpent").value!="")
		{
			/*alert('Please select a checkbox at lease if you already entered "Time Spent".');
			document.getElementById("srFullCustom").focus();*/
			swal({
				title: 'Error!',
				text: 'Please select a checkbox at lease if you already entered "Time Spent".',
				type: "error",
				showCancelButton: false,
				confirmButtonText: 'OK',
				closeOnConfirm: false
			},
			function(){
				swal.close();
				$("#srFullCustom").focus();
			});
			return false;
		}
		if(b == 1)
		{
			
			if(document.getElementById("srTimeSpent").value=="")
			{
				/*alert('Please enter a value in "Time Spent" field before Save & Submit.');
				document.getElementById("srTimeSpent").focus();*/
				swal({
					title: 'Error!',
					text: 'Please enter a value in "Time Spent" field before Save & Submit.',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#srTimeSpent").focus();
				});
				return false;
			}
			
			/*swal({
				title: 'Confirmation!',
				text: 'Are you sure?',
				type: "warning",
				showCancelButton: true,
				confirmButtonText: 'Yes, Proceed',
				cancelButtonText: 'No, Cancel',
				closeOnConfirm: false,
				closeOnCancel: false
			},
			function(isConfirm) {
				if (isConfirm) {
					return true;
				} else {
					return false;
				}
			});*/
		}else{
			/*if(document.getElementById("srTimeSpent").value=="")
			 {
			 if( confirm("Hey! you didn't enter the Time Spent, do you want to continue?") )
			 {
			 return true;
			 }else{
			 return false;
			 }
			 }*/
			 return true;
		}
		
		
	}
	function showAppropriateDiv(divId){
		var counter = 10;
		var navLinks=new Array();
		navLinks[1]="Benefits";
		navLinks[2]="Frequency";
		navLinks[3]="Basic";
		navLinks[4]="Prosthodontics / Major";
		navLinks[5]="Codes";
		navLinks[6]="History";
		navLinks[7]="Secondary Insurance";
		navLinks[8]="Orthodontics";
		navLinks[9]="Recall / Partial Breakdown";
		if (document.getElementById('divOrthodontics')) {
			document.getElementById('divOrthodontics').style.display = 'none';
			document.getElementById('ortho_link').setAttribute('class','leftMenu');
		}
		if (document.getElementById('divRecall')) {
			document.getElementById('divRecall').style.display = 'none';
			document.getElementById('recall_link').setAttribute('class','leftMenu');
		}
		else {
			<? if($patientEligibilityCustom == "1"){ ?>
			for(var i=1;i<counter;i++)
			{
				//document.getElementById('rightLabel').innerHTML = navLinks[i];
				document.getElementById('divCustom').style.display = '';
				//document.getElementById('link'+i).setAttribute('class','leftMenuSelected');
			}
			<? }else{ ?>
			for(var i=1;i<counter;i++){
				if(document.getElementById('div'+i)){
					if('div'+i == divId){
						document.getElementById('rightLabel').innerHTML = navLinks[i];
						document.getElementById('div'+i).style.display = '';
						document.getElementById('link'+i).setAttribute('class','leftMenuSelected');
					}else{
						document.getElementById('div'+i).style.display = 'none';
						document.getElementById('link'+i).setAttribute('class','leftMenu');
					}
				}
			}
			<? } ?>
		}
		if (document.getElementById('divOrthodontics')) {
			if (divId == "divOrthodontics") {
				document.getElementById(divId).style.display = '';
				document.getElementById('ortho_link').setAttribute('class','leftMenuSelected');
				document.getElementById('rightLabel').innerHTML = navLinks[8];
			}
		}
		if (document.getElementById('divRecall')) {
			if (divId == "divRecall") {
				document.getElementById(divId).style.display = '';
				document.getElementById('recall_link').setAttribute('class','leftMenuSelected');
				document.getElementById('rightLabel').innerHTML = navLinks[9];
			}
		}
	}
</script>
<?
//
$patientId = sanitize($_GET["patientId"]);
$patientFname = getField("cui_patients","patientId",$patientId,"patientFname");
$patientLname = getField("cui_patients","patientId",$patientId,"patientLname");
?>
<table border="0" cellpadding="0" cellspacing="" width="100%" class="h1WithBg">
	<tr>
		<td width="35%" nowrap="nowrap"><h1 style="/*color:#fff;*/">Patients - <?=$patientFname." <b style='color:black !important;'>".$patientLname."</b>"?></h1></td>
		<td><h1 style="/*color:#fff;*/">&nbsp;- Eligibility Check</h1></td>
	</tr>
</table>
<div id="pageContainer">
	<form name="form1" id="form1" method="post">
		<table class="form-spacing" width="100%" cellpadding="5" cellspacing="1">
			<tr>
				<td class="left-table-sidebar" width="130px" valign="top">
					<table width="100%" cellpadding="5" cellspacing="0">
						<?
						//check if we need to show full
						if($patientEligibilityCustom == 1){
							?>
							<tr class="titleTr">
								<? $date=getField("cui_patients_history","patientId",$patientId,"updatedDate","and customStatus=1 order by historyId desc limit 0,1"); ?>
								<td>CUSTOM - <? if ($date!="") echo date('m/d/y',$date); else echo "N/A"; ?></td>
							</tr>
							<tr>
								<td class="leftMenuTd">
									<a href="#" class="leftMenuSelected" id="recall_link" onclick="JavaScript:void(0);">Custom Form</a>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
						<?}?>
						<?
						//check if we need to show full
						if($patientEligibilityFull == 1){
							?>
							<tr class="titleTr">
								<? $date=getField("cui_patients_history","patientId",$patientId,"updatedDate","and fullStatus=1 order by historyId desc limit 0,1"); ?>
								<td>FULL - <? if ($date!="") echo date('m/d/y',$date); else echo "N/A"; ?></td>
							</tr>
							<tr>
								<td class="leftMenuTd">
									<?
									//var to start div counter
									$divCounter=0;
									?>
									<a href="#" id="link<?=$divCounter+=1?>" class="leftMenuSelected" data-targetEle="div<?=$divCounter?>" onclick="showAppropriateDiv('div<?=$divCounter?>'); checkToDelete();">Benefits</a>
									<a href="#" id="link<?=$divCounter+=1?>" class="leftMenu" data-targetEle="div<?=$divCounter?>" onclick="showAppropriateDiv('div<?=$divCounter?>'); checkToDelete();">Frequency</a>
									<a href="#" id="link<?=$divCounter+=1?>" class="leftMenu" data-targetEle="div<?=$divCounter?>" onclick="showAppropriateDiv('div<?=$divCounter?>'); checkToDelete();">Basic</a>
									<a href="#" id="link<?=$divCounter+=1?>" class="leftMenu" data-targetEle="div<?=$divCounter?>" onclick="showAppropriateDiv('div<?=$divCounter?>'); checkToDelete();">Prosthodontics / Major</a>
									<a href="#" id="link<?=$divCounter+=1?>" class="leftMenu" data-targetEle="div<?=$divCounter?>" onclick="showAppropriateDiv('div<?=$divCounter?>'); checkToDelete();">Codes</a>
									<a href="#" id="link<?=$divCounter+=1?>" class="leftMenu" data-targetEle="div<?=$divCounter?>" onclick="showAppropriateDiv('div<?=$divCounter?>'); checkToDelete();">History</a>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
						<?}?>
						<?
						//check if we need to show full
						if($patientEligibilityPartial == 1){
							?>
							<tr class="titleTr">
								<? $date=getField("cui_patients_history","patientId",$patientId,"updatedDate","and partialStatus=1 order by historyId desc limit 0,1"); ?>
								<td>PARTIAL - <? if ($date!="") echo date('m/d/y',$date); else echo "N/A"; ?></td>
							</tr>
							<tr>
								<td class="leftMenuTd">
									<a href="#" class="leftMenuSelected" id="recall_link" data-targetEle="divRecall" onclick="showAppropriateDiv('divRecall'); checkToDelete();">Recall / Partial Breakdown</a>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
						<?}?>
						<?
						//check if we need to show full
						if($patientEligibilityOrtho == 1){
							?>
							<tr class="titleTr">
								<? $date=getField("cui_patients_history","patientId",$patientId,"updatedDate","and orthoStatus=1 order by historyId desc limit 0,1"); ?>
								<td>ORTHODONTICS - <? if ($date!="") echo date('m/d/y',$date); else echo "N/A"; ?></td>
							</tr>
							<tr>
								<td class="leftMenuTd">
									<a id="ortho_link" href="#" class="leftMenu" data-targetEle="divOrthodontics" onclick="showAppropriateDiv('divOrthodontics'); checkToDelete();">Orthodontics</a>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
						<?}?>
					</table>
				</td>
				<td valign="top">
					<? if($msg!=""){ ?>
						<div class="success"><?=$msg?></div>
					<? } ?>
					<table width="100%" cellpadding="5" cellspacing="0">
						<tr class="titleTr">
							<?
							//check if we need to show full
							if($patientEligibilityPartial == 1){
								?>
								<td><label id="rightLabel">Recall / Partial Breakdown</label></td>
							<? } else { ?>
								<? if($patientEligibilityCustom == 1){ ?>
									<td><label id="rightLabel">CUSTOM</label></td>
								<? }else{ ?>
									<td><label id="rightLabel">BENEFITS</label></td>
								<? } ?>
							<? } ?>
						</tr>
						<tr>
							<td>
								<div class="message">
									<table width="100%" cellpadding="0" cellspacing="0">
										<tr>
											<td>Fields marked with <span class="required">*</span> are required</td>
											<td align="right">
												<?php //if one is allowed to view this module
												if($_SESSION["cuiSessionUserLevel"] <= 34 && $patientStatus == 'Check'){?>
													<input class="btn clearbt" type="button" name="clear_data_btn" id="clear_data_btn" value="Clear Data" onclick="clear_data()" />
												<? }?>
												<input class="btn searchbt" type="submit" name="submit" id="submit" value="Save" onclick="return chkForm('0')" />
												<!--<input class="btn searchbt" type="submit" name="submit" id="submit" value="Save & Submit" onclick='return chkForm("1"); javascript: if (!confirm("Are you sure?")) return false;'/>-->
												<input class="btn searchbt" type="submit" name="submit" id="submit" value="Save & Submit" onclick="return chkForm('1'); "/>
												<input class="btn clearbt" type="button" name="cancel" id="cancel" value="Cancel" onclick="window.location='index.php?do=patients'"/>
											</td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<!-- Divs (start) -->
								<?
								//div var starting count again
								$divCounter = 0;
								?>

								<? if($patientEligibilityCustom == 1){ ?>
									<!-- Custom Start -->
									<? include ("forms/".$_SESSION["tmpSessionCompanyTag"].".php") ?>
									<!-- Custom End -->
								<? }else{ ?>
									<? if($patientEligibilityPartial != 1){ ?>
										<!-- Benefits Start -->
										<? include ("forms/benefits.php") ?>
										<!-- Benefits End -->
										<!-- Preventative Start -->
										<? include ("forms/preventative.php") ?>
										<!-- Preventative End -->
									<? } ?>
									<? if($_SESSION["tmpSessionCompanyId"] != "16" 									
									&& $_SESSION["tmpSessionCompanyId"] != "20" 																		
									&& $_SESSION["tmpSessionCompanyId"] != "21" 									
									&& $_SESSION["tmpSessionCompanyId"] != "22" 																		
									&& $_SESSION["tmpSessionCompanyId"] != "27" 									
									&& $_SESSION["tmpSessionCompanyId"] != "29"  									
									&& $_SESSION["tmpSessionCompanyId"] != "30"									
									&& $_SESSION["tmpSessionCompanyId"] != "33"  																		
									&& $_SESSION["tmpSessionCompanyId"] != "34"										
									&& $_SESSION["tmpSessionCompanyId"] != "83"
									&& $_SESSION["tmpSessionCompanyId"] != "88"
									&& $_SESSION["tmpSessionCompanyId"] != "103"
									&& $_SESSION["tmpSessionCompanyId"] != "113"
									&& $_SESSION["tmpSessionCompanyId"] != "114"
									&& $_SESSION["tmpSessionCompanyId"] != "115"
									&& $_SESSION["tmpSessionCompanyId"] != "118"
									&& $_SESSION["tmpSessionCompanyId"] != "119"
									&& $_SESSION["tmpSessionCompanyId"] != "120"
									&& $_SESSION["tmpSessionCompanyId"] != "121"
									&& $_SESSION["tmpSessionCompanyId"] != "122"
									&& $_SESSION["tmpSessionCompanyId"] != "124"
									&& $_SESSION["tmpSessionCompanyId"] != "125"
									&& $_SESSION["tmpSessionCompanyId"] != "126"
									&& $_SESSION["tmpSessionCompanyId"] != "127"
									&& $_SESSION["tmpSessionCompanyId"] != "128"
									&& $_SESSION["tmpSessionCompanyId"] != "130"
									&& $_SESSION["tmpSessionCompanyId"] != "131"
									&& $_SESSION["tmpSessionCompanyId"] != "132"
									&& $_SESSION["tmpSessionCompanyId"] != "133"
									&& $_SESSION["tmpSessionCompanyId"] != "134"
									&& $_SESSION["tmpSessionCompanyId"] != "135"
									&& $_SESSION["tmpSessionCompanyId"] != "136"
									&& $_SESSION["tmpSessionCompanyId"] != "137"
									&& $_SESSION["tmpSessionCompanyId"] != "138"
									&& $_SESSION["tmpSessionCompanyId"] != "139"
									&& $_SESSION["tmpSessionCompanyId"] != "140"
									&& $_SESSION["tmpSessionCompanyId"] != "141"
									&& $_SESSION["tmpSessionCompanyId"] != "142"
									&& $_SESSION["tmpSessionCompanyId"] != "143"
									&& $_SESSION["tmpSessionCompanyId"] != "144"
									&& $_SESSION["tmpSessionCompanyId"] != "145"
									&& $_SESSION["tmpSessionCompanyId"] != "146"
									&& $_SESSION["tmpSessionCompanyId"] != "147"
									&& $_SESSION["tmpSessionCompanyId"] != "148"
									&& $_SESSION["tmpSessionCompanyId"] != "149"
									&& $_SESSION["tmpSessionCompanyId"] != "150"
									&& $_SESSION["tmpSessionCompanyId"] != "151"
									&& $_SESSION["tmpSessionCompanyId"] != "152"
									&& $_SESSION["tmpSessionCompanyId"] != "153"
									&& $_SESSION["tmpSessionCompanyId"] != "154"
									&& $_SESSION["tmpSessionCompanyId"] != "155"
									&& $_SESSION["tmpSessionCompanyId"] != "156"
									&& $_SESSION["tmpSessionCompanyId"] != "157"
									&& $_SESSION["tmpSessionCompanyId"] != "158"
									&& $_SESSION["tmpSessionCompanyId"] != "159"
									&& $_SESSION["tmpSessionCompanyId"] != "160"
									&& $_SESSION["tmpSessionCompanyId"] != "161"
									&& $_SESSION["tmpSessionCompanyId"] != "162"
									&& $_SESSION["tmpSessionCompanyId"] != "163"
									&& $_SESSION["tmpSessionCompanyId"] != "164"
									&& $_SESSION["tmpSessionCompanyId"] != "165"
									&& $_SESSION["tmpSessionCompanyId"] != "166"
									&& $_SESSION["tmpSessionCompanyId"] != "167"
									&& $_SESSION["tmpSessionCompanyId"] != "168"
									&& $_SESSION["tmpSessionCompanyId"] != "169"
									&& $_SESSION["tmpSessionCompanyId"] != "170"
									&& $_SESSION["tmpSessionCompanyId"] != "172"
									&& $_SESSION["tmpSessionCompanyId"] != "173"
									&& $_SESSION["tmpSessionCompanyId"] != "174"
									&& $_SESSION["tmpSessionCompanyId"] != "175"
									&& $_SESSION["tmpSessionCompanyId"] != "176"
									&& $_SESSION["tmpSessionCompanyId"] != "177"
									&& $_SESSION["tmpSessionCompanyId"] != "178"){ ?>
										<!-- Frequency Start -->
										<? include ("forms/frequency.php") ?>
										<!-- Frequency End -->
										<!-- Age Start -->
										<? include ("forms/age.php") ?>

										<? //include ("forms/pocketdepthsrp.php") ?>
										<!-- Age End -->
										<!-- Basic Start -->
										<? include ("forms/basic.php") ?>
										<!-- Basic End -->
										<!-- PROSTHODONTICS/MAJOR start -->
										<? include ("forms/major.php") ?>
										<!-- PROSTHODONTICS/MAJOR End -->
										<!-- Codes (start) -->
										<? include ("forms/codes.php") ?>
										<!-- Codes (end) -->
										<!-- History (start) -->
										<? include ("forms/history.php") ?>

										<!-- History (end) -->
									<? } ?>
									<?
									//check if we need to show full
									if($patientEligibilityPartial == 1){
										?>
										<!-- Recall / Partial Breakdown -->
										<? include ("forms/recall.php") ?>
										<!-- Recall / Partial Breakdown -->
									<? } ?>
									<?
									//check if we need to show full
									if($patientEligibilityOrtho == 1){
										?>
										<!-- Ortho (start) -->
										<? include ("forms/ortho.php") ?>
										<!-- Ortho (end) -->
									<? } ?>

									<!-- Special Request (start) -->

									<!-- Special Request (start) -->

									<!-- Special Request (start) -->
									<? include ("forms/special_request.php") ?>
									<!-- Special Request (start) -->
								<? } ?>
								<!-- Divs (end) -->
								<!-- Status Report for all forms - Start -->
								<br />
								<table width="100%" cellpadding="5" cellspacing="0">
									<tr class="titleTr">
										<td colspan="4"><label id="rightLabel">Status Report</label></td>
									</tr>
								</table>
								<table border="1" cellpadding="5" cellspacing="5" width="100%" style="border:1px solid; border-collapse:collapse;">
									<tr>
										<td colspan="8" align="center"><b>Type of Breakdown</b></td>
										<td align="center" rowspan="2"><b>Time spent<br />(in&nbsp;minutes)</b></td>
										<td align="center" rowspan="2"><b>Remarks</b></td>
									</tr>
									<tr>
										<td align="center"><b>Full/Custom</b></td>
										<td align="center"><b>Partial</b></td>
										<td align="center"><b>Ortho</b></td>
										<td align="center"><b>Codes</b></td>
										<td align="center"><b>Special Request</b></td>
										<td align="center"><b>Incorrect Info</b></td>
										<td align="center"><b>Create Patient</b></td>
										<td align="center"><b>Remote Access</b></td>
									</tr>
									<tr>
										<td align="center"><input type="checkbox" name="srFullCustom" id="srFullCustom" value="1" /></td>
										<td align="center"><input type="checkbox" name="srPartial" id="srPartial" value="1" /></td>
										<td align="center"><input type="checkbox" name="srOrtho" id="srOrtho" value="1" /></td>
										<td align="center"><input type="checkbox" name="srCodes" id="srCodes" value="1" /></td>
										<td align="center"><input type="checkbox" name="srSpecialRequest" id="srSpecialRequest" value="1" /></td>
										<td align="center"><input type="checkbox" name="srIncorrectInfo" id="srIncorrectInfo" value="1" /></td>
										<td align="center"><input type="checkbox" name="srCreatePatient" id="srCreatePatient" value="1" /></td>
										<td align="center"><input type="checkbox" name="srRemotzAccess" id="srRemotzAccess" value="1" /></td>
										<td align="center"><input type="text" class="textbox" name="srTimeSpent" id="srTimeSpent" value="" style="width:50px;" maxlength="10" /></td>
										<td align="center"><textarea name="srRemarks" id="srRemarks" rows="2" cols="20" style="font-weight:normal;"></textarea></td>
									</tr>
								</table>
								<!-- Status Report for all forms - Start -->
								<!-- Custom Report -->
								<br />
								<a title="Time Spent" href="timeSpent.php?patientId=<?=$patientId?>&TB_iframe=true&height=300&width=1000" class="thickbox"><input class="btn searchbt" type="button" value="Time Spent" /></a>
							</td>
						</tr>
						<tr>
							<td>

								<div class="message">
									<table width="100%" cellpadding="0" cellspacing="0">
										<tr>
											<td></td>
											<td align="right">
												<?php //if one is allowed to view this module
												if($_SESSION["cuiSessionUserLevel"] <= 34 && $patientStatus == 'Check'){?>
													<input class="btn clearbt" type="button" name="clear_data_btn" id="clear_data_btn" value="Clear Data" onclick="clear_data()" />
												<?php }?>
												<input class="btn searchbt" type="submit" name="submit" id="submit" value="Save" onclick="return chkForm('0')" /> 
												<input class="btn searchbt" type="submit" name="submit" id="submit" value="Save & Submit" onclick="javascript:return chkForm('1');"/> 
												<input class="btn clearbt" type="button" name="cancel" id="cancel" value="Cancel" onclick="window.location='index.php?do=patients'"/></td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
</div>
<div id="display_custom"></div>