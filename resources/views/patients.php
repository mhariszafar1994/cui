<?php
//roles check
if ($moduleAll == 0 and $modulePatientsView == 0 and $modulePatientsViewAll == 0) {
	echo "<script>window.location='index.php?do=authorization'</script>";
}
//Company All checker - start
if ($_SESSION["cuiSessionCompanyId"] == "All") {
	if ($_GET["patientId"] != "") {
		$tmpPatientId = sanitize($_GET["patientId"]);
		$tmpSql = "Select companyId, officeId, doctorId from cui_patients where patientId='$tmpPatientId'";
//		$tmpRs = mysql_query($tmpSql);
		$tmpRs = mysqli_query($con, $tmpSql);
		//
		//		if(mysql_num_rows($tmpRs) > 0)
		if (@mysqli_num_rows($tmpRs) > 0) {
//			$compId = mysql_result($tmpRs, 0, "companyId");
			$compId = mysqli_result($tmpRs, 0, "companyId");
//			$offId = mysql_result($tmpRs, 0, "officeId");
			$offId = mysqli_result($tmpRs, 0, "officeId");
			$_SESSION["tmpSessionCompanyId"] = $compId;
			$_SESSION["tmpSessionOfficeId"] = $offId;
			//for custom tag
			$tmpCustCompName = getField("cui_companies", "companyId", $compId, "companyCompanyName", " and companyCustom='1'");
			$tmpCustCompTag = getField("cui_custom_companies", "custCompanyName", $tmpCustCompName, "custCompanyTag", " and custCompanyStatus='1'");
			$_SESSION["tmpSessionCompanyTag"] = $tmpCustCompTag;
		}
	}
} else {
	$_SESSION["tmpSessionCompanyId"] = $_SESSION["cuiSessionCompanyId"];
	$_SESSION["tmpSessionOfficeId"] = $_SESSION["cuiSessionOfficeId"];
	$_SESSION["tmpSessionCompanyTag"] = $_SESSION["cuiSessionCompanyTag"];
}
//Company All checker - end
//checking the current company custom or not
$checkCustom = getField("cui_companies", "companyId", $_SESSION["tmpSessionCompanyId"], "companyCustom", " and companyStatus='1'");
if (isset($_GET["act"]) && $_GET["act"] == "edit") {
	$patientId = sanitize($_GET["patientId"]);
	$patientFname = getField("cui_patients", "patientId", $patientId, "patientFname");
	$patientLname = getField("cui_patients", "patientId", $patientId, "patientLname");
}
?>

<?
if (isset($_GET["patientStatus"]) && $_GET["patientStatus"] == "Check") {
	$subheading = "Check";
	$clearfilter = "&patientStatus=Check";
} elseif (isset($_GET["patientStatus"]) && $_GET["patientStatus"] == "Updated") {
	$subheading = "Updated";
	$clearfilter = "&patientStatus=Updated";
} elseif (isset($_GET["patientStatus"]) && $_GET["patientStatus"] == "Printed") {
	$subheading = "Reports";
	$clearfilter = "&patientStatus=Printed";
} else {
	$subheading = "";
	$clearfilter = "";
}
?>
<h1 class="h1WithBg">Patients <?=$subheading;?><?if (isset($_GET["act"]) && $_GET["act"] == "edit") {
	echo " - " . $patientFname . " " . $patientLname;
}?></h1>
<div id="pageContainer">
	<script type="text/javascript">
        function showDoctors(str) {
            if (str == "") {
                document.getElementById("ddlDoctors").innerHTML = '<select name="doctorId" id="doctorId" tabindex="38"><option value="">--- select ---</option></select>';
                return;
            }
            if (window.XMLHttpRequest) {
                xmlhttp = new XMLHttpRequest();
            }
            else {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("ddlDoctors").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "getDoctors.php?officeid=" + str, true);
            xmlhttp.send();
        }
        $(document).ready(function () {
            $('#patientPhoneHome, #patientPhoneCell, #patientPhoneWork, #patientInsPhone').keyup(function () {
                ph = $(this).val().split("-").join(""); // remove hyphens
                ph = ph.match(new RegExp('.{1,4}$|.{1,3}', 'g')).join("-");
                $(this).val(ph);
            });
            /*$('#patientSubSS').keyup(function() {
             sn = $(this).val().split("-").join(""); // remove hyphens
             sn = sn.match(new RegExp('.{1,4}$|.{1,3}', 'g')).join("-");
             $(this).val(sn);
             });*/
        });

    </script>

    <?php
//vars
$success = 0;
$act = "";
$showPatientForm = 0;
$msg = "";
$sortBy = "";
$sortOrder = "";
$sqlVars = "";
$queryString = "";
$patientStatus = "";
if ($_GET["page"] != '') {
} else {
	unset($_SESSION['sortBy']);
}
//if get sortBy
if ($_GET["sortBy"]) {
	$sortBy = sanitize($_GET["sortBy"]);
	$_SESSION['sortBy'] = $sortBy;
} else {
	if (isset($_SESSION['sortBy']) && $_SESSION['sortBy'] != '') {
		$sortBy = sanitize($_SESSION['sortBy']);
	}
}
//if get sortOrder
if ($_GET["sortOrder"]) {
	$sortOrder = sanitize($_GET["sortOrder"]);
	$_SESSION['sortOrder'] = $sortOrder;
} else {
	if (isset($_SESSION['sortOrder']) && $_SESSION['sortOrder'] != '') {
		$sortOrder = sanitize($_SESSION['sortOrder']);
	}
}
//if get act
if ($_GET["act"]) {
	$act = sanitize($_GET["act"]);
}
//if get msg
if ($_GET["msg"]) {
	$msg = sanitize($_GET["msg"]);
}
//if get patientStatus
$patientStatus = '';
$getPatientStatus = '';
if ($_GET["patientStatus"]) {
	$patientStatus = sanitize($_GET["patientStatus"]);
	$getPatientStatus = sanitize($_GET["patientStatus"]);
}
//restrictions and conditions
$proceed = 0;
if ($moduleAll == 1) {
	$proceed = 1;
} else {
	//checking status
	if (($patientStatus == '' or $patientStatus == 'New') and $modulePatientsMain == 1) {
		$proceed = 1;
	} elseif ($patientStatus == 'Check' and $modulePatientsCheck == 1) {
		$proceed = 1;
	} elseif ($patientStatus == 'Updated' and $modulePatientsUpdated == 1) {
		$proceed = 1;
	} elseif ($patientStatus == 'Printed' and $modulePatientsPrinted == 1) {
		$proceed = 1;
	}
	//checking act
	if ($act == "add" and $modulePatientsAdd == 1) {
		$proceed = 1;
	} elseif ($act == 'edit' and ($modulePatientsEdit == 1 or $modulePatientsCheck == 1)) {
		$proceed = 1;
	}
}
if ($proceed == 0) {
	echo "<script>window.location='index.php?do=authorization'</script>";
}
//if not blank
if ($patientStatus != "" and $patientStatus != "New") {
	$queryString .= "&patientStatus=$patientStatus";
	$sqlVars .= " and patientStatus='$patientStatus'";
}
//company and office selection check
if ($sessionCompanyId != "All" and $sessionCompanyId != "") {
	$sqlVars .= " and a.companyId='$sessionCompanyId'";
} else {
	/* May 30th, 2017 adding condition that was never there to filter "All" for assigned users
    	*/

	//Condition to switch between administrator and other user types
	switch ($_SESSION["cuiSessionUserLevel"]) {
	case 1:
		//Do nothing for admin
		break;

	default:
		//First get all assigned companies
		$getCompanySql = "select distinct(a.companyId) from cui_companies as a join cui_users_companies as b where a.companyStatus='1' and a.companyId=b.companyId and b.userId='$sessionId'  order by companyName ASC";
		$getCompanyWhileCounter = 0;
		if ($getCompanyResult = mysqli_query($con, $getCompanySql)) {
			while ($getCompanyRs = @mysqli_fetch_array($getCompanyResult)) {
				$getCompanyWhileCounter++;
				$getCompanyId = $getCompanyRs["companyId"];

				if ($getCompanyWhileCounter == 1) {
					$sqlVars .= " AND (a.companyId = '$getCompanyId'";
				} else {
					$sqlVars .= " OR a.companyId = '$getCompanyId'";
				}

			}
			$sqlVars .= ")";
		}
		break;
	}
}
if ($sessionOfficeId != "All" and $sessionOfficeId != "") {
	$sqlVars .= " and officeId='$sessionOfficeId'";
} else {
	//see if user has access to all offices
	if ($moduleAll != 1 and $moduleCompaniesViewAll != 1) {
		//check if all offices are assigned (use return array parameter
		$checkOfficesAndGetArray = checkAssignStatus($sessionId, $sessionCompanyId, $sessionOfficeId, 1);
		//if array has values
		if (count($checkOfficesAndGetArray) > 0) {
			//loop
			foreach ($checkOfficesAndGetArray as $officeArrayValue) {
				//if not zero then don't bother include as not including gets (All) as All = 0
				if ($officeArrayValue != '0') {
					$sqlVars .= " and officeId=$officeArrayValue";
				}
			}
		}
	}
}
/** IMPORTANT CHECK **/
$userCompanyArray = array();
$userCompanyCounter = -1;
if ($moduleAll != 1 and $modulePatientsViewAll != 1) {
	$sqlCompanies = "SELECT distinct(companyId) FROM cui_users_companies where userId='$sessionId'";
//    $resultCompanies = mysql_query($sqlCompanies);
	$resultCompanies = mysqli_query($con, $sqlCompanies);
//    while ($rsCompanies = mysql_fetch_array($resultCompanies)) {
	while ($rsCompanies = @mysqli_fetch_array($resultCompanies)) {
		$userCompanyId = $rsCompanies["companyId"];
		$userCompanyoffices = getCompanyOffices($userCompanyId, " and userId=$sessionId");
		$userCompanyArray[$userCompanyId] = str_replace("|", ",", $userCompanyoffices);
	}
}
//this array determines what will be shown depending on assigned companies and offices
//if not session
/*
if($sessionCompanyId=="All" or $sessionCompanyId==""){
$ucaCounter = 0;
if($userCompanyArray){
foreach ($userCompanyArray as $key=>$value){
if($value!=""){
$ucaCounter++;
$valueEx = explode(",",$value);
//add or
if($ucaCounter>1){
$sqlVars .= " or ";
}elseif ($ucaCounter == 1){
//add first and
$sqlVars .= " and (";
}
$sqlVars .= "companyId=$key and ";
//explode array
if(count($valueEx>0)){
$ucaCounter2=0;
$sqlVars .= "(";
//explode for each loop
foreach ($valueEx as $vE){
$ucaCounter2++;
//add or
if($ucaCounter2>1){
$sqlVars .= " or ";
}
$sqlVars .= "officeId=$vE";
}
$sqlVars .= ")";
}
//if last sql
if($ucaCounter == count($userCompanyArray)){
$sqlVars .= ")";
}
}
}
}
}
 */
/** IMPORTANT CHECK **/
//if act == changestatus
if ($act == "changeStatus") {
	//roles check (one can edit or not)
	if ($moduleAll == 0 and $modulePatientsEdit == 0) {
		echo "<script>window.location='index.php?do=authorization'</script>";
	}
	$patientId = sanitize($_GET["patientId"]);
	$statusParameter = sanitize($_GET["parameter"]);
	//if check
	if ($statusParameter == "Check") {
		//additional information to save
		//override patient id
		$patientId = sanitize($_POST["patientId"]);
		$patientEligibility = sanitize($_POST["eligibility"]);
		$patientOrtho = sanitize($_POST["ortho"]);
		//first check the previous status
		//		$sqlCheckStatus = mysql_query("Select patientStatus from cui_patients WHERE patientId='$patientId'");
		$sqlCheckStatus = mysqli_query($con, "Select patientStatus from cui_patients WHERE patientId='$patientId'");
//		$rsStatus = mysql_result($sqlCheckStatus,0,"patientStatus");
		$rsStatus = mysqli_result($sqlCheckStatus, 0, "patientStatus");
		//echo $rsStatus; exit;
		//update table set the requirements
		$dateTimeUpdated = date("Y-m-d H:i:s");
//        mysql_query("UPDATE cui_patients SET patientEligibility='$patientEligibility', patientOrtho='$patientOrtho', patientStatus='Check', patientUpdated='$dateTimeUpdated' WHERE patientId='$patientId'");
		mysqli_query($con, "UPDATE cui_patients SET patientEligibility='$patientEligibility', patientOrtho='$patientOrtho', patientStatus='Check', patientUpdated='$dateTimeUpdated' WHERE patientId='$patientId'");
		// insert history
		if ($patientEligibility == "Custom") {
			$custom = 1;
		} else {
			$custom = 0;
		}
		if ($patientEligibility == "Partial") {
			$partial = 1;
			$full = 0;
		} else {
			$full = 1;
			$partial = 0;
		}
		if ($patientOrtho == "Yes") {
			$ortho = 1;
		} else {
			$ortho = 0;
		}
		//creating REMARKS
		$remarks = "";
		$ramarksEligibility = "";
		if ($_SESSION["tmpSessionCompanyId"] == "16"
			|| $_SESSION["tmpSessionCompanyId"] == "20"
			|| $_SESSION["tmpSessionCompanyId"] == "21"
			|| $_SESSION["tmpSessionCompanyId"] == "22"
			|| $_SESSION["tmpSessionCompanyId"] == "27"
			|| $_SESSION["tmpSessionCompanyId"] == "29"
			|| $_SESSION["tmpSessionCompanyId"] == "33"
			|| $_SESSION["tmpSessionCompanyId"] == "29"
			|| $_SESSION["tmpSessionCompanyId"] == "30"
			|| $_SESSION["tmpSessionCompanyId"] == "34"
			|| $_SESSION["tmpSessionCompanyId"] == "83"
			|| $_SESSION["tmpSessionCompanyId"] == "88"
			|| $_SESSION["tmpSessionCompanyId"] == "103"
			|| $_SESSION["tmpSessionCompanyId"] == "113"
			|| $_SESSION["tmpSessionCompanyId"] == "114"
			|| $_SESSION["tmpSessionCompanyId"] == "115"
			|| $_SESSION["tmpSessionCompanyId"] == "118"
			|| $_SESSION["tmpSessionCompanyId"] == "119"
			|| $_SESSION["tmpSessionCompanyId"] == "120"
			|| $_SESSION["tmpSessionCompanyId"] == "121"
			|| $_SESSION["tmpSessionCompanyId"] == "122"
			|| $_SESSION["tmpSessionCompanyId"] == "124"
			|| $_SESSION["tmpSessionCompanyId"] == "125"
			|| $_SESSION["tmpSessionCompanyId"] == "126"
			|| $_SESSION["tmpSessionCompanyId"] == "127"
			|| $_SESSION["tmpSessionCompanyId"] == "128"
			|| $_SESSION["tmpSessionCompanyId"] == "130"
			|| $_SESSION["tmpSessionCompanyId"] == "131"
			|| $_SESSION["tmpSessionCompanyId"] == "132"
			|| $_SESSION["tmpSessionCompanyId"] == "133"
			|| $_SESSION["tmpSessionCompanyId"] == "134"
			|| $_SESSION["tmpSessionCompanyId"] == "135"
			|| $_SESSION["tmpSessionCompanyId"] == "136"
			|| $_SESSION["tmpSessionCompanyId"] == "137"
			|| $_SESSION["tmpSessionCompanyId"] == "138"
			|| $_SESSION["tmpSessionCompanyId"] == "139"
			|| $_SESSION["tmpSessionCompanyId"] == "140"
			|| $_SESSION["tmpSessionCompanyId"] == "141"
			|| $_SESSION["tmpSessionCompanyId"] == "142"
			|| $_SESSION["tmpSessionCompanyId"] == "143"
			|| $_SESSION["tmpSessionCompanyId"] == "144"
			|| $_SESSION["tmpSessionCompanyId"] == "145"
			|| $_SESSION["tmpSessionCompanyId"] == "146"
			|| $_SESSION["tmpSessionCompanyId"] == "147"
			|| $_SESSION["tmpSessionCompanyId"] == "148"
			|| $_SESSION["tmpSessionCompanyId"] == "149"
			|| $_SESSION["tmpSessionCompanyId"] == "150"
			|| $_SESSION["tmpSessionCompanyId"] == "151"
			|| $_SESSION["tmpSessionCompanyId"] == "152"
			|| $_SESSION["tmpSessionCompanyId"] == "153"
			|| $_SESSION["tmpSessionCompanyId"] == "154"
			|| $_SESSION["tmpSessionCompanyId"] == "155"
			|| $_SESSION["tmpSessionCompanyId"] == "156"
			|| $_SESSION["tmpSessionCompanyId"] == "157"
			|| $_SESSION["tmpSessionCompanyId"] == "158"
			|| $_SESSION["tmpSessionCompanyId"] == "159"
			|| $_SESSION["tmpSessionCompanyId"] == "160"
			|| $_SESSION["tmpSessionCompanyId"] == "161"
			|| $_SESSION["tmpSessionCompanyId"] == "162"
			|| $_SESSION["tmpSessionCompanyId"] == "163"
			|| $_SESSION["tmpSessionCompanyId"] == "164"
			|| $_SESSION["tmpSessionCompanyId"] == "165"
			|| $_SESSION["tmpSessionCompanyId"] == "166"
			|| $_SESSION["tmpSessionCompanyId"] == "167"
			|| $_SESSION["tmpSessionCompanyId"] == "168"
			|| $_SESSION["tmpSessionCompanyId"] == "169"
			|| $_SESSION["tmpSessionCompanyId"] == "170"
			|| $_SESSION["tmpSessionCompanyId"] == "172"
			|| $_SESSION["tmpSessionCompanyId"] == "173"
			|| $_SESSION["tmpSessionCompanyId"] == "174"
			|| $_SESSION["tmpSessionCompanyId"] == "175"
			|| $_SESSION["tmpSessionCompanyId"] == "176"
			|| $_SESSION["tmpSessionCompanyId"] == "177"
			|| $_SESSION["tmpSessionCompanyId"] == "178") {
			$ramarksEligibility .= " for";
			if ($patientEligibility == "Custom") {
				$ramarksEligibility .= " Custom";
			}
			if ($patientEligibility == "Partial") {
				$ramarksEligibility .= " Partial";
			}
			$ramarksEligibility .= " bd";
		} else {
			if ($patientEligibility == "Custom") {
				$ramarksEligibility .= " for Custom bd";
			} else {
				if ($patientEligibility != "" || $patientOrtho != "") {
					$ramarksEligibility .= " for";
					if ($patientEligibility == "Full") {
						if ($patientOrtho == "Yes") {
							$ramarksEligibility .= " Full + ";
						} else {
							$ramarksEligibility .= " Full";
						}
					}
					if ($patientEligibility == "Partial") {
						if ($patientOrtho == "Yes") {
							$ramarksEligibility .= " Partial + ";
						} else {
							$ramarksEligibility .= " Partial";
						}
					}
					if ($patientOrtho == "Yes") {
						$ramarksEligibility .= " Ortho";
					}
					$ramarksEligibility .= " bd";
				} else {
					$ramarksEligibility .= "";
				}
			}
		}
		//
		if (getField("cui_patients", "patientId", $patientId, "patientAppDate") != "" && getField("cui_patients", "patientId", $patientId, "patientAppDate") != "0000-00-00 00:00:00") {
			$patientAppDate2 = date("m/d/Y", strtotime(getField("cui_patients", "patientId", $patientId, "patientAppDate")));
			$patientAppTime2 = date("H:i:s a", strtotime(getField("cui_patients", "patientId", $patientId, "patientAppDate")));
		} else {
			$patientAppDate2 = "00/00/0000";
			$patientAppTime2 = "00:00:00";
		}
		//sending email to exiting patient after change the status only
		$patientFname = getField("cui_patients", "patientId", $patientId, "patientFname");
		$patientLname = getField("cui_patients", "patientId", $patientId, "patientLname");
		$companyId = getField("cui_patients", "patientId", $patientId, "companyId");
		$officeId = getField("cui_patients", "patientId", $patientId, "officeId");
		$updatedBy = getField("cui_users", "userId", $_SESSION[$sessionUserID], "userFname") . " " . getField("cui_users", "userId", $_SESSION[$sessionUserID], "userLname");
		// additional header pieces for errors, From cc's, bcc's, etc
		$headers = "From: Checkurinsurance <info@checkurinsurance.com>\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		//subject
		$subject = "Patient '" . $patientFname . " " . $patientLname . " (" . getField("cui_companies", "companyId", $companyId, "companyName") . " - " . getField("cui_companies_offices", "officeId", $officeId, "officeName") . ")' Status Has Been Changed to Check" . " - " . $patientAppDate2 . " " . $patientAppTime2;
		//$toEmail = "info@checkurinsurance.com";
		//$toEmail = "nomanmustafa.khan@gmail.com";
		//message
		$message = "Dear Admin,<br><br>";
		$message .= "The patient <b>" . $patientFname . " " . $patientLname . " (ID: " . $patientId . ")</b> status has been changed to Check" . ".<br>";
		$message .= "Appointment Date/Time:  " . $patientAppDate2 . " " . $patientAppTime2 . "<br>";
		$message .= "Updated By:  " . $updatedBy . "<br>";
		$message .= "Updated Date/Time:  " . date('m/d/Y  H:i a') . " PDT.<br><br>";
		$message .= $httpHost;
		//$message .= "Checkurinsurance.com";
		//echo $message; exit;
		mail($toEmail, $subject, $message, $headers);
		//additinal email sending to Company users
		$companyEmails = getField("cui_companies", "companyId", $_SESSION["tmpSessionCompanyId"], "companyRecipientEmails", " and companyStatus='1'");
		$tmpSplit = explode(",", $companyEmails);
		if (count($tmpSplit) > 0) {
			for ($s = 0; $s < count($tmpSplit); $s++) {
				mail($tmpSplit[$s], $subject, $message, $headers);
			}
		}
		if ($act == "add") {
			$remarks = "Patient added with status " . $patientStatus . $ramarksEligibility;
		} else {
			if ($patientStatus != $rsStatus) {
				$remarks = "Patient changed from " . $rsStatus . " to " . $statusParameter . $ramarksEligibility;
			} else {
				$remarks = "Patient updated with status " . $patientStatus . $ramarksEligibility;
			}
		}
//        mysql_query("INSERT INTO cui_patients_history (patientId, custom, full, partial, ortho, checkerId, checkedDate, remarks) values ('$patientId', '$custom', '$full', '$partial', '$ortho', '$_SESSION[$sessionUserID]', '".time()."', '$remarks')");
		mysqli_query($con, "INSERT INTO cui_patients_history (patientId, custom, full, partial, ortho, checkerId, checkedDate, remarks) values ('$patientId', '$custom', '$full', '$partial', '$ortho', '$_SESSION[$sessionUserID]', '" . time() . "', '$remarks')");
	} else {
		//update table
		//        mysql_query("UPDATE cui_patients set patientStatus='$statusParameter' where patientId='$patientId'");
		mysqli_query($con, "UPDATE cui_patients set patientStatus='$statusParameter' where patientId='$patientId'");
	}
	$success = 1;
	$msg = "Success: Patient Status has been changed!";
	//replace query string to redirect to listing
	$queryString = str_replace("patientId=$patientId", "", $queryString);
	//redirect
	echo "<script>window.location='index.php?do=patients$queryString&msg=$msg'</script>";
}
//if act is delete
if ($act == "delete") {
	//roles check (one can delete or not)
	if ($moduleAll == 0 and $modulePatientsDelete == 0) {
		echo "<script>window.location='index``?do=authorization'</script>";
	}
	$patientId = sanitize($_GET["patientId"]);
	$deleteSql = "DELETE from cui_patients where patientId='$patientId'";
	//delete data from other tables
	if ($_SESSION["tmpSessionCompanyTag"] != "") {
		$deleteSql2 = "DELETE from cui_patients_" . $_SESSION["tmpSessionCompanyTag"] . " where patientId='$patientId'";
	}
	$deleteSql3 = "DELETE from cui_patients_full where patientId='$patientId'";
	$deleteSql4 = "DELETE from cui_patients_files where patientId='$patientId'";
	$deleteSql5 = "DELETE from cui_patients_history where patientId='$patientId'";
	$deleteSql6 = "DELETE from cui_patients_status_report where srPatientId='$patientId'";
	if ($_SESSION["cuiSessionUserLevel"] == 1) {
//		mysql_query($deleteSql);
		mysqli_query($con, $deleteSql);
		if ($_SESSION["tmpSessionCompanyTag"] != "") {
//			mysql_query($deleteSql2);
			mysqli_query($con, $deleteSql2);
		}
//		mysql_query($deleteSql3);
		mysqli_query($con, $deleteSql3);
//		mysql_query($deleteSql4);
		//Deleting File
		$fileSql = "SELECT * from cui_patients_files where patientId='$patientId' LIMIT 1";
		$fileResult = mysqli_query($con, $fileSql);
		$fileRowsCount = @mysqli_num_rows($fileResult);
		if ($fileRowsCount > 0) {
			$fileRow = @mysqli_fetch_array($fileResult);
			$file = $fileRow['fileName'];
			$directory = 'files/';
			if (file_exists($directory.$file)) {
				unlink($directory.$file);
			}
		}
		mysqli_query($con, $deleteSql4);
//		mysql_query($deleteSql5);
		mysqli_query($con, $deleteSql5);
//		mysql_query($deleteSql6);
		mysqli_query($con, $deleteSql6);
		$success = 1;
		$msg = "Success: Patient has been deleted.";
	} else {
		//checking permision for Dental to delete record
		$tmpCustom = $checkCustom;
		$tmpCompTag = $_SESSION["tmpSessionCompanyTag"];
		if ($tmpCustom == 1) {
			$tblName = "cui_patients_" . $tmpCompTag;
		} else {
			$tblName = "cui_patients_full";
		}
		// checking record inserted or not
		$tmpSqlPatients = "SELECT * FROM " . $tblName . " WHERE patientId='$patientId'";
//		$tmpResultPatients = mysql_query($tmpSqlPatients);
		$tmpResultPatients = mysqli_query($con, $tmpSqlPatients);
//		$tmpTotalRec = mysql_num_rows($tmpResultPatients);
		$tmpTotalRec = @mysqli_num_rows($tmpResultPatients);
//		$tmpSqlStatus = mysql_query("SELECT patientStatus from cui_patients where patientId='$patientId'");
		$tmpSqlStatus = mysqli_query($con, "SELECT patientStatus from cui_patients where patientId='$patientId'");
//		if(mysql_num_rows($tmpSqlStatus))
		if (@mysqli_num_rows($tmpSqlStatus)) {
//			$tmpPatientStatus = mysql_result($tmpSqlStatus,0,"patientStatus");
			$tmpPatientStatus = mysqli_result($tmpSqlStatus, 0, "patientStatus");
			if ($tmpPatientStatus == "New" || ($tmpPatientStatus == "Check" && $tmpTotalRec == 0)) {
//				mysql_query($deleteSql);
				mysqli_query($con, $deleteSql);
				if ($_SESSION["tmpSessionCompanyTag"] != "") {
//					mysql_query($deleteSql2);
					mysqli_query($con, $deleteSql2);
				}
//				mysql_query($deleteSql3);
				mysqli_query($con, $deleteSql3);
//				mysql_query($deleteSql4);
				//Deleting File
				$fileSql = "SELECT * from cui_patients_files where patientId='$patientId' LIMIT 1";
				$fileResult = mysqli_query($con, $fileSql);
				$fileRowsCount = @mysqli_num_rows($fileResult);
				if ($fileRowsCount > 0) {
					$fileRow = @mysqli_fetch_array($fileResult);
					$file = $fileRow['fileName'];
					$directory = 'files/';
					if (file_exists($directory.$file)) {
						unlink($directory.$file);
					}
				}
				mysqli_query($con, $deleteSql4);
//				mysql_query($deleteSql5);
				mysqli_query($con, $deleteSql5);
//				mysql_query($deleteSql6);
				mysqli_query($con, $deleteSql6);
				$success = 1;
				$msg = "Success: Patient has been deleted.";
			}
		}
	}
}
//getting data
if ($act == "edit") {
	//roles check (one can edit or not)
	if ($moduleAll == 0 and ($modulePatientsEdit == 0 and $modulePatientsCheck == 0)) {
		echo "<script>window.location='index.php?do=authorization'</script>";
	}
	$showPatientForm = 1;
	$patientId = sanitize($_GET["patientId"]);
	$headingTitle = "Edit Patient (ID: $patientId)";
	//$patientSql = "SELECT * FROM cui_patients WHERE patientId='$patientId' And companyId='".$_SESSION["tmpSessionCompanyId"]."' And officeId='".$_SESSION["tmpSessionOfficeId"]."'";
	//echo $patientSql;
	$patientSql = "SELECT * FROM cui_patients WHERE patientId='$patientId'";
//    $patientResult = mysql_query($patientSql);
	$patientResult = mysqli_query($con, $patientSql);
//	if(mysql_num_rows($patientResult) == 0)
	if (@mysqli_num_rows($patientResult) == 0) {
		echo "<script>window.location='index.php?do=authorization'</script>";
		exit;
	}
//    if(mysql_num_rows($patientResult)>0){
	if (@mysqli_num_rows($patientResult) > 0) {
		//take all values in an array var
		//        $patientRsArray = mysql_fetch_array($patientResult);
		$patientRsArray = @mysqli_fetch_array($patientResult);
		//foreach loop for all fields
		foreach ($patientRsArray as $key => $value) {
			//the above array brings numbers unwanted
			if (!is_numeric($key) and $key != "") {
				//remove commas
				$value = str_replace("'", "", $value);
				//initialize variables using eval
				eval("$" . $key . " = '" . $value . "';");
			}
		}
		$btnValue = "Update";
	}
	//extra field mods
	if ($patientAppDate != "") {
		$patientAppDateEx = explode(" ", $patientAppDate);
		$patientAppDate = formatDate($patientAppDateEx[0]);
		$patientAppTime = $patientAppDateEx[1] . " " . $patientAppDateEx[2];
		if (trim($patientAppTime) != "00:00:00") {
			$patientAppTime = changeTime($patientAppTime, 12);
		}
		$patientAppTimeEx = explode(":", $patientAppTime);
		$patientAppTimeHours = $patientAppTimeEx[0];
		$patientAppTimeEx = explode(" ", $patientAppTimeEx[1]);
		$patientAppTimeMinutes = $patientAppTimeEx[0];
		$patientAppTimeAmPm = $patientAppTimeEx[1];
	}
	$tmpPatientSubId = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', encrypt_decrypt('decrypt', $patientSubId));
	if ($patientSubSS != "") {
		$tmpPatientSubSS = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', encrypt_decrypt('decrypt', $patientSubSS));
	}
	$tmpPatientGroup = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', encrypt_decrypt('decrypt', $patientGroup));
	//echo encrypt_decrypt('encrypt', 'w28488855');
	//echo $tmpPatientGroup;
	//exit;
	$tmpPatientDob = encrypt_decrypt('decrypt', $patientDob);
	if ($tmpPatientDob > 0) {
		$patientDobEx = explode("-", $tmpPatientDob);
		$patientDobYear = intval($patientDobEx[0]);
		$patientDobMonth = intval($patientDobEx[1]);
		$patientDobDay = intval($patientDobEx[2]);
	}
	$tmpPatientFamilyDob = encrypt_decrypt('decrypt', $patientFamilyDob);
	if ($tmpPatientFamilyDob > 0) {
		$patientFamilyDobEx = explode("-", $tmpPatientFamilyDob);
		$patientFamilyDobYear = intval($patientFamilyDobEx[0]);
		$patientFamilyDobMonth = intval($patientFamilyDobEx[1]);
		$patientFamilyDobDay = intval($patientFamilyDobEx[2]);
	}
}
//getting data
if ($act == "view") {
	//roles check (one can edit or not)
	if ($moduleAll == 0 and $modulePatientsView == 0) {
		echo "<script>window.location='index.php?do=authorization'</script>";
	}
	$viewPatient = 1;
	$patientId = sanitize($_GET["patientId"]);
	$headingTitle = "View Patient (ID: $patientId)";
	$patientSql = "SELECT * FROM cui_patients WHERE patientId='$patientId'";
//    $patientResult = mysql_query($patientSql);
	$patientResult = mysqli_query($con, $patientSql);
//    if(mysql_num_rows($patientResult)>0){
	if (@mysqli_num_rows($patientResult) > 0) {
		//take all values in an array var
		//        $patientRsArray = mysql_fetch_array($patientResult);
		$patientRsArray = @mysqli_fetch_array($patientResult);
		//foreach loop for all fields
		foreach ($patientRsArray as $key => $value) {
			//the above array brings numbers unwanted
			if (!is_numeric($key) and $key != "") {
				//remove commas
				$value = str_replace("'", "", $value);
				//initialize variables using eval
				eval("$" . $key . " = '" . $value . "';");
			}
		}
		$btnValue = "Update";
	}
	//extra field mods
	if ($patientAppDate != "") {
		$patientAppDateEx = explode(" ", $patientAppDate);
		$patientAppDate = formatDate($patientAppDateEx[0]);
		$patientAppTime = $patientAppDateEx[1] . " " . $patientAppDateEx[2];
		$patientAppTime = changeTime($patientAppTime, 12);
		$patientAppTimeEx = explode(":", $patientAppTime);
		$patientAppTimeHours = $patientAppTimeEx[0];
		$patientAppTimeEx = explode(" ", $patientAppTimeEx[1]);
		$patientAppTimeMinutes = $patientAppTimeEx[0];
		$patientAppTimeAmPm = $patientAppTimeEx[1];
	}
	$tmpPatientDob = encrypt_decrypt('decrypt', $patientDob);
	if ($tmpPatientDob > 0) {
		$patientDobEx = explode("-", $tmpPatientDob);
		$patientDobYear = $patientDobEx[0];
		$patientDobMonth = $patientDobEx[1];
		$patientDobDay = $patientDobEx[2];
	}
	$tmpPatientFamilyDob = encrypt_decrypt('decrypt', $patientFamilyDob);
	if ($patientFamilyDob > 0) {
		$patientFamilyDobEx = explode("-", $tmpPatientFamilyDob);
		$patientFamilyDobYear = $patientFamilyDobEx[0];
		$patientFamilyDobMonth = $patientFamilyDobEx[1];
		$patientFamilyDobDay = $patientFamilyDobEx[2];
	}
}
//check for state tr alternate
$patientCountry = "United States"; //forcing US for now later it will be same as selected company country
if ($patientCountry == "United States") {
	$stateTrAlternate = 'style="display:none"';
	$stateTr = "";
} else {
	$stateTr = 'style="display:none"';
	$stateTrAlternate = "";
}
if ($act == "add") {
	//roles check (one can add or not)
	if ($moduleAll == 0 and $modulePatientsAdd == 0) {
		echo "<script>window.location='index.php?do=authorization'</script>";
	}
	//vars when adding
	$patientOrtho = "No";
	$patientEligibility = "Full";
	$headingTitle = "Add New Patient";
	$showPatientForm = 1;
}
//saving information
if ($_POST) {
	$showPatientForm = 1;
	//assign all posts to vars
	foreach ($_POST as $key => $value) {
		//initialize variables using eval
		eval("$" . $key . " = '" . sanitize($value) . "';");
	}
	//sending email when change status to New, Check and Updated
	if ($act == "add") {
		if ($patientAppDate != "") {
			$patientAppDate2 = date("m/d/Y", strtotime($patientAppDate));
			$patientAppTime2 = date("H:i:s a", strtotime(changeTime($patientAppTimeHours . ":" . $patientAppTimeMinutes . " " . $patientAppTimeAmPm, 24)));
		} else {
			$patientAppDate2 = "00/00/0000";
			$patientAppTime2 = "00:00:00";
		}
		$addedBy = getField("cui_users", "userId", $_SESSION[$sessionUserID], "userFname") . " " . getField("cui_users", "userId", $_SESSION[$sessionUserID], "userLname");
		//sending email to new patient add
		// additional header pieces for errors, From cc's, bcc's, etc
		$headers = "From: Checkurinsurance <info@checkurinsurance.com>\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		//subject
		$subject = "New Patient '" . $patientFname . " " . $patientLname . " (" . html_entity_decode(getField("cui_companies", "companyId", $companyId, "companyName")) . " - " . getField("cui_companies_offices", "officeId", $officeId, "officeName") . ")' Added - " . $patientAppDate2 . " " . $patientAppTime2;
		//to email
		//$toEmail = "info@checkurinsurance.com";
		//$toEmail = "nomanmustafa.khan@gmail.com";
		//message
		$message = "Dear Admin,<br><br>";
		$message .= "The new CUI patient <b>" . $patientFname . " " . $patientLname . " (" . getField("cui_companies", "companyId", $companyId, "companyName") . " - " . getField("cui_companies_offices", "officeId", $officeId, "officeName") . ")</b> has been added with <b>" . $patientStatus . "</b> status.<br>";
		$message .= "Appointment Date/Time:  " . $patientAppDate2 . " " . $patientAppTime2 . "<br><br>";
		$message .= "Added By:  " . $addedBy . "<br>";
		$message .= "Added Data/Time:  " . date('m/d/Y  H:i a') . " PDT.<br><br>";
		$message .= $httpHost;
		mail($toEmail, $subject, $message, $headers);
		//additinal email sending to Company users
		$companyEmails = getField("cui_companies", "companyId", $_SESSION["tmpSessionCompanyId"], "companyRecipientEmails", " and companyStatus='1'");
		$tmpSplit = explode(",", $companyEmails);
		if (count($tmpSplit) > 0) {
			for ($s = 0; $s < count($tmpSplit); $s++) {
				//echo $tmpSplit[$s]."<br>";
				mail($tmpSplit[$s], $subject, $message, $headers);
			}
		}
	} else {
		if (getField("cui_patients", "patientId", $patientId, "patientAppDate") != "" && getField("cui_patients", "patientId", $patientId, "patientAppDate") != "0000-00-00 00:00:00") {
			$patientAppDate2 = date("m/d/Y", strtotime(getField("cui_patients", "patientId", $patientId, "patientAppDate")));
			$patientAppTime2 = date("H:i:s a", strtotime(getField("cui_patients", "patientId", $patientId, "patientAppDate")));
		} else {
			$patientAppDate2 = "00/00/0000";
			$patientAppTime2 = "00:00:00";
		}
		//first check the previous status
		//		$sqlCheckStatus = mysql_query("Select patientStatus from cui_patients WHERE patientId='$patientId'");
		$sqlCheckStatus = mysqli_query($con, "Select patientStatus from cui_patients WHERE patientId='$patientId'");
//		$rsStatus = mysql_result($sqlCheckStatus,0,"patientStatus");
		$rsStatus = mysqli_result($sqlCheckStatus, 0, "patientStatus");
		if ($_POST["saveExit"] != "" || $_POST["addClone"] != "") {
			if ($patientStatus == "New" || $patientStatus == "Check" || $patientStatus == "Updated") {
				$updatedBy = getField("cui_users", "userId", $_SESSION[$sessionUserID], "userFname") . " " . getField("cui_users", "userId", $_SESSION[$sessionUserID], "userLname");
				//sending email to exiting patient after change the status only
				// additional header pieces for errors, From cc's, bcc's, etc
				$headers = "From: Checkurinsurance <info@checkurinsurance.com>\r\n";
				$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
				//subject
				$subject = "Patient '" . $patientFname . " " . $patientLname . " (" . getField("cui_companies", "companyId", $companyId, "companyName") . " - " . getField("cui_companies_offices", "officeId", $officeId, "officeName") . ")' Status Has Been Changed to " . $patientStatus . " - " . $patientAppDate2 . " " . $patientAppTime2;
				//$toEmail = "info@checkurinsurance.com";
				//$toEmail = "nomanmustafa.khan@gmail.com";
				//message
				$message = "Dear Admin,<br><br>";
				$message .= "The patient <b>" . $patientFname . " " . $patientLname . " (ID: " . $patientId . ")</b> status has been changed to " . $patientStatus . ".<br>";
				$message .= "Appointment Date/Time:  " . $patientAppDate2 . " " . $patientAppTime2 . "<br>";
				$message .= "Updated By:  " . $updatedBy . "<br>";
				$message .= "Updated Date/Time:  " . date('m/d/Y  H:i a') . " PDT.<br><br>";
				$message .= $httpHost;
				//$message .= "Checkurinsurance.com";
				//echo $message; exit;
				if ($patientStatus != $rsStatus) {
					mail($toEmail, $subject, $message, $headers);
					//additinal email sending to Company users
					$companyEmails = getField("cui_companies", "companyId", $_SESSION["tmpSessionCompanyId"], "companyRecipientEmails", " and companyStatus='1'");
					$tmpSplit = explode(",", $companyEmails);
					if (count($tmpSplit) > 0) {
						for ($s = 0; $s < count($tmpSplit); $s++) {
							//echo $tmpSplit[$s]."<br>";
							mail($tmpSplit[$s], $subject, $message, $headers);
						}
					}
				}
			}
		}
	}
	//to exclude
	$excludeCounter = -1;
	$excludeArray[$excludeCounter += 1] = "clonePatient";
	$excludeArray[$excludeCounter += 1] = "addClone";
	$excludeArray[$excludeCounter += 1] = "addNew";
	$excludeArray[$excludeCounter += 1] = "saveExit";
	$excludeArray[$excludeCounter += 1] = "saveContinue";
	$excludeArray[$excludeCounter += 1] = "patientDobMonth";
	$excludeArray[$excludeCounter += 1] = "patientDobDay";
	$excludeArray[$excludeCounter += 1] = "patientDobYear";
	$excludeArray[$excludeCounter += 1] = "patientFamilyDobMonth";
	$excludeArray[$excludeCounter += 1] = "patientFamilyDobDay";
	$excludeArray[$excludeCounter += 1] = "patientFamilyDobYear";
	$excludeArray[$excludeCounter += 1] = "patientAppTimeHours";
	$excludeArray[$excludeCounter += 1] = "patientAppTimeMinutes";
	$excludeArray[$excludeCounter += 1] = "patientAppTimeAmPm";
	$excludeArray[$excludeCounter += 1] = "patientStateAlternate";
	$excludeArray[$excludeCounter += 1] = "patientStatusOld";
	//to include
	$_POST["patientCountry"] = "United States";
	$_POST["patientDob"] = encrypt_decrypt('encrypt', $patientDobYear . "-" . $patientDobMonth . "-" . $patientDobDay);
	$_POST["patientFamilyDob"] = encrypt_decrypt('encrypt', $patientFamilyDobYear . "-" . $patientFamilyDobMonth . "-" . $patientFamilyDobDay);
	if ($_POST["patientAppDate"] != "") {
		$patientAppDate = formatDate($patientAppDate, 2) . " " . changeTime($patientAppTimeHours . ":" . $patientAppTimeMinutes . " " . $patientAppTimeAmPm, 24);
		//manage appointment date
		//$d = explode("-",$patientAppDate);
		$pd = explode(" ", $patientAppDate);
		$pos = strpos($_POST["patientAppDate"], "/");
		if ($pos === false) {
			$pp = explode("-", $_POST["patientAppDate"]);
		} else {
			$pp = explode("/", $_POST["patientAppDate"]);
		}
		$pdatee = $pp[2] . "-" . $pp[0] . "-" . $pp[1];
		$patientAppDate = $pdatee . " " . $pd[1];
	}
	/*echo $patientAppDate;
    exit;*/
	/*echo $patientAppDate;
		    echo "<br>p ".$_POST["patientAppDate"];
		    exit;
		    if(isset($_GET["act"]) && $_GET["act"]!="edit")
		    {
		        $patientAppDate = $d[3]."-".$d[1]."-".$d[2]." ".str_replace("0 ","",$d[4]);
	*/
	$_POST["patientSubId"] = encrypt_decrypt('encrypt', $patientSubId);
	$_POST["patientSubSS"] = encrypt_decrypt('encrypt', $patientSubSS);
	$_POST["patientGroup"] = encrypt_decrypt('encrypt', $patientGroup);
	$_POST["patientAppDate"] = $patientAppDate;
	//Some data doesn't get inserted so we skip sanitize
	$skipSanitizeArray = array(
		'patientInstructions',
	);
	if ($act == "add") {
		$patientId = fastInsertUpdate("insert", "cui_patients", $_POST, $excludeArray, '', '', $skipSanitizeArray);
		if ($patientId > 0) //if($patientId == "success")
		{
			//updating files with patient id
			$tmp_sess_id = session_id();
			$sql_update = "Update cui_patients_files set patientId='$patientId' where session_id='$tmp_sess_id'";
//			mysql_query($sql_update);
			mysqli_query($con, $sql_update);
			$redirect = 1;
			$msg = "Patient data has been saved!";
		} else {
			$error = $insertFunction;
		}
	} else {
		$updateFunction = fastInsertUpdate("update", "cui_patients", $_POST, $excludeArray, " WHERE patientId='$patientId'", '', $skipSanitizeArray);
		if ($updateFunction == "success") {
			$redirect = 1;
			$msg = "Patient data has been updated!";
		} else {
			$error = $updateFunction;
		}
	}
	//disabled by Noman - May-17-2015  -  if clone patient
	/*if($addClone != "")
	{
		$patientCloneId = fastInsertUpdate("insert","cui_patients",$_POST, $excludeArray, '', '', $skipSanitizeArray);
		//now update the patient Name and DOB as empty
		if($patientCloneId > 0)
		{
			$sqlPatientUpdate = mysql_query("Update cui_patients set patientFname='', patientLname='', patientDob='' where patientId='$patientCloneId'");
		}
	}*/
	//if redirect
	if ($redirect == 1) {
		//Look for status "Check" and create history
		//disabled following line on Aug-31-2014 by Noman
		//if($patientStatus == "Check" and $patientStatusOld != "Check")
		if ($patientStatus != "") {
			//additional information to save
			$patientEligibility = sanitize($_POST["patientEligibility"]);
			$patientOrtho = sanitize($_POST["patientOrtho"]);
			// insert history
			if ($patientEligibility != "Custom") {
				if ($patientEligibility == "Partial") {
					$partial = 1;
					$full = 0;
				} else {
					$full = 1;
					$partial = 0;
				}
				if ($patientOrtho == "Yes") {
					$ortho = 1;
				} else {
					$ortho = 0;
				}
			} else {
				$custom = 1;
				$full = 0;
				$partial = 0;
				$ortho = 0;
			}
			//creating REMARKS
			$remarks = "";
			$ramarksEligibility = "";
			if ($_SESSION["tmpSessionCompanyId"] == "16"
				|| $_SESSION["tmpSessionCompanyId"] == "20"
				|| $_SESSION["tmpSessionCompanyId"] == "21"
				|| $_SESSION["tmpSessionCompanyId"] == "22"
				|| $_SESSION["tmpSessionCompanyId"] == "27"
				|| $_SESSION["tmpSessionCompanyId"] == "29"
				|| $_SESSION["tmpSessionCompanyId"] == "33"
				|| $_SESSION["tmpSessionCompanyId"] == "30"
				|| $_SESSION["tmpSessionCompanyId"] == "34"
				|| $_SESSION["tmpSessionCompanyId"] == "83"
				|| $_SESSION["tmpSessionCompanyId"] == "88"
				|| $_SESSION["tmpSessionCompanyId"] == "103"
				|| $_SESSION["tmpSessionCompanyId"] == "113"
				|| $_SESSION["tmpSessionCompanyId"] == "114"
				|| $_SESSION["tmpSessionCompanyId"] == "115"
				|| $_SESSION["tmpSessionCompanyId"] == "118"
				|| $_SESSION["tmpSessionCompanyId"] == "119"
				|| $_SESSION["tmpSessionCompanyId"] == "120"
				|| $_SESSION["tmpSessionCompanyId"] == "121"
				|| $_SESSION["tmpSessionCompanyId"] == "122"
				|| $_SESSION["tmpSessionCompanyId"] == "124"
				|| $_SESSION["tmpSessionCompanyId"] == "125"
				|| $_SESSION["tmpSessionCompanyId"] == "126"
				|| $_SESSION["tmpSessionCompanyId"] == "127"
				|| $_SESSION["tmpSessionCompanyId"] == "128"
				|| $_SESSION["tmpSessionCompanyId"] == "130"
				|| $_SESSION["tmpSessionCompanyId"] == "131"
				|| $_SESSION["tmpSessionCompanyId"] == "132"
				|| $_SESSION["tmpSessionCompanyId"] == "133"
				|| $_SESSION["tmpSessionCompanyId"] == "134"
				|| $_SESSION["tmpSessionCompanyId"] == "135"
				|| $_SESSION["tmpSessionCompanyId"] == "136"
				|| $_SESSION["tmpSessionCompanyId"] == "137"
				|| $_SESSION["tmpSessionCompanyId"] == "138"
				|| $_SESSION["tmpSessionCompanyId"] == "139"
				|| $_SESSION["tmpSessionCompanyId"] == "140"
				|| $_SESSION["tmpSessionCompanyId"] == "141"
				|| $_SESSION["tmpSessionCompanyId"] == "142"
				|| $_SESSION["tmpSessionCompanyId"] == "143"
				|| $_SESSION["tmpSessionCompanyId"] == "144"
				|| $_SESSION["tmpSessionCompanyId"] == "145"
				|| $_SESSION["tmpSessionCompanyId"] == "146"
				|| $_SESSION["tmpSessionCompanyId"] == "147"
				|| $_SESSION["tmpSessionCompanyId"] == "148"
				|| $_SESSION["tmpSessionCompanyId"] == "149"
				|| $_SESSION["tmpSessionCompanyId"] == "150"
				|| $_SESSION["tmpSessionCompanyId"] == "151"
				|| $_SESSION["tmpSessionCompanyId"] == "152"
				|| $_SESSION["tmpSessionCompanyId"] == "153"
				|| $_SESSION["tmpSessionCompanyId"] == "154"
				|| $_SESSION["tmpSessionCompanyId"] == "155"
				|| $_SESSION["tmpSessionCompanyId"] == "156"
				|| $_SESSION["tmpSessionCompanyId"] == "157"
				|| $_SESSION["tmpSessionCompanyId"] == "158"
				|| $_SESSION["tmpSessionCompanyId"] == "159"
				|| $_SESSION["tmpSessionCompanyId"] == "160"
				|| $_SESSION["tmpSessionCompanyId"] == "161"
				|| $_SESSION["tmpSessionCompanyId"] == "162"
				|| $_SESSION["tmpSessionCompanyId"] == "163"
				|| $_SESSION["tmpSessionCompanyId"] == "164"
				|| $_SESSION["tmpSessionCompanyId"] == "165"
				|| $_SESSION["tmpSessionCompanyId"] == "166"
				|| $_SESSION["tmpSessionCompanyId"] == "167"
				|| $_SESSION["tmpSessionCompanyId"] == "168"
				|| $_SESSION["tmpSessionCompanyId"] == "169"
				|| $_SESSION["tmpSessionCompanyId"] == "170"
				|| $_SESSION["tmpSessionCompanyId"] == "172"
				|| $_SESSION["tmpSessionCompanyId"] == "173"
				|| $_SESSION["tmpSessionCompanyId"] == "174"
				|| $_SESSION["tmpSessionCompanyId"] == "175"
				|| $_SESSION["tmpSessionCompanyId"] == "176"
				|| $_SESSION["tmpSessionCompanyId"] == "177"
				|| $_SESSION["tmpSessionCompanyId"] == "178") {
				$ramarksEligibility .= " for";
				if ($patientEligibility == "Custom") {
					$ramarksEligibility .= " Custom";
				}
				if ($patientEligibility == "Partial") {
					$ramarksEligibility .= " Partial";
				}
				$ramarksEligibility .= " bd";
			} else {
				if ($patientEligibility == "Custom") {
					$ramarksEligibility .= " for Custom bd";
				} else {
					if ($patientEligibility != "" || $patientOrtho != "") {
						$ramarksEligibility .= " for";
						if ($patientEligibility == "Full") {
							if ($patientOrtho == "Yes") {
								$ramarksEligibility .= " Full + ";
							} else {
								$ramarksEligibility .= " Full";
							}
						}
						if ($patientEligibility == "Partial") {
							if ($patientOrtho == "Yes") {
								$ramarksEligibility .= " Partial + ";
							} else {
								$ramarksEligibility .= " Partial";
							}
						}
						if ($patientOrtho == "Yes") {
							$ramarksEligibility .= " Ortho";
						}
						$ramarksEligibility .= " bd";
					} else {
						$ramarksEligibility .= "";
					}
				}
			}
			if ($act == "add") {
				if ($patientStatus == "New") {
					$remarks = "Patient added with status " . $patientStatus;
				} else {
					$remarks = "Patient added with status " . $patientStatus . $ramarksEligibility;
				}
			} else {
				if ($patientStatus != $rsStatus) {
					$remarks = "Patient changed from " . $rsStatus . " to " . $patientStatus . $ramarksEligibility;
				} else {
					$remarks = "Patient updated with status " . $patientStatus . $ramarksEligibility;
				}
			}
			//echo $remarks; exit;
			$sqlHistory = "INSERT INTO cui_patients_history (patientId, custom, full, partial, ortho, checkerId, checkedDate,remarks) values ('$patientId', '$custom', '$full', '$partial', '$ortho', '$_SESSION[$sessionUserID]', '" . time() . "','$remarks')";
			//only saving history when Save & Exit clicked.
			if ($act == "add") {
//				mysql_query($sqlHistory);
				mysqli_query($con, $sqlHistory);
			} else {
				//if($_POST["saveExit"] != "")
				if ($_POST["saveExit"] != "" || $_POST["addClone"] != "") {
//					mysql_query($sqlHistory);
					mysqli_query($con, $sqlHistory);
				}
			}
		}
		//now check redirect type
		if ($_POST["saveContinue"]) {
			$redirectPage = $do . "_eligibility&patientId=$patientId&patientStatus=$patientStatus";
			//finally redirect
			echo "<script>window.location='" . HTTP_SERVER . "index.php?do=$redirectPage&msg=$msg'</script>";
		} else {
			//added colone condition
			if ($addClone != "") {
				$patientFname = "";
				$patientLname = "";
				$patientDobYear = "";
				$patientDobMonth = "";
				$patientDobDay = "";
				$tmpPatientDob = $patientDob;
				if ($tmpPatientDob > 0) {
					$patientDobEx = explode("-", $tmpPatientDob);
					$patientDobYear = intval($patientDobEx[0]);
					$patientDobMonth = intval($patientDobEx[1]);
					$patientDobDay = intval($patientDobEx[2]);
				}
				$tmpPatientSubId = $patientSubId;
				$tmpPatientSubSS = $patientSubSS;
				$tmpPatientGroup = $patientGroup;
				//and then stop the redirection
			} else {
				if ($_POST["addNew"]) {
					$redirectPage = $do . "&act=add";
				} else {
					$redirectPage = $do . "&patientStatus=$patientStatus";
				}
				//$redirectPage = $do;
				//finally redirect
				echo "<script>window.location='" . HTTP_SERVER . "index.php?do=$redirectPage&msg=$msg'</script>";
			}
		}
	}
}
//filters
if (isset($_GET["filter"])) {
	$filter = sanitize($_GET["filter"]);
	$filterText = sanitize(trim($_GET["filterText"]));
	$filter2 = sanitize($_GET["filter2"]);
	$filterText2 = sanitize(trim($_GET["filterText2"]));
	$filterAndOr = sanitize($_GET["filterAndOr"]);
//	$queryString .= "&filter=$filter&filterText=$filterText";
	$queryString .= "&filter=$filter&filterText=$filterText&filter2=$filter2&filterText2=$filterText2&filterAndOr=$filterAndOr";
} else {
	$filterText = "";
	$filter = "";
	$filterText2 = "";
	$filter2 = "";
	$filterAndOr = "";
}
?>
    <?php if ($msg != "") {?>
        <div class="success"><?=$msg?></div>
    <?}?>
    <script type="text/javascript" language="JavaScript">
        function chkForm() {
            var phoneRE = /^\d{3}-\d{3}-\d{4}$/;
            var ssRE = /^\d{3}-\d{2}-\d{4}$/;
            if (document.getElementById("companyId").value == "") {
                /*alert('Please select a value for "Company"');
                document.getElementById("companyId").focus();*/
				swal({
					title: 'Error!',
					text: 'Please select a value for "Company"',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#companyId").focus();
				});
                return false;
            }
            if (document.getElementById("officeId").value == "") {
                /*alert('Please select a value for "Office"');
                document.getElementById("officeId").focus();*/
				swal({
					title: 'Error!',
					text: 'Please select a value for "Office"',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#officeId").focus();
				});
                return false;
            }
            if (document.getElementById("patientFname").value == "") {
                /*alert('Please enter a value for "First Name"');
                document.getElementById("patientFname").focus();*/
				swal({
					title: 'Error!',
					text: 'Please enter a value for "First Name"',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#patientFname").focus();
				});
                return false;
            }
            if (document.getElementById("patientLname").value == "") {
                /*alert('Please enter a value for "Last Name"');
                document.getElementById("patientLname").focus();*/
				swal({
					title: 'Error!',
					text: 'Please enter a value for "Last Name"',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#patientLname").focus();
				});
                return false;
            }
            if (document.getElementById("patientDobMonth").value == "") {
                /*alert('Please select a value for "DOB Month"');
                document.getElementById("patientDobMonth").focus();*/
				swal({
					title: 'Error!',
					text: 'Please select a value for "DOB Month"',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#patientDobMonth").focus();
				});
                return false;
            }
            if (document.getElementById("patientDobDay").value == "") {
                /*alert('Please select a value for "DOB Day"');
                document.getElementById("patientDobDay").focus();*/
				swal({
					title: 'Error!',
					text: 'Please select a value for "DOB Day"',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#patientDobDay").focus();
				});
                return false;
            }
            if (document.getElementById("patientDobYear").value == "") {
                /*alert('Please select a value for "DOB Year"');
                document.getElementById("patientDobYear").focus();*/
				swal({
					title: 'Error!',
					text: 'Please select a value for "DOB Year"',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#patientDobYear").focus();
				});
                return false;
            }
            //if(document.getElementById("patientPhoneHome").value=="" || document.getElementById("patientPhoneHome").value=="XXX-XXX-XXXX"){
            //  alert('Please enter a value for "Phone (Home)"');
            //  document.getElementById("patientPhoneHome").focus();
            //  document.getElementById("patientPhoneHome").value="";
            //  return false;
            // }
            if (document.getElementById("patientPhoneHome").value != "") {
                if (!phoneRE.test(document.getElementById("patientPhoneHome").value) && document.getElementById("patientPhoneHome").value != "XXX-XXX-XXXX") {
                    /*alert("Phone (Home) is in invalid format! (Correct Format: XXX-XXX-XXXX)");
                    document.getElementById("patientPhoneHome").focus();*/
					swal({
						title: 'Error!',
						text: 'Phone (Home) is in invalid format! (Correct Format: XXX-XXX-XXXX)',
						type: "error",
						showCancelButton: false,
						confirmButtonText: 'OK',
						closeOnConfirm: false
					},
					function(){
						swal.close();
						$("#patientPhoneHome").focus();
					});
                    return false;
                }
            }
            if (document.getElementById("patientPhoneWork").value != "") {
                if (!phoneRE.test(document.getElementById("patientPhoneWork").value) && document.getElementById("patientPhoneWork").value != "XXX-XXX-XXXX") {
                    /*alert("Phone (Work) is in invalid format! (Correct Format: XXX-XXX-XXXX)");
                    document.getElementById("patientPhoneWork").focus();*/
					swal({
						title: 'Error!',
						text: 'Phone (Work) is in invalid format! (Correct Format: XXX-XXX-XXXX)',
						type: "error",
						showCancelButton: false,
						confirmButtonText: 'OK',
						closeOnConfirm: false
					},
					function(){
						swal.close();
						$("#patientPhoneWork").focus();
					});
                    return false;
                }
            }
            if (document.getElementById("patientPhoneCell").value != "") {
                if (!phoneRE.test(document.getElementById("patientPhoneCell").value) && document.getElementById("patientPhoneCell").value != "XXX-XXX-XXXX") {
                    /*alert("Phone (Cell) is in invalid format! (Correct Format: XXX-XXX-XXXX)");
                    document.getElementById("patientPhoneCell").focus();*/
					swal({
						title: 'Error!',
						text: 'Phone (Cell) is in invalid format! (Correct Format: XXX-XXX-XXXX)',
						type: "error",
						showCancelButton: false,
						confirmButtonText: 'OK',
						closeOnConfirm: false
					},
					function(){
						swal.close();
						$("#patientPhoneCell").focus();
					});
                    return false;
                }
            }
            if (document.getElementById("patientEmail").value != "") {
                if (document.getElementById("patientEmail").value != "") {
                    var str = document.getElementById("patientEmail").value
                    var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
                    if (filter.test(str))
                        testresults = true
                    else {
                        /*alert('Email Address is invalid!')
                        document.getElementById("patientEmail").focus();*/
						swal({
							title: 'Error!',
							text: 'Email Address is invalid!',
							type: "error",
							showCancelButton: false,
							confirmButtonText: 'OK',
							closeOnConfirm: false
						},
						function(){
							swal.close();
							$("#patientEmail").focus();
						});
                        return false;
                    }
                }
            }
            if (document.getElementById("patientInsCompany").value == "") {
                /*alert('Please enter a value for "Insurance Company"');
                document.getElementById("patientInsCompany").focus();*/
				swal({
					title: 'Error!',
					text: 'Please enter a value for "Insurance Company"',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#patientInsCompany").focus();
				});
                return false;
            }
            if (document.getElementById("patientInsPhone").value == "" || document.getElementById("patientInsPhone").value == "XXX-XXX-XXXX") {
                /*alert('Please enter a value for "Insurance Phone"');
                document.getElementById("patientInsPhone").focus();*/
				swal({
					title: 'Error!',
					text: 'Please enter a value for "Insurance Phone"',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#patientInsPhone").focus();
				});
                return false;
            }
            if (!phoneRE.test(document.getElementById("patientInsPhone").value) && document.getElementById("patientInsPhone").value != "XXX-XXX-XXXX") {
                /*alert("Insurance Phone is in invalid format! (Correct Format: XXX-XXX-XXXX)");
                document.getElementById("patientInsPhone").focus();*/
				swal({
					title: 'Error!',
					text: 'Insurance Phone is in invalid format! (Correct Format: XXX-XXX-XXXX)',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#patientInsPhone").focus();
				});
                return false;
            }
            if (document.getElementById("patientSubName").value == "") {
                /*alert('Please enter a value for "Subscriber Name"');
                document.getElementById("patientSubName").focus();*/
				swal({
					title: 'Error!',
					text: 'Please enter a value for "Subscriber Name"',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#patientSubName").focus();
				});
                return false;
            }
            if (document.getElementById("patientSubId").value == "") {
                /*alert('Please enter a value for "Subscriber ID"');
                document.getElementById("patientSubId").focus();*/
				swal({
					title: 'Error!',
					text: 'Please enter a value for "Subscriber ID"',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#patientSubId").focus();
				});
                return false;
            }
            /*if (document.getElementById("patientGroup").value == "") {
                swal({
					title: 'Error!',
					text: 'Please enter a value for "Group/Policy #"',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#patientGroup").focus();
				});
                return false;
            }*/
            if (document.getElementById("patientSubSS").value != "" && document.getElementById("patientSubSS").value != "XXX-XX-XXXX") {
                if (!ssRE.test(document.getElementById("patientSubSS").value)) {
                    /*alert("Subscriber SS# is in invalid format! (Correct Format: XXX-XX-XXXX)");
                    document.getElementById("patientSubSS").focus();*/
					swal({
						title: 'Error!',
						text: 'Subscriber SS# is in invalid format! (Correct Format: XXX-XX-XXXX)',
						type: "error",
						showCancelButton: false,
						confirmButtonText: 'OK',
						closeOnConfirm: false
					},
					function(){
						swal.close();
						$("#patientSubSS").focus();
					});
                    return false;
                }
            }
            if (document.getElementById("patientFamilyDobMonth").value == "") {
                /*alert('Please select a value for "Family DOB Month"');
                document.getElementById("patientFamilyDobMonth").focus();*/
				swal({
					title: 'Error!',
					text: 'Please select a value for "Subscriber DOB Month"',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#patientFamilyDobMonth").focus();
				});
                return false;
            }
            if (document.getElementById("patientFamilyDobDay").value == "") {
                /*alert('Please select a value for "Family DOB Day"');
                document.getElementById("patientFamilyDobDay").focus();*/
				swal({
					title: 'Error!',
					text: 'Please select a value for "Subscriber DOB Day"',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#patientFamilyDobDay").focus();
				});
                return false;
            }
            if (document.getElementById("patientFamilyDobYear").value == "") {
                /*alert('Please select a value for "Family DOB Year"');
                document.getElementById("patientFamilyDobYear").focus();*/
				swal({
					title: 'Error!',
					text: 'Please select a value for "Subscriber DOB Year"',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#patientFamilyDobYear").focus();
				});
                return false;
            }
            if (document.getElementById("doctorId").value == "") {
                //alert('Please select a value for "Patient Doctor"');
				//document.getElementById("doctorId").focus();
				swal({
					title: 'Error!',
					text: 'Please select a value for "Patient Doctor"',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#doctorId").focus();
				});
                return false;
            }
            if (document.getElementById("patientAppTimeHours").value == "" || document.getElementById("patientAppTimeMinutes").value == "" || document.getElementById("patientAppDate").value == "") {

				//return
				/*swal({
					title: 'Confirmation!',
					text: 'No apt date/time has been set for the patient. Do you want to continue?',
					type: "warning",
					showCancelButton: true,
					confirmButtonText: 'Yes, Proceed',
					cancelButtonText: 'No, Cancel',
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm) {
					if (isConfirm) {
						swal.close();
						return true;
					 } else {
						swal.close();
						$("#patientAppTimeHours").focus();
						$("#patientAppTimeMinutes").focus();
						$("#patientAppDate").focus();
						return false;
					}
				});*/
				//break;
				//return false;
				if (confirm("No apt date/time has been set for the patient. Do you want to continue?")) {
                    return true;
                } else {
                    return false;
                }
            }
            //in the end convert all phones that are XXX-XXX-XXXX to blank
            if (document.getElementById("patientPhoneHome").value == "XXX-XXX-XXXX") {
                document.getElementById("patientPhoneHome").value = "";
            }
            if (document.getElementById("patientInsPhone").value == "XXX-XXX-XXXX") {
                document.getElementById("patientInsPhone").value = "";
            }
            if (document.getElementById("patientPhoneWork").value == "XXX-XXX-XXXX") {
                document.getElementById("patientPhoneWork").value = "";
            }
            if (document.getElementById("patientPhoneCell").value == "XXX-XXX-XXXX") {
                document.getElementById("patientPhoneCell").value = "";
            }
            if (document.getElementById("patientSubSS").value == "XXX-XX-XXXX") {
                document.getElementById("patientSubSS").value = "";
            }
        }
        //update patient hidden
        function updatePatientHidden(pid) {
            if (document.getElementById("patientId")) {
                document.getElementById("patientId").value = pid;
            }
        }


        function checksamepatient12() {
            //alert("asdfasdf");


            var companyId = $("#companyId").val();


            var patientFname = $("#patientFname").val();
            var patientLname = $("#patientLname").val();
            var patientDobMonth = $("#patientDobMonth").val();
            var patientDobDay = $("#patientDobDay").val();
            var patientDobYear = $("#patientDobYear").val();
            var officeId = $("#officeId").val();
            var removepopup = $("#removepopup").val();

            if (companyId != '' && patientFname != '' && patientLname != '' && patientDobYear != '' && patientDobMonth != '' && patientDobDay != '' && officeId != '' && removepopup == 0) {

                var patientDob = patientDobYear + '-' + patientDobMonth + '-' + patientDobDay;

                result = $.ajax({

                    type: "GET",

                    url: "<?php echo $httpsServer; ?>ajax.php",

                    data: {
                        json: 'check-same-patient',
                        patientDob: patientDob,
                        patientFname: patientFname,
                        patientLname: patientLname,
                        companyId: companyId,
                        officeId: officeId
                    },

                    async: false

                }).responseText.split('||');

                if (result.length > 1) {


                    $("#div_dialog").dialog({
                        title: "",
                        buttons: {
                            "Continue": function () {
                                /*var redirectUrl = 'payments-automatic.html?act=cancelEftProcess';
                                 window.location = redirectUrl;*/
                                //document.getElementById("removepopup").value = 1;
                                //$(this).dialog("close");
                            }
                        }
                    });
                    $("#div_dialog").dialog("option", "width", "650px");
                    $("#div_dialog").html("<p>Patient already exist in database with same first name,last name, company and data of birth</p><table width='100%' border='1'><tr><td>Patient ID</td><td>Patient Name</td></tr><tr><td>" + result[1] + "</td><td>" + result[0] + "</td></tr></table><div class='button12'><a href='<?php echo HTTP_SERVER; ?>index.php?do=patients&act=edit&patientId=" + result[1] + "'>Go to existing patient</a></div><div class='button13'><a href='javascript:' onclick=\"closeddiv();\">Continue adding new patient</a></div>");

                    $("#div_dialog").dialog("open");
                    $(".ui-dialog-titlebar-close").hide();
                    $(".ui-button").hide();
                }

            }

        }

        function closeddiv() {
            document.getElementById("removepopup").value = 1;
            $(".ui-dialog").hide();
        }

    </script>
	<style>
		hr {
			border-bottom: 0px;
		}
		table.form-spacing tbody tr td {
			padding-bottom: 9px;
		}
		</style>
    <?
//include functions 2 file
include SITE_ROOT . 'includes/functions_2.php';
?>
    <?if ($viewPatient == 1) {
	?>
        <div id="viewPatient" style="width: 960px;">
            <table class="form-spacing" align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                <tr class="titleTr">
                    <td colspan="2"><h3 style="padding-top: 9px; padding-left: 5px;"><?=$headingTitle?></h3></td>
                </tr>
				<tr>
                    <td height="3"></td>
                </tr>
                <tr>
                    <td>
                        <?if ($checkCustom == "1") {
		?>
                            <input type="button" class="btn searchbt" value="View General"
                                   onclick="javascript: show('general')"/>
                            <?if ($patientCustomDone == "1") {?>
                                <input type="button" class="btn searchbt" value="View Custom"
                                       onclick="javascript: show('<?=$_SESSION["tmpSessionCompanyTag"]?>')"/>
                            <?}?>
                            <?if ($_SESSION["tmpSessionCompanyId"] == "16"
			|| $_SESSION["tmpSessionCompanyId"] == "20"
			|| $_SESSION["tmpSessionCompanyId"] == "21"
			|| $_SESSION["tmpSessionCompanyId"] == "22"
			|| $_SESSION["tmpSessionCompanyId"] == "27"
			|| $_SESSION["tmpSessionCompanyId"] == "29"
			|| $_SESSION["tmpSessionCompanyId"] == "30"
			|| $_SESSION["tmpSessionCompanyId"] == "33"
			|| $_SESSION["tmpSessionCompanyId"] == "34"
			|| $_SESSION["tmpSessionCompanyId"] == "83"
			|| $_SESSION["tmpSessionCompanyId"] == "88"
			|| $_SESSION["tmpSessionCompanyId"] == "103"
			|| $_SESSION["tmpSessionCompanyId"] == "113"
			|| $_SESSION["tmpSessionCompanyId"] == "114"
			|| $_SESSION["tmpSessionCompanyId"] == "115"
			|| $_SESSION["tmpSessionCompanyId"] == "118"
			|| $_SESSION["tmpSessionCompanyId"] == "119"
			|| $_SESSION["tmpSessionCompanyId"] == "120"
			|| $_SESSION["tmpSessionCompanyId"] == "121"
			|| $_SESSION["tmpSessionCompanyId"] == "122"
			|| $_SESSION["tmpSessionCompanyId"] == "124"
			|| $_SESSION["tmpSessionCompanyId"] == "125"
			|| $_SESSION["tmpSessionCompanyId"] == "126"
			|| $_SESSION["tmpSessionCompanyId"] == "127"
			|| $_SESSION["tmpSessionCompanyId"] == "128"
			|| $_SESSION["tmpSessionCompanyId"] == "130"
			|| $_SESSION["tmpSessionCompanyId"] == "131"
			|| $_SESSION["tmpSessionCompanyId"] == "132"
			|| $_SESSION["tmpSessionCompanyId"] == "133"
			|| $_SESSION["tmpSessionCompanyId"] == "134"
			|| $_SESSION["tmpSessionCompanyId"] == "135"
			|| $_SESSION["tmpSessionCompanyId"] == "136"
			|| $_SESSION["tmpSessionCompanyId"] == "137"
			|| $_SESSION["tmpSessionCompanyId"] == "138"
			|| $_SESSION["tmpSessionCompanyId"] == "139"
			|| $_SESSION["tmpSessionCompanyId"] == "140"
			|| $_SESSION["tmpSessionCompanyId"] == "141"
			|| $_SESSION["tmpSessionCompanyId"] == "142"
			|| $_SESSION["tmpSessionCompanyId"] == "143"
			|| $_SESSION["tmpSessionCompanyId"] == "144"
			|| $_SESSION["tmpSessionCompanyId"] == "145"
			|| $_SESSION["tmpSessionCompanyId"] == "146"
			|| $_SESSION["tmpSessionCompanyId"] == "147"
			|| $_SESSION["tmpSessionCompanyId"] == "148"
			|| $_SESSION["tmpSessionCompanyId"] == "149"
			|| $_SESSION["tmpSessionCompanyId"] == "150"
			|| $_SESSION["tmpSessionCompanyId"] == "151"
			|| $_SESSION["tmpSessionCompanyId"] == "152"
			|| $_SESSION["tmpSessionCompanyId"] == "153"
			|| $_SESSION["tmpSessionCompanyId"] == "154"
			|| $_SESSION["tmpSessionCompanyId"] == "155"
			|| $_SESSION["tmpSessionCompanyId"] == "156"
			|| $_SESSION["tmpSessionCompanyId"] == "157"
			|| $_SESSION["tmpSessionCompanyId"] == "158"
			|| $_SESSION["tmpSessionCompanyId"] == "159"
			|| $_SESSION["tmpSessionCompanyId"] == "160"
			|| $_SESSION["tmpSessionCompanyId"] == "161"
			|| $_SESSION["tmpSessionCompanyId"] == "162"
			|| $_SESSION["tmpSessionCompanyId"] == "163"
			|| $_SESSION["tmpSessionCompanyId"] == "164"
			|| $_SESSION["tmpSessionCompanyId"] == "165"
			|| $_SESSION["tmpSessionCompanyId"] == "166"
			|| $_SESSION["tmpSessionCompanyId"] == "167"
			|| $_SESSION["tmpSessionCompanyId"] == "168"
			|| $_SESSION["tmpSessionCompanyId"] == "169"
			|| $_SESSION["tmpSessionCompanyId"] == "170"
			|| $_SESSION["tmpSessionCompanyId"] == "172"
			|| $_SESSION["tmpSessionCompanyId"] == "173"
			|| $_SESSION["tmpSessionCompanyId"] == "174"
			|| $_SESSION["tmpSessionCompanyId"] == "175"
			|| $_SESSION["tmpSessionCompanyId"] == "176"
			|| $_SESSION["tmpSessionCompanyId"] == "177"
			|| $_SESSION["tmpSessionCompanyId"] == "178") {?>
                                <?if ($patientPartialDone == "1") {?>
                                    <input type="button" class="btn searchbt" value="View Partial"
                                           onclick="javascript: show('partial')"/>
                                <?}?>
                            <?}?>
                        <?} else {?>
                            <?if ($patientFullDone == "1" || $patientPartialDone == "1" || $patientOrthoDone == "1") {?>
                                <input type="button" class="btn searchbt" value="View General"
                                       onclick="javascript: show('general')"/>
                            <?}?>
                            <?if ($patientFullDone == "1") {?>
                                <input type="button" class="btn searchbt" value="View Full"
                                       onclick="javascript: show('full')"/>
                            <?}?>
                            <?if ($patientPartialDone == "1") {?>
                                <input type="button" class="btn searchbt" value="View Partial"
                                       onclick="javascript: show('partial')"/>
                            <?}?>
                            <?if ($patientOrthoDone == "1") {?>
                                <input type="button" class="btn searchbt" value="View Ortho"
                                       onclick="javascript: show('ortho')"/>
                            <?}?>
                            <?if ($patientFullDone == "1" || $patientPartialDone == "1" || $patientOrthoDone == "1") {?>
                                <input type="button" class="btn searchbt" value="View Special"
                                       onclick="javascript: show('special')"/>
                            <?}?>
                        <?}?>
                    </td>
                    <td align="right"><input type="button"
                                             onclick="javascript: window.location='index.php?do=patients&patientStatus=<?=$patientStatus?>'"
                                             class="btn clearbt" value="Back To Patients"/></td>
                </tr>
                <tr>
                    <td width="50%" align="left"></td>
                    <?if ($checkCustom == "1") {?>
                        <td width="50%" align="right"><strong>Office
                                Name:</strong> <?=getField("cui_companies_offices", "officeId", $officeId, "officeName")?>
                            <br><strong>Date /
                                Time:</strong> <?=getField("cui_patients_custom", "patientId", $patientId, "custDateTime")?>
                            <br><strong>Spoke
                                with:</strong> <?=getField("cui_patients_custom", "patientId", $patientId, "custSpokeWith")?>
                        </td>
                    <?} else {?>
                        <td width="50%" align="right"><strong>Office
                                Name:</strong> <?=getField("cui_companies_offices", "officeId", $officeId, "officeName")?>
                            <br><strong>Date /
                                Time:</strong> <?=getField("cui_patients_full", "patientId", $patientId, "fullDateTime")?>
                            <br><strong>Spoke
                                with:</strong> <?=getField("cui_patients_full", "patientId", $patientId, "spokeWith")?>
                        </td>
                    <?}?>
                </tr>
            </table>
            <?include SITE_ROOT . "view_patient.php";?>
        </div>
    <?}?>
    <style>
        .button12 {
            padding: 7px 10px;
            margin-top: 10px;
            width: 160px;
            text-align: center;
            background: #898161;
            border-radius: 4px;
            color: #fff;
            float: left;
        }

        .button13 {
            background: #898161 none repeat scroll 0 0;
            border-radius: 4px;
            color: #fff;
            float: left;
            margin-left: 10px;
            margin-top: 10px;
            padding: 7px 10px;
            text-align: center;
            width: 200px;
        }

        .button12 a {
            color: #fff;
        }

        .button13 a {
            color: #fff;
        }

    </style>
	<?php if ($act == 'add' || $act == 'edit') {
	?>
    <div id="addPatient" <?php if ($showPatientForm == 0) {?>style="display:none"<?}?>>
        <form method="POST" id="pForm1" name="pForm1">
            <table class="form-spacing" align="center" width="100%" border="0" cellpadding="5" cellspacing="1">
                <tr class="titleTr">
                    <td><h3 style="padding-top: 9px; padding-left: 5px;"><?=$headingTitle?></h3></td>
                </tr>
                <tr>
                    <td>
                        <?php if ($error != "") {?>
                            <div class="error"><?=$error?></div>
                        <?} else {?>
                            <div class="message">The required fields are marked with <span class="required">*</span>
                            </div>
                        <?}?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="85%" cellpadding="0" cellspacing="0" align="center">
                            <tr>
                                <td width="50%" valign="top">
                                    <!-- Left Row -->
                                    <table width="100%" align="center" cellpadding="3" cellspacing="0">
                                        <tr>
                                            <td width="160px">Select Company <span class="required">*</span></td>
                                            <td>
                                                <?php if ($act == "add") {
		?>
                                                    <?php
$companyId = $_SESSION["tmpSessionCompanyId"];
		$officeId = $_SESSION["tmpSessionOfficeId"];
		?>
                                                    <script type="text/javascript">
                                                        $(document).ready(function () {
                                                            <?if ($addClone == "") {?>
                                                            showDoctors(document.getElementById("officeId").value);
                                                            <?}?>
//	$('.company_for_trigger_first').trigger('change');
                                                        });
                                                    </script>
                                                <?}?>
                                                <select class="company_for_trigger_first" id="companyId"
                                                        name="companyId"
                                                        onchange="officeOptions(this.value, 0);<?php if ($act == 'add') {?>checksamepatient12();<?php }?>"
                                                        tabindex="1">
                                                    <?php
if ($moduleAll == 1 or $moduleCompaniesViewAll == 1) {
		//$companySql = "select * from cui_companies where companyStatus='1' order by companyName ASC";
		$companySql = "select * from cui_companies order by companyName ASC";
	} else {
		echo $companySql = "select distinct(a.companyId) from cui_companies as a join cui_users_companies as b where a.companyStatus='1' and a.companyId=b.companyId and b.userId='$sessionId'";

	}
	echo '<option value="">--- select ---</option>';
	//$companyResult = mysql_query($companySql);
	$companyResult = mysqli_query($con, $companySql);
	//while ($companyRs = mysql_fetch_array($companyResult)) {
	while ($companyRs = @mysqli_fetch_array($companyResult)) {
		$rsCompanyId = $companyRs["companyId"];
		$companyName = getField("cui_companies", "companyId", $rsCompanyId, "companyName");
		$sel = '';
		if ($companyId == $rsCompanyId) {
			$sel = "selected";
		}
		echo '<option ' . $sel . ' value="' . $rsCompanyId . '">' . $companyName . '</option>';
	}

	?>
                                                </select>
                                                <?php //echo $companySql; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="150px">Patient First Name <span class="required">*</span></td>
                                            <td><input type="text" class="textbox" id="patientFname" name="patientFname"
                                                       value="<?=$patientFname?>" tabindex="3"
                                                       onblur="<?php if ($act == 'add') {?>checksamepatient12();<?php }?>"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Date of Birth <span class="required">*</span></td>
                                            <td>
                                                <select name="patientDobMonth" id="patientDobMonth" tabindex="5"
                                                        onchange="<?php if ($act == 'add') {?>checksamepatient12();<?php }?>">
                                                    <option value="">MM</option>
                                                    <?
	for ($i = 1; $i < 13; $i++) {
		if ($i < 10) {
			$j = "0" . $i;
		} else {
			$j = $i;
		}
		$sel = "";
		if ($patientDobMonth == $j) {
			$sel = "selected";
		}
		echo "<option $sel value=\"$j\">$j</option>";
	}
	?>
                                                </select>
                                                <select name="patientDobDay" id="patientDobDay" tabindex="6"
                                                        onchange="<?php if ($act == 'add') {?>checksamepatient12();<?php }?>">
                                                    <option value="">DD</option>
                                                    <?
	for ($i = 1; $i < 32; $i++) {
		if ($i < 10) {
			$j = "0" . $i;
		} else {
			$j = $i;
		}
		$sel = "";
		if ($patientDobDay == $j) {
			$sel = "selected";
		}
		echo "<option $sel value=\"$j\">$j</option>";
	}
	?>
                                                </select>
                                                <select name="patientDobYear" id="patientDobYear" tabindex="7"
                                                        onchange="<?php if ($act == 'add') {?>checksamepatient12();<?php }?>">
                                                    <?
	$currentYear = date("Y");
	$tillYear = $currentYear - 100;
	if ($patientDobYear == "") {
		$patientDobYear = $currentYear - 50;
	}

	for ($i = $tillYear; $i <= $currentYear; $i++) {
		$sel = "";
		if ($patientDobYear == $i) {
			$sel = "selected";
		}
		echo "<option $sel value=\"$i\">$i</option>";
	}
	?>
                                                </select></td>
                                        </tr>
                                        <tr>
                                            <td width="150px">Address Line 2</td>
                                            <td><input type="text" class="textbox" id="patientAddress2"
                                                       name="patientAddress2" value="<?=$patientAddress2?>"
                                                       tabindex="9"/></td>
                                        </tr>
                                        <tr id="stateTr" <?=$stateTr?>>
                                            <td>State</td>
                                            <td>
                                                <select name="patientState" id="patientState" tabindex="11">
                                                    <option value="">--- select ---</option>
                                                    <?
	$stateSql = "SELECT * from cui_states order by stateName ASC";
	//$stateResult = mysql_query($stateSql);
	$stateResult = mysqli_query($con, $stateSql);
	//while ($stateRs = mysql_fetch_array($stateResult)) {
	while ($stateRs = @mysqli_fetch_array($stateResult)) {
		$getStateName = $stateRs["stateName"];
		if ($patientState == $getStateName) {
			$sel = "selected";
		} else {
			$sel = "";
		}
		echo "<option $sel value=\"$getStateName\">$getStateName</option>";
	}
	?>
                                                </select></td>
                                        </tr>
                                        <tr id="stateTrAlternate" <?=$stateTrAlternate?>>
                                            <td>State / Province <span class="required">*</span></td>
                                            <td><input type="text" name="patientStateAlternate"
                                                       id="patientStateAlternate" value="<?=$patientStateAlternate?>"
                                                       class="textbox"/></td>
                                        </tr>
                                        <tr>
                                            <td>Phone (Home)</td>
                                            <td><input type="text" class="textbox" id="patientPhoneHome"
                                                       name="patientPhoneHome"
                                                       value="<?=($patientPhoneHome ? $patientPhoneHome : "XXX-XXX-XXXX")?>"
                                                       tabindex="13"
                                                       onclick="if(this.value == 'XXX-XXX-XXXX'){this.value = ''}"
                                                       onblur="if(this.value == ''){this.value = 'XXX-XXX-XXXX'}"/></td>
                                        </tr>
                                        <tr>
                                            <td>Phone (Cell)</td>
                                            <td><input type="text" class="textbox" id="patientPhoneCell"
                                                       name="patientPhoneCell"
                                                       value="<?=($patientPhoneCell ? $patientPhoneCell : "XXX-XXX-XXXX")?>"
                                                       tabindex="15"
                                                       onclick="if(this.value == 'XXX-XXX-XXXX'){this.value = ''}"
                                                       onblur="if(this.value == ''){this.value = 'XXX-XXX-XXXX'}"/></td>
                                        </tr>
                                        <tr>
                                            <td>Insurance Company <span class="required">*</span></td>
                                            <td><input type="text" class="textbox" id="patientInsCompany"
                                                       name="patientInsCompany" value="<?=$patientInsCompany?>"
                                                       tabindex="17"/></td>
                                        </tr>
                                        <tr>
                                            <td>Insurance Address</td>
                                            <td><input type="text" class="textbox" id="patientInsAddress"
                                                       name="patientInsAddress" value="<?=$patientInsAddress?>"
                                                       tabindex="19"/></td>
                                        </tr>
                                        <tr>
                                            <td>Insurance State</td>
                                            <td>
                                                <select name="patientInsState" id="patientInsState" tabindex="21">
                                                    <option value="">--- select ---</option>
                                                    <?
	$stateSql = "SELECT * from cui_states order by stateName ASC";
	//$stateResult = mysql_query($stateSql);
	$stateResult = mysqli_query($con, $stateSql);
	//while ($stateRs = mysql_fetch_array($stateResult)) {
	while ($stateRs = @mysqli_fetch_array($stateResult)) {
		$getStateName = $stateRs["stateName"];
		if ($patientInsState == $getStateName) {
			$sel = "selected";
		} else {
			$sel = "";
		}
		echo "<option $sel value=\"$getStateName\">$getStateName</option>";
	}
	?>
                                                </select></td>
                                        </tr>
                                        <tr>
                                            <td>Subscriber Name <span class="required">*</span></td>
                                            <td><input type="text" class="textbox" id="patientSubName"
                                                       name="patientSubName" value="<?=$patientSubName?>"
                                                       tabindex="23"/></td>
                                        </tr>
                                        <tr>
                                            <td>Employer</td>
                                            <td><input type="text" class="textbox" id="patientEmployer"
                                                       name="patientEmployer" value="<?=$patientEmployer?>"
                                                       tabindex="25"/></td>
                                        </tr>
                                        <tr>
                                            <td>Subscriber SS #</td>
                                            <td><input type="text" class="textbox" id="patientSubSS" name="patientSubSS"
                                                       value="<?=($tmpPatientSubSS ? $tmpPatientSubSS : "XXX-XX-XXXX")?>"
                                                       tabindex="27"
                                                       onclick="if(this.value == 'XXX-XX-XXXX'){this.value = ''}"
                                                       onblur="if(this.value == ''){this.value = 'XXX-XX-XXXX'}"/></td>
                                        </tr>
                                        <tr>
                                            <td>Appointment Time / Date</td>
                                            <td>
                                                <select name="patientAppTimeHours" id="patientAppTimeHours"
                                                        tabindex="34">
                                                    <option value="">HH</option>
                                                    <?
	for ($i = 0; $i < 13; $i++) {
		$j = $i;
		if ($i < 10) {
			$j = "0" . $i;
		}
		$sel = "";
		if ($patientAppTimeHours == $j) {
			$sel = "selected";
		}
		echo "<option $sel value=\"$j\">$j</option>";
	}
	?>
                                                </select>
                                                <select name="patientAppTimeMinutes" id="patientAppTimeMinutes"
                                                        tabindex="35">
                                                    <option value="">MM</option>
                                                    <?
	for ($i = 0; $i < 46; $i += 15) {
		$j = $i;
		if ($i < 1) {
			$j = "0" . $i;
		}
		$sel = "";
		if ($patientAppTimeMinutes == $j) {
			$sel = "selected";
		}
		echo "<option $sel value=\"$j\">$j</option>";
	}
	?>
                                                </select><select name="patientAppTimeAmPm" id="patientAppTimeAmPm"
                                                                 tabindex="36">
                                                    <option <?php if ($patientAppTimeAmPm == "am") {?>selected<?}?>
                                                            value="am">am
                                                    </option>
                                                    <option <?php if ($patientAppTimeAmPm == "pm") {?>selected<?}?>
                                                            value="pm">pm
                                                    </option>
                                                </select> <input type="text" class="textbox" id="patientAppDate"
                                                                 name="patientAppDate"
                                                                 value="<?php if ($patientAppDate != "00/00/0000") {
		echo $patientAppDate;
	}?>" style="width: 60px" tabindex="31"
                                                                 readonly/><input type="button" class="smallButton"
                                                                                  value="Pick"
                                                                                  onclick="displayDatePicker('patientAppDate');"
                                                                                  tabindex="32"/></td>
                                        </tr>
                                        <tr>
                                            <td>Select Doctor <span class="required">*</span></td>
                                            <td>
                                                <?php /*?><select name="doctorId" id="doctorId" tabindex="38">
	<option value="">--- select ---</option>
	<?
	$doctorSql = "SELECT * from cui_users where userStatus=1 AND userLevel=$applicationDoctorLevelId order by userFname ASC";
	$doctorResult = mysql_query($doctorSql);
	if(mysql_num_rows($doctorResult) > 0)
	{
	while ($doctorRs = mysql_fetch_array($doctorResult)) {
	$rsDoctorId = $doctorRs["userId"];
	$rsDoctorFname = $doctorRs["userFname"];
	$rsDoctorLname = $doctorRs["userLname"];
	if($doctorId == $rsDoctorId){
	$sel = "selected";
	}else{
	$sel = "";
	}
	echo "<option $sel value=\"$rsDoctorId\">$rsDoctorFname $rsDoctorLname</option>";
	}
	}
	?>
	</select><?php */?>
                                                <div id="ddlDoctors">
                                                    <select name="doctorId" id="doctorId" tabindex="38">
                                                        <option value="">--- select ---</option>
                                                        <?
	$doctorSql = "SELECT * from cui_users where userStatus=1 AND userLevel='$applicationDoctorLevelId' order by userFname ASC";
	//$doctorResult = mysql_query($doctorSql);
	$doctorResult = mysqli_query($con, $doctorSql);
	//if(mysql_num_rows($doctorResult) > 0)
	if (@mysqli_num_rows($doctorResult) > 0) {
//    while ($doctorRs = mysql_fetch_array($doctorResult)) {
		while ($doctorRs = @mysqli_fetch_array($doctorResult)) {
			$rsDoctorId = $doctorRs["userId"];
			$rsDoctorFname = $doctorRs["userFname"];
			$rsDoctorLname = $doctorRs["userLname"];
			if ($doctorId == $rsDoctorId) {
				$sel = "selected";
				echo "<option $sel value=\"$rsDoctorId\">$rsDoctorFname $rsDoctorLname</option>";
			} else {
				$sel = "";
			}
		}
	}
	?>
                                                    </select>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <?if ($checkCustom == "0" || $checkCustom == "") {
		?>
                                                    <table width="75%" cellspacing="5" cellpadding="0"
                                                           style="border:1px solid; background-color: #EFEFEF;">
                                                        <tr>
                                                            <td><b>Type</b></td>
                                                            <td><b>Complete</b></td>
                                                            <td><b>Date</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td><?$date = getField("cui_patients_history", "patientId", $patientId, "updatedDate", "and fullStatus=1 order by historyId desc limit 0,1");
		if ($date != "") {?><a
                                                                    href="index.php?do=patients_eligibility&patientId=<?=$patientId?>">
                                                                        Full</a><?} else {
			echo "Full";
		}
		?></td>
                                                            <td><?=(getField("cui_patients", "patientId", $patientId, "patientFullDone") ? "Yes" : "No")?></td>
                                                            <td>
                                                                <?
		if ($date != "") {
			echo date('m-d-Y', $date);
		} else {
			echo "N/A";
		}

		?>  </td>
                                                        </tr>
                                                        <tr>
                                                            <td><?$date = getField("cui_patients_history", "patientId", $patientId, "updatedDate", "and patientId>'0' and partialStatus=1 order by historyId desc limit 0,1");
		if ($date != "") {?><a
                                                                    href="index.php?do=patients_eligibility&patientId=<?=$patientId?>">
                                                                        Partial</a><?} else {
			echo "Partial";
		}
		?></td>
                                                            <td><?=(getField("cui_patients", "patientId", $patientId, "patientPartialDone") ? "Yes" : "No")?></td>
                                                            <td>
                                                                <?
		if ($date != "") {
			echo date('m-d-Y', $date);
		} else {
			echo "N/A";
		}

		?>  </td>
                                                        </tr>
                                                        <tr>
                                                            <td><?$date = getField("cui_patients_history", "patientId", $patientId, "updatedDate", "and orthoStatus=1 order by historyId desc limit 0,1");
		if ($date != "") {?><a
                                                                    href="index.php?do=patients_eligibility&patientId=<?=$patientId?>">
                                                                        Ortho</a><?} else {
			echo "Ortho";
		}
		?></td>
                                                            <td><?=(getField("cui_patients", "patientId", $patientId, "patientOrthoDone") ? "Yes" : "No")?></td>
                                                            <td>
                                                                <?
		if ($date != "") {
			echo date('m-d-Y', $date);
		} else {
			echo "N/A";
		}

		?>  </td>
                                                        </tr>
                                                    </table>
                                                <?} else {
		?>
                                                    <table width="75%" cellspacing="5" cellpadding="0"
                                                           style="border:1px solid; background-color: #EFEFEF;">
                                                        <tr>
                                                            <td><b>Type</b></td>
                                                            <td><b>Complete</b></td>
                                                            <td><b>Date</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <?$date = getField("cui_patients_history", "patientId", $patientId, "updatedDate", "and custom='1' and updatedDate!='' order by historyId desc limit 0,1");
		if ($date != "") {?><!--<a href="index.php?do=patients_eligibility&patientId=<?=$patientId?>">-->
                                                                Custom<!--</a>--><?} else {
			echo "Custom";
		}
		?></td>
                                                            <td><?=(getField("cui_patients", "patientId", $patientId, "patientCustomDone") ? "Yes" : "No")?></td>
                                                            <td>
                                                                <?
		if ($date != "") {
			echo date('m-d-Y', $date);
		} else {
			echo "N/A";
		}

		?>  </td>
                                                        </tr>
                                                        <?if ($_SESSION["tmpSessionCompanyId"] == "16"
			|| $_SESSION["tmpSessionCompanyId"] == "20"
			|| $_SESSION["tmpSessionCompanyId"] == "21"
			|| $_SESSION["tmpSessionCompanyId"] == "22"
			|| $_SESSION["tmpSessionCompanyId"] == "27"
			|| $_SESSION["tmpSessionCompanyId"] == "29"
			|| $_SESSION["tmpSessionCompanyId"] == "33"
			|| $_SESSION["tmpSessionCompanyId"] == "29"
			|| $_SESSION["tmpSessionCompanyId"] == "30"
			|| $_SESSION["tmpSessionCompanyId"] == "34"
			|| $_SESSION["tmpSessionCompanyId"] == "83"
			|| $_SESSION["tmpSessionCompanyId"] == "88"
			|| $_SESSION["tmpSessionCompanyId"] == "103"
			|| $_SESSION["tmpSessionCompanyId"] == "113"
			|| $_SESSION["tmpSessionCompanyId"] == "114"
			|| $_SESSION["tmpSessionCompanyId"] == "115"
			|| $_SESSION["tmpSessionCompanyId"] == "118"
			|| $_SESSION["tmpSessionCompanyId"] == "119"
			|| $_SESSION["tmpSessionCompanyId"] == "120"
			|| $_SESSION["tmpSessionCompanyId"] == "121"
			|| $_SESSION["tmpSessionCompanyId"] == "122"
			|| $_SESSION["tmpSessionCompanyId"] == "124"
			|| $_SESSION["tmpSessionCompanyId"] == "125"
			|| $_SESSION["tmpSessionCompanyId"] == "126"
			|| $_SESSION["tmpSessionCompanyId"] == "127"
			|| $_SESSION["tmpSessionCompanyId"] == "128"
			|| $_SESSION["tmpSessionCompanyId"] == "130"
			|| $_SESSION["tmpSessionCompanyId"] == "131"
			|| $_SESSION["tmpSessionCompanyId"] == "132"
			|| $_SESSION["tmpSessionCompanyId"] == "133"
			|| $_SESSION["tmpSessionCompanyId"] == "134"
			|| $_SESSION["tmpSessionCompanyId"] == "135"
			|| $_SESSION["tmpSessionCompanyId"] == "136"
			|| $_SESSION["tmpSessionCompanyId"] == "137"
			|| $_SESSION["tmpSessionCompanyId"] == "138"
			|| $_SESSION["tmpSessionCompanyId"] == "139"
			|| $_SESSION["tmpSessionCompanyId"] == "140"
			|| $_SESSION["tmpSessionCompanyId"] == "141"
			|| $_SESSION["tmpSessionCompanyId"] == "142"
			|| $_SESSION["tmpSessionCompanyId"] == "143"
			|| $_SESSION["tmpSessionCompanyId"] == "144"
			|| $_SESSION["tmpSessionCompanyId"] == "145"
			|| $_SESSION["tmpSessionCompanyId"] == "146"
			|| $_SESSION["tmpSessionCompanyId"] == "147"
			|| $_SESSION["tmpSessionCompanyId"] == "148"
			|| $_SESSION["tmpSessionCompanyId"] == "149"
			|| $_SESSION["tmpSessionCompanyId"] == "150"
			|| $_SESSION["tmpSessionCompanyId"] == "151"
			|| $_SESSION["tmpSessionCompanyId"] == "152"
			|| $_SESSION["tmpSessionCompanyId"] == "153"
			|| $_SESSION["tmpSessionCompanyId"] == "154"
			|| $_SESSION["tmpSessionCompanyId"] == "155"
			|| $_SESSION["tmpSessionCompanyId"] == "156"
			|| $_SESSION["tmpSessionCompanyId"] == "157"
			|| $_SESSION["tmpSessionCompanyId"] == "158"
			|| $_SESSION["tmpSessionCompanyId"] == "159"
			|| $_SESSION["tmpSessionCompanyId"] == "160"
			|| $_SESSION["tmpSessionCompanyId"] == "161"
			|| $_SESSION["tmpSessionCompanyId"] == "162"
			|| $_SESSION["tmpSessionCompanyId"] == "163"
			|| $_SESSION["tmpSessionCompanyId"] == "164"
			|| $_SESSION["tmpSessionCompanyId"] == "165"
			|| $_SESSION["tmpSessionCompanyId"] == "166"
			|| $_SESSION["tmpSessionCompanyId"] == "167"
			|| $_SESSION["tmpSessionCompanyId"] == "168"
			|| $_SESSION["tmpSessionCompanyId"] == "169"
			|| $_SESSION["tmpSessionCompanyId"] == "170"
			|| $_SESSION["tmpSessionCompanyId"] == "172"
			|| $_SESSION["tmpSessionCompanyId"] == "173"
			|| $_SESSION["tmpSessionCompanyId"] == "174"
			|| $_SESSION["tmpSessionCompanyId"] == "175"
			|| $_SESSION["tmpSessionCompanyId"] == "176"
			|| $_SESSION["tmpSessionCompanyId"] == "177"
			|| $_SESSION["tmpSessionCompanyId"] == "178") {
			?>
                                                            <tr>
                                                                <td>
                                                                    <?$date = getField("cui_patients_history", "patientId", $patientId, "updatedDate", "and patientId>'0' and partial=1 order by historyId desc limit 0,1");
			if ($date != "") {?><!--<a href="index.php?do=patients_eligibility&patientId=<?=$patientId?>">-->
                                                                    Partial<!--</a>--><?} else {
				echo "Partial";
			}
			?></td>
                                                                <td><?=(getField("cui_patients", "patientId", $patientId, "patientPartialDone") ? "Yes" : "No")?></td>
                                                                <td>
                                                                    <?
			if ($date != "") {
				echo date('m-d-Y', $date);
			} else {
				echo "N/A";
			}

			?>  </td>
                                                            </tr>
                                                        <?}?>
                                                    </table>
                                                <?}?></td>
                                        </tr>
                                    </table>
                                    <!-- Left Row --></td>
                                <td width="5%">&nbsp;</td>
                                <td width="40%" valign="top">
                                    <!-- Right Row -->
                                    <table width="100%" cellpadding="3" cellspacing="0">
                                        <tr>
                                            <td>Select Office <span class="required">*</span></td>
                                            <td>
                                                <select id="officeId" name="officeId" tabindex="2"
                                                        onchange="showDoctors(this.value);<?php if ($act == 'add') {?>checksamepatient12();<?php }?>">
                                                    <?
	if ($companyId != "") {
		$editSql = "SELECT * from cui_companies_offices where companyId='$companyId'" . (($officeId != "All") ? " AND officeId = '$officeId'" : "");
//    $editResult = mysql_query($editSql);
		$editResult = mysqli_query($con, $editSql);
//    while ($editRs = mysql_fetch_array($editResult)) {
		while ($editRs = @mysqli_fetch_array($editResult)) {
			$rsofficeId = $editRs["officeId"];
			$rsOfficeName = $editRs["officeName"];
			$sel = "";
			if ($officeId == $rsofficeId) {
				$sel = "selected";
			}
			echo '<option ' . $sel . ' value="' . $rsofficeId . '">' . $rsOfficeName . '</option>';
		}
	} else {
		echo '<option value="">--- select ---</option>';
	}?>
                                                    <?//
	//if($moduleAll == 1 or $moduleCompaniesViewAll == 1){
	//	echo '<option value="All">- All Offices -</option>';
	//}else{
	//	if($_SESSION["cuiSessionUserLevel"] <= 34)
	//	{
	//		echo '<option value="All">- All Offices -</option>';
	//	}else{
	//		echo '<option value="">--- select ---</option>';
	//	}
	//}
	//if($_SESSION["cuiSessionCompanyId"]){
	//	if($moduleAll == 1 or $moduleCompaniesViewAll == 1){
	//		$editSql = "SELECT * from cui_companies_offices where companyId='$sessionCompanyId'";
	//	}else{
	//		$editSql = "select a.companyId, b.officeId from cui_companies as a join cui_users_companies as b where a.companyId=b.companyId and b.userId='$sessionId' and a.companyId=".$sessionCompanyId." order by b.officeId ASC";
	//	}
	////    $editResult = mysql_query($editSql);
	//	$editResult = mysqli_query($con, $editSql);
	////    while ($editRs = mysql_fetch_array($editResult)) {
	//	while ($editRs = @mysqli_fetch_array($editResult)) {
	//		$rsofficeId = $editRs["officeId"];
	//		$officeName = getField("cui_companies_offices","officeId",$rsofficeId,"officeName");
	//		if($rsofficeId == 0){
	//			$rsofficeId = "All";
	//			$officeName = "- All Offices -";
	//			$getAllOfficesList = getCompanyOffices($sessionCompanyId);
	//			//add all offices
	//			$sel = '';
	//			if($sessionOfficeId == $rsofficeId){
	//				$sel = "selected";
	//			}
	//			//add below condition on Jun-30-2015 with CUI employees
	//			if($_SESSION["cuiSessionUserLevel"] > 34)
	//			{
	//				echo '<option '.$sel.' value="'.$rsofficeId.'">'.$officeName.'</option>';
	//			}
	//			//explode
	//			$getAllOfficesListEx = explode("|", $getAllOfficesList);
	//			foreach ($getAllOfficesListEx as $GAOL){
	//				//office name
	//				$getOfficeName = getField("cui_companies_offices","officeId",$GAOL,"officeName");
	//				$sel = '';
	//				if($sessionOfficeId == $GAOL){
	//					$sel = "selected";
	//				}
	//				echo '<option '.$sel.' value="'.$GAOL.'">'.$getOfficeName.'</option>';
	//			}
	//			//skip loop
	//			break;
	//		}
	//		$sel = '';
	//		if($_SESSION["cuiSessionOfficeId"] == $rsofficeId){
	//			$sel = "selected";
	//		}
	//		echo '<option '.$sel.' value="'.$rsofficeId.'">'.$officeName.'</option>';
	//	}
	//}?>
                                                </select>
                                                <script>
                                                    $(document).ready(function () {
//		$('.company_for_trigger_first').trigger('change');
                                                    });
                                                </script>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="150px">Patient Last Name <span class="required">*</span></td>
                                            <td><input type="text" class="textbox" id="patientLname" name="patientLname"
                                                       value="<?=$patientLname?>" tabindex="4"
                                                       onblur="<?php if ($act == 'add') {?>checksamepatient12();<?php }?>"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="150px">Address Line 1</td>
                                            <td><input type="text" class="textbox" id="patientAddress1"
                                                       name="patientAddress1" value="<?=$patientAddress1?>"
                                                       tabindex="8"/></td>
                                        </tr>
                                        <tr>
                                            <td>City</td>
                                            <td><input type="text" class="textbox" id="patientCity" name="patientCity"
                                                       value="<?=$patientCity?>" tabindex="10"/></td>
                                        </tr>
                                        <tr>
                                            <td>Zip Code</td>
                                            <td><input type="text" class="textbox" id="patientZip" name="patientZip"
                                                       size="10" value="<?=$patientZip?>" tabindex="12"/></td>
                                        </tr>
                                        <tr>
                                            <td>Phone (Work)</td>
                                            <td><input type="text" class="textbox" id="patientPhoneWork"
                                                       name="patientPhoneWork"
                                                       value="<?=($patientPhoneWork ? $patientPhoneWork : "XXX-XXX-XXXX")?>"
                                                       tabindex="14"
                                                       onclick="if(this.value == 'XXX-XXX-XXXX'){this.value = ''}"
                                                       onblur="if(this.value == ''){this.value = 'XXX-XXX-XXXX'}"/></td>
                                        </tr>
                                        <tr>
                                            <td>Email Address</td>
                                            <td><input type="text" class="textbox" id="patientEmail" name="patientEmail"
                                                       value="<?=$patientEmail?>" tabindex="16"/></td>
                                        </tr>
                                        <tr>
                                            <td>Insurance Phone <span class="required">*</span></td>
                                            <td><input type="text" class="textbox" id="patientInsPhone"
                                                       name="patientInsPhone"
                                                       value="<?=($patientInsPhone ? $patientInsPhone : "XXX-XXX-XXXX")?>"
                                                       tabindex="18"
                                                       onclick="if(this.value == 'XXX-XXX-XXXX'){this.value = ''}"
                                                       onblur="if(this.value == ''){this.value = 'XXX-XXX-XXXX'}"/>&nbsp;<input
                                                    type="text" name="patientInsPhoneExt" id="patientInsPhoneExt"
                                                    class="textbox"
                                                    value="<?=($patientInsPhoneExt ? $patientInsPhoneExt : "ext")?>"
                                                    size="3" onclick="if(this.value == 'ext'){this.value = ''}"
                                                    tabindex="18.5"/></td>
                                        </tr>
                                        <tr>
                                            <td>Insurance City</td>
                                            <td><input type="text" class="textbox" id="patientInsCity"
                                                       name="patientInsCity" value="<?=$patientInsCity?>" size="10"
                                                       tabindex="20"/></td>
                                        </tr>
                                        <tr>
                                            <td>Insurance Zip</td>
                                            <td><input type="text" class="textbox" id="patientInsZip"
                                                       name="patientInsZip" value="<?=$patientInsZip?>" size="10"
                                                       tabindex="22"/></td>
                                        </tr>
                                        <tr>
                                            <td>Subscriber ID <span class="required">*</span></td>
                                            <td><input type="text" class="textbox" id="patientSubId" name="patientSubId"
                                                       value="<?=$tmpPatientSubId?>" tabindex="24"/></td>
                                        </tr>
                                        <tr>
                                            <td>Group/Policy # <span class="required">*</span></td>
                                            <td><input type="text" class="textbox" id="patientGroup" name="patientGroup"
                                                       value="<?=$tmpPatientGroup?>" tabindex="26"/></td>
                                        </tr>
                                        <tr>
                                            <td>Subscriber DOB <span class="required">*</span></td>
                                            <td>
                                                <select name="patientFamilyDobMonth" id="patientFamilyDobMonth"
                                                        tabindex="28">
                                                    <option value="">MM</option>
                                                    <?
	for ($i = 1; $i < 13; $i++) {
		if ($i < 10) {
			$j = "0" . $i;
		} else {
			$j = $i;
		}
		$sel = "";
		if ($patientFamilyDobMonth == $j) {
			$sel = "selected";
		}
		echo "<option $sel value=\"$j\">$j</option>";
	}
	?>
                                                </select>
                                                <select name="patientFamilyDobDay" id="patientFamilyDobDay"
                                                        tabindex="29">
                                                    <option value="">DD</option>
                                                    <?
	for ($i = 1; $i < 32; $i++) {
		if ($i < 10) {
			$j = "0" . $i;
		} else {
			$j = $i;
		}
		$sel = "";
		if ($patientFamilyDobDay == $j) {
			$sel = "selected";
		}
		echo "<option $sel value=\"$j\">$j</option>";
	}
	?>
                                                </select>
                                                <select name="patientFamilyDobYear" id="patientFamilyDobYear"
                                                        tabindex="30">
                                                    <option value="">YYYY</option>
                                                    <?
	$currentYear = date("Y");
	$tillYear = $currentYear - 100;
	if ($patientFamilyDobYear == "") {
		$patientFamilyDobYear = $currentYear - 50;
	}
	for ($i = $tillYear; $i <= $currentYear; $i++) {
		$sel = "";
		if ($patientFamilyDobYear == $i) {
			$sel = "selected";
		}
		echo "<option $sel value=\"$i\">$i</option>";
	}
	?>
                                                </select></td>
                                        </tr>
                                        <tr>
                                            <td>Priority</td>
                                            <td>
                                                <select id="patientPriority" name="patientPriority" tabindex="33">
                                                    <?
	$prioritySql = "SELECT * from cui_patients_priority order by priorityTitle DESC";
	//$priorityResult = mysql_query($prioritySql);
	$priorityResult = mysqli_query($con, $prioritySql);
	//while ($priorityRs = mysql_fetch_array($priorityResult)){
	while ($priorityRs = @mysqli_fetch_array($priorityResult)) {
		$priorityTitle = $priorityRs["priorityTitle"];
		?>
                                                        <option
                                                            <?php if ($patientPriority == $priorityTitle) {
			?>selected<?
		}?> value="<?=$priorityTitle?>"><?=$priorityTitle?></option>
                                                        <?
	}
	?>
                                                </select></td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td>
                                                <select name="patientStatus" id="patientStatus" tabindex="36">
                                                    <?
	$statusSql = "SELECT * from cui_patients_status order by statusName ASC";
	//$statusResult = mysql_query($statusSql);
	$statusResult = mysqli_query($con, $statusSql);
	//while ($statusRs = mysql_fetch_array($statusResult)) {
	while ($statusRs = @mysqli_fetch_array($statusResult)) {
		$getStatusName = $statusRs["statusName"];
		if ($patientStatus != "") {
			if ($patientStatus == "New") {
				if ($getStatusName == "Check") {
					$sel = "selected";
				} else {
					$sel = "";
				}
			} else {
				if ($patientStatus == $getStatusName) {
					$sel = "selected";
				} else {
					$sel = "";
				}
			}
		} else {
			if ($getStatusName == "Check") {
				$sel = "selected";
			} else {
				$sel = "";
			}

		}
		echo "<option $sel value=\"$getStatusName\">$getStatusName</option>";
	}
	?>
                                                </select>
                                                <input type="hidden" name="patientStatusOld" id="patientStatusOld"
                                                       value="<?php echo $patientStatus; ?>"/></td>
                                        </tr>
                                        <tr>
                                            <td>Eligibility Check <span class="required">*</span></td>
                                            <td>
                                                <?if ($checkCustom == "0" || $checkCustom == "") {?>
                                                    <table cellpadding="1" cellspacing="0">
                                                        <tr>
                                                            <td><input type="radio" name="patientEligibility"
                                                                       value="Full"
                                                                       <?php if ($patientEligibility == "Full") {?>checked<?}?>
                                                                       tabindex="39"/></td>
                                                            <td>Full</td>
                                                            <td width="10px">&nbsp;</td>
                                                            <td><input type="radio" name="patientEligibility"
                                                                       value="Partial"
                                                                       <?php if ($patientEligibility == "Partial") {?>checked<?}?>
                                                                       tabindex="40"/></td>
                                                            <td>Partial</td>
                                                        </tr>
                                                    </table>
                                                <?} else {
		?>
                                                    <table cellpadding="1" cellspacing="0">
                                                        <tr>
                                                            <td><input type="radio" name="patientEligibility"
                                                                       value="Custom" checked tabindex="39"/></td>
                                                            <td>Custom</td>
                                                            <?if ($_SESSION["tmpSessionCompanyId"] == "16"
			|| $_SESSION["tmpSessionCompanyId"] == "20"
			|| $_SESSION["tmpSessionCompanyId"] == "21"
			|| $_SESSION["tmpSessionCompanyId"] == "22"
			|| $_SESSION["tmpSessionCompanyId"] == "27"
			|| $_SESSION["tmpSessionCompanyId"] == "29"
			|| $_SESSION["tmpSessionCompanyId"] == "33"
			|| $_SESSION["tmpSessionCompanyId"] == "29"
			|| $_SESSION["tmpSessionCompanyId"] == "30"
			|| $_SESSION["tmpSessionCompanyId"] == "34"
			|| $_SESSION["tmpSessionCompanyId"] == "83"
			|| $_SESSION["tmpSessionCompanyId"] == "88"
			|| $_SESSION["tmpSessionCompanyId"] == "103"
			|| $_SESSION["tmpSessionCompanyId"] == "113"
			|| $_SESSION["tmpSessionCompanyId"] == "114"
			|| $_SESSION["tmpSessionCompanyId"] == "115"
			|| $_SESSION["tmpSessionCompanyId"] == "118"
			|| $_SESSION["tmpSessionCompanyId"] == "119"
			|| $_SESSION["tmpSessionCompanyId"] == "120"
			|| $_SESSION["tmpSessionCompanyId"] == "121"
			|| $_SESSION["tmpSessionCompanyId"] == "122"
			|| $_SESSION["tmpSessionCompanyId"] == "124"
			|| $_SESSION["tmpSessionCompanyId"] == "125"
			|| $_SESSION["tmpSessionCompanyId"] == "126"
			|| $_SESSION["tmpSessionCompanyId"] == "127"
			|| $_SESSION["tmpSessionCompanyId"] == "128"
			|| $_SESSION["tmpSessionCompanyId"] == "130"
			|| $_SESSION["tmpSessionCompanyId"] == "131"
			|| $_SESSION["tmpSessionCompanyId"] == "132"
			|| $_SESSION["tmpSessionCompanyId"] == "133"
			|| $_SESSION["tmpSessionCompanyId"] == "134"
			|| $_SESSION["tmpSessionCompanyId"] == "135"
			|| $_SESSION["tmpSessionCompanyId"] == "136"
			|| $_SESSION["tmpSessionCompanyId"] == "137"
			|| $_SESSION["tmpSessionCompanyId"] == "138"
			|| $_SESSION["tmpSessionCompanyId"] == "139"
			|| $_SESSION["tmpSessionCompanyId"] == "140"
			|| $_SESSION["tmpSessionCompanyId"] == "141"
			|| $_SESSION["tmpSessionCompanyId"] == "142"
			|| $_SESSION["tmpSessionCompanyId"] == "143"
			|| $_SESSION["tmpSessionCompanyId"] == "144"
			|| $_SESSION["tmpSessionCompanyId"] == "145"
			|| $_SESSION["tmpSessionCompanyId"] == "146"
			|| $_SESSION["tmpSessionCompanyId"] == "147"
			|| $_SESSION["tmpSessionCompanyId"] == "148"
			|| $_SESSION["tmpSessionCompanyId"] == "149"
			|| $_SESSION["tmpSessionCompanyId"] == "150"
			|| $_SESSION["tmpSessionCompanyId"] == "151"
			|| $_SESSION["tmpSessionCompanyId"] == "152"
			|| $_SESSION["tmpSessionCompanyId"] == "153"
			|| $_SESSION["tmpSessionCompanyId"] == "154"
			|| $_SESSION["tmpSessionCompanyId"] == "155"
			|| $_SESSION["tmpSessionCompanyId"] == "156"
			|| $_SESSION["tmpSessionCompanyId"] == "157"
			|| $_SESSION["tmpSessionCompanyId"] == "158"
			|| $_SESSION["tmpSessionCompanyId"] == "159"
			|| $_SESSION["tmpSessionCompanyId"] == "160"
			|| $_SESSION["tmpSessionCompanyId"] == "161"
			|| $_SESSION["tmpSessionCompanyId"] == "162"
			|| $_SESSION["tmpSessionCompanyId"] == "163"
			|| $_SESSION["tmpSessionCompanyId"] == "164"
			|| $_SESSION["tmpSessionCompanyId"] == "165"
			|| $_SESSION["tmpSessionCompanyId"] == "166"
			|| $_SESSION["tmpSessionCompanyId"] == "167"
			|| $_SESSION["tmpSessionCompanyId"] == "168"
			|| $_SESSION["tmpSessionCompanyId"] == "169"
			|| $_SESSION["tmpSessionCompanyId"] == "170"
			|| $_SESSION["tmpSessionCompanyId"] == "172"
			|| $_SESSION["tmpSessionCompanyId"] == "173"
			|| $_SESSION["tmpSessionCompanyId"] == "174"
			|| $_SESSION["tmpSessionCompanyId"] == "175"
			|| $_SESSION["tmpSessionCompanyId"] == "176"
			|| $_SESSION["tmpSessionCompanyId"] == "177"
			|| $_SESSION["tmpSessionCompanyId"] == "178") {?>
                                                                <td width="10px">&nbsp;</td>
                                                                <td><input type="radio" name="patientEligibility"
                                                                           value="Partial"
                                                                           <?php if ($patientEligibility == "Partial") {?>checked<?}?>
                                                                           tabindex="40"/></td>
                                                                <td>Partial</td>
                                                            <?}?>
                                                        </tr>
                                                    </table>
                                                <?}?></td>
                                        </tr>
                                        <?if ($checkCustom == "0" || $checkCustom == "") {?>
                                            <tr>
                                                <td>Include Ortho? <span class="required">*</span></td>
                                                <td>
                                                    <table cellpadding="1" cellspacing="0">
                                                        <tr>
                                                            <td><input type="radio" name="patientOrtho" value="Yes"
                                                                       <?php if ($patientOrtho == "Yes") {?>checked<?}?>
                                                                       tabindex="39"/></td>
                                                            <td>Yes</td>
                                                            <td width="10px">&nbsp;</td>
                                                            <td><input type="radio" name="patientOrtho" value="No"
                                                                       <?php if ($patientOrtho == "No") {?>checked<?}?>
                                                                       tabindex="40"/></td>
                                                            <td>No</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        <?}?>

<!--                                        --><?//if ($checkCustom == "1" || $checkCustom == "0" || $checkCustom == "") {?>
<!--                                            <tr>-->
<!--                                                <td>Files <span class="required"></span></td>-->
<!--                                                <td><a title="Patient Files"-->
<!--                                                       href="patient_files.php?patientId=--><?//=$patientId?><!--&TB_iframe=true&height=250&width=800"-->
<!--                                                       class="thickbox">View/Upload Files</a></td>-->
<!--                                            </tr>-->
<!--                                        --><?//}?>
                                    </table>
                                    <!-- Right Row --></td>
                            </tr>
                            <tr>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <table width="100%" cellpadding="3" cellspacing="0">
                                        <tr>
                                            <td valign="top" width="150px">Dental Office Notes</td>
                                            <td>
                                                <textarea name="patientInstructions" id="patientInstructions"
                                                          class="textbox" tabindex="41" style="width:100%;"
                                                          rows="5"><?=$patientInstructions?></textarea>
                                                <script type="text/javascript">
                                                    //CKEDITOR.replace( 'message' );
                                                    CKEDITOR.replace('patientInstructions',
                                                        {
                                                            //filebrowserBrowseUrl :'http://noman-pc/Projects/Picoa/Web/admin/javascript/ckeditor/filemanager/browser/default/browser.php?Connector=http://noman-pc/Projects/Picoa/Web/admin/javascript/ckeditor/filemanager/connectors/php/connector.php',
                                                            //filebrowserUploadUrl  :'http://noman-pc/Projects/Picoa/Web/admin/javascript/ckeditor/filemanager/connectors/php/upload.php?Type=File',
                                                            //filebrowserImageUploadUrl : 'http://noman-pc/Projects/Picoa/Web/admin/javascript/ckeditor/filemanager/connectors/php/upload.php?Type=Image',
                                                            //filebrowserFlashUploadUrl : 'http://noman-pc/Projects/Picoa/Web/admin/javascript/ckeditor/filemanager/connectors/php/upload.php?Type=Flash'
                                                        });
                                                </script>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <?if ($act == "add") {?>
                                <tr>
                                    <td>
                                        <table width="100%" cellpadding="3" cellspacing="0">
                                            <tr>
                                                <td valign="top" width="150px">Clone This Patient</td>
                                                <td>
                                                    <table cellpadding="1" cellspacing="0">
                                                        <tr>
                                                            <td><input type="radio" name="clonePatient" value="Yes"
                                                                       tabindex="42"
                                                                       onclick="document.getElementById('clone').style.display=''"/>
                                                            </td>
                                                            <td>Yes</td>
                                                            <td width="10px">&nbsp;</td>
                                                            <td><input type="radio" name="clonePatient" value="No"
                                                                       checked tabindex="43"
                                                                       onclick="document.getElementById('clone').style.display='none'"/>
                                                            </td>
                                                            <td>No</td>
                                                            <td id="clone" style="display:none;">&nbsp;&nbsp;<input
                                                                    type="submit" id="addClone" name="addClone"
                                                                    class="btn searchbt" value="Clone"
                                                                    onclick="return chkForm();" tabindex="44"/></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            <?}?>
                            <tr>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center">
                                    <?if ($act == "add") {?>
                                        <input type="hidden" name="patientAddedBy" id="patientAddedBy"
                                               value="<?echo $_SESSION[$sessionUserID]; ?>"/>
                                        <input type="hidden" name="patientAdded" id="patientAdded"
                                               value="<?echo date("Y-m-d H:i:s"); ?>"/>
                                    <?}?>
                                    <input type="hidden" name="patientUpdated" id="patientUpdated"
                                           value="<?echo date("Y-m-d H:i:s"); ?>"/>
                                    <?if ($act == "add") {?>
                                        <input type="submit" name="addNew" class="btn searchbt" value="Add New"
                                               onclick="return chkForm()" tabindex="44"/>
                                    <?}?>
                                    <?if ($act == "edit") {?>
                                        <input type="button" name="addNew" class="btn searchbt" value="Add New"
                                               onclick="window.location='<?=HTTP_SERVER . "index.php?do=patients&act=add"?>';"
                                               tabindex="44"/>
                                    <?}?>
                                    <input type="submit" name="saveExit" class="btn searchbt" value="Save & Exit" onclick="return chkForm()" tabindex="44"/>
									<?php //if one is allowed to view this module
	if ($_SESSION["cuiSessionUserLevel"] == 1 || $_SESSION["cuiSessionUserLevel"] == 34) {?>
									<input type="submit" class="btn searchbt" name="saveContinue" value="Save & Continue" onclick="return chkForm()" tabindex="45"/>
									<?}?>
									<input type="button" class="btn clearbt" value="Cancel" onclick="javascript: history.back(-1)" tabindex="46"/>
									</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </form>
    </div>
	<?php }?>
    <?php if ($showPatientForm == 0 && $viewPatient == 0) {
	?>
        <div id="help">
            <div id="helpContent" style="display: none">
                <table cellpadding="3" cellspacing="0">
                    <tr>
                        <td><span title="Delete" class="glyphicon glyphicon-trash"></span></td>
                        <td>&nbsp;</td>
                        <td>Delete an existing patient.</td>
                    </tr>
                    <tr>
                        <td><span title="Edit" class="glyphicon glyphicon-pencil"></span></td>
                        <td>&nbsp;</td>
                        <td>Edit existing patient information.</td>
                    </tr>
                    <tr>
                        <td><img alt="Active" src="<?=HTTP_SERVER?>images/green.png" border="0"/></td>
                        <td>&nbsp;</td>
                        <td>Patient's status is active.</td>
                    </tr>
                    <tr>
                        <td><img alt="Blocked" src="<?=HTTP_SERVER?>images/red.png" border="0"/></td>
                        <td>&nbsp;</td>
                        <td>Patient's status is inactive / blocked.</td>
                    </tr>
                </table>
            </div>
        </div>
        <!--<form method="GET" action="index.php">-->
		<div class="searchcard">
			<div class="searchformcontainer">
				<form class="form-inline" method="GET" action="index.php">
					<input type="hidden" name="do" value="<?=$do?>"/>
					<div class="form-group">
						<select name="filter" class="selectpicker">
							<option value="">Search By</option>
                            <option <?php if ($filter == "patientId") {?>selected<?}?> value="patientId">Patient ID#</option>
							<option <?php if ($filter == "patientFname") {?>selected<?}?> value="patientFname">Patient First Name</option>
							<option <?php if ($filter == "patientLname") {?>selected<?}?> value="patientLname">Patient Last Name</option>
							<option <?php if ($filter == "patientCity") {?>selected<?}?> value="patientCity">Patient City</option>
							<option <?php if ($filter == "patientState") {?>selected<?}?> value="patientState">Patient State</option>
							<option <?php if ($filter == "patientZip") {?>selected<?}?> value="patientZip">Patient Zip</option>
							<option <?php if ($filter == "patientEligibility") {?>selected<?}?> value="patientEligibility">Eligibility</option>
							<option <?php if ($filter == "userFname") {?>selected<?}?> value="userFname">Doctor First Name</option>
							<option <?php if ($filter == "userLname") {?>selected<?}?> value="userLname">Doctor Last Name</option>
							<option <?php if ($filter == "patientInsCompany") {?>selected<?}?> value="patientInsCompany">Insurance Company</option>
							<option <?php if ($filter == "patientInsCity") {?>selected<?}?> value="patientInsCity">Insurance City</option>
							<option <?php if ($filter == "patientInsState") {?>selected<?}?> value="patientInsState">Insurance State</option>
							<option <?php if ($filter == "patientInsZip") {?>selected<?}?> value="patientInsZip">Insurance Zip</option>
							<option <?php if ($filter == "officeName") {?>selected<?}?> value="officeName">Dental Office</option>
							<option <?php if ($filter == "companyName") {?>selected<?}?> value="companyName">Dental Company</option>
							<option <?php if ($filter == "patientAppDate") {?>selected<?}?> value="patientAppDate">Appointment Date</option>
							<option <?php if ($filter == "patientStatus") {?>selected<?}?> value="patientStatus">Status</option>
							<option <?php if ($filter == "patientPriority") {?>selected<?}?> value="patientPriority">Priority</option>
							<option <?php if ($filter == "patientEmployer") {?>selected<?}?> value="patientEmployer">Employer</option>
							<option <?php if ($filter == "patientGroup") {?>selected<?}?> value="patientGroup">Group#</option>
						</select>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="text" name="filterText" class="filterTextBox" value="<?=$filterText?>" placeholder="Search Text">
					</div>
					<span class="searchcondition">AND</span>
					<div class="radio radio-inline">
                        <input type="radio" name="filterAndOr" value="and" id="inlineRadio2" <?php if ($filterAndOr == "and") {?>checked<?}?> <?php if ($filterAndOr == "") {?>checked<?}?> />
                    </div>
					<span class="searchcondition"> | OR</span>
					<div class="radio radio-inline">
						<input type="radio" name="filterAndOr" value="or" id="inlineRadio2" <?php if ($filterAndOr == "or") {?>checked<?}?> />
                    </div>
					<div class="form-group">
						<select class="selectpicker" name="filter2">
							<option value="">Search By 2</option>
							<option <?php if ($filter2 == "patientId") {?>selected<?}?> value="patientId">Patient ID#</option>
							<option <?php if ($filter2 == "patientFname") {?>selected<?}?> value="patientFname">Patient First Name</option>
							<option <?php if ($filter2 == "patientLname") {?>selected<?}?> value="patientLname">Patient Last Name</option>
							<option <?php if ($filter2 == "patientCity") {?>selected<?}?> value="patientCity">Patient City</option>
							<option <?php if ($filter2 == "patientState") {?>selected<?}?> value="patientState">Patient State</option>
							<option <?php if ($filter2 == "patientZip") {?>selected<?}?> value="patientZip">Patient Zip</option>
							<option <?php if ($filter2 == "patientEligibility") {?>selected<?}?> value="patientEligibility">Eligibility</option>
							<option <?php if ($filter2 == "userFname") {?>selected<?}?> value="userFname">Doctor First Name</option>
							<option <?php if ($filter2 == "userLname") {?>selected<?}?> value="userLname">Doctor Last Name</option>
							<option <?php if ($filter2 == "patientInsCompany") {?>selected<?}?> value="patientInsCompany">Insurance Company</option>
							<option <?php if ($filter2 == "patientInsCity") {?>selected<?}?> value="patientInsCity">Insurance City</option>
							<option <?php if ($filter2 == "patientInsState") {?>selected<?}?> value="patientInsState">Insurance State</option>
							<option <?php if ($filter2 == "patientInsZip") {?>selected<?}?> value="patientInsZip">Insurance Zip</option>
							<option <?php if ($filter2 == "officeName") {?>selected<?}?> value="officeName">Dental Office</option>
							<option <?php if ($filter2 == "companyName") {?>selected<?}?> value="companyName">Dental Company</option>
							<option <?php if ($filter2 == "patientAppDate") {?>selected<?}?> value="patientAppDate">Appointment Date</option>
							<option <?php if ($filter2 == "patientStatus") {?>selected<?}?> value="patientStatus">Status</option>
							<option <?php if ($filter2 == "patientPriority") {?>selected<?}?> value="patientPriority">Priority</option>
							<option <?php if ($filter2 == "patientEmployer") {?>selected<?}?> value="patientEmployer">Employer</option>
							<option <?php if ($filter2 == "patientGroup") {?>selected<?}?> value="patientGroup">Group#</option>
						</select>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Search Text 2" name="filterText2" class="filterTextBox" value="<?=$filterText2?>"/>
					</div>
					<div class="form-group">
						<input type="hidden" name="patientStatus" value="<?=$patientStatus?>"/>
                        <!--<input type="submit" value="Search" class="filterButton"/>-->
						<button type="submit" class="btn searchbt">Search</button>
					</div>
					<div class="form-group">
						<button type="button" onclick="javascript: window.location='index.php?do=patients<?=$clearfilter;?>'" class="btn clearbt">Clear</button>
					</div>
					<?
	//roles check (one can add or not)
	if ($moduleAll == 1 or $modulePatientsAdd == 1) {
		?>
					<div class="form-group">
						<button type="button" onclick="javascript: window.location='index.php?do=patients&act=add'" class="btn greenbt">Add Patient</button>
					</div>
					<?}?>
				</form>
			</div>
		</div>
        <!--</form>-->
        <br>

        <table class="table table-bordered form-spacing" align="center" width="100%" border="0" cellpadding="3" cellspacing="1">
            <thead>
			<!--<tr>
                <td colspan="10"><h3 style="color:black;">Patient Listing</h3></td>
            </tr>-->
            <tr align="center">
                <td width=""><a
                        href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=patientId&sortOrder=<?php if ($sortBy == "patientId") {
		if ($sortOrder == "ASC") {
			echo "DESC";
		} else {
			echo "ASC";
		}
	} else {
		echo "ASC";
	}?>">ID<?php if ($sortBy == "patientId") {?>&nbsp;<img
                            src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0"
                            alt="<?=$sortOrder?> Order" /><?}?></a></td>
                <td width=""><a
                        href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=patientFname&sortOrder=<?php if ($sortBy == "patientFname") {
		if ($sortOrder == "ASC") {
			echo "DESC";
		} else {
			echo "ASC";
		}
	} else {
		echo "ASC";
	}?>">Patient Name<?php if ($sortBy == "patientFname") {?>&nbsp;<img
                            src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0"
                            alt="<?=$sortOrder?> Order" /><?}?></a></td>
                <td width=""><a
                        href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=patientInsCompany&sortOrder=<?php if ($sortBy == "patientInsCompany") {
		if ($sortOrder == "ASC") {
			echo "DESC";
		} else {
			echo "ASC";
		}
	} else {
		echo "ASC";
	}?>">Insurance Company<?php if ($sortBy == "patientInsCompany") {?>&nbsp;<img
                            src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0"
                            alt="<?=$sortOrder?> Order" /><?}?></a></td>

                <td width=""><a
                        href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=companyName&sortOrder=<?php if ($sortBy == "companyName") {
		if ($sortOrder == "ASC") {
			echo "DESC";
		} else {
			echo "ASC";
		}
	} else {
		echo "ASC";
	}?>">
						Dental Company<?php if ($sortBy == "companyName") {?>&nbsp;<img
                            src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0"
                            alt="<?=$sortOrder?> Order" /><?}?></a></td>

                <td width=""><a
                        href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=officeId&sortOrder=<?php if ($sortBy == "officeId") {
		if ($sortOrder == "ASC") {
			echo "DESC";
		} else {
			echo "ASC";
		}
	} else {
		echo "ASC";
	}?>">Dental Office<?php if ($sortBy == "officeId") {?>&nbsp;<img
                            src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0"
                            alt="<?=$sortOrder?> Order" /><?}?></a></td>
                <td width=""><a
                        href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=patientAppDate&sortOrder=<?php if ($sortBy == "patientAppDate") {
		if ($sortOrder == "ASC") {
			echo "DESC";
		} else {
			echo "ASC";
		}
	} else {
		echo "ASC";
	}?>">Appointment<?php if ($sortBy == "patientAppDate") {?>&nbsp;<img
                            src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0"
                            alt="<?=$sortOrder?> Order" /><?}?></a></td>
                <td width=""><a
                        href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=patientEligibility&sortOrder=<?php if ($sortBy == "patientEligibility") {
		if ($sortOrder == "ASC") {
			echo "DESC";
		} else {
			echo "ASC";
		}
	} else {
		echo "ASC";
	}?>">Eligibility<?php if ($sortBy == "patientEligibility") {?>&nbsp;<img
                            src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0"
                            alt="<?=$sortOrder?> Order" /><?}?></a></td>
                <td width=""><a
                        href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=patientStatus&sortOrder=<?php if ($sortBy == "patientStatus") {
		if ($sortOrder == "ASC") {
			echo "DESC";
		} else {
			echo "ASC";
		}
	} else {
		echo "ASC";
	}?>">Status<?php if ($sortBy == "patientStatus") {?>&nbsp;<img
                            src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0"
                            alt="<?=$sortOrder?> Order" /><?}?></a></td>
                <?php if ($getPatientStatus == "Check") {
		?>
                    <td width=""><a
                            href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=patientPriority&sortOrder=<?php if ($sortBy == "patientPriority") {
			if ($sortOrder == "ASC") {
				echo "DESC";
			} else {
				echo "ASC";
			}
		} else {
			echo "ASC";
		}?>">Priority<?php if ($sortBy == "patientPriority") {?>&nbsp;<img
                                src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0"
                                alt="<?=$sortOrder?> Order" /><?}?></a></td>
                <?php }?>
                <td width="">Actions</td>
            </tr>
			</thead>
			<tbody>
            <?php
//pagination code
	if ((!isset($_GET['page'])) && (!isset($_POST['page']))) {
		$page = 1;
	} else {
		if (!$_GET['page']) {
			$page = $_POST['page'];
		} else {
			$page = $_GET['page'];
		}
	}

	function getSqlVarsForFilter($con, $options = []) {
		$filter = $options['filter'];
		$filterText = $options['filterText'];
		$filter2 = $options['filter2'];
		$filterText2 = $options['filterText2'];
		$filterAndOr = $options['filterAndOr'];
	}

	//if get filters
	if ($_GET["filter"] || $_GET["filter2"]) {
		$filter = sanitize($_GET["filter"]);
		$filter2 = sanitize($_GET["filter2"]);
		if ($filter != "" || $filter2 != "") {
			$filterText = sanitize(trim($_GET["filterText"]));
			$filterText2 = sanitize(trim($_GET["filterText2"]));
			$filterAndOr = 'AND';
			if ($_GET["filterAndOr"]) {
				$filterAndOr = strtoupper(sanitize($_GET["filterAndOr"]));
			}
			$filter_reserved = false;
			$filter_reserved2 = false;

			/* //more conditions
				            if($filter == "patientStatus"){
				                if($filterText == strtolower("blocked") or $filterText == strtolower("inactive") or $filterText == strtolower("disabled")){
				                    $filterText=0;
				                    }else{
				                    $filterText=1;
				                }
				                $sqlVars .= " and $filter =".$filterText;
				            }// get doctor id
			*/
			$cui_users_fields_arr = [
				'userFname',
				'userLname',
			];
			$isUsers = false;
			if (in_array($filter, $cui_users_fields_arr) || in_array($filter2, $cui_users_fields_arr)) {
				$where_clause_user = "";
				if (in_array($filter, $cui_users_fields_arr)) {
					$filter_reserved = true;
					if ($filterText != "") {
						$where_clause_user .= "$filter like '%$filterText%'";
						if (in_array($filter2, $cui_users_fields_arr)) {
							$filter_reserved2 = true;
							if ($filterText2 != "") {
								$where_clause_user .= " $filterAndOr $filter2 like '%$filterText2%'";
							}
						}
					} else {
						if (in_array($filter2, $cui_users_fields_arr)) {
							$filter_reserved2 = true;
							if ($filterText2 != "") {
								$where_clause_user .= "$filter2 like '%$filterText2%'";
							}
						}
					}
				} else {
					if (in_array($filter2, $cui_users_fields_arr)) {
						$filter_reserved2 = true;
						if ($filterText2 != "") {
							$where_clause_user .= "$filter2 like '%$filterText2%'";
						}
					}
				}
				if ($where_clause_user != "") {
					$isUsers = true;
					$query_syntax = "select userId from cui_users where $where_clause_user AND userLevel = '$applicationDoctorLevelId'";
					$sql = mysqli_query($con, $query_syntax);
					if (@mysqli_num_rows($sql) > 0) {
						$sqlVars .= " and (";
						$sqlCounter = 0;
						while ($rs = mysqli_fetch_object($sql)) {
							$sqlCounter++;
							if ($sqlCounter > 1) {
								$sqlVars .= " or ";
							}
//						if($sqlCounter>1){
							//							$sqlVars .= " or ";
							//						}
							$sqlVars .= "userId = '" . $rs->userId . "'";
						}
						$sqlVars .= ")";
					}
				}
			}
//            if($filter == "userFname" || $filter2 == "userFname"){
			//
			//				$where_clause = "userFname like '%$filterText%'";
			//				if(in_array($filter2, $cui_users_fields_arr)){
			//					$where_clause .= " $filterAndOr $filter2 like '%$filterText2%'";
			//				}
			////                $sql= mysql_query("select userId from cui_users where userFname like '%$filterText%' AND userLevel = '$applicationDoctorLevelId'");
			//                $query_syntax = "select userId from cui_users where $where_clause AND userLevel = '$applicationDoctorLevelId'";
			//				echo $query_syntax;
			//				echo "<br />";
			//				$sql= mysqli_query($con, $query_syntax);
			////                if(mysql_num_rows($sql)>0){
			//				echo mysqli_num_rows($sql);
			//				echo "<br />";
			//                if(@mysqli_num_rows($sql)>0){
			//                    $sqlVars .= " and (";
			//                    $sqlCounter=0;
			////                    while ($rs = mysql_fetch_object($sql)) {
			//                    while ($rs = mysqli_fetch_object($sql)) {
			//                        $sqlCounter++;
			////                        if($sqlCompanies>1){
			////                            $sqlVars .= " or ";
			////                        }
			//                        if($sqlCounter>1){
			//                            $sqlVars .= " or ";
			//                        }
			//                        $sqlVars .= "userId = '".$rs->userId."'";
			//                    }
			//                    $sqlVars .= ")";
			//                }
			//            }// get doctor id
			//            else if($filter == "userLname" || $filter2 == "userLname"){
			////                $sql= mysql_query("select userId from cui_users where userLname like '%$filterText%' AND userLevel = '$applicationDoctorLevelId'");
			//                $sql= mysqli_query($con, "select userId from cui_users where userLname like '%$filterText%' $filterAndOr userLname like '%$filterText2%' AND userLevel = '$applicationDoctorLevelId'");
			////                if(mysql_num_rows($sql)>0){
			//                if(@mysqli_num_rows($sql)>0){
			//                    $sqlVars .= " and (";
			//                    $sqlCounter=0;
			////                    while ($rs = mysql_fetch_object($sql)) {
			//                    while ($rs = mysqli_fetch_object($sql)) {
			//                        $sqlCounter++;
			//                        if($sqlCompanies>1){
			//                            $sqlVars .= " or ";
			//                        }
			//                        $sqlVars .= "userId = '".$rs->userId."'";
			//                    }
			//                    $sqlVars .= ")";
			//                }
			//            }
			// get office name
			//            else if($filter == "officeName" || $filter2 == "officeName"){
			$isOffice = false;
			if ($filter == "officeName" || $filter2 == "officeName") {
				$where_clause_office = "";
				if ($filter == "officeName") {
					$filter_reserved = true;
					if ($filterText != "") {
						$where_clause_office .= "officeName like '%$filterText%'";
						if ($filter2 == "officeName") {
							$filter_reserved2 = true;
							if ($filterText2 != "") {
								$where_clause_office .= " $filterAndOr officeName like '%$filterText2%'";
							}
						}
					} else {
						if ($filter2 == "officeName") {
							$filter_reserved2 = true;
							if ($filterText2 != "") {
								$where_clause_office .= "officeName like '%$filterText2%'";
							}
						}
					}
				} else {
					if ($filter2 == "officeName") {
						$filter_reserved2 = true;
						if ($filterText2 != "") {
							$where_clause_office .= "officeName like '%$filterText2%'";
						}
					}
				}
				if ($where_clause_office != "") {
					$isOffice = true;
					//                $sql= mysql_query("select officeId from cui_companies_offices where officeName like '%$filterText%'");
					//                $sql= mysqli_query($con, "select officeId from cui_companies_offices where officeName like '%$filterText%' $filterAndOr officeName like '%$filterText2%'");
					$sql = mysqli_query($con, "select officeId from cui_companies_offices where $where_clause_office");
//                if(mysql_num_rows($sql)>0)
					if (@mysqli_num_rows($sql) > 0) {
//                    $sqlVars .= " and (";
						if ($isUsers) {
							$sqlVars .= " $filterAndOr (";
						} else {
							$sqlVars .= " and (";
						}
						$sqlCounter = 0;
//                    while ($rs = mysql_fetch_object($sql)) {
						while ($rs = mysqli_fetch_object($sql)) {
							$sqlCounter++;
							if ($sqlCounter > 1) {
								$sqlVars .= " or ";
							}
							$sqlVars .= "officeId = '" . $rs->officeId . "'";
						}
						$sqlVars .= ")";
					} else {
						$sqlVars .= " and (";
						$sqlVars .= "officeId = '0'";
						$sqlVars .= ")";
					}
				} else {
					$sqlVars .= " and (";
					$sqlVars .= "officeId = '0'";
					$sqlVars .= ")";
				}
			}
			// get dental company name
			//            else if($filter == "patientAppDate" || $filter2 == "patientAppDate"){
			$isAppointmentData = false;
			if ($filter == "patientAppDate" || $filter2 == "patientAppDate") {
				if ($filter == "patientAppDate") {
					$filter_reserved = true;
				}
				if ($filter2 == "patientAppDate") {
					$filter_reserved2 = true;
				}
				//if not empty
				if ($filter == "patientAppDate" && $filterText != "") {
					$filterText = formatDate($filterText, 2);
				}
				if ($filter2 == "patientAppDate" && $filterText2 != "") {
					$filterText2 = formatDate($filterText2, 2);
				}
				$where_clause_appointment_data = "";
				if ($filter == "patientAppDate") {
					if ($filterText != "") {
						if ($isUsers || $isOffice) {
							$where_clause_appointment_data .= " $filterAndOr $filter like '%" . $filterText . "%'";
						} else {
							$where_clause_appointment_data .= " and $filter like '%" . $filterText . "%'";
						}
						if ($filter2 == "patientAppDate") {
							if ($filterText2 != "") {
								$where_clause_appointment_data .= " $filterAndOr $filter2 like '%" . $filterText2 . "%'";
							}
						}
					} else {
						if ($filter2 == "patientAppDate") {
							if ($filterText2 != "") {
								if ($isUsers || $isOffice) {
									$where_clause_appointment_data .= " $filterAndOr $filter2 like '%" . $filterText2 . "%'";
								} else {
									$where_clause_appointment_data .= " and $filter2 like '%" . $filterText2 . "%'";
								}
							}
						}
					}
				} else {
					if ($filter2 == "patientAppDate") {
						if ($filterText2 != "") {
							if ($isUsers || $isOffice) {
								$where_clause_appointment_data .= " $filterAndOr $filter2 like '%" . $filterText2 . "%'";
							} else {
								$where_clause_appointment_data .= " and $filter2 like '%" . $filterText2 . "%'";
							}
						}
					}
				}
				if ($where_clause_appointment_data != "") {
					$isAppointmentData = true;
					$sqlVars .= $where_clause_appointment_data;
				}

				//if not empty
				//                if($filterText!=""){
				//                	$filterText = formatDate($filterText,2);
				//                	//override
				//                	$sqlVars .= " and $filter like '%".$filterText."%'";
				//                }
				//                if($filterText2!=""){
				//                	$filterText2 = formatDate($filterText2,2);
				//                	//override
				//                	$sqlVars .= " $filterAndOr $filter2 like '%".$filterText2."%'";
				//                }
			}
			// get dental company name
			//            else if($filter == "companyName" || $filter == "companyName"){
			$isCompany = false;
			if ($filter == "companyName" || $filter2 == "companyName") {
				$where_clause_company = "";
				if ($filter == "companyName") {
					$filter_reserved = true;
					if ($filterText != "") {
						$where_clause_company .= "$filter like '%$filterText%'";
						if ($filter2 == "companyName") {
							$filter_reserved2 = true;
							if ($filterText2 != "") {
								$where_clause_company .= " $filterAndOr $filter2 like '%$filterText2%'";
							}
						}
					} else {
						if ($filter2 == "companyName") {
							$filter_reserved2 = true;
							if ($filterText2 != "") {
								$where_clause_company .= "$filter2 like '%$filterText2%'";
							}
						}
					}
				} else {
					if ($filter2 == "companyName") {
						$filter_reserved2 = true;
						if ($filterText2 != "") {
							$where_clause_company .= "$filter2 like '%$filterText2%'";
						}
					}
				}
				if ($where_clause_company != "") {
					$isCompany = true;
					//                $sql= mysql_query("select companyId from cui_companies where companyName like '%$filterText%'");
					$sql = mysqli_query($con, "select companyId from cui_companies where $where_clause_company");
//                if(mysql_num_rows($sql)>0){
					if (@mysqli_num_rows($sql) > 0) {
//                    $sqlVars .= " and (";
						if ($isUsers || $isOffice || $isAppointmentData) {
							$sqlVars .= " $filterAndOr (";
						} else {
							$sqlVars .= " and (";
						}
						$sqlCounter = 0;
//                    while ($rs = mysql_fetch_object($sql)) {
						while ($rs = mysqli_fetch_object($sql)) {
							$sqlCounter++;
							if ($sqlCounter > 1) {
								$sqlVars .= " or ";
							}
							$sqlVars .= "a.companyId = '" . $rs->companyId . "'";
						}
						$sqlVars .= ")";
					} else {
						$sqlVars .= " and (";
						$sqlVars .= "a.companyId = '0'";
						$sqlVars .= ")";
					}
				} else {
					$sqlVars .= " and (";
					$sqlVars .= "a.companyId = '0'";
					$sqlVars .= ")";
				}
			}
//            else{
			////                $sqlVars .= " and $filter like '%".$filterText."%'";
			//            }

//			patientGroup
			if ($filter == "patientGroup") {
				if ($filterText != "") {
					$filterText = encrypt_decrypt('encrypt', $filterText);
				}
			}
			if ($filter2 == "patientGroup") {
				if ($filterText2 != "") {
					$filterText2 = encrypt_decrypt('encrypt', $filterText2);
				}
			}
			if (!$filter_reserved) {
				if (!$filter_reserved2) {
					// both
					if ($filter != "" && $filterText != "") {
						$sqlVars .= " and $filter like '%" . $filterText . "%'";
					}
					if ($filter2 != "" && $filterText2 != "") {
						$sqlVars .= " $filterAndOr $filter2 like '%" . $filterText2 . "%'";
					}
				} else {
					// only filter
					if ($filter != "" && $filterText != "") {
						$sqlVars .= " $filterAndOr $filter like '%" . $filterText . "%'";
					}
				}
			} else {
				if (!$filter_reserved2) {
					if ($filter2 != "" && $filterText2 != "") {
						$sqlVars .= " $filterAndOr $filter2 like '%" . $filterText2 . "%'";
					}
				}
			}

//            $queryString .= "&filter=".$filter."&filterText=".$filterText;
			$queryString .= "&filter=$filter&filterText=$filterText&filter2=$filter2&filterText2=$filterText2&filterAndOr=$filterAndOr";
		}
	}
	//if sort by is there
	if ($sortBy != "") {
		//if patient status is checked or updated
		if ($patientStatus == "Check" or $patientStatus == "Checked" or $patientStatus == "Updated") {
			//$sqlVars .= " GROUP BY patientId order by b.prioritySort ASC";
			$sqlVars .= " GROUP BY patientId order by $sortBy $sortOrder, b.prioritySort ASC";
		} else {
			$sqlVars .= " GROUP BY patientId order by $sortBy $sortOrder";
		}
	} else {
		if ($_GET["patientStatus"] == "Check") {
			$sqlVars .= " GROUP BY patientId order by b.prioritySort ASC, patientAppDate ASC";
		} else if ($_GET["patientStatus"] == "Updated" || $_GET["patientStatus"] == "Printed") {
			$sqlVars .= " GROUP BY patientId order by patientUpdated DESC, patientId DESC";
		} else {
			//$sqlVars .= " order by patientFname ASC";
			//$sqlVars .= " order by patientId DESC";
			$sqlVars .= " GROUP BY patientId order by patientUpdated DESC, patientId DESC";
		}
	}
	//echo $sqlVars; exit;
	$itemCountSql = "SELECT 
  a.*,
  b.*,
  c.companyName 
FROM
  cui_patients AS a 
  JOIN cui_patients_priority AS b 
  JOIN cui_companies AS c 
WHERE a.companyId = c.companyId 
  AND patientId != '0' 
  AND a.patientPriority = b.priorityTitle 
GROUP BY patientId 
ORDER BY patientUpdated DESC,
  patientId DESC ";

	//die("$itemCountSql");
	//	$itemCountResult = mysql_query($itemCountSql);
	$itemCountResult = mysqli_query($con, $itemCountSql);
	//	$itemCount = mysql_num_rows($itemCountResult);
	$itemCount = @mysqli_num_rows($itemCountResult);
//	die(json_encode($itemCountSql));
	//if item count > 0
	if ($itemCount > 0) {
		//pagination vars
		$limit = 20;
		$PaginateIt = new pagination();
		$PaginateIt->SetItemsPerPage($limit);
		$PaginateIt->SetLinksToDisplay(5);
		$PaginateIt->SetItemCount($itemCount);
		$PaginateIt->SetLinksFormat('<< Back', ' ', 'Next >>');
		if ($_GET["page"]) {
			$pageVar = ($limit * $_GET["page"]) - $limit;
		} else {
			$pageVar = 0;
		}
		if ($itemCount) {
			$productPageLinks = $PaginateIt->GetPageLinks();
		}

		$counter = 0;
		$sqlPatients = "SELECT a.*,b.*,c.companyName FROM cui_patients as a
				LEFT JOIN cui_patients_priority AS b ON b.`priorityTitle` = a.patientPriority
				LEFT JOIN cui_companies AS c ON a.companyId = c.companyId
				WHERE a.companyId=c.companyId AND patientId !='0' $sqlVars " . $PaginateIt->GetSqlLimit();
//	    $sqlPatientsResult=mysql_query($sqlPatients);
		$sqlPatientsResult = mysqli_query($con, $sqlPatients);
//	    while ($sqlPatientsRs=mysql_fetch_array($sqlPatientsResult)) {
		$noDuplicate_arr = [];
		while ($sqlPatientsRs = @mysqli_fetch_array($sqlPatientsResult)) {
			$noDuplicate_arr[] = $sqlPatientsRs;
		}
//                while ($sqlPatientsRs = @mysqli_fetch_array($sqlPatientsResult)) {
		foreach ($noDuplicate_arr as $arr_key => $sqlPatientsRs) {
			$counter++;
			$patientId = $sqlPatientsRs["patientId"];
			$patientCode = $sqlPatientsRs["patientCode"];
			$patientFname = $sqlPatientsRs["patientFname"];
			$patientLname = $sqlPatientsRs["patientLname"];
			$officeId = $sqlPatientsRs["officeId"];
			$companyId = $sqlPatientsRs["companyId"];
			$patientInsCompany = $sqlPatientsRs["patientInsCompany"];
			$patientCustomDone = ($sqlPatientsRs["patientCustomDone"] ? "C" : "");
			$patientFullDone = ($sqlPatientsRs["patientFullDone"] ? "F" : "");
			$patientPartialDone = ($sqlPatientsRs["patientPartialDone"] ? "P" : "");
			$patientOrthoDone = ($sqlPatientsRs["patientOrthoDone"] ? "O" : "");
			$patientEligibility = "";
			if ($patientFullDone == "F") {
				$patientEligibility .= "F";
			}

			if ($patientCustomDone == "C") {
				if ($patientEligibility == "") {
					$patientEligibility .= "C";
				} else {
					$patientEligibility .= " + C";
				}

			}
			if ($patientPartialDone == "P") {
				if ($patientEligibility == "") {
					$patientEligibility .= "P";
				} else {
					$patientEligibility .= " + P";
				}

			}
			if ($patientOrthoDone == "O") {
				if ($patientEligibility == "") {
					$patientEligibility .= "O";
				} else {
					$patientEligibility .= " + O";
				}

			}
			$patientAppDate = $sqlPatientsRs["patientAppDate"];
			$patientAppDate = formatDate(substr($patientAppDate, 0, 10)) . " " . changeTime(substr($patientAppDate, 11, 10), 24);
			$patientStatus = $sqlPatientsRs["patientStatus"];
			$patientPriority = $sqlPatientsRs["patientPriority"];
			if ($counter > 1) {
				$bgcolor = "lightgrey";
				$counter = 0;
			} elsE {
				$bgcolor = "#FFFFFF";
			}
			?>
                    <tr class="alternatebg">
						<?
			$row_cname = getField("cui_companies", "companyId", $companyId, "companyName");
			$row_oname = getField("cui_companies_offices", "officeId", $officeId, "officeName");
			?>
                        <td align="center"><?=$patientId?></td>
                        <td><?=$patientFname?>&nbsp;<?=$patientLname?></td>
                        <td><?=$patientInsCompany?></td>
                        <td><?=$row_cname?></td>
                        <td><?=$row_oname?></td>
                        <td align="center"><?=$patientAppDate?></td>
                        <td align="center"><?=$patientEligibility?></td>
                        <td align="center"><?=$patientStatus?></td>
                        <?php if ($getPatientStatus == "Check") {?>
                            <td align="center"><?=$patientPriority?></td>
                        <?php }?>
                        <td>
                            <table cellpadding="0" cellspacing="2">
                                <tr>
                                    <?
			//roles check (one can delete or not)
			//if($moduleAll == 1 or $modulePatientsDelete == 1){
			//if($_SESSION["cuiSessionUserLevel"] <= 34){
			?>
                                    <td align="left">
										<!--<a title="Patient: (<?=$patientFname . " " . $patientLname . " - " . $patientId;?>) - Company/Office: <?=$row_cname . " / " . $row_oname;?> - Time Spent" href="timeSpent.php?patientId=<?=$patientId?>&TB_iframe=true&height=300&width=1000" class="btn btn-default btn-sm thickbox">-->
										<a title="View Time Spent" href="timeSpent.php?patientId=<?=$patientId?>&TB_iframe=true&height=300&width=1000" class="btn btn-default btn-sm thickbox">
											<span class="glyphicon glyphicon-time"></span>
										</a>
                                    </td>
                                    <td width="5px">&nbsp;</td>
                                    <?//} ?>
                                    <?
			//roles check (one can edit or not)
			if ($moduleAll == 1 or $modulePatientsView == 1) {
				?>
                                        <td align="left">
											<!--<a title="Patient: (<?=$patientFname . " " . $patientLname . " - " . $patientId;?>) - Company/Office: <?=$row_cname . " / " . $row_oname;?> - History" href="history.php?patientId=<?=$patientId?>&TB_iframe=true&height=300&width=900" class="btn btn-default btn-sm thickbox">-->
											<a title="View History" href="history.php?patientId=<?=$patientId?>&TB_iframe=true&height=300&width=900" class="btn btn-default btn-sm thickbox">
												<span class="glyphicon glyphicon-check"></span>
											</a>
                                        </td>
                                        <td width="5px">&nbsp;</td>
                                        <?
			}?>
                                    <?
			//roles check (one can edit or not)
			if ($moduleAll == 1 or $modulePatientsView == 1) {
				?>
                                        <td align="left">
											<a title="View" class="btn btn-default btn-sm" href="index.php?do=<?=$do?>&act=view&patientId=<?=$patientId?>&patientStatus=<?=$patientStatus?>">
												<span class="glyphicon glyphicon-zoom-in"></span>
											</a>
                                        </td>
                                        <td width="5px">&nbsp;</td>
                                        <?
			}?>
                                    <?
			if ($moduleAll == 1 or $modulePatientsEdit == 1) {
				?>
                                        <td align="left">
											<a title="Edit" class="btn btn-default btn-sm edit" href="index.php?do=<?=$do?>&act=edit&patientId=<?=$patientId?>&patientStatus=<?=$patientStatus?>">
												<span class="glyphicon glyphicon-pencil"></span>
											</a>
                                        </td>
                                        <td width="5px">&nbsp;</td>
                                        <?
			}?>
                                    <?
			//roles check (one can delete or not)
			if ($moduleAll == 1 or $modulePatientsDelete == 1) {
				?>
                                        <td>
                                            <?if ($_SESSION["cuiSessionUserLevel"] == 1) {?>
												<!--<a title="Delete" class="btn btn-default btn-sm trash" href="javascript:if(confirm('Proceed with deletion?')){window.location='index.php?do=<?=$do?>&act=delete&patientId=<?=$patientId?>';}">-->
												<a title="Delete" class="btn btn-default btn-sm trash" href="javascript:swal({title: 'Confirmation!',text: 'Proceed with deletion?',type: 'warning',showCancelButton: true,confirmButtonText: 'OK',closeOnConfirm: false},function(){swal.close();window.location='index.php?do=<?=$do?>&act=delete&patientId=<?=$patientId?>';})">
													<span class="glyphicon glyphicon-trash"></span>
												</a>
                                            <?} else {
					?>
                                                <?
					//checking permision for Dental to delete record
					$tmpCustom = getField("cui_companies", "companyId", $companyId, "companyCustom", " and companyStatus='1'");
					$tmpCompName = getField("cui_companies", "companyId", $companyId, "companyName", " and companyStatus='1'");
					$tmpCompTag = getField("cui_custom_companies", "custCompanyName", $tmpCompName, "custCompanyTag", " and custCompanyStatus='1'");
					if ($tmpCustom == 1) {
						$tblName = "cui_patients_" . $tmpCompTag;
					} else {
						$tblName = "cui_patients_full";
					}
					// checking record inserted or not
					$tmpSqlPatients = "SELECT * FROM " . $tblName . " WHERE patientId='$patientId'";
//					$tmpResultPatients = mysql_query($tmpSqlPatients);
					$tmpResultPatients = mysqli_query($con, $tmpSqlPatients);
//					$tmpTotalRec = mysql_num_rows($tmpResultPatients);
					$tmpTotalRec = @mysqli_num_rows($tmpResultPatients);
					if ($patientStatus == "New" || ($patientStatus == "Check" && $tmpTotalRec == 0)) {
						?>
													<!--<a title="Delete" class="btn btn-default btn-sm trash" href="javascript:if(confirm('Proceed with deletion?')){window.location='index.php?do=<?=$do?>&act=delete&patientId=<?=$patientId?>';}">-->
													<a title="Delete" class="btn btn-default btn-sm trash" href="javascript:swal({title: 'Confirmation!',text: 'Proceed with deletion?',type: 'warning',showCancelButton: true,confirmButtonText: 'OK',closeOnConfirm: false},function(){swal.close();window.location='index.php?do=<?=$do?>&act=delete&patientId=<?=$patientId?>';})">
														<span class="glyphicon glyphicon-trash"></span>
													</a>
                                                <?} else {
						?>
													<a title="Delete" class="btn btn-default btn-sm trash" href="javascript:void(0);" onclick="swal({title: 'Error!',text: 'Please contact system administrator to delete the patient.',type: 'error',showCancelButton: true,confirmButtonText: 'OK',closeOnConfirm: true});">
														<span class="glyphicon glyphicon-trash"></span>
													</a>
                                                <?
					}
					?>
                                            <?}?>
                                        </td>
                                        <td width="5px">&nbsp;</td>
                                        <?
			}?>
                                    <?
			//roles check (one can delete or not)
			if ($moduleAll == 1 or $modulePatientsView == 1) {
				?>

                                            <?php
//status buttons
				switch ($patientStatus) {
				case 'New':
				case 'Printed':
					if (($modulePatientsPrinted == 1 or $moduleAll == 1) and ($_GET["patientStatus"] == "Printed")) {
						//echo "<input type=\"button\" alt=\"report.php?patientId=$patientId&TB_iframe=true&height=150&width=300\" class=\"thickbox smallButton\" value=\"Report\" title=\"Report\"/>";
						echo "<td><input type=\"button\" title=\"Patient: $patientFname $patientLname - $patientId - Report\" class=\"btn btn-default btn-sm thickbox\" alt=\"report.php?patientId=$patientId&TB_iframe=true&height=200&width=400\" value=\"Report\"></td>";
					} else {
						//echo "<input type=\"button\" alt=\"#TB_inline?height=150&width=300&inlineId=actionDivCheck\" title=\"Actions\" class=\"thickbox smallButton\" value=\"Check\" onclick=\"updatePatientHidden($patientId)\" />";
						//echo "<input type=\"button\" alt=\"check.php?patientId=$patientId&TB_iframe=true&height=150&width=300\" title=\"Actions\" class=\"thickbox smallButton\" value=\"Check\" />";
						echo "<td><input type=\"button\" title=\"Patient: $patientFname $patientLname - $patientId - Check\" class=\"btn btn-default btn-sm thickbox\" alt=\"check.php?patientId=$patientId&TB_iframe=true&height=200&width=400\" value=\"Check\"></td>";
					}
					break;
				case 'Check':
					if ($modulePatientsCheck == 1 or $moduleAll == 1) {
						echo " <td><a title=\"Update\" class=\"btn btn-default btn-sm\" href=\"index.php?do=$do&act=edit&patientId=$patientId$queryString\">
															Update
														</a> </td>";
					}

					break;
				case 'Updated':
					break;
				}
				if ($subheading != 'Check') {
					if ($_GET["patientStatus"] != "Printed" && $patientStatus != "New" and ($modulePatientsPrinted == 1 or $moduleAll == 1))
					//echo "<td><input type=\"button\" alt=\"report.php?patientId=$patientId&TB_iframe=true&height=170&width=300\" class=\"thickbox smallButton\" value=\"Print\" title=\"Print\"/> </td>";
					{
						echo "<td><input type=\"button\" title=\"Patient: $patientFname $patientLname - $patientId - Print\" class=\"btn btn-default btn-sm thickbox\" alt=\"report.php?patientId=$patientId&TB_iframe=true&height=200&width=400\" value=\"Print\"> </td>";
					}

				}
				?>

                                        <?
			}?>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <?
		}
		//if pagination is needed
		if ($itemCount > $limit) {
			?>
                    <tr>
                        <td colspan="10" align="center"><?echo $productPageLinks; ?></td>
                    </tr>
                    <?
		}
	}
	else {
		echo "<tr>
        <td colspan=\"10\" align=\"center\" height=\"100px\">No Results Found !</td>
        </tr>";
	}
	?>
			</tbody>
        </table>
    <?}?>
</div>
<div id="actionDivCheck" style="display: none">
    <form method="POST"
          action="<?=HTTP_SERVER?>index.php?do=patients&patientId=<?=$patientId?>&act=changeStatus&parameter=Check">
        <input type="hidden" name="patientId" id="patientId" value=""/>
        <table cellpadding="3" cellspacing="0" align="center">
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">Please choose from the options below:</td>
            </tr>
            <?if ($checkCustom == "0" || $checkCustom == "") {?>
                <tr>
                    <td width="150px"><b>Eligibility Check</b></td>
                    <td>
                        <table cellpadding="1" cellspacing="0">
                            <tr>
                                <td><input type="radio" name="eligibility" value="Full" checked/></td>
                                <td>Full</td>
                                <td width="15px">&nbsp;</td>
                                <td><input type="radio" name="eligibility" value="Partial"/></td>
                                <td>Partial</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td><b>Include Ortho?</b></td>
                    <td>
                        <table cellpadding="1" cellspacing="0">
                            <tr>
                                <td><input type="radio" name="ortho" value="Yes"/></td>
                                <td>Yes</td>
                                <td width="15px">&nbsp;</td>
                                <td><input type="radio" name="ortho" value="No" checked/></td>
                                <td>No</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            <?} else {
	?>
                <tr>
                    <td width="150px"><b>Eligibility Check</b></td>
                    <td>
                        <table cellpadding="1" cellspacing="0">
                            <tr>
                                <td><input type="radio" name="eligibility" value="Custom" checked/></td>
                                <td>Custom</td>
                                <?if ($_SESSION["tmpSessionCompanyId"] == "16"
		|| $_SESSION["tmpSessionCompanyId"] == "20"
		|| $_SESSION["tmpSessionCompanyId"] == "21"
		|| $_SESSION["tmpSessionCompanyId"] == "22"
		|| $_SESSION["tmpSessionCompanyId"] == "27"
		|| $_SESSION["tmpSessionCompanyId"] == "29"
		|| $_SESSION["tmpSessionCompanyId"] == "33"
		|| $_SESSION["tmpSessionCompanyId"] == "29"
		|| $_SESSION["tmpSessionCompanyId"] == "30"
		|| $_SESSION["tmpSessionCompanyId"] == "34"
		|| $_SESSION["tmpSessionCompanyId"] == "83"
		|| $_SESSION["tmpSessionCompanyId"] == "88"
		|| $_SESSION["tmpSessionCompanyId"] == "103"
		|| $_SESSION["tmpSessionCompanyId"] == "113"
		|| $_SESSION["tmpSessionCompanyId"] == "114"
		|| $_SESSION["tmpSessionCompanyId"] == "115"
		|| $_SESSION["tmpSessionCompanyId"] == "118"
		|| $_SESSION["tmpSessionCompanyId"] == "119"
		|| $_SESSION["tmpSessionCompanyId"] == "120"
		|| $_SESSION["tmpSessionCompanyId"] == "121"
		|| $_SESSION["tmpSessionCompanyId"] == "122"
		|| $_SESSION["tmpSessionCompanyId"] == "124"
		|| $_SESSION["tmpSessionCompanyId"] == "125"
		|| $_SESSION["tmpSessionCompanyId"] == "126"
		|| $_SESSION["tmpSessionCompanyId"] == "127"
		|| $_SESSION["tmpSessionCompanyId"] == "128"
		|| $_SESSION["tmpSessionCompanyId"] == "130"
		|| $_SESSION["tmpSessionCompanyId"] == "131"
		|| $_SESSION["tmpSessionCompanyId"] == "132"
		|| $_SESSION["tmpSessionCompanyId"] == "133"
		|| $_SESSION["tmpSessionCompanyId"] == "134"
		|| $_SESSION["tmpSessionCompanyId"] == "135"
		|| $_SESSION["tmpSessionCompanyId"] == "136"
		|| $_SESSION["tmpSessionCompanyId"] == "137"
		|| $_SESSION["tmpSessionCompanyId"] == "138"
		|| $_SESSION["tmpSessionCompanyId"] == "139"
		|| $_SESSION["tmpSessionCompanyId"] == "140"
		|| $_SESSION["tmpSessionCompanyId"] == "141"
		|| $_SESSION["tmpSessionCompanyId"] == "142"
		|| $_SESSION["tmpSessionCompanyId"] == "143"
		|| $_SESSION["tmpSessionCompanyId"] == "144"
		|| $_SESSION["tmpSessionCompanyId"] == "145"
		|| $_SESSION["tmpSessionCompanyId"] == "146"
		|| $_SESSION["tmpSessionCompanyId"] == "147"
		|| $_SESSION["tmpSessionCompanyId"] == "148"
		|| $_SESSION["tmpSessionCompanyId"] == "149"
		|| $_SESSION["tmpSessionCompanyId"] == "150"
		|| $_SESSION["tmpSessionCompanyId"] == "151"
		|| $_SESSION["tmpSessionCompanyId"] == "152"
		|| $_SESSION["tmpSessionCompanyId"] == "153"
		|| $_SESSION["tmpSessionCompanyId"] == "154"
		|| $_SESSION["tmpSessionCompanyId"] == "155"
		|| $_SESSION["tmpSessionCompanyId"] == "156"
		|| $_SESSION["tmpSessionCompanyId"] == "157"
		|| $_SESSION["tmpSessionCompanyId"] == "158"
		|| $_SESSION["tmpSessionCompanyId"] == "159"
		|| $_SESSION["tmpSessionCompanyId"] == "160"
		|| $_SESSION["tmpSessionCompanyId"] == "161"
		|| $_SESSION["tmpSessionCompanyId"] == "162"
		|| $_SESSION["tmpSessionCompanyId"] == "163"
		|| $_SESSION["tmpSessionCompanyId"] == "164"
		|| $_SESSION["tmpSessionCompanyId"] == "165"
		|| $_SESSION["tmpSessionCompanyId"] == "166"
		|| $_SESSION["tmpSessionCompanyId"] == "167"
		|| $_SESSION["tmpSessionCompanyId"] == "168"
		|| $_SESSION["tmpSessionCompanyId"] == "169"
		|| $_SESSION["tmpSessionCompanyId"] == "170"
		|| $_SESSION["tmpSessionCompanyId"] == "172"
		|| $_SESSION["tmpSessionCompanyId"] == "173"
		|| $_SESSION["tmpSessionCompanyId"] == "174"
		|| $_SESSION["tmpSessionCompanyId"] == "175"
		|| $_SESSION["tmpSessionCompanyId"] == "176"
		|| $_SESSION["tmpSessionCompanyId"] == "177"
		|| $_SESSION["tmpSessionCompanyId"] == "178") {?>
                                    <td width="15px">&nbsp;</td>
                                    <td><input type="radio" name="eligibility" value="Partial"/></td>
                                    <td>Partial</td>
                                <?}?>
                            </tr>
                        </table>
                    </td>
                </tr>
            <?}?>
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" value="Done" class="smallButton"
                           onclick="javascript:if(document.getElementById('patientId').value == ''){return false;}"/>
                </td>
            </tr>
        </table>
    </form>
    <input type="hidden" value="0" name="removepopup" id="removepopup"/>
</div>