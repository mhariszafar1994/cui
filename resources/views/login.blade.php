	@extends('layout')

	@section('content')
	<div class="top_bar">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-12 col-sm-12">
					<div class="row">
						<div class="col-lg-5 col-md-12 col-sm-12">
							<i class="fa fa-clock"></i> Mon to Fri: 8:00 am-5:00 pm PST
						</div>
						<div class="col-lg-7 col-md-12 col-sm-12">
							<i class="fa fa-globe"></i> <a href="" title="">www.checkurinsurance.com</a>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12 col-sm-12">
					<div class="row">
						<div class="col-lg-3 offset-lg-4 col-md-12 col-sm-12">
							<i class="fa fa-phone"></i> <a href="" title="">951-479.1967</a>
						</div>
						<div class="col-lg-5 col-md-12 col-sm-12">
							<i class="fa fa-envelope"></i> <a href="" title="">info@checkurinsurance.com</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="header_banner" style="background:url('{{asset('/assets/img/lonin_banner.jpg')}}') #fff;">
		
	</div>
	<!-- begin #page-container -->
	<div id="page-container" class="fade">
		<!-- begin login -->
		<div class="login login-v1">
			<div class="row w-100">
				<div class="col-lg-4 offset-lg-4">
					<!-- begin login-container -->
					<div class="login-container">
						<!-- begin login-header -->
						<div class="login-header">
							<div class="brand">
								<b>CUI </b>Login
								<small>Exsiting members may login below using their login ID and password. New members please contact CUI to get access at the applicaiton.</small>
							</div>
							<!-- <div class="icon">
								<i class="fa fa-lock"></i>
							</div> -->
						</div>
						<!-- end login-header -->
						<!-- begin login-body -->
						<div class="login-body">
							<!-- begin login-content -->
							<div class="login-content">
								<form action="index.html" method="GET" class="margin-bottom-0">
									<div class="form-group m-b-20">
										<input type="text" class="form-control form-control-lg inverse-mode" placeholder="User name" required />
									</div>
									<div class="form-group m-b-20">
										<input type="password" class="form-control form-control-lg inverse-mode" placeholder="Password" required />
									</div>
									<div class="row">
										<div class="col-lg-5 col-md-12">
											<div class="checkbox checkbox-css m-b-20 pt-0">
												<input type="checkbox" id="remember_checkbox" /> 
												<label for="remember_checkbox">
												Remember Me
												</label>
											</div>
										</div>
										<div class="col-lg-7 col-md-12">
											<div class="text-right">
												<a class="text-light" href="javascript:;" title="">Forgot password?</a>
												&nbsp;|&nbsp;
												<a class="text-light" href="javascript" title="">New User?</a>
											</div>
										</div>
									</div>
									<div class="login-buttons">
										<button type="submit" class="btn btn-red btn-block btn-lg">Sign in</button>
									</div>
								</form>
							</div>
							<!-- end login-content -->
						</div>
						<!-- end login-body -->
					</div>
					<!-- end login-container -->
				</div>
			</div>
		</div>
		<!-- end login -->
	</div>
	<!-- end page container -->
@endsection
