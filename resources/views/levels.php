<?
//levels check
if($moduleAll == 0 and $moduleLevelView == 0){
	echo "<script>window.location='index.php?do=authorization'</script>";
}
?>

<h1 class="h1WithBg">User Levels</h1>	
<div id="pageContainer">
	
<?
//vars
$success = 0;
$act = "";
$showLevelForm = 0;
$showLimitedRights = 1;
$msg = "";
$error = 0;
$levelId = 0;

//if get act
if($_GET["act"]){
	$act = sanitize($_GET["act"]);
}

//if get msg
if($_GET["msg"]){
	$msg = sanitize($_GET["msg"]);
}

//if act == changestatus
if($act == "changeStatus"){
	
	//levels check (one can edit or not)
	if($moduleAll == 0 and $moduleLevelEdit == 0){
		echo "<script>window.location='index.php?do=authorization'</script>";
	}
	
	$levelId = sanitize($_GET["levelId"]);
	$statusParameter = sanitize($_GET["parameter"]);
	if($statusParameter == "disable"){
//		mysql_query("UPDATE cui_users_levels set levelStatus=0 where levelId='$levelId'");
		mysqli_query($con, "UPDATE cui_users_levels set levelStatus=0 where levelId='$levelId'");
		$success = 1;
		$msg = "Success: Level has been blocked,";
	}else{
//		mysql_query("UPDATE cui_users_levels set levelStatus=1 where levelId='$levelId'");
		mysqli_query($con, "UPDATE cui_users_levels set levelStatus=1 where levelId='$levelId'");
		$success = 1;
		$msg = "Success: Level has been activated,";
	}
}

//if act is delete
if($act == "delete"){	
	
	//levels check (one can delete or not)
	if($moduleAll == 0 and $moduleLevelDelete == 0){
		echo "<script>window.location='index.php?do=authorization'</script>";
	}
	
	$levelId=sanitize($_GET["levelId"]);
	
	$sqlCheck = "SELECT * from cui_users where userLevel = '$levelId'";
//	$resultCheck = mysql_query($sqlCheck);
	$resultCheck = mysqli_query($con, $sqlCheck);
//	if(mysql_num_rows($resultCheck)<1){
	if(@mysqli_num_rows($resultCheck)<1){

		$deleteSql = "DELETE from cui_users_levels where levelId='$levelId'";
//		mysql_query($deleteSql);
		mysqli_query($con, $deleteSql);
		$deleteSql = "DELETE from cui_users_levels_functions where levelId='$levelId'";
//		mysql_query($deleteSql);
		mysqli_query($con, $deleteSql);
		$success = 1;
		$msg = "Success: Level has been deleted,";
	
	}else{
		$error = 1;
		$msg = "Cannot delete this level because some users under this category still exist. Please delete all users for this level first.";
	}
}


//getting data
if($act == "edit"){
	
	//levels check (one can edit or not)
	if($moduleAll == 0 and $moduleLevelEdit == 0){
		echo "<script>window.location='index.php?do=authorization'</script>";
	}
	
	$headingTitle = "Edit Level";
	$showLevelForm = 1;
	$levelId=sanitize($_GET["levelId"]);
	$levelql="select * from cui_users_levels where levelId='$levelId'";
//	$levelResult=mysql_query($levelql);
	$levelResult=mysqli_query($con, $levelql);
//	if(mysql_num_rows($levelResult)){
	if(@mysqli_num_rows($levelResult)){
//		$levelDesc=mysql_result($levelResult,0,"levelDesc");
		$levelDesc=mysqli_result($levelResult,0,"levelDesc");
//		$levelTitle=mysql_result($levelResult,0,"levelTitle");
		$levelTitle=mysqli_result($levelResult,0,"levelTitle");
		$btnValue = "Update";
	}
}else{		
	$levelDesc="";
	$levelTitle="";
	$btnValue = "Add";
}

if($act == "add"){
	
	//levels check (one can add or not)
	if($moduleAll == 0 and $moduleLevelAdd == 0){
		echo "<script>window.location='index.php?do=authorization'</script>";
	}
	
	$headingTitle = "Add New Level";
	$showLevelForm = 1;
}


//saving information
if($_POST["submit"]){
	
	
	$showLevelForm = 1;
	$levelTitle=sanitize($_POST["levelTitle"]);
	$levelDesc=sanitize($_POST["levelDesc"]);
	$levelCode =strtolower(str_replace(" ","",$levelTitle));
	
	if($act == "add"){
		$sqlCheck = "SELECT * FROM cui_users_levels where levelTitle = '$levelTitle'";
//		$sqlResult = mysql_query($sqlCheck);
		$sqlResult = mysqli_query($con, $sqlCheck);

//		if(mysql_num_rows($sqlResult)<1){
		if(@mysqli_num_rows($sqlResult)<1){

			$sqlLevel="INSERT INTO cui_users_levels (levelTitle, levelCode) values ('$levelTitle', '$levelCode')";
//			if(mysql_query($sqlLevel)){
			if(mysqli_query($con, $sqlLevel)){

				//get level id
//				$levelId = mysql_insert_id();
				$levelId = mysqli_insert_id($con);

				//first check if module All is there
				if($_POST["moduleAll"]){
					if($_POST["moduleAll"] == 1){
						//insert module all right
//						mysql_query("INSERT INTO cui_users_levels_levels(functionId, functionModule, functionValue)values($levelId, 'moduleAll', 1)");
						mysqli_query($con, "INSERT INTO cui_users_levels_levels(functionId, functionModule, functionValue)values($levelId, 'moduleAll', 1)");
					}
				}else{
					//now insert the functions and rights
					foreach ($_POST as $key=>$value){
						if($key!="levelTitle" and $key!="levelCode" and $key!="levelDesc" and $key!="submit" and $key!="moduleAll"){						
							//insert module all levels
//						mysql_query("INSERT INTO cui_users_levels_functions(levelId, functionModule, functionValue)values($levelId, '$key', $value)");
						mysqli_query($con, "INSERT INTO cui_users_levels_functions(levelId, functionModule, functionValue)values($levelId, '$key', $value)");
						}
					}
				}
				
				
			}
			
			$success=1;		
			$msg = "Success: Level has been added.";
			echo "<script>window.location='".HTTP_SERVER."index.php?do=levels&msg=$msg'</script>";
			
		}else{
			$error = 1;
			$msg = "The following user level (role) already exists !";
		}
	}else{
		//get old level name
		$previousLevel = getField("cui_users_levels","levelId",$levelId,"levelTitle");
		
		$sqlLevel="update cui_users_levels set levelTitle='$levelTitle' where levelId='$levelId'";
//		if(mysql_query($sqlLevel)){
		if(mysqli_query($con, $sqlLevel)){

			//first delete all rights
//			mysql_query("DELETE FROM cui_users_levels_functions where levelId='$levelId'");
			mysqli_query($con, "DELETE FROM cui_users_levels_functions where levelId='$levelId'");

			//first check if module All is there
			if($_POST["moduleAll"]){
				if($_POST["moduleAll"] == 1){
					//insert module all right
//					mysql_query("INSERT INTO cui_users_levels_functions(levelId, functionModule, functionValue)values($levelId, 'moduleAll', 1)");
					mysqli_query($con, "INSERT INTO cui_users_levels_functions(levelId, functionModule, functionValue)values($levelId, 'moduleAll', 1)");
				}
			}else{
				//now insert the functions and rights
				foreach ($_POST as $key=>$value){
					if($key!="levelTitle" and $key!="levelCode" and $key!="levelDesc" and $key!="submit" and $key!="moduleAll"){										
					//insert module all levels
//					mysql_query("INSERT INTO cui_users_levels_functions(levelId, functionModule, functionValue)values($levelId, '$key', $value)");
					mysqli_query($con, "INSERT INTO cui_users_levels_functions(levelId, functionModule, functionValue)values($levelId, '$key', $value)");
					}
				}
			}	
		}
		
		$success=1;
		$msg = "Success: Level details have been updated.";
		echo "<script>window.location='".HTTP_SERVER."index.php?do=levels&msg=$msg'</script>";
	}
	
}



//filters
if($_GET["filter"]){
	$filter = sanitize($_GET["filter"]);
	$filterText = sanitize($_GET["filterText"]);
}else{
	$filterText = "";
	$filter = "";
}

?>

<?if($error == 1){?>
	<div class="error"><?=$msg?></div>		
<?}else{?>
	<?if($msg!=""){?>
		<div class="success"><?=$msg?></div>
	<?}?>	
<?}?>
<style>
		hr {
			border-bottom: 0px;
		}
		table.form-spacing tbody tr td {
			padding-bottom: 9px;
		}
		</style>
<script type="text/javascript" language="JavaScript">
	function chkForm(){
		var a=document.getElementById("levelTitle").value;
		
		if(a==""){
			//alert("One or more required fields are left blank !");
			swal({
				title: 'Error!',
				text: 'One or more required fields are left blank !',
				type: "error",
				showCancelButton: false,
				confirmButtonText: 'OK',
				closeOnConfirm: false
			},
			function(){
				swal.close();
				$("#officeId").focus();
			});
			return false;
		}
		
		if(isNaN(b)){
			//alert("Rate must be a number !");
			swal({
				title: 'Error!',
				text: 'Rate must be a number !',
				type: "error",
				showCancelButton: false,
				confirmButtonText: 'OK',
				closeOnConfirm: false
			},
			function(){
				swal.close();
				$("#officeId").focus();
			});
			return false;
		}
		
	}
	
	//function to show hide limited rights define tr
	function checkRights(fieldThis, showHide){
		if(fieldThis.checked){
			if(showHide == 1){
				document.getElementById("trLimitedRights").style.display = '';
			}else{
				document.getElementById("trLimitedRights").style.display = 'none';
			}
		}
	}
	
</script>
<div id="addLevel" <?if($showLevelForm == 0){?>style="display:none"<?}?>>
	<form method="POST">		
		<table class="form-spacing" align="center" width="100%" border="0" cellpadding="5" cellspacing="1">
			<tr class="titleTr">
				<td><h3 style="padding-top: 9px; padding-left: 5px;"><?=$headingTitle?></h3></td>
			</tr>
			<tr>
				<td>
					<div class="message">The required fields are marked with <span class="required">*</span></div>
				</td>
			</tr>
			<tr>
				<td>
					<table width="95%" align="center" cellpadding="5" cellspacing="0">	
						<tr>
							<td width="150px">Level Name (Role) <span class="required">*</span></td>
							<td colspan="2"><input type="text" class="textbox" id="levelTitle" name="levelTitle" value="<?=$levelTitle?>" /></td>
						</tr>
						
						<tr>
							<td>Rights</td>
							<td>
								<table cellpadding="0" cellspacing="0">
									<tr align="center">
										<td><input type="radio" onclick="return checkRights(this, 1)" <?if(getLevelFunctions($levelId, "moduleAll") == 0){?>checked<?}?> name="moduleAll" value="0" /></td>
										<td>Limited Access</td>
										<td width="20px">&nbsp;</td>
										<td><input type="radio" onclick="return checkRights(this, 0)" <?if(getLevelFunctions($levelId, "moduleAll") == 1){?>checked<?}?> name="moduleAll" value="1" /></td>
										<td>Full Access (Administrator)</td>
									</tr>
								</table>	
							</td>
						</tr>
						<tr id="trLimitedRights" <?if(getLevelFunctions($levelId, "moduleAll") == 1){?>style="display: none"<?}?>>
							<td>Define Rights</td>
							<td colspan="2" class="bordered">
								<table class="table table-bordered" width="100%" cellpadding="5" cellspacing="1">
									<thead>
									<tr class="titleTr" align="center">
										<td width="28%">Module</td>
										<td width="8%">View</td>
										<td width="8%">View All</td>
										<td width="8%">Add</td>
										<td width="8%">Edit</td>
										<td width="8%">Delete</td>	
										<td width="8%">Patients</td>
										<td width="8%">Check</td>
										<td width="8%">Update</td>									
										<td width="8%">Print</td>									
									</tr>
									</thead>
									<tbody>
									<tr>
										<td>Manage Users</td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "moduleUsersView") == 1){?>checked<?}?> type="checkbox" name="moduleUsersView" value="1" /></td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "moduleUsersAdd") == 1){?>checked<?}?> type="checkbox" name="moduleUsersAdd" value="1" /></td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "moduleUsersEdit") == 1){?>checked<?}?> type="checkbox" name="moduleUsersEdit" value="1" /></td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "moduleUsersDelete") == 1){?>checked<?}?> type="checkbox" name="moduleUsersDelete" value="1" /></td>
										<td align="center">-</td>
										<td align="center">-</td>
										<td align="center">-</td>
										<td align="center">-</td>
										<td align="center">-</td>
										
									</tr>
									<tr class="alternate">
										<td>Manage Companies</td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "moduleCompaniesView") == 1){?>checked<?}?> type="checkbox" name="moduleCompaniesView" value="1" /></td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "moduleCompaniesAdd") == 1){?>checked<?}?> type="checkbox" name="moduleCompaniesAdd" value="1" /></td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "moduleCompaniesEdit") == 1){?>checked<?}?> type="checkbox" name="moduleCompaniesEdit" value="1" /></td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "moduleCompaniesDelete") == 1){?>checked<?}?> type="checkbox" name="moduleCompaniesDelete" value="1" /></td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "moduleCompaniesViewAll") == 1){?>checked<?}?> type="checkbox" name="moduleCompaniesViewAll" value="1" /></td>
										<td align="center">-</td>
										<td align="center">-</td>
										<td align="center">-</td>
										<td align="center">-</td>
									</tr>
									<tr>
										<td>Manage Company Offices</td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "moduleOfficesView") == 1){?>checked<?}?> type="checkbox" name="moduleOfficesView" value="1" /></td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "moduleOfficesAdd") == 1){?>checked<?}?> type="checkbox" name="moduleOfficesAdd" value="1" /></td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "moduleOfficesEdit") == 1){?>checked<?}?> type="checkbox" name="moduleOfficesEdit" value="1" /></td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "moduleOfficesDelete") == 1){?>checked<?}?> type="checkbox" name="moduleOfficesDelete" value="1" /></td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "moduleOfficesViewAll") == 1){?>checked<?}?> type="checkbox" name="moduleOfficesViewAll" value="1" /></td>
										<td align="center">-</td>
										<td align="center">-</td>
										<td align="center">-</td>
										<td align="center">-</td>
									</tr>
									<tr class="alternate">
										<td>Manage Doctors</td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "moduleDoctorssView") == 1){?>checked<?}?> type="checkbox" name="moduleDoctorssView" value="1" /></td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "moduleDoctorssAdd") == 1){?>checked<?}?> type="checkbox" name="moduleDoctorssAdd" value="1" /></td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "moduleDoctorssEdit") == 1){?>checked<?}?> type="checkbox" name="moduleDoctorssEdit" value="1" /></td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "moduleDoctorssDelete") == 1){?>checked<?}?> type="checkbox" name="moduleDoctorssDelete" value="1" /></td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "moduleDoctorsViewAll") == 1){?>checked<?}?> type="checkbox" name="moduleDoctorsViewAll" value="1" /></td>
										<td align="center">-</td>
										<td align="center">-</td>
										<td align="center">-</td>
										<td align="center">-</td>
									</tr>
									<tr>
										<td>Manage Patients</td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "modulePatientsView") == 1){?>checked<?}?> type="checkbox" name="modulePatientsView" value="1" /></td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "modulePatientsViewAll") == 1){?>checked<?}?> type="checkbox" name="modulePatientsViewAll" value="1" /></td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "modulePatientsAdd") == 1){?>checked<?}?> type="checkbox" name="modulePatientsAdd" value="1" /></td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "modulePatientsEdit") == 1){?>checked<?}?> type="checkbox" name="modulePatientsEdit" value="1" /></td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "modulePatientsDelete") == 1){?>checked<?}?> type="checkbox" name="modulePatientsDelete" value="1" /></td>										
										<td align="center"><input <?if(getLevelFunctions($levelId, "modulePatientsMain") == 1){?>checked<?}?> type="checkbox" name="modulePatientsMain" value="1" /></td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "modulePatientsCheck") == 1){?>checked<?}?> type="checkbox" name="modulePatientsCheck" value="1" /></td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "modulePatientsUpdated") == 1){?>checked<?}?> type="checkbox" name="modulePatientsUpdated" value="1" /></td>
										<td align="center"><input <?if(getLevelFunctions($levelId, "modulePatientsPrinted") == 1){?>checked<?}?> type="checkbox" name="modulePatientsPrinted" value="1" /></td>
									</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="3">&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td colspan="2"><input type="submit" class="btn searchbt" name="submit" value="<?=$btnValue?>" onclick="return chkForm()" />&nbsp;<input type="button" class="btn clearbt" value="Cancel" onclick="javascript:window.location='index.php?do=<?=$do?>'" /></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
</div>

<?if($showLevelForm == 0){?>
	<div id="help">
		<div id="helpContent" style="display: none">
			<table cellpadding="3" cellspacing="0">	
				
				<tr>			
					<td><span title="Edit" class="glyphicon glyphicon-trash"></span></td>
					<td>&nbsp;</td>
					<td>Delete an existing level.</td>
				</tr>
				<tr>			
					<td><span title="Edit" class="glyphicon glyphicon-pencil"></span></td>
					<td>&nbsp;</td>
					<td>Edit existing level information.</td>
				</tr>
				<tr>			
					<td><img alt="Active" src="<?=HTTP_SERVER?>images/green.png" border="0" /></td>
					<td>&nbsp;</td>
					<td>Level's status is active.</td>
				</tr>
				<tr>			
					<td><img alt="Blocked" src="<?=HTTP_SERVER?>images/red.png" border="0" /></td>
					<td>&nbsp;</td>
					<td>Level's status is inactive / blocked.</td>
				</tr>
				
			</table>
		</div>
	</div>	
	<!--<form method="GET" action="index.php">-->
		<div class="searchcard">
			<div class="searchformcontainer">
				<form class="form-inline" method="GET" action="index.php">
				<input type="hidden" name="do" value="<?=$do?>" />
				<div class="form-group">
					<select name="filter" class="selectpicker">
						<option value="">--- select ---</option>
						<option <?if($filter == "levelTitle"){?>selected<?}?> value="levelTitle">User Level / Level</option>
						<option <?if($filter == "levelStatus"){?>selected<?}?> value="levelStatus">Status</option>
					</select>
				</div>
				<div class="form-group">
					<input type="text" name="filterText" id="filterText" class="form-control" id="text" value="<?=$filterText;?>" placeholder="Search Text">
				</div>
				<div class="form-group">
					<input type="hidden" name="userType" value="<?=$userType?>" />
					<button type="submit" class="btn searchbt">Search</button>
				</div>
				<div class="form-group">
					<button type="button" onclick="javascript: window.location='index.php?do=levels'" class="btn clearbt">Clear</button>
				</div>
				<?							
				//roles check (one can add or not)
				if($moduleAll == 1 or $moduleLevelAdd == 1){?>
				<div class="form-group">
					<button type="button" onclick="javascript: window.location='index.php?do=levels&act=add'" class="btn greenbt">Add Level</button>
				</div>
				<? } ?>
				</form>
			</div>
		</div>	
	<!--</form>					-->
	<br />	
	<table class="table table-bordered form-spacing"  align="center" width="100%" border="0" cellpadding="5" cellspacing="1">
		<thead>
		<tr>
			<td colspan="6"><h3 style="color:black;">Levels Listing</h3></td>
		</tr>
		<tr align="center">
			<td width="6%">Status</td>
			<td width="84%">User Level / Level</td>
			<td width="10%">Actions</td>
		</tr>
		</thead>
		<tbody>
		<?
		//variables
		$sqlVars = "";
		$queryString = "";
		
		//pagination code
		if((!isset($_GET['page'])) && (!isset($_POST['page']))){ 
		    $page = 1; 
		} else { 
			if (!$_GET['page']) {
				$page = $_POST['page']; 
			} else {
				$page = $_GET['page']; 
			}	  
		} 
	
		//if get filters
		if($_GET["filter"]){
			$filter = sanitize($_GET["filter"]);			
			if($filter!=""){
				$filterText = sanitize($_GET["filterText"]);
				
				//more conditions
				if($filter == "levelStatus"){
					if($filterText == strtolower("blocked")){
						$filterText=0;
					}else{
						$filterText=1;
					}
					$sqlVars .= " and $filter =".$filterText;
				}else{					
					$sqlVars .= " and $filter like '%".$filterText."%'";	
				}
				
				
				$queryString .= "&filter=".$filter."&filterText=".$filterText;
			}
			
		}
		
		
		$sqlVars .= " order by levelTitle ASC";
		
		
		$total_results = mysqli_result(mysqli_query($con, "SELECT COUNT(*) from cui_users_levels where levelId !='0' $sqlVars"),0);
		$itemCount = $total_results;
		if($itemCount>0){
			//pagination vars
			$limit = 20;
			$PaginateIt = new pagination();
			$PaginateIt->SetItemsPerPage($limit);
			$PaginateIt ->SetLinksToDisplay(5);
			$PaginateIt->SetItemCount($itemCount);
			$PaginateIt->SetLinksFormat( '<< Back', ' ', 'Next >>' );
			if($_GET["page"]){
				$pageVar = ($limit*$_GET["page"]) - $limit;
			}else{
				$pageVar = 0;
			}
			$productPageLinks = $PaginateIt->GetPageLinks();
			$counter=0;
			$sqlLevel="select * from cui_users_levels where levelId !='0' $sqlVars ".$PaginateIt->GetSqlLimit();
			$sqlLevelResult=mysqli_query($con, $sqlLevel);
			while ($sqlLevelRs=@mysqli_fetch_array($sqlLevelResult)) {
				$counter++;
				$levelId=$sqlLevelRs["levelId"];
				$levelTitle=$sqlLevelRs["levelTitle"];
				$levelDesc=stripslashes($sqlLevelRs["levelDesc"]);
				$levelStatus=$sqlLevelRs["levelStatus"];
				
				if($levelStatus == 1){
					$levelStatusImg = '<img alt="Active" src="'.HTTP_SERVER.'images/green.png" border="0" />';
				}else{
					$levelStatusImg = '<img alt="Blocked" src="'.HTTP_SERVER.'images/red.png" border="0" />';
				}
				
				if($counter>1){
					$bgcolor="lightgrey";
					$counter=0;
				}else{
					$bgcolor="#FFFFFF";
				}
				?>
				<tr class="alternatebg">
					<td align="center"><?=$levelStatusImg?></td>					
					<td><?=$levelTitle?></td>					
					<td>
						<table cellpadding="0" cellspacing="2" align="center">
							<tr>
								<?							
								//levels check (one can edit or not)
								if($moduleAll == 1 or $moduleLevelEdit == 1){?>
								<td align="left">
									<button title="Edit" type="button" class="btn btn-default btn-sm edit" onclick="window.location='index.php?do=<?=$do?>&act=edit&levelId=<?=$levelId?>'">
										<span class="glyphicon glyphicon-pencil"></span> 
									</button>
								</td>
								<td width="5px">&nbsp;</td>
								<?}?>
								<?							
								//levels check (one can delete or not)
								if($moduleAll == 1 or $moduleLevelDelete == 1){?>
								<td>
									<!--<button title="Delete" type="button" class="btn btn-default btn-sm trash" onclick="javascript:if(confirm('Proceed with deletion?')){window.location='index.php?do=<?=$do?>&act=delete&levelId=<?=$levelId?>';}">-->
									<button title="Delete" type="button" class="btn btn-default btn-sm trash" onclick="javascript:swal({title: 'Confirmation!',text: 'Proceed with deletion?',type: 'warning',showCancelButton: true,confirmButtonText: 'OK',closeOnConfirm: false},function(){swal.close();window.location='index.php?do=<?=$do?>&act=delete&levelId=<?=$levelId?>';})">
										<span class="glyphicon glyphicon-trash"></span> 
									</button>
								</td>
								<td width="5px">&nbsp;</td>
								<?}?>
								<?							
								//levels check (one can edit status or not)
								if($moduleAll == 1 or $moduleLevelEdit == 1){?>
								<td>
									<?if($levelStatus == "1"){?>
										<a title="Block" href="index.php?do=<?=$do?>&act=changeStatus&parameter=disable&levelId=<?=$levelId?>"><img src="<?=HTTP_SERVER?>images/red.png" border="0" alt="Block" /></a>
									<?}else{?>
										<a title="Activate" href="index.php?do=<?=$do?>&act=changeStatus&parameter=enable&levelId=<?=$levelId?>"><img src="<?=HTTP_SERVER?>images/green.png" alt="Activate" border="0" /></a>
									<?}?>
								</td>
								<?}?>
								<?								
								/*						
								//levels check (one can delete or not)
								if($levelsNCode == "accountexecutive" and ($moduleAll == 1 or $moduleLevelAssignSalesAgents == 1)){?>
								<td>
									<a title="Assign Agents & Specialists" href="index.php?do=<?=$do?>_levels&aeId=<?=$levelId?>"><img src="<?=HTTP_SERVER?>images/icon_assignperson.png" border="0" alt="Assign Agents & Specialists" /></a>
								</td>
								<td width="5px">&nbsp;</td>
								<?}*/?>
							</tr>
						</table>
					</td>
				</tr>
				<?
			}
				//if pagination is needed
				if($itemCount > $limit){
				?>
				<tr>
					<td colspan="6" align="center"><?echo $productPageLinks;?></td>
				</tr>
				<?
				}
			}else{
				echo "<tr>
					<td colspan=\"6\" align=\"center\" height=\"100px\">No Results Found !</td>
				</tr>";
			}
			?>
		</tbody>
	</table>
<?}?>
</div>
	