<?
//roles check
if($moduleAll == 0 and $moduleCompaniesView == 0){
	//disabled by Noman on May-09-2015
	//echo "<script>window.location='index.php?do=authorization'</\script>";
	
	//added
	$admin = 0;
}else{
	$admin = 1;
}

//checking for CUI Employree
if($_SESSION["cuiSessionUserLevel"] > 34)
{
	echo "<script>window.location='index.php?do=authorization'</script>";
}
?>
<h1 class="h1WithBg">Companies</h1>
<div id="pageContainer">
	
<?
//vars
$success = 0;
$act = "";
$showCompanyForm = 0;
$msg = "";
$sortBy = "";
$sortOrder = "";
$queryString = "";

//if get sortBy
if($_GET["sortBy"]){
	$sortBy = sanitize($_GET["sortBy"]);
}

//if get sortOrder
if($_GET["sortOrder"]){
	$sortOrder = sanitize($_GET["sortOrder"]);
}

//if get act
if($_GET["act"]){
	$act = sanitize($_GET["act"]);
}

//if get msg
if($_GET["msg"]){
	$msg = sanitize($_GET["msg"]);
}

//if act == changestatus
if($act == "changeStatus"){
	
	//roles check (one can edit or not)
	if($moduleAll == 0 and $moduleCompaniesEdit == 0){
		echo "<script>window.location='index.php?do=authorization'</script>";
	}
	
	$companyId = sanitize($_GET["companyId"]);
	$statusParameter = sanitize($_GET["parameter"]);
	if($statusParameter == "disable"){
//		mysql_query("UPDATE cui_companies set companyStatus=0 where companyId=$companyId");
		mysqli_query($con, "UPDATE cui_companies set companyStatus=0 where companyId=$companyId");
		$success = 1;
		$msg = "Success: Company has been blocked,";
	}else{
//		mysql_query("UPDATE cui_companies set companyStatus=1 where companyId=$companyId");
		mysqli_query($con, "UPDATE cui_companies set companyStatus=1 where companyId=$companyId");
		$success = 1;
		$msg = "Success: Company has been activated,";
	}
}

//if act is delete
if($act == "delete"){	
	
	//roles check (one can delete or not)
	if($moduleAll == 0 and $moduleCompaniesDelete == 0){
		echo "<script>window.location='index.php?do=authorization'</script>";
	}
	//get company id
	$companyId=sanitize($_GET["companyId"]);
	if(getCountField("cui_patients","companyId","where companyId=".$companyId)==0 && getCountField("cui_users_companies","companyId","where companyId=".$companyId)==0){
		$deleteSql = "DELETE from cui_companies_offices where companyId=$companyId";
//		mysql_query($deleteSql);
		mysqli_query($con, $deleteSql);
		$deleteSql = "DELETE from cui_companies where companyId=$companyId";
//		mysql_query($deleteSql);
		mysqli_query($con, $deleteSql);
		$success = 1;
		$msg = "Success: Company has been deleted,";	
	}else{
		$error = "Company has either offices or patients assigned to it. Delete those first!";
	}
}

//getting data
if($act == "edit"){
	
	//roles check (one can edit or not)
	if($moduleAll == 0 and $moduleCompaniesEdit == 0){
		//disabled by Noman on May-09-2015
		//echo "<script>window.location='index.php?do=authorization'</\script>";
	}
	
	$headingTitle = "Edit Company";
	$showCompanyForm = 1;
	$companyId=sanitize($_GET["companyId"]);
	$companySql="select * from cui_companies where companyId=$companyId";
//	$companyResult=mysql_query($companySql);
	$companyResult=mysqli_query($con, $companySql);
//	if(mysql_num_rows($companyResult)){
	if(@mysqli_num_rows($companyResult)){
//		$companyCode=mysql_result($companyResult,0,"companyCode");
		$companyCode=mysqli_result($companyResult,0,"companyCode");
//		$companyCity=mysql_result($companyResult,0,"companyCity");
		$companyCity=mysqli_result($companyResult,0,"companyCity");
//		$companyState=mysql_result($companyResult,0,"companyState");
		$companyState=mysqli_result($companyResult,0,"companyState");
//		$companyCountry=mysql_result($companyResult,0,"companyCountry");
		$companyCountry=mysqli_result($companyResult,0,"companyCountry");

		if($companyCountry != "United States"){
			$companyStateAlternate = $companyState;
		}
		
//		$companyName=mysql_result($companyResult,0,"companyName");
		$companyName=mysqli_result($companyResult,0,"companyName");
//		$companyZip=mysql_result($companyResult,0,"companyZip");
		$companyZip=mysqli_result($companyResult,0,"companyZip");
//		$companyCustom=mysql_result($companyResult,0,"companyCustom");
		$companyCustom=mysqli_result($companyResult,0,"companyCustom");
//		$companyCompanyName=mysql_result($companyResult,0,"companyCompanyName");
		$companyCompanyName=mysqli_result($companyResult,0,"companyCompanyName");
//		$companyTaxID=mysql_result($companyResult,0,"companyTaxID");
		$companyTaxID=mysqli_result($companyResult,0,"companyTaxID");
//		$companyNPI=mysql_result($companyResult,0,"companyNPI");
		$companyNPI=mysqli_result($companyResult,0,"companyNPI");
//		$companyStateLicense=mysql_result($companyResult,0,"companyStateLicense");
		$companyStateLicense=mysqli_result($companyResult,0,"companyStateLicense");
//		$companyCounty=mysql_result($companyResult,0,"companyCounty");
		$companyCounty=mysqli_result($companyResult,0,"companyCounty");
//		$companyHMOPPO=mysql_result($companyResult,0,"companyHMOPPO");
		$companyHMOPPO=mysqli_result($companyResult,0,"companyHMOPPO");
//		$companyDoctorsName=mysql_result($companyResult,0,"companyDoctorsName");
		$companyDoctorsName=mysqli_result($companyResult,0,"companyDoctorsName");
//		$companyNotes=mysql_result($companyResult,0,"companyNotes");
		$companyNotes=mysqli_result($companyResult,0,"companyNotes");
//		$companyFileName=mysql_result($companyResult,0,"companyFileName");
		$companyFileName=mysqli_result($companyResult,0,"companyFileName");
//		$companyFile=mysql_result($companyResult,0,"companyFile");
		$companyFile=mysqli_result($companyResult,0,"companyFile");
//		$companyFileName2=mysql_result($companyResult,0,"companyFileName2");
		$companyFileName2=mysqli_result($companyResult,0,"companyFileName2");
//		$companyFile2=mysql_result($companyResult,0,"companyFile2");
		$companyFile2=mysqli_result($companyResult,0,"companyFile2");
//		$companyFileName3=mysql_result($companyResult,0,"companyFileName3");
		$companyFileName3=mysqli_result($companyResult,0,"companyFileName3");
//		$companyFile3=mysql_result($companyResult,0,"companyFile3");
		$companyFile3=mysqli_result($companyResult,0,"companyFile3");
//		$companyRecipientEmails=mysql_result($companyResult,0,"companyRecipientEmails");
		$companyRecipientEmails=mysqli_result($companyResult,0,"companyRecipientEmails");
		$companyShowOnReport=mysqli_result($companyResult,0,"companyShowOnReport");
		$btnValue = "Update";
	}
}else{		
	$companyCode="";
	$companyCity="";
	$companyState="";
	$companyCountry="United States";
	$companyZip="";
	$companyCustom="";
	$companyCompanyName="";
	$companyTaxID="";
	$companyNPI="";
	$companyStateLicense="";
	$companyCounty="";
	$companyHMOPPO="";
	$companyDoctorsName="";
	$companyNotes="";
	$companyFileName="";
	$companyFile="";
	$companyFileName2="";
	$companyFile2="";
	$companyFileName3="";
	$companyFile3="";
	$companyRecipientEmails="";
	$companyShowOnReport="";
	$btnValue = "Add";
}

//check for state tr alternate
if($companyCountry == "United States"){
	$stateTrAlternate = 'style="display:none"';
	$stateTr = "";
}else{
	$stateTr = 'style="display:none"';
	$stateTrAlternate = "";
}

if($act == "add"){
	
	//roles check (one can add or not)
	if($moduleAll == 0 and $moduleCompaniesAdd == 0){
		echo "<script>window.location='index.php?do=authorization'</script>";
	}
	
	$headingTitle = "Add New Company";
	$showCompanyForm = 1;
}


//saving information
if($_POST["submit"]){
	$showCompanyForm = 1;
	$companyName = sanitize($_POST["companyName"]);
	$companyCountry = sanitize($_POST["companyCountry"]);
	$companyCode = sanitize($_POST["companyCode"]);
	$companyCity = sanitize($_POST["companyCity"]);	
	$companyZip = sanitize($_POST["companyZip"]);
	$companyState = sanitize($_POST["companyState"]);
	$companyStateAlternate = sanitize($_POST["companyStateAlternate"]);
	$companyCustom = sanitize($_POST["companyCustom"]);
	if($companyCustom=="1")
	{
		$companyCompanyName = sanitize($_POST["companyCompanyName"]);
	}else{
		$companyCompanyName = "";
	}
	
	$companyTaxID = sanitize($_POST["companyTaxID"]);
	$companyNPI = sanitize($_POST["companyNPI"]);
	$companyStateLicense = sanitize($_POST["companyStateLicense"]);
	$companyCounty = sanitize($_POST["companyCounty"]);
	$companyHMOPPO = sanitize($_POST["companyHMOPPO"]);
	$companyDoctorsName = sanitize($_POST["companyDoctorsName"]);
	$companyNotes = sanitize($_POST["companyNotes"]);
	$companyFileName = sanitize($_POST["companyFileName"]);
	$companyFile = $_FILES["companyFile"]["name"];
	$companyFileName2 = sanitize($_POST["companyFileName2"]);
	$companyFile2 = $_FILES["companyFile2"]["name"];
	$companyFileName3 = sanitize($_POST["companyFileName3"]);
	$companyFile3 = $_FILES["companyFile3"]["name"];
	$companyRecipientEmails = sanitize($_POST["companyRecipientEmails"]);
	$companyShowOnReport = sanitize($_POST["companyShowOnReport"]);
	
	//uploading file
	if($companyFile != "")
	{
		$targetDir = "files/companies/";
		$targetFile = $targetDir . basename($companyFile);
		$fileType = pathinfo($targetFile,PATHINFO_EXTENSION);
		move_uploaded_file($_FILES["companyFile"]["tmp_name"], $targetFile);
	}else{
		$companyFile = "";
	}
	//
	if($companyFile2 != "")
	{
		$targetDir = "files/companies/";
		$targetFile = $targetDir . basename($companyFile2);
		$fileType = pathinfo($targetFile,PATHINFO_EXTENSION);
		move_uploaded_file($_FILES["companyFile2"]["tmp_name"], $targetFile);
	}else{
		$companyFile2 = "";
	}
	//
	if($companyFile3 != "")
	{
		$targetDir = "files/companies/";
		$targetFile = $targetDir . basename($companyFile3);
		$fileType = pathinfo($targetFile,PATHINFO_EXTENSION);
		move_uploaded_file($_FILES["companyFile3"]["tmp_name"], $targetFile);
	}else{
		$companyFile3 = "";
	}
	
	//if US
	if($companyCountry != "United States"){
		$companyState = $companyStateAlternate;
	}
	
	if($act == "add"){
		$sqlCheck = "SELECT * FROM cui_companies where companyName = '$companyName' or companyCode='$companyCode'";
//		$sqlResult = mysql_query($sqlCheck);
		$sqlResult = mysqli_query($con, $sqlCheck);

//		if(mysql_num_rows($sqlResult)<1){
		if(@mysqli_num_rows($sqlResult)<1){

			$sqlCompany="INSERT INTO cui_companies (companyCode, companyName, companyState, companyCountry, companyCity, companyZip, companyCustom, companyCompanyName, companyTaxID, companyNPI, companyStateLicense, companyCounty, companyHMOPPO, companyDoctorsName, companyNotes, companyFileName, companyFile, companyFileName2, companyFile2, companyFileName3, companyFile3, companyRecipientEmails, companyShowOnReport) values ('$companyCode', '$companyName', '$companyState', '$companyCountry', '$companyCity', '$companyZip', '$companyCustom', '$companyCompanyName', '$companyTaxID', '$companyNPI', '$companyStateLicense', '$companyCounty', '$companyHMOPPO', '$companyDoctorsName', '$companyNotes', '$companyFileName', '$companyFile', '$companyFileName2', '$companyFile2', '$companyFileName3', '$companyFile3', '$companyRecipientEmails', '$companyShowOnReport')";
			
//			mysql_query($sqlCompany);
			mysqli_query($con, $sqlCompany);

			//if auto assign
			if($_POST["companyAutoId"]){
				//get last inserted id
//				$companyId = mysql_insert_id();
				$companyId = mysqli_insert_id($con);
				//add companyId
//				mysql_query("UPDATE cui_companies SET companyCode='CUI$companyId' WHERE companyId=$companyId");
				mysqli_query($con, "UPDATE cui_companies SET companyCode='CUI$companyId' WHERE companyId=$companyId");
			}
			
			$success=1;		
			$msg = "Success: Company has been added.";
			echo "<script>window.location='".HTTP_SERVER."index.php?do=companies&msg=$msg'</script>";
		}else{
			$error = "The company details you provided match another company in the database.";
		}
	}else{
		$tmpFilesVars = "companyFileName='$companyFileName',companyFileName2='$companyFileName2',companyFileName3='$companyFileName3',";
		if($companyFile != "")
		{
			$tmpFilesVars .= "companyFile='$companyFile',";
		}
		if($companyFile2 != "")
		{
			$tmpFilesVars .= "companyFile2='$companyFile2',";
		}
		if($companyFile3 != "")
		{
			$tmpFilesVars .= "companyFile3='$companyFile3',";
		}
		$sqlCompany="update cui_companies set companyName = '$companyName', companyState='$companyState', companyCountry='$companyCountry', companyCode='$companyCode', companyCity='$companyCity', companyZip='$companyZip', companyCustom='$companyCustom', companyCompanyName='$companyCompanyName', companyTaxID='$companyTaxID', companyNPI='$companyNPI', companyStateLicense='$companyStateLicense', companyCounty='$companyCounty', companyHMOPPO='$companyHMOPPO', companyDoctorsName='$companyDoctorsName', companyNotes='$companyNotes', $tmpFilesVars companyRecipientEmails='$companyRecipientEmails', companyShowOnReport='$companyShowOnReport' where companyId=$companyId";
//		mysql_query($sqlCompany);
		mysqli_query($con, $sqlCompany);

		$success=1;
		$msg = "Success: Company details have been updated.";
		echo "<script>window.location='".HTTP_SERVER."index.php?do=companies&msg=$msg'</script>";
	}
	
}

//filters
if($_GET["filter"]){
	$filter = sanitize($_GET["filter"]);
	$filterText = sanitize($_GET["filterText"]);
	$queryString .= "&filter=$filter&filterText=$filterText";
}else{
	$filterText = "";
	$filter = "";
}

?>

<?if($msg!=""){?>
	<div class="success"><?=$msg?></div>
<?}?>

<script type="text/javascript" language="JavaScript">
	function chkForm(){
		var a=document.getElementById("companyName").value;
		var b=document.getElementById("companyState").value;
		var c=document.getElementById("companyZip").value;
		var d=document.getElementById("companyCity").value;
		
		if(a=="" || b=="" || c=="" || d==""){
			//alert("Company Name, State, City and Zip are required !");
			swal({
				title: 'Error!',
				text: 'Company Name, State, City and Zip are required !',
				type: "error",
				showCancelButton: false,
				confirmButtonText: 'OK',
				closeOnConfirm: true
			});
			return false;
		}
		
	}
	
	function companyCodeFunction(checkBoxId){
		if(document.getElementById(checkBoxId).checked){
			setReadOnly('disable', 'companyCode');
		}else{
			setReadOnly('enable', 'companyCode');
		}
	}
	
</script>

<?
if($admin==1)
{
	$desabledCond = 'class="textbox"';
}else{
	$desabledCond = 'class="textboxDisabled" disabled';
}
?>
<style>
		hr {
			border-bottom: 0px;
		}
		table.form-spacing tbody tr td {
			padding-bottom: 9px;
		}
		</style>
<div id="addCompany" <?if($showCompanyForm == 0){?>style="display:none"<?}?>>
	<form method="POST" enctype="multipart/form-data">		
		<table class="form-spacing" align="center" width="100%" border="0" cellpadding="5" cellspacing="1">
			<tr class="titleTr">
				<td><h3 style="padding-top: 9px; padding-left: 5px;"><?=$headingTitle?></h3></td>
			</tr>
			<tr>
				<td>
					<?if($error != ""){?>
						<div class="error"><?=$error?></div>		
					<?}else{?>
						<div class="message">The required fields are marked with <span class="required">*</span></div>
					<?}?>
				</td>
			</tr>
			<tr>
				<td>
					<table width="50%" border="0" align="center" cellpadding="5" cellspacing="0">
						<tr>
							<td>Company Id <span class="required">*</span></td>
							<td><input readonly class="textboxDisabled" type="text" id="companyCode" name="companyCode" value="<?=$companyCode?>" /></td>
							<?
							if($act!="edit"){
							?>
							
							<td>
								<table cellpadding="1" cellspacing="0">
									<tr>
										<td><input type="checkbox" id="companyAutoId" name="companyAutoId" checked value="1" onclick="return companyCodeFunction(this.id)" /></td>
										<td>Auto assign company id</td>
									</tr>
								</table>
							</td>
							
							<?}?>
							
						</tr>
						<tr>
							<td>Company Name <span class="required">*</span></td>
							<td><input type="text" <?=$desabledCond?> id="companyName" name="companyName" value="<?=$companyName?>" /></td>
						</tr>
						<tr>
							<td>Country <span class="required">*</span></td>
							<td colspan="2">
								<select name="companyCountry" id="companyCountry" onchange="javascript: hideUsStates(this.value,'stateTr','stateTrAlternate')" <?=$desabledCond?> <? if($admin==0){ ?>style="background-color:#ccc;"<? } ?>>
									<?
									$countrySql = "SELECT * from cui_countries order by countryName ASC";
//									$countryResult = mysql_query($countrySql);
									$countryResult = mysqli_query($con, $countrySql);
//									while ($countryRs = mysql_fetch_array($countryResult)) {
									while ($countryRs = @mysqli_fetch_array($countryResult)) {

										$getCountryName = $countryRs["countryName"];
										
										if($companyCountry == $getCountryName){
											$sel = "selected";
										}else{
											$sel = "";
										}
										
										echo "<option $sel value=\"$getCountryName\">$getCountryName</option>";
									}
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td>City <span class="required">*</span></td>
							<td><input type="text" <?=$desabledCond?> id="companyCity" name="companyCity" value="<?=$companyCity?>" /></td>
						</tr>
						
						<tr id="stateTr" <?=$stateTr?>>
							<td>State <span class="required">*</span></td>
							<td colspan="2">
								<select name="companyState" id="companyState" <?=$desabledCond?> <? if($admin==0){ ?>style="background-color:#ccc;"<? } ?>>
									<?
									$stateSql = "SELECT * from cui_states order by stateName ASC";
//									$stateResult = mysql_query($stateSql);
									$stateResult = mysqli_query($con, $stateSql);
//									while ($stateRs = mysql_fetch_array($stateResult)) {
									while ($stateRs = @mysqli_fetch_array($stateResult)) {

										$getStateName = $stateRs["stateName"];
										
										if($companyState == $getStateName){
											$sel = "selected";
										}else{
											$sel = "";
										}
										
										echo "<option $sel value=\"$getStateName\">$getStateName</option>";
									}
									?>
								</select>
							</td>
						</tr>
						
						<tr id="stateTrAlternate" <?=$stateTrAlternate?>>
							<td>State / Province <span class="required">*</span></td>
							<td><input type="text" name="companyStateAlternate" id="companyStateAlternate" value="<?=$companyStateAlternate?>" <?=$desabledCond?> /></td>
						</tr>
						
						<tr>
							<td>Zip Code <span class="required">*</span></td>
							<td><input type="text" <?=$desabledCond?> id="companyZip" name="companyZip" size="10" value="<?=$companyZip?>" /></td>
						</tr>
						
                        <tr>
							<td>Custom <span class="required">*</span></td>
							<td colspan="2">
                            	<table>
                                <tr>
                                    <td style="height:25px;"><input type="radio" id="companyCustom" name="companyCustom" value="0" checked="checked" onclick="document.getElementById('companyCompanyName').style.display='none'" <?=$desabledCond?> />No &nbsp; <input type="radio" id="companyCustom" name="companyCustom" value="1" <?php if($companyCustom == "1"){ ?>checked="checked"<?php } ?> onclick="document.getElementById('companyCompanyName').style.display=''" <?=$desabledCond?> />Yes</td>
                                    <td>&nbsp;</td>
                                    <td>
                                    	<select name="companyCompanyName" id="companyCompanyName" size="1" <?php if($companyCustom != "1"){ ?>style="display:none;"<?php } ?> <?=$desabledCond?>>
                                        	<option value="">-- Select --</option>
                                            <?
											$compSql = "SELECT * from cui_custom_companies where custCompanyStatus='1' order by custCompanyName ASC";
//											$compResult = mysql_query($compSql);
											$compResult = mysqli_query($con, $compSql);
//											while ($compRs = mysql_fetch_array($compResult)) {
											while ($compRs = @mysqli_fetch_array($compResult)) {

												$getCompName = $compRs["custCompanyName"];
												
												if($companyName == $getCompName){
													$sel = "selected";
												}else{
													$sel = "";
												}
												
												echo "<option $sel value=\"$getCompName\">$getCompName</option>";
											}
											?>
                                        </select>
                                    </td>
                                </tr>
                                </table>
                            </td>
						</tr>
                        
                        <tr>
							<td>Tax ID</td>
							<td colspan="2"><input type="text" name="companyTaxID" id="companyTaxID" value="<?=$companyTaxID?>" <?=$desabledCond?> /></td>
                        </tr>
                        <tr>
							<td>NPI#</td>
							<td colspan="2"><input type="text" name="companyNPI" id="companyNPI" value="<?=$companyNPI?>" <?=$desabledCond?> /></td>
                        </tr>
                        <tr>
							<td>State License#</td>
							<td colspan="2"><input type="text" name="companyStateLicense" id="companyStateLicense" value="<?=$companyStateLicense?>" <?=$desabledCond?> /></td>
                        </tr>
                        <tr>
							<td>County</td>
							<td colspan="2"><input type="text" name="companyCounty" id="companyCounty" value="<?=$companyCounty?>" <?=$desabledCond?> /></td>
                        </tr>
                        <tr>
							<td nowrap="nowrap">HMO or PPO Provider</td>
							<td colspan="2"><input type="text" name="companyHMOPPO" id="companyHMOPPO" value="<?=$companyHMOPPO?>" <?=$desabledCond?> /></td>
                        </tr>
                        <tr>
							<td>Doctors Name</td>
							<td colspan="2"><input type="text" name="companyDoctorsName" id="companyDoctorsName" value="<?=$companyDoctorsName?>" <?=$desabledCond?> /></td>
                        </tr>
                        <tr>
							<td>Notes</td>
							<td colspan="2"><textarea cols="60" rows="4" name="companyNotes" id="companyNotes" <?=$desabledCond?> <? if($admin==0){ ?>style="background-color:#ccc;"<? } ?>><?=$companyNotes?></textarea></td>
                        </tr>
                        <tr>
							<td>File 1</td>
							<td colspan="2" ><input type="text" name="companyFileName" id="companyFileName" value="<?=$companyFileName?>" <?=$desabledCond?> /> <input type="file" name="companyFile" id="companyFile" <?=$desabledCond?> /> <? if($companyFile != ""){ ?>(<a href="#" onclick="window.open('<?=HTTP_SERVER."generate_download.php?path=companies&filename=".$companyFile?>', '_blank'); win.focus();">Viwe/Download File</a>)<? } ?></td>
                        </tr>
                        <tr>
							<td>File 2</td>
							<td colspan="2" ><input type="text" name="companyFileName2" id="companyFileName2" value="<?=$companyFileName2?>" <?=$desabledCond?> /> <input type="file" name="companyFile2" id="companyFile2" <?=$desabledCond?> /> <? if($companyFile2 != ""){ ?>(<a href="#" onclick="window.open('<?=HTTP_SERVER."generate_download.php?path=companies&filename=".$companyFile2?>', '_blank'); win.focus();">Viwe/Download File</a>)<? } ?></td>
                        </tr>
                        <tr>
							<td>File 3</td>
							<td colspan="2" ><input type="text" name="companyFileName3" id="companyFileName3" value="<?=$companyFileName3?>" <?=$desabledCond?> /> <input type="file" name="companyFile3" id="companyFile3" <?=$desabledCond?> /> <? if($companyFile3 != ""){ ?>(<a href="#" onclick="window.open('<?=HTTP_SERVER."generate_download.php?path=companies&filename=".$companyFile3?>', '_blank'); win.focus();">Viwe/Download File</a>)<? } ?></td>
                        </tr>
                        
                        <? if($admin == 1){ ?>
                        <tr>
							<td>Recipient Emails</td>
							<td colspan="2"><input type="text" name="companyRecipientEmails" id="companyRecipientEmails" size="75" value="<?=$companyRecipientEmails?>" <?=$desabledCond?> /> <span style="font-size:11px">(Enter comma(,) separated email address)</span></td>
                        </tr>
                        <? } ?>
                        
						<tr>
							<td>Show on Report</td>
							<td colspan="2">
								<input type="radio" id="companyShowOnReport" name="companyShowOnReport" value="0" <?php if($companyShowOnReport == "0"){ ?>checked="checked"<?php } ?>  />No &nbsp; 
								<input type="radio" id="companyShowOnReport" name="companyShowOnReport" value="1" <?php if($companyShowOnReport == "1"){ ?>checked="checked"<?php } ?> />Yes
							</td>
                        </tr>
						
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
                        
						<? if($moduleAll == 0 and $moduleCompaniesEdit == 0){ ?>
                        	<? //nothing ?>
                        <? }else{ ?>
                        <tr>
							<td>&nbsp;</td>
							<td><input type="submit" class="btn searchbt" name="submit" value="<?=$btnValue?>" onclick="return chkForm()" />&nbsp;<input type="button" class="btn clearbt" value="Cancel" onclick="javascript:window.location='index.php?do=<?=$do?>'" /></td>
						</tr>
                        <? } ?>
					</table>
				</td>
			</tr>
		</table>
	</form>
</div>

<?if($showCompanyForm == 0){?>
	<?if($error != ""){?>
		<div class="error"><?=$error?></div>
	<?}?>
	<div id="help">
		<div id="helpContent" style="display: none">
			<table cellpadding="3" cellspacing="0">	
				<tr>			
					<td><span title="Delete" class="glyphicon glyphicon-trash"></span></td>
					<td>&nbsp;</td>
					<td>Delete an existing company.</td>
				</tr>
				<tr>			
					<td><span title="Edit" class="glyphicon glyphicon-pencil"></span></td>
					<td>&nbsp;</td>
					<td>Edit existing company information.</td>
				</tr>
				<tr>			
					<td><img alt="Active" src="<?=HTTP_SERVER?>images/status_active.png" border="0" /></td>
					<td>&nbsp;</td>
					<td>Company's status is active.</td>
				</tr>
				<tr>			
					<td><img alt="Blocked" src="<?=HTTP_SERVER?>images/status_inactive.png" border="0" /></td>
					<td>&nbsp;</td>
					<td>Company's status is inactive / blocked.</td>
				</tr>
			</table>
		</div>
	</div>	
	<!--<form method="GET" action="index.php">-->
		<div class="searchcard">
			<div class="searchformcontainer">
				<form class="form-inline" method="GET" action="index.php">
				<input type="hidden" name="do" value="<?=$do?>" />
				<div class="form-group">
					<select name="filter" class="selectpicker">
						<option value="">--- select ---</option>
						<option <?if($filter == "companyCode"){?>selected<?}?> value="companyCode">Company Id</option>
						<option <?if($filter == "companyName"){?>selected<?}?> value="companyName">Company Name</option>
						<option <?if($filter == "companyState"){?>selected<?}?> value="companyState">Client</option>
					</select>
				</div>
				<div class="form-group">
					<input type="text" name="filterText" id="filterText" class="form-control" id="text" value="<?=$filterText;?>" placeholder="Search Text">
				</div>
				<div class="form-group">
					<input type="hidden" name="userType" value="<?=$userType?>" />
					<button type="submit" class="btn searchbt">Search</button>
				</div>
				<div class="form-group">
					<button type="button" onclick="javascript: window.location='index.php?do=companies'" class="btn clearbt">Clear</button>
				</div>
				<?							
				//roles check (one can add or not)
				if($moduleAll == 1 or $moduleCompaniesAdd == 1){?>
				<div class="form-group">
					<button type="button" onclick="javascript: window.location='index.php?do=companies&act=add'" class="btn greenbt">Add Company</button>
				</div>
				<? } ?>
				</form>
			</div>
		</div>		
	<!--</form>-->
	<br />	
	<table class="table table-bordered form-spacing" align="center" width="100%" border="0" cellpadding="5" cellspacing="1">
		<thead>
		<tr>
			<td colspan="8"><h3 style="color:black;">Company Listing</h3></td>
		</tr>
		<tr align="center">
			<td width="5%"><a href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=companyStatus&sortOrder=<?if($sortBy == "companyStatus"){if($sortOrder == "ASC"){echo "DESC";}else{echo "ASC";}}else{echo "ASC";}?>">Status<?if($sortBy == "companyStatus"){?>&nbsp;<img src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0" alt="<?=$sortOrder?> Order" /><?}?></a></td>
			<td width="10%"><a href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=companyCode&sortOrder=<?if($sortBy == "companyCode"){if($sortOrder == "ASC"){echo "DESC";}else{echo "ASC";}}else{echo "ASC";}?>">Company Id<?if($sortBy == "companyCode"){?>&nbsp;<img src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0" alt="<?=$sortOrder?> Order" /><?}?></a></td>
			<td width="27%"><a href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=companyName&sortOrder=<?if($sortBy == "companyName"){if($sortOrder == "ASC"){echo "DESC";}else{echo "ASC";}}else{echo "ASC";}?>">Company Name<?if($sortBy == "companyName"){?>&nbsp;<img src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0" alt="<?=$sortOrder?> Order" /><?}?></a></td>
			<td width="19%"><a href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=companyCountry&sortOrder=<?if($sortBy == "companyCountry"){if($sortOrder == "ASC"){echo "DESC";}else{echo "ASC";}}else{echo "ASC";}?>">Country<?if($sortBy == "companyCountry"){?>&nbsp;<img src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0" alt="<?=$sortOrder?> Order" /><?}?></a></td>
			<td width="14%"><a href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=companyState&sortOrder=<?if($sortBy == "companyState"){if($sortOrder == "ASC"){echo "DESC";}else{echo "ASC";}}else{echo "ASC";}?>">State<?if($sortBy == "companyState"){?>&nbsp;<img src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0" alt="<?=$sortOrder?> Order" /><?}?></a></td>
			<td width="12%"><a href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=companyZip&sortOrder=<?if($sortBy == "companyZip"){if($sortOrder == "ASC"){echo "DESC";}else{echo "ASC";}}else{echo "ASC";}?>">Zip<?if($sortBy == "companyZip"){?>&nbsp;<img src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0" alt="<?=$sortOrder?> Order" /><?}?></a></td>
			<td width="13%">Actions</td>
		</tr>		
		</thead>
		<tbody>
		<?		
		//variables
		$sqlVars = "";
		$queryString = "";
		
		//pagination code
		if((!isset($_GET['page'])) && (!isset($_POST['page']))){ 
		    $page = 1; 
		} else { 
			if (!$_GET['page']) {
				$page = $_POST['page']; 
			} else {
				$page = $_GET['page']; 
			}	  
		} 
	
		//if get filters
		if($_GET["filter"]){
			$filter = sanitize($_GET["filter"]);			
			if($filter!=""){
				$filterText = sanitize($_GET["filterText"]);
				
				//more conditions
				if($filter == "companyStatus"){
					if($filterText == strtolower("blocked") or $filterText == strtolower("inactive") or $filterText == strtolower("disabled")){
						$filterText=0;
					}else{
						$filterText=1;
					}
					$sqlVars .= " and $filter =".$filterText;
				}else{
					$sqlVars .= " and $filter like '%".$filterText."%'";	
				}
				
				
				$queryString .= "&filter=".$filter."&filterText=".$filterText;
			}
			
		}
		
		//
		if($admin == 0)
		{
			$tmpUserId = $_SESSION[$sessionUserID];
//			$sqlComp = mysql_query("Select * from cui_users_companies where userId='$tmpUserId'");
			$sqlComp = mysqli_query($con, "Select * from cui_users_companies where userId='$tmpUserId'");
			$counter = 0;
			$sqlCond = "";
//			while($row = mysql_fetch_array($sqlComp))
			while($row = @mysqli_fetch_array($sqlComp))
			{
				$counter++;
				
				if($counter>1)
				{
					$comma = ",";
				}else{
					$comma = "";
				}
				
				$sqlCond .= $comma.$row["companyId"];
			}
			$sqlVars2 = " and companyId in ($sqlCond) ";
			
		}else{
			$sqlVars2 = "";
		}
		
		//if sort by is there
		if($sortBy!=""){
			$sqlVars .= " $sqlVars2 order by $sortBy $sortOrder";
		}else{
			$sqlVars .= " $sqlVars2 order by companyName ASC";
		}
		
		$total_results = mysqli_result(mysqli_query($con, "SELECT COUNT(*) from cui_companies where companyId !=0 $sqlVars"),0);
		$itemCount = $total_results;
		
		
		if($itemCount>0){
			//pagination vars
			$limit = 20;
			$PaginateIt = new pagination();
			$PaginateIt->SetItemsPerPage($limit);
			$PaginateIt ->SetLinksToDisplay(5);
			$PaginateIt->SetItemCount($itemCount);
			$PaginateIt->SetLinksFormat( '<< Back', ' ', 'Next >>' );
			if($_GET["page"]){
				$pageVar = ($limit*$_GET["page"]) - $limit;
			}else{
				$pageVar = 0;
			}
			$productPageLinks = $PaginateIt->GetPageLinks();
			$counter=0;
			$sqlCompanies="select * from cui_companies where companyId !=0 $sqlVars ".$PaginateIt->GetSqlLimit();
		//		$sqlCompaniesResult=mysql_query($sqlCompanies);
			$sqlCompaniesResult=mysqli_query($con, $sqlCompanies);
			while ($sqlCompaniesRs=@mysqli_fetch_array($sqlCompaniesResult)) {
				$counter++;
				$companyId=$sqlCompaniesRs["companyId"];
				$companyCode=$sqlCompaniesRs["companyCode"];
				$companyName=$sqlCompaniesRs["companyName"];
				$companyState=$sqlCompaniesRs["companyState"];
				$companyCountry=$sqlCompaniesRs["companyCountry"];
				$companyZip=$sqlCompaniesRs["companyZip"];
				$companyCity=$sqlCompaniesRs["companyCity"];
				$companyStatus=$sqlCompaniesRs["companyStatus"];
				
				//this will insert jobsview right for all employees
				//tempInsert($companyId, "moduleJobsView");
			
				if($companyStatus == 1){
					$companyStatusImg = '<img alt="Active" src="'.HTTP_SERVER.'images/green.png" border="0" />';
				}else{
					$companyStatusImg = '<img alt="Blocked" src="'.HTTP_SERVER.'images/red.png" border="0" />';
				}
				
				if($counter>1){
					$bgcolor="lightgrey";
					$counter=0;
				}elsE{
					$bgcolor="#FFFFFF";
				}
				?>
				<tr class="alternatebg">
					<td align="center"><?=$companyStatusImg?></td>
					<td><?=$companyCode?></td>
					<td><?=$companyName?></td>
					<td align="center"><?=$companyCountry?></td>
					<td align="center"><?=$companyState?></td>	
					<td align="center"><?=$companyZip?></td>	
					<td>
						<table cellpadding="0" cellspacing="2">
							<tr>
								<?							
								//roles check (one can edit or not)
								//disabled by Noman on May-09-2015
								//if($moduleAll == 1 or $moduleCompaniesEdit == 1){?>
								<td align="left">
									<button title="Edit" type="button" class="btn btn-default btn-sm edit" onclick="window.location='index.php?do=<?=$do?>&act=edit&companyId=<?=$companyId?>'">
										<span class="glyphicon glyphicon-pencil"></span> 
									</button>
								</td>
								<td width="5px">&nbsp;</td>
								<? //} ?>
								<?							
								//roles check (one can delete or not)
								if($moduleAll == 1 or $moduleCompaniesDelete == 1){?>
								
									<td>
										<!--<button title="Delete" type="button" class="btn btn-default btn-sm trash" onclick="javascript:if(confirm('Proceed with deletion?')){window.location='index.php?do=<?=$do?>&act=delete&companyId=<?=$companyId?>';}">-->
										<button title="Delete" type="button" class="btn btn-default btn-sm trash" onclick="javascript:swal({title: 'Confirmation!',text: 'Proceed with deletion?',type: 'warning',showCancelButton: true,confirmButtonText: 'OK',closeOnConfirm: false},function(){swal.close();window.location='index.php?do=<?=$do?>&act=delete&companyId=<?=$companyId?>';})">
											<span class="glyphicon glyphicon-trash"></span> 
										</button>
									</td>
									<td width="5px">&nbsp;</td>
								
								<?}?>
								<?							
								//roles check (one can edit status or not)
								if($moduleAll == 1 or $moduleCompaniesEdit == 1){?>
								<td>
									<?if($companyStatus == "1"){?>
										<a title="Block" href="index.php?do=<?=$do?>&act=changeStatus&parameter=disable&companyId=<?=$companyId?>"><img src="<?=HTTP_SERVER?>images/red.png" border="0" alt="Block" /></a>
									<?}else{?>
										<a title="Activate" href="index.php?do=<?=$do?>&act=changeStatus&parameter=enable&companyId=<?=$companyId?>"><img src="<?=HTTP_SERVER?>images/green.png" alt="Activate" border="0" /></a>
									<?}?>
								</td>
								<td width="5px">&nbsp;</td>
								<td>				
									<button title="Offices" type="button" class="btn btn-default btn-sm " onclick="window.location='index.php?do=<?=$do?>_offices&companyId=<?=$companyId?>'">
										<span class="glyphicon glyphicon-search"></span>
									</button>
								</td>
								<td width="5px">&nbsp;</td>
								<td>
									<button title="Offices" type="button" class="btn btn-default btn-sm " onclick="window.location='index.php?do=users_companies&companyId=<?=$companyId?>&assignType=users'">
										<span class="glyphicon glyphicon-user"></span>
									</button>
								</td>
								<?}?>
							</tr>
						</table>
					</td>
				</tr>
				<?
			}
			//if pagination is needed
			if($itemCount > $limit){
			?>
			<tr>
				<td colspan="8" align="center"><?echo $productPageLinks;?></td>
			</tr>
			<?
			}
		}else{
			echo "<tr>
				<td colspan=\"8\" align=\"center\" height=\"100px\">No Results Found !</td>
			</tr>";
		}
		?>
		</tbody>
	</table>
<?}?>
</div>
	