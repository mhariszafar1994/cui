<?php
//top file
require("includes/application_top.php");

//if not ssl redirect
if(SSL_ENABLE == 1){
	if($_SERVER['SERVER_PORT'] != 443 and $_SERVER['HTTP_HOST'] != 'localhost'){
		//redirect var
		$redirect = HTTP_SERVER.'index.php?'.$_SERVER['QUERY_STRING'];
		//redirect script
		echo "<script>window.location='$redirect'</script>";
		
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Dental insurance Verification, Validate Insurance, Insurance Verification Companies, Affordable Dental Insurance- checkurinsurance.com</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="keywords" content="dental insurance verification, validate insurance, insurance verification companies, affordable dental insurance, dental, dentist, insurance verification, dental coverage, patient eligibility, verifying dental benefits, validate insurance, california dental insurance, dental insurance verification form, cheap dentist, insurance verification forms, patient eligibility verification, Insurance Verification Program" />
		
<meta name="description" content="Insurance verification services at checkurinsurance.com. Our representatives will take care of verifying insurance. Check ur Dental insurance, affordable dental coverage insurance, Insurance Verification Program, verify patient eligibility, dental insurance companies" />

<link href="<?=HTTP_SERVER?>css/style.css" rel="stylesheet" type="text/css" />
<link href="<?=HTTP_SERVER?>css/thickbox.css" rel="stylesheet" type="text/css" />
<link href="<?=HTTP_SERVER?>css/datePicker.css" rel="stylesheet" type="text/css" />
<script src="<?=HTTP_SERVER?>javascript/util.js" type="text/javascript"></script>
<script src="<?=HTTP_SERVER?>javascript/jquery.js" type="text/javascript"></script>
<script src="<?=HTTP_SERVER?>javascript/thickbox.js" type="text/javascript"></script>
<?php /*?><script type="text/javascript" src="<?=HTTP_SERVER?>javascript/datepickercontrol.js"></script><?php */?>
<script type="text/javascript" src="<?=HTTP_SERVER?>javascript/datePicker.js"></script>
<?
//timeout based on config variable
if(SERVER_TIMEOUT == 1){
	?>
	<script type="text/javascript">
	trackTime = setTimeout ("logOut()", 7200000);
	function logOut () {
		<?
		//if not already on login page
		if($do!="login"){
		?>
	    window.location = "index.php?do=logout&expire=1";
	    <?}?>
	}
	</script>
	<?}?>
</head>
<body>
<div id="mainOuter">
<div id="mainDiv">
<div id="topDiv">
<div id="logo"></div>
<div id="rightSection">
<div class="topButtons">
<table cellpadding="0" cellspacing="0" align="right">
<tr>
<td>
Welcome to CheckUrInsurance <strong><?=$welcomeName?></strong>!
<?if($sessionId != ""){?>
    &nbsp;&nbsp;&nbsp; <a href="<?=HTTP_SERVER?>index.php?do=select&comp=yes">Company</a>: <?=$sessionCompanyName?><?if($sessionOfficeId!=""){echo " > ".$sessionOfficeName;}?> | <a href="<?=HTTP_SERVER?>index.php?do=change_password">Account</a> | <a href="<?=HTTP_SERVER?>index.php?do=logout" class="logout">Logout</a>
    <?}else{?>
    Please <a href="<?=HTTP_SERVER?>index.php?do=login">Log In</a> or <a href="<?=HTTP_SERVER?>index.php?do=contactus">Sign Up</a>
<?}?>
</td>
</tr>
</table>
</div>
<div class="bottomButtons">
<div class="bottomButtonsInner">
<ul>
<?if($sessionId == ""){?>
    <li><a href="<?=HTTP_SERVER?>index.php?do=login" <?if($do == "login" or $do == "home"){?>class="selected"<?}?>>Home</a></li>
    <li><a href="<?=HTTP_SERVER?>index.php?do=aboutus" <?if($do == "aboutus"){?>class="selected"<?}?>>About Us</a></li>
    <li><a href="<?=HTTP_SERVER?>index.php?do=whatis" <?if($do == "whatis"){?>class="selected"<?}?>>What is it?</a></li>
    <li><a href="<?=HTTP_SERVER?>index.php?do=advantages" <?if($do == "advantages"){?>class="selected"<?}?>>Advantages</a></li>
    <li><a href="<?=HTTP_SERVER?>index.php?do=contactus" <?if($do == "contactus"){?>class="selected"<?}?>>Contact Us</a></li>
    <li><a href="<?=HTTP_SERVER?>index.php?do=testimonials" <?if($do == "testimonials"){?>class="selected"<?}?>>Testimonials</a></li>
    <?}else{?>
    <li><a href="<?=HTTP_SERVER?>index.php?do=home" <?if($do == "home"){?>class="selected"<?}?>>Home</a></li>
    <?
    //if one is allowed to view this module
    if($moduleAll == 1 or $moduleUsersView == 1){?>
        <li><a href="<?=HTTP_SERVER?>index.php?do=users" <?if($do == "users" and !$_GET["userType"]){?>class="selected"<?}?>>Users</a></li>
    <?}?>
    <?
    //if one is allowed to view this module
    if($moduleAll == 1 or $moduleDoctorsView == 1){?>
        <li><a href="<?=HTTP_SERVER?>index.php?do=users&userType=doctor" <?if($do == "users" and $_GET["userType"]){?>class="selected"<?}?>>Doctors</a></li>
    <?}?>
    <?
    //if one is allowed to view this module
    if($moduleAll == 1 or $moduleLevelsView == 1){?>
        <li><a href="<?=HTTP_SERVER?>index.php?do=levels" <?if($do == "levels"){?>class="selected"<?}?>>User Levels</a></li>
    <?}?>
    <?
    //if one is allowed to view this module
    if($moduleAll == 1 or $moduleCompaniesView == 1){?>
        <li><a href="<?=HTTP_SERVER?>index.php?do=companies" <?if($do == "companies" or $doExplode[0] == "companies"){?>class="selected"<?}?>>Companies</a></li>
    <?}?>
    <?
    //if one is allowed to view this module
    if($moduleAll == 1 or ($modulePatientsView == 1 and $modulePatientsMain == 1)){?>
        <li><a href="<?=HTTP_SERVER?>index.php?do=patients" <?if($do == "patients" and ($_GET["patientStatus"] == "" or $_GET["patientStatus"]=="New")){?>class="selected"<?}?>>Patients</a></li>
    <?}?>
    <?
    //if one is allowed to view this module
    if($moduleAll == 1 or ($modulePatientsView ==1 and $modulePatientsCheck == 1)){?>
        <li><a href="<?=HTTP_SERVER?>index.php?do=patients&patientStatus=Check" <?if(($do == "patients" or $do == "patients_eligibility") and ($_GET["patientStatus"] == "Check")){?>class="selected"<?}?>>Check</a></li>
    <?}?>
    <?
    //if one is allowed to view this module
    if($moduleAll == 1 or ($modulePatientsView ==1 and $modulePatientsUpdated == 1)){?>
        <li><a href="<?=HTTP_SERVER?>index.php?do=patients&patientStatus=Updated" <?if($do == "patients" and $_GET["patientStatus"] == "Updated"){?>class="selected"<?}?>>Updated</a></li>
    <?}?>
    <?
    //if one is allowed to view this module
    if($moduleAll == 1 or ($modulePatientsView ==1 and $modulePatientsPrinted == 1)){?>
        <li><a href="<?=HTTP_SERVER?>index.php?do=patients&patientStatus=Printed" <?if($do == "patients" and $_GET["patientStatus"] == "Printed"){?>class="selected"<?}?>>Reports</a></li>
    <?}?>
<?}?>
</ul>
</div>
</div>
</div>
</div>
<div id="middleDiv">
<table width="100%" cellpadding="0" cellspacing="0" align="center">
<tr>
<td id="pageTable" valign="top">
<?require(SITE_ROOT.'pages/'.$do.'.php');?>
</td>
</tr>
</table>
</div>
<div id="disclaimer">
	Disclaimer - The information provided for insurance verifications is supplied to Check Ur Insurance by the patientís insurance provider. Check Ur Insurance Verification Specialist do their very best to ensure that the information we provide is accurate and up to date. Check Ur Insurance relies solely on the insurance provider to obtain accurate information.
</div>
<div id="bottomDiv">
<table width="100%" cellpadding="0" cellspacing="0" align="center">
<tr>
<td align="left">&copy; Copyright <?=date("Y")?> CheckUrInsurance.com | All Rights Reserved | <a href="<?=HTTP_SERVER?>">Home</a> | <a href="<?=HTTP_SERVER?>/index.php?do=aboutus">About Us</a> | <a href="<?=HTTP_SERVER?>/index.php?do=whatis">What is it?</a> | <a href="<?=HTTP_SERVER?>/index.php?do=advantages">Advantages</a> | <a href="<?=HTTP_SERVER?>/index.php?do=contactus">Contact Us</a> | <a href="<?=HTTP_SERVER?>/index.php?do=testimonials">Testimonials</a></td>
<td align="right">
<div id="logo">
<object type="application/x-shockwave-flash" data="<?=HTTP_SERVER?>flash/lyja_logo.swf" width="83" height="35">
<param name="movie" value="<?=HTTP_SERVER?>flash/lyja_logo.swf" />
</object>
</div>
</td>
</tr>
</table>
</div>
</div>
</div>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-35608752-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>
<?exit();?>
<?
define("FTH",1);
?>