<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Color Admin | Login Page</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
	<link href="{{asset('/assets/css/app.min.css')}}" rel="stylesheet" />
	<link href="{{asset('/assets/plugins/ionicons/css/ionicons.min.css')}}" rel="stylesheet" />
	<style type="text/css">
		.top_bar {
			min-height: 40px;
			display: flex;
			width: 100%;
			background-color:#000;
			align-items: center;
			color: #fff; 
		}
		.top_bar a {
			color: #fff;
		}
		.header_banner {min-height: 160px;background-repeat: no-repeat!important;background-position: center center!important;}
		.login_footer {background-color: #000;min-height: 125px;display: flex;align-items: center;color:#fff;}
		.btn-red {background-color: #c51b1b !important;color: #fff;font-size: 16px;border-radius: 0px;}
	</style>
</head>
<body class="pace-top">
	
	




	@yield('content')



<footer class="login_footer">
	<div class="container">
		<div class="row">
			<div class="col-lg-2 col-md-12 d-flex align-items-center">
				<img src="https://www.checkurinsurance.com/app/images/logo-white.png" alt="">
			</div>
			<div class="col-lg-3 col-md-12">
				<p class="mt-4"><i class="fa fa-clock"></i> Mon to Fri: 8:00 am-5:00 pm PST</p>
				<p><i class="fa fa-globe"></i> <a href="" title="">www.checkurinsurance.com</a></p>
			</div>
			<div class="col-lg-7 col-md-12">
				<p class="mt-3"><b>Disclaimer:</b> The information provided for insurance verification's is supplied to Check Ur Insurance by the patient’s insurance provider. Check Ur Insurance Verification Specialist do their very best to ensure that the information we provide is accurate and up to date. Check Ur Insurance relies solely on the insurance provider to obtain accurate information.</p>
				<div class="row">
					<div class="col-lg-6 col-sm-12">
						<p>Copyright © 2017, Check Ur Insurance. All Rights Reserved</p>
					</div>
					<div class="col-lg-6 col-sm-12">
						<p class="text-right">Powered by <a href="" title="">Lyja</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>


<!-- ================== BEGIN BASE JS ================== -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" type="text/javascript"></script>

<script src="{{asset('/assets/js/app.min.js')}}"></script>
<script src="{{asset('/assets/js/theme/apple.min.js')}}"></script>
<!-- ================== END BASE JS ================== -->
</body>
</html>