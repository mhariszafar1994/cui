<h1>About Us</h1>
<div id="pageContainer">		
	<table width="100%" cellpadding="15" cellspacing="0">
		<tr>
			<td valign="top">
				<span class="subHeading">What is Check Ur Insurance?</span><br />
Check Ur Insurance is a one stop shop for validating insurance information of patients. Our dedicated resources will contact various insurance carriers, gather the data required, and present the patientsí insurance information to you whenever required. You will no longer have to dedicate resources within your staff to perform this task; it will be completed by us. As scheduled patients arrive to the clinics, you will be presented with all the information gathered by our resources on Check Ur Insurance secure online application. You will also have the option to print the information through reports.
<br /><br />
<span class="subHeading">What you do?</span><br />
Simply enter patientís information that requires insurance validation to this secured site.
<br /><br />
<span class="subHeading">What we will do?</span><br />
We will take the burden of validating insurance off your shoulders. Our resources that are dedicated to your clinic will contact the insurance carriers, capture the insurance information and store it in the online application. Necessary infrastructure will be provided by us for making these calls.<br /><br />
Our dedicated resource will check which patient information has to be updated and then call the insurance carriers and work with them to collect this information. Once the information is collected it will be readily available for your office in customized reports or online. Insurance information can be viewed at any time within the application or printed as reports.
			</td>
		</tr>
		<tr>
			<td align="center">
				<table cellpadding="0" cellspacing="0" align="center" width="800px">
					<tr>
						<td>&nbsp;</td>
						<td width="10px">&nbsp;</td>
						<td align="center"><b>Insurance information viewed by our employees</b></td>
						<td width="10px">&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="5">&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td width="10px">&nbsp;</td>
						<td><img src="<?=HTTP_SERVER?>images/what1_01.jpg" /></td>
						<td width="10px">&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>						
						<td><b>You enter insurance & patient Information in Check Ur Insurance secured website 24x7</b></td>
						<td width="10px">&nbsp;</td>
						<td><img src="<?=HTTP_SERVER?>images/what_02.jpg" /></td>
						<td width="10px">&nbsp;</td>
						<td><b>Our employee  contacts insurance company and validates information</b></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td width="10px">&nbsp;</td>
						<td><img src="<?=HTTP_SERVER?>images/what_03_02.jpg" /></td>
						<td width="10px">&nbsp;</td>						
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="5">&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td width="10px">&nbsp;</td>
						<td align="center"><b>You view patient information on the secure website or can run reports</b></td>
						<td width="10px">&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>	