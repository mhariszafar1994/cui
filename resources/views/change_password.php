<?
 $error_msg = "";
//if post
if(array_key_exists('pwdsubmit', $_POST)){
	$currentPassword = sanitize($_POST["currentPassword"]);
	$newPassword = sanitize($_POST["newPassword"]);
	$confirmPassword = sanitize($_POST["confirmPassword"]);
	
	if($currentPassword == "" or $newPassword == "" or $confirmPassword == ""){
		$error = 1;
		$error_msg = "All fields are required!<br>";
	} 
	

	
	if(strlen($newPassword)<8 || strlen($confirmPassword)<8){
		
		$error_msg .= "Password must be 8 characters long!<br>";
		$error = 1;
		 
	} 
	
	
	if(!preg_match('/[A-Z]+/', $newPassword) || !preg_match('/[0-9]+/', $newPassword) || !preg_match('/[A-Z]+/', $confirmPassword) || !preg_match('/[0-9]+/', $confirmPassword)){
		
		

		
		 $error_msg .= "Password must have one capital letter, one number!<br>";
		 $error = 1;

	}
	
	if(empty($error_msg)){
		
		if($newPassword == $confirmPassword){
		
			$sqlPassword = "SELECT * FROM cui_users WHERE userPassword='".md5($currentPassword)."' and userId = '$sessionId'";
//			$resultPassword = mysql_query($sqlPassword);
			$resultPassword = mysqli_query($con, $sqlPassword);
//			if(mysql_num_rows($resultPassword)>0){
			if(@mysqli_num_rows($resultPassword)>0){
				//new password
//				$userFname = sanitize(mysql_result($resultPassword,0,"userFname"));
				$userFname = sanitize(mysqli_result($resultPassword,0,"userFname"));
//				$userEmail = sanitize(mysql_result($resultPassword,0,"userEmail"));
				$userEmail = sanitize(mysqli_result($resultPassword,0,"userEmail"));
				$newPasswordEncrypted = md5($newPassword);
				$newPasswordClean = encrypt_decrypt('encrypt', sanitize($newPassword));
				$sqlUpdate = "UPDATE cui_users SET userPassword='$newPasswordEncrypted', userCleanPassword='$newPasswordClean' where userId='$sessionId'";
//				mysql_query($sqlUpdate);
				mysqli_query($con, $sqlUpdate);

				//sending email
				// additional header pieces for errors, From cc's, bcc's, etc 
				$headers = "From: Checkforinsurance <admin@syserco-support.com>\r\n";
				$headers.="Content-type: text/plain; charset=iso-8859-1\r\n";	
				
				//subject
				$subject = "Your Syserco Password Has Been Changed";
				
				//message
				$message = "Dear $userFname \n\n";
				$message .= "Your password has been changed and your new password is: \n\n";		
				$message .= "$newPassword \n\n";
				$message .= "Please use the new password above to login from now on. You can always change your password after logging in to the system";
				
				//mail($userEmail,$subject,$message,$headers);
				
				echo "<script>window.location='".HTTP_SERVER."index.php?do=$do&success=1'</script>";
			}else{
				$error = 1;
				$error_msg = "Your current password is incorrect. Please try again!";
			}
		
		}else{
			$error = 1;
			$error_msg = "Your New and Confirm Password fields do not match!";
		}
	}
}

?>
<style>
		hr {
			border-bottom: 0px;
		}
		table.form-spacing tbody tr td {
			padding-bottom: 9px;
		}
		#password-strength-status {padding: 5px 10px;color: #FFFFFF; border-radius:4px;margin-top:5px;}
		#password-match-status {padding: 5px 10px;color: #FFFFFF; border-radius:4px;margin-top:5px;}
		.medium-password{background-color: #E4DB11;border:#BBB418 1px solid;}
		.weak-password{background-color: #FF6600;border:#AA4502 1px solid;}
		.strong-password{background-color: #12CC1A;border:#0FA015 1px solid;}
		.glyphicon-ok {color: green;}
		.glyphicon-remove {color: red;}
		</style>

<h1 class="h1WithBg">Settings</h1>	
<div id="pageContainer">
	
	<table class="form-spacing" width="100%" cellpadding="5" cellspacing="0" align="center">
		<tr class="titleTr">
			<td><h3 style="padding: 10px 0px 0px 10px !important;">Change Password<h3></td>
		</tr>

		<?
		//if no success
		if(!$_GET["success"]){
		?>
			
		<?if($error == 1){?>
		<tr>
			<td>
				<div class="error"><?=$error_msg?></div>
			</td>
		</tr>
		<?}?>
		<tr>
			<td>
				Please enter your current password and new password below in order to change your password.
			</td>
		</tr>
		<tr align="center">
			<td>
				<form method="post" name="frm134">
				<table cellpadding="5" cellspacing="0" align="center">
					<tr>
						<td>Current Password</td>
						<td><input type="password" name="currentPassword" id="currentPassword" class="textbox form-control" size="50" /></td>
					</tr>
					<tr>
						<td>New Password</td>
						<td><input type="password" name="newPassword" id="newPassword" class="textbox form-control" onKeyUp="checkPasswordStrength();checkPasswordMatch();" size="50" /></td>
					</tr>
					<tr>
						<td></td>
						<td><div id="password-strength-status"></div></td>
					</tr>
					<tr>
						<td>Confirm New Password</td>
						<td><input type="password" name="confirmPassword" id="confirmPassword" onKeyUp="checkPasswordMatch();" class="textbox form-control" size="50" /></td>
						<td>
							<button type="button" name="pwdbutton" id="pwdbutton" class="btn searchbt" >Continue</button>
							<button type="submit" style="display:none;" name="pwdsubmit" id="pwdsubmit"></button>
						</td>
					</tr>
					<tr>
						<td></td>
						<td><div id="password-match-status"></div></td>
					</tr>
				</table>
				</form>
			</td>
		</tr>
		<tr align="center">
			<td>
				<table cellpadding="5" cellspacing="0" align="center">
					<tbody>
						<tr>
							<td>
								<p><span id="strlength" class="glyphicon glyphicon-remove"></span> Minimum 8 chars long.</p>
								<p><span id="lowercase" class="glyphicon glyphicon-remove"></span> At least one lower case.</p>
								<p><span id="uppercase" class="glyphicon glyphicon-remove"></span> At least one upper case.</p>
								<p><span id="onenum" class="glyphicon glyphicon-remove"></span> At least one number.</p>
								<p><span id="specchar" class="glyphicon glyphicon-remove"></span> At least one special character.</p>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>

		
		<?}else{?>
		
		<tr>
			<td>
				<div class="success">Your password has been changed and the new password has been emailed to you.</div>
			</td>
		</tr>
		
		<?}?>
		
	</table>
</div>
<script>
function checkPasswordMatch() {
	if ($('#newPassword').val() != $('#confirmPassword').val()) {
		$('#password-match-status').removeClass();
		$('#password-match-status').addClass('weak-password');
		$('#password-match-status').html("Password not matched");
	} else {
		$('#password-match-status').removeClass();
		$('#password-match-status').addClass('strong-password');
		$('#password-match-status').html("Password matched");
	}
}
		
function checkPasswordStrength() {
	var number = /([0-9])/;
	var lowercase = /([a-z])/;
	var uppercase = /([A-Z])/;
	var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
	
	var allowed_length = 0;
	var allowed_lower_case = 0;
	var allowed_upper_case = 0;
	var allowed_number = 0;
	var allowed_special_characters = 0;
	if($('#newPassword').val().length<8) {
		$('#strlength').removeClass();
		$('#strlength').addClass('glyphicon glyphicon-remove');
		$('#password-strength-status').removeClass();
		$('#password-strength-status').addClass('weak-password');
		$('#password-strength-status').html("Weak (should be atleast 8 characters.)");
	} else {
		allowed_length = 1;
		$('#strlength').removeClass();
		$('#strlength').addClass('glyphicon glyphicon-ok');
		//Numbers
		if($('#newPassword').val().match(number)) {
			allowed_number = 1;
			$('#onenum').removeClass();
			$('#onenum').addClass('glyphicon glyphicon-ok');
		} else {
			$('#onenum').removeClass();
			$('#onenum').addClass('glyphicon glyphicon-remove');
			$('#password-strength-status').removeClass();
			$('#password-strength-status').addClass('weak-password');
			$('#password-strength-status').html("Weak (should include at least one number)");
		}
		
		//Lower Case
		if($('#newPassword').val().match(lowercase)) {
			allowed_lower_case = 1;
			$('#lowercase').removeClass();
			$('#lowercase').addClass('glyphicon glyphicon-ok');
		} else {
			$('#lowercase').removeClass();
			$('#lowercase').addClass('glyphicon glyphicon-remove');
			$('#password-strength-status').removeClass();
			$('#password-strength-status').addClass('weak-password');
			$('#password-strength-status').html("Weak (should include at least one lower case)");
		}
		
		//Upper Case
		if($('#newPassword').val().match(uppercase)) {
			allowed_upper_case = 1;
			$('#uppercase').removeClass();
			$('#uppercase').addClass('glyphicon glyphicon-ok');
		} else {
			$('#uppercase').removeClass();
			$('#uppercase').addClass('glyphicon glyphicon-remove');
			$('#password-strength-status').removeClass();
			$('#password-strength-status').addClass('weak-password');
			$('#password-strength-status').html("Weak (should include at least one upper case)");
		}
		
		//Special Characters
		if($('#newPassword').val().match(special_characters)) {
			allowed_special_characters = 1;
			$('#specchar').removeClass();
			$('#specchar').addClass('glyphicon glyphicon-ok');
		} else {
			$('#specchar').removeClass();
			$('#specchar').addClass('glyphicon glyphicon-remove');
			$('#password-strength-status').removeClass();
			$('#password-strength-status').addClass('weak-password');
			$('#password-strength-status').html("Weak (should include at least one special character)");
		}
		
		if(allowed_length == 1 && allowed_number == 1 && allowed_lower_case == 1 && allowed_upper_case == 1 && allowed_special_characters == 1) {
			$('#password-strength-status').removeClass();
			$('#password-strength-status').addClass('strong-password');
			$('#password-strength-status').html("Strong");
		} else if(allowed_length == 1 && allowed_number == 1 && allowed_special_characters == 1) {
			$('#password-strength-status').removeClass();
			$('#password-strength-status').addClass('medium-password');
			$('#password-strength-status').html("Medium (should include alphabets, numbers & special characters.)");
		} 
	}
}
$( document ).ready(function() {
    $('#pwdbutton').on('click', function() {
		var number = /([0-9])/;
		var lowercase = /([a-z])/;
		var uppercase = /([A-Z])/;
		var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
		var allowed_length = 0;
		var allowed_lower_case = 0;
		var allowed_upper_case = 0;
		var allowed_number = 0;
		var allowed_special_characters = 0;
		if($('#newPassword').val().length>8) {
			allowed_length = 1;
			if ($('#newPassword').val().match(number)) {
				allowed_number = 1;
			}
			if ($('#newPassword').val().match(lowercase)) {
				allowed_lower_case = 1;
			} 
			if ($('#newPassword').val().match(uppercase)) {
				allowed_upper_case = 1;
			} 
			if ($('#newPassword').val().match(special_characters)) {
				allowed_special_characters = 1;
			} 
		}
		if ($('#newPassword').val() == $('#confirmPassword').val() && allowed_length == 1 && allowed_number == 1 && allowed_lower_case == 1 && allowed_upper_case == 1 && allowed_special_characters == 1) {
			//Form Submit 
			$('#pwdsubmit').click();
		}
	});
});
</script>