<?
//roles check
if($moduleAll == 0 and $moduleUsersView == 0){
	echo "<script>window.location='index.php?do=authorization'</script>";
}

//vars
$success = 0;
$act = "";
$showUserForm = 0;
$msg = "";
$sortBy = "";
$sortOrder = "";
$userType = '';
$queryString = '';
$sqlVars = '';

//if get userType
if($_GET["userType"]){
	$userType = sanitize($_GET["userType"]);
}
	
//vars to apply switch below
switch ($userType){
	case 'doctor':
		$headingTitle = 'Doctor';
		$queryString .= '&userType='.$userType;
		$sqlVars .= " and userLevel=$applicationDoctorLevelId";
		
		break;
	default:
		$headingTitle = 'User';
		$sqlVars .= " and userLevel!=$applicationDoctorLevelId";
		break;	
}	

//if get sortBy
if($_GET["sortBy"]){
	$sortBy = sanitize($_GET["sortBy"]);
}

//if get sortOrder
if($_GET["sortOrder"]){
	$sortOrder = sanitize($_GET["sortOrder"]);
}

//if get act
if($_GET["act"]){
	$act = sanitize($_GET["act"]);
}

//if get msg
if($_GET["msg"]){
	$msg = sanitize($_GET["msg"]);
}

//if get userId
if($_GET["userId"]){
	$userId = sanitize($_GET["userId"]);
}

//if act == changestatus
if($act == "changeStatus"){
	
	//roles check (one can edit or not)
	if($moduleAll == 0 and $moduleUsersEdit == 0){
		echo "<script>window.location='index.php?do=authorization'</script>";
	}
	
	$userId = sanitize($_GET["userId"]);
	$statusParameter = sanitize($_GET["parameter"]);
	if($statusParameter == "disable"){
//		mysql_query("UPDATE cui_users set userStatus=0 where userId='$userId'");
		mysqli_query($con, "UPDATE cui_users set userStatus=0 where userId='$userId'");
		$success = 1;
		$msg = "Success: $headingTitle has been blocked,";
	}else{
//		mysql_query("UPDATE cui_users set userStatus=1 where userId='$userId'");
		mysqli_query($con, "UPDATE cui_users set userStatus=1 where userId='$userId'");
		$success = 1;
		$msg = "Success: $headingTitle has been activated,";
	}
}

//if act is delete
if($act == "delete"){	
	
	//roles check (one can delete or not)
	if($moduleAll == 0 and $moduleUsersDelete == 0){
		echo "<script>window.location='index.php?do=authorization'</script>";
	}
	
	$userId=sanitize($_GET["userId"]);
	$deleteSql = "DELETE from cui_users_companies where userId='$userId'";
//	mysql_query($deleteSql);
	mysqli_query($con, $deleteSql);
	$deleteSql = "DELETE from cui_users where userId='$userId'";
//	mysql_query($deleteSql);
	mysqli_query($con, $deleteSql);
	$success = 1;
	$msg = "Success: $headingTitle has been deleted,";
}

//getting data
if($act == "edit"){
	
	//roles check (one can edit or not)
	if($moduleAll == 0 and $moduleUsersEdit == 0){
		echo "<script>window.location='index.php?do=authorization'</script>";
	}
	
	$headingInstruction = "Edit $headingTitle";
	$showUserForm = 1;
	$userSql="select * from cui_users where userId='$userId'";
//	$userResult=mysql_query($userSql);
	$userResult=mysqli_query($con, $userSql);
//	if(mysql_num_rows($userResult)){
	if(@mysqli_num_rows($userResult)){
//		$userFname = mysql_result($userResult,0,"userFname");
		$userFname = mysqli_result($userResult,0,"userFname");
//		$userLname = mysql_result($userResult,0,"userLname");
		$userLname = mysqli_result($userResult,0,"userLname");
		/*$userGender = mysql_result($userResult,0,"userGender");
		$userDob = mysql_result($userResult,0,"userDob");
		$userDobMonth = substr($userDob,5,2);
		$userDobDay = substr($userDob,8,2);
		$userDobYear = substr($userDob,0,4);*/
//		$userAddress = mysql_result($userResult,0,"userAddress");
		$userAddress = mysqli_result($userResult,0,"userAddress");
//		$userCountry = mysql_result($userResult,0,"userCountry");
		$userCountry = mysqli_result($userResult,0,"userCountry");
//		$userCity = mysql_result($userResult,0,"userCity");
		$userCity = mysqli_result($userResult,0,"userCity");
//		$userState = mysql_result($userResult,0,"userState");
		$userState = mysqli_result($userResult,0,"userState");

		if($userCountry != "United States"){
			$userStateAlternate = $userState;
		}
		
//		$userZip = mysql_result($userResult,0,"userZip");
		$userZip = mysqli_result($userResult,0,"userZip");
//		$userTaxId = mysql_result($userResult,0,"userTaxId");
		$userTaxId = mysqli_result($userResult,0,"userTaxId");
//		$userNationalId = mysql_result($userResult,0,"userNationalId");
		$userNationalId = mysqli_result($userResult,0,"userNationalId");
//		$userPhoneHome = mysql_result($userResult,0,"userPhoneHome");
		$userPhoneHome = mysqli_result($userResult,0,"userPhoneHome");
//		$userPhoneCellular = mysql_result($userResult,0,"userPhoneCellular");
		$userPhoneCellular = mysqli_result($userResult,0,"userPhoneCellular");
//		$userPhoneBusiness = mysql_result($userResult,0,"userPhoneBusiness");
		$userPhoneBusiness = mysqli_result($userResult,0,"userPhoneBusiness");
//		$userFax = mysql_result($userResult,0,"userFax");
		$userFax = mysqli_result($userResult,0,"userFax");
//		$userEmail = mysql_result($userResult,0,"userEmail");
		$userEmail = mysqli_result($userResult,0,"userEmail");
//		$userLevel = mysql_result($userResult,0,"userLevel");
		$userLevel = mysqli_result($userResult,0,"userLevel");
//		$userLogin = mysql_result($userResult,0,"userLogin");
		$userLogin = mysqli_result($userResult,0,"userLogin");
//		$userPassword=mysql_result($userResult,0,"userPassword");
		$userPassword=mysqli_result($userResult,0,"userPassword");
//		$usercleanPassword = mysql_result($userResult,0,"userCleanPassword");
		$usercleanPassword = mysqli_result($userResult,0,"userCleanPassword");
		$btnValue = "Update";
	}
}else{		
	$userFname = "";
	$userLname = "";
	/*$userGender = 'Male';
	$userDobMonth = "";
	$userDobDay = "";
	$userDobYear = "";*/
	$userAddress = "";
	$userCountry ="United States";
	$userCity = "";
	$userState = "";
	$userZip = "";
	$userTaxId = "";
	$userNationalId = "";
	$userPhoneHome = "";
	$userPhoneCellular = "";
	$userPhoneBusiness = "";
	$userFax = "";
	$userEmail = "";
	$userLevel = "";
	$userLogin = "";
	$userPassword = "";
	$btnValue = "Add";
}

//check for state tr alternate
if($userCountry == "United States"){
	$stateTrAlternate = 'style="display:none"';
	$stateTr = "";
}else{
	$stateTr = 'style="display:none"';
	$stateTrAlternate = "";
}

if($act == "add"){
	
	//roles check (one can add or not)
	if($moduleAll == 0 and $moduleUsersAdd == 0){
		echo "<script>window.location='index.php?do=authorization'</script>";
	}
	
	$headingInstruction = "Add New $headingTitle";
	$showUserForm = 1;
}


//saving information
if($_POST["submit"]){
	
	/*
	foreach ($_POST as $key=>$value){
		echo "$key = '$$key',";
	}
	*/
	
	$showUserForm = 1;
	$userFname = sanitize($_POST["userFname"]);
	$userLname = sanitize($_POST["userLname"]);
	/*$userGender = sanitize($_POST["userGender"]);
	$userDobMonth = sanitize($_POST["userDobMonth"]);
	$userDobDay = sanitize($_POST["userDobDay"]);
	$userDobYear = sanitize($_POST["userDobYear"]);
	$userDob = $userDobYear."-".$userDobMonth."-".$userDobDay;*/
	$userAddress = sanitize($_POST["userAddress"]);
	$userCountry = sanitize($_POST["userCountry"]);
	$userCity = sanitize($_POST["userCity"]);
	$userState = sanitize($_POST["userState"]);
	$userStateAlternate = sanitize($_POST["userStateAlternate"]);
	
	//if US
	if($userCountry != "United States"){
		$userState = $userStateAlternate;
	}
	
	$userZip = sanitize($_POST["userZip"]);
	if($_POST["userTaxId"]){$userTaxId = sanitize($_POST["userTaxId"]);}else{$userTaxId = "";};
	if($_POST["userNationalId"]){$userNationalId = sanitize($_POST["userNationalId"]);}else{$userNationalId = "";};
	$userPhoneHome = sanitize($_POST["userPhoneHome"]);
	$userPhoneCellular = sanitize($_POST["userPhoneCellular"]);
	$userPhoneBusiness = sanitize($_POST["userPhoneBusiness"]);
	$userFax = sanitize($_POST["userFax"]);
	$userEmail = sanitize($_POST["userEmail"]);
	$userLevel = sanitize($_POST["userLevel"]);
	$userLogin = sanitize($_POST["userLogin"]);
	if ($_POST["userPassword"] != ""){
		$userPassword=md5(sanitize($_POST["userPassword"]));
		///$usercleanPassword=sanitize($_POST["userPassword"]);
		$usercleanPassword=encrypt_decrypt('encrypt', sanitize($_POST["userPassword"]));
		$cleanpwd = ", userCleanPassword = '$usercleanPassword'";
	}
	if($userPassword == ""){
		$userPassword = sanitize($_POST["oldPassword"]);
		$cleanpwd = "";
	}
	
	if($act == "add"){
		$sqlCheck = "SELECT * FROM cui_users where userLogin = '$userLogin'";
//		$sqlResult = mysql_query($sqlCheck);
		$sqlResult = mysqli_query($con, $sqlCheck);

//		if(mysql_num_rows($sqlResult)<1){
		if(@mysqli_num_rows($sqlResult)<1){
			$sqlUser="INSERT INTO cui_users (userFname,userLname,userAddress,userCountry,userCity,userState,userZip,userTaxId,userNationalId,userPhoneHome,userPhoneCellular,userPhoneBusiness,userFax,userEmail,userLevel,userLogin,userPassword, userCleanPassword) values ('$userFname','$userLname','$userAddress','$userCountry','$userCity','$userState','$userZip','$userTaxId','$userNationalId','$userPhoneHome','$userPhoneCellular','$userPhoneBusiness','$userFax','$userEmail','$userLevel','$userLogin','$userPassword','$usercleanPassword')";
		
//			mysql_query($sqlUser);
			mysqli_query($con, $sqlUser);

			$success=1;		
			$msg = "Success: $headingTitle has been added.";
		
		}else{
			$error = "User login already exists.";
		}
	}else{
		$sqlCheck = "SELECT * FROM cui_users where userLogin = '$userLogin' AND userid!='$userId'";
//		$sqlResult = mysql_query($sqlCheck);
		$sqlResult = mysqli_query($con, $sqlCheck);

//		if(mysql_num_rows($sqlResult)<1){
		if(@mysqli_num_rows($sqlResult)<1){
		$sqlUser="update cui_users set userFname = '$userFname',userLname = '$userLname',userAddress = '$userAddress',userCountry = '$userCountry',userCity = '$userCity',userState = '$userState',userZip = '$userZip',userTaxId='$userTaxId',userNationalId='$userNationalId',userPhoneHome = '$userPhoneHome',userPhoneCellular = '$userPhoneCellular',userPhoneBusiness = '$userPhoneBusiness',userFax = '$userFax',userEmail = '$userEmail',userLevel = '$userLevel',userLogin = '$userLogin',userPassword = '$userPassword' $cleanpwd where userid='$userId'";
		
//		mysql_query($sqlUser);
		mysqli_query($con, $sqlUser);

		$success=1;
		$msg = "Success: $headingTitle details have been updated.";
		}else{
			$error = "User login already exists.";
		}
		
	}

	if ($success == 1) {
		//redirect
		echo "<script>window.location='".HTTP_SERVER."index.php?do=users&$queryString&msg=$msg'</script>";
	}
	
}

//filters
if($_GET["filter"]){
	$filter = sanitize($_GET["filter"]);
	$filterText = sanitize($_GET["filterText"]);
	$filter2 = sanitize($_GET["filter2"]);
	$filterText2 = sanitize($_GET["filterText2"]);
	$filterAndOr = sanitize($_GET["filterAndOr"]);
	//$queryString .= "&filter=$filter&filterText=$filterText";
	$queryString .= "&filter=$filter&filterText=$filterText&filter2=$filter2&filterText2=$filterText2&filterAndOr=$filterAndOr";
}else{
	$filterText = "";
    $filter = "";
    $filterText2 = "";
    $filter2 = "";
    $filterAndOr = "";
}

?>

<h1 class="h1WithBg"><?=$headingTitle?>s</h1>
<div id="pageContainer">
	

<?if($msg!=""){?>
	<div class="success"><?=$msg?></div>
<?}?>

<script type="text/javascript" language="JavaScript">
	function chkForm(){
		var phoneRE = /^\d{3}-\d{3}-\d{4}$/;
		if(document.getElementById("userFname").value==""){
			/*alert('Please enter a value for "First Name"');
			document.getElementById("userFname").focus();*/
			swal({
				title: 'Error!',
				text: 'Please enter a value for "First Name"',
				type: "error",
				showCancelButton: false,
				confirmButtonText: 'OK',
				closeOnConfirm: false
			},
			function(){
				swal.close();
				$("#userFname").focus();
			});
			return false;
		}
		
		if(document.getElementById("userLname").value==""){
			/*alert('Please enter a value for "Last Name"');
			document.getElementById("userLname").focus();*/
			swal({
				title: 'Error!',
				text: 'Please enter a value for "Last Name"',
				type: "error",
				showCancelButton: false,
				confirmButtonText: 'OK',
				closeOnConfirm: false
			},
			function(){
				swal.close();
				$("#userLname").focus();
			});
			return false;
		}
		
		if(document.getElementById("userAddress").value==""){
			/*alert('Please enter a value for "Address"');
			document.getElementById("userAddress").focus();*/
			swal({
				title: 'Error!',
				text: 'Please enter a value for "Address"',
				type: "error",
				showCancelButton: false,
				confirmButtonText: 'OK',
				closeOnConfirm: false
			},
			function(){
				swal.close();
				$("#userAddress").focus();
			});
			return false;
		}
		
		if(document.getElementById("userCity").value==""){
			/*alert('Please enter a value for "City"');
			document.getElementById("userCity").focus();*/
			swal({
				title: 'Error!',
				text: 'Please enter a value for "City"',
				type: "error",
				showCancelButton: false,
				confirmButtonText: 'OK',
				closeOnConfirm: false
			},
			function(){
				swal.close();
				$("#userCity").focus();
			});
			return false;
		}
		
		if(document.getElementById("userState").value==""){
			/*alert('Please select a value for "State"');
			document.getElementById("userState").focus();*/
			swal({
				title: 'Error!',
				text: 'Please select a value for "State"',
				type: "error",
				showCancelButton: false,
				confirmButtonText: 'OK',
				closeOnConfirm: false
			},
			function(){
				swal.close();
				$("#userState").focus();
			});
			return false;
		}
		
		if(document.getElementById("userZip").value==""){
			/*alert('Please enter a value for "Zip Code"');
			document.getElementById("userZip").focus();*/
			swal({
				title: 'Error!',
				text: 'Please enter a value for "Zip Code"',
				type: "error",
				showCancelButton: false,
				confirmButtonText: 'OK',
				closeOnConfirm: false
			},
			function(){
				swal.close();
				$("#userZip").focus();
			});
			return false;
		}
		
		if(document.getElementById("userCountry").value==""){
			/*alert('Please select a value for "Country"');
			document.getElementById("userCountry").focus();*/
			swal({
				title: 'Error!',
				text: 'Please select a value for "Country"',
				type: "error",
				showCancelButton: false,
				confirmButtonText: 'OK',
				closeOnConfirm: false
			},
			function(){
				swal.close();
				$("#userCountry").focus();
			});
			return false;
		}
		
		if(document.getElementById("userPhoneHome").value!=""){
			if (!phoneRE.test(document.getElementById("userPhoneHome").value)){
				/*alert("Phone Home is in invalid format!");
				document.getElementById("userPhoneHome").focus();*/
				swal({
					title: 'Error!',
					text: 'Phone Home is in invalid format!',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#userPhoneHome").focus();
				});
				return false;
			}
		}
		
		if(document.getElementById("userPhoneCellular").value!=""){
			if (!phoneRE.test(document.getElementById("userPhoneCellular").value)){
				/*alert("Phone Cellular is in invalid format!");
				document.getElementById("userPhoneCellular").focus();*/
				swal({
					title: 'Error!',
					text: 'Phone Cellular is in invalid format!',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#userPhoneCellular").focus();
				});
				return false;
			}
		}
		
		if(document.getElementById("userPhoneBusiness").value!=""){
			if (!phoneRE.test(document.getElementById("userPhoneBusiness").value)){
				/*alert("Phone Business is in invalid format!");
				document.getElementById("userPhoneBusiness").focus();*/
				swal({
					title: 'Error!',
					text: 'Phone Business is in invalid format!',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#userPhoneBusiness").focus();
				});
				return false;
			}
		}
		
		if(document.getElementById("userFax").value!=""){
			if (!phoneRE.test(document.getElementById("userFax").value)){
				/*alert("Fax is in invalid format!");
				document.getElementById("userFax").focus();*/
				swal({
					title: 'Error!',
					text: 'Fax is in invalid format!',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#userFax").focus();
				});
				return false;
			}
		}
		
/*		if(document.getElementById("userEmail").value==""){
			alert('Please enter a value for "Email Address"');
			document.getElementById("userEmail").focus();
			return false;
		}*/
		
		if(document.getElementById("userEmail").value!=""){
			var str=document.getElementById("userEmail").value
				var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
			if (filter.test(str))
				testresults=true
			else{
				/*alert('A valid "Email Address" is required')
				document.getElementById("userEmail").focus();*/
				swal({
					title: 'Error!',
					text: 'A valid "Email Address" is required',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#userEmail").focus();
				});
				return false;
			}
		}
		
		if(document.getElementById("userLogin").value==""){
			/*alert('Please enter a value for "User Login"');
			document.getElementById("userLogin").focus();*/
			swal({
				title: 'Error!',
				text: 'Please enter a value for "User Login"',
				type: "error",
				showCancelButton: false,
				confirmButtonText: 'OK',
				closeOnConfirm: false
			},
			function(){
				swal.close();
				$("#userLogin").focus();
			});
			return false;
		}
		
		<? if($act != "edit"){?>
		
		if(document.getElementById("userPassword").value==""){
			/*alert('Please enter a value for "Password"');
			document.getElementById("userPassword").focus();*/
			swal({
				title: 'Error!',
				text: 'Please enter a value for "Password"',
				type: "error",
				showCancelButton: false,
				confirmButtonText: 'OK',
				closeOnConfirm: false
			},
			function(){
				swal.close();
				$("#userPassword").focus();
			});
			return false;
		}
		
		if(document.getElementById("userPassword").value!=""){
			var chkLen = document.getElementById("userPassword").value;
			if(chkLen.length < 5)
			{
				/*alert('"Password" length should be at least 5 digits');
				document.getElementById("userPassword").focus();*/
				swal({
					title: 'Error!',
					text: '"Password" length should be at least 5 digits',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#userPassword").focus();
				});
				return false;
			}
		}
		
		if(document.getElementById("userPassword").value!=document.getElementById("userCPassword").value){
			/*alert('Password and Confirm Password do not match');
			document.getElementById("userPassword").focus();*/
			swal({
				title: 'Error!',
				text: 'Password and Confirm Password do not match',
				type: "error",
				showCancelButton: false,
				confirmButtonText: 'OK',
				closeOnConfirm: false
			},
			function(){
				swal.close();
				$("#userPassword").focus();
			});
			return false;
		}
		
		
		<? } else { ?>
		if(document.getElementById("userPassword").value!=""){
			var chkLen = document.getElementById("userPassword").value;
			if(chkLen.length < 5)
			{
				/*alert('"Password" length should be at least 5 digits');
				document.getElementById("userPassword").focus();*/
				swal({
					title: 'Error!',
					text: '"Password" length should be at least 5 digits',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#userPassword").focus();
				});
				return false;
			}
		}
		<? } ?>
	
	}
	
</script>
<div id="addUser" <?if($showUserForm == 0){?>style="display:none"<?}?>>
	<form method="POST">		
		<?
		//if act == edit take users previous password in hidden
		if($act == "edit"){?>
		<input type="hidden" name="oldPassword" value="<?=$userPassword?>" />
		<?}?>
		<style>
		hr {
			border-bottom: 0px;
		}
		table.form-spacing tbody tr td {
			padding-bottom: 9px;
		}
		</style>
		<table align="center" class="form-spacing" width="100%" border="0" cellpadding="5" cellspacing="1">
			<tr class="titleTr">
				<td><h3 style="padding-top: 9px; padding-left: 5px;"><?=$headingInstruction?></h3></td>
			</tr>
			<tr>
				<td>
					<?if($error != ""){?>
						<div class="error"><?=$error?></div>		
					<?}else{?>
						<div class="message">The required fields are marked with <span class="required">*</span></div>
					<?}?>
				</td>
			</tr>
			<tr>
				<td>
					<table width="55%" align="center" cellpadding="3" cellspacing="0">	
						<tr>
							<td colspan="2"><strong>Personal Information:</strong></td>
						</tr>
						<tr>
							<td colspan="2"><hr /></td>
						</tr>	
						<tr>
							<td width="150px">First Name <span class="required">*</span></td>
							<td><input type="text" class="textbox" id="userFname" name="userFname" value="<?=$userFname?>" /></td>
						</tr>
						<tr>
							<td>Last Name <span class="required">*</span></td>
							<td><input type="text" class="textbox" id="userLname" name="userLname" value="<?=$userLname?>" /></td>
						</tr>	
						
						<?php /*?><tr>
							<td>Gender</td>
							<td>
								<select name="userGender" id="userGender">
									<option <?if($userGender == "Male"){?>selected<?}?> value="Male">Male</option>
									<option <?if($userGender == "Female"){?>selected<?}?> value="Female">Female</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Date of Birth</td>
							<td>
								<select name="userDobMonth" id="userDobMonth">
									<?
									for ($i=1;$i<13;$i++){
																				
										if($i<10){
											$j = "0".$i;
										}else{
											$j = $i;
										}
										
										$sel = "";
										if($userDobMonth == $j){
											$sel = "selected";
										}
										
										echo "<option $sel value=\"$j\">$j</option>";
										
									}
									?>
								</select>
								<select name="userDobDay" id="userDobDay">
									<?
									for ($i=1;$i<32;$i++){
																				
										if($i<10){
											$j = "0".$i;
										}else{
											$j = $i;
										}
										
										$sel = "";
										if($userDobDay == $j){
											$sel = "selected";
										}
										
										echo "<option $sel value=\"$j\">$j</option>";
										
									}
									?>
								</select>
								<select name="userDobYear" id="userDobYear">
									<?
									$currentYear = date("Y");
									$tillYear = $currentYear-100;
									for ($i=$tillYear;$i<=$currentYear;$i++){
										$sel = "";
										if($userDobYear == $i){
											$sel = "selected";
										}
										echo "<option $sel value=\"$i\">$i</option>";
										
									}
									?>
								</select>
							</td>
						</tr><?php */?>
						<tr>
							<td>Address <span class="required">*</span></td>
							<td>
								<textarea id="userAddress" name="userAddress" class="textbox" style="width: 250px; height: 50px"><?=$userAddress?></textarea>
							</td>
						</tr>						
						<tr>
							<td>City <span class="required">*</span></td>
							<td><input type="text" class="textbox" id="userCity" name="userCity" value="<?=$userCity?>" /></td>
						</tr>
						
						<tr id="stateTr" <?=$stateTr?>>
							<td>State <span class="required">*</span></td>
							<td>
								<select name="userState" id="userState">
									<?
									$stateSql = "SELECT * from cui_states order by stateName ASC";
//									$stateResult = mysql_query($stateSql);
									$stateResult = mysqli_query($con, $stateSql);
//									while ($stateRs = mysql_fetch_array($stateResult)) {
									while ($stateRs = @mysqli_fetch_array($stateResult)) {

										$getStateName = $stateRs["stateName"];
										
										if($userState == $getStateName){
											$sel = "selected";
										}else{
											$sel = "";
										}
										
										echo "<option $sel value=\"$getStateName\">$getStateName</option>";
									}
									?>
								</select>
							</td>
						</tr>
						
						<tr id="stateTrAlternate" <?=$stateTrAlternate?>>
							<td>State / Province <span class="required">*</span></td>
							<td><input type="text" name="userStateAlternate" id="userStateAlternate" value="<?=$userStateAlternate?>" class="textbox" /></td>
						</tr>
						
						<tr>
							<td>Zip Code <span class="required">*</span></td>
							<td><input type="text" class="textbox" id="userZip" name="userZip" size="10" value="<?=$userZip?>" /></td>
						</tr>
						<tr>
							<td>Country <span class="required">*</span></td>
							<td>
								<select name="userCountry" id="userCountry" onchange="javascript: hideUsStates(this.value,'stateTr','stateTrAlternate')">
									<?
									$countrySql = "SELECT * from cui_countries order by countryName ASC";
//									$countryResult = mysql_query($countrySql);
									$countryResult = mysqli_query($con, $countrySql);
//									while ($countryRs = mysql_fetch_array($countryResult)) {
									while ($countryRs = @mysqli_fetch_array($countryResult)) {

										$getCountryName = $countryRs["countryName"];
										
										if($userCountry == $getCountryName){
											$sel = "selected";
										}else{
											$sel = "";
										}
										
										echo "<option $sel value=\"$getCountryName\">$getCountryName</option>";
									}
									?>
								</select>
							</td>
						</tr>
						<?
						//special setting for doctors
						if($userType == "doctor"){?>
						<tr>
							<td>Tax ID# <span class="required">*</span></td>
							<td><input type="text" class="textbox" id="userTaxId" name="userTaxId" value="<?=$userTaxId?>" /></td>
						</tr>	
						<tr>
							<td>National Provider ID# <span class="required">*</span></td>
							<td><input type="text" class="textbox" id="userNationalId" name="userNationalId" value="<?=$userNationalId?>" /></td>
						</tr>												
						<?}?>
						<tr>
							<td>Phone</td>
							<td>
								<table cellpadding="2" cellspacing="0">
									<tr>
										<td width="120px">Home</td>
										<td><input type="text" class="textbox" id="userPhoneHome" name="userPhoneHome" size="20" value="<?=$userPhoneHome?>" /> (eg. xxx-xxx-xxxx)</td>
									</tr>
									<tr>
										<td width="120px">Cellular</td>
										<td><input type="text" class="textbox" id="userPhoneCellular" name="userPhoneCellular" size="20" value="<?=$userPhoneCellular?>" /> (eg. xxx-xxx-xxxx)</td>
									</tr>
									<tr>
										<td width="120px">Business</td>
										<td><input type="text" class="textbox" id="userPhoneBusiness" name="userPhoneBusiness" size="20" value="<?=$userPhoneBusiness?>" /> (eg. xxx-xxx-xxxx)</td>
									</tr>
								</table>
							</td>
						</tr>
						
						<tr>
							<td>Fax</td>
							<td><input type="text" class="textbox" id="userFax" name="userFax" value="<?=$userFax?>" /> (eg. xxx-xxx-xxxx)</td>
						</tr>
						<tr>
							<td>Email Address</td>
							<td><input type="text" class="textbox" id="userEmail" name="userEmail" value="<?=$userEmail?>" size="40" /></td>
						</tr>		
						
						<tr>
							<td colspan="2"><br />&nbsp;</td>
						</tr>						
						<tr>
							<td colspan="2"><strong>Login Information:</strong></td>
						</tr>
						<tr>
							<td colspan="2"><hr /></td>
						</tr>				
						<?
						//special setting for doctors
						if($userType == "doctor"){?>
						<input type="hidden" name="userLevel" id="userLevel" value="<?=$applicationDoctorLevelId?>" />	
						<?}else{?>
						<tr>
							<td>User Level</td>
							<td>
								<select name="userLevel" id="userLevel">
									<?
									$levelCounter=0;
									$levelRate = 0;
									$levelSql = "SELECT * from cui_users_levels order by levelTitle ASC";
//									$levelResult = mysql_query($levelSql);
									$levelResult = mysqli_query($con, $levelSql);
//									while ($levelRs = mysql_fetch_array($levelResult)) {
									while ($levelRs = @mysqli_fetch_array($levelResult)) {
										$levelCounter++;
										$getLevelId = $levelRs["levelId"];
										$getLevelTitle = $levelRs["levelTitle"];
										
										if($userLevel == $getLevelId){
											$sel = "selected";
										}else{
											$sel = "";
										}
										
										echo "<option $sel value=\"$getLevelId\">$getLevelTitle</option>";
									}
									?>
								</select>
							</td>
						</tr>		
						<?}?>
						<tr>
							<td><?=$headingTitle?> Login <span class="required">*</span></td>
							<td><input type="text" class="textbox" id="userLogin" name="userLogin" value="<?=$userLogin?>" /></td>
						</tr>
						<tr>
							<td>Password <span class="required">*</span></td>
							<td>
								<table cellpadding="0" cellspacing="0">
									<tr>
										<td align="left">
											<input type="password" class="textbox" id="userPassword" name="userPassword" />
										</td>
										<?
										if($act=="edit"){
										?>
										<td align="left" style="padding-left:05px;">
											<input type="button" class="btn searchbt" value="Show Password" onclick="javascript:showHideSingle('pwdContent');" />
										</td>
										<td align="left" style="padding-left:05px;">
											<div id="pwdContent" style="display: none;">
												<em style="font-weight:bold;"><?=encrypt_decrypt('decrypt', $usercleanPassword)?></em>
											</div>
										</td>
										<? } ?>
									</tr>
								</table>
							</td>
							
						</tr>
						<?
						if($act!="edit"){
						?>
						
						<tr>
							<td>Confirm Password <span class="required">*</span></td>
							<td><input type="password" class="textbox" id="userCPassword" name="userCPassword" /></td>
						</tr>
						
						<?}else{?>					
						<tr>
							<td colspan="2"><div class="notes">NOTE: Leave the password field blank if you do not wish to change it!</div></td>
						</tr>
						<?}?>
						
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td><input type="submit" class="btn searchbt" name="submit" value="<?=$btnValue?>" onclick="return chkForm()" />&nbsp;<input type="button" class="btn clearbt" value="Cancel" onclick="javascript:window.location='index.php?do=<?=$do?><?=$queryString?>'" /></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
</div>

<?if($showUserForm == 0){?>
	<div id="help">
		<div id="helpContent" style="display: none">
			<table cellpadding="3" cellspacing="0">	
				
				<tr>			
					<td><span title="Edit" class="glyphicon glyphicon-trash"></span></td>
					<td>&nbsp;</td>
					<td>Delete an existing user.</td>
				</tr>
				<tr>
					<td><span title="Edit" class="glyphicon glyphicon-pencil"></span></td>
					<td>&nbsp;</td>
					<td>Edit existing user information.</td>
				</tr>
				<tr>			
					<td><img alt="Active" src="<?=HTTP_SERVER?>images/green.png" border="0" /></td>
					<td>&nbsp;</td>
					<td><?=$headingTitle?>'s status is active.</td>
				</tr>
				<tr>			
					<td><img alt="Blocked" src="<?=HTTP_SERVER?>images/red.png" border="0" /></td>
					<td>&nbsp;</td>
					<td><?=$headingTitle?>'s status is inactive / blocked.</td>
				</tr>
				
			</table>
		</div>
	</div>	
	<!--<form method="GET" action="index.php">-->
		<div class="searchcard">
			<div class="searchformcontainer">
				<form class="form-inline" method="GET" action="index.php">
				<input type="hidden" name="do" value="<?=$do?>" />
				<div class="form-group">
					<select name="filter" class="selectpicker">
						<option value="">--- select ---</option>
						<option <?if($filter == "userFname"){?>selected<?}?> value="userFname">First Name</option>
						<option <?if($filter == "userLname"){?>selected<?}?> value="userLname">Last Name</option>
						<option <?if($filter == "userEmail"){?>selected<?}?> value="userEmail">Email Address</option>
						<option <?if($filter == "userLevel"){?>selected<?}?> value="userLevel">Level / Role</option>
						<option <?if($filter == "userStatus"){?>selected<?}?> value="userStatus">Status</option>
						<option <?if($filter == "companyName"){?>selected<?}?> value="companyName">Dental Company</option>
						<option <?if($filter == "officeName"){?>selected<?}?> value="officeName">Dental Office</option>
						<option <?if($filter == "companyCity"){?>selected<?}?> value="companyCity">Dental City</option>
						<option <?if($filter == "companyState"){?>selected<?}?> value="companyState">Dental State</option>
					</select>
				</div>
				<div class="form-group">
					<input type="text" name="filterText" id="filterText" class="form-control" id="text" value="<?=$filterText;?>" placeholder="Search Text">
				</div>
				<span class="searchcondition">AND</span>
				<div class="radio radio-inline">
                    <input type="radio" name="filterAndOr" value="and" id="inlineRadio2" <?php if ($filterAndOr == "and"){ ?>checked<? } ?> <?php if ($filterAndOr == ""){ ?>checked<? } ?> />
                </div>
				<span class="searchcondition"> | OR</span>
				<div class="radio radio-inline">
					<input type="radio" name="filterAndOr" value="or" id="inlineRadio2" <?php if ($filterAndOr == "or"){ ?>checked<? } ?> />
                </div>
				<div class="form-group">
					<select name="filter2" class="selectpicker">
						<option value="">--- select ---</option>
						<option <?if($filter2 == "userFname"){?>selected<?}?> value="userFname">First Name</option>
						<option <?if($filter2 == "userLname"){?>selected<?}?> value="userLname">Last Name</option>
						<option <?if($filter2 == "userEmail"){?>selected<?}?> value="userEmail">Email Address</option>
						<option <?if($filter2 == "userLevel"){?>selected<?}?> value="userLevel">Level / Role</option>
						<option <?if($filter2 == "userStatus"){?>selected<?}?> value="userStatus">Status</option>
						<option <?if($filter2 == "companyName"){?>selected<?}?> value="companyName">Dental Company</option>
						<option <?if($filter2 == "officeName"){?>selected<?}?> value="officeName">Dental Office</option>
						<option <?if($filter2 == "companyCity"){?>selected<?}?> value="companyCity">Dental City</option>
						<option <?if($filter2 == "companyState"){?>selected<?}?> value="companyState">Dental State</option>
					</select>
				</div>
				<div class="form-group">
					<input type="text" name="filterText2" id="filterText2" class="form-control" id="text" value="<?=$filterText2;?>" placeholder="Search Text 2">
				</div>
				<div class="form-group">
					<input type="hidden" name="userType" value="<?=$userType?>" />
					<button type="submit" class="btn searchbt">Search</button>
				</div>
				<div class="form-group">
					<? 
					if ($headingTitle == 'Doctor') {
						$clearfilter = '&userType=doctor';
					} else {
						$clearfilter = '';
					}
					?>
					<button type="button" onclick="javascript: window.location='index.php?do=users<?=$clearfilter?>'" class="btn clearbt">Clear</button>
				</div>
				<?							
				//roles check (one can add or not)
				if($moduleAll == 1 or $moduleUsersAdd == 1){?>
				<div class="form-group">
					<button type="button" onclick="javascript: window.location='index.php?do=users&act=add<?=$queryString?>'" class="btn greenbt">Add <?=$headingTitle?></button>
				</div>
				<? } ?>
				</form>
			</div>
		</div>	
	<!--</form>					-->
	<br />	
	<table class="table table-bordered">
		<!--<tr class="titleTr">
			<td colspan="8"><h3><?=$headingTitle?> Listing</h3></td>
		</tr>-->
		<thead>
		<tr>
			<td width="6%">Status</td>
			<td width="10%"><a href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=userLogin&sortOrder=<?if($sortBy == "userLogin"){if($sortOrder == "ASC"){echo "DESC";}else{echo "ASC";}}else{echo "ASC";}?>">Login<?if($sortBy == "userLogin"){?>&nbsp;<img src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0" alt="<?=$sortOrder?> Order" /><?}?></a></td>
			<td width="12%"><a href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=userFname&sortOrder=<?if($sortBy == "userFname"){if($sortOrder == "ASC"){echo "DESC";}else{echo "ASC";}}else{echo "ASC";}?>">Name<?if($sortBy == "userFname"){?>&nbsp;<img src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0" alt="<?=$sortOrder?> Order" /><?}?></a></td>
			<td width="12%"><a href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=userCity&sortOrder=<?if($sortBy == "userCity"){if($sortOrder == "ASC"){echo "DESC";}else{echo "ASC";}}else{echo "ASC";}?>">City<?if($sortBy == "userCity"){?>&nbsp;<img src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0" alt="<?=$sortOrder?> Order" /><?}?></a></td>
			<td width="10%"><a href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=userState&sortOrder=<?if($sortBy == "userState"){if($sortOrder == "ASC"){echo "DESC";}else{echo "ASC";}}else{echo "ASC";}?>">State<?if($sortBy == "userState"){?>&nbsp;<img src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0" alt="<?=$sortOrder?> Order" /><?}?></a></td>
			<td width="16%"><a href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=userEmail&sortOrder=<?if($sortBy == "userEmail"){if($sortOrder == "ASC"){echo "DESC";}else{echo "ASC";}}else{echo "ASC";}?>">Email Address<?if($sortBy == "userEmail"){?>&nbsp;<img src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0" alt="<?=$sortOrder?> Order" /><?}?></a></td>
			<td width="12%"><a href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=userLevel&sortOrder=<?if($sortBy == "userLevel"){if($sortOrder == "ASC"){echo "DESC";}else{echo "ASC";}}else{echo "ASC";}?>">Level / Role<?if($sortBy == "userLevel"){?>&nbsp;<img src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0" alt="<?=$sortOrder?> Order" /><?}?></a></td>	
			<td width="10%">Actions</td>
		</tr>
		</thead>
		<tbody>
		<?				
		//pagination code
		if((!isset($_GET['page'])) && (!isset($_POST['page']))){ 
		    $page = 1; 
		} else { 
			if (!$_GET['page']) {
				$page = $_POST['page']; 
			} else {
				$page = $_GET['page']; 
			}	  
		} 
	
		//if get filters
		/*if ($_GET["filter"] || $_GET["filter2"]) {
            $filter = sanitize($_GET["filter"]);
            $filter2 = sanitize($_GET["filter2"]);
            if ($filter != "" || $filter2 != "") {
                $filterText = sanitize($_GET["filterText"]);
                $filterText2 = sanitize($_GET["filterText2"]);
                $filterAndOr = 'AND';
                if ($_GET["filterAndOr"]) {
                    $filterAndOr = strtoupper(sanitize($_GET["filterAndOr"]));
                }
                $filter_reserved = false;
                $filter_reserved2 = false;

                $cui_users_fields_arr = [
                    'userFname',
                    'userLname'
                ];
                $isUsers = false;
                if (in_array($filter, $cui_users_fields_arr) || in_array($filter2, $cui_users_fields_arr)) {
                    $where_clause_user = "";
                    if (in_array($filter, $cui_users_fields_arr)) {
                        $filter_reserved = true;
                        if ($filterText != "") {
                            $where_clause_user .= "$filter like '%$filterText%'";
                            if (in_array($filter2, $cui_users_fields_arr)) {
                                $filter_reserved2 = true;
                                if ($filterText2 != "") {
                                    $where_clause_user .= " $filterAndOr $filter2 like '%$filterText2%'";
                                }
                            }
                        } else {
                            if (in_array($filter2, $cui_users_fields_arr)) {
                                $filter_reserved2 = true;
                                if ($filterText2 != "") {
                                    $where_clause_user .= "$filter2 like '%$filterText2%'";
                                }
                            }
                        }
                    } else {
                        if (in_array($filter2, $cui_users_fields_arr)) {
                            $filter_reserved2 = true;
                            if ($filterText2 != "") {
                                $where_clause_user .= "$filter2 like '%$filterText2%'";
                            }
                        }
                    }
                    if ($where_clause_user != "") {
                        $isUsers = true;
                        $query_syntax = "select userId from cui_users where $where_clause_user AND userLevel = '$applicationDoctorLevelId'";
                        $sql = mysqli_query($con, $query_syntax);
                        if (@mysqli_num_rows($sql) > 0) {
                            $sqlVars .= " and (";
                            $sqlCounter = 0;
                            while ($rs = mysqli_fetch_object($sql)) {
                                $sqlCounter++;
                                if ($sqlCounter > 1) {
                                    $sqlVars .= " or ";
                                }
                                $sqlVars .= "userId = '" . $rs->userId . "'";
                            }
                            $sqlVars .= ")";
                        }
                    }
                }
                $isOffice = false;
                if ($filter == "officeName" || $filter2 == "officeName") {
                    $where_clause_office = "";
                    if ($filter == "officeName") {
                        $filter_reserved = true;
                        if ($filterText != "") {
                            $where_clause_office .= "officeName like '%$filterText%'";
                            if ($filter2 == "officeName") {
                                $filter_reserved2 = true;
                                if ($filterText2 != "") {
                                    $where_clause_office .= " $filterAndOr officeName like '%$filterText2%'";
                                }
                            }
                        } else {
                            if ($filter2 == "officeName") {
                                $filter_reserved2 = true;
                                if ($filterText2 != "") {
                                    $where_clause_office .= "officeName like '%$filterText2%'";
                                }
                            }
                        }
                    } else {
                        if ($filter2 == "officeName") {
                            $filter_reserved2 = true;
                            if ($filterText2 != "") {
                                $where_clause_office .= "officeName like '%$filterText2%'";
                            }
                        }
                    }
                    if ($where_clause_office != "") {
                        $isOffice = true;
                        $sql = mysqli_query($con, "select officeId from cui_companies_offices where $where_clause_office");
                        if (@mysqli_num_rows($sql) > 0) {
                            if ($isUsers) {
                                $sqlVars .= " $filterAndOr (";
                            } else {
                                $sqlVars .= " and (";
                            }
                            $sqlCounter = 0;
                            while ($rs = mysqli_fetch_object($sql)) {
                                $sqlCounter++;
                                if ($sqlCounter > 1) {
                                    $sqlVars .= " or ";
                                }
                                $sqlVars .= "officeId = '" . $rs->officeId . "'";
                            }
                            $sqlVars .= ")";
                        } else {
                            $sqlVars .= " and (";
                            $sqlVars .= "officeId = '0'";
                            $sqlVars .= ")";
                        }
                    } else {
                        $sqlVars .= " and (";
                        $sqlVars .= "officeId = '0'";
                        $sqlVars .= ")";
                    }
                }
                $isAppointmentData = false;
                if ($filter == "patientAppDate" || $filter2 == "patientAppDate") {
                    if ($filter == "patientAppDate") {
                        $filter_reserved = true;
                    }
                    if ($filter2 == "patientAppDate") {
                        $filter_reserved2 = true;
                    }
                    //if not empty
                    if ($filter == "patientAppDate" && $filterText != "") {
                        $filterText = formatDate($filterText, 2);
                    }
                    if ($filter2 == "patientAppDate" && $filterText2 != "") {
                        $filterText2 = formatDate($filterText2, 2);
                    }
                    $where_clause_appointment_data = "";
                    if ($filter == "patientAppDate") {
                        if ($filterText != "") {
                            if ($isUsers || $isOffice) {
                                $where_clause_appointment_data .= " $filterAndOr $filter like '%" . $filterText . "%'";
                            } else {
                                $where_clause_appointment_data .= " and $filter like '%" . $filterText . "%'";
                            }
                            if ($filter2 == "patientAppDate") {
                                if ($filterText2 != "") {
                                    $where_clause_appointment_data .= " $filterAndOr $filter2 like '%" . $filterText2 . "%'";
                                }
                            }
                        } else {
                            if ($filter2 == "patientAppDate") {
                                if ($filterText2 != "") {
                                    if ($isUsers || $isOffice) {
                                        $where_clause_appointment_data .= " $filterAndOr $filter2 like '%" . $filterText2 . "%'";
                                    } else {
                                        $where_clause_appointment_data .= " and $filter2 like '%" . $filterText2 . "%'";
                                    }
                                }
                            }
                        }
                    } else {
                        if ($filter2 == "patientAppDate") {
                            if ($filterText2 != "") {
                                if ($isUsers || $isOffice) {
                                    $where_clause_appointment_data .= " $filterAndOr $filter2 like '%" . $filterText2 . "%'";
                                } else {
                                    $where_clause_appointment_data .= " and $filter2 like '%" . $filterText2 . "%'";
                                }
                            }
                        }
                    }
                    if ($where_clause_appointment_data != "") {
                        $isAppointmentData = true;
                        $sqlVars .= $where_clause_appointment_data;
                    }
                }
                // get dental company name
                $isCompany = false;
                if ($filter == "companyName" || $filter2 == "companyName") {
                    $where_clause_company = "";
                    if ($filter == "companyName") {
                        $filter_reserved = true;
                        if ($filterText != "") {
                            $where_clause_company .= "$filter like '%$filterText%'";
                            if ($filter2 == "companyName") {
                                $filter_reserved2 = true;
                                if ($filterText2 != "") {
                                    $where_clause_company .= " $filterAndOr $filter2 like '%$filterText2%'";
                                }
                            }
                        } else {
                            if ($filter2 == "companyName") {
                                $filter_reserved2 = true;
                                if ($filterText2 != "") {
                                    $where_clause_company .= "$filter2 like '%$filterText2%'";
                                }
                            }
                        }
                    } else {
                        if ($filter2 == "companyName") {
                            $filter_reserved2 = true;
                            if ($filterText2 != "") {
                                $where_clause_company .= "$filter2 like '%$filterText2%'";
                            }
                        }
                    }
                    if ($where_clause_company != "") {
                        $isCompany = true;
                        $sql = mysqli_query($con, "select companyId from cui_companies where $where_clause_company");
                        if (@mysqli_num_rows($sql) > 0) {
                            if ($isUsers || $isOffice || $isAppointmentData) {
                                $sqlVars .= " $filterAndOr (";
                            } else {
                                $sqlVars .= " and (";
                            }
                            $sqlCounter = 0;
                            while ($rs = mysqli_fetch_object($sql)) {
                                $sqlCounter++;
                                if ($sqlCounter > 1) {
                                    $sqlVars .= " or ";
                                }
                                $sqlVars .= "a.companyId = '" . $rs->companyId . "'";
                            }
                            $sqlVars .= ")";
                        } else {
                            $sqlVars .= " and (";
                            $sqlVars .= "a.companyId = '0'";
                            $sqlVars .= ")";
                        }
                    } else {
                        $sqlVars .= " and (";
                        $sqlVars .= "a.companyId = '0'";
                        $sqlVars .= ")";
                    }
                }

                if ($filter == "patientGroup") {
                    if ($filterText != "") {
                        $filterText = encrypt_decrypt('encrypt', $filterText);
                    }
                }
                if ($filter2 == "patientGroup") {
                    if ($filterText2 != "") {
                        $filterText2 = encrypt_decrypt('encrypt', $filterText2);
                    }
                }
                if (!$filter_reserved) {
                    if (!$filter_reserved2) {
                        // both
                        if ($filter != "" && $filterText != "") {
                            $sqlVars .= " and $filter like '%" . $filterText . "%'";
                        }
                        if ($filter2 != "" && $filterText2 != "") {
                            $sqlVars .= " $filterAndOr $filter2 like '%" . $filterText2 . "%'";
                        }
                    } else {
                        // only filter
                        if ($filter != "" && $filterText != "") {
                            $sqlVars .= " $filterAndOr $filter like '%" . $filterText . "%'";
                        }
                    }
                } else {
                    if (!$filter_reserved2) {
                        if ($filter2 != "" && $filterText2 != "") {
                            $sqlVars .= " $filterAndOr $filter2 like '%" . $filterText2 . "%'";
                        }
                    }
                }
                $queryString .= "&filter=$filter&filterText=$filterText&filter2=$filter2&filterText2=$filterText2&filterAndOr=$filterAndOr";
            }
        }*/
					
		if ($_GET["filter"] || $_GET["filter2"]) {
			$filter = sanitize($_GET["filter"]);			
			$filter2 = sanitize($_GET["filter2"]);			
			if ($filter != "" || $filter2 != "") {
				$filterText = sanitize($_GET["filterText"]);
				$filterText2 = sanitize($_GET["filterText2"]);
                $filterAndOr = 'AND';
                if ($_GET["filterAndOr"]) {
                    $filterAndOr = strtoupper(sanitize($_GET["filterAndOr"]));
                }
				$filterAndOrON = false;
				//more conditions
				if($filter == "userStatus" || $filter2 == "userStatus"){
					if ($filter == "userStatus") {
						if($filterText == strtolower("blocked") or $filterText == strtolower("inactive") or $filterText == strtolower("disabled")){
							$filterText=0;
						}else{
							$filterText=1;
						}
						$sqlVars .= " and $filter =".$filterText;
					} else {
						if($filterText2 == strtolower("blocked") or $filterText2 == strtolower("inactive") or $filterText2 == strtolower("disabled")){
							$filterText2=0;
						}else{
							$filterText2=1;
						}
						$sqlVars .= " $filterAndOr ($filter2 =".$filterText2.")";
					}
				}
				if($filter == "userLevel" || $filter2 == "userLevel") {
					if ($filter == "userLevel") {
						$filterLevelSql="SELECT * from cui_users_levels where levelTitle like '%".$filterText."%'";
						$filterLevelResult = mysqli_query($con, $filterLevelSql);
						if(@mysqli_num_rows($filterLevelResult)>0){
							$sqlVars .= " and $filter = ".mysqli_result($filterLevelResult,0,"levelId");
						}
					} else {
						$filterLevelSql="SELECT * from cui_users_levels where levelTitle like '%".$filterText2."%'";
						$filterLevelResult = mysqli_query($con, $filterLevelSql);
						if(@mysqli_num_rows($filterLevelResult)>0){
							$sqlVars .= " $filterAndOr ($filter2 = ".mysqli_result($filterLevelResult,0,"levelId").")";
						}
					}
				}// get dental company
				if($filter == "companyName" || $filter2 == "companyName"){
					if ($filter == "companyName") {
						$sql= mysqli_query($con, "select companyId from cui_companies where companyName like '%$filterText%'");
						while ($rs = mysqli_fetch_object($sql)) {
							if ($vars != "")
								$vars .= " or ";
							if ($vars == "")
								$vars .= " and (";
							$vars .= "companyId = '".$rs->companyId."'";
						}
					
						if (@mysqli_num_rows($sql) == 0)
							$vars .= " and 1=0";
						else
							$vars .= ")";
						
						if (@mysqli_num_rows($sql) > 0) {
							$sql= mysqli_query($con, "select distinct(userId) as user from cui_users_companies where 1=1 $vars") or die(mysqli_error($con));
							$vars = "";
							while ($rs = mysqli_fetch_object($sql)) {
								if ($vars != "")
									$vars .= " or ";
								if ($vars == "")
									$vars .= " and (";
								$vars .= "userId = '".$rs->user."'";
							}
							if (@mysqli_num_rows($sql) == 0)
								$vars .= " and 1=0";
							else
								$vars .= ")";
							$sqlVars = $sqlVars.$vars;
						}
						else {
							$sqlVars = $sqlVars.$vars;
						}
					} else {
						$sql= mysqli_query($con, "select companyId from cui_companies where companyName like '%$filterText2%'");
						while ($rs = mysqli_fetch_object($sql)) {
							if ($vars != "")
								$vars .= " or ";
							if ($vars == "")
								$vars .= " and (";
							$vars .= "companyId = '".$rs->companyId."'";
						}
					
						if (@mysqli_num_rows($sql) == 0)
							$vars .= " and 1=0";
						else
							$vars .= ")";
						
						if (@mysqli_num_rows($sql) > 0) {
							$sql= mysqli_query($con, "select distinct(userId) as user from cui_users_companies where 1=1 $vars") or die(mysqli_error($con));
							$vars = "";
							while ($rs = mysqli_fetch_object($sql)) {
								if ($vars != "")
									$vars .= " or ";
								if ($vars == "")
									$vars .= " and (";
								$vars .= "userId = '".$rs->user."'";
							}
							if (@mysqli_num_rows($sql) == 0)
								$vars .= " and 1=0";
							else
								$vars .= ")";
							$sqlVars = $sqlVars.$vars;
						}
						else {
							$sqlVars = $sqlVars.$vars;
						}
					}
				}// get dental office
				if($filter == "officeName" || $filter2 == "officeName"){
					if ($filter == "officeName") {
						$sql= mysqli_query($con, "select officeId from cui_companies_offices where officeName like '%$filterText%'");
						while ($rs = mysqli_fetch_object($sql)) {
							if ($vars != "")
								$vars .= " or ";
							if ($vars == "")
								$vars .= " and (";
							$vars .= "officeId = '".$rs->officeId."'";
						}
						
						if (@mysqli_num_rows($sql) == 0)
							$vars .= " and 1=0";
						else
							$vars .= ")";
						
						if (@mysqli_num_rows($sql) > 0) {
							$sql= mysqli_query($con, "select distinct(userId) as user from cui_users_companies where 1=1 $vars") or die(mysqli_error($con));
							$vars = "";
							while ($rs = mysqli_fetch_object($sql)) {
								if ($vars != "")
									$vars .= " or ";
								if ($vars == "")
									$vars .= " and (";
								$vars .= "userId = '".$rs->user."'";
							}
							if (@mysqli_num_rows($sql) == 0)
								$vars .= " and 1=0";
							else
								$vars .= ")";
							$sqlVars = $sqlVars.$vars;
						}
						else {
							$sqlVars = $sqlVars.$vars;
						}
					} else {
						$sql= mysqli_query($con, "select officeId from cui_companies_offices where officeName like '%$filterText2%'");
						while ($rs = mysqli_fetch_object($sql)) {
							if ($vars != "")
								$vars .= " or ";
							if ($vars == "")
								$vars .= " and (";
							$vars .= "officeId = '".$rs->officeId."'";
						}
						
						if (@mysqli_num_rows($sql) == 0)
							$vars .= " and 1=0";
						else
							$vars .= ")";
						
						if (@mysqli_num_rows($sql) > 0) {
							$sql= mysqli_query($con, "select distinct(userId) as user from cui_users_companies where 1=1 $vars") or die(mysqli_error($con));
							$vars = "";
							while ($rs = mysqli_fetch_object($sql)) {
								if ($vars != "")
									$vars .= " or ";
								if ($vars == "")
									$vars .= " and (";
								$vars .= "userId = '".$rs->user."'";
							}
							if (@mysqli_num_rows($sql) == 0)
								$vars .= " and 1=0";
							else
								$vars .= ")";
							$sqlVars = $sqlVars.$vars;
						}
						else {
							$sqlVars = $sqlVars.$vars;
						}
					}
				}// get dental city
				if($filter == "companyCity" || $filter2 == "companyCity"){
					if ($filter == "companyCity") {
						$sql= mysqli_query($con, "select companyId from cui_companies where companyCity like '%$filterText%'");
						while ($rs = mysqli_fetch_object($sql)) {
							if ($vars != "")
								$vars .= " or ";
							if ($vars == "")
								$vars .= " and (";
							$vars .= "companyId = '".$rs->companyId."'";
						}
						
						if (@mysqli_num_rows($sql) == 0)
							$vars .= " and 1=0";
						else
							$vars .= ")";
						
						if (@mysqli_num_rows($sql) > 0) {
							$sql= mysqli_query($con, "select distinct(userId) as user from cui_users_companies where 1=1 $vars") or die(mysqli_error($con));
							$vars = "";
							while ($rs = mysqli_fetch_object($sql)) {
								if ($vars != "")
									$vars .= " or ";
								if ($vars == "")
									$vars .= " and (";
								$vars .= "userId = '".$rs->user."'";
							}
							if (@mysqli_num_rows($sql) == 0)
								$vars .= " and 1=0";
							else
								$vars .= ")";
							$sqlVars = $sqlVars.$vars;
						}
						else {
							$sqlVars = $sqlVars.$vars;
						}
					} else {
						$sql= mysqli_query($con, "select companyId from cui_companies where companyCity like '%$filterText2%'");
						while ($rs = mysqli_fetch_object($sql)) {
							if ($vars != "")
								$vars .= " or ";
							if ($vars == "")
								$vars .= " and (";
							$vars .= "companyId = '".$rs->companyId."'";
						}
						
						if (@mysqli_num_rows($sql) == 0)
							$vars .= " and 1=0";
						else
							$vars .= ")";
						
						if (@mysqli_num_rows($sql) > 0) {
							$sql= mysqli_query($con, "select distinct(userId) as user from cui_users_companies where 1=1 $vars") or die(mysqli_error($con));
							$vars = "";
							while ($rs = mysqli_fetch_object($sql)) {
								if ($vars != "")
									$vars .= " or ";
								if ($vars == "")
									$vars .= " and (";
								$vars .= "userId = '".$rs->user."'";
							}
							if (@mysqli_num_rows($sql) == 0)
								$vars .= " and 1=0";
							else
								$vars .= ")";
							$sqlVars = $sqlVars.$vars;
						}
						else {
							$sqlVars = $sqlVars.$vars;
						}
					}
				}// get dental state
				if($filter == "companyState" || $filter2 == "companyState"){
					if ($filter == "companyState") {
						$sql= mysqli_query($con, "select companyId from cui_companies where companyState like '%$filterText%'");
						while ($rs = mysqli_fetch_object($sql)) {
							if ($vars != "")
								$vars .= " or ";
							if ($vars == "")
								$vars .= " and (";
							$vars .= "companyId = '".$rs->companyId."'";
						}
						
						if (@mysqli_num_rows($sql) == 0)
							$vars .= " and 1=0";
						else
							$vars .= ")";
						
						if (@mysqli_num_rows($sql) > 0) {
							$sql= mysqli_query($con, "select distinct(userId) as user from cui_users_companies where 1=1 $vars") or die(mysqli_error($con));
							$vars = "";
							while ($rs = mysqli_fetch_object($sql)) {
								if ($vars != "")
									$vars .= " or ";
								if ($vars == "")
									$vars .= " and (";
								$vars .= "userId = '".$rs->user."'";
							}
							if (@mysqli_num_rows($sql) == 0)
								$vars .= " and 1=0";
							else
								$vars .= ")";
							$sqlVars = $sqlVars.$vars;
						}
						else {
							$sqlVars = $sqlVars.$vars;
						}
					} else {
						$sql= mysqli_query($con, "select companyId from cui_companies where companyState like '%$filterText2%'");
						while ($rs = mysqli_fetch_object($sql)) {
							if ($vars != "")
								$vars .= " or ";
							if ($vars == "")
								$vars .= " and (";
							$vars .= "companyId = '".$rs->companyId."'";
						}
						
						if (@mysqli_num_rows($sql) == 0)
							$vars .= " and 1=0";
						else
							$vars .= ")";
						
						if (@mysqli_num_rows($sql) > 0) {
							$sql= mysqli_query($con, "select distinct(userId) as user from cui_users_companies where 1=1 $vars") or die(mysqli_error($con));
							$vars = "";
							while ($rs = mysqli_fetch_object($sql)) {
								if ($vars != "")
									$vars .= " or ";
								if ($vars == "")
									$vars .= " and (";
								$vars .= "userId = '".$rs->user."'";
							}
							if (@mysqli_num_rows($sql) == 0)
								$vars .= " and 1=0";
							else
								$vars .= ")";
							$sqlVars = $sqlVars.$vars;
						}
						else {
							$sqlVars = $sqlVars.$vars;
						}
					}
				}
				else{
					if ($filter != '' && $filter2 == '') {
						$sqlVars .= " and $filter like '%".$filterText."%'";
					} elseif ($filter != '' && $filter2 != '') {
						$sqlVars .= " and $filter like '%".$filterText."%' $filterAndOr ($filter2 like '%".$filterText2."%')";
					} elseif ($filter == '' && $filter2 != '') {
						$sqlVars .= " and $filter2 like '%".$filterText2."%'";
					} else {
						$sqlVars .= " and $filter like '%".$filterText."%'";
					}
				}
				
				
				$queryString .= "&filter=".$filter."&filterText=".$filterText."&filter2=".$filter2."&filterText2=".$filterText2;
			}
			
		}
		
		//if sort by is there
		if($sortBy!=""){
			$sqlVars .= " order by $sortBy $sortOrder";
		}else{
			$sqlVars .= " order by userFname ASC";
		}
		//echo "SELECT COUNT(*) from cui_users where userId !='0' $sqlVars";
		$total_results = mysqli_result(mysqli_query($con, "SELECT COUNT(*) from cui_users where userId !='0' $sqlVars"),0);
		$itemCount = $total_results;
		
		
		if($itemCount>0){
			//pagination vars
			$limit = 20;
			$PaginateIt = new pagination();
			$PaginateIt->SetItemsPerPage($limit);
			$PaginateIt ->SetLinksToDisplay(5);
			$PaginateIt->SetItemCount($itemCount);
			$PaginateIt->SetLinksFormat( '<< Back', ' ', 'Next >>' );
			if($_GET["page"]){
				$pageVar = ($limit*$_GET["page"]) - $limit;
			}else{
				$pageVar = 0;
			}
			$productPageLinks = $PaginateIt->GetPageLinks();	
			$counter=0;
			$sqlUsers="select * from cui_users where userId !='0' $sqlVars ".$PaginateIt->GetSqlLimit();
			$sqlUsersResult=mysqli_query($con, $sqlUsers);
			while ($sqlUsersRs=@mysqli_fetch_array($sqlUsersResult)) {
				$counter++;
				$userId=$sqlUsersRs["userId"];
				$userLogin=$sqlUsersRs["userLogin"];
				$userFname=$sqlUsersRs["userFname"];
				$userLname=$sqlUsersRs["userLname"];
				$userCountry=$sqlUsersRs["userCountry"];
				$userCity=$sqlUsersRs["userCity"];
				$userState=$sqlUsersRs["userState"];
				$userZip=$sqlUsersRs["userZip"];
				$userEmail=$sqlUsersRs["userEmail"];
				$userLevel=$sqlUsersRs["userLevel"];
				$userLevelTitle = getField("cui_users_levels","levelId",$userLevel,"levelTitle");
				$userStatus=$sqlUsersRs["userStatus"];
				
				//replacing pass by Noman at Feb 19, 2014
				/*$userCPass=$sqlUsersRs["userCleanPassword"];
				$currPass=encrypt_decrypt('encrypt', $userCPass);
				$sql_update_pass = mysql_query("update cui_users set userCleanPassword='$currPass' where userId='$userId' and userDefault=1");*/
				
				//this will insert jobsview right for all employees
				//tempInsert($userId, "moduleJobsView");
			
				if($userStatus == 1){
					$userStatusImg = '<img alt="Active" src="'.HTTP_SERVER.'images/green.png" border="0" />';
				}else{
					$userStatusImg = '<img alt="Blocked" src="'.HTTP_SERVER.'images/red.png" border="0" />';
				}
				
				if($counter>1){
					$bgcolor="lightgrey";
					$counter=0;
				}elsE{
					$bgcolor="#FFFFFF";
				}
				?>
				
				<tr class="alternatebg">
					<td align="center"><?=$userStatusImg?></td>
					<td><?=$userLogin?></td>
					<td><?=$userFname?>&nbsp;<?=$userLname?></td>
					<td align="center"><?=$userCity?></td>
					<td align="center"><?=$userState?></td>
					<td><?=$userEmail?></td>
					<td align="center"><?=$userLevelTitle?></td>			
					<td>
						<table cellpadding="0" cellspacing="2" align="center">
							<tr>
								<?							
								//roles check (one can edit or not)
								if($moduleAll == 1 or $moduleUsersEdit == 1){?>
								<td align="left">
									<button title="Edit" onclick="javascript: window.location='index.php?do=<?=$do?>&act=edit&userId=<?=$userId?><?=$queryString?>'" type="button" class="btn btn-default btn-sm edit">
										<span class="glyphicon glyphicon-pencil"></span> 
									</button>
								</td>
								<td width="5px">&nbsp;</td>
								<?}?>
								<?							
								//roles check (one can delete or not)
								if($moduleAll == 1 or $moduleUsersDelete == 1){?>
								<td>
									<!--<button title="Delete" type="button" onclick="javascript:if(confirm('Proceed with deletion?')){window.location='index.php?do=<?=$do?>&act=delete&userId=<?=$userId?><?=$queryString?>';}" class="btn btn-default btn-sm trash">-->
									<button title="Delete" type="button" onclick="javascript:swal({title: 'Confirmation!',text: 'Proceed with deletion?',type: 'warning',showCancelButton: true,confirmButtonText: 'OK',closeOnConfirm: false},function(){swal.close();window.location='index.php?do=<?=$do?>&act=delete&userId=<?=$userId?><?=$queryString?>';})" class="btn btn-default btn-sm trash">
										<span class="glyphicon glyphicon-trash"></span> 
									</button>
								</td>
								<td width="5px">&nbsp;</td>
								<?}?>
								<?							
								//roles check (one can edit status or not)
								if($moduleAll == 1 or $moduleUsersEdit == 1){?>
								<td>
									<?if($userStatus == "1"){?>
										<a title="Block" href="javascript: window.location='index.php?do=<?=$do?>&act=changeStatus&parameter=disable&userId=<?=$userId?><?=$queryString?>'"><img src="<?=HTTP_SERVER?>images/red.png" border="0" alt="Block" /></a>
									<?}else{?>
										<a title="Activate" href="javascript: window.location='index.php?do=<?=$do?>&act=changeStatus&parameter=enable&userId=<?=$userId?><?=$queryString?>'"><img src="<?=HTTP_SERVER?>images/green.png" alt="Activate" border="0" /></a>
									<?}?>
								</td>
								<td width="5px">&nbsp;</td>
								<td>	
									<button title="Assign Company Offices" type="button" class="btn btn-default btn-sm btactive" onclick="javascript: window.location='index.php?do=<?=$do?>_companies&userId=<?=$userId?>&userType=<?=$userType?>'">
										<span class="glyphicon glyphicon-ok"></span> 
									</button>
								</td>
								<?}?>
								<?	
								
								/*						
								//roles check (one can delete or not)
								if($userLevelCode == "accountexecutive" and ($moduleAll == 1 or $moduleUsersAssignSalesAgents == 1)){?>
								<td>
									<a title="Assign Agents & Specialists" href="index.php?do=<?=$do?>_users&aeId=<?=$userId?>"><img src="<?=HTTP_SERVER?>images/icon_assignperson.png" border="0" alt="Assign Agents & Specialists" /></a>
								</td>
								<td width="5px">&nbsp;</td>
								<?}*/?>
							</tr>
						</table>
					</td>
				</tr>
				<?
			}
			//if pagination is needed
			if($itemCount > $limit){
			?>
			<tr>
				<td colspan="8" align="center"><?echo $productPageLinks;?></td>
			</tr>
			<?
			}
		}else{
			echo "<tr>
				<td colspan=\"8\" align=\"center\" height=\"100px\">No Results Found !</td>
			</tr>";
		}
		?>
		</tbody>
	</table>
<?}?>
</div>
	