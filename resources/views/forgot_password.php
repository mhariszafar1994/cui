<?
//if post
if($_POST){
	$error = 0;
	$userName = sanitize($_POST["userName"]);
	$dentalCompany = sanitize($_POST["dentalCompany"]);
	$dentalOffice = sanitize($_POST["dentalOffice"]);
	$phoneNo = sanitize($_POST["phoneNo"]);
	$comment = sanitize($_POST["message"]);
	$emailAddress = sanitize($_POST["emailAddress"]);
	if($emailAddress == ""){
		$error = 1;
		$error_msg = "Please enter a valid Email Address or Login Id!";
	} elseif($userName == ""){
		$error = 1;
		$error_msg = "Please enter a valid Name/User Name!";
	} elseif($dentalCompany == ""){
		$error = 1;
		$error_msg = "Please enter a valid Dental Company!";
	} elseif($phoneNo == ""){
		$error = 1;
		$error_msg = "Please enter a valid Phone No!";
	} else{
		$sqlEmail = "SELECT * FROM cui_users WHERE userEmail = '$emailAddress' or userLogin = '$emailAddress'";
		$resultEmail = mysqli_query($con, $sqlEmail);
		if(@mysqli_num_rows($resultEmail)>0){
			//random password
			/*$userId = sanitize(mysqli_result($resultEmail,0,"userId"));
			$userFname = sanitize(mysqli_result($resultEmail,0,"userFname"));
			$userEmail = sanitize(mysqli_result($resultEmail,0,"userEmail"));
			$randomPassword = substr(session_id(),0,8);
			$randomPasswordEncrypted = md5($randomPassword);
			$sqlUpdate = "UPDATE pmt_users SET userPassword='$randomPasswordEncrypted' where userId='$userId'";
			mysqli_query($con, $sqlUpdate);*/
			$to = "info@checkurinsurance.com"; //Admin Email
			//$to = "write4saadat@hotmail.com"; //Admin Email
			//sending email
			// additional header pieces for errors, From cc's, bcc's, etc 
			$headers = "From: ". $userName . " <". $emailAddress . ">\r\n";
			$headers .= "Reply-To: ". $emailAddress . "\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .="Content-type: text/html; charset=iso-8859-1\r\n";	
			
			//subject
			$subject = "Password Reset Request - ". $dentalCompany ."";
			
			//message
			$message = "<html><body>";
			$message .= "<h1>CheckUrInsurance - Forgot Password Submission</h1>";
			$message .= "<h2>Forgot Password Request from $userName - $dentalCompany</h2>";
			$message .= "<h3>Dear Admin,</h3>";
			$message .= "<p>$userName requested to reset the password, following informations submitted by $userName as below:</p>";
			$message .= "<table width='700' border='1' cellpadding='5' style='border-collapse: collapse;'>";
			
			$message .= "<tr><td width='150'>UserName/Name</td>";
			$message .= "<td width='550'>$userName</td></tr>";
			
			$message .= "<tr><td width='150'>Dental Company</td>";
			$message .= "<td width='550'>$dentalCompany</td></tr>";
			
			$message .= "<tr><td width='150'>Dental Office</td>";
			$message .= "<td width='550'>$dentalOffice</td></tr>";
			
			$message .= "<tr><td width='150'>Phone No</td>";
			$message .= "<td width='550'>$phoneNo</td></tr>";
			
			$message .= "<tr><td width='150'>Email Address</td>";
			$message .= "<td width='550'>$emailAddress</td></tr>";
			
			$message .= "<tr><td width='150'>Message</td>";
			$message .= "<td width='550'>$comment</td></tr>";
			
			$message .= "</table>";
			$message .= "</body></html>";
			
			/*$message = "Dear $userFname \n\n";
			$message .= "Your password has been reset and your new password is: \n\n";		
			$message .= "$randomPassword \n\n";
			$message .= "Please use the new password above to login from now on. You can always change your password after logging in to the system";*/
			
			mail($to,$subject,$message,$headers);
			
			echo "<script>window.location='".HTTP_SERVER."index.php?do=$do&success=1'</script>";
		}else{
			$error = 1;
			$error_msg = "The Email Address / Login Id you entered was not found in the database!";
		}
	}
}

?>
<style>
		hr {
			border-bottom: 0px;
		}
		table.form-spacing tbody tr td {
			padding-bottom: 9px;
		}
		</style>

			<div class="row lightblue">
			<div class="container">
				<div class="loginbox col-md-6 col-md-offset-3 col-xs-offset-2">
					<div class="customer_login">
						<h4 class="customer_login_heading">CUI Forgot Password</h4>
					</div>
					<div class="linestyles">
						<p>
						Please enter either your email address or login id your new password will be sent to you.
						</p>
					</div>
					<div class="loginbox_fields">
					<form method="POST" class="form">
						<?if($error == 1){?>
						<div class="error" style="margin-bottom:15px;"><?=$error_msg?></div>
						<? } ?>
						<? if(isset ($_GET["success"]) && $_GET["success"] != ''){ ?>
						<div class="success">Your message has been sent. CUI Support will get back to you very soon.</div>
						<? } ?>
						<div class="form-group">
							<div class="icon-addon addon-md">
								<input type="text" placeholder="Enter your name here" class="form-control" id="userName" name="userName" value="<?=sanitize($_POST['userName']);?>">
								<label for="userName" class="glyphicon glyphicon-user" rel="tooltip" title="Enter Your Name"></label>
							</div>
						</div>
						
						<div class="form-group">
							<div class="icon-addon addon-md">
								<input type="text" placeholder="Enter your dental company here" class="form-control" id="dentalCompany" name="dentalCompany" value="<?=sanitize($_POST['dentalCompany']);?>">
								<label for="dentalCompany" class="glyphicon glyphicon-asterisk" rel="tooltip" title="Enter Dental Company"></label>
							</div>
						</div>
						
						<div class="form-group">
							<div class="icon-addon addon-md">
								<input type="text" placeholder="Enter your dental office here" class="form-control" id="dentalOffice" name="dentalOffice" value="<?=sanitize($_POST['dentalOffice']);?>">
								<label for="dentalOffice" rel="tooltip" title="Enter Dental Office"></label>
							</div>
						</div>
						
						<div class="form-group">
							<div class="icon-addon addon-md">
								<input type="text" placeholder="Enter your phone no here" class="form-control" id="phoneNo" name="phoneNo" value="<?=sanitize($_POST['phoneNo']);?>">
								<label for="phoneNo" class="glyphicon glyphicon-phone" rel="tooltip" title="Enter Phone No"></label>
							</div>
						</div>
						
						<div class="form-group">
							<div class="icon-addon addon-md">
								<input type="email" placeholder="Enter your email here" class="form-control" id="emailAddress" name="emailAddress" value="<?=sanitize($_POST['emailAddress']);?>">
								<label for="emailAddress" class="glyphicon glyphicon-envelope" rel="tooltip" title="Enter Email Address"></label>
							</div>
						</div>
						
						<div class="form-group">
							<div class="icon-addon addon-md">
							    <input type="hidden" name="csrf_token" value="<?php echo hash_hmac('sha256', '/my_form.php', $_SESSION['second_token']);?>" />
								<!--<input type="text" placeholder="Enter your email here" class="form-control" id="message" name="message">-->
								<textarea placeholder="Enter your message here" rows="4" class="form-control" id="message" name="message"><?=sanitize($_POST['message']);?></textarea>
								<label for="message" rel="tooltip" title="Enter Message"></label>
							</div>
						</div>
			
						<div class="form-group">
							<button type="submit" class="btn sign-in_btn" name="submit">Submit</button>
							<div class="pull-right forgotpassword">
								<ul style="list-style: none; padding">
									<li style="display: inline;"><a href="<?php echo HTTP_SERVER;?>index.php?do=forgot_password">Forgot password?</a></li>
									<li style="display: inline; color: black">|</li>
									<li style="display: inline;"><a href="javascript:window.location='<?php echo str_replace('app/', '', HTTP_SERVER);?>contact-us/'">New User?</a></li>
								</ul>
							</div>
							
						</div>
					</form> 
					</div>
				</div>
			</div>
			</div>