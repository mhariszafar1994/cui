<?
//roles check
if($moduleAll == 0 and $moduleOfficesView == 0){
	echo "<script>window.location='index.php?do=authorization'</script>";
}
?>
<h1 class="h1WithBg">Company Offices</h1>
<div id="pageContainer">
	
<?
//vars
$success = 0;
$act = "";
$showOfficeForm = 0;
$msg = "";
$sortBy = "";
$sortOrder = "";
$queryString = "";


//if get sortBy
if($_GET["sortBy"]){
	$sortBy = sanitize($_GET["sortBy"]);
}

//if get sortOrder
if($_GET["sortOrder"]){
	$sortOrder = sanitize($_GET["sortOrder"]);
}
//if get act
if($_GET["act"]){
	$act = sanitize($_GET["act"]);
}

//if get msg
if($_GET["msg"]){
	$msg = sanitize($_GET["msg"]);
}

//if get companyId
if($_GET["companyId"]){
	$companyId = sanitize($_GET["companyId"]);
	$companyName = getField("cui_companies","companyId",$companyId,"companyName");
	$queryString .= "&companyId=$companyId";
	$sqlVars .= " and b.companyId=$companyId";
}

//if act == changestatus
if($act == "changeStatus"){
	
	//roles check (one can edit or not)
	if($moduleAll == 0 and $moduleOfficesEdit == 0){
		echo "<script>window.location='index.php?do=authorization'</script>";
	}
	
	$officeId = sanitize($_GET["officeId"]);
	$statusParameter = sanitize($_GET["parameter"]);
	if($statusParameter == "disable"){
//		mysql_query("UPDATE cui_companies_offices set officeStatus=0 where officeId='$officeId'");
		mysqli_query($con, "UPDATE cui_companies_offices set officeStatus=0 where officeId='$officeId'");
		$success = 1;
		$msg = "Success: Office has been blocked,";
	}else{
//		mysql_query("UPDATE cui_companies_offices set officeStatus=1 where officeId='$officeId'");
		mysqli_query($con, "UPDATE cui_companies_offices set officeStatus=1 where officeId='$officeId'");
		$success = 1;
		$msg = "Success: Office has been activated,";
	}
}

//if act is delete
if($act == "delete"){	
	
	//roles check (one can delete or not)
	if($moduleAll == 0 and $moduleOfficesDelete == 0){
		echo "<script>window.location='index.php?do=authorization'</script>";
	}
	
	$officeId=sanitize($_GET["officeId"]);
	if(getCountField("cui_patients","officeId","where officeId=".$officeId)==0){
		$deleteSql = "DELETE from cui_companies_offices where officeId='$officeId'";
//		mysql_query($deleteSql);
		mysqli_query($con, $deleteSql);
		$success = 1;
		$msg = "Success: Office has been deleted,";
	}else{
		$error = "Office has patients assigned to it. Delete them first!";
	}
}

//getting data
if($act == "edit"){
	
	//roles check (one can edit or not)
	if($moduleAll == 0 and $moduleOfficesEdit == 0){
		echo "<script>window.location='index.php?do=authorization'</script>";
	}
	
	$headingTitle = "Edit Office";
	$showOfficeForm = 1;
	$officeId=sanitize($_GET["officeId"]);
	$companySql="select * from cui_companies_offices where officeId='$officeId'";
//	$companyResult=mysql_query($companySql);
	$companyResult=mysqli_query($con, $companySql);
//	if(mysql_num_rows($companyResult)){
	if(@mysqli_num_rows($companyResult)){
//		$companyId = mysql_result($companyResult,0,"companyId");
		$companyId = mysqli_result($companyResult,0,"companyId");
		$companyName=getField("cui_companies","companyId",$companyId,"companyName");
//		$officeName=mysql_result($companyResult,0,"officeName");
		$officeName=mysqli_result($companyResult,0,"officeName");
//		$officeAddress1=mysql_result($companyResult,0,"officeAddress1");
		$officeAddress1=mysqli_result($companyResult,0,"officeAddress1");
//		$officeAddress2=mysql_result($companyResult,0,"officeAddress2");
		$officeAddress2=mysqli_result($companyResult,0,"officeAddress2");
//		$officeCity=mysql_result($companyResult,0,"officeCity");
		$officeCity=mysqli_result($companyResult,0,"officeCity");
//		$officeState=mysql_result($companyResult,0,"officeState");
		$officeState=mysqli_result($companyResult,0,"officeState");
//		$officeZip=mysql_result($companyResult,0,"officeZip");
		$officeZip=mysqli_result($companyResult,0,"officeZip");
//		$officePhone=mysql_result($companyResult,0,"officePhone");
		$officePhone=mysqli_result($companyResult,0,"officePhone");
//		$officeFax=mysql_result($companyResult,0,"officeFax");
		$officeFax=mysqli_result($companyResult,0,"officeFax");
//		$officeCPerson=mysql_result($companyResult,0,"officeCPerson");
		$officeCPerson=mysqli_result($companyResult,0,"officeCPerson");
		$btnValue = "Update";
	}
}else{		
	$officeName="";
	$officeAddress1="";
	$officeAddress2="";
	$officeCity="";
	$officeState="";
	$officeZip="";
	$officePhone="";
	$officeFax="";
	$officeCPerson="";
	$btnValue = "Add";
}

//check for state tr alternate
if($companyCountry == "United States"){
	$stateTrAlternate = 'style="display:none"';
	$stateTr = "";
}else{
	$stateTr = 'style="display:none"';
	$stateTrAlternate = "";
}

if($act == "add"){
	
	//roles check (one can add or not)
	if($moduleAll == 0 and $moduleOfficesAdd == 0){
		echo "<script>window.location='index.php?do=authorization'</script>";
	}
	
	$headingTitle = "Add New Office";
	$showOfficeForm = 1;
}


//saving information
if($_POST["submit"]){
	$showOfficeForm = 1;
	$officeName = sanitize($_POST["officeName"]);
	$officeAddress1 = sanitize($_POST["officeAddress1"]);
	$officeAddress2 = sanitize($_POST["officeAddress2"]);
	$officeCity = sanitize($_POST["officeCity"]);	
	$officeState = sanitize($_POST["officeState"]);
	$officeZip = sanitize($_POST["officeZip"]);
	$officePhone = sanitize($_POST["officePhone"]);
	$officeFax = sanitize($_POST["officeFax"]);
	$officeCPerson = sanitize($_POST["officeCPerson"]);
	if($act == "add"){
		$sqlCheck = "SELECT * FROM cui_companies_offices where companyId = '$companyId' and officeName='$officeName'";
//		$sqlResult = mysql_query($sqlCheck);
		$sqlResult = mysqli_query($con, $sqlCheck);

//		if(mysql_num_rows($sqlResult)<1){
		if(@mysqli_num_rows($sqlResult)<1){

			$sqlOffice="INSERT INTO cui_companies_offices (companyId, officeName, officeAddress1, officeAddress2, officeCity, officeState, officeZip, officePhone, officeFax, officeCPerson) values ('$companyId', '$officeName', '$officeAddress1', '$officeAddress2', '$officeCity', '$officeState', '$officeZip', '$officePhone', '$officeFax', '$officeCPerson')";
//			mysql_query($sqlOffice);
			mysqli_query($con, $sqlOffice);
			$success=1;
			$msg = "Success: Office has been added.";
			echo "<script>window.location='".HTTP_SERVER."index.php?do=companies_offices&companyId=$companyId&msg=$msg'</script>";
		}else{
			$error = "The company details you provided match another company in the database.";
		}
	}else{
		$sqlOffice="update cui_companies_offices set companyId='$companyId', officeName='$officeName', officeAddress1='$officeAddress1', officeAddress2='$officeAddress2', officeCity='$officeCity', officeState='$officeState', officeZip='$officeZip', officePhone='$officePhone', officeFax='$officeFax', officeCPerson='$officeCPerson' where officeId=$officeId";
//		mysql_query($sqlOffice);
		mysqli_query($con, $sqlOffice);

		$success=1;
		$msg = "Success: Office details have been updated.";
		echo "<script>window.location='".HTTP_SERVER."index.php?do=companies_offices&companyId=$companyId&msg=$msg'</script>";
	}
	
}

//filters
if($_GET["filter"]){
	$filter = sanitize($_GET["filter"]);
	$filterText = sanitize($_GET["filterText"]);
	$queryString .= "&filter=$filter&filterText=$filterText";
}else{
	$filterText = "";
	$filter = "";
}

?>

<?if($msg!=""){?>
	<div class="success"><?=$msg?></div>
<?}?>

<script type="text/javascript" language="JavaScript">
	function chkForm(){
		var phoneRE = /^\d{3}-\d{3}-\d{4}$/;
		
		if(document.getElementById("officeName").value==""){
			/*alert("Office Name is required!");
			document.getElementById("officeName").focus();*/
			swal({
				title: 'Error!',
				text: 'Office Name is required!',
				type: "error",
				showCancelButton: false,
				confirmButtonText: 'OK',
				closeOnConfirm: false
			},
			function(){
				swal.close();
				$("#officeName").focus();
			});
			return false;
		}
		
		if(document.getElementById("officePhone").value!=""){
			if (!phoneRE.test(document.getElementById("officePhone").value)){
				/*alert("Phone Number is in invalid format!");
				document.getElementById("officePhone").focus();*/
				swal({
					title: 'Error!',
					text: 'Phone Number is in invalid format!',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#officePhone").focus();
				});
				return false;
			}
		}
		
		if(document.getElementById("officeFax").value!=""){
			if (!phoneRE.test(document.getElementById("officeFax").value)){
				/*alert("Fax Number is in invalid format!");
				document.getElementById("officeFax").focus();*/
				swal({
					title: 'Error!',
					text: 'Fax Number is in invalid format!',
					type: "error",
					showCancelButton: false,
					confirmButtonText: 'OK',
					closeOnConfirm: false
				},
				function(){
					swal.close();
					$("#officeFax").focus();
				});
				return false;
			}
		}
		
	}
	
</script>
<style>
		hr {
			border-bottom: 0px;
		}
		table.form-spacing tbody tr td {
			padding-bottom: 9px;
		}
		</style>
<div id="addOffice" <?if($showOfficeForm == 0){?>style="display:none"<?}?>>
	<form method="POST">		
		<table class="form-spacing"  align="center" width="100%" border="0" cellpadding="5" cellspacing="1">
			<tr class="titleTr">
				<td><h3 style="padding-top: 9px; padding-left: 5px;"><?=$headingTitle?></h3></td>
			</tr>
			<tr>
				<td>
					<?if($error != ""){?>
						<div class="error"><?=$error?></div>		
					<?}else{?>
						<div class="message">The required fields are marked with <span class="required">*</span></div>
					<?}?>
				</td>
			</tr>
			<tr>
				<td>
					<table width="50%" align="center" cellpadding="5" cellspacing="0">
						<tr>
							<td>Company</td>
							<td><?=$companyName?></td>
						</tr>
						<tr>
							<td>Office Name <span class="required">*</span></td>
							<td><input type="text" class="textbox" id="officeName" name="officeName" value="<?=$officeName?>" /></td>
						</tr>
						
						<tr>
							<td>Office Address 1</td>
							<td><input type="text" class="textbox" id="officeAddress1" name="officeAddress1" value="<?=$officeAddress1?>" style="width: 250px" /></td>
						</tr>
						<tr>
							<td>Office Address 2</td>
							<td><input type="text" class="textbox" id="officeAddress2" name="officeAddress2" value="<?=$officeAddress2?>" style="width: 250px" /></td>
						</tr>
						<tr>
							<td>City</td>
							<td><input type="text" class="textbox" id="officeCity" name="officeCity" value="<?=$officeCity?>" /></td>
						</tr>
						<tr id="stateTr">
							<td>State</td>
							<td>
								<select name="officeState" id="officeState">
									<?
									$stateSql = "SELECT * from cui_states order by stateName ASC";
//									$stateResult = mysql_query($stateSql);
									$stateResult = mysqli_query($con, $stateSql);
//									while ($stateRs = mysql_fetch_array($stateResult)) {
									while ($stateRs = @mysqli_fetch_array($stateResult)) {

										$getStateName = $stateRs["stateName"];
										
										if($officeState == $getStateName){
											$sel = "selected";
										}else{
											$sel = "";
										}
										
										echo "<option $sel value=\"$getStateName\">$getStateName</option>";
									}
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td>Zip Code</td>
							<td><input type="text" class="textbox" id="officeZip" name="officeZip" size="10" value="<?=$officeZip?>" /></td>
						</tr>
						<tr>
							<td>Phone</td>
							<td><input type="text" class="textbox" id="officePhone" name="officePhone" value="<?=$officePhone?>" /> (eg. xxx-xxx-xxxx)</td>
						</tr>
						<tr>
							<td>Fax</td>
							<td><input type="text" class="textbox" id="officeFax" name="officeFax" value="<?=$officeFax?>" /> (eg. xxx-xxx-xxxx)</td>
						</tr>
						<tr>
							<td>Contact Person</td>
							<td><input type="text" class="textbox" id="officeCPerson" name="officeCPerson" value="<?=$officeCPerson?>" /></td>
						</tr>
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td><input type="submit" class="btn searchbt" name="submit" value="<?=$btnValue?>" onclick="return chkForm()" />&nbsp;<input type="button" class="btn clearbt" value="Cancel" onclick="javascript:window.location='index.php?do=<?=$do?>&companyId=<?=$companyId?>'" /></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
</div>

<?if($showOfficeForm == 0){?>
	<?if($error != ""){?>
		<div class="error"><?=$error?></div>
	<?}?>
	<div id="help">
		<div id="helpContent" style="display: none">
			<table cellpadding="3" cellspacing="0">	
				
				<tr>			
					<td><span title="Delete" class="glyphicon glyphicon-trash"></span></td>
					<td>&nbsp;</td>
					<td>Delete an existing company.</td>
				</tr>
				<tr>			
					<td><span title="Edit" class="glyphicon glyphicon-pencil"></span></td>
					<td>&nbsp;</td>
					<td>Edit existing company information.</td>
				</tr>
				<tr>			
					<td><img alt="Active" src="<?=HTTP_SERVER?>images/green.png" border="0" /></td>
					<td>&nbsp;</td>
					<td>Office's status is active.</td>
				</tr>
				<tr>			
					<td><img alt="Blocked" src="<?=HTTP_SERVER?>images/red.png" border="0" /></td>
					<td>&nbsp;</td>
					<td>Office's status is inactive / blocked.</td>
				</tr>
			</table>
		</div>
	</div>	
	<!--<form method="GET" action="index.php">-->
		<div class="searchcard">
			<div class="searchformcontainer">
				<form class="form-inline" method="GET" action="index.php">
				<input type="hidden" name="do" value="<?=$do?>" />
				<input type="hidden" name="companyId" value="<?=$companyId?>" />
				<div class="form-group">
					<select name="filter" class="selectpicker">
						<option value="">--- select ---</option>
						<option <?if($filter == "officeName"){?>selected<?}?> value="officeName">Office Name</option>										
						<option <?if($filter == "officeCity"){?>selected<?}?> value="officeCity">Office City</option>
						<option <?if($filter == "officeState"){?>selected<?}?> value="officeState">Office State</option>
						<option <?if($filter == "officeZip"){?>selected<?}?> value="officeZip">Office Zip</option>
					</select>
				</div>
				<div class="form-group">
					<input type="text" name="filterText" id="filterText" class="form-control" id="text" value="<?=$filterText;?>" placeholder="Search Text">
				</div>
				<div class="form-group">
					<input type="hidden" name="userType" value="<?=$userType?>" />
					<button type="submit" class="btn searchbt">Search</button>
				</div>
				<div class="form-group">
					<?
					if (isset($_GET['companyId']) && $_GET['companyId'] != '') {
						$clearfilter = "&companyId=$_GET[companyId]";
					} else {	
						$clearfilter = '';
					}
					?>
					<button type="button" onclick="javascript: window.location='index.php?do=companies_offices<?=$clearfilter;?>'" class="btn clearbt">Clear</button>
				</div>
				<?							
				//roles check (one can add or not)
				if($moduleAll == 1 or $moduleOfficesAdd == 1){?>
				<div class="form-group">
					<button type="button" onclick="javascript: window.location='index.php?do=companies_offices&companyId=<?=$companyId?>&act=add'" class="btn greenbt">Add Office</button>
				</div>
				<? } ?>
				</form>
			</div>
		</div>	
	<!--</form>-->
	<br />	
	<table class="table table-bordered form-spacing" align="center" width="100%" border="0" cellpadding="5" cellspacing="1">
		<thead>
		<tr>
			<td colspan="7"><h3 style="color:black;">Office Listing</h3></td>
		</tr>
		<tr align="center">
			<td width="5%"><a href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=officeStatus&sortOrder=<?if($sortBy == "officeStatus"){if($sortOrder == "ASC"){echo "DESC";}else{echo "ASC";}}else{echo "ASC";}?>">Status<?if($sortBy == "officeStatus"){?>&nbsp;<img src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0" alt="<?=$sortOrder?> Order" /><?}?></a></td>
			<td width="25%"><a href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=companyName&sortOrder=<?if($sortBy == "companyName"){if($sortOrder == "ASC"){echo "DESC";}else{echo "ASC";}}else{echo "ASC";}?>">Company Name<?if($sortBy == "companyName"){?>&nbsp;<img src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0" alt="<?=$sortOrder?> Order" /><?}?></a></td>
			<td width="25%"><a href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=officeName&sortOrder=<?if($sortBy == "officeName"){if($sortOrder == "ASC"){echo "DESC";}else{echo "ASC";}}else{echo "ASC";}?>">Office Name<?if($sortBy == "officeName"){?>&nbsp;<img src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0" alt="<?=$sortOrder?> Order" /><?}?></a></td>
			<td width="15%"><a href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=officeCity&sortOrder=<?if($sortBy == "officeCity"){if($sortOrder == "ASC"){echo "DESC";}else{echo "ASC";}}else{echo "ASC";}?>">Office City<?if($sortBy == "officeCity"){?>&nbsp;<img src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0" alt="<?=$sortOrder?> Order" /><?}?></a></td>
			<td width="10%"><a href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=officeState&sortOrder=<?if($sortBy == "officeState"){if($sortOrder == "ASC"){echo "DESC";}else{echo "ASC";}}else{echo "ASC";}?>">Office State<?if($sortBy == "officeState"){?>&nbsp;<img src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0" alt="<?=$sortOrder?> Order" /><?}?></a></td>
			<td width="10%"><a href="<?=HTTP_SERVER?>index.php?do=<?=$do?><?=$queryString?>&sortBy=officeZip&sortOrder=<?if($sortBy == "officeZip"){if($sortOrder == "ASC"){echo "DESC";}else{echo "ASC";}}else{echo "ASC";}?>">Office Zip<?if($sortBy == "officeZip"){?>&nbsp;<img src="<?=HTTP_SERVER?>images/sort_<?=strtolower($sortOrder)?>.png" border="0" alt="<?=$sortOrder?> Order" /><?}?></a></td>
			<td width="10%">Actions</td>
		</tr>
		</thead>
		<tbody>
		<?		
		//variables
		//$sqlVars = "";
		$queryString = "";
		
		//pagination code
		if((!isset($_GET['page'])) && (!isset($_POST['page']))){ 
		    $page = 1; 
		} else { 
			if (!$_GET['page']) {
				$page = $_POST['page']; 
			} else {
				$page = $_GET['page']; 
			}	  
		} 
	
		//if get filters
		if($_GET["filter"]){
			$filter = sanitize($_GET["filter"]);			
			if($filter!=""){
				$filterText = sanitize($_GET["filterText"]);
				
				//more conditions
				if($filter == "officeStatus"){
					if($filterText == strtolower("blocked") or $filterText == strtolower("inactive") or $filterText == strtolower("disabled")){
						$filterText=0;
					}else{
						$filterText=1;
					}
					$sqlVars .= " and $filter =".$filterText;
				}else{
					$sqlVars .= " and $filter like '%".$filterText."%'";	
				}
				
				
				$queryString .= "&filter=".$filter."&filterText=".$filterText;
			}
			
		}
		
		//if sort by is there
		if($sortBy!=""){
			$sqlVars .= " order by $sortBy $sortOrder";
		}else{
			$sqlVars .= " order by officeName ASC";
		}
		$total_results = mysqli_result(mysqli_query($con, "select a.*,b.companyName from cui_companies_offices as a join cui_companies as b where a.companyId = b.companyId $sqlVars"),0);
		$itemCount = count($total_results);
		
		
		if($itemCount>0){
			//pagination vars
			$limit = 20;
			$PaginateIt = new pagination();
			$PaginateIt->SetItemsPerPage($limit);
			$PaginateIt ->SetLinksToDisplay(5);
			$PaginateIt->SetItemCount($itemCount);
			$PaginateIt->SetLinksFormat( '<< Back', ' ', 'Next >>' );
			if($_GET["page"]){
				$pageVar = ($limit*$_GET["page"]) - $limit;
			}else{
				$pageVar = 0;
			}
			$productPageLinks = $PaginateIt->GetPageLinks();
			$counter=0;
			$sqlOffices="select a.*,b.companyName from cui_companies_offices as a join cui_companies as b where a.companyId = b.companyId $sqlVars ".$PaginateIt->GetSqlLimit();
			$sqlOfficesResult=mysqli_query($con, $sqlOffices);
			while ($sqlOfficesRs=@mysqli_fetch_array($sqlOfficesResult)) {
				$counter++;
				$officeId=$sqlOfficesRs["officeId"];
				$companyId=$sqlOfficesRs["companyId"];
				$companyName=getField("cui_companies","companyId",$companyId,"companyName");
				$officeName=$sqlOfficesRs["officeName"];
				$officeCity=$sqlOfficesRs["officeCity"];
				$officeState=$sqlOfficesRs["officeState"];
				$officeZip=$sqlOfficesRs["officeZip"];
				$officeStatus=$sqlOfficesRs["officeStatus"];
				
				//this will insert jobsview right for all employees
				//tempInsert($companyId, "moduleJobsView");
				
				if($officeStatus == 1){
					$officeStatusImg = '<img alt="Active" src="'.HTTP_SERVER.'images/green.png" border="0" />';
				}else{
					$officeStatusImg = '<img alt="Blocked" src="'.HTTP_SERVER.'images/red.png" border="0" />';
				}
				
				if($counter>1){
					$bgcolor="lightgrey";
					$counter=0;
				}elsE{
					$bgcolor="#FFFFFF";
				}
				?>
				<tr class="alternatebg">
					<td align="center"><?=$officeStatusImg?></td>
					<td><?=$companyName?></td>
					<td><?=$officeName?></td>
					<td align="center"><?=$officeCity?></td>
					<td align="center"><?=$officeState?></td>
					<td align="center"><?=$officeZip?></td>
					<td>
						<table cellpadding="0" cellspacing="2" align="center">
							<tr>
								<?							
								//roles check (one can edit or not)
								if($moduleAll == 1 or $moduleOfficesEdit == 1){?>
								<td align="left">
									<button title="Edit" type="button" class="btn btn-default btn-sm edit" onclick="window.location='index.php?do=<?=$do?>&act=edit&officeId=<?=$officeId?>&companyId=<?=$companyId?>'">
										<span class="glyphicon glyphicon-pencil"></span> 
									</button>
								</td>
								<td width="5px">&nbsp;</td>
								<?}?>
								<?							
								//roles check (one can delete or not)
								if($moduleAll == 1 or $moduleOfficesDelete == 1){?>
								<td>
									<!--<button title="Delete" type="button" class="btn btn-default btn-sm trash" onclick="javascript:if(confirm('Proceed with deletion?')){window.location='index.php?do=<?=$do?>&act=delete&officeId=<?=$officeId?>&companyId=<?=$companyId?>';}">-->
									<button title="Delete" type="button" class="btn btn-default btn-sm trash" onclick="javascript:swal({title: 'Confirmation!',text: 'Proceed with deletion?',type: 'warning',showCancelButton: true,confirmButtonText: 'OK',closeOnConfirm: false},function(){swal.close();window.location='index.php?do=<?=$do?>&act=delete&officeId=<?=$officeId?>&companyId=<?=$companyId?>';})">
									
										<span class="glyphicon glyphicon-trash"></span> 
									</button>
								</td>
								<td width="5px">&nbsp;</td>
								<?}?>
								<?							
								//roles check (one can edit status or not)
								if($moduleAll == 1 or $moduleOfficesEdit == 1){?>
								<td>
									<?if($officeStatus == "1"){?>
										<a title="Block" href="index.php?do=<?=$do?>&act=changeStatus&parameter=disable&officeId=<?=$officeId?>&companyId=<?=$companyId?>"><img src="<?=HTTP_SERVER?>images/red.png" border="0" alt="Block" /></a>
									<?}else{?>
										<a title="Activate" href="index.php?do=<?=$do?>&act=changeStatus&parameter=enable&officeId=<?=$officeId?>&companyId=<?=$companyId?>"><img src="<?=HTTP_SERVER?>images/green.png" alt="Activate" border="0" /></a>
									<?}?>
								</td>
								
								<?}?>
								
							</tr>
						</table>
					</td>
				</tr>
				<?
			}
			//if pagination is needed
			if($itemCount > $limit){
			?>
			<tr>
				<td colspan="7" align="center"><?echo $productPageLinks;?></td>
			</tr>
			<?
			}
		}else{
			echo "<tr>
				<td colspan=\"7\" align=\"center\" height=\"100px\">No Results Found !</td>
			</tr>";
		}
		?>
		</tbody>
	</table>
<?}?>
</div>
	