          <div class="row lightblue">
			<div class="container">
				<div class="loginbox col-md-6 col-md-offset-3 col-xs-offset-2">
					<div class="customer_login">
						<h4 class="customer_login_heading">Error 404</h4>
					</div>
					<div class="linestyles">
						<p>
						Sorry! It seems that page you are looking for no longer exists. Please <a href="https://checkurinsurance.com/app/">click here</a>.
						</p>
					</div>
				</div>
			</div>
			</div>
            