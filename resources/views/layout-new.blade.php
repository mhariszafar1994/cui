<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Color Admin | Dashboard v3</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
	<link href="{{asset('/assets/css/app.min.css')}}" rel="stylesheet" />
	<link href="{{asset('/assets/plugins/ionicons/css/ionicons.min.css')}}" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{{asset('/assets/css/bootstrap-select.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('/assets/css/select2.min.css')}}">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="{{asset('/assets/css/custom.css')}}">
	<style type="text/css">
		/*Global Styling*/
		.bgcolor-checkurinsurance {
		    background-color: #9e1a03 !important;
		    background: #9e1a03 !important;
		}
		.text-checkurinsurance {
		    color: #9e1a03 !important;
		}
		.bgcolor-checkurinsurance a p {color: #fff;white-space: nowrap;font-weight: 500;font-size: 1.2em;}
		.bgcolor-white {
			background-color: #fff !important;
		    background: #fff !important;
		}
		.btn-checkurinsurance {
			background-color: #9e1a03;
		    border-color: #9e1a03;
		    box-shadow: 0;
		    color: #fff;
		}
		.btn-checkurinsurance:hover {
			color: #fff;
		    background-color: #8e2009;
		    border-color: #8e2009;
		}
		.active-link {
			background-color: #000;
		}
		.border-dashed {
			border: 1px dashed lightgray;
			height: 0;
		}
		.min-h-50 {
			min-height: 500px;
		}
		/*Custom Styling*/
		.title-nav {
			padding: 1.5rem;
			background-color: #f7f7f7;
		}
		.dropdown-item.active, .dropdown-item:active {
		    color: #fff;
		    text-decoration: none;
		    background-color: #9e1a03!important;
		}
		.logo_img {
			width: 100%;
		    object-fit: contain;
		    min-height: 55px;
		}
		.navigation li {
			border-right:2px solid #8e2009;
		}
		.navigation li a {
			text-align: center;
			padding:0.5rem !important;
		}
		.welcome-text {
			font-size: 1.5em;
			display: inline-flex;
		}

		@media screen and (max-width: 1024px) {
			.header {
				display: block;
				position: initial !important;
			}
			.page-header-fixed {
				padding-top: 0 !important;
			}
			.navigation li {
				display: inline-flex;
				width: 9%;
				padding: 0;
			}
			.navigation li a {
				width: 100%;
			}
			.navbar-header {
				width: 100% !important;
			}
			.logo_img {
				min-height: auto;
			}
			.welcome-text {
				display: block;
			}
			.bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
				width: 175px;
			}
			
		}
		@media screen and (max-width: 768px) {
			.pace .pace-progress {
			    top: 0;
			}
			.pace .pace-activity {
			    top: 50px;
			}
			.navbar-toggle {
			    display: block!important;
			}
			.header .navbar-nav {
				display: none;
			}
			.navbar-brand {
				align-items: left;
			}
			.navigation li {
			    display: block;
			    width: 100%;
			    padding: 0;
			}
			.logo_img {
			    width: 100%;
			    object-fit: contain;
			    object-position: 0;
			}
			.navigation li a {
			    padding: 1rem 0 !important;
			}
			.bgcolor-checkurinsurance a p {
				display: inline-block;
			}
			.dropdown {
				width: 100%!important;
				margin:0.5em 0;
			}
			.responsive-elements {
				width: 100%!important;
				margin:0.5em 0;
			}
		}
	</style>
</head>
<body>
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
		<!-- begin #header -->
		<div id="header" class="header navbar-default bgcolor-checkurinsurance">
			<!-- begin navbar-header -->

			<!--class navbar-header -->
			<div class="navbar-header bgcolor-white">
				<a href="javascript:;" class="navbar-brand">
					<img class="logo_img" id="desktop_view_img" src="{{asset('/assets/img/logo.jpg')}}" alt="">
					<img class="logo_img" id="mobile_view_img" src="assets/img/pp-logo.png" alt="">
					<!-- <span class="navbar-logo"><i class="ion-ios-cloud"></i></span> <b>Color</b> Admin -->
				</a>
				<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<!-- end navbar-header -->
			<!-- begin header-nav -->



			<ul class="navbar-nav navbar-left navigation">
				<li class="col active-link"> 
					<a href="" title="">
						<img src="{{asset('/assets/img/icon_dashboard.png')}}" alt="" />
						<p class="m-0">Home</p>
					</a>
				</li>
				<li class="col"> 
					<a href="" title="">
						<img src="{{asset('/assets/img/icon_users_roles.png')}}" alt="" />
						<p class="m-0">Users</p>
					</a>
				</li>
				<li class="col"> 
					<a href="" title="">
						<img src="{{asset('/assets/img/icon_doctors.png')}}" alt="" />
						<p class="m-0">Doctors</p>
					</a>
				</li>
				<li class="col"> 
					<a href="" title="">
						<img src="{{asset('/assets/img/icon_levels.png')}}" alt="" />
						<p class="m-0">Users Levels</p>
					</a>
				</li>
				<li class="col"> 
					<a href="" title="">
						<img src="{{asset('/assets/img/icon_company.png')}}" alt="" />
						<p class="m-0">Companies</p>
					</a>
				</li>
				<li class="col"> 
					<a href="" title="">
						<img src="{{asset('/assets/img/icon_patient.png')}}" alt="" />
						<p class="m-0">Patients</p>
					</a>
				</li>
				<li class="col"> 
					<a href="" title="">
						<img src="{{asset('/assets/img/icon_check.png')}}" alt="" />
						<p class="m-0">Check</p>
					</a>
				</li>
				<li class="col"> 
					<a href="" title="">
						<img src="{{asset('/assets/img/icon_updated.png')}}" alt="" />
						<p class="m-0">Updated</p>
					</a>
				</li>
				<li class="col"> 
					<a href="" title="">
						<img src="{{asset('/assets/img/icon_reports.png')}}" alt="" />
						<p class="m-0">Reports</p>
					</a>
				</li>
				<li class="col dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img src="{{asset('/assets/img/icon_profile.png')}}" alt="" />
						<p class="m-0">Settings</p>
					</a>
					<div class="dropdown-menu">
						<a href="javascript:;" class="dropdown-item">Change Password</a>
						<a href="javascript:;" class="dropdown-item">Log Out</a>
					</div>
				</li>
				<li class="col"> 
					<a href="" title="">
						<img src="{{asset('/assets/img/icon_logout.png')}}" alt="" />
						<p class="m-0">Logout</p>
					</a>
				</li>
				
			</ul>
		</div>
		<!-- end #header -->


		<div class="container-fluid">
			<div class="alert alert-light text-dark mt-3 d-md-flex align-items-center">
				<p class="welcome-text mb-0">Welcome to CheckUrInsurance <b class="text-checkurinsurance"> &nbsp;John!</b></p>
				<div class="ml-auto">
					<select class="selectpicker mr-2" data-style="btn-white">
						<option value="" selected>All Companies</option>
					</select>
					<select class="selectpicker" data-style="btn-white">
						<option value="" selected>All Offices</option>
					</select>
				</div>
			</div>
		</div>



	@yield('content')



	<!-- ================== BEGIN BASE JS ================== -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" type="text/javascript"></script>
<script src="{{asset('/assets/js/app.min.js')}}"></script>
<script src="{{asset('/assets/js/theme/apple.min.js')}}"></script>
<script src="{{asset('/assets/js/bootstrap-select.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/assets/js/select2.min.js')}}" type="text/javascript"></script>
<!-- ================== END BASE JS ================== -->

<script type="text/javascript">
	$(document).ready(function(){
		$('.navbar-toggle').click(function(){
			$('.navigation').slideToggle();
		})
	});
</script>
</body>
</html>