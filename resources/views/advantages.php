<h1>About Us</h1>
<div id="pageContainer">		
	<table width="100%" cellpadding="15" cellspacing="0">
		<tr>
			<td valign="top">
				<span class="subHeading">Some of the advantages of Check Ur Insurance are as follows:</span><br />
				<ul>
					<li>1: Patient database is within a click away.</li>
					<li>2: Accessibility to patient�s information and their insurance carriers 24/7.</li>
					<li>3: Reduce your staffing cost while improving quality of service in your clinic.</li>
					<li>4: Cut your staff�s benefits cost and Workers Compensation.</li>
					<li>5: Software designed for your clinics requirements.</li>
					<li>6: Continuous support for your business needs as more requirements arise.</li>
					<li>7: Very low setup fees that can be waived if multiple dedicated resources are hired.</li>
				</ul>
				<br />
				Our goal is to guarantee that your satisfaction will not be comprised and that all your needs are met.
Please join our family.<br /><Br />Give us a call at <span class="subHeading">951-479-1346</span> or email at <a href="mailto:info@checkurinsurance.com">info@checkurinsurance.com</a> to see if Check Ur Insurance is right for your office. 
			</td>
			<td align="right" valign="top"><img src="images/advantages_img.gif" /></td>
		</tr>
		
	</table>
</div>	