<h1>About Us</h1>
<div id="pageContainer">		
	<table width="100%" cellpadding="15" cellspacing="0">
		<tr>
			<td valign="top">
				Check Ur Insurance is based in Southern California providing various solutions for medical and dental offices. We saw that clinics lose productivity by spending time collecting patientís insurance information from various insurance carriers.<br /><br />So we partnered up with dental clinics and built this package that consists of online application and dedicated resources and thus outsourced this whole process. In business world it is known as Business Process Outsourcing (BPO).<br /><br />Immediately we saw that clinics increased their productivity by allocating their resource to better use and offloading this tedious task to us. We are confident that you will also see great benefits by using our services.<br /><Br />We are committed to keeping our customers happy. We will work with you to tailor our product to your needs and ensure that Check Ur Insurance works well for each of our customer.<br /><br />
Please give us a call at <span class="subHeading">951-479-1967</span> or email at <a href="mailto:info@checkurinsurance.com">info@checkurinsurance.com</a> to see if Check Ur Insurance is right for your office.
			</td>
			<td valign="top">
				<img src="<?=HTTP_SERVER?>images/aboutus.gif" alt="About Us" />
			</td>
		</tr>
	</table>
</div>	