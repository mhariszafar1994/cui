<h1>About Us</h1>
<div id="pageContainer">		
	<table width="100%" cellpadding="15" cellspacing="0">
		<tr>
			<td valign="top">
				Please contact us to obtain information about our services. Also, please send us feedback using the email addresses below. Tell us what you think about our web site, our services and our organization. We welcome all of your comments and suggestions.
			<br /><br />
			<table cellpadding="5" cellspacing="0">
				<td width="300px" valign="top">
					<table cellpadding="1" cellspacing="0">
						<tr>
							<td><span class="subHeading">Address:</span><br /></td>
						</tr>
						<tr>
							<td><b>Check Ur Insurance</b></td>
						</tr>	
						
						<tr>
							<td>Corona CA 92883</td>
						</tr>
						<tr>
							<td><b>Tel:</b> 951-479-1967</td>
						</tr>
						<tr>
							<td><b>fax:</b> 951-616-2050</td>
						</tr>				
					</table>
				</td>
				<td valign="top">
					<span class="subHeading">Contact Email:</span><br />
					<table cellpadding="4" cellspacing="1">
						<tbody>
				            <tr>
				              <td class="box_black3" style="width: 2%; background-color: rgb(239, 223, 173);"><strong>CheckUrInsurance:</strong></td>
				              <td style="width: 20%; background-color: rgb(239, 223, 173);"><a href="mailto:checkurinsurance@checkurinsurance.com" class="normal4">checkurinsurance@checkurinsurance.com</a></td>
				            </tr>
				            <tr>
				              <td style="width: 2%; background-color: rgb(239, 223, 173);" class="box_black3"><strong>Support:</strong></td>
				
				              <td style="width: 20%; background-color: rgb(239, 223, 173);"><a href="mailto:support@checkurinsurance.com" class="normal4">support@checkurinsurance.com</a></td>
				            </tr>
				            <tr>
				              <td style="width: 2%; background-color: rgb(239, 223, 173);" class="box_black3"><strong>Info:</strong></td>
				              <td style="width: 20%; background-color: rgb(239, 223, 173);"><a href="mailto:info@checkurinsurance.com" class="normal4">info@checkurinsurance.com</a></td>
				            </tr>
				            <tr>
				
				              <td style="width: 2%; background-color: rgb(239, 223, 173);" class="box_black3"><strong>Sales:</strong></td>
				              <td style="width: 20%; background-color: rgb(239, 223, 173);"><a href="mailto:sales@checkurinsurance.com" class="normal4">sales@checkurinsurance.com</a></td>
				            </tr>
				          </tbody>
					</table>
				</td>
			</table>	
			</td>
			<td align="right" valign="top"><img src="images/contactus_img.gif" /></td>
		</tr>
		
	</table>
</div>	