<h1>Access Denied!</h1>
<div id="pageContainer">		
	<table width="100%" cellpadding="15" cellspacing="0">
		<tr>
			<td valign="top">
				You may not have sufficient privileges to view this page. Please contact the administrator for details for details. Click <a href="javascript: history.back(-1)">here</a> to go back!.
			</td>
		</tr>
	</table>
</div>	