<?php
if (isset($_POST['action']) && $_POST['action'] == 'submit') {

//ALTER TABLE `cui_patients_history` CHANGE `remarks` `remarks` LONGTEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
    $data = addslashes($_POST['request_data']);
    $patient_id = $_POST['patient_id'];
    $deleted_by = $_POST['deleted_by'];
    $tblName = $_POST['tblName'];
    $type = $_POST['type'];
    $allFields = $_POST['allFields'];
    $isRemove = $_POST['isRemove'];
    if($isRemove != 'false'){
        $syntax = "DELETE FROM {$tblName} WHERE `patientId` = '{$patient_id}'";
        $removeQuery = mysqli_query($con, $syntax) or die (mysqli_error($con));
    } else {
        $updateQueryString = '';
        $i = 0;
        $totalFields = count($allFields);
        foreach($allFields as $index => $value){
            $updateQueryString .= "`$index`=''";
            if(++$i === $totalFields) {

            } else {
                $updateQueryString .= ", ";
            }
        }
        $syntax = "UPDATE {$tblName} SET {$updateQueryString} WHERE `patientId` = '{$patient_id}'";
        $updateQuery = mysqli_query($con, $syntax) or die (mysqli_error($con));
    }

    $query = mysqli_query($con, "INSERT INTO `cui_patients_deleted_history` (`patient_id`, `reported_data`, `deleted_by`) VALUES ('{$patient_id}', '{$data}', '{$deleted_by}')") or die (mysqli_error($con));

    $btn_html = '<form method="post" action="'.$httpServer.'custom_contents.php" target="_blank">
    <input type="hidden" value="'.mysqli_insert_id($con).'" name="history_id" />
	<input type="hidden" value="'.$type.'" name="type" />
    <input type="hidden" value="'.$patient_id.'" name="pid" />
    <input type="hidden" value="'.getField("cui_patients", "patientId", $patient_id, "patientFname").' '.getField("cui_patients", "patientId", $patient_id, "patientLname").'" name="pname" />
    <input type="hidden" value="'.getField("cui_companies", "companyId", getField("cui_patients","patientId",$patient_id,"companyId"), "companyName").'" name="office_name" />
	<button title="Delete" type="submit" class="btn btn-default btn-sm trash" title=" View Cleared Data ">
		<span class="glyphicon glyphicon-trash"></span> View Cleared Data
	</button>
</form>';
    $current_time = time();
    $history_query = mysqli_query($con, "INSERT INTO `cui_patients_history` (`patientId`, `checkerId`, `checkedDate`, `remarks`) VALUES ('{$patient_id}', '{$deleted_by}', '{$current_time}', '{$btn_html}')") or die (mysqli_error($con));
    $result['bool'] = true;
    echo json_encode($result);
}