<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear_cache', function () {
    $exitCode = Artisan::call('cache:clear');
    $exitCode .= Artisan::call('view:clear');
    $exitCode .= Artisan::call('route:clear');
});

Route::get('/dashboard', function () {
    return view('dashboard');

});
Route::get('/content', function () {
    return view('content');

});
Route::get('/', function () {
    // return view('dashboard');
    return view('login');
});

// Route::get('/dashboard', function () {
//     return view('dashboard');
// });
